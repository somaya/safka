<?php

namespace App\Console\Commands;

use App\Enums\Status;
use App\Events\GetNotificationShop;
use App\Helpers\Setting\Utility;
use App\Models\Admin;
use App\Models\Notification;
use App\Models\Owner;
use App\Models\Payment;
use Carbon\Carbon;
use Illuminate\Console\Command;

class BlockShop extends Command
{

    protected $signature = 'block:shop';


    protected $description = 'block shop when not pay the invoice';


    public function handle()
    {
        $payments = Payment::query()->with(['shop.owner.user'])
            ->where([
                'status' => Status::Not_Active->value,
                //['end_date', '=', now()->format('Y-m-d')]
            ])->get();
        if ($payments) {
            foreach ($payments as $payment) {
                $paymentId = $payment->id;
                $owner = $payment->shop?->owner?->user;
                $data = Carbon::parse($payment->end_date)->addDays(Utility::getValByName('grace_period'))->format('Y-m-d');
                if ($data == now()->format('Y-m-d')) {
                    $payment->shop->update(['status' => Status::Not_Active->value]);
                } else {
                    $notification = Notification::query()->create([
                        'type' => 'invoice',
                        'senderable_type' => Admin::class,
                        'senderable_id' => 1,
                        'receiverable_type' => Owner::class,
                        'receiverable_id' => $payment->shop?->owner?->user_id,
                        'data' => [
                            'payment_id' => $paymentId,
                            'shop_id' => $payment->shop_id,
                        ]
                    ]);
                    event(new GetNotificationShop($owner, $notification));
                }
            }
        }
        $this->info('Done Send Notification or  Block Shops :)');
    }

}
