<?php

namespace App\Console\Commands;

use App\Models\Conversation;
use App\Models\Message;
use App\Models\Notification;
use Carbon\Carbon;
use Illuminate\Console\Command;

class DeleteNotification extends Command
{

    protected $signature = 'delete:notification';


    protected $description = 'remove notifications from table notifications after 5 day ago.';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Notification::query()
            ->whereDate('created_at', '<=', Carbon::now()->subDays(5)->format('Y-m-d'))
            ->delete();

        $conversations = Conversation::query()->with(['messages'])
            ->whereDate('last_time_message', '<=', Carbon::now()->subDays(5)->format('Y-m-d'));

        $ids = $conversations->pluck('id')->toArray();

        $conversations->delete();
        Message::query()->whereIn('conversion_id', $ids)->delete();

        $this->info('Done Delete Notification Successfully and conversations');
    }
}
