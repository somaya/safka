<?php

namespace App\Console\Commands;

use App\Enums\OrderStatus;
use App\Enums\Status;
use App\Enums\StatusPay;
use App\Enums\SubscriptionType;
use App\Models\Shop;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class ShopPayments extends Command
{

    protected $signature = 'shop:payments';


    protected $description = 'Get all shop status active  payments in this month';


    public function handle()
    {
        $shops = Shop::query()
            ->where([
                'status' => Status::Active->value,
            ]);
        $this->shopPercent($shops);

        $this->shopSubscription($shops);

    }

    protected function shopSubscription($shops)
    {
        // create invoice in end date
        $shops = $shops->where([
            'subscription_type' => SubscriptionType::participation->value,
           // 'subscribe_date' => now()->format('Y-m-d'),
        ])->get();
        try {
            DB::beginTransaction();
            foreach ($shops as $shop) {
                $end_date = Carbon::parse($shop->subscribe_date)->addDays(30);
                if ($end_date->format('Y-m-d') == now()->format('Y-m-d')){
                    $net_amount = $shop->subscription_value;
                    $shop->payments()->create([
                        'shop_id' => $shop->id,
                        'start_date' => $shop->subscribe_date,
                        'subscription_type' => $shop->subscription_type,
                        'end_date' => $end_date->format('Y-m-d'),
                        'net_amount' => $net_amount,
                    ]);
                    $shop->update(['subscribe_date' => $end_date->format('Y-m-d')]);
                }
            }
            $this->info('Finish from shop subscription done :)');
            DB::commit();
        } catch (\Exception $exception) {
            info('shop:payments', $exception->getMessage());
            $this->warn($exception->getMessage());
            DB::rollBack();
        }

    }

    protected function shopPercent($shops)
    {
        $shops = $shops->with(['orders'])->whereHas('orders', function (Builder $query) {
            $query->where([
                'is_created_invoice' => Status::Not_Active->value,
//                'status_pay' => StatusPay::Paid->value,
                'order_status' => OrderStatus::completed->value,
            ]);
        })->where([
            'subscription_type' => SubscriptionType::Percent->value,
        ])->get();
        foreach ($shops as $shop) {
            $orders = $shop->orders()->where(function (Builder $query) use ($shop) {
                $query->orWhere('created_at', '<=', now()->addDays($shop->invoice_duration)->format('Y-m-d'));
                    //->where('created_at', '<=', now()->format('Y-m-d'))
                   // ->orWhere('created_at', '>=', now()->addDays($shop->invoice_duration)->format('Y-m-d'));
            })->get();

            try {
                DB::beginTransaction();
                $amount = 0;
                foreach ($orders as $order) {
                    $amount += $order->amount;
                    $order->update(['is_created_invoice' => 1]);
                }
                $percent = calculatePercentage($amount, $shop->subscription_value, 'increase');
                if ($orders->count() > 0) {
                    $shop->payments()->create([
                        'shop_id' => $shop->id,
                        'start_date' => now()->format('Y-m-d'),
                        'subscription_type' => $shop->subscription_type,
                        'end_date' => now()->addDays($shop->invoice_duration)->format('Y-m-d'),
                        'net_amount' => $percent['amount'],
                    ]);
                }
                DB::commit();
            } catch (\Exception $exception) {
                DB::rollBack();
                info('shop:payments', $exception->getMessage());
                $this->warn($exception->getMessage());
            }

        }
        $this->info('Finish from shop percent done :)');
    }


}
