<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{

    protected function schedule(Schedule $schedule)
    {
        $schedule->command('delete:notification')->daily();
        $schedule->command('shop:payments')->daily();
        $schedule->command('block:shop')->daily();
        $schedule->command('backup:clean')->timezone('Africa/Cairo')->daily()->at('01:00');
        $schedule->command('backup:run')->timezone('Africa/Cairo')->daily()->at('02:00');
    }


    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
