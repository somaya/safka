<?php

namespace App\Enums;

enum CouponType: string
{
    case For_All = 'for_all';
    case Customized = 'customized';
}
