<?php

namespace App\Enums;

enum OrderStatus: string
{
    case pending = 'pending';
    case receive = 'receive';
//    case processing = 'processing';
//    case out_for_delivery = 'out_for_delivery';
//    case delivered = 'delivered';
    case returned = 'returned';
//    case failed = 'failed';
    case canceled = 'canceled';
    case completed = 'completed';
}
