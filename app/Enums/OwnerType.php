<?php

namespace App\Enums;

enum OwnerType: string
{
    case System = 'system';
    case Owner = 'owner';
}
