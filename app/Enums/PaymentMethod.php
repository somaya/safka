<?php

namespace App\Enums;

enum PaymentMethod: string
{
    case Cash = 'cash';
    case Visa = 'visa';
    case VodafoneCash = 'vodafone_cash';
    case MobileBalance = 'mobile_balance';
}
