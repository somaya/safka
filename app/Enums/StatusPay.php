<?php

namespace App\Enums;

enum StatusPay: string
{
    case Unpaid = 'unpaid';
    case Paid = 'paid';
}
