<?php

namespace App\Events;

use App\Http\Resources\Message\AdminConversationsResource;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class GetConversationAdmin implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $admin;
    public $conversations;

    public function __construct($admin, $conversations)
    {
        $this->admin = $admin;
        $this->conversations = $conversations;
    }


    public function broadcastOn(): Channel
    {
        return new Channel("listConversationsAdmin_{$this->admin->id}");
    }

    /* used in frontend side same name event*/
    public function broadcastAs(): string
    {
        return 'GetConversationAdmin';
    }

    /* used to attach data you want to add event */
    public function broadcastWith()
    {
        return [
            'conversations' => AdminConversationsResource::collection($this->conversations),
        ];
    }
}
