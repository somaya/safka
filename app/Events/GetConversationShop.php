<?php

namespace App\Events;

use App\Http\Resources\Message\ConversationsResource;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class GetConversationShop implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $conversations;

    public function __construct($user, $conversations)
    {
        $this->user = $user;
        $this->conversations = $conversations;
    }


    public function broadcastOn(): Channel
    {
        return new Channel("listConversations_{$this->user->id}");
    }

    /* used in frontend side same name event*/
    public function broadcastAs(): string
    {
        return 'GetConversationShop';
    }

    /* used to attach data you want to add event */
    public function broadcastWith()
    {
        return [
            'conversations' => ConversationsResource::collection($this->conversations),
        ];
    }
}
