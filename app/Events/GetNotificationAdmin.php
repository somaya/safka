<?php

namespace App\Events;

use App\Http\Resources\Notification\NotificationResource;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class GetNotificationAdmin implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $admin;
    public $notification;

    public function __construct($admin, $notification)
    {
        $this->admin = $admin;
        $this->notification = $notification;
    }


    public function broadcastOn(): Channel
    {
        return new Channel("admins_{$this->admin->id}");
    }

    /* used in frontend side same name event*/
    public function broadcastAs(): string
    {
        return 'GetNotificationAdmin';
    }

    /* used to attach data you want to add event */
    public function broadcastWith()
    {
        return [
            'notification' => NotificationResource::make($this->notification),
        ];
    }
}
