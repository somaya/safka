<?php

namespace App\Events;

use App\Http\Resources\Notification\NotificationResource;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class GetNotificationShop implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $notification;

    public function __construct($user, $notification)
    {
        $this->user = $user;
        $this->notification = $notification;
    }


    public function broadcastOn(): Channel
    {
        return new Channel("owners_{$this->user->id}");
    }

    /* used in frontend side same name event*/
    public function broadcastAs(): string
    {
        return 'GetNotificationShop';
    }

    /* used to attach data you want to add event */
    public function broadcastWith()
    {
        return [
            'notification' => NotificationResource::make($this->notification),
        ];
    }
}
