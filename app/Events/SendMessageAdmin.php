<?php

namespace App\Events;

use App\Http\Resources\Message\AdminMessageResource;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class SendMessageAdmin implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $admin;
    public $conversation;
    private $message;

    public function __construct($admin, $conversation, $message)
    {
        $this->admin = $admin;
        $this->conversation = $conversation;
        $this->message = $message;
    }


    public function broadcastOn(): Channel
    {
        return new Channel("conversionAdmin_{$this->conversation->id}");
    }

    /* used in frontend side same name event*/
    public function broadcastAs(): string
    {
        return 'SendMessageAdmin'; //SendMessageShop
    }

    /* used to attach data you want to add event */
    public function broadcastWith()
    {
        return [
            'message' => AdminMessageResource::make($this->message), //MessageResource
        ];
    }
}
