<?php

namespace App\Events;

use App\Http\Resources\Message\MessageResource;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class SendMessageShop implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $conversation;
    private $message;

    public function __construct($user, $conversation, $message)
    {
        $this->user = $user;
        $this->conversation = $conversation;
        $this->message = $message;
    }


    public function broadcastOn(): Channel
    {
        return new Channel("conversion_{$this->conversation->id}");
    }

    /* used in frontend side same name event*/
    public function broadcastAs(): string
    {
        return 'SendMessageShop';
    }

    /* used to attach data you want to add event */
    public function broadcastWith()
    {
        return [
            'message' => MessageResource::make($this->message),
        ];
    }
}
