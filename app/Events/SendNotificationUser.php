<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class SendNotificationUser
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $data;
    public $token;
    public $title;
    public $body;

    public function __construct($data, $token, $title = 'default', $body = 'default')
    {
        $this->data = $data;
        $this->token = $token;
        $this->title = $title;
        $this->body = $body;
    }

}
