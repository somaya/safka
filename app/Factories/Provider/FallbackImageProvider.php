<?php

declare(strict_types=1);

namespace App\Factories\Provider;

use Faker\Provider\Image as LoremPixelImage;
use Throwable;

final class FallbackImageProvider implements Image
{
    public static function image(
        $dir = null,
        $width = 640,
        $height = 480,
        $category = null,
        $fullPath = true,
        $randomize = true,
        $word = null
    ) {
        $providers = [LoremPixelImage::class, PicsumImage::class];
        while ($provider = array_shift($providers)) {
            try {
                $result = $provider::image($dir, $width, $height, $category, $fullPath, $randomize, $word);
            } catch (Throwable $exception) {
                $result = false;
            }
            if (false !== $result) {
                return $result;
            }
        }
        return false;
    }
}
