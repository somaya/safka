<?php

namespace App\Helpers\CPU;

use App\Models\DriverTransaction;
use Illuminate\Support\Arr;

class CreateTransaction
{

    public static function create($data): \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Builder
    {
        return DriverTransaction::query()->create([
            'driver_id' => $data['driver_id'],
            'order_id' => $data['order_id'],
            'in' => Arr::exists($data, 'in') ? $data['in'] : 0,
            'out' => Arr::exists($data, 'out') ? $data['out'] : 0,
            'balance' => $data['balance'],
            'transaction_type' => $data['transaction_type'],
            'owner_type' => $data['owner_type'],
            'notes' => Arr::exists($data, 'notes') ? $data['notes'] : null,
        ]);
    }
}
