<?php

namespace App\Helpers\CPU;

use App\Enums\Status;
use App\Models\BranchType;
use App\Models\Country;
use App\Models\File;
use App\Models\Governorate;
use App\Models\Owner;
use App\Models\Product;
use App\Models\Category;
use App\Models\Region;
use App\Models\Shop;
use App\Models\Size;
use App\Models\VehicleType;
use App\Models\WeekDay;
use App\Rules\OwnerAvailability;
use Illuminate\Support\Facades\Storage;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class Models
{

    public static function countries($status = Status::Active)
    {
        return Country::query()->status($status)->orderByDesc('created_at')->get();
    }

    public static function governorates($countryId, $status = Status::Active)
    {
        return Governorate::query()->whereCountryId($countryId)->status($status)->orderByDesc('created_at')->get();
    }

    public static function regions($governorateId, $status = Status::Active)
    {
        return Region::query()->whereGovernorateId($governorateId)->status($status)->orderByDesc('created_at')->get();
    }

    public static function categories($status = Status::Active)
    {
        return Category::query()->whereNull('parent_id')->orderBy('ranking')->status($status)->get();
    }

    public static function subCategories($parentId, $status = Status::Active)
    {
        return Category::query()->whereParentId($parentId)->status($status)->get();
    }

    public static function owners($status = null, $owner = null): \Illuminate\Database\Eloquent\Builder
    {
        $owners = Owner::query()->has('user')->with(['user'])->orderByDesc('created_at');
        if ($status) {
            $owners = $owners->whereRelation('user', 'status', '=', $status->value);
        }
        if ($owner) {
            $owners = $owners->where('created_by', '=', $owner);
        } else {
            $owners = $owners->whereNull('created_by');
        }
        return $owners;
    }

    public static function staff($status = null, $owner = null): \Illuminate\Database\Eloquent\Collection
    {
        $staff = Owner::query()->with(['user'])->orderByDesc('created_at');
        if ($status) {
            $staff = $staff->whereRelation('user', 'status', '=', $status->value);
        };
        if ($owner) {
            $staff = $staff->where('created_by', '=', $owner);
        }
        return $staff;
    }

    public static function shops($status = Status::Active, $ownerId = null)
    {
        $shop = Shop::query();
        if ($ownerId) {
            $shop = $shop->whereOwnerId($ownerId);
        }
        return $shop->status($status)->get();
    }

    public static function weeks($status = Status::Active)
    {
        return WeekDay::query()->status($status)->get();
    }

    public static function products($shopId, $status = Status::Active)
    {
        return Product::query()->where('shop_id', '=', $shopId)->status($status)->get();
    }

    public static function sizes($productId)
    {
        return Size::query()->whereProductId($productId)->get();
    }

    public static function ownerAndShop($ownerId, $status = Status::Active)
    {
        return [
            'owner' => self::owners($status)->get(),
            'shop' => Shop::query()->with(['country', 'governorate', 'region'])
                ->where([
                    'owner_id' => getOwner(),
                    'status' => $status->value
                ])->orderByDesc('created_at')->get()
        ];
    }
    public static function ownerShop()
    {
        return Shop::query()->with(['country', 'governorate', 'region'])
            ->whereIn('id', userType())
            ->where('owner_id', '=', getOwner())
            ->orderByDesc('created_at')->status()
            ->get();
    }

    public static function branchTypes($status = Status::Active)
    {
        return BranchType::query()->status($status)->get();
    }

    public static function qrCodeGenerator($generateName, $storePath): void
    {
        /*$userId = 1; //Crypt::encryptString(12);

        $qr = QrCode::format('png')
            ->eye('circle')
            ->gradient(0, 0, 0, 0, 0, 0, 'vertical')
            ->merge('/public/assets/images/no-image.png', .3)
            ->generate($userId);
        Storage::disk('public')->put('merge_images/' . $userId . '.png', $qr);*/

        //Crypt::encryptString(12);
        //dd($userId);
        //\Artisan::call('storage:link');
        //dd(Crypt::decryptString($userId));
        $qr = QrCode::format('png')
            //->size(100)
            //->mergeString($mergePath)
            ->size(284,284)
            //->color(255, 255, 255)
            //->style('dot')
            ->eye('square') // circle
            //->eyeColor(255,255,255,255,255,255,255)
            //->backgroundColor(255, 255, 255)
            //->backgroundColor(10,14,244)
            //     ->gradient(100, 150, 200, 250, 125, 130, 'horizontal')
            //->merge($mergePath) // '/public/assets/images/no-image.png'
            ->generate($generateName);

        Storage::disk('public')->put($storePath, $qr);
    }

    public static function tax($data): \Illuminate\Database\Eloquent\Collection|array
    {
        return Tax::query()->where([
            'shop_id' => $data['shop_id'],
            'status' => Status::Active->value,
        ])->get();
    }

    public static function deleteFile(array $data)
    {
        return File::query()->where([
            'id' => $data['id'],
            'relationable_type' => $data['relationable_type'],
            'relationable_id' => $data['relationable_id'],
        ])->first();
    }

    public static function ruleImages(int $files): array
    {
        $rules = [];
        if ($files == 0) {
            $rules += [
                'images' => ['required', 'array', 'min:1', 'max:' . 5 - $files],
                'images.*' => ['required', validationImage()],
            ];

        } else {
            $rules += [
                'images' => ['nullable', 'array', 'min:1', 'max:' . 5 - $files],
                'images.*' => ['nullable', validationImage()],
            ];
        }
        return $rules;
    }

    public static function ruleOwner($owner = null): array
    {
        $rules = [];
        if (request('guard') == 'admin') {
            $rules = [
                'owner_id' => ['required', 'integer', new OwnerAvailability()],
            ];
        } else {
            request()->merge(['owner_id' => getOwner()]);
        }
        return $rules;
    }

    public static function staffByOwner($ownerId, $staffId)
    {
        return Owner::query()->with(['user'])->whereCreatedBy($ownerId)->find($staffId);
    }
    public static function vehicleTypes($status = Status::Active)
    {
        return VehicleType::query()->orderByDesc('created_at')->status($status->value)->get();
    }

}
