<?php

namespace App\Helpers;

use Spatie\Permission\Models\Permission;

class PermissionAdmin
{
    private $adminRole;
    private $admin;

    public function __construct($adminRole, $admin)
    {
        $this->adminRole = $adminRole;
        $this->admin = $admin;
    }

    public static function models(): array
    {
        return [
            'Language',
            'Country',
            'Governorate',
            'Region',
            'Category',
            'VehicleType',
            'Role',
            'Employee',
            'Setting',
            'Owner',
            'Shop',
            'Role#Staff',
            'Staff',
            'Order',
            'Driver',
            'DriverOrder',
            'Coupon',
            'Product',
            'Discount',
            'Delivery',
            'Offer',
            'Save',
            'Banner',
            'Branch#Type',
            'Category',
            'Blog',
            'Blog Category',
            'SocialMedia',
            'Page#Setup',
            'Currency',
            'Notification',
            'JoinUs',
            'ContactUs',
            'Customer',
            'Chat',
            'Chart',
            'Rate',
            'Day',
            'Payment',
            'Live#Chat',
//            'Our#Partner',
            'Service',
            'Company'
        ];
    }

    public static function lists(): array
    {
        return [
            'list', 'add', 'edit', 'delete',
        ];
    }

    public function createPermissions()
    {
        foreach ($this->models() as $row) {
            foreach ($this->lists() as $value) {
                Permission::query()->updateOrCreate([
                    'name' => $row . " " . $value,
                    'guard_name' => 'admin'
                ], [
                    'name' => $row . " " . $value,
                    'guard_name' => 'admin'
                ]);
            }
        }
    }

    public function store()
    {
        $this->createPermissions();

        $adminPermissions = $this->permissions($this->models(), $this->lists());

        $this->adminRole->givePermissionTo($adminPermissions);


        $this->admin->assignRole($this->adminRole);
    }

    private function permissions($modules, $lists): array
    {
        $data = [];

        foreach ($modules as $value) {
            foreach ($lists as $item) {
                $data[] = ['name' => $value . ' ' . $item];
            }
        }
        return $data;
    }
}
