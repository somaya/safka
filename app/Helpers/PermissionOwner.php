<?php

namespace App\Helpers;

use Spatie\Permission\Models\Permission;

class PermissionOwner
{
    private $ownerRole;

    public function __construct($ownerRole)
    {
        $this->ownerRole = $ownerRole;
    }

    public static function models(): array
    {
        return [
            'Role#Staff',
            'Staff',
            'Order',
            'Chart',
//            'DriverOrder',
//            'Driver',
//            'Delivery',
            'Notification',
            'Chat',
            'Payment',
            'Live#Chat',
            'Product',
            'Offer',
            'Save',
            'Discount',
        ];
    }

    public static function lists(): array
    {
        return [
            'list', 'add', 'edit', 'delete'
        ];
    }

    public function createPermissions(): void
    {
        foreach ($this->models() as $row) {
            foreach ($this->lists() as $value) {
                Permission::query()->updateOrCreate([
                    'name' => $row . " " . $value,
                    'guard_name' => 'web',
                ], [
                    'name' => $row . " " . $value,
                    'guard_name' => 'web',
                ]);
            }
        }
    }

    public function store()
    {
        $this->createPermissions();

        $ownerPermissions = $this->permissions($this->models(), $this->lists());

        $this->ownerRole->givePermissionTo($ownerPermissions);

        //$this->owner->assignRole($this->ownerRole);
    }

    private function permissions($modules, $lists): array
    {
        $data = [];

        foreach ($modules as $value) {
            foreach ($lists as $item) {
                $data[] = ['name' => $value . ' ' . $item];
            }
        }
        return $data;
    }
}
