<?php

use App\Enums\Status;
use App\Helpers\CPU\CreateFileLanguages;
use App\Models\Admin;
use App\Models\CartDetail;
use App\Models\Customer;
use App\Models\Governorate;
use App\Models\Language;
use App\Models\Owner;
use App\Models\Product;
use App\Models\Region;
use App\Models\Shop;
use App\Models\ShopStaff;
use App\Models\User;
use Carbon\Carbon;
use Intervention\Image\Facades\Image;

# validateImage
if (!function_exists('validationImage')) {
    function validationImage($extension = null, $type = null): array
    {
        if ($extension == null) {
            return ['image', 'mimes:jpg,jpeg,png,bmp',];
        } else {
            return [$type, 'mimes:' . $extension];
        }
    }
}
if (!function_exists('imageDimenssion')) {
    function imageDimenssion()
    {
            return 'dimensions:max_width='.Utility::getValByName('image_max_width').',max_height='.Utility::getValByName('image_max_height').'';
    }
}

# format date
if (!function_exists('formatDate')) {
    function formatDate($format, $date): string
    {
        return date($format, strtotime(Carbon::parse($date)));
    }
}
if (!function_exists('userType')) {
    function userType()
    {
        $user = auth()->user();
        if ($user->userable_type ==Owner::class) {
            $ownerId = $user->userable->shops->pluck('id')->toArray();
        }
        elseif ($user->userable_type == ShopStaff::class ) {
            $ownerId =$user->userable->owner->shops->pluck('id')->toArray();
        }
        else {
            $ownerId =Shop::query()->pluck('id')->toArray();
        }
        return $ownerId;
    }
}
if (!function_exists('getNotifications')) {
    function getNotifications($model_type, $model_id, $type = []): \Illuminate\Database\Eloquent\Collection|array
    {
        $notifications = \App\Models\Notification::query()
            ->with(['customer.user'])
            ->where('receiverable_type', '=', $model_type)
            ->where('receiverable_id', '=', $model_id);
        if (!empty($type)) {
            $notifications = $notifications->whereIn('type', $type);
        }
        return $notifications->whereNull('read_at')->orderByDesc('created_at')->get();
    }
}
if (!function_exists('getNotificationOwners')) {
    function getNotificationOwners($type = []): \Illuminate\Database\Eloquent\Collection|array
    {
        $notifications = \App\Models\Notification::query()->with(['customer.orders']);
        if (auth()->user()->userable_type == ShopStaff::class) {
            $notifications = $notifications
                ->where(function (\Illuminate\Database\Eloquent\Builder $builder) {
                    $builder
                        ->orWhere('type', '=', 'live-chat')
                        ->orWhere('receiverable_type', '=', Shop::class)
                        ->orWhereIn('receiverable_id', userType())
                        ->orWhereHas('customer.orders', function ($query) {
                            $query->whereIn('shop_id', userType());
                        });
                });
        } else {
            $notifications = $notifications
                ->where('receiverable_id', '=', auth()->user()->id)
                ->where(function ($q) {
                $q->whereIn('receiverable_type', [User::class,Shop::class] )
                    ->orWhereIn('senderable_type',[Admin::class,Customer::class]);
            });
        }

        if (!empty($type)) {
            $notifications = $notifications->whereIn('type', $type);
        }
        return $notifications->whereNull('read_at')->orderByDesc('created_at')->get();
    }
}
if (!function_exists('calculatePercentage')) {
    function calculatePercentage($amount, $percentage, $type): array
    {
        $price = ($amount / 100) * $percentage;
        if ($type == 'increase') {
            $total = $amount + $price;
        } else {
            $total = $amount - $price;
        }
        return [
            'amount' => $price,
            'total_amount' => round($total, 2)
        ];

    }
}

if (!function_exists('languages')) {
    function languages()
    {
        return Language::query()->status()->get();
    }
}
if (!function_exists('getReducedAvatar')) {
    function getReducedAvatar($base_file, $file): string
    {
        $img_path = public_path('storage/' . $base_file);
        $dest_img_path = public_path('storage/reduced/' . $file);

        if (file_exists($dest_img_path))
            return asset('storage/reduced/' . $file);

        $img = Image::make($img_path)
            ->resize(300, 300)
            ->save($dest_img_path);
        return !empty($img) ? asset('storage/reduced/' . $img->basename) : asset('assets/images/no-image.png');
    }
}

if (!function_exists('locale')) {
    function locale()
    {
        if (request()->expectsJson()) {
            return request('lang') ?? config('app.locale');
        }
        return config('app.locale');
    }
}

if (!function_exists('locales')) {
    function locales()
    {
        return languages()->pluck('code')->toArray();
    }
}

if (!function_exists('default_lang')) {
    function default_lang()
    {
        if (session()->has('local')) {
            $lang = session('local');
        } else {
            $data = languages();
            $code = 'en';
            $direction = 'ltr';
            foreach ($data as $value) {
                if (array_key_exists('default', $value->toArray()) && $value->default == Status::Active->value) {
                    $code = $value->code;
                    if (array_key_exists('direction', $value->toArray())) {
                        $direction = $value->direction;
                    }
                }
            }
            session()->forget('local');
            session()->forget('direction');
            session()->put('local', $code);
            session()->put('direction', $direction);
            $lang = $code;
        }
        return $lang;

    }
}

if (!function_exists('_trans')) {
    function _trans($key,$locale=null): array|string|\Illuminate\Contracts\Translation\Translator|\Illuminate\Contracts\Foundation\Application|null
    {
        if ($locale){
            $local=$locale;
        }else{
            $local = default_lang();
        }
        App::setLocale($local);

        if (!file_exists(base_path('lang/' . $local))) {
            CreateFileLanguages::file($local);
        }
        $lang_array = include(base_path('lang/' . $local . '/message.php'));
        $processed_key = ucfirst(str_replace('_', ' ', remove_invalid_characters($key)));
        if (!array_key_exists($key, $lang_array)) {
            $lang_array[$key] = $processed_key;
            $str = "<?php return " . var_export($lang_array, true) . ";";
            file_put_contents(base_path('lang/' . $local . '/message.php'), $str);
            $result = $processed_key;
        } else {
            $result = __('message.' . $key);
        }
        return $result;
    }
}
if (!function_exists('_transs')) {
    function _transs($key,$local): array|string|\Illuminate\Contracts\Translation\Translator|\Illuminate\Contracts\Foundation\Application|null
    {
        if (!file_exists(base_path('lang/' . $local))) {
            CreateFileLanguages::file($local);
        }
        $lang_array = include(base_path('lang/' . $local . '/message.php'));
        $processed_key = ucfirst(str_replace('_', ' ', remove_invalid_characters($key)));
        if (!array_key_exists($key, $lang_array)) {
            $lang_array[$key] = $processed_key;
            $str = "<?php return " . var_export($lang_array, true) . ";";
            file_put_contents(base_path('lang/' . $local . '/message.php'), $str);
            $result = $processed_key;
        } else {
            $result = __('message.' . $key);
        }
//        dd($result);
        return $result;

    }
}

if (!function_exists('remove_invalid_characters')) {
    function remove_invalid_characters($str): array|string
    {
        return str_ireplace(['\'', '"', ',', ';', '<', '>', '?'], ' ', $str);
    }
}

if (!function_exists('menuRoute')) {
    function menuRoute($route, $type = 'route')
    {
        if ($type == 'route') {
            return Route::currentRouteName() == $route ? 'active-page' : '';
        } elseif ($type == 'lists') {
            foreach ($route as $value) {
                return Request::is($value) ? 'active-page' : '';
            }
        } else {
            return Request::is($route) ? 'active-page' : '';
        }
    }
}

if (!function_exists('getAvatar')) {
    function getAvatar($path): string
    {
        return !empty($path) ? asset('storage/' . $path) : asset('assets/images/img/400x400/img2.jpg');
    }
}

if (!function_exists('getCodeTable')) {

    function getCodeTable($str, $tableName, $edit = false, $id = null): string

    {
        if ($edit) {
            $TableId = $id;
        } else {
            $table_info = DB::select("show table status like '{$tableName}'");
            $TableId = $table_info[0]->Auto_increment;
        }

        return "{$str}#" . str_pad($TableId, 1, "0", STR_PAD_LEFT);
    }
}

if (!function_exists('getWeeks')) {
    function getWeeks($weeks)
    {
        $date = Carbon::now()->format('l');
        $time = now(Utility::getValByName('timezone'));
        $days = [
            'Sunday' => 1,
            'Monday' => 2,
            'Tuesday' => 3,
            'Wednesday' => 4,
            'Thursday' => 5,
            'Friday' => 6,
            'Saturday' => 7,
        ];
        $day = Arr::first($weeks, function ($value, $key) use ($date, $days) {
            $number = $days[$date];
            return $value['day_id'] == $number;
        });
        if ($day) {
            if ($time->greaterThanOrEqualTo(Carbon::parse($day['from'])) && $time->lessThanOrEqualTo(Carbon::parse($day['to']))) {
                return true;
            }
            return false;
        }
        return false;
    }
}

if (!function_exists('tableCode')) {
    function tableCode($table): string
    {
        $table_info = DB::select("show table status like '{$table}'");
        return 'Order#' . str_pad($table_info[0]->Auto_increment, 8, "0", STR_PAD_LEFT);
    }
}

if (!function_exists('getProducts')) {
    function getProducts($shopId)
    {
        return Product::query()->whereShopId($shopId)->status()->get();
    }
}
//if (!function_exists('getOwner')) {
//    function getOwner($userId)
//    {
//        $owner = Owner::query()->without(['user'])
//            ->whereRelation('user','userable_id','=',$userId)
//            ->first();
//        if ($owner) {
//            return $owner->id;
//        }
//        return 0;
//    }
//}
if (!function_exists('getOwner')) {
    function getOwner()
    {
        if (auth()->user()->userable_type ==Owner::class  )
        {
            $owner = Owner::query()->without(['user'])
                ->whereRelation('user','id','=',auth()->user()->id)
                ->first();
            if ($owner) {
                return $owner->id;
            }
            return 0;

        }
        if(auth()->user()->userable_type ==ShopStaff::class){
           $staff= ShopStaff::query()->without(['user'])
                ->whereRelation('user','id','=',auth()->user()->id)
                ->first();
            if ($staff) {
                return $staff->owner->id;
            }
            return 0;



        }

    }
}
if (!function_exists('getGovernorates')) {
    function getGovernorates($country_id)
    {
        return Governorate::query()->whereCountryId($country_id)->orderByDesc('created_at')->status()->get();
    }
}
if (!function_exists('getRegions')) {
    function getRegions($governorate_id)
    {
        return Region::query()->without(['translations'])->whereGovernorateId($governorate_id)->orderByDesc('created_at')->status()->get();
    }
}

if (!function_exists('getFiles')) {
    function getFiles(array $data): \Illuminate\Database\Eloquent\Collection|array
    {
        return \App\Models\File::query()->where([
            'relationable_id' => $data['id'],
            'relationable_type' => $data['type'],
        ])->get();
    }
}
if (!function_exists('getQtyInCartDetails')) {
    function getQtyInCartDetails($model_type, $model_id): string
    {
        return CartDetail::query()->where([
            ['modelable_type', '=', $model_type],
            ['modelable_id', '=', $model_id],
        ])->sum('qty');
    }
}
if (!function_exists('getCartQty')) {
    function getCartQty($model_type, $model_id,?User $user): int
    {
        if (is_null($user)) {
            return false;
        }
        $cart=\App\Models\Cart::query()->where('customer_id',$user->userable->id)->first();
        if ($cart){
           return CartDetail::query()->where([
                ['modelable_type', '=', $model_type],
                ['modelable_id', '=', $model_id],
                ['cart_id', '=', $cart->id],
            ])->sum('qty');
        }else {
            return 0;
        }
    }
}
if (!function_exists('formatLocations')) {
    function formatLocations($order): array
    {
        $locations = [
            'name' => $order->location_info['first_name'] . ' ' . $order->location_info['last_name'],
            'phone' => $order->location_info['phone'],
        ];
        if (Arr::exists($order->location_info, 'country_id')) {
            $locations += [
                'country' => \App\Models\Country::query()->find($order->location_info['country_id'])->translate(locale())?->name,
            ];
        }
        if (Arr::exists($order->location_info, 'governorate_id')) {
            $locations += [
                'governorate' => \App\Models\Governorate::query()->find($order->location_info['governorate_id'])->translate(locale())?->name,
            ];
        }
        if (Arr::exists($order->location_info, 'region_id')) {
            $locations += [
                'region' => \App\Models\Region::query()->find($order->location_info['region_id'])->translate(locale())?->name,
            ];
        }
        $locations += [
            'building_type' => $order->location_info['building_type'],
            'street' => $order->location_info['street'],
            'address' => $order->location_info['address'],
            'building_number' => $order->location_info['building_number'],
            'floor_no' => $order->location_info['floor_no'],
            'apartment_number' => $order->location_info['apartment_number'],
            'note' => $order->location_info['notes'],
        ];
        return $locations;
    }
    if (!function_exists('totalTimeTrip')) {
        function totalTimeTrip($start, $end): string
        {
            $to = Carbon::parse($start);
            $from = Carbon::parse($end);

            $hours = $to->diffInHours($from);
            $minutes = $to->diffInMinutes($from);
            if ($hours > 0) {
                return $hours . ':- ' . _trans('Hours') . ',' . $minutes . ':- ' . _trans('Minutes');
            } else {
                return $minutes . ' :- ' . _trans('Minutes');

            }
        }
    }


}



