<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Setting\Utility;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\BannerRequest;
use App\Interfaces\UploadFile\UploadFileRepositoryInterface;
use App\Models\Banner;
use App\Traits\ApiResponses;
use App\Traits\UploadFileTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BannerController extends Controller
{
    use UploadFileTrait, ApiResponses;

    public function __construct()
    {
        $this->middleware(['permission:Banner list,admin'])->only(['index']);
        $this->middleware(['permission:Banner add,admin'])->only(['create']);
        $this->middleware(['permission:Banner edit,admin'])->only(['edit']);
        $this->middleware(['permission:Banner delete,admin'])->only(['destroy']);
    }

    public function index(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $data = Banner::allBanners();
            $columns = [
                'all' => _trans('All'),
                'code' => _trans('Banner Code'),
//                'resource_type' => _trans('Resource Type'),
//                'banner_type' => _trans('Banner Type'),
            ];
            return view('admin.banner.index', compact('data','columns'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function create(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $edit = false;
            return view('admin.banner.form', compact('edit'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }


    public function store(BannerRequest $request): string|\Illuminate\Http\RedirectResponse
    {
        try {
            DB::beginTransaction();
            $data = $request->validated();
            if ($request->hasFile('image')) {
                $data['image'] = $this->upload([
                    'file' => 'image',
                    'path' => 'banner',
                    'upload_type' => 'single',
                    'delete_file' => ''
                ]);
            }
            $banner = Banner::query()->create($data);
            if ($banner) {
                DB::commit();
                return redirect()->route('admin.banner.index')->with('success', _trans('Done Save Data Successfully'));
            }
            return redirect()->back()->with('warning', _trans('Some failed errors'))->withInput($data);
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage())->withInput($request->all());
        }
    }

    public function edit(Banner $banner): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $edit = true;
            return view('admin.banner.form', compact('edit', 'banner'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function update(BannerRequest $request, Banner $banner): \Illuminate\Http\RedirectResponse
    {
        try {
            DB::beginTransaction();
            $data = $request->validated();
            if ($request->hasFile('image')) {
                $data['image'] = $this->upload([
                    'file' => 'image',
                    'path' => 'banner',
                    'upload_type' => 'single',
                    'delete_file' => $banner->image ?? ''
                ]);
            }
            if ($banner->update($data)) {
                DB::commit();
                return redirect()->route('admin.banner.index')->with('success', _trans('Done Updated Data Successfully'));
            }
            return redirect()->back()->with('warning', _trans('Some failed errors'))->withInput($data);
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage())
                ->withInput($request->all());
        }
    }



    public function updateStatus(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            DB::beginTransaction();
            $banner= Banner::query()->findOrFail($request->id);
            if (!$banner) {
                return $this->success( _trans('Not Found'), false);
            }
            $status = !$banner->status;
            if ($banner->update(['status' => $status])) {
                DB::commit();
                return $this->success(_trans('Done Updated Data Successfully'), '');
            }
            return $this->success( _trans('Some failed errors'), false);
        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->success( $exception->getMessage(), false);
        }
    }
}
