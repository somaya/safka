<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\BlogCategoryRequest;
use App\Interfaces\UploadFile\UploadFileRepositoryInterface;
use App\Models\BlogCategory;
use App\Traits\ApiResponses;
use App\Traits\UploadFileTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BlogCategoryController extends Controller
{
    use UploadFileTrait, ApiResponses;

    public function __construct()
    {
        $this->middleware(['permission:Blog Category list,admin'])->only(['index']);
        $this->middleware(['permission:Blog Category add,admin'])->only(['create']);
        $this->middleware(['permission:Blog Category edit,admin'])->only(['edit']);
        $this->middleware(['permission:Blog Category delete,admin'])->only(['destroy']);
    }

    public function index(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $data = BlogCategory::allCategories();
            $columns = [
                'all' => _trans('All'),
                'code' => _trans('Category Code'),
                'name' => _trans('Category name'),
            ];
            return view('admin.blog_categories.index', compact('data','columns'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function create(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $edit = false;
            return view('admin.blog_categories.form', compact('edit'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }


    public function store(BlogCategoryRequest $request): string|\Illuminate\Http\RedirectResponse
    {
        try {
            DB::beginTransaction();
            $data = $request->validated();

            if ($request->hasFile('image')) {
                $data['image'] = $this->upload([
                    'file' => 'image',
                    'path' => 'blog_category',
                    'upload_type' => 'single',
                    'delete_file' => ''
                ]);
            }
            $category = BlogCategory::query()->create($data);
            if ($category) {
                DB::commit();
                return redirect()->route('admin.blog-category.index')->with('success', _trans('Done Save Data Successfully'));
            }
            return redirect()->back()->with('warning', _trans('Some failed errors'))->withInput($data);
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage())->withInput($request->all());
        }
    }

    public function edit(BlogCategory $blog_category): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $edit = true;
            return view('admin.blog_categories.form', compact('edit', 'blog_category'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function update(BlogCategoryRequest $request, BlogCategory $blog_category): \Illuminate\Http\RedirectResponse
    {
        try {
            DB::beginTransaction();
            $data = $request->validated();
            if ($request->hasFile('image')) {
                $data['image'] = $this->upload([
                    'file' => 'image',
                    'path' => 'blog_category',
                    'upload_type' => 'single',
                    'delete_file' => $blog_category->image ?? ''
                ]);
            }
            if ($blog_category->update($data)) {
                DB::commit();
                return redirect()->route('admin.blog-category.index')->with('success', _trans('Done Updated Data Successfully'));
            }
            return redirect()->back()->with('warning', _trans('Some failed errors'))->withInput($data);
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage())
                ->withInput($request->all());
        }
    }

    public function updateStatus(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            DB::beginTransaction();
            $category = BlogCategory::query()->findOrFail($request->id);
            if (!$category) {
                return $this->apiResponse('', _trans('Not Found'), false);
            }
            $status = !$category->status;
            if ($category->update(['status' => $status])) {
                DB::commit();
                return $this->apiResponse(_trans('Done Updated Data Successfully'), '');
            }
            return $this->apiResponse('', _trans('Some failed errors'), false);
        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->apiResponse('', $exception->getMessage(), false);
        }
    }
}
