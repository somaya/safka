<?php

namespace App\Http\Controllers\Admin;

use App\Enums\Status;
use App\Helpers\Setting\Utility;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\BlogRequest;
use App\Interfaces\UploadFile\UploadFileRepositoryInterface;
use App\Models\Blog;
use App\Models\BlogCategory;
use App\Models\Size;
use App\Traits\ApiResponses;
use App\Traits\UploadFileTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BlogController extends Controller
{
    use UploadFileTrait, ApiResponses;

    public function __construct()
    {
        $this->middleware(['permission:Blog list,admin'])->only(['index']);
        $this->middleware(['permission:Blog add,admin'])->only(['create']);
        $this->middleware(['permission:Blog edit,admin'])->only(['edit']);
        $this->middleware(['permission:Blog delete,admin'])->only(['destroy']);
    }

    public function index(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $data = Blog::allBlogs();
            $columns = [
                'all' => _trans('All'),
                'code' => _trans('Blog Code'),
                'category' => _trans('Category name'),
                'title' => _trans('Blog title'),

            ];
            return view('admin.blog.index', compact('data','columns'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function create(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $edit = false;
            $categories=BlogCategory::query()->where('status', '=', Status::Active->value)->get();
            return view('admin.blog.form', compact('edit','categories'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function store(BlogRequest $request)
    {

            $data = $request->validated();

            if ($request->hasFile('default_image')) {
                $data['default_image'] = $this->upload([
                    'file' => 'default_image',
                    'path' => 'blog',
                    'upload_type' => 'single',
                    'delete_file' => ''
                ]);
            }
        try {
            DB::beginTransaction();

            $blog = Blog::query()->create($data);
            if ($request->has('images')) {
                $this->upload([
                    'file' => 'images',
                    'path' => 'blog',
                    'upload_type' => 'files',
                    'multi_upload' => true,
                    'relationable_id' => $blog->id,
                    'relationable_type' => Blog::class,
                ]);
            }
            if ($blog) {

                if ($request->tags) {
                    foreach ($request->tags as $tag) {
                        if ($tag['name:ar'] && $tag['name:en']) {
                            $blog->tags()->create($tag);
                        }
                    }
                }
                DB::commit();
                return redirect()->route('admin.blogs.index')->with('success', _trans('Done Save Data Successfully'));
            }
            return redirect()->back()->with('warning', _trans('Some failed errors'))->withInput($data);
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage())->withInput($request->all());
        }
    }


    public function edit(Blog $blog): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $edit = true;
            $categories=BlogCategory::query()->where('status', '=', Status::Active->value)->get();
            return view('admin.blog.form', compact('edit', 'categories','blog'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function update(BlogRequest $request, Blog $blog): \Illuminate\Http\RedirectResponse
    {
            $data = $request->validated();
            if ($request->hasFile('default_image')) {
                $data['icon'] = $this->upload([
                    'file' => 'default_image',
                    'path' => 'product',
                    'upload_type' => 'single',
                    'delete_file' => $blog->icon ?? ''
                ]);
            }

        try {
            DB::beginTransaction();
            if ($blog->update($data)) {
                $blog->tags()->delete();
                if ($request->tags) {
                    foreach ($request->tags as $tag) {
                        if ($tag['name:ar'] && $tag['name:en']) {
                            $blog->tags()->create($tag);
                        }
                    }
                }

                DB::commit();
                return redirect()->route('admin.blogs.index')->with('success', _trans('Done Updated Data Successfully'));
            }
            return redirect()->back()->with('warning', _trans('Some failed errors'))->withInput($data);
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage())
                ->withInput($request->all());
        }
    }

    public function updateStatus(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            DB::beginTransaction();
            $blog = Blog::query()->findOrFail($request->id);
            if (!$blog) {
                return $this->apiResponse('', _trans('Not Found'), false);
            }
            $status = !$blog->status;
            if ($blog->update(['status' => $status])) {
                DB::commit();
                return $this->apiResponse(_trans('Done Updated Data Successfully'), '');
            }
            return $this->apiResponse('', _trans('Some failed errors'), false);
        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->apiResponse('', $exception->getMessage(), false);
        }
    }

}
