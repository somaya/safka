<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\BranchTypeRequest;
use App\Models\BranchType;
use App\Traits\ApiResponses;
use App\Traits\UploadFileTrait;
use Illuminate\Http\Request;

class BranchTypeController extends Controller
{
    use UploadFileTrait, ApiResponses;

    public function __construct()
    {
        $this->middleware(['permission:Branch#Type list,admin'])->only(['index']);
        $this->middleware(['permission:Branch#Type add,admin'])->only(['create']);
        $this->middleware(['permission:Branch#Type edit,admin'])->only(['edit']);
        $this->middleware(['permission:Branch#Type delete,admin'])->only(['destroy']);
    }

    public function index(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $data = BranchType::allBranches();
            $columns = [
                'all' => _trans('All'),
                'code' => _trans('Branch type code'),
                'name' => _trans('Branch type name'),
            ];
            return view('admin.branch-type.index', compact('data', 'columns'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function create(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        $edit = false;
        return view('admin.branch-type.form', compact('edit'));
    }

    public function store(BranchTypeRequest $request): string|\Illuminate\Http\RedirectResponse
    {
        $data = $request->validated();
        if ($request->hasFile('image')) {
            $data['image'] = $this->upload([
                'file' => 'image',
                'path' => 'branch_type',
                'upload_type' => 'single',
                'delete_file' => ''
            ]);
        }
        BranchType::query()->create($data);
        return redirect()->route('admin.branch-type.index')->with('success', _trans('Done Save Data Successfully'));

    }

    public function show(BranchType $branchType)
    {
        //
    }

    public function edit($id): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        $edit = true;
        $branchType = BranchType::query()->findOrFail($id);
        return view('admin.branch-type.form', compact('edit', 'branchType'));
    }

    public function update(BranchTypeRequest $request, $id): \Illuminate\Http\RedirectResponse
    {
        $branchType = BranchType::query()->findOrFail($id);
        $data = $request->validated();
        if ($request->hasFile('image')) {
            $data['image'] = $this->upload([
                'file' => 'image',
                'path' => 'branch_type',
                'upload_type' => 'single',
                'delete_file' => $branchType->image ?? ''
            ]);
        }
        $branchType->update($data);
        return redirect()->route('admin.branch-type.index')->with('success', _trans('Done Updated Data Successfully'));
    }

    public function destroy($id)
    {
        //
    }

    public function updateStatus(Request $request): \Illuminate\Http\JsonResponse
    {
        $branchType = BranchType::query()->findOrFail($request->id);
        if (!$branchType) {
            return $this->success(_trans('Not Found'), status: false);
        }
        $status = !$branchType->status;
        $branchType->update(['status' => $status]);
        return $this->success(_trans('Done Updated Data Successfully'));
    }

    public function updatePublished(Request $request): \Illuminate\Http\JsonResponse
    {
        $branchType = BranchType::query()->findOrFail($request->id);
        if (!$branchType) {
            return $this->success(_trans('Not Found'), status: false);
        }
        $published = !$branchType->published;
        $branchType->update(['published' => $published]);
        return $this->success(_trans('Done Updated Data Successfully'));
    }
}
