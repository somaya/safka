<?php

namespace App\Http\Controllers\Admin;

use App\Events\SendNotificationUser;
use App\Http\Controllers\Controller;
use App\Http\Resources\Message\ConversationsResource;
use App\Http\Resources\Message\MessageResource;
use App\Models\AdminConversation;
use App\Models\AdminMessage;
use App\Traits\ApiResponses;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ChatController extends Controller
{
    use ApiResponses;

    public function __construct()
    {
        $this->middleware(['permission:Chat list,admin'])->only(['index']);
    }

    public function index()
    {
        $conversations = AdminConversation::query()->with(['lastMessage', 'customer'])
            ->withCount(['messages as message_not_read' => function ($query) {
                $query->where('read', '=', 0);
            }])->with(['customer'])->orderByDesc('last_time_message')->get();
        $data = json_encode($conversations->toArray(), true);
        return view('admin.chat.index', compact('conversations', 'data'));
    }

    public function messages(Request $request)
    {
        $conversation = AdminConversation::query()->with(['customer'])->find($request->conversation_id);
        if ($conversation) {
            $customer = $conversation->customer;
            $messages = AdminMessage::query()->whereAdminConversionId($conversation->id);
            AdminMessage::query()->where([
                'admin_conversion_id' => $conversation->id,
                'read' => 0,
            ])->update(['read' => 1]);
            $messages = $messages->orderBy('created_at')->get();
            return $this->success([
                'view' => view('admin.chat.messages', compact('messages', 'customer', 'conversation'))->render(),
            ]);
        }
        return $this->success( _trans('Not found this conversation'), status: false);
    }

    public function sendMessage(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'conversation_id' => ['required', 'integer', Rule::exists('admin_conversations', 'id')],
                'message' => ['required', 'string', 'min:2', 'max:255'],
            ]);

            if ($validator->fails()) {
                return $this->success( $validator->errors()->first(), status: false);
            }
            $conversation = AdminConversation::query()->with(['customer.fcmTokens'])->find($request->conversation_id);
            if (!$conversation) {
                return $this->success( _trans('not found this conversation'), status: false);
            }
            $message = $conversation->messages()->create([
                'sender_id' => $conversation->receiver_id,
                'receiver_id' => $conversation->sender_id,
                'user_id' => auth()->user()->id,
                'read' => false,
                'body' => $request->message,
                'type' => 'text',
            ]);
            AdminMessage::query()->where([
                'admin_conversion_id' => $conversation->id,
                'read' => 0,
            ])->update(['read' => 1]);
            $token = $conversation->customer->fcmTokens()->pluck('fcm_token')->toArray();
            $conversation->update(['last_time_message' => now()]);
            event(new SendNotificationUser([
                'notification_type' => 'ticket',
                'message' => MessageResource::make($message),
            ], $token, $message->body, $message->body));
            return $this->success([
                'view' => view('admin.chat.message', compact('message'))->render()
            ]);
            //return $this->apiResponse(MessageResource::make($message));
        } catch (\Exception $exception) {
            return $this->success( $exception->getMessage());
        }
    }

    public function search(Request $request)
    {
        $conversations = AdminConversation::query()
            ->withCount(['messages as message_not_read' => function ($query) {
                $query->where('read', '=', 0);
            }])
            ->orWhereRelation('customer.user', 'name', 'LIKE', '%' . $request->search . '%')
            ->with(['customer', 'lastMessage'])->orderByDesc('last_time_message')->get();

        return $this->success(ConversationsResource::collection($conversations));
    }
}
