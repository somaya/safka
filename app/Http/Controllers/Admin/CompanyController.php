<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CompanyRequest;
use App\Models\Company;
use App\Traits\ApiResponses;
use App\Traits\UploadFileTrait;
use Illuminate\Http\Request;
use Utility;

class CompanyController extends Controller
{
    use UploadFileTrait, ApiResponses;

    public function __construct()
    {
        $this->middleware(['permission:Company list,admin'])->only(['index']);
        $this->middleware(['permission:Company add,admin'])->only(['create', 'store']);
        $this->middleware(['permission:Company edit,admin'])->only(['edit', 'update', 'updateStatus']);
        $this->middleware(['permission:Company delete,admin'])->only(['destroy']);
    }

    public function index(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $companies = Company::query()
                ->paginate(Utility::getValByName('pagination_limit'));

            return view('admin.company.index', compact('companies'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function create(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        $edit = false;
        return view('admin.company.form', compact('edit'));
    }


    public function store(CompanyRequest $request): string|\Illuminate\Http\RedirectResponse
    {
        $data = $request->validated();
        if ($request->hasFile('logo')) {
            $data['logo'] = $this->upload([
                'file' => 'logo',
                'path' => 'company',
                'upload_type' => 'single',
                'delete_file' => ''
            ]);
        }
        Company::query()->create($data);
        return redirect()->route('admin.company.index')->with('success', _trans('Done Save Data Successfully'));
    }


    public function edit(Company $company): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        $edit = true;
        return view('admin.company.form', compact('edit',  'company'));
    }

    public function update(CompanyRequest $request, Company $company): \Illuminate\Http\RedirectResponse
    {
        $data = $request->validated();
        if ($request->hasFile('logo')) {
            $data['logo'] = $this->upload([
                'file' => 'logo',
                'path' => 'company',
                'upload_type' => 'single',
                'delete_file' => $company->logo ?? ''
            ]);
        }
        $company->update($data);
        return redirect()->route('admin.company.index')->with('success', _trans('Done Updated Data Successfully'));
    }



    public function updateStatus(Request $request): \Illuminate\Http\JsonResponse
    {
        $company = Company::query()->findOrFail($request->id);
        if (!$company) {
            return $this->success(_trans('Not Found'), status: false);
        }
        $status = !$company->status;
        $company->update(['status' => $status]);
        return $this->success(_trans('Done Updated Data Successfully'));
    }
}
