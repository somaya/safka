<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ContactUsRequest;
use App\Mail\Customer\ContactUs as ContactUsMail;
use App\Models\ContactUs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactUsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:ContactUs list,admin'])->only(['index']);
        $this->middleware(['permission:ContactUs add,admin'])->only(['create']);
        $this->middleware(['permission:ContactUs edit,admin'])->only(['edit']);
        $this->middleware(['permission:ContactUs delete,admin'])->only(['destroy']);
    }

    public function index()
    {
        try {
            $data = ContactUs::allContactUs();
            $columns = [
                'all' => _trans('All'),
                'full_name' => _trans('Full name'),
                'phone' => _trans('Phone'),
                'email' => _trans('E-mail'),
            ];
            return view('admin.contact-us.index', compact('data', 'columns'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }


    public function create()
    {
        try {
            $edit = false;
            return view('admin.contact-us.form', compact('edit'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function store(ContactUsRequest $request)
    {
        $data = $request->validated();
        $data['full_name'] = $request->name;
        try {
            Mail::to($data['email'])->send(new ContactUsMail(['data' => $data]));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage())->withInput($request->all());
        }
        return redirect()->route('admin.contact-us.index')->with('success', _trans('Done reply successfully'));
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        try {
            $edit = true;
            $contact = ContactUs::query()->findOrFail($id);
            return view('admin.contact-us.form', compact('edit', 'contact'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function update(ContactUsRequest $request, $id)
    {
        $contact = ContactUs::query()->findOrFail($id);
        $contact->update(['reply' => $request->message]);
        try {
            $contact['message'] = $request->message;
            Mail::to($contact['email'])->send(new ContactUsMail(['data' => $contact->toArray()]));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage())->withInput($request->all());
        }
        return redirect()->route('admin.contact-us.index')->with('success', _trans('Done reply successfully'));
    }

    public function destroy($id)
    {
        try {
            $contact = ContactUs::query()->findOrFail($id);
            $contact->delete();
            return redirect()->route('admin.contact-us.index')->with('success', _trans('Done Deleted Data Successfully'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }
}
