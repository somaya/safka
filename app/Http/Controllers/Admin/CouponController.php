<?php

namespace App\Http\Controllers\Admin;

use App\Enums\CouponType;
use App\Events\SendNotificationUser;
use App\Helpers\CPU\Models;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CouponRequest;
use App\Models\Coupon;
use App\Models\Customer;
use App\Models\Notification;
use App\Models\Shop;
use App\Traits\ApiResponses;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CouponController extends Controller
{
    use ApiResponses;

    public function __construct()
    {
        $this->middleware(['permission:Coupon list,admin'])->only(['index']);
        $this->middleware(['permission:Coupon add,admin'])->only(['create', 'store']);
        $this->middleware(['permission:Coupon edit,admin'])->only(['edit', 'update', 'updateStatus']);
        $this->middleware(['permission:Coupon delete,admin'])->only(['destroy']);
    }

    public function index(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $coupons = Coupon::query()->with(['owner.user', 'shop'])->withCount(['usedCoupon', 'owner', 'shop']);
            $coupons = Coupon::allCoupons($coupons);
            $columns = [
                'all' => _trans('All'),
                'code' => _trans('Coupon code'),
                'name' => _trans('Coupon name'),
                'owner' => _trans('Owner name'),
                'shop' => _trans('Shop name'),
            ];
            return view('admin.coupon.index', compact('coupons', 'columns'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }


    public function create(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        $edit = false;
        $owners = Models::owners()->get();
        return view('admin.coupon.form', compact('edit', 'owners'));
    }


    public function store(CouponRequest $request): \Illuminate\Http\RedirectResponse
    {
        try {
            DB::beginTransaction();
            $data = $request->validated();
            $coupon = Coupon::query()->create($data);
            if ($coupon) {
                $this->notifications($coupon, $data, $request);
                DB::commit();
                return redirect()->route('admin.coupon.index')->with('success', _trans('Done Save Data Successfully'));
            }
            return redirect()->back()->with('warning', _trans('Some failed errors'))->withInput($data);
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage())
                ->withInput($request->all());
        }
    }



    public function show(Coupon $coupon)
    {
        //
    }


    public function edit(Coupon $coupon): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        $edit = true;
        $owners = Models::owners()->get();
        return view('admin.coupon.form', compact('edit', 'owners', 'coupon'));
    }


    public function update(CouponRequest $request, Coupon $coupon): \Illuminate\Http\RedirectResponse
    {
        $data = $request->validated();
        $coupon->update($data);
        $coupon->fresh();
        if ($coupon->wasChanged(['coupon_type', 'owner_id', 'shop_id', 'code',])) {
            $this->notifications($coupon, $data, $request);
        }
        return redirect()->route('admin.coupon.index')->with('success', _trans('Done Updated Data Successfully'));
    }


    public function destroy(Coupon $coupon)
    {
        //
    }

    public function updateStatus(Request $request): \Illuminate\Http\JsonResponse
    {
        $coupon = Coupon::query()->findOrFail($request->id);
        if (!$coupon) {
            return $this->success(_trans('Not Found'), status: false);
        }
        $status = !$coupon->status;
        $coupon->update(['status' => $status]);
        return $this->success(_trans('Done Updated Data Successfully'));
    }
    public function notifications($coupon, $data, $request)
    {
        $customers = Customer::query()->with(['fcmTokens'])->has('fcmTokens')->orderByDesc('created_at')->get();
        foreach ($customers as $customer) {
            $notifications = $customer->fcmTokens()->pluck('fcm_token', 'lang')->toArray();
            foreach ($notifications as $lang => $token) {
                if ($request->coupon_type == CouponType::Customized->value) {
                    event(new SendNotificationUser([
                        'notification_type' => 'coupon',
                        'code' => $coupon->code,
                        'shop_id' => $coupon->shop_id,
                    ], [$token], $data[$lang]['name'], $data[$lang]['description']));
                } else {
                    event(new SendNotificationUser([
                        'notification_type' => 'general',
                        'code' => $coupon->code,
                    ], [$token], $data[$lang]['name'], $data[$lang]['description']));
                }

            }
        }
    }

}
