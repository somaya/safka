<?php

namespace App\Http\Controllers\Admin;

use App\Models\Customer;
use App\Models\Order;
use App\Traits\ApiResponses;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Spatie\SimpleExcel\SimpleExcelWriter;

class CustomerController extends Controller
{
    use ApiResponses;

    public function __construct()
    {
        $this->middleware(['permission:Customer list,admin'])->only(['index']);

    }

    public function index(Request $request)
    {
        try {
            $customers = Customer::query()->orderBy('created_at','ASC')
                ->when($request->shop, function ($q) use ($request) {
                $q->whereHas('orders', function ($q) use ($request) {
                    return $q->whereHas('shop',function ($query) use($request){
                        $query->whereTranslation('name', $request->shop, default_lang());
                    });
                });
            })
            ->when($request->coupon, function (Builder $builder) use ($request) {
                $builder->whereHas('coupons', function (Builder $builder) use ($request) {
                    $builder->where('coupons.id', '=', $request->coupon);
                });
            });

            $customers = $customers->withCount(['orders']);
            $data = Customer::allCustomers($customers);
            $columns = [
                'all' => _trans('All'),
                'name' => _trans('Name'),
                'email' => _trans('Email'),
                'phone' => _trans('Phone'),
                'shop' => _trans('Shop'),
            ];
            return view('admin.customer.index', compact('data', 'columns'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }


    public function show($id)
    {
        try {
            $customer = Customer::query()->find($id);
            $orders=Order::query()->where('customer_id',$id)->get();
            if ($customer) {
                return view('admin.customer.show', compact('customer','orders'));
            }
            return redirect()->back()->with('warning', _trans('Not found this customer'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }


    public function updateStatus(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            DB::beginTransaction();
            $customer = Customer::query()->find($request->id);
            if (!$customer) {
                return $this->success(_trans('Not Found'), false);
            }
            $status = !$customer->user->status;
            $customer->update(['blocked' => !$status]);
            if ($customer->user->update(['status' => $status])) {
                $customer->user->tokens()->delete();
                DB::commit();
                return $this->success(_trans('Done Updated Data Successfully'));
            }
            return $this->success( _trans('Some failed errors'), false);
        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->success( $exception->getMessage(), false);
        }
    }
    public function export()
    {
        $customer = Customer::query();
        $search = request('search');
        if (request('column_name') == 'name') {
            $customer->whereHas('user', function ($query) use ($search) {
                $query->where('name', 'LIKE', "%{$search}%");
            });
        }
        if (request('column_name') == 'email') {
            $customer->whereHas('user', function ($query) use ($search) {
                $query->where('email', 'LIKE', "%{$search}%");
            });
        }
        if (request('column_name') == 'phone') {
            $customer->where('phone', 'LIKE', "%{$search}%");
        }
        if (request('column_name') == 'shop') {
            $customer->whereHas('orders', function ($query) use ($search) {
                $query->whereHas('shop', function ($q) use ($search) {
                    $q->whereTranslationLike('name', "%{$search}%", default_lang());
                });
            });
        }

        if (request('column_name') == 'notification_mobile') {
            $customer->has('fcmTokens');
        }

        $rows = [];
        $customer->chunk(2000, function ($customers) use (&$rows) {

            foreach ($customers->toArray() as $customer) {
                $rows[] = [
                    'id' => $customer['user']['id'],
                    'user_id' => $customer['id'],
                    'name' => $customer['user']['name'],
                    'email' => $customer['user']['email'],
                    'phone' => $customer['phone'],
                    'birthday' => $customer['birthday'],
                    'age' => $customer['age'],
                    'delete account' => $customer['user']['status'] == 1 ? 'active' : 'not active',
                ];
            }
        });

        SimpleExcelWriter::streamDownload('customers.csv', 'csv')->addRows($rows);
    }
    public function changePassword(Request $request): \Illuminate\Http\RedirectResponse
    {
        $request->validate([
            'id' => ['required', 'integer', Rule::exists('customers', 'id')
            ],
            'password' => ['required', 'string', 'min:3', 'confirmed'],
            'password_confirmation' => ['required', 'same:password'],
        ]);
        $customer = Customer::query()->with(['user'])->find($request->id);
        $customer->user()->update(['password' => Hash::make($request->password)]);
        return redirect()->route('admin.customer.index')->with('success', _trans('Done Updated Data Successfully'));
    }
}
