<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\PermissionAdmin;
use App\Helpers\PermissionOwner;
use App\Http\Controllers\Controller;
use App\Models\AdminConversation;
use App\Models\Customer;
use App\Models\Discount;
use App\Models\Extra;
use App\Models\Offer;
use App\Models\Order;
use App\Models\Owner;
use App\Models\Product;
use App\Models\Save;
use App\Models\Shop;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class DashboardController extends Controller
{
    public function dashboard(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Contracts\Foundation\Application
    {
        $all_orders = Order::query()->count();
        $shops = Shop::query()->status()->count();
        $owners = Owner::query()->count();
        $customers = Customer::query()->count();
        $offers = Offer::query()->status()->count();
        $saves = Save::query()->status()->count();
        $discounts = Discount::query()->status()->count();
        $products = Product::query()->status()->count();
        $delivery_price = Order::query()->where('status_pay', 'paid')->sum('delivery_price');
        $cash_orders = Order::where(['status_pay' => 'paid', 'payment_method' => 'cash'])->count();
        $credit_orders = Order::where(['status_pay' => 'paid', 'payment_method' => 'visa'])->count();
        $subscription_participation = Shop::where('subscription_type', 'participation')->status()->count();
        $shops_by_percent = Shop::where('subscription_type', 'percent')->status()->count();
        $complaints = AdminConversation::query()->count();

        $admin_messages = AdminConversation::query()->groupBy('sender_id')->count();
        return view('admin.dashboard',
            compact('all_orders', 'shops', 'owners', 'customers',
                'offers', 'saves', 'discounts', 'products', 'delivery_price',
                'cash_orders', 'credit_orders', 'subscription_participation', 'shops_by_percent', 'admin_messages', 'complaints'));
    }

    public function permissionAdmin(): string
    {
        try {
            DB::beginTransaction();
            $roleSuperAdmin = Role::findById(1, 'admin');
            $models = PermissionAdmin::models();
            $lists = PermissionAdmin::lists();
            $adminPermissions = $this->storePermissions($models, $lists);
            $roleSuperAdmin->givePermissionTo($adminPermissions);
            DB::commit();
            return redirect()->back()->with('success', 'done');
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }
    public function permissionOwner(): string
    {
        try {
            DB::beginTransaction();
            $roleSuperAdmin = Role::findById(2,'web');
            $models = PermissionOwner::models();
            $lists = PermissionOwner::lists();

            $adminPermissions = $this->storePermissions($models, $lists);
            $roleSuperAdmin->givePermissionTo($adminPermissions);
            DB::commit();
            return 'done';
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    private function storePermissions($models, $lists)
    {
        foreach ($models as $row) {
            foreach ($lists as $value) {
                Permission::query()->updateOrCreate([
                    'name' => $row . " " . $value,
                    'guard_name' => 'admin',
                ], [
                    'name' => $row . " " . $value,
                    'guard_name' => 'admin',
                ]);
            }
        }
        return $this->permissions($models, $lists);
    }

    private function permissions($modules, $lists): array
    {
        $data = [];
        foreach ($modules as $value) {
            foreach ($lists as $item) {
                $data[] = ['name' => $value . ' ' . $item];
            }
        }
        return $data;
    }
}
