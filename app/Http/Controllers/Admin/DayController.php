<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\DayRequest;
use App\Models\WeekDay;

class DayController extends Controller
{


    public function __construct()
    {
        $this->middleware(['permission:Day list,admin'])->only(['index']);
        $this->middleware(['permission:Day edit,admin'])->only(['edit', 'update']);
        $this->middleware(['permission:Day delete,admin'])->only(['destroy']);
    }

    public function index(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $data = WeekDay::allDays();
            $columns = [
                'all' => _trans('All'),
                'name' => _trans('Day name'),
            ];
            return view('admin.day.index', compact('data', 'columns'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function create()
    {
        //
    }


    public function store(DayRequest $request)
    {
        //
    }

    public function show(WeekDay $day)
    {
        //
    }

    public function edit(WeekDay $day): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $edit = true;
            return view('admin.day.form', compact('edit', 'day'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function update(DayRequest $request, WeekDay $day): \Illuminate\Http\RedirectResponse
    {
        $data = $request->validated();
        $day->update($data);
        return redirect()->route('admin.day.index')->with('success', _trans('Done Updated Data Successfully'));
    }

    public function destroy(WeekDay $day)
    {

    }

}
