<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\CPU\Models;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\JoinUsRequest;
use App\Models\JoinUs;
use App\Models\Owner;
use App\Models\Shop;
use App\Traits\UploadFileTrait;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Spatie\Permission\Models\Role;

class JoinUsController extends Controller
{
    use UploadFileTrait;
    public function __construct()
    {
        $this->middleware(['permission:JoinUs list,admin'])->only(['index']);
        $this->middleware(['permission:JoinUs add,admin'])->only(['show']);
        $this->middleware(['permission:JoinUs delete,admin'])->only(['delete']);
    }

    public function index(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $data = JoinUs::allJoinUs();
            $columns = [
                'all' => _trans('All'),
                'shop_name' => _trans('Shop name'),
                'owner_name' => _trans('Owner name'),
                'owner_phone' => _trans('Owner phone'),
            ];
            return view('admin.join-us.index', compact('data', 'columns'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function show($id): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $countries = Models::countries();
            $weeks = Models::weeks();
            $branchTypes = Models::branchTypes();
            $shop = JoinUs::query()->with(['branchTypes'])->findOrFail($id);
            return view('admin.join-us.form', compact('countries', 'weeks', 'branchTypes',
                 'shop'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function store(JoinUsRequest $request)
    {
        try {
            DB::beginTransaction();
            $userData = $request->only(['country_id', 'governorate_id', 'region_id']);
            $role = Role::findByName('owner', 'web');
            $userData['name'] = $request->owner_name;
            $userData['email'] = $request->owner_email;
            $userData['password'] = $request->password;
            $userData['role_id'] = $role->id;
            $dataOwner = $request->only([ 'address', 'brand_name']);
            $dataOwner['phone'] = $request->owner_phone;

            if (!$role) {
                return back()->with('error', _trans('Call Developer'))->withInput($request->all());
            }
            $owner = Owner::query()->create($dataOwner);
            $user = $owner->user()->create($userData);
            $user->assignRole($role);

            if ($user) {
                $shopData = $request->validated();
                $shopData['owner_id'] = $owner->id;
                if ($request->hasFile('logo')) {
                    $shopData['logo'] = $this->upload([
                        'file' => 'logo',
                        'path' => 'shop',
                        'upload_type' => 'single',
                        'delete_file' => ''
                    ]);
                }
                $shop = Shop::query()->create($shopData);
                $shop->branchTypes()->attach($request->branch_type_id);
                $shop->workingHours()->createMany($request->shops);
                if ($shop) {
                    $encrypt = Crypt::encryptString($shop->id);
                    $url = route('download-app') . "?token=$encrypt";
                    Models::qrCodeGenerator($url, 'shop/shop_id_' . $shop['id'] . '.png');
                    $shop->update(['qr_code' => 'shop/shop_id_' . $shopData['id'] . '.png']);
                    JoinUs::query()->whereId($request->join_id)->delete();
                    DB::commit();
                    return redirect()->route('admin.shop.index')->with('success', _trans('Done Save Data Successfully'));
                }
                return redirect()->back()->with('warning', _trans('Please try again'));
            }
            return redirect()->back()->with('warning', _trans('Please try again'));
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }


    public function delete($id): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $data = JoinUs::query()->findOrFail($id);
            $data->delete();
            return redirect()->back()->with('success', _trans('Done Delete information shop'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

}
