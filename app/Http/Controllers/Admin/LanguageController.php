<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\CPU\CreateFileLanguages;
use App\Helpers\Setting\Utility;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\LanguageRequest;
use App\Models\Language;
use App\Traits\ApiResponses;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class LanguageController extends Controller
{
    use ApiResponses;
    public function __construct()
    {
        $this->middleware(['permission:Language list,admin'])->only(['index']);
        $this->middleware(['permission:Language add,admin'])->only(['create']);
        $this->middleware(['permission:Language edit,admin'])->only(['edit']);
        $this->middleware(['permission:Language delete,admin'])->only(['destroy']);
    }

    public function index(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $data = Language::allLanguages();
            $columns = [
                'all' => _trans('All'),
                'code' => _trans('Language Code'),
                'name' => _trans('Language name'),
            ];
            return view('admin.language.index', compact('data', 'columns'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function create(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $edit = false;
            $files = File::files(base_path('public/flags'));
            $jsonString = file_get_contents(base_path('public/js/codes.json'));
            $codes = json_decode($jsonString, true);
            return view('admin.language.form', compact('edit', 'files','codes'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function store(LanguageRequest $request)
    {
        try {
            DB::beginTransaction();
            $data = $request->validated();
            $data['status'] = 1;
            $language = Language::query()->create($data);
            if ($language) {
                DB::commit();
                /*if (!file_exists(base_path('lang/' . $request['code']))) {
                    // make direction and file php
                    mkdir(base_path('lang/' . $request['code']), 0777, true);
                }

                $lang_file = fopen(base_path('lang/' . $request['code'] . '/' . 'message.php'), "w") or die("Unable to open file!");
                $read = file_get_contents(base_path('lang/en/message.php'));
                fwrite($lang_file, $read);*/
                CreateFileLanguages::file($request['code']);
                return redirect()->route('admin.language.index')->with('success', _trans('Done Save Data Successfully'));
            }
            return redirect()->back()->with('warning', _trans('Some failed errors'));
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage())
                ->withInput($request->all());
        }
    }

    public function show(Language $language): \Illuminate\Http\RedirectResponse
    {
        return redirect()->back()->with('error', _trans('Not Allow Access Here'));
    }

    public function edit(Language $language): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $edit = true;
            $files = File::files(base_path('public/flags'));
            $jsonString = file_get_contents(base_path('public/js/codes.json'));
            $codes = json_decode($jsonString, true);
            return view('admin.language.form', compact('edit', 'codes','files', 'language'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function update(LanguageRequest $request, Language $language): \Illuminate\Http\RedirectResponse
    {
        try {
            DB::beginTransaction();
            $data = $request->validated();
            if ($language->update($data)) {
                CreateFileLanguages::file($request['code']);
                DB::commit();
                return redirect()->route('admin.language.index')->with('success', _trans('Done Updated Data Successfully'));
            }
            return redirect()->back()->with('warning', _trans('Some failed errors'));
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage())
                ->withInput($request->all());
        }
    }

    public function updateStatus(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            DB::beginTransaction();
            $language = Language::query()->findOrFail($request->id);
            if (!$language) {
                return $this->success( _trans('Not Found'), false);
            }
            $languages = Language::query();
            // if main is status change to 0
            if ($language->status == 1) {
                $languages = $languages->where('status', '=', 1)
                    ->whereNotIn('id', [$language->id])->count();
            } else {
                // if main is status change to 1
                $languages = $languages->count();
            }

            if ($languages == 0) {
                return $this->success(_trans('You can not disabled is language'), false);
            }

            $status = !$language->status;
            if ($language->update(['status' => $status])) {
                DB::commit();
                return $this->success(_trans('Done Updated Data Successfully'));
            }
            return $this->success( _trans('Some failed errors'), false);
        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->success( $exception->getMessage(), false);
        }
    }

    public function updateDefaultStatus(Request $request)
    {
        try {
            DB::beginTransaction();
            $language = Language::query()->findOrFail($request->id);
            if (!$language) {
                return $this->success( _trans('Not Found'), false);
            }
            if ($language->update(['default' => 1, 'status' => 1])) {
                Language::query()->where([
                    ["default", "=", 1],
                    ["id", "<>", $request->id]
                ])->update(['default' => 0]);
                DB::commit();
                return $this->success(_trans('Done Updated Data Successfully'));
            }
            return $this->success( _trans('Some failed errors'), false);
        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->success( $exception->getMessage(), false);
        }
    }

    public function destroy(Language $language): \Illuminate\Http\RedirectResponse
    {
        try {
            DB::beginTransaction();
            $languages = Language::query()->count();
            if ($languages != 1) {
                if ($language->delete()) {
                    /*$dir = base_path('lang/' . $language->code);
                    $it = new RecursiveDirectoryIterator($dir, RecursiveDirectoryIterator::SKIP_DOTS);
                    $files = new RecursiveIteratorIterator($it, RecursiveIteratorIterator::CHILD_FIRST);
                    foreach ($files as $file) {
                        if ($file->isDir()) {
                            rmdir($file->getRealPath());
                        } else {
                            unlink($file->getRealPath());
                        }
                    }
                    rmdir($dir);*/
                    DB::commit();
                    return redirect()->route('admin.language.index')->with('success', _trans('Done Deleted Data Successfully'));
                }
                return redirect()->back()->with('warning', _trans('Some failed errors'));
            } else {
                Language::query()->first()->update(['status' => 1, 'default' => 1]);
                return redirect()->route('admin.language.index')->with('warning', _trans('You Can not delete this Language'));
            }

        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function translate(Request $request, $lang): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Contracts\Foundation\Application
    {
        $columns = [
            'all' => _trans('All'),
            'key' => _trans('Key'),
            'value' => _trans('Value'),
        ];

        $full_data = include(base_path('lang/' . $lang . '/message.php'));
        $data = [];
        ksort($full_data);

        foreach ($full_data as $key => $value) {
            array_push($data, ['key' => $key, 'value' => $value]);
        }
        if ($request->filled('column_name')) {
            $data = collect($data)->filter(function ($item) use ($request) {
                if ($request->column_name == 'key') {
                    return stripos($item['key'], $request->search) !== false;
                } elseif ($request->column_name == 'value') {
                    return stripos($item['value'], $request->search) !== false;
                } else {
                    return stripos($item['key'], $request->search) !== false || stripos($item['value'], $request->search) !== false;
                }
            });
        }

        $data = $this->paginate($data);
        $data->withPath('');

        return view('admin.language.translate', compact('lang', 'data', 'columns'));
    }

    public function translate_submit(Request $request, $lang): \Illuminate\Http\JsonResponse
    {
        $full_data = include(base_path('lang/' . $lang . '/message.php'));
        $full_data[$request['key']] = $request['value'];
        $str = "<?php return " . var_export($full_data, true) . ";";
        file_put_contents(base_path('lang/' . $lang . '/message.php'), $str);
        return $this->success(_trans('Done Updated Data Successfully'));
    }

    public function paginate($items, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, Utility::getValByName('pagination_limit')), $items->count(), Utility::getValByName('pagination_limit'), $page, $options);
    }
}
