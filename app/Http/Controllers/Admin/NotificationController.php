<?php

namespace App\Http\Controllers\Admin;

use App\Enums\Status;
use App\Events\SendNotificationUser;
use App\Helpers\Setting\Utility;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\NotificationRequest;
use App\Models\Admin;
use App\Models\Customer;
use App\Models\Notification;
use App\Models\Owner;
use App\Models\Shop;
use App\Models\ShopStaff;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Spatie\SimpleExcel\SimpleExcelReader;

class NotificationController extends Controller
{

    public function __construct()
    {
        $this->middleware(['permission:Notification list'])->only(['index']);
        $this->middleware(['permission:Notification add'])->only(['store']);
    }

    public function index()
    {
        try {
            return view('admin.notification.index');
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function notificationAdmin(Request $request)
    {
        try {
            $query = [
                'receiverable_type' => Admin::class,
                'receiverable_id' => auth()->user()->id,
            ];
            $data = Notification::query()->with(['customer.user', 'senderable'])->where($query)
                ->orderByDesc('created_at')->paginate(Utility::getValByName('pagination_limit'));

            Notification::query()->with(['customer.user', 'senderable'])
                ->where($query)->whereNull('read_at')->update(['read_at' => now()]);
            return view('admin.notification.view', $data);
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }

    }

    public function store(NotificationRequest $request)
    {
        try {
            $this->sendNotification($request);
            return redirect()->back()->with('success', _trans('Done send notification successfully'))->withInput($request->all());
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage())->withInput($request->all());
        }
    }

    public function sendNotification($request)
    {
        if (($request->notification_type == 'customer' && $request->custom_type == 'all') || $request->notification_type == 'all') {
            $customers = Customer::query()->has('fcmTokens')
                ->with(['fcmTokens'])
                ->where('blocked', '=', Status::Not_Active->value)
                ->get();
            $this->customers($request, $customers, 'customer');

        } elseif ($request->notification_type == 'customer' && $request->custom_type == 'custom') {
            $this->customNotifications($request, 'customer');
        } elseif ($request->notification_type == 'shop' && $request->custom_type == 'all') {
            $customers = Customer::query()->has('fcmTokens')
                ->with(['fcmTokens'])
//                ->whereRelation('orders', 'shop_id', '=', $request->shop_id)
                ->where('blocked', '=', Status::Not_Active->value)
                ->get();
            $this->customers($request, $customers, 'shop');

        } elseif ($request->notification_type == 'shop' && $request->custom_type == 'custom') {
            $this->customNotifications($request, 'shop');
        }

    }

    protected function customers($request, $customers, $type) // if custom_type all
    {
        foreach ($customers as $customer) {
            $notifications = $customer->fcmTokens->pluck('fcm_token', 'lang')->toArray();
            foreach ($notifications as $lang => $token) {
                if ($type == 'customer') {
                    Notification::query()->create([
                        'senderable_type' => Admin::class,
                        'senderable_id' => 1,
                        'receiverable_type' => Customer::class,
                        'receiverable_id' => $customer->id,
                        'type' => 'global',
                        'data' => [
                            'type' => 'global',
                            'code' => 'home',
                            'title' => $request[locale()]['title'],
                            'description' => $request[locale()]['body'],
                        ],
                    ]);
                    event(new SendNotificationUser(['notification_type' => 'global'], [$token],
                        $request[$lang]['title'], $request[$lang]['body']));
                } else {
                    Notification::query()->create([
                        'senderable_type' => Shop::class,
                        'senderable_id' => $request->shop_id,
                        'receiverable_type' => Customer::class,
                        'receiverable_id' => $customer->id,
                        'type' => 'shop',
                        'data' => [
                            'type' => 'shop',
                            'code' => $request['shop_id'],
                            'title' => $request[locale()]['title'],
                            'description' => $request[locale()]['body'],
                        ],
                    ]);
                    event(new SendNotificationUser([
                        'notification_type' => 'notification_shop',
                        'shop_id' => $request->shop_id,
                    ], [$token],
                        $request[$lang]['title'], $request[$lang]['body']));
                }
            }
        }
    }

    protected function customNotifications($request, $type)
    {
        SimpleExcelReader::create($request->file, 'csv',)
            ->headersToSnakeCase()
            ->getRows()
            ->each(function (array $rowProperties) use ($request, $type) {
                $customerId = Arr::exists($rowProperties, 'user_id') ? $rowProperties['user_id'] : null;
                $customer = Customer::query()
                    ->where('blocked', '=', Status::Not_Active->value);
                /*if ($type == 'furniture') {
                    $customer = $customer->whereRelation('orders', 'furniture_id', '=', $request->furniture_id);
                }*/
                $customer = $customer->has('fcmTokens')->with(['fcmTokens'])->find($customerId);
                if ($customer) {
                    $notifications = $customer->fcmTokens->pluck('fcm_token', 'lang')->toArray();
                    foreach ($notifications as $lang => $token) {
                        if ($type == 'shop') {
                            event(new SendNotificationUser([
                                'notification_type' => 'notification_shop',
                                'shop_id' => $request->shop_id,
                            ], [$token],
                                $request[$lang]['title'], $request[$lang]['body']));
                        } else {

                            event(new SendNotificationUser(['notification_type' => 'global'], [$token],
                                $request[$lang]['title'], $request[$lang]['body']));
                        }
                    }
                }
            });
    } // if custom_type  custom


    public function notificationOwner(Request $request)
    {
        try {
            $data = Notification::query()->with(['customer.user']);
            $notification = Notification::query();

            if (auth()->user()->userable_type == ShopStaff::class) {
                $data = $data->whereHas('customer.orders', function (Builder $query) {
                    $query->whereIn('shop_id', userType())
                        ->whereIn('type', ['order', 'account_order', 'message', 'invoice']);
                });

                $notification = $notification->whereHas('customer.orders', function (Builder $query) {
                    $query->whereIn('shop_id', userType())
                        ->whereIn('type', ['order', 'account_order', 'message', 'invoice']);
                });
            } else {
                $data = $data->where(function (Builder $query) {
                    $query->where('receiverable_type', '=', User::class)
                        ->where('receiverable_id', '=', auth()->user()->id)
                        ->orWhere('senderable_type', '=', Admin::class);
                });
                $notification = $notification->where(function (Builder $query) {
                    $query->where('receiverable_type', '=', User::class)
                        ->where('receiverable_id', '=', auth()->user()->id)
                        ->orWhere('senderable_type', '=', Admin::class);
                });
            }
            $data = $data->orderByDesc('created_at')->paginate(Utility::getValByName('pagination_limit'));
            $notification->whereNull('read_at')->update(['read_at' => now()]);
            return view('admin.notification.view', $data);
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }

    }
}
