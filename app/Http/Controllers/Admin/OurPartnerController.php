<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Setting\Utility;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\OurPartnerRequest;
use App\Models\OurPartner;
use App\Traits\ApiResponses;
use App\Traits\UploadFileTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class OurPartnerController extends Controller
{
    use ApiResponses, UploadFileTrait;

    public function __construct()
    {
        $this->middleware(['permission:Our#Partner list'])->only(['index']);
        $this->middleware(['permission:Our#Partner add'])->only(['create']);
        $this->middleware(['permission:Our#Partner edit'])->only(['edit']);
        $this->middleware(['permission:Our#Partner delete'])->only(['destroy']);
    }

    public function index(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        $search = str_replace('OurPartner#', '', request('search'));
        $ourPartners = OurPartner::query()
            ->when(request()->column_name == 'code', function (Builder $builder) use ($search) {
                $builder->where('id', '=', $search);
            })
            ->orderByDesc('ranking')
            ->paginate(Utility::getValByName('pagination_limit'))
            ->withQueryString();
        $columns = [
            'code' => _trans('Our Partner Code'),
        ];

        return view('admin.our-partner.index', compact('ourPartners', 'columns'));
    }

    public function create(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        $edit = false;
        return view('admin.our-partner.form', compact('edit'));
    }

    public function store(OurPartnerRequest $request): \Illuminate\Http\RedirectResponse
    {
        $data = $request->validated();
        if ($request->hasFile('logo')) {
            $data['logo'] = $this->upload([
                'file' => 'logo',
                'path' => 'our-partner',
                'upload_type' => 'single',
                'delete_file' => ''
            ]);
        }
        OurPartner::query()->create($data);
        return redirect()->route('admin.our-partner.index')->with('success', _trans('Done Save Data Successfully'));
    }

    public function show(OurPartner $ourPartner)
    {
        //
    }

    public function edit(OurPartner $ourPartner): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        $edit = true;
        return view('admin.our-partner.form', compact('edit', 'ourPartner'));
    }

    public function update(OurPartnerRequest $request, OurPartner $ourPartner): \Illuminate\Http\RedirectResponse
    {
        $data = $request->validated();
        if ($request->hasFile('logo')) {
            $data['logo'] = $this->upload([
                'file' => 'logo',
                'path' => 'our-partner',
                'upload_type' => 'single',
                'delete_file' => $ourPartner->logo ?? ''
            ]);
        }
        $ourPartner->update($data);
        return redirect()->route('admin.our-partner.index')->with('success', _trans('Done Updated Data Successfully'));
    }

    public function destroy(OurPartner $ourPartner)
    {
        //
    }

    public function updateStatus(Request $request): \Illuminate\Http\JsonResponse
    {
        $ourPartner = OurPartner::query()->findOrFail($request->id);
        if (!$ourPartner) {
            return $this->success(_trans('Not Found'), status: false);
        }
        $status = !$ourPartner->status;
        $ourPartner->update(['status' => $status]);
        return $this->success(_trans('Done Updated Data Successfully'));
    }
}
