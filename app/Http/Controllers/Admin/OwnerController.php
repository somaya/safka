<?php

namespace App\Http\Controllers\Admin;

use App\Enums\Status;
use App\Helpers\CPU\Models;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\OwnerRequest;
use App\Models\Order;
use App\Models\Owner;
use App\Models\Shop;
use App\Models\User;
use App\Traits\ApiResponses;
use App\Traits\UploadFileTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class OwnerController extends Controller
{
    use UploadFileTrait, ApiResponses;

    public function __construct()
    {
        $this->middleware(['permission:Owner list,admin'])->only(['index']);
        $this->middleware(['permission:Owner add,admin'])->only(['create', 'store']);
        $this->middleware(['permission:Owner edit,admin'])->only(['edit', 'update', 'updateStatus']);
        $this->middleware(['permission:Owner delete,admin'])->only(['destroy']);
    }

    public function index(Request $request): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $query = Owner::query()->whereHas('user')
                ->with(['user.country','user.governorate', 'user.region'])
                ->withCount('shops')
                ->when($request->owner, function ($query) use ($request) {
                    $query->whereRelation("user", "name", "LIKE", "%{$request->owner}%");
                });
            $data = Owner::allOwners($query);
            $columns = [
                'all' => _trans('All'),
                'code' => _trans('Owner Code'),
                'name' => _trans('Owner name'),
                'shop' => _trans('Shop name'),
                'country' => _trans('Country name'),
                'governorate' => _trans('Governorate name'),
                'region' => _trans('Region name'),
            ];
            return view('admin.owner.index', compact('data', 'columns'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }


    public function create(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        $edit = false;
        $countries = Models::countries();
        return view('admin.owner.form', compact('edit', 'countries'));
    }

    public function store(OwnerRequest $request): string|\Illuminate\Http\RedirectResponse
    {
        $dataOwner = $request->validated();
        $dataUser = $request->only(['name', 'email', 'password', 'country_id', 'governorate_id', 'region_id']);
        $role = Role::findByName('owner', 'web');
        $dataUser['role_id'] = $role->id;
        $dataUser['status'] = Status::Active->value;
        try {
            DB::beginTransaction();
            $owner = Owner::query()->create($dataOwner);
            if ($request->hasFile('avatar')) {
                $dataUser['avatar'] = $this->upload([
                    'file' => 'avatar',
                    'path' => 'owner',
                    'upload_type' => 'single',
                    'delete_file' => ''
                ]);
            }
            $user = $owner->user()->create($dataUser);
            $user->assignRole($role);
            DB::commit();
            return redirect()->route('admin.owner.index')->with('success', _trans('Done Save Data Successfully'));
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage())->withInput($request->all());
        }
    }

    public function show($id)
    {

        try {
            $owner = Owner::query()->find($id);

            $query = Shop::query()->orderBy('created_at','DESC')
                ->with(['country','branchTypes', 'governorate','region', 'rates'])
                ->withCount(['products'])
                ->where('owner_id', $id);

            $data = Shop::allShops($query);
            $columns = [
                'all' => _trans('All'),
                'code' => _trans('Shop Code'),
                'name' => _trans('Shop name'),
                'country' => _trans('Country name'),
                'governorate' => _trans('Governorate name'),
                'region' => _trans('Region name'),
            ];
            $orders = Order::whereHas('shop', function ($q) use ($id) {
                $q->where('owner_id', $id);
            })->get();

            return view('admin.owner.show', compact('data', 'columns', 'owner','orders'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }

    }

    public function edit($id): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $edit = true;
            $owner = Owner::query()->with(['user'])->findOrFail($id);
            $countries = Models::countries();
            return view('admin.owner.form', compact('edit', 'countries', 'owner'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }


    public function update(OwnerRequest $request, $id): string|\Illuminate\Http\RedirectResponse
    {
        $dataOwner = $request->validated();
        $owner = Owner::query()->with(['user'])->findOrFail($id);
        $dataUser = $request->only(['name', 'email', 'country_id', 'governorate_id', 'region_id']);
        $role = Role::findByName('owner', 'web');
        $dataUser['role_id'] = $role->id;
        try {
            DB::beginTransaction();
            $owner->update($dataOwner);
            if ($request->hasFile('avatar')) {
                $dataUser['avatar'] = $this->upload([
                    'file' => 'avatar',
                    'path' => 'owner',
                    'upload_type' => 'single',
                    'delete_file' => $owner->user->avatar ?? ''
                ]);
            }
            $owner->user()->update($dataUser);
            DB::commit();
            return redirect()->route('admin.owner.index')->with('success', _trans('Done Save Data Successfully'));
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage())->withInput($request->all());
        }
    }


    public function destroy($id)
    {
        //
    }

    public function updateStatus(Request $request): \Illuminate\Http\JsonResponse
    {
        $owner = Owner::query()->with(['user'])->find($request->id);
        if (!$owner) {
            return $this->success(_trans('Not Found'), status: false);
        }
        $status = !$owner->user->status;
        $owner->user()->update(['status' => $status]);
        return $this->success(_trans('Done Updated Data Successfully'));
    }

    public function changePassword(OwnerRequest $request): \Illuminate\Http\RedirectResponse
    {
        $owner = Owner::query()->with(['user'])->find($request->id);
        $owner->user()->update(['password' => Hash::make($request->password)]);
        return redirect()->route('admin.owner.index')->with('success', _trans('Done Updated Data Successfully'));
    }
}
