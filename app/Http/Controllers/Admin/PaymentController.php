<?php

namespace App\Http\Controllers\Admin;

use App\Enums\Status;
use App\Enums\SubscriptionType;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\PaymentRequest;
use App\Models\Payment;
use Illuminate\Support\Facades\DB;
use LaravelDaily\Invoices\Invoice;
use LaravelDaily\Invoices\Classes\Buyer;
use LaravelDaily\Invoices\Classes\InvoiceItem;
use LaravelDaily\Invoices\Classes\Party;

class PaymentController extends Controller
{

    public function __construct()
    {
        $this->middleware(['permission:Payment list'])->only(['index']);
        $this->middleware(['permission:Payment add'])->only(['create']);
        $this->middleware(['permission:Payment edit'])->only(['edit']);
        $this->middleware(['permission:Payment delete'])->only(['destroy']);
    }

    public function index(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $payments = Payment::query()->with(['shop' => [
                'owner.user',
                'country.currency'
            ]])->orderByDesc('created_at');
            $payments = Payment::allPayments($payments);
            $columns = [
                'all' => _trans('All'),
                'code' => _trans('Payment code'),
                'shop' => _trans('Shop name'),
            ];
            return view('admin.payment.index', compact('payments', 'columns'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }


    public function store(PaymentRequest $request): string|\Illuminate\Http\RedirectResponse
    {
        $payment = Payment::query()->with(['shop'])->findOrFail($request->payment_id);
        try {
            DB::beginTransaction();
            if ($payment->subscription_type == SubscriptionType::Percent->value) {
                $payment->update([
                    'amount' => $payment->net_amount,
                    'status' => Status::Active->value
                ]);
                $payment->shop->update(['status' => Status::Active->value]);
            }
            if ($payment->subscription_type == SubscriptionType::participation->value) {
                $payment->update([
                    'amount' => $payment->net_amount,
                    'status' => Status::Active->value
                ]);
                $payment->shop->update([
                    'status' => Status::Active->value,
                    'subscribe_date' => now()->format('Y-m-d'),
                ]);
            }
            if ($payment->subscription_type == SubscriptionType::Percent->value) {
                $payment->update([
                    'amount' => $payment->amount,
                    'status' => Status::Active->value
                ]);
                $payment->shop->update([
                    'status' => Status::Active->value,
                ]);
            }
            DB::commit();
            return redirect()->route('admin.payment.index')->with('success', _trans('Done Save Data Successfully'));
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage())->withInput($request->all());
        }
    }

    public function invoice($id)
    {
        $payment = Payment::query()->with(['shop' => [
            'owner.user',
            'country'
        ]])->findOrFail($id);

        return view('admin.payment.invoice', compact('payment'));
    }


}
