<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Rate;
use App\Traits\ApiResponses;
use DB;
use Illuminate\Http\Request;

class RatesController extends Controller
{
    use ApiResponses;
    public function __construct()
    {
        $this->middleware(['permission:Rate list,admin'])->only(['index', 'show','updateStatus']);

    }

    public function index(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $data = Rate::allRates();
            $columns = [
                'all' => _trans('All'),
                'model' => _trans('Model name'),
                'comment' => _trans('Comment'),
                'username' => _trans('Username'),
            ];
            return view('admin.rate.index', compact('data', 'columns'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function show($id)
    {
        try {
            $rate = Rate::query()->findOrFail($id);

            return view('admin.rate.show',
                compact('rate'));

        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }

    }

    public function destroy($id)
    {
        try {
            $rate = Rate::query()->findOrFail($id);
            $rate->delete();
            return redirect()->route('admin.rate.index')->with('success', _trans('Done Deleted Data Successfully'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }



    public function updateStatus(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            DB::beginTransaction();
            $rate = Rate::query()->findOrFail($request->id);
            if (!$rate) {
                return $this->success( _trans('Not Found'), false);
            }
            $status = !$rate->status;
            if ($rate->update(['status' => $status])) {
                DB::commit();
                return $this->success(_trans('Done Updated Data Successfully'));
            }
            return $this->success(_trans('Some failed errors'), false);
        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->success( $exception->getMessage(), false);
        }
    }
}
