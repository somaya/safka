<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\CPU\Models;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ServiceRequest;
use App\Models\Service;
use App\Traits\ApiResponses;
use App\Traits\UploadFileTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ServiceController extends Controller
{
    use UploadFileTrait, ApiResponses;

    public function __construct()
    {
        $this->middleware(['permission:Service list,admin'])->only(['index']);
        $this->middleware(['permission:Service add,admin'])->only(['create', 'store']);
        $this->middleware(['permission:Service edit,admin'])->only(['edit', 'update', 'updateStatus']);
        $this->middleware(['permission:Service delete,admin'])->only(['destroy']);
    }

    public function index(Request $request): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {

        try {
            $query = Service::query()->orderBy('created_at', 'DESC');
            $data = Service::allServices($query);
            $columns = [
                'all' => _trans('All'),
                'code' => _trans('Service code'),
                'name' => _trans('Service name'),
            ];

            return view('admin.service.index', compact('data', 'columns'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function create(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        $edit = false;
        return view('admin.service.form', compact('edit'));
    }

    public function store(ServiceRequest $request)
    {
        $data = $request->validated();
        if ($request->hasFile('image')) {
            $data['image'] = $this->upload([
                'file' => 'image',
                'path' => 'service',
                'upload_type' => 'single',
                'delete_file' => ''
            ]);
        }
        try {
            DB::beginTransaction();
            $service = Service::query()->create($data);
            if ($request->has('images')) {
                $this->upload([
                    'file' => 'images',
                    'path' => 'service',
                    'upload_type' => 'files',
                    'multi_upload' => true,
                    'relationable_id' => $service->id,
                    'relationable_type' => Service::class,
                ]);
            }
            DB::commit();
            return redirect()->route('admin.service.index')->with('success', _trans('Done Save Data Successfully'));
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage())->withInput($request->all());
        }
    }

    public function show($id)
    {
        $service = Service::query()->findOrFail($id);
        return view('admin.service.show', compact('service'));
    }

    public function edit(Service $service): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        $edit = true;
        return view('admin.service.form', compact('edit', 'service'));
    }

    public function update(ServiceRequest $request, Service $service): \Illuminate\Http\RedirectResponse
    {
        $data = $request->validated();
        if ($request->hasFile('image')) {
            $data['image'] = $this->upload([
                'file' => 'image',
                'path' => 'service',
                'upload_type' => 'single',
                'delete_file' => $service->image ?? ''
            ]);
        }
        try {
            DB::beginTransaction();
            if ($service->update($data)) {
                if ($request->has('images')) {
                    $this->upload([
                        'file' => 'images',
                        'path' => 'service',
                        'upload_type' => 'files',
                        'multi_upload' => true,
                        'relationable_id' => $service->id,
                        'relationable_type' => Service::class,
                    ]);
                }
                DB::commit();
                return redirect()->route('admin.service.index')->with('success', _trans('Done Updated Data Successfully'));
            }
            return redirect()->back()->with('warning', _trans('Some failed errors'))->withInput($data);
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage())->withInput($request->all());
        }
    }

    public function destroy(Request $request)
    {
        $service = Service::query()->findOrFail($request->id);
        if (!$service) {
            return $this->success(_trans('Not Found'), status: false);
        }
        $img = $service->image;
        $images = $service->images;
        if ($service->delete()) {
            foreach ($images as $image) {
                $this->deleteFile($image->full_file);
            }
            $images->delete();
            $this->deleteFile($img);
        }
        return $this->success(_trans('Done Updated Data Successfully'));
    }

    public function updateStatus(Request $request): \Illuminate\Http\JsonResponse
    {
        $service = Service::query()->findOrFail($request->id);
        if (!$service) {
            return $this->success(_trans('Not Found'), status: false);
        }
        $status = !$service->status;
        $service->update(['status' => $status]);
        return $this->success(_trans('Done Updated Data Successfully'));
    }

    public function deleteImage(Request $request)
    {
        $file = Models::deleteFile([
            'id' => $request->id,
            'relationable_type' => Service::class,
            'relationable_id' => $request->relation_id,
        ]);
        if ($file) {
            $this->deleteFile($file->full_file);
            $file->delete();
            return $this->success(_trans('Done Deleted Image Successfully'));
        }
        return $this->success(_trans('Not found this image'), status: false);
    }
}
