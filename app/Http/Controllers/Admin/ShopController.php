<?php

namespace App\Http\Controllers\Admin;

use App\Enums\SubscriptionType;
use App\Helpers\CPU\Models;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ShopRequest;
use App\Models\Coupon;
use App\Models\DeliveryArea;
use App\Models\Discount;
use App\Models\Offer;
use App\Models\Order;
use App\Models\Product;
use App\Models\Shop;
use App\Models\Save;
use App\Traits\ApiResponses;
use App\Traits\UploadFileTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use PDF;

class ShopController extends Controller
{
    use UploadFileTrait, ApiResponses;

    public function __construct()
    {
        $this->middleware(['permission:Shop list,admin'])->only(['index']);
        $this->middleware(['permission:Shop add,admin'])->only(['create', 'store']);
        $this->middleware(['permission:Shop edit,admin'])->only(['edit', 'update', 'updateStatus']);
        $this->middleware(['permission:Shop delete,admin'])->only(['destroy']);
    }

    public function index(Request $request): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $query = Shop::query()->orderBy('created_at','DESC')
                ->with(['country','branchTypes', 'governorate','region', 'owner'])
                ->withCount(['products'])
                ->when($request->owner, function ($query) use ($request) {
                    $query->whereRelation('owner.user', 'name', '=', $request->owner);
                })
                ->when($request->shop, function ($query) use ($request) {
                    $query->whereTranslationLike('name', "%{$request->shop}%", default_lang());
                })
                ->when($request->branch_type, function ($query) use ($request) {
                    $query->whereRelation('branchTypes', 'name', 'LIKE', "%{$request->branch_type}%");
                })->when($request->subscription_type, function ($q) use ($request) {
                    $q->where('subscription_type', '=', $request->subscription_type);
                });
            $data = Shop::allShops($query);
            $columns = [
                'all' => _trans('All'),
                'code' => _trans('Shop Code'),
                'name' => _trans('Shop name'),
                'country' => _trans('Country name'),
                'governorate' => _trans('Governorate name'),
                'region' => _trans('Region name'),

            ];
            return view('admin.shop.index', compact('data', 'columns'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function create(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        $edit = false;
        $countries = Models::countries();
        $owners = Models::owners()->get();
        $weeks = Models::weeks();
        $branchTypes = Models::branchTypes();
        return view('admin.shop.form', compact('edit', 'countries', 'owners', 'weeks', 'branchTypes'));
    }

    public function store(ShopRequest $request): string|\Illuminate\Http\RedirectResponse
    {
        $data = $request->validated();
        if ($request->hasFile('logo')) {
            $data['logo'] = $this->upload([
                'file' => 'logo',
                'path' => 'shop',
                'upload_type' => 'single',
                'delete_file' => ''
            ]);
        }
        if ($request->hasFile('image')) {
            $data['image'] = $this->upload([
                'file' => 'image',
                'path' => 'shop',
                'upload_type' => 'single',
                'delete_file' => ''
            ]);
        }
        try {
            DB::beginTransaction();
            if ($request->subscription_type == SubscriptionType::participation->value) {
                $data['subscribe_date'] = $request->subscribe_date;
            }
            if ($request->subscription_type == SubscriptionType::Percent->value) {
                $data['invoice_duration'] = $request->invoice_duration;
            }
            $shop = Shop::query()->create($data);
            if ($shop) {
                $shop->branchTypes()->attach($request->branch_type_id);
                if ($request->shops) {
                    foreach ($request->shops as $item){
                        if ($item['day_id'] && $item['from'] && $item['to'] ){
                            $shop->workingHours()->create($item);
                        }
                    }
                }
                $encrypt = Crypt::encryptString($shop->id);
                $url = route('download-app') . "?token=$encrypt";
                Models::qrCodeGenerator($url, 'shop/shop_id_' . $shop['id'] . '.png');
                $shop->update(['qr_code' => 'shop/shop_id_' . $shop['id'] . '.png']);
                DB::commit();
                return redirect()->route('admin.shop.index')->with('success', _trans('Done Save Data Successfully'));
            }
            return redirect()->back()->with('warning', _trans('Some failed errors'))->withInput($data);
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage())->withInput($request->all());
        }
    }

    public function show($id)
    {
        try {
            $shop = Shop::query()->findOrFail($id);
            $query = Product::query()->where('shop_id', $id)->with(['category', 'shop', 'rates'])
                ->orderBy('created_at', 'desc');
            $data = Product::allProducts($query);
            $columns = [
                'all' => _trans('All'),
                'code' => _trans('Product Code'),
                'name' => _trans('Product name'),
                'category' => _trans('Category name'),
                'shop' => _trans('Shop name'),
            ];

            $offers = Offer::where('shop_id', $id)->status()->count();
            $saves = Save::where('shop_id', $id)->status()->count();
            $discounts = Discount::where('shop_id', $id)->status()->count();
            $delivery_areas = DeliveryArea::where('shop_id', $id)->status()->count();
            $coupons = Coupon::where('shop_id', $id)->status()->count();
            $orders = Order::where('shop_id', $id)->get();
            $orders_total_price = $orders->sum('total_price') - $orders->sum('coupon_price');
            if ($shop->subscription_type == 'participation') {
                $subscription = $shop->subscription_value;
            } else {
                $subscription = ($shop->subscription_value / 100) * $orders_total_price;
            }

            return view('admin.shop.show',
                compact('data', 'columns', 'shop', 'offers',
                    'saves', 'discounts', 'delivery_areas', 'coupons', 'orders',
                    'orders_total_price', 'subscription'));

        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }

    }


    public function edit($id): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        $edit = true;
        $shop = Shop::query()->with(['branchTypes', 'workingHours'])->findOrFail($id);
        $countries = Models::countries();
        $owners = Models::owners()->get();
        $branchTypes = Models::branchTypes();
        $weeks = Models::weeks();
        return view('admin.shop.form', compact('edit', 'shop', 'countries', 'owners', 'branchTypes', 'weeks'));
    }

    public function update(ShopRequest $request, $id): string|\Illuminate\Http\RedirectResponse
    {
        try {
            $data = $request->validated();
            $shop = Shop::query()->findOrFail($id);
            if ($request->hasFile('logo')) {
                $data['logo'] = $this->upload([
                    'file' => 'logo',
                    'path' => 'shop',
                    'upload_type' => 'single',
                    'delete_file' => $shop->logo ?? ''
                ]);
            }
            if ($request->hasFile('image')) {
                $data['image'] = $this->upload([
                    'file' => 'image',
                    'path' => 'shop',
                    'upload_type' => 'single',
                    'delete_file' => $shop->image ?? ''
                ]);
            }
            DB::beginTransaction();
            if ($request->subscription_type == SubscriptionType::participation->value) {
                $data['subscribe_date'] = $request->subscribe_date;
            }
            if ($request->subscription_type == SubscriptionType::Percent->value) {
                $data['invoice_duration'] = $request->invoice_duration;
            }
            if ($shop->update($data))
            {
                if (empty($shop->qr_code)) {
                    $encrypt = Crypt::encryptString($shop->id);
                    $url = route('download-app') . "?token=$encrypt";
                    Models::qrCodeGenerator($url, 'shop/shop_id_' . $shop['id'] . '.png');
                    $shop->update(['qr_code' => 'shop/shop_id_' . $shop['id'] . '.png']);
                }
                $shop->workingHours()->delete();
                if ($request->shops) {
                    foreach ($request->shops as $item){
                        if ($item['day_id'] && $item['from'] && $item['to'] ){
                            $shop->workingHours()->create($item);
                        }
                    }
                }
                $shop->branchTypes()->sync($request->branch_type_id);
                DB::commit();
                return redirect()->route('admin.shop.index')->with('success', _trans('Done Save Data Successfully'));
            }
            return redirect()->back()->with('warning', _trans('Some failed errors'))->withInput($data);
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage())->withInput($request->all());
        }
    }

    public function destroy($id)
    {
        //
    }

    public function updateStatus(Request $request): \Illuminate\Http\JsonResponse
    {
        $shop = Shop::query()->findOrFail($request->id);
        if (!$shop) {
            return $this->success(_trans('Not Found'), status: false);
        }
        $status = !$shop->status;
        $shop->update(['status' => $status]);
        return $this->success(_trans('Done Updated Data Successfully'));
    }

    function downloadQrCode($id)
    {
        $id = Crypt::decrypt($id);
        $shop = Shop::query()->findOrFail($id);
        if ($shop->qr_code) {
            if (Storage::disk('public')->exists($shop->qr_code)) {
                return Storage::download($shop->qr_code, $shop->translate(locale())?->name . '.png');
            }
            return redirect()->back()->with('warning', _trans('Not found this qr code') . ' ' . $shop->translate(locale())?->name);
        }
        return redirect()->back()->with('warning', _trans('Please update in shop') . ' ' . $shop->translate(locale())?->name);
    }
}
