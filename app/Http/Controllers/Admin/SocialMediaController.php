<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Setting\Utility;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\SocialMediaRequest;
use App\Interfaces\UploadFile\UploadFileRepositoryInterface;
use App\Models\SocialMedia;
use App\Traits\ApiResponses;
use App\Traits\UploadFileTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SocialMediaController extends Controller
{
    use UploadFileTrait, ApiResponses;


    public function __construct()
    {
        $this->middleware(['permission:SocialMedia list,admin'])->only(['index']);
        $this->middleware(['permission:SocialMedia add,admin'])->only(['create']);
        $this->middleware(['permission:SocialMedia edit,admin'])->only(['edit']);
        $this->middleware(['permission:SocialMedia delete,admin'])->only(['destroy']);
    }

    public function index(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $data = SocialMedia::query()->orderByDesc('created_at');
            $data = SocialMedia::allSocialMedia($data);
            $columns = [
                'all' => _trans('All'),
                'slug' => _trans('Social Media name'),
            ];
            return view('admin.social-media.index', compact('data', 'columns'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }


    }

    public function create(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $edit = false;
            return view('admin.social-media.form', compact('edit'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }


    public function store(SocialMediaRequest $request): \Illuminate\Http\RedirectResponse
    {
        try {
            DB::beginTransaction();
            $data = $request->validated();
            if ($request->hasFile('icon')) {
                $data['icon'] = $this->upload([
                    'file' => 'icon',
                    'path' => 'social_media',
                    'upload_type' => 'single',
                    'delete_file' => ''
                ]);
            }
            $socialMedia = SocialMedia::query()->create($data);
            if ($socialMedia) {
                DB::commit();
                return redirect()->route('admin.social-media.index')->with('success', _trans('Done Save Data Successfully'));
            }
            \Storage::delete($data['icon']);
            return redirect()->back()->with('warning', _trans('Some failed errors'))->withInput($data);
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage())
                ->withInput($request->all());
        }
    }


    public function show(SocialMedia $socialProvider)
    {
        //
    }


    public function edit($id): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $edit = true;
            $socialMedia = SocialMedia::query()->findOrFail($id);
            return view('admin.social-media.form', compact('edit', 'socialMedia'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }


    public function update(SocialMediaRequest $request, $id): \Illuminate\Http\RedirectResponse
    {
        DB::beginTransaction();
        $socialMedia = SocialMedia::query()->findOrFail($id);
        $data = $request->validated();
        if ($request->hasFile('icon')) {
            $data['icon'] = $this->upload([
                'file' => 'icon',
                'path' => 'social_media',
                'upload_type' => 'single',
                'delete_file' => $socialMedia->icon ?? ''
            ]);
        }
        try {
            if ($socialMedia->update($data)) {
                DB::commit();
                return redirect()->route('admin.social-media.index')->with('success', _trans('Done Updated Data Successfully'));
            }
            \Storage::delete($data['icon']);
            return redirect()->back()->with('warning', _trans('Some failed errors'))->withInput($data);
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage())
                ->withInput($request->all());
        }
    }


    public function destroy($id): \Illuminate\Http\RedirectResponse
    {
        try {
            $socialMedia = SocialMedia::query()->findOrFail($id);
            $status = !$socialMedia->status;
            if ($socialMedia->update(['status' => $status])) {
                return redirect()->route('admin.social-media.index')->with('success', _trans('Done Updated Data Successfully'));
            }
            /*if ($socialMedia->delete()) {
                return redirect()->route('admin.social-media.index')->with('success', _trans('Done Deleted Data Successfully'));
            }*/
            return redirect()->back()->with('warning', _trans('Some failed errors'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }


    public function updateStatus(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            DB::beginTransaction();
            $socialMedia = SocialMedia::query()->findOrFail($request->id);
            $status = !$socialMedia->status;
            if ($socialMedia->update(['status' => $status])) {
                DB::commit();
                return $this->apiResponse(_trans('Done Updated Data Successfully'), '');
            }
            return $this->apiResponse('', _trans('Some failed errors'), false);
        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->apiResponse('', $exception->getMessage(), false);
        }
    }
}
