<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\VehicleTypeRequest;
use App\Models\Currency;
use App\Models\VehicleType;
use App\Traits\ApiResponses;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VehicleTypesController extends Controller
{
    use ApiResponses;
    public function __construct()
    {
        $this->middleware(['permission:VehicleType list,admin'])->only(['index']);
        $this->middleware(['permission:VehicleType add,admin'])->only(['create', 'store']);
        $this->middleware(['permission:VehicleType edit,admin'])->only(['edit', 'update', 'updateStatus']);
        $this->middleware(['permission:VehicleType delete,admin'])->only(['destroy']);
    }

    public function index(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $data = VehicleType::allVehicleTypes();
            $columns = [
                'all' => _trans('All'),
                'code' => _trans('VehicleType Code'),
                'name' => _trans('VehicleType name'),
            ];
            return view('admin.vehicle-type.index', compact('data', 'columns'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function create(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $edit = false;
            $currencies = Currency::query()->status()->get();
            return view('admin.vehicle-type.form', compact('edit', 'currencies'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function store(VehicleTypeRequest $request): \Illuminate\Http\RedirectResponse
    {
        try {
            DB::beginTransaction();
            $data = $request->validated();
            $vehicleType = VehicleType::query()->create($data);
            if ($vehicleType) {
                DB::commit();
                return redirect()->route('admin.vehicle-type.index')->with('success', _trans('Done Save Data Successfully'));
            }
            return redirect()->back()->with('warning', _trans('Some failed errors'))->withInput($data);
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage())
                ->withInput($request->all());
        }
    }

    public function show(VehicleType $vehicleType)
    {
        //
    }

    public function edit(VehicleType $vehicleType): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $edit = true;
            $currencies = Currency::query()->status()->get();
            return view('admin.vehicle-type.form', compact('edit', 'currencies', 'vehicleType'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function update(VehicleTypeRequest $request, VehicleType $vehicleType): \Illuminate\Http\RedirectResponse
    {
        try {
            DB::beginTransaction();
            $data = $request->validated();
            if ($vehicleType->update($data)) {
                DB::commit();
                return redirect()->route('admin.vehicle-type.index')->with('success', _trans('Done Updated Data Successfully'));
            }
            return redirect()->back()->with('warning', _trans('Some failed errors'))->withInput($data);
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage())
                ->withInput($request->all());
        }
    }


    public function destroy(VehicleType $vehicleType): \Illuminate\Http\RedirectResponse
    {
        try {
            $status = !$vehicleType->status;
            if ($vehicleType->update(['status' => $status])) {
                return redirect()->route('admin.vehicle-type.index')->with('success', _trans('Done Updated Data Successfully'));
            }
            /*if ($vehicleType->delete()) {
                return redirect()->route('admin.vehicle-type.index')->with('success', _trans('Done Deleted Data Successfully'));
            }*/
            return redirect()->back()->with('warning', _trans('Some failed errors'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }


    public function updateStatus(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            DB::beginTransaction();
            $vehicleType = VehicleType::query()->findOrFail($request->id);
            if (!$vehicleType) {
                return $this->success(_trans('Not Found'), false);
            }
            $status = !$vehicleType->status;
            if ($vehicleType->update(['status' => $status])) {
                DB::commit();
                return $this->success(_trans('Done Updated Data Successfully'), '');
            }
            return $this->success('', _trans('Some failed errors'), false);
        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->success('', $exception->getMessage(), false);
        }
    }
}
