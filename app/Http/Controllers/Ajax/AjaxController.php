<?php

namespace App\Http\Controllers\Ajax;

use App\Enums\OrderStatus;
use App\Enums\OwnerType;
use App\Enums\Status;
use App\Enums\StatusPay;
use App\Helpers\CPU\Models;
use App\Http\Controllers\Controller;
use App\Http\Resources\Category\CategoryResource;
use App\Http\Resources\Customer\CustomerResource;
use App\Http\Resources\Governorate\GovernorateResource;
use App\Http\Resources\Offer\OfferResource;
use App\Http\Resources\Owner\OwnerResource;
use App\Http\Resources\Product\ProductResource;
use App\Http\Resources\Category\SubcategoryResource;
use App\Http\Resources\Region\RegionResource;
use App\Http\Resources\Role\RoleResource;
use App\Http\Resources\Shop\ShopResource;
use App\Http\Resources\Save\SaveResource;
use App\Models\Areas;
use App\Models\Category;
use App\Models\Driver;
use App\Models\File;
use App\Models\Offer;
use App\Models\Order;
use App\Models\Owner;
use App\Models\Product;
use App\Models\Save;
use App\Models\Shop;
use App\Models\User;
use App\Traits\ApiResponses;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;

class AjaxController extends Controller
{
    use ApiResponses;

    public function governorates(Request $request): \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
    {
        try {
            $governorates = Models::governorates($request->country_id);
            return $this->success(GovernorateResource::collection($governorates));
        } catch (\Exception $exception) {
            return $this->success($exception->getMessage(), status: false);
        }

    }
    public function deleteProductImage(Request $request): \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
    {
        try {
            if ($request->ajax()) {
                File::findOrFail($request->image_id)->delete();
                return $this->success(1);
            }
            return redirect()->back()->with('error', _trans('Not All Access Here'));
        } catch (\Exception $exception) {
            return $this->success('', $exception->getMessage(), false);
        }

    }

    public function regions(Request $request): \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
    {
        try {
            $governorates = Models::regions($request->governorate_id);
            return $this->success(RegionResource::collection($governorates));
        } catch (\Exception $exception) {
            return $this->success($exception->getMessage(), status: false);
        }

    }

    public function shops(Request $request): \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
    {
        if ($request->ajax()) {
            $shops = Models::shops(Status::Active, $request->owner_id);
            return $this->success(ShopResource::collection($shops));
        }
        return redirect()->back()->with('error', _trans('Not All Access Here'));
    }

    public function roles(Request $request): \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
    {
        if ($request->ajax()) {
            $roles = Role::query()->select(['id', 'name', 'created_by'])->where([
                'created_by' => $request->owner_id,
                'guard_name' => 'web'
            ])->orderByDesc('created_at')->get();
            return $this->success($roles);
        }
        return redirect()->back()->with('error', _trans('Not All Access Here'));
    }


    public function products(Request $request): \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
    {
        if ($request->ajax()) {
            $products = Models::products($request->shop_id);
            return $this->success($products);
        }
        return redirect()->back()->with('error', _trans('Not All Access Here'));
    }

    public function transactions(Request $request): \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
    {
        try {
            if ($request->ajax()) {
                $shop = Shop::query()->findOrFail($request->shop_id);
                $exist = Transaction::query()->where('shop_id', $request->shop_id)
                    ->whereBetween('start', [$request->start, $request->end])
                    ->orWhereBetween('end', [$request->start, $request->end])
                    ->orWhereBetween('end', [$request->start, $request->end])
                    ->orWhere(function ($q) use ($request) {
                        $q->where('start', '<=', $request->start)
                            ->where('end', '>=', $request->end);
                    })
                    ->get();
                if (count($exist) > 0) {
                    return $this->success(_trans('This period includes a pre-existing date'), status: false);
                }
                if ($shop->subscription_type == 'percent') {
                    $orders = Order::query()->where('shop_id', $request->restaurant_id)
                        ->where('status_pay', 'paid')
                        ->where('created_at', '>=', $request->start)
                        ->where('created_at', '<=', Carbon::parse($request->end)->addDay()->format('Y-m-d'))->get();
                    $orders_total_price = $orders->sum('total_price') - $orders->sum('coupon_price');
                    $amount = ($shop->subscription_value / 100) * $orders_total_price;

                }
                if ($shop->subscription_type == 'participation') {
                    $end = Carbon::parse($request->end);
                    $diff_days = $end->diffInDays(Carbon::parse($request->start));
                    $amount = ($diff_days / 30) * $shop->subscription_value;
                }
                $amount = ceil($amount);
                return $this->success($amount);
            }
            return redirect()->back()->with('error', _trans('Not All Access Here'));
        } catch (\Exception $exception) {
            return $this->success($exception->getMessage(), status: false);
        }

    }

    public function offers(Request $request): \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
    {
        try {
            if ($request->ajax()) {

                $offers = Offer::query()->where('shop_id', '=', $request->restaurant_id)->get();
                return $this->success(OfferResource::collection($offers));
            }
            return redirect()->back()->with('error', _trans('Not All Access Here'));
        } catch (\Exception $exception) {
            return $this->success($exception->getMessage(), status: false);
        }
    }

    public function saves(Request $request): \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
    {
        try {
            if ($request->ajax()) {

                $saves = Save::query()->where('shop_id', '=', $request->shop_id)->get();
                return $this->success(SaveResource::collection($saves));
            }
            return redirect()->back()->with('error', _trans('Not All Access Here'));
        } catch (\Exception $exception) {
            return $this->success($exception->getMessage(), status: false);
        }
    }

    public function getProductDetails(Request $request): \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
    {
        if ($request->ajax()) {
            $product = Product::query()->findOrFail($request->product_id);
            return $this->success($product);
        }
        return redirect()->back()->with('error', _trans('Not All Access Here'));
    }

    public function subCategories(Request $request): \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
    {
        if ($request->ajax()) {
            $subCategories = Models::subcategories($request->category_id);
            return $this->success(SubcategoryResource::collection($subCategories));
        }
        return redirect()->back()->with('error', _trans('Not All Access Here'));
    }

    public function getResourceData(Request $request): \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
    {
        try {
            if ($request->ajax()) {
                if ($request->resource == 'Product') {
                    $products = Product::query()->whereStatus(Status::Active->value)->get();
                    return $this->success(ProductResource::collection($products));
                }

                if ($request->resource == 'Shop') {
                    $shops = Shop::query()->whereStatus(Status::Active->value)->get();
                    return $this->success(ShopResource::collection($shops));
                }
                if ($request->resource == 'Category') {
                    $categories = Category::query()->whereStatus(Status::Active->value)->get();
                    return $this->success(CategoryResource::collection($categories));
                }
            }
            return redirect()->back()->with('error', _trans('Not All Access Here'));
        } catch (\Exception $exception) {
            return $this->success($exception->getMessage(), status: false);
        }

    }


    public function customerTokens()
    {
        $customers = User::query()->has('userable.fcmTokens')->orderByDesc('created_at')->get();

        return $this->success(CustomerResource::collection($customers));
    }

    public function owners()
    {
        $owners = Owner::query()->has('user')->orderByDesc('created_at')->get();
        return $this->success(OwnerResource::collection($owners));
    }
    public function shop(Request $request): \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
    {
        if ($request->ajax()) {
            $shop = Shop::query()->with(['country', 'governorate', 'region'])
                ->where([
                    ['owner_id', '=', $request->owner_id],
                    ['id', '=', $request->shop_id],
                ])
                ->status(1)->first();
            if (!empty($shop)) {
                return $this->success(ShopResource::make($shop), '');
            }
            return $this->apiResponse('', 'Not found', false);
        }
        return redirect()->back()->with('error', _trans('Not All Access Here'));
    }
    public function role(Request $request): \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
    {
        if ($request->ajax()) {
            $role = Role::query()
                ->where('created_by',$request->owner_id)
                ->where('guard_name', '=', 'web')
                ->orderByDesc('created_at')->get();
            if (!empty($role)) {
                return $this->success(RoleResource::make($role), '');
            }
            return $this->success('', 'Not found', false);
        }
        return redirect()->back()->with('error', _trans('Not All Access Here'));
    }


    public
    function deleteArea(Request $request)
    {
        try {
            $area = Areas::query()->find($request->id);
            if ($area) {
                $area->regions()->delete();
                $area->delete();
                return $this->success(_trans('Done delete data successfully'));
            }
            return $this->success(_trans('Not found'), status: false);
        } catch (\Exception $exception) {
            return $this->success($exception->getMessage(), status: false);
        }
    }


}
