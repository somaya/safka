<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\AddressRequest;
use App\Http\Resources\Address\AddressResource;
use App\Models\Customer;
use App\Models\Address;
use App\Traits\ApiResponses;
use Illuminate\Http\Request;


class AddressController extends Controller
{
    use ApiResponses;

    private function customer($request)
    {
        return Customer::query()->whereHas('user', function ($query) use ($request) {
            $query->where('userable_type', Customer::class)
                ->where('id', $request->user()->id);
        })->first();
    }

    public function index(Request $request)
    {
        try {
            $customer = $this->customer($request);
            if ($customer) {
                $addresses = Address::query()->with(['country', 'governorate', 'region'])
                    ->whereCustomerId($customer->id)->orderByDesc('created_at')->get();
                return $this->success(AddressResource::collection($addresses), message: _trans('List address customer'));
            }
            return $this->failure(_trans('Not found this customer'));
        } catch (\Exception $exception) {
            return $this->exception($exception);
        }
    }

    public function store(AddressRequest $request)
    {
        try {
            $customer = $this->customer($request);
            if ($customer) {
                $data = $request->validated();
                $data['customer_id'] = $customer->id;
                $address = Address::query()->create($data);
                if ($address) {
                    return $this->success(message: _trans('Done save address customer'));
                }
                return $this->failure(_trans('please try again'));
            }
            return $this->failure(_trans('Not found this customer'));
        } catch (\Exception $exception) {
            return $this->exception($exception);
        }
    }

    public function update(AddressRequest $request)
    {
        try {
            $customer = $this->customer($request);
            if ($customer) {
                $data = $request->validated();
                $address = Address::query()->whereCustomerId($customer->id)->find($request->address_id);
                if ($address) {
                    if ($address->update($data)) {
                        return $this->success(message: _trans('Done save address customer'));
                    }
                    return $this->failure(_trans('please try again'));
                }
                return $this->failure(_trans('Not found this address'));

            }
            return $this->failure(_trans('Not found this customer'));
        } catch (\Exception $exception) {
            return $this->exception($exception);
        }
    }

    public function show(Request $request)
    {
        try {
            $customer = $this->customer($request);
            if ($customer) {
                $address = Address::query()->with(['country', 'governorate', 'region'])->whereCustomerId($customer->id)
                    ->find($request->address_id);
                if ($address) {
                    return $this->success(AddressResource::make($address), message: _trans('show address customer'));
                }
                return $this->failure(_trans('Not found this address'));
            }
            return $this->failure(_trans('Not found this customer'));
        } catch (\Exception $exception) {
            return $this->exception($exception);
        }
    }

    public function delete(Request $request)
    {
        try {
            $customer = $this->customer($request);
            if ($customer) {
                $address = Address::query()->whereCustomerId($customer->id)->find($request->address_id);
                if ($address) {
                    $address->delete();
                    return $this->success(message: _trans('address customer deleted successfully'));
                }
                return $this->failure(_trans('Not found this address'));
            }
            return $this->failure(_trans('Not found this customer'));
        } catch (\Exception $exception) {
            return $this->exception($exception);
        }
    }

}
