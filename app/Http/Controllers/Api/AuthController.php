<?php

namespace App\Http\Controllers\Api;

use App\Events\SendNotificationUser;
use App\Helpers\Setting\Utility;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\AuthRequest;
use App\Http\Requests\Api\LoginRequest;
use App\Http\Resources\Customer\CustomerResource;
use App\Http\Resources\Notification\NotificationDetailResource;
use App\Interfaces\Auth\AuthRepositoryInterface;
use App\Interfaces\UploadFile\UploadFileRepositoryInterface;
use App\Mail\ResetPassword;
use App\Models\Customer;
use App\Models\Notification;
use App\Models\NotificationToken;
use App\Models\User;
use App\Models\VerificationCode;
use App\Traits\ApiResponses;
use App\Traits\UploadFileTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Propaganistas\LaravelPhone\PhoneNumber;

class AuthController extends Controller
{
    use UploadFileTrait, ApiResponses;

    private AuthRepositoryInterface $authRepositoryInterface;

    public function __construct( AuthRepositoryInterface $authRepositoryInterface)
    {
        $this->authRepositoryInterface = $authRepositoryInterface;
    }

    public function sendVerificationCode(AuthRequest $request): \Illuminate\Http\JsonResponse
    {
        //Generate random code
        $code = random_int(100000, 999999);

        //Get message
        $message = _trans('Your confirmation code is: ') . $code;

        //Store code on storage or get it if exist
        $user = User::query()->firstOrCreate(['phone' => $request['phone']]);
        $verification_code = new SmsVerificationCode();

        $verification_code->code = $code;

        DB::transaction(function () use ($user, $verification_code) {
            $user->verificationCodes()->delete();
            $user->verificationCodes()->save($verification_code);
            Customer::query()->updateOrCreate([
                'user_id' => $user->id
            ], [
                'user_id' => $user->id
            ]);
        });

        //Send message to customer phone numbers
        //TODO temporary
        return $this->success($code);
        /*return (new SmsService)
            ->receiver($phone)
            ->message($message)
            ->send();*/
    }

    public function register(AuthRequest $request)
    {
        try {
            DB::beginTransaction();
            $userData = $request->only(['first_name','last_name', 'email', 'password', 'avatar', 'lang', 'country_id', 'governorate_id', 'region_id',]);
            $userData['name']=$request->first_name .' '. $request->last_name;
            $customerData = $request->only(['facility_name','branch_type','address','lat','lng', 'gender','phone', 'birthday']);
            $customerData['phone'] = (string)PhoneNumber::make($request->phone, $request->country_code);

            if ($request->hasFile('avatar')) {
                $userData['avatar'] = $this->upload([
                    'file' => 'avatar',
                    'path' => 'customer',
                    'upload_type' => 'single',
                    'delete_file' => ''
                ]);
            }
            $customer = Customer::query()->create($customerData);
            if ($customer) {
                $user = $customer->user()->create($userData);
                $user['token'] = $user->createToken($user->name)->plainTextToken;
                DB::commit();
                return $this->success(CustomerResource::make($user), message: _trans('Done register successfully'));
            }
            return $this->failure(message: _trans('Please register again'));
        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->exception($exception);
        }
    }

    public function login(LoginRequest $request)
    {
        /* login from social */
        $credentials = $request->getCredentials();
        if ($request->country_code && Arr::exists($credentials, 'phone'))
        {
            $phone=(string)PhoneNumber::make($credentials['phone'], $request->country_code);
            $user = Customer::query()->with(['user'])->wherePhone($phone)->first();
            if (!$user) {
                return $this->failure(_trans('Phone is not valid'));
            }
            /* is account blocked ==> remove account */
            if ($user->blocked) {
                return $this->failure(_trans('Not found this account please register first'));
            }
            $user = $user->user;
            if (!$user->status) {
                return $this->failure(_trans('This Account is blocked please call support'));
            }
            if (!Hash::check($request->password, $user->password)) {
                return $this->failure(_trans('Password is not correct'));
            }
        } else {
            if (!Auth::validate($credentials)) {
                return $this->failure(_trans('Phone is not valid'));
            }
            $user = Auth::getProvider()->retrieveByCredentials($credentials);
        }

        if (!$user->status) {
            $this->authRepositoryInterface->logout('web');
            $user->tokens()->delete();
            return $this->failure(_trans('This Account is blocked please call support'));
        }
        /* is account blocked ==> remove account */
        if ($user->userable->blocked) {
            $user->tokens()->delete();
            return $this->failure(_trans('Not found this account please register first'));
        }
        $user->tokens()->delete();
        $user['token'] = $user->createToken($user->name)->plainTextToken;

        return $this->success(CustomerResource::make($user), message: _trans('Done Login successfully'));
    }


    private function socialUser($request)
    {
        return Customer::query()->with(['user'])->where([
            ['provider_type', '=', $request['provider_type']],
            ['provider_id', '=', $request['provider_id']],
        ])->first();
    }

    public function checkSocial(AuthRequest $request)
    {
        try {
            $customer = $this->socialUser($request);
            if ($customer) {
                $user = $customer->user;
                $user->tokens()->delete();
                $user['token'] = $user->createToken($user->name)->plainTextToken;
                return $this->success(CustomerResource::make($user), message: _trans('Done login social successfully'));
            }
            return $this->failure(_trans('Please get data from provider name ') . $request['provider_type']);
        } catch (\Exception $exception) {
            return $this->exception($exception);
        }
    }

    public function loginSocial(AuthRequest $request)
    {
        try {
            DB::beginTransaction();
            $customer = $this->socialUser($request);
            if ($customer) {
                $user = $customer->user;
                if (!$user->status) {
                    $this->authRepositoryInterface->logout('web');
                    $user->tokens()->delete();
                    return $this->failure(message: _trans('This Account Is Blocked'));

                }
                $user->tokens()->delete();
                $user['token'] = $user->createToken($user->name)->plainTextToken;
                DB::commit();
                return $this->success(CustomerResource::make($user), message: _trans('Done login social successfully'));
            }
            $data = $request->only(['first_name','last_name', 'email']);
            $data['name']=$request->first_name .' '.$request->last_name;
//            $user = User::query()->whereEmail($request->email)->first();
//            if ($user) {
//                $user->tokens()->delete();
//                $user['token'] = $user->createToken($user->name)->plainTextToken;
//                DB::commit();
//                return $this->success(CustomerResource::make($user), message: _trans('Done login social successfully'));
//            }
            $customer = Customer::query()->create([
                'provider_type' => $request['provider_type'],
                'provider_id' => $request['provider_id'],
            ]);
            $user = $customer->user()->create($data);
            $user['token'] = $user->createToken($user->name)->plainTextToken;

            if ($request->fcm_token) {
                NotificationToken::query()->create([
                    'tokenable_type' => Customer::class,
                    'tokenable_id' => $customer->id,
                    'fcm_token' => $request['fcm_token'],
                    'lang' => $request->header('Accept-Language', default_lang())
                ]);
            }
            DB::commit();
            return $this->success(CustomerResource::make($user), message: _trans('Done login social successfully'));
        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->exception($exception);
        }
    }

    public function forgetPasswordWeb(AuthRequest $request)
    {
        $user = User::query()->where([
            'email' => $request->username,
        ])->first();
        if (!$user) {
            return $this->failure(_trans('Not found this user'));
        }
        $token = Str::random(70);
        try {
            DB::beginTransaction();
            $user->tokens()->delete();
            DB::table('password_resets')->updateOrInsert([
                'email' => $user->email,
            ], [
                'email' => $user->email,
                'token' => $token,
                'created_at' => Carbon::now()
            ]);
            $user['url'] = route('user.reset', $token);
            Mail::to($user->email)->send(new ResetPassword(['data' => $user, 'token' => $token]));
            DB::commit();
            return $this->success(message: _trans('Done send rest link, please check your email address'));
        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->exception($exception);
        }
    }

    public function forgetPassword(AuthRequest $request)
    {
        $validated = $request->validated();
        $code = random_int(100000, 999999);
        $message = __('auth.sms_message_confirmation', ['code' => $code]);
        $username = $validated['username'];
        if ($request->country_code) {
            $username = (string)PhoneNumber::make($validated['username'], $request->country_code);
        }
        $user = User::findByEmailOrPhone($username);
        if (!$user) {
            return $this->failure(_trans('Not found this customer'));
        }
        $verificationCode = new VerificationCode();
        $verificationCode->code = $code;

        DB::transaction(function () use ($user, $verificationCode) {
            $user->tokens()->delete();
            $user->verificationCode()->delete();
            $user->verificationCode()->save($verificationCode);
        });

        $fcmToken = $user->userable->fcmTokens->pluck('fcm_token')->toArray();
        event(new SendNotificationUser([
            'notification_type' => 'forget_password',
            'code' => $code,
        ], $fcmToken, _trans('Done send code verification'), _trans('Done send code verification')));
        return $this->success(['code' => $code], message: $message);
    }

    public function confirm(AuthRequest $request): \Illuminate\Http\JsonResponse
    {
        //Check if verification code correct and mobile is exists
        $username = $request['username'];
        if ($request->country_code) {
            $username = (string)PhoneNumber::make($request['username'], $request->country_code);
        }
        $user = User::findByEmailOrPhone($username);
        if (!$user) {
            return $this->failure(_trans('Not found this customer'));
        }
        $verificationCode = $user->verificationCode()->where('created_at', ">=", Carbon::now()->subMinutes(20))->first();

        if (!$verificationCode || $verificationCode['code'] != $request->code) {
            return $this->failure(_trans('The provided pin is incorrect.'));
        }
        $verificationCode->token = hash('sha256', $plainTextToken = Str::random(40));

        $user->verificationCode()->save($verificationCode);

        $token = $verificationCode->id . '|' . $plainTextToken;

        return $this->success([
            'token' => $token
        ]);

    }

    public function resetPassword(AuthRequest $request): \Illuminate\Http\JsonResponse
    {
        $instance = VerificationCode::findToken($request->token);
        if (!$instance) {
            return $this->failure("Unauthorized.");
        }
        $user = $instance->modelable;
        DB::transaction(function () use ($user, $instance, $request) {
            $user->update(['password' => $request->password]);
            $user->tokens()->delete();
            $instance->delete();
        });
        return $this->success(message: _trans("Password changed successfully"));
    }

    public function profile()
    {
        try {
            return $this->success(CustomerResource::make(auth()->user()));
        } catch (\Exception $exception) {
            return $this->exception($exception);
        }
    }

    public function firebaseToken(AuthRequest $request)
    {
        try {
            $notificationToken = NotificationToken::query()
                ->updateOrCreate([
                    'tokenable_type' => Customer::class,
                    'tokenable_id' => $request->user()->userable->id,
                    'fcm_token' => $request->fcm_token
                ], [
                    'tokenable_type' => Customer::class,
                    'tokenable_id' => $request->user()->userable->id,
                    'fcm_token' => $request->fcm_token,
                    'device_name' => $request->device_name,
                    'last_used_at' => now(),
                    'lang' => $request->header('Accept-Language', default_lang()),
                ]);
            if ($notificationToken)
                return $this->success(message: _trans('Done save token successfully'));
            return $this->success(message: _trans('Please try again'));
        } catch (\Exception $exception) {
            return $this->exception($exception);
        }
    }

    public function updateProfile(AuthRequest $request)
    {
        try {
            DB::beginTransaction();
            $user = auth()->user();
            $userData = $request->only(['name', 'email', 'avatar', 'country_id', 'governorate_id', 'region_id']);
            $customerData = $request->only(['facility_name','branch_type','address','lat','lng', 'gender', 'phone','birthday']);
            $customerData['phone'] = (string)PhoneNumber::make($request->phone, $request->country_code);
            if ($request->hasFile('avatar')) {
                $userData['avatar'] = $this->upload([
                    'file' => 'avatar',
                    'path' => 'customer',
                    'upload_type' => 'single',
                    'delete_file' =>$user->avatar ?? ''
                ]);
            }
            if ($user->update($userData)) {
                $user->userable()->update($customerData);
                DB::commit();
                return $this->success(CustomerResource::make($user), message: _trans('Done updated profile successfully'));
            }
            return $this->failure(message: _trans('Please register again'));
        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->exception($exception);
        }
    }

    public function updatePassword(AuthRequest $request)
    {
        try {
            $user = auth()->user();

            if ($this->authRepositoryInterface->changePassword($user, $request)) {
                return $this->success(message: _trans('Password changed successfully'));
            }
            return $this->failure(message: _trans('Please make sure your old password correct'));

        } catch (\Exception $exception) {
            return $this->exception($exception);
        }
    }


    public function logout(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            DB::beginTransaction();
            $this->authRepositoryInterface->logout('web');
            $request->user()->currentAccessToken()->delete();
            NotificationToken::query()->where([
                'tokenable_type' => Customer::class,
                'tokenable_id' => $request->user()->userable->id,
            ])->delete();
            DB::commit();
            return $this->success(message: _trans('Logged out successfully.'));
        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->exception($exception);
        }

    }

    public function changeLanguage(AuthRequest $request): \Illuminate\Http\JsonResponse
    {
        try {
            $request->user()->update(['lang' => $request->code]);
            return $this->success(message: _trans('Done Update language'));
        } catch (\Exception $exception) {
            return $this->exception($exception);
        }
    }
    public function deleteAccount()
    {
        $user = auth()->user();
        $user->update(['status' => 0]);
        $user->userable->update(['blocked'=>1]);
        $user->tokens()->delete();
        $user->currentAccessToken()->delete();
        return $this->success(message: _trans('Done remove account successfully'));
    }

    public function notifications()
    {
        $customer = auth()->user()->userable;
        $notifications = Notification::query()->where([
            'receiverable_type' => Customer::class,
            'receiverable_id' => $customer->id,
        ])->whereIn('type', ['shop', 'global', 'coupon'])
            ->orderByDesc('created_at')
            ->paginate(Utility::getValByName('pagination_limit'));

        return $this->success(NotificationDetailResource::collection($notifications), $notifications);
    }

    public function readNotification()
    {
        $customer = auth()->user()->userable;
        Notification::query()->where([
            'receiverable_type' => Customer::class,
            'receiverable_id' => $customer->id,
        ])->whereIn('type', ['shop', 'global', 'coupon'])
            ->whereNull('read_at')->update(['read_at' => now()]);
        return $this->success(_trans('Done read all notifications'));
    }
}
