<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\BannerRequest;
use App\Http\Resources\Banner\BannerResource;
use App\Models\Banner;
use App\Traits\ApiResponses;
use Illuminate\Http\Request;

class BannerController extends Controller
{
    use  ApiResponses;
    public function banners()
    {
        try {
            $banners= Banner::query()->status()->get();
            return $this->success(BannerResource::collection($banners), message: _trans('Banners List'));

        } catch (\Exception $exception) {
            return $this->exception($exception);
        }

    }
}
