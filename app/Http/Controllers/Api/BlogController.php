<?php

namespace App\Http\Controllers\Api;

use App\Helpers\Setting\Utility;
use App\Http\Controllers\Controller;
use App\Http\Resources\Blog\BlogCategoryResource;
use App\Http\Resources\Blog\BlogDetailedResource;
use App\Http\Resources\Blog\BlogResource;
use App\Models\Blog;
use App\Models\BlogCategory;
use App\Traits\ApiResponses;

class BlogController extends Controller
{
    use ApiResponses;

    public function blogs()
    {
        $blogs = Blog::query()->with(['category'])->orderByDesc('created_at')->status()->paginate(Utility::getValByName('pagination_limit'));
        return $this->success(BlogResource::collection($blogs), $blogs, message: _trans('List of blogs'));
    }
    public function blogsByCateory($slug)
    {
        $category=BlogCategory::query()
            ->whereTranslation('slug',$slug)->orWhere('id',$slug)->first();
        $blogs = Blog::query()->where('category_id',$category->id)
            ->orderByDesc('created_at')->status()->paginate(Utility::getValByName('pagination_limit'));
        return $this->success(BlogResource::collection($blogs), $blogs, message: _trans('List of blogs'));
    }

    public function blogCategory()
    {
        $categories = BlogCategory::query()->status()->withCount(['blogs'])->get();
        return $this->success(BlogCategoryResource::collection($categories), message: _trans('List of Blog Categories'));
    }

    public function blogDetails($slug)
    {
        $blog = Blog::query()
            ->with(['category', 'images', 'tags'])
            ->whereTranslation('slug',$slug)->orWhere('id',$slug)->first();
        if ($blog) {
            $latest = Blog::query()->latest()->where('id', '!=', $blog->id)->status()->get();
            return $this->success([
                'blog' => BlogDetailedResource::make($blog),
                'latest' => BlogResource::collection($latest)
            ], message: _trans('Blog Details'));
        }
        return $this->failure(_trans('Blog Not Found'));

    }

}
