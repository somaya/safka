<?php

namespace App\Http\Controllers\Api;

use App\Enums\CouponType;
use App\Enums\DiscountTypeCoupon;
use App\Enums\Status;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Cart\CouponRequest;
use App\Http\Requests\Api\Cart\ProductRequest;
use App\Http\Requests\Api\Cart\RemoveModelTypeRequest;
use App\Http\Requests\Api\Cart\SelectLocationRequest;
use App\Http\Requests\Api\CartRequest;
use App\Http\Resources\Cart\CartResource;
use App\Http\Resources\Location\LocationResource;
use App\Models\Areas;
use App\Models\Cart;
use App\Models\CartDetail;
use App\Models\Color;
use App\Models\Coupon;
use App\Models\Customer;
use App\Models\Discount;
use App\Models\Location;
use App\Models\Offer;
use App\Models\Product;
use App\Models\Save;
use App\Models\Shop;
use App\Models\Size;
use App\Models\UserCoupon;
use App\Traits\ApiResponses;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CartController extends Controller
{
    use ApiResponses;

    public function customer($request)
    {
        return Customer::query()->whereHas('user', function ($query) use ($request) {
            $query->where('userable_type', Customer::class)
                ->where('id', $request->user()->id);
        })->first();
    }

    public function carts($customerId)
    {
        return Cart::query()->with(['cartDetails', 'shop'])
            ->whereRelation('shop', ['country_id'=>request('country_id'),'status'=>Status::Active->value])
            ->whereCustomerId($customerId);
    }

    public function index(Request $request)
    {
        try {
            $customer = $this->customer($request);
            if ($customer) {
                $carts = $this->carts($customer->id)->with(['shop', 'area'])->orderByDesc('created_at')->get();
                return $this->success(CartResource::collection($carts), message: _trans('List cart customer'));
            }
            return $this->failure(_trans('Not found this customer'));
        } catch (\Exception $exception) {
            return $this->exception($exception);
        }
    }

    private function onData($request, $cart)
    {
        $cartDetailData = $request->only('qty');
        $cartDetailData['cart_id'] = $cart->id;
        $extraInfo = [];
        $extras = $request->extras;
        $totalPriceExtras = 0;
        if (!is_null($extras) && count($extras) > 0)
        {
            foreach ($extras as $extra) {
                $price = Extra::query()->find($extra['id'])->price;
                $extraInfo[] = [
                    'id' => $extra['id'],
                    'qty' => $extra['qty'],
                    'price' => $price,
                    'total_price' => $price * $extra['qty'],
                ];
                $totalPriceExtras += $price * $extra['qty'];
                $extraIds[] = $extra['id'];
            }
            //is added extra data
            $cartDetailData['extra_info'] = $extraInfo;
            $cartDetailData['total_price_extras'] = $totalPriceExtras;
        } else {
            $cartDetailData['extra_info'] = NULL;
            $cartDetailData['total_price_extras'] = $totalPriceExtras;
        }

        // product or product size
        if ($request->model_type == 'product') {
            $cartDetailData['modelable_type'] = Product::class;
            $cartDetailData['modelable_id'] = $request->model_id;
            $cartDetailData['qty'] = $request->qty;
            $product = Product::query()->find($cartDetailData['modelable_id']);
            $cartDetailData['price'] = $product->price;
            if (!is_null($request->size_id)) {
                $size = Size::query()->find($request->size_id);
                $cartDetailData['modelable_id'] = $size->product_id;
                $cartDetailData['size_id'] = $request->size_id;
                $cartDetailData['price'] = $size->price;
            }
            if (!is_null($request->color_id)) {
                $cartDetailData['color_id'] = $request->color_id;
            }
        }


        // offer or save
        if ($request->model_type == 'offer' || $request->model_type == 'save') {
            $cartDetailData['modelable_type'] = $request->model_type == 'offer' ? Offer::class : Save::class;
            $cartDetailData['modelable_id'] = $request->model_id;
            $cartDetailData['qty'] = $request->qty;
            $data = $this->offerOrSave($request, $request->model_type == 'offer' ? 'offer' : 'save');
            $cartDetailData['products'] = $data['products'];
            $cartDetailData['price'] = $data['price'];
        }

        // discount
        if ($request->model_type == 'discount') {
            $cartDetailData['modelable_type'] = Discount::class;
            $cartDetailData['modelable_id'] = $request->model_id;
            $cartDetailData['qty'] = $request->qty;
            $discount = Discount::query()->find($request->model_id);
            $cartDetailData['price'] = $discount['price_after'];
        }


        return $cartDetailData;
    }

    private function offerOrSave($request, $type)
    {
        if ($type == 'offer') {
            $data = Offer::query()->find($request->model_id);
        } else {
            $data = Save::query()->find($request->model_id);
        }
        return [
            'products' => $data->products,
            'price' => $data->price,
        ];
    }

    private function storeCart($request)
    {
        $customer = $this->customer($request);
        if ($customer) {
            $cart = $this->carts($customer->id)->first();
            if (empty($cart)) {
                $cartData = $request->only('shop_id', 'special_order');
                $cartData['customer_id'] = $customer->id;
                $cart = Cart::query()->create($cartData);
                $cartDetailData = $this->onData($request, $cart);
                $cart->cartDetails()->create($cartDetailData);
                return [
                    'cart' => $cart,
                    'status' => 'store'
                ];
            }
            return [
                'cart' => $cart,
                'status' => 'update'
            ];
        }
        return [
            'cart' => null,
            'status' => 'false'
        ];
    }

    /*product default or size */
    public function addProduct(ProductRequest $request)
    {
        try {
            DB::beginTransaction();
            $data = $this->storeCart($request);
            $cart = $data['cart'];

            if ($data['status'] == 'store') {
                DB::commit();
                $freshCart = $cart->fresh(['shop', 'cartDetails']);
                return $this->success(CartResource::make($freshCart), message: _trans('Done save product in cart successfully'));
            } elseif ($data['status'] == 'update') {
                if ($cart->shop_id == $request->shop_id) {
                    $cartDetail = $cart->cartDetails()->where([
                        ['modelable_type', '=', Product::class],
                        ['modelable_id', '=', $request->model_id],
                    ]);
                    if (!is_null($request->size_id)) {
                        $cartDetail = $cartDetail->where('size_id', '=', $request->size_id);
                    }
                    if (!is_null($request->color_id)) {
                        $cartDetail = $cartDetail->where('color_id', '=', $request->color_id);

                    }
                    $cartDetail = $cartDetail->first();
                    if (empty($cartDetail)) {
                        $cartDetailData = $this->onData($request, $cart);
                        $cart->cartDetails()->create($cartDetailData);
                        DB::commit();
                        $freshCart = $cart->fresh(['shop', 'cartDetails']);
                        return $this->success(CartResource::make($freshCart), message: _trans('Done add new product in cart successfully'));

                    } else {

                        $data = $this->onData($request, $cart);
                        $cartDetail->update($data);
                        $freshCart = $cart->fresh(['shop', 'cartDetails']);
                        DB::commit();
                        return $this->success(CartResource::make($freshCart), message: _trans('Done update qty product in cart successfully'));
                    }
                }
                return $this->failure(message: _trans('Please choose a product from the same shop as the card'));
            } else {
                return $this->failure(_trans('Not found this customer'));
            }
        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->exception($exception);
        }
    }

    /*offer and save */
    public function addOfferOrSave(CartRequest $request)
    {
        try {
            DB::beginTransaction();
            $modelName = $request->model_type == 'offer' ? 'offer' : 'save';
            $data = $this->storeCart($request);
            $cart = $data['cart'];
            if ($data['status'] == 'store') {
                DB::commit();
                $freshCart = $cart->fresh(['shop', 'cartDetails']);
                return $this->success(CartResource::make($freshCart), message: _trans('Done save ' . $modelName . ' in cart successfully'));
            } elseif ($data['status'] == 'update') {
                if ($cart->shop_id == $request->shop_id) {
                    $model = $request->model_type == 'offer' ? Offer::class : Save::class;
                    $cartDetail = $cart->cartDetails()->where([
                        ['modelable_type', '=', $model],
                        ['modelable_id', '=', $request->model_id],
                    ])->first();
                    if (empty($cartDetail)) {
                        $cartDetailData = $this->onData($request, $cart);
                        $cart->cartDetails()->create($cartDetailData);
                        DB::commit();
                        $freshCart = $cart->fresh(['shop', 'cartDetails']);
                        return $this->success(CartResource::make($freshCart), message: _trans('Done add new ' . $modelName . ' in cart successfully'));
                    } else {
                        $data = $this->onData($request, $cart);
                        $cartDetail->update($data);
                        $freshCart = $cart->fresh(['shop', 'cartDetails']);
                        DB::commit();
                        return $this->success(CartResource::make($freshCart), message: _trans('Done update qty ' . $modelName . ' in cart successfully'));
                    }
                }
                return $this->failure(message: _trans('Please choose a product from the same shop as the card'));
            }
            return $this->failure(_trans('Not found this customer'));
        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->exception($exception);
        }
    }

    /*discount */
    public function addDiscountOrExtra(CartRequest $request)
    {
        try {
            DB::beginTransaction();
            $modelName = $request->model_type == 'discount' ? 'discount' : 'extra';
            $data = $this->storeCart($request);
            $cart = $data['cart'];
            if ($data['status'] == 'store') {
                DB::commit();
                $freshCart = $cart->fresh(['shop', 'cartDetails']);
                return $this->success(CartResource::make($freshCart), message: _trans('Done save ' . $modelName . ' in cart successfully'));
            } elseif ($data['status'] == 'update') {
                if ($cart->shop_id == $request->shop_id) {
                    $model = $request->model_type == 'discount' ? Discount::class : Extra::class;
                    $cartDetail = $cart->cartDetails()->where([
                        ['modelable_type', '=', $model],
                        ['modelable_id', '=', $request->model_id],
                    ])->first();
                    if (empty($cartDetail)) {
                        $cartDetailData = $this->onData($request, $cart);
                        $cart->cartDetails()->create($cartDetailData);
                        DB::commit();
                        $freshCart = $cart->fresh(['shop', 'cartDetails']);
                        return $this->success(CartResource::make($freshCart), message: _trans('Done add new ' . $modelName . ' in cart successfully'));
                    } else {
                        $data = $this->onData($request, $cart);
                        $cartDetail->update($data);
                        $freshCart = $cart->fresh(['shop', 'cartDetails']);
                        DB::commit();
                        return $this->success(CartResource::make($freshCart), message: _trans('Done update qty ' . $modelName . ' in cart successfully'));
                    }
                }
                return $this->failure(message: _trans('Please choose a product from the same shop as the card'));
            }
            return $this->failure(_trans('Not found this customer'));
        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->exception($exception);
        }
    }

    public function removeModelType(RemoveModelTypeRequest $request)
    {
        try {
            DB::beginTransaction();
            $customer = $this->customer($request);
            if ($customer) {
                $cart = $this->carts($customer->id)->first();
                if (empty($cart)) {
                    return $this->failure(_trans('Please add to cart first'));
                } else {
                    $cartDetails = CartDetail::query()->whereCartId($cart->id)->count();
                    if ($cartDetails == 1) {
                        $cart->delete();
                        DB::commit();
                        return $this->success(message: _trans('Done remove all items from cart'));
                    }
                    $cartDetail = $cart->cartDetails();
                    if ($request->model_type == 'product') {

                        if (!empty($request->size_id))
                        {
                            $cartDetail = $cartDetail->where([
                                ['size_id', '=', $request->size_id],
                                ['modelable_type', '=', Product::class],
                                ['modelable_id', '=', $request->model_id],
                            ])->first();
                            $cartDetail->delete();
                            DB::commit();
                            $freshCart = $cart->fresh(['shop', 'cartDetails']);
                            return $this->success(CartResource::make($freshCart), message: _trans('done remove product size successfully'));
                        } elseif (!empty($request->color_id))
                        {
                            $cartDetail = $cartDetail->where([
                                ['color_id', '=', $request->color_id],
                                ['modelable_type', '=', Product::class],
                                ['modelable_id', '=', $request->model_id],
                            ])->first();
                            $cartDetail->delete();
                            DB::commit();
                            $freshCart = $cart->fresh(['shop', 'cartDetails']);
                            return $this->success(CartResource::make($freshCart), message: _trans('done remove product size successfully'));
                        }
                        else {
                            $cartDetail = $cartDetail->where([
                                ['modelable_type', '=', Product::class],
                                ['modelable_id', '=', $request->model_id],
                            ])->first();
                            $cartDetail->delete();
                            DB::commit();
                            $freshCart = $cart->fresh(['shop', 'cartDetails']);
                            return $this->success(CartResource::make($freshCart), message: _trans('done remove product successfully'));
                        }
                    }

                    if ($request->model_type == 'offer') {
                        $cartDetail = $cartDetail->where([
                            ['modelable_type', '=', Offer::class],
                            ['modelable_id', '=', $request->model_id],
                        ])->first();
                        $cartDetail->delete();
                        DB::commit();
                        $freshCart = $cart->fresh(['shop', 'cartDetails']);
                        return $this->success(CartResource::make($freshCart), message: _trans('done remove offer successfully'));
                    }

                    if ($request->model_type == 'discount') {
                        $cartDetail = $cartDetail->where([
                            ['modelable_type', '=', Discount::class],
                            ['modelable_id', '=', $request->model_id],
                        ])->first();
                        $cartDetail->delete();
                        DB::commit();
                        $freshCart = $cart->fresh(['shop', 'cartDetails']);
                        return $this->success(CartResource::make($freshCart), message: _trans('done remove discount successfully'));
                    }

                    if ($request->model_type == 'extra') {
                        $cartDetail = $cartDetail->where([
                            ['modelable_type', '=', Extra::class],
                            ['modelable_id', '=', $request->model_id],
                        ])->first();
                        $cartDetail->delete();
                        DB::commit();
                        $freshCart = $cart->fresh(['shop', 'cartDetails']);
                        return $this->success(CartResource::make($freshCart), message: _trans('done remove extra successfully'));
                    }

                    if ($request->model_type == 'save') {
                        $cartDetail = $cartDetail->where([
                            ['modelable_type', '=', Save::class],
                            ['modelable_id', '=', $request->model_id],
                        ])->first();
                        $cartDetail->delete();
                        DB::commit();
                        $freshCart = $cart->fresh(['shop', 'cartDetails']);
                        return $this->success(CartResource::make($freshCart), message: _trans('done remove save successfully'));
                    }

                    return $this->failure(message: _trans('Not found offer in carts'));
                }
            }
            return $this->failure(_trans('Not found this customer'));

        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->exception($exception);
        }
    }

    public function removeAllCart(Request $request)
    {
        try {
            $customer = $this->customer($request);
            if ($customer) {
                $carts = $this->carts($customer->id)->first();
                if (!empty($carts)) {
                    $carts->delete();
                    return $this->success([
                        'cart_count' => 0,
                        'product_count' => 0,
                        'total_price' => 0,
                    ], message: _trans('Done delete cart all'));
                } else {
                    return $this->failure(_trans('Not found cart'));
                }
            }
            return $this->failure(_trans('Not found this customer'));
        } catch (\Exception $exception) {
            return $this->exception($exception);
        }
    }

    public function selectLocation(SelectLocationRequest $request)
    {
        try {
            $customer = $this->customer($request);
            if ($customer) {
                $carts = $this->carts($customer->id)->first();
                if (!empty($carts)) {
                    $shop = Shop::query()->whereCountryId($request->country_id)->find($carts->shop_id);
                    if ($shop) {
                        $location = Location::query()->whereCountryId($request->country_id)->find($request->location_id);
                        $area = Areas::query()
                            ->whereRelation('governorate', 'country_id', '=', $request->country_id)
                            ->with(['deliveryArea'])->has('deliveryArea')
                            ->whereHas('deliveryArea', function ($query) use ($carts, $location) {
                                $query->where([
                                    'shop_id' => $carts->shop_id,
                                    'country_id' => $location->country_id,
                                ]);
                            })
                            ->where([
                                'governorate_id' => $location->governorate_id,
                            ])->whereHas('regions', function ($query) use ($location) {
                                $query->where('area_regions.region_id', $location->region_id);
                            })->first();
                        if ($area) {
                            $delivery_price = $area->delivery_price;
                            if ($carts->total_price < $area->min_order_price) {
                                return $this->failure(_trans('Order Total Price Less Than Minimum ' . $area->min_order_price));
                            }
                            $carts->update([
                                'location_id' => $location->id,
                                'area_id' => $area->id,
                                'delivery_price' => $delivery_price,
                                'location_info' => $location->toArray()
                            ]);
                            DB::commit();
                            return $this->success([
                                'delivery_price' => $delivery_price,
                                'total_price' => $carts->total_price,
                                'coupon_price' => $carts->coupon_price,
                                'amount' => $delivery_price + $carts->total_price - $carts->coupon_price,
                            ], message: _trans('Done adding delivery price'));
                        }
                        return $this->failure(_trans('This place is out of pronunciation delivery'));

                    }
                    return $this->failure(_trans('This shop Not Found'));

                }
                return $this->failure(_trans('Please add to cart first'));

            }
            return $this->failure(_trans('Not found this customer'));
        } catch (\Exception $exception) {
            return $this->exception($exception);
        }

    }

    public function verifyPhoneLocation(SelectLocationRequest $request)
    {
        try {
            $customer = $this->customer($request);
            if ($customer) {
                $carts = $this->carts($customer->id)->first();
                if (!empty($carts)) {
                    $location = Location::query()->find($request->location_id);
                    $location->update(['phone_verified' => $request->phone_verified]);
                    return LocationResource::make($location);
                }
                return $this->failure(_trans('Please add to cart first'));
            }
            return $this->failure(_trans('Not found this customer'));
        } catch (\Exception $exception) {
            return $this->exception($exception);
        }
    }

    public function coupon(CouponRequest $request)
    {
        try {
            DB::beginTransaction();
            $customer = $this->customer($request);
            if ($customer) {
                $cart = $this->carts($customer->id)->first();

                if (empty($cart)) {
                    return $this->failure(message: _trans('please add item in cart first'));
                } else {
                    $coupon = Coupon::query()
//                        ->whereRelation('shop',  ['country_id'=>$request->country_id,'status'=>Status::Active->value])
                        ->whereCode($request->code)
//                        ->whereShopId($cart->shop_id)
                        ->first();
                    if (!$coupon) {
                        return $this->failure(message: _trans('Not found this coupon'));
                    }
                    if ($coupon->coupon_type == CouponType::Customized->value && $coupon->shop_id != $cart->shop_id) {
                        return $this->failure(message: _trans('not found this coupon on this shop'));
                    }

                    if ($coupon->coupon_type == CouponType::Customized->value && $coupon->shop->country_id != $request->country_id) {
                        return $this->failure(message: _trans('not found this coupon on this shop on this country'));
                    }
                    $userCoupon = UserCoupon::query()->where([
                        ['customer_id', '=', $customer->id],
                        ['coupon_id', '=', $coupon->id],
                    ])->count();
                    if ($userCoupon == $coupon->limit) {
                        return $this->failure(message: _trans('This coupon is used before'));
                    } else {
                        $cartDetails = CartDetail::query()->whereCartId($cart->id)->get();
                        $total = 0;
                        foreach ($cartDetails as $detail) {
                            $total += $detail->total_price_extras;
                            $total += $detail->total_price;
                        }
                        if ($coupon->min_purchase > $total) {
                            return $this->failure(_trans('price purchase must be bigger then ' . $coupon->min_purchase));
                        }
                        if ($coupon->coupon_type == CouponType::For_All->value) {
                            return $this->submitCode($coupon, $cart, $total);
                        }
                        if ($cart->shop_id == $coupon->shop_id) {
                            return $this->submitCode($coupon, $cart, $total);
                        }
                        return $this->failure(message: _trans('Please check your code in the same shop as the card'));
                    }
                }
            }
            return $this->failure(_trans('Not found this customer'));

        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->exception($exception);
        }
    }
    public function submitCode($coupon, $cart, $total)
    {
        if ($coupon->discount_type == DiscountTypeCoupon::Percentage->value) {
            $discount = calculatePercentage($total, $coupon->discount, 'decrease');
            $cart->update(['coupon_code' => $coupon->code, 'coupon_price' => $discount['amount']]);
            DB::commit();
            return $this->success([
                'delivery_price' => $cart->delivery_price,
                'total_price' => $cart->total_price,
                'coupon_price' => $cart->coupon_price,
                'amount' => ($cart->delivery_price + $cart->total_price) - $cart->coupon_price,
            ], message: _trans('Done submit discount code'));
        } else {
            $cart->update(['coupon_code' => $coupon->code, 'coupon_price' => $coupon->discount]);
            DB::commit();
            return $this->success([
                'delivery_price' => $cart->delivery_price,
                'total_price' => $cart->total_price,
                'coupon_price' => $cart->coupon_price,
            ], message: _trans('Done submit discount code'));
        }
    }


}
