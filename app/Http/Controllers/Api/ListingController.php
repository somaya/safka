<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\ListingRequest;
use App\Http\Resources\Listing\ListingResource;
use App\Models\Customer;
use App\Models\Listing;
use App\Traits\ApiResponses;
use Illuminate\Http\Request;

class ListingController extends Controller
{
    use ApiResponses;

    private function customer($request)
    {
        return Customer::query()->whereHas('user', function ($query) use ($request) {
            $query->where('userable_type', Customer::class)
                ->where('id', $request->user()->id);
        })->first();
    }

    public function index(Request $request)
    {
        try {
            $customer = $this->customer($request);
            if ($customer) {
                $listing = Listing::query()
                    ->whereCustomerId($customer->id)->orderByDesc('created_at');
                if($request->date){
                    $listing=$listing->where('date',$request->date);
                }
                $listing=$listing->get();
                return $this->success(ListingResource::collection($listing), message: _trans('Customer Listing'));
            }
            return $this->failure(_trans('Not found this customer'));
        } catch (\Exception $exception) {
            return $this->exception($exception);
        }
    }

    public function store(ListingRequest $request)
    {
        try {
            $customer = $this->customer($request);
            if ($customer) {
                $data = $request->validated();
                $data['customer_id'] = $customer->id;
                $listing = Listing::query()->create($data);
                if ($listing) {
                    return $this->success(message: _trans('Done save listing customer'));
                }
                return $this->failure(_trans('please try again'));
            }
            return $this->failure(_trans('Not found this customer'));
        } catch (\Exception $exception) {
            return $this->exception($exception);
        }
    }

    public function update(ListingRequest $request)
    {
        try {
            $customer = $this->customer($request);
            if ($customer) {
                $data = $request->validated();
                $listing = Listing::query()->whereCustomerId($customer->id)->find($request->listing_id);
                if ($listing) {
                    if ($listing->update($data)) {
                        return $this->success(message: _trans('Done save listing customer'));
                    }
                    return $this->failure(_trans('please try again'));
                }
                return $this->failure(_trans('Not found this address'));

            }
            return $this->failure(_trans('Not found this customer'));
        } catch (\Exception $exception) {
            return $this->exception($exception);
        }
    }

    public function show(Request $request)
    {
        try {
            $customer = $this->customer($request);
            if ($customer) {
                $listing = Listing::query()->whereCustomerId($customer->id)
                    ->find($request->listing_id);
                if ($listing) {
                    return $this->success(ListingResource::make($listing), message: _trans('show Listing customer'));
                }
                return $this->failure(_trans('Not found this Listing'));
            }
            return $this->failure(_trans('Not found this customer'));
        } catch (\Exception $exception) {
            return $this->exception($exception);
        }
    }

    public function delete(Request $request)
    {
        try {
            $customer = $this->customer($request);
            if ($customer) {
                $listing = Listing::query()->whereCustomerId($customer->id)->find($request->listing_id);
                if ($listing) {
                    $listing->delete();
                    return $this->success(message: _trans('Listing customer deleted successfully'));
                }
                return $this->failure(_trans('Not found this listing'));
            }
            return $this->failure(_trans('Not found this customer'));
        } catch (\Exception $exception) {
            return $this->exception($exception);
        }
    }
    public function buying(Request $request)
    {
        try {
            $customer = $this->customer($request);
            if ($customer) {
                $listing = Listing::query()->whereCustomerId($customer->id)->find($request->listing_id);
                $buying = !$listing->buying;
                if ($listing) {
                    $listing->update(['buying'=>$buying]);
                    return $this->success(message: _trans('Listing customer updated successfully'));
                }
                return $this->failure(_trans('Not found this listing'));
            }
            return $this->failure(_trans('Not found this customer'));
        } catch (\Exception $exception) {
            return $this->exception($exception);
        }
    }
}
