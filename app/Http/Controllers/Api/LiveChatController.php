<?php

namespace App\Http\Controllers\Api;

use App\Events\GetNotificationShop;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\LiveChatRequest;
use App\Http\Resources\Chat\_ConversationsResource;
use App\Http\Resources\Chat\ChatResource;
use App\Http\Resources\Shop\ShopResource;
use App\Models\Admin;
use App\Models\Notification;
use App\Models\Shop;
use App\Traits\ApiResponses;
use App\Traits\UploadFileTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class LiveChatController extends Controller
{

    use ApiResponses, UploadFileTrait;

//    public function __construct()
//    {
//        $this->middleware(['country-id'])->except(['index', 'show']);
//    }

    public function shops()
    {
        $shops = Shop::query()
            ->with([
                '_conversation' => function ($builder) {
                    $builder
                        ->where([
                            'senderable_type' => request()->user()->userable_type,
                            'senderable_id' => request()->user()->userable_id,
                        ])
                        ->withCount([
                            'chats as chats_un_seen' => function (Builder $builder) {
                                $builder->where(function (Builder $builder) {
                                    $builder->where('seen', '=', 0)
                                        ->where('modelable_type', '!=', request()->user()->userable_type)
                                        ->where('modelable_id', '!=', request()->user()->userable_id);
                                });
                            }
                        ]);
                }
            ])
            ->where([
                'country_id' => request()->country_id,
            ])
            ->get();
        $shops->filter(function ($shop) {
            $_conversation = request()->user()->userable
                ->_conversations()
                ->with(['lastMessage'])
                ->where([
                    'senderable_type' => request()->user()->userable_type,
                    'senderable_id' => request()->user()->userable_id,
                    'receiverable_type' => Shop::class,
                    'receiverable_id' => $shop->id,
                ])
                ->first();
            if ($_conversation) {
                if ($_conversation->lastMessage->updated_at > $shop->created_at) {
                    $shop->sort = $_conversation->lastMessage->updated_at;
                } else {
                    $shop->sort = Carbon::parse($shop->created_at)->subYears(100);
                }
                $shop->active = 1;
            } else {
                if ($shop->chat_enable == 1 && $shop->status == 1) {
                    $shop->sort = $shop->created_at;
                    $shop->active = 1;
                } else {
                    $shop->active = 0;
                }
            }
        });
        $filter = $shops->where('active', '=', 1)->sortBy([
            ['sort', 'desc'],
        ]);
        return $this->success(ShopResource::collection($filter));
    }

    public function index(Request $request)
    {
        $_conversations = request()->user()->userable
            ->_conversations()
            ->with(['lastMessage'])
            ->withCount([
                'chats as chats_un_seen' => function (Builder $builder) {
                    $builder->where(function (Builder $builder) {
                        $builder->where('seen', '=', 0)
                            ->where('modelable_type', '!=', request()->user()->userable_type)
                            ->where('modelable_id', '!=', request()->user()->userable_id);
                    });
                }
            ])
            /*->where([
                'receiverable_type' => Shop::class,
                'receiverable_id' => $request->shop_id,
            ])*/
            ->orderByDesc('updated_at')
            ->get();

        return $this->success(_ConversationsResource::collection($_conversations));

    }


    public function store(LiveChatRequest $request)
    {
        $_conversations = $request->user()->userable->_conversations()->updateOrCreate([
            'senderable_type' => $request->user()->userable_type,
            'senderable_id' => $request->user()->userable_id,
            'receiverable_type' => Shop::class,
            'receiverable_id' => $request->shop_id,
        ], [
            'senderable_type' => $request->user()->userable_type,
            'senderable_id' => $request->user()->userable_id,
            'receiverable_type' => Shop::class,
            'receiverable_id' => $request->shop_id,
        ]);

        $chat = $_conversations->chats()->create([
            'modelable_type' => $request->user()->userable_type,
            'modelable_id' => $request->user()->userable_id,
            'message' => $request->message,
        ]);
        $shop = Shop::query()->find($request->shop_id);

        $notificationAdmin = Notification::query()->create([
            'type' => 'live-chat',
            'senderable_type' => $request->user()->userable_type,
            'senderable_id' => $request->user()->userable_id,
            'receiverable_type' => Admin::class,
            'receiverable_id' => Admin::query()->first()?->id,
            'data' => [
                'chat_id' => $chat->id,
                'conversion_id' => $chat->conversation_id,
                'modelable_type' => $request->user()->userable_type,
                'modelable_id' => $request->user()->userable_id,
                'message' => $request->message,
                'shop_id' => $request->shop_id,
                'receiverable_type' => Shop::class,
                'receiverable_id' => $request->shop_id,
            ],
        ]);

        $notificationOwner = Notification::query()->create([
            'type' => 'live-chat',
            'senderable_type' => $request->user()->userable_type,
            'senderable_id' => $request->user()->userable_id,
            'receiverable_type' => Shop::class,
            'receiverable_id' => $shop->owner->user->id,
            'data' => [
                'chat_id' => $chat->id,
                'conversion_id' => $chat->conversation_id,
                'modelable_type' => $request->user()->userable_type,
                'modelable_id' => $request->user()->userable_id,
                'message' => $request->message,
                'shop_id' => $request->shop_id,
            ],
        ]);

        event(new GetNotificationShop($shop->owner, $notificationAdmin));

        return $this->success(ChatResource::make($chat));
    }


    public function show($id)
    {
        $_conversation = request()->user()->userable->_conversations()
            ->with([
                'lastMessage',
                'chats' => function ($builder) {
                    $builder->orderByDesc('created_at');
                }
            ])
            ->when(request()->routeIs('api.live-chat.chats'), function (Builder $builder) use ($id) {
                $builder->where([
                    'receiverable_type' => Shop::class,
                    'receiverable_id' => $id,
                ]);
            })
            ->when(request()->routeIs('api.live-chat.show'), function (Builder $builder) use ($id) {
                $builder->where('id', '=', $id);
            })->first();
        if (!$_conversation) {
            return $this->success([]);
        }
        foreach ($_conversation->chats as $chat){
            $chat->update(['seen'=>1]);
        }
        return $this->success(ChatResource::collection($_conversation->chats));
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
}
