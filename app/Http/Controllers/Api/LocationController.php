<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\LocationRequest;
use App\Http\Resources\Location\LocationResource;
use App\Models\Customer;
use App\Models\Location;
use App\Traits\ApiResponses;
use Illuminate\Http\Request;
use Propaganistas\LaravelPhone\PhoneNumber;


class LocationController extends Controller
{
    use ApiResponses;

    private function customer($request)
    {
        return Customer::query()->whereHas('user', function ($query) use ($request) {
            $query->where('userable_type',Customer::class)
                ->where('id', $request->user()->id);
        })->first();
    }

    public function index(Request $request)
    {
        try {
            $customer = $this->customer($request);
            if ($customer) {
                $locations = Location::query()
                    ->whereCountryId($request->country_id)
                    ->with(['country', 'governorate', 'region'])
                    ->whereCustomerId($customer->id)->orderByDesc('created_at')->get();
                return $this->success(LocationResource::collection($locations), message: _trans('List location customer'));
            }
            return $this->failure(_trans('Not found this customer'));
        } catch (\Exception $exception) {
            return $this->exception($exception);
        }
    }

    public function store(LocationRequest $request)
    {

        try {
            $customer = $this->customer($request);
            if ($customer) {
                $data = $request->validated();
                $data['customer_id'] = $customer->id;
                $data['phone']=(string) PhoneNumber::make($request->phone, $request->country_code);
                $location = Location::query()->create($data);
                if ($location) {
                    return $this->success(message: _trans('Done save location customer'));
                }
                return $this->failure(_trans('please try again'));
            }
            return $this->failure(_trans('Not found this customer'));
        } catch (\Exception $exception) {
            return $this->exception($exception);
        }
    }

    public function update(LocationRequest $request)
    {
        try {
            $customer = $this->customer($request);
            if ($customer) {
                $data = $request->validated();
                $data['phone']=(string) PhoneNumber::make($request->phone, $request->country_code);
                $location = Location::query()->whereCountryId($request->country_id)->whereCustomerId($customer->id)->find($request->location_id);
                if ($location) {
                    if ($location->update($data)) {
                        return $this->success(message: _trans('Done save location customer'));
                    }
                    return $this->failure(_trans('please try again'));
                }
                return $this->failure(_trans('Not found this location'));

            }
            return $this->failure(_trans('Not found this customer'));
        } catch (\Exception $exception) {
            return $this->exception($exception);
        }
    }

    public function show(Request $request)
    {
        try {
            $customer = $this->customer($request);
            if ($customer) {
                $location = Location::query()
                    ->whereCountryId($request->country_id)
                    ->with(['country', 'governorate', 'region'])
                    ->whereCustomerId($customer->id)->find($request->location_id);
                if ($location) {
                    return $this->success(LocationResource::make($location), message: _trans('show location customer'));
                }
                return $this->failure(_trans('Not found this location'));
            }
            return $this->failure(_trans('Not found this customer'));
        } catch (\Exception $exception) {
            return $this->exception($exception);
        }
    }

    public function delete(Request $request)
    {
        try {
            $customer = $this->customer($request);
            if ($customer) {
                $location = Location::query()
                    ->whereCountryId($request->country_id)
                    ->whereCustomerId($customer->id)
                    ->find($request->location_id);
                if ($location) {
                    $location->delete();
                    return $this->success(message: _trans('location customer deleted successfully'));
                }
                return $this->failure(_trans('Not found this location'));
            }
            return $this->failure(_trans('Not found this customer'));
        } catch (\Exception $exception) {
            return $this->exception($exception);
        }
    }

}
