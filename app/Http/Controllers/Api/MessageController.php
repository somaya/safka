<?php

namespace App\Http\Controllers\Api;

use App\Enums\Status;
use App\Events\GetConversationAdmin;
use App\Events\GetConversationShop;
use App\Events\GetNotificationAdmin;
use App\Events\GetNotificationShop;
use App\Events\SendMessageAdmin;
use App\Events\SendMessageShop;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\MessageRequest;
use App\Http\Resources\Message\AdminMessageResource;
use App\Http\Resources\Message\ChatsResource;
use App\Http\Resources\Message\MessageResource;
use App\Http\Resources\Order\MessageOrderResource;
use App\Models\Admin;
use App\Models\AdminConversation;
use App\Models\AdminMessage;
use App\Models\Conversation;
use App\Models\Customer;
use App\Models\Message;
use App\Models\Notification;
use App\Models\Order;
use App\Models\User;
use App\Traits\ApiResponses;
use Illuminate\Support\Facades\DB;


class MessageController extends Controller
{
    use ApiResponses;

    public function index($orderId)
    {
        $order = Order::query()->with('customer')->find($orderId);
        if ($order)
        {
        $messages = Message::query()
            ->whereRelation('conversion.order.shop',  ['country_id'=>request('country_id'),'status'=>Status::Active->value])
            ->whereHas('conversion', function ($query) use ($orderId) {
                $query->where('order_id', $orderId);
            })->orderByDesc('created_at')->get();
        if ($messages->isNotEmpty()){
            $messages->toQuery()->update(['read'=>1]);
        }

            return $this->success([
                'info_orders' =>MessageOrderResource::make($order),
                'messages' => MessageResource::collection($messages)
            ], message: _trans('list of message customer with shop'));
        }
        return $this->failure(_trans('order Not Found'));
    }

    public function chats()
    {
        $user = request()->user()->userable;
        $chats = Conversation::query()
            ->whereRelation('order.shop',  ['country_id'=>request('country_id'),'status'=>Status::Active->value])
            ->where('sender_id', $user->id)->has('order')
            ->with(['order'])->orderByDesc('created_at')->get();
        return $this->success(ChatsResource::collection($chats), message: _trans('list of Chats'));

    }

    public function store(MessageRequest $request)
    {
        try {
            DB::beginTransaction();
            $customer = $request->user()->userable;
            $order = Order::query()->whereRelation('shop',  ['country_id'=>$request->country_id,'status'=>Status::Active->value])
                ->whereCustomerId($customer->id)
                ->find($request->order_id);
            $conversation = Conversation::query()->with(['customer', 'order.shop.owner'])->whereOrderId($order?->id)->first();
            if (!$conversation) {
                $conversation = Conversation::query()->create([
                    'sender_id' => $customer->id,
                    'receiver_id' => $order->shop->id,
                    'order_id' => $order->id,
                    'last_time_message' => now(),
                ]);
            }
            $message = $conversation->messages()->create([
                'sender_id' => $customer->id,
                'receiver_id' => $order->shop->id, // shop_id
                'user_id' => $customer->user->id, // owner_shop owner_id
                'read' => false,
                'body' => $request->message,
                'type' => 'text',
            ]);
            $conversation->update(['last_time_message' => now()]);
            event(new SendMessageShop($customer->user, $conversation, $message));
            $conversations = Conversation::query()->orderByDesc('last_time_message')->get();
            event(new GetConversationShop($order->shop->owner->user, $conversations));
            $notifications = Notification::query()->create([
                'type' => 'message',
                'senderable_type' => Customer::class,
                'senderable_id' => $customer->id,
                'receiverable_type' => User::class,
                'receiverable_id' => $order->shop->owner->user->id,
                'data' => [
                    'order_id' => 0,
                    'message_id' => $message->id,
                    'conversion_id' => $message->conversion_id,
                    'shop_id' => $order->shop->id,
                ],
            ]);
            event(new GetNotificationShop($order->shop->owner->user, $notifications));
            DB::commit();
            return $this->success(MessageResource::make($message), message: _trans('Done send message successfully'));

        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->exception($exception);
        }
    }

    public function getTicket()
    {
        $user = request()->user()->userable;

        $messages = AdminMessage::query()->whereHas('conversion', function ($query) use ($user) {
            $query->where('sender_id', $user->id);
        })->orderByDesc('created_at')->get();
        return $this->success(MessageResource::collection($messages), message: _trans('list of ticket customer'));
    }

    public function storeTicket(MessageRequest $request)
    {
        try {
            DB::beginTransaction();
            $user = $request->user();
            $admin = Admin::query()->first();
            $conversation = AdminConversation::query()->with(['customer'])->whereSenderId($user->userable->id)->first();
            if (!$conversation) {
                $conversation = AdminConversation::query()->create([
                    'sender_id' => $user->userable->id,
                    'receiver_id' => $admin->id,
                    'last_time_message' => now(),
                ]);
            }
            $message = $conversation->messages()->create([
                'sender_id' => $user->userable->id,
                'receiver_id' => $admin->id,
                'user_id' => $user->id, // owner or staff owner and customer owner_id
                'read' => false,
                'body' => $request->message,
                'type' => 'text',
            ]);
            $conversation->update(['last_time_message' => now()]);
            event(new SendMessageAdmin($admin, $conversation, $message));
            $conversations = AdminConversation::query()->with(['customer'])->orderByDesc('last_time_message')->get();
            event(new GetConversationAdmin($admin, $conversations));
            $notifications = Notification::query()->create([
                'type' => 'ticket',
                'senderable_type' => Customer::class,
                'senderable_id' => $user->userable->id,
                'receiverable_type' => Admin::class,
                'receiverable_id' => $admin->id,
                'data' => [
                    'customer_id' => $user->userable->id,
                ],
            ]);
            event(new GetNotificationAdmin($admin, $notifications));
            DB::commit();
            return $this->success(AdminMessageResource::make($message), message: _trans('Done send message successfully'));

        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->exception($exception);
        }
    }

}
