<?php

namespace App\Http\Controllers\Api;

use App\Enums\CouponType;
use App\Enums\Status;
use App\Events\AddTaxOnModel;
use App\Events\AddTaxOnOrder;
use App\Helpers\Setting\Utility;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\OrderRequest;
use App\Http\Resources\Order\OrderResource;
use App\Models\Areas;
use App\Models\Color;
use App\Models\Coupon;
use App\Models\Discount;
use App\Models\Location;
use App\Models\Offer;
use App\Models\Order;
use App\Models\Product;
use App\Models\Save;
use App\Models\Size;
use App\Models\User;
use App\Models\UserCoupon;
use App\Traits\ApiResponses;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;


class OrderController extends Controller
{
    use ApiResponses;

    private function onData($model, $extras = null)
    {
        $cartDetailData = $extraInfo = [];
        $totalPriceExtras = 0;
        if (!is_null($extras) && count($extras) > 0)
        {
            foreach ($extras as $extra) {
                $price = Extra::query()->find($extra['id'])->price;
                $extraInfo[] = [
                    'id' => $extra['id'],
                    'qty' => $extra['qty'],
                    'price' => $price,
                    'total_price' => $price * $extra['qty'],
                ];
                $totalPriceExtras += $price * $extra['qty'];
                $extraIds[] = $extra['id'];
            }
            //is added extra data
            $cartDetailData['extra_info'] = $extraInfo;
            $cartDetailData['total_price_extras'] = $totalPriceExtras;
        } else {
            $cartDetailData['extra_info'] = NULL;
            $cartDetailData['total_price_extras'] = $totalPriceExtras;
        }


        // product or product size
        if ($model->modelable_type == Product::class) {
            $cartDetailData['modelable_type'] = $model->modelable_type;
            $cartDetailData['modelable_id'] = $model->modelable_id;
            $cartDetailData['qty'] = $model->qty;
            $product = Product::query()->find($cartDetailData['modelable_id']);
            $cartDetailData['price'] = $product->price;

            if (!is_null($model->size_id)) {
                $size = Size::query()->find($model->size_id);
                $cartDetailData['modelable_id'] = $size->product_id;
                $cartDetailData['size_id'] = $model->size_id;
                $cartDetailData['price'] = $model->price;
            }
            if (!is_null($model->color_id)) {
                $color = Color::query()->find($model->color_id);
                $cartDetailData['modelable_id'] = $color->product_id;
                $cartDetailData['color_id'] = $model->color_id;
                $cartDetailData['code'] = $model->code;
            }

        }

        // offer or save
        if ($model->modelable_type == Offer::class || $model->modelable_type == Save::class) {
            $cartDetailData['modelable_type'] = $model->modelable_type;
            $cartDetailData['modelable_id'] = $model->modelable_id;
            $cartDetailData['qty'] = $model->qty;
            $data = $this->offerOrSave($model, $model->modelable_type);
            $cartDetailData['products'] = $data['products'];
            $cartDetailData['price'] = $data['price'];
        }

        // discount
        if ($model->modelable_type == Discount::class) {
            $cartDetailData['modelable_type'] = $model->modelable_type;
            $cartDetailData['modelable_id'] = $model->modelable_id;
            $cartDetailData['qty'] = $model->qty;
            $discount = Discount::query()->find($model->modelable_id);
            $cartDetailData['price'] = $discount['price_after'];
        }


        return $cartDetailData;
    }

    private function offerOrSave($model, $type)
    {
        if ($type == Offer::class) {
            $data = Offer::query()->find($model->modelable_id);
        } else {
            $data = Save::query()->find($model->modelable_id);
        }
        return [
            'products' => $data->products,
            'price' => $data->price,
        ];
    }

    public function checkout(OrderRequest $request)
    {
        try {
            DB::beginTransaction();
            $data = new CartController();
            $customer = $data->customer($request);
            if ($customer) {
                $carts = $data->carts($customer->id)->first();

                if (empty($carts)) {
                    return $this->failure(message: _trans('Please add a product to the cart first'));
                } else {
                    $isOpen = getWeeks($carts->shop->workingHours);
//                    if (!$isOpen) {
//                        return $this->failure(message: _trans('This Shop is currently closed, please try again later. Thank you'));
//                    }
                    $orderData = [
                        'customer_id' => $carts->customer_id,
                        'shop_id' => $carts->shop_id,
                        'special_order' => $request->special_order,
                        'order_status' => 'pending',
                        'status_pay' => 'unpaid',
                    ];
                    /* check if coupon in cart */
                    if (!empty($carts->coupon_code)) {
                        $date = now()->format('Y-m-d');
                        $coupon = Coupon::query()
                            ->with(['shop'])
                            ->where(function ($query) {
                                $query->where('start_date', '<=', now()->format('Y-m-d'))
                                    ->orWhere('end_date', '>=', now()->format('Y-m-d'));
                            })
                            ->where([
                                'code' => $carts->coupon_code,
                                'status' => Status::Active->value,
                            ])
                            ->first();
                        if ($coupon) {
                            if ($coupon->coupon_type == CouponType::Customized->value && $coupon->shop_id != $carts->shop_id) {
                                return $this->failure(message: _trans('not found this coupon on this shop'));
                            }

                            if ($coupon->coupon_type == CouponType::Customized->value && $coupon->shop->country_id != $request->country_id) {
                                return $this->failure(message: _trans('not found this coupon on this shop on this country'));
                            }
                            $userCoupon = UserCoupon::query()->where([
                                ['customer_id', '=', $customer->id],
                                ['coupon_id', '=', $coupon->id],
                            ])->count();
                            if ($userCoupon == $coupon->limit) {
                                return $this->failure(message: _trans('This coupon is used before'));
                            }
                            $orderData += [
                                'coupon_code' => $carts->coupon_code,
                                'coupon_id' => $coupon->id,
                                'coupon_price' => $carts->coupon_price,
                            ];
                        } else {
                            return $this->failure(message: _trans('Please update to coupon on cart is expired'));
                        }
                    }

//                    if (empty($carts->location_id)) {
//                        return $this->failure(_trans('Please select Location to delivery order'));
//                    }
//                    $location = Location::query()->whereCountryId(request('country_id'))->find($carts->location_id);
//                    $area = Areas::query()
//                        ->whereRelation('governorate', 'country_id', '=',request('country_id'))
//                        ->with(['deliveryArea'])->has('deliveryArea')
//                        ->whereHas('deliveryArea', function ($query) use ($carts, $location) {
//                            $query->where([
//                                'shop_id' => $carts->shop_id,
//                                'country_id' => $location->country_id,
//                            ]);
//                        })
//                        ->where([
//                            'governorate_id' => $location->governorate_id,
//                        ])->whereHas('regions', function ($query) use ($location) {
//                            $query->where('area_regions.region_id', $location->region_id);
//                        })->first();
//                    if (empty($area)) {
//                        return $this->failure(_trans('This location is out of pronunciation delivery please updated location on cart'));
//                    }
//                    $orderData += [
//                        'location_id' => $location->id,
//                        'area_id' => $area->id,
//                        'delivery_price' => $area->delivery_price,
//                        'location_info' => $location->toArray(),
//                    ];


                    /* modelable_type offer or save or discount */
                    $models = $carts->cartDetails;
                    $dateTime = Carbon::now();
                    $date = $dateTime->format('Y-m-d');
                    $time = $dateTime->format('H:i');

                    $dataModels = [];

                    $orderDetails = [];
                    foreach ($models as $key => $model) {
                        if ($model->modelable_type == Product::class) {
                            $product=Product::query()->where('status', '=', Status::Active->value)
                                ->find($model->modelable_id);
                            if (!$product) {
                                return $this->failure(_trans('This Product is not available please updated in cart'));
                            }else{
                               if ($model->qty > $product->maximum_order_number){
                                   return $this->failure(_trans('Product '.$product->translate(locale())->name.' Quantity  More than maximum order number please updated in cart'));
                               }
                                $orderDetails[$key] = $this->onData($model, $model->extra_info);

                            }
                        }
                        if ($model->modelable_type == Offer::class) {
                            $offer = Offer::query()->where([
                                ['start', '<=', $date],
                                ['end', '>=', $date],
                                ['status', '=', Status::Active->value],
                            ])->find($model->modelable_id);
                            if (!$offer) {
                                return $this->failure(_trans('This offer is not available or expired please updated in cart'));
                            }
                            $orderDetails[$key] = $this->onData($model, $model->extra_info);
                        }
                        if ($model->modelable_type == Discount::class) {
                            $discount = Discount::query()->where([
                                ['start', '<=', $date],
                                ['end', '>=', $date],
                                ['status', '=', Status::Active->value],
                            ])->find($model->modelable_id);
                            if (!$discount) {
                                return $this->failure(_trans('This discount is not available or expired please updated in cart'));
                            }
                            $orderDetails[$key] = $this->onData($model, $model->extra_info);
                        }

                        if ($model->modelable_type == Save::class) {
                            $save = Save::query()->where([
                                ['start', '<=', $date],
                                ['end', '>=', $date],
                                /* ['from', '<=', $time],
                                ['to', '>=', $time],*/
                                ['status', '=', Status::Active->value],
                            ])->find($model->modelable_id);
                            if (!$save) {
                                return $this->failure(_trans('This save 50 is not available or expired please updated in cart'));
                            }
                            $orderDetails[$key] = $this->onData($model, $model->extra_info);
                        }
                    }
                    if ($carts->total_price < $carts->shop->minimum_order_price){
                        return $this->failure(_trans('Total Price less than minimum order price for this shop please updated in cart'));
                    }
                    $orderData['payment_method'] = 'cash';
                    $orderData['code'] = tableCode('orders');
                    $order = Order::query()->create($orderData);
                    $order->orderDetails()->createMany($orderDetails);
                    if (!empty($carts->coupon_code)) {
                        UserCoupon::query()->create([
                            'customer_id' => $customer->id,
                            'coupon_id' => $orderData['coupon_id'],
                            'order_id' => $order->id,
                        ]);
                    }
                    $carts->delete();
                    $notification = $customer->senderable()->create([
                        'type' => 'order',
                        'receiverable_type' => User::class,
                        'receiverable_id' => $order->shop->owner->user->id,
                        'data' => [
                            'order_id' => $order->id,
                            'order_code' => $order->code,
                            'shop_id' => $order->shop_id,
                        ]
                    ]);
                    $user = $order->shop->owner->user;
                    /* redis */
                    //event(new GetNotificationfurniture($user, $notification));
                    $order->trackingOrder()->create([
                        'order_status' => $order->order_status,
                        'start_at' => now(),
                    ]);
                    DB::commit();
                    return $this->success(OrderResource::make($order), message: _trans('Done Save order successfully'));
                }
            }
            return $this->failure(_trans('Not found this customer'));

        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->exception($exception);
        }
    }

    public function orders(Request $request)
    {
        try {
            $data = new CartController();
            $customer = $data->customer($request);
            if ($customer) {
                $orders = Order::query()
                    ->whereRelation('shop',  ['country_id'=>request('country_id'),'status'=>Status::Active->value])
                    ->with(['shop', 'orderDetails', 'area']);
                if (Route::currentRouteName() == 'api.order-current') {
                    $orders = $orders->whereIn('order_status', ['pending', 'on_the_way', 'delivered']);
                } elseif (Route::currentRouteName() == 'api.order-previous') {
                    $orders = $orders->whereIn('order_status', ['completed', 'canceled']);
                }
                $orders = $orders->whereCustomerId($customer->id)->orderByDesc('created_at')->paginate(Utility::getValByName('pagination_limit'));
                return $this->success(OrderResource::collection($orders), $orders, message: _trans('List of orders customer'));
            }
            return $this->failure(_trans('Not found this customer'));
        } catch (\Exception $exception) {
            return $this->exception($exception);
        }
    }

    public function show(Request $request, $order_id)
    {
        try {
            $data = new CartController();
            $customer = $data->customer($request);
            if ($customer) {
                $order = Order::query()
                    ->whereRelation('shop',  ['country_id'=>request('country_id'),'status'=>Status::Active->value])
                    ->with(['shop', 'orderDetails', 'area'])
                    ->whereCustomerId($customer->id)->find($order_id);
                if ($order) {
                    return $this->success(OrderResource::make($order), message: _trans('order details on this customer'));
                }
                return $this->failure(_trans('Not found this order on this customer'));
            }
            return $this->failure(_trans('Not found this customer'));
        } catch (\Exception $exception) {
            return $this->exception($exception);
        }
    }
}
