<?php

namespace App\Http\Controllers\Api;

use App\Helpers\Setting\Utility;
use App\Http\Controllers\Controller;
use App\Http\Resources\OurPartner\OurPartnerResource;
use App\Models\OurPartner;
use App\Traits\ApiResponses;

class OurPartnerController extends Controller
{
    use ApiResponses;

    public function __invoke()
    {
        $ourPartners = OurPartner::query()
            ->orderByDesc('ranking')
            ->status()
            ->paginate(Utility::getValByName('pagination_limit'))
            ->withQueryString();
        return $this->success(OurPartnerResource::collection($ourPartners), $ourPartners);
    }

}
