<?php

namespace App\Http\Controllers\Api;

use App\Enums\Status;
use App\Http\Controllers\Controller;
use App\Http\Resources\Color\ColorResource;
use App\Http\Resources\Discount\DiscountResource;
use App\Http\Resources\Favourite\ProductFavouriteResource;
use App\Http\Resources\Offer\OfferResource;
use App\Http\Resources\Product\ProductResource;
use App\Http\Resources\Rate\RateResource;
use App\Http\Resources\Save\SaveResource;
use App\Http\Resources\Shop\BranchResource;
use App\Http\Resources\Shop\MenuResource;
use App\Http\Resources\Shop\ShopDetailResource;
use App\Http\Resources\Shop\ShopResource;
use App\Http\Resources\Size\SizeResource;
use App\Models\Category;
use App\Models\Color;
use App\Models\Discount;
use App\Models\Favourite;
use App\Models\Offer;
use App\Models\Product;
use App\Models\Rate;
use App\Models\Save;
use App\Models\Shop;
use App\Models\Size;
use App\Traits\ApiResponses;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class ShopController extends Controller
{
    public $offerIds = [];
    public $saveIds = [];
    use ApiResponses;

    public function shopDetails($slug)
    {
        $shop = Shop::query()
            ->whereCountryId(request('country_id'))
            ->with(['country', 'governorate', 'region','branchTypes'=>function($q){
            $q->withCount(['shops'=>function($q){
                $q->status();
            }]);
        }])
            ->withCount(['rates'])
            ->where(function ($q)use($slug){
                $q->whereTranslation('slug',$slug)->orWhere('id',$slug);
            })->first();
        if ($shop) {
            $menu = Category::query()->whereHas('products', function ($q) use ($shop) {
                $q->where('shop_id', $shop->id)
                    ->status();
            })
                ->with(['children' => function ($q) use ($shop) {
                    $q->whereHas('subcategoryProducts', function ($q) use ($shop) {
                        $q->where('shop_id', $shop->id)
                            ->status();
                    });

                }])
                ->with(['products' => function ($q) use ($shop){
                    $q->where('shop_id', $shop->id)
                        ->status()->withCount(['rates'])
                        ->with(['shop','images','sizes' => function ($q) {
                        $q->status();
                    },'colors' => function ($q) {
                            $q->status();
                        }]);

                }])->status()->get();
            $most_wanted_products = Product::query()
                ->whereRelation('shop',  ['country_id'=>request('country_id'),'status'=>Status::Active->value])
                ->withCount(['rates'])->where('shop_id', $shop->id)->status()
                ->wanted()->with(['shop','images','sizes' => function ($q) {
                $q->status();
            },'colors' => function ($q) {
                    $q->status();
                }])->get();

            $branches = $shop->owner->shops()->where('id', '!=', $shop->id)->status()->get();

            $favourites = Favourite::where('user_id', auth('sanctum')->id())
                ->with(['favourite' => function ($q) {
                    $q->withCount(['rates'])->with('images')->with(['sizes' => function ($q) {
                        $q->status();
                    },'colors' => function ($q) {
                        $q->status();
                    }]);
                }])->whereHasMorph('favourite', Product::class, function ($query) use ($shop) {
                    $query->where('shop_id', $shop->id)
                        ->status();
                })->get();

            $offers = Offer::query()->where('shop_id', $shop->id)->withCount(['rates'])->with(['images','shop'])
                ->where('start', '<=', Carbon::now()->format('Y-m-d'))
                ->where('end', '>=', Carbon::now()->format('Y-m-d'))
                ->status();
            $query=$offers->get();
            $query->each(function ($offer) use ($offers) {

                $productIds = [];
                foreach ($offer->products as $product) {
                    $productIds [] = $product['product_id'];
                }
                $checkProduct = Product::query()->whereStatus(Status::Not_Active->value)
                    ->whereIn('id', $productIds)->count();

                if ($checkProduct > 0) {
                    array_push($this->offerIds, $offer->id);
                }
            });

            $offers = $offers->whereNotIn('id', $this->offerIds)
                ->orderByDesc('created_at')->get();
            $saves = Save::query()->where('shop_id', $shop->id)->with(['shop', 'images'])
                ->whereHas('shop', function ($q) {
                    $q->status();
                })
                ->withCount(['rates'])
                ->where('start', '<=', Carbon::now()->format('Y-m-d'))
                ->where('end', '>=', Carbon::now()->format('Y-m-d'))
                ->where('from', '<=', Carbon::now()->format('H:i:s'))
                ->where('to', '>=', Carbon::now()->format('H:i:s'))
                ->status();
            $query = $saves->get();
            $query->each(function ($save) use ($saves) {

                $productIds = [];
                foreach ($save->products as $product) {
                    $productIds [] = $product['product_id'];
                }
                $checkProduct = Product::query()->whereStatus(Status::Not_Active->value)
                    ->whereIn('id', $productIds)->count();

                if ($checkProduct > 0) {
                    array_push($this->saveIds, $save->id);
                }
            });

            $saves = $saves->whereNotIn('id', $this->saveIds)
                ->orderByDesc('created_at')->get();
            $discounts = Discount::query()->where('shop_id', $shop->id)
                ->whereHas('shop', function ($q) {
                    $q->status();
                })->whereHas('product', function ($q) {
                    $q->status();
                })
                ->with(['shop','product' => function ($q) {
                    $q->with('images');

                }])->withCount(['rates'])
                ->where('start', '<=', Carbon::now()->format('Y-m-d'))
                ->where('end', '>=', Carbon::now()->format('Y-m-d'))
               ->status()->get();


            $rates = Rate::where('rate_type', Shop::class)
                ->where('rate_id', $shop->id)->get();

            return $this->success([
                'shop' => ShopDetailResource::make($shop),
                'most_wanted_products' => ProductResource::collection($most_wanted_products),
                'menu' => MenuResource::collection($menu),
                'offers' => OfferResource::collection($offers),
                'saves' => SaveResource::collection($saves),
                'discounts' => DiscountResource::collection($discounts),
                'branches' => BranchResource::collection($branches),
                'favourites' => ProductFavouriteResource::collection($favourites),
                'rates' => RateResource::collection($rates),
            ], message: _trans('Shop Details'));
        }
        return $this->failure(_trans('Shop Not Found'));

    }

    public function shopDetailsQrcode(Request $request)
    {
        try {
            $decryptIdShop = Crypt::decryptString($request->qrcode);
            return $this->shopDetails($decryptIdShop);
        } catch (\Exception $exception) {
            return $this->failure($exception->getMessage());
        }

    }

    public function productDetails($slug)
    {
        $product = Product::query()
            ->whereRelation('shop', 'country_id', '=', request('country_id'))
            ->withCount(['rates'])
            ->with(['shop', 'category', 'images', 'sizes' => function ($q) {
                $q->status();
            },'colors' => function ($q) {
                $q->status();
            }])
            ->where(function ($q)use($slug){
                $q->whereTranslation('slug',$slug)->orWhere('id',$slug);
            })->first();
        if ($product) {
            $rates = Rate::query()->with(['user'])
                ->where([
                    ['rate_type', '=', Product::class],
                    ['rate_id', '=', $product->id],
                ])->orderByDesc('created_at')->get();

            return $this->success([
                'product' => ProductResource::make($product),
                'rates' => RateResource::collection($rates),
            ], message: _trans('Product Details'));
        }
        return $this->failure(_trans('Product Not Found'));

    }

    public function productsBySubCategory($shop_slug, $subSlug)
    {
        $shop = Shop::query()
            ->whereCountryId(request('country_id'))
            ->where(function ($q)use($shop_slug){
                $q->whereTranslation('slug',$shop_slug)->orWhere('id',$shop_slug);
            })->first();
        $subCategory=Category::query()
            ->whereNotNull('parent_id')
            ->where(function ($q)use($subSlug){
                $q->whereTranslation('slug',$subSlug)->orWhere('id',$subSlug);
            })->first();
        if ($shop && $subCategory) {
            $products = Product::where('shop_id', $shop->id)
                ->withCount(['rates'])
                ->where('sub_category_id', $subCategory->id)->with(['images','sizes' => function ($q) {
                    $q->status();
                },'colors' => function ($q) {
                    $q->status();
                }])->status()->get();

            return $this->success(ProductResource::collection($products), message: _trans('Products By Sub Catgory'));
        }
        return $this->failure(_trans('shop or sub category Not Found'));
    }

    public function productColors()
    {
        $colors = Color::query()->whereHas('product',function ($q){
            $q->status();
        })->whereHas('shop',function ($q){
            $q->status();
        })->get()->unique('code');
        return $this->success(ColorResource::collection($colors), message: _trans('Products Colors'));
    }
    public function productSizes(Request $request)
    {
        $sizes = Size::query()->whereHas('product',function ($q){
            $q->status();
        })->whereHas('shop',function ($q){
            $q->status();
        })->get()->unique('name');
        return $this->success(SizeResource::collection($sizes), message: _trans('Products Sizes'));
    }
    public function productColorsAndSizes()
    {
        $colors = Color::query()->whereHas('product',function ($q){
            $q->status();
        })->whereHas('shop',function ($q){
            $q->status();
        })->get()->unique('code');
        $sizes = Size::query()->whereHas('product',function ($q){
            $q->status();
        })->whereHas('shop',function ($q){
            $q->status();
        })->get()->unique('name');
        return $this->success([
            'colors'=>ColorResource::collection($colors),
           'sizes'=> SizeResource::collection($sizes)
        ], message: _trans('Products Colors and sizes'));
    }


}
