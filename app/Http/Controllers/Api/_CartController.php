<?php

namespace App\Http\Controllers\Api;

use App\Enums\DiscountTypeCoupon;
use App\Enums\UserType;
use App\Helpers\Trait\ApiResponses;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Cart\CouponRequest;
use App\Http\Requests\Api\Cart\ExtraRequest;
use App\Http\Requests\Api\Cart\ProductRequest;
use App\Http\Requests\Api\Cart\ProductSizeRequest;
use App\Http\Requests\Api\Cart\RemoveModelTypeRequest;
use App\Http\Requests\Api\Cart\SelectLocationRequest;
use App\Http\Requests\Api\CartRequest;
use App\Http\Resources\Cart\CartResource;
use App\Models\Cart;
use App\Models\CartDetail;
use App\Models\Coupon;
use App\Models\Customer;
use App\Models\DeliveryArea;
use App\Models\Discount;
use App\Models\Extra;
use App\Models\Location;
use App\Models\Offer;
use App\Models\Product;
use App\Models\Save;
use App\Models\Size;
use App\Models\UserCoupon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class _CartController extends Controller
{
    use ApiResponses;

    public function customer($request)
    {
        return Customer::query()->whereHas('user', function ($query) use ($request) {
            $query->where('user_type', UserType::Customer->value)
                ->where('id', $request->user()->id);
        })->first();
    }

    public function carts($customerId)
    {
        return Cart::query()->with(['cartDetails'])->whereCustomerId($customerId);
    }

    public function index(Request $request)
    {
        try {
            $customer = $this->customer($request);
            if ($customer) {
                $carts = $this->carts($customer->id)->with(['furniture'])->orderByDesc('created_at')->get();
                return $this->success(CartResource::collection($carts), message: _trans('List cart customer'));
            }
            return $this->failure(_trans('Not found this customer'));
        } catch (\Exception $exception) {
            return $this->exception($exception);
        }
    }

    private function onData($request, $cart)
    {
        $cartDetailData = $request->only('model_id');
        $cartDetailData['cart_id'] = $cart->id;
        if ($request->model_type == 'product') {
            $cartDetailData['model_type'] = Product::class;
            $cartDetailData['model_id'] = $request->model_id;
        }
        if ($request->model_type == 'product_size') {
            $cartDetailData['model_type'] = Product::class . 'Size';
            $cartDetailData['model_id'] = $request->model_id;
        }
        if ($request->model_type == 'offer') {
            $cartDetailData['model_type'] = Offer::class;
            $cartDetailData['model_id'] = $request->model_id;
            $cartDetailData['data_offers'] = $this->offerOrSave($request, 'offer');
        }
        if ($request->model_type == 'discount') {
            $cartDetailData['model_type'] = Discount::class;
            $cartDetailData['model_id'] = $request->model_id;
        }
        if ($request->model_type == 'save') {
            $cartDetailData['model_type'] = Save::class;
            $cartDetailData['model_id'] = $request->model_id;
            $cartDetailData['data_offers'] = $this->offerOrSave($request, 'save');
        }
        return $cartDetailData;
    }

    /* add product not used size */
    public function addProduct(ProductRequest $request)
    {
        try {
            DB::beginTransaction();
            $customer = $this->customer($request);
            if ($customer) {
                $carts = $this->carts($customer->id)->first();
                if (empty($carts)) {
                    // is empty
                    $cartData = $request->only('shop_id');
                    $cartData['customer_id'] = $customer->id;
                    $cart = Cart::query()->create($cartData);

                    $cartDetailData = $this->onData($request, $cart);
                    $cartDetail = $cart->cartDetails()->create($cartDetailData);
                    $product = Product::query()->find($request->model_id);
                    $cartDetail->cartProduct()->create([
                        'product_id' => $product['id'],
                        'qty' => $request->qty,
                        'product_price' => $product['price']
                    ]);
                    DB::commit();
                    return $this->success(message: _trans('Done save product in cart successfully'));
                } else {
                    if ($carts->shop_id == $request->shop_id) {

                        $cartDetail = $carts->cartDetails()->where([
                            ['model_type', '=', Product::class],
                            ['model_id', '=', $request->model_id],
                        ])->first();
                        $product = Product::query()->find($request->model_id);
                        if (empty($cartDetail)) {
                            $cartDetailData = $this->onData($request, $carts);
                            $cartDetail = $carts->cartDetails()->create($cartDetailData);
                            $cartDetail->cartProduct()->create([
                                'product_id' => $product['id'],
                                'qty' => $request->qty,
                                'product_price' => $product['price']
                            ]);
                            DB::commit();
                            return $this->success(message: _trans('Done add new product in cart successfully'));
                        } else {
                            $cartDetail->cartProduct()->update([
                                'product_id' => $product['id'],
                                'qty' => $request->qty,
                                'product_price' => $product['price']
                            ]);
                            DB::commit();
                            return $this->success(message: _trans('Done update qty product in cart successfully'));
                        }

                    }
                    return $this->failure(message: _trans('Please choose a product from the same furniture as the card'));
                }
            }
            return $this->failure(_trans('Not found this customer'));

        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->exception($exception);
        }
    }

    /* add product size */
    public function addProductSize(ProductSizeRequest $request)
    {
        try {
            DB::beginTransaction();
            $customer = $this->customer($request);
            if ($customer) {
                $carts = $this->carts($customer->id)->first();
                if (empty($carts)) {
                    // is empty
                    $cartData = $request->only('shop_id');
                    $cartData['customer_id'] = $customer->id;
                    $cart = Cart::query()->create($cartData);

                    $cartDetailData = $this->onData($request, $cart);
                    $cartDetail = $cart->cartDetails()->create($cartDetailData);
                    $size = Size::query()->with(['product'])->find($request->size_id);
                    $cartDetail->cartProduct()->create([
                        'product_id' => $size->product->id,
                        'product_size' => $size['id'],
                        'qty' => $request->qty,
                        'product_price' => $size['price']
                    ]);
                    DB::commit();
                    return $this->success(message: _trans('Done save product size in cart successfully'));
                } else {
                    if ($carts->shop_id == $request->shop_id) {

                        $cartDetail = $carts->cartDetails()->where([
                            ['model_type', '=', Product::class . 'Size'],
                            ['model_id', '=', $request->model_id],
                        ])->first();
                        $size = Size::query()->with(['product'])->find($request->size_id);
                        if (empty($cartDetail)) {
                            $cartDetailData = $this->onData($request, $carts);
                            $cartDetail = $carts->cartDetails()->create($cartDetailData);
                            $cartDetail->cartProduct()->create([
                                'product_id' => $size->product->id,
                                'product_size' => $size['id'],
                                'qty' => $request->qty,
                                'product_price' => $size['price']
                            ]);
                            DB::commit();
                            return $this->success(message: _trans('Done add new product size in cart successfully'));
                        } else {
                            $cartDetail->cartProduct()->update([
                                'product_id' => $size->product->id,
                                'product_size' => $size['id'],
                                'qty' => $request->qty,
                                'product_price' => $size['price']
                            ]);
                            DB::commit();
                            return $this->success(message: _trans('Done update qty product size in cart successfully'));
                        }

                    }
                    return $this->failure(message: _trans('Please choose a product size from the same furniture as the card'));
                }
            }
            return $this->failure(_trans('Not found this customer'));

        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->exception($exception);
        }
    }

    /* offer in cart */
    private function offerOrSave($request, $type)
    {
        if ($type == 'offer') {
            $data = Offer::query()->find($request->model_id);
        } else {
            $data = Save::query()->find($request->model_id);
        }
        $products = [];
        foreach ($data->products as $product) {
            array_push($products, [
                'product_id' => $product['product_id'],
                'qty' => $product['quantity'],
                'product_price' => $data->price / (int)$product['quantity']
            ]);
        }
        return $products;
    }

    /* offer or save in cart */
    public function addOfferOrSave(CartRequest $request)
    {
        try {
            DB::beginTransaction();
            $customer = $this->customer($request);
            if ($customer) {
                $carts = $this->carts($customer->id)->first();

                if (empty($carts)) { // is empty
                    $cartData = $request->only('shop_id');
                    $cartData['customer_id'] = $customer->id;
                    $cart = Cart::query()->create($cartData);

                    $cartDetailData = $this->onData($request, $cart);
                    $dataOffers = $cartDetailData['data_offers'];
                    unset($cartDetailData['data_offers']);
                    $cartDetail = $cart->cartDetails()->create($cartDetailData);
                    $cartDetail->cartProduct()->createMany($dataOffers);
                    DB::commit();
                    return $this->success(message: _trans('Done save offer in cart successfully'));
                } else {
                    if ($carts->shop_id == $request->shop_id) {
                        $cartDetailData = $this->onData($request, $carts);
                        $dataOffers = $cartDetailData['data_offers'];
                        unset($cartDetailData['data_offers']);
                        $cartDetail = $carts->cartDetails()->create($cartDetailData);
                        $cartDetail->cartProduct()->createMany($dataOffers);
                        DB::commit();
                        return $this->success(message: _trans('Done add new offer in cart successfully'));
                    }
                    return $this->failure(message: _trans('Please choose a offer from the same furniture as the card'));
                }
            }
            return $this->failure(_trans('Not found this customer'));

        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->exception($exception);
        }
    }

    /* discount in cart */
    public function addDiscount(CartRequest $request)
    {
        try {
            DB::beginTransaction();
            $customer = $this->customer($request);
            if ($customer) {
                $carts = $this->carts($customer->id)->first();

                if (empty($carts)) { // is empty
                    $cartData = $request->only('shop_id');
                    $cartData['customer_id'] = $customer->id;
                    $cart = Cart::query()->create($cartData);

                    $cartDetailData = $this->onData($request, $cart);
                    $cartDetail = $cart->cartDetails()->create($cartDetailData);
                    $discount = Discount::query()->find($request->model_id);
                    $cartDetail->cartProduct()->create([
                        'product_id' => $discount['product_id'],
                        'qty' => $request->qty,
                        'product_price' => $discount['price_after']
                    ]);
                    DB::commit();
                    return $this->success(message: _trans('Done save discount in cart successfully'));
                } else {
                    if ($carts->restaurant_id == $request->restaurant_id) {

                        $cartDetail = $carts->cartDetails()->where([
                            ['model_type', '=', Discount::class],
                            ['model_id', '=', $request->model_id],
                        ])->first();
                        $discount = Discount::query()->find($request->model_id);
                        if (empty($cartDetail)) {
                            $cartDetailData = $this->onData($request, $carts);
                            $cartDetail = $carts->cartDetails()->create($cartDetailData);
                            $cartDetail->cartProduct()->create([
                                'product_id' => $discount['product_id'],
                                'qty' => $request->qty,
                                'product_price' => $discount['price_after']
                            ]);
                            DB::commit();
                            return $this->success(message: _trans('Done add new discount in cart successfully'));
                        } else {
                            $cartDetail->cartProduct()->update([
                                'product_id' => $discount['product_id'],
                                'qty' => $request->qty,
                                'product_price' => $discount['price_after']
                            ]);
                            DB::commit();
                            return $this->success(message: _trans('Done update qty discount in cart successfully'));
                        }

                    }
                    return $this->failure(message: _trans('Please choose a product from the same furniture as the card'));
                }
            }
            return $this->failure(_trans('Not found this customer'));

        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->exception($exception);
        }
    }

    /* extra in cart */
    public function addExtra(ExtraRequest $request)
    {
        try {
            DB::beginTransaction();
            $customer = $this->customer($request);
            if ($customer) {
                $cart = $this->carts($customer->id)->first();

                if (empty($cart)) {
                    return $this->failure(message: _trans('please add item in cart first'));
                } else {
                    $cartDetails = $cart->cartDetails()->where([
                        ['model_type', '=', Extra::class,],
                        ['model_id', '=', $request->extra_id],
                    ])->first();
                    $extra = Extra::query()->find($request->extra_id);
                    if ($cartDetails) {
                        $extraCart = $cartDetails->extras()->whereExtraId($request->extra_id)->first();
                        if ($extraCart) {
                            $extraCart->update([
                                'extra_price' => $extra->price,
                                'qty' => $request->qty,
                            ]);
                            DB::commit();
                            return $this->success(message: _trans('Done update in quantity extra in cart successfully'));
                        } else {
                            $cartDetails->extras()->create([
                                'extra_id' => $request->extra_id,
                                'extra_price' => $extra->price,
                                'qty' => $request->qty,
                            ]);
                            DB::commit();
                            return $this->success(message: _trans('Done add extra in cart successfully'));
                        }
                    } else {
                        $cartDetails = $cart->cartDetails()->create([
                            'model_type' => Extra::class,
                            'model_id' => $request->extra_id,
                        ]);
                        $cartDetails->extras()->create([
                            'extra_id' => $request->extra_id,
                            'extra_price' => $extra->price,
                            'qty' => $request->qty,
                        ]);
                        DB::commit();
                        return $this->success(message: _trans('Done add extra in cart successfully'));
                    }

                }
            }
            return $this->failure(_trans('Not found this customer'));

        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->exception($exception);
        }
    }

    /* remove offer or save or discount in cart */
    public function removeModelType(RemoveModelTypeRequest $request)
    {
        try {
            DB::beginTransaction();
            $customer = $this->customer($request);
            if ($customer) {

                $carts = $this->carts($customer->id)->first();
                if (empty($carts)) {
                    return $this->failure(_trans('Please add to cart first'));
                } else {
                    $cartDetail = CartDetail::query()
                        ->whereRelation('cart', 'customer_id', '=', $customer->id);
                    if ($request->model_type == 'product') {
                        $cartDetail = $cartDetail->whereModelType(Product::class);
                    }
                    if ($request->model_type == 'offer') {
                        $cartDetail = $cartDetail->whereModelType(Offer::class);
                    }
                    if ($request->model_type == 'discount') {
                        $cartDetail = $cartDetail->whereModelType(Discount::class);
                    }
                    if ($request->model_type == 'save') {
                        $cartDetail = $cartDetail->whereModelType(Save::class);
                    }
                    $cartDetail = $cartDetail->find($request->cart_detail_id);
                    if ($cartDetail) {
                        $cartDetails = CartDetail::query()->whereNotIn('id', [$cartDetail->id])->count();
                        if ($cartDetails == 0) {
                            $cartDetail->delete();
                            $carts->delete();
                            DB::commit();
                            return $this->success(message: _trans('Done remove offer from cart'));
                        }
                        $cartDetail->delete();
                        DB::commit();
                        return $this->success(message: _trans('Done remove offer from cart'));
                    }
                    return $this->failure(message: _trans('Not found offer in carts'));
                }
            }
            return $this->failure(_trans('Not found this customer'));

        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->exception($exception);
        }
    }

    public function removeAllCart(Request $request)
    {
        try {
            $customer = $this->customer($request);
            if ($customer) {
                $carts = $this->carts($customer->id)->first();
                if (!empty($carts)) {
                    $carts->delete();
                    return $this->failure(message: _trans('Done delete cart all'));
                } else {
                    return $this->failure(_trans('Not found cart'));
                }
            }
            return $this->failure(_trans('Not found this customer'));
        } catch (\Exception $exception) {
            return $this->exception($exception);
        }
    }

    public function selectLocation(SelectLocationRequest $request)
    {
        try {
            $customer = $this->customer($request);
            if ($customer) {
                $carts = $this->carts($customer->id)->first();
                if (!empty($carts)) {
                    $location = Location::query()->find($request->location_id);
                    $deliveryPrice = DeliveryArea::query()->whereRestaurantId($carts->restaurant_id)->status()
                        ->whereHas('furniture', function ($query) use ($location) {
                            $query->where([
                                ['country_id', '=', $location->country_id],
                                ['governorate_id', '=', $location->governorate_id],
                                ['region_id', '=', $location->region_id],
                            ]);
                        })->first();
                    if ($deliveryPrice) {
                        $delivery_price = $deliveryPrice->delivery_price;
                        $carts->update([
                            'location_id' => $location->id,
                            'delivery_price' => $delivery_price,
                            'location_info' => $location->toArray()
                        ]);
                        DB::commit();
                        return $this->success(CartResource::make($carts), message: _trans('Done adding delivery price'));
                    }
                    return $this->failure(_trans('This place is out of pronunciation delivery'));

                }
                return $this->failure(_trans('Please add to cart first'));

            }
            return $this->failure(_trans('Not found this customer'));
        } catch (\Exception $exception) {
            return $this->exception($exception);
        }
    }

    public function coupon(CouponRequest $request)
    {
        try {
            DB::beginTransaction();
            $customer = $this->customer($request);
            if ($customer) {
                $cart = $this->carts($customer->id)->first();

                if (empty($cart)) {
                    return $this->failure(message: _trans('please add item in cart first'));
                } else {
                    $coupon = Coupon::query()->whereCode($request->code)->first();
                    $userCoupon = UserCoupon::where([
                        ['customer_id', '=', $customer->id],
                        ['coupon_id', '=', $coupon->id],
                    ])->count();
                    if ($userCoupon == $coupon->limit) {
                        return $this->failure(message: _trans('This coupon is used before'));
                    } else {
                        if ($cart->restaurant_id == $coupon->restaurant_id) {
                            $cartDetail = CartDetail::with(['cartProduct', 'extras'])->whereCartId($cart->id)->get();
                            $total = 0;
                            foreach ($cartDetail as $details) {
                                foreach ($details->cartProduct as $product) {
                                    $total += $product->total_price;
                                }
                                foreach ($details->extras as $extras) {
                                    $total += $extras->total_price;
                                }
                            }
                            if ($coupon->min_purchase < $total) {
                                if ($coupon->discount_type == DiscountTypeCoupon::Percentage->value) {
                                    $discount = calculatePercentage($total, $coupon->discount, 'decrease');
                                    $cart->update(['coupon_code' => $coupon->code, 'coupon_price' => $discount['total_amount']]);
                                } else {
                                    $cart->update(['coupon_code' => $coupon->code, 'coupon_price' => $coupon->discount]);

                                }
                            }
                        }
                        return $this->failure(message: _trans('Please check your code in the same furniture as the card'));
                    }
                }
            }
            return $this->failure(_trans('Not found this customer'));

        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->exception($exception);
        }
    }


}
