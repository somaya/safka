<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\Setting\Utility;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\AuthRequest;
use App\Interfaces\UploadFile\UploadFileRepositoryInterface;
use App\Mail\ResetPassword;
use App\Models\Customer;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class AuthController extends Controller
{

    private UploadFileRepositoryInterface $uploadFileRepositoryInterface;

    public function __construct(UploadFileRepositoryInterface $uploadFileRepositoryInterface)
    {
        $this->uploadFileRepositoryInterface = $uploadFileRepositoryInterface;
        $this->middleware(['guest'])->only(['resetPassword', 'postResetPassword', 'reset', 'postReset']);
    }


    public function resetPassword(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            return view('customer.auth.email');
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }

    }

    public function postResetPassword(AuthRequest $request): \Illuminate\Http\RedirectResponse
    {
        try {
            DB::beginTransaction();
            $user = User::query()
                ->where([
                    'userable_type' =>Customer::class,
                    'email' => $request->email,
                ])->first();
            if (!$user) {
                return redirect()->back()->with('warning', _trans('This account not available'));
            }
            $token = Str::random(70);
            $data = DB::table('password_resets')->updateOrInsert([
                'email' => $user->email,
            ], [
                'email' => $user->email,
                'token' => $token,
                'created_at' => Carbon::now()
            ]);
            if ($data) {
                Mail::to($user->email)->send(new ResetPassword(['data' => $user, 'token' => $token]));
                DB::commit();
                return redirect()->back()
                    ->with('success', _trans('Done Rest link is sent'))
                    ->withInput($request->all());
            }
            return redirect()->back()->with('error', _trans('Please try again'))->withInput($request->all());

        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage())
                ->withInput($request->all());
        }
    }

    public function getReset($token): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $data = DB::table('password_resets')
                ->where('token', '=', $token)
                ->where('created_at', '>', now()->subHours(2))->first();

            if (!empty($data)) {
                return view('customer.auth.reset', compact('data'));
            } else {
                return redirect()->route('user.get.reset.password')->with('warning', _trans('This link is expired'));
            }
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }

    }

    public function postReset(AuthRequest $request, $token): \Illuminate\Http\RedirectResponse
    {
        try {
            $checkToken = DB::table('password_resets')
                ->where('token', '=', $token)
                ->where('created_at', '>', now()->subHours(2))->first();
            if (empty($checkToken))
                return redirect()->route('user.get.reset.password')->with('warning', _trans('This link is expired'));;
            $user = User::query()->where([
                'userable_type' => Customer::class,
                'email' => $checkToken->email,
            ])->first();
            if (!$user)
                return redirect()->back()->with('warning', _trans('This account not available'));
            $user->update([
                'email' => $checkToken->email,
                'password' => $request->password
            ]);
            DB::table('password_resets')->where('email', '=', $request->email)->delete();
            return redirect(Utility::getValByName('link_website'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage())->withInput($request->all());
        }
    }

    public function profile(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $user = Auth::user();
            return view('admin.profile.form', compact('user'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }

    }

    public function storeProfile(AuthRequest $request): \Illuminate\Http\RedirectResponse
    {
        try {
            $user = Auth::user();
            $data = $request->validated();
            if ($request->hasFile('avatar')) {
                $data['avatar'] = $this->uploadFileRepositoryInterface->upload([
                    'file' => 'avatar',
                    'path' => 'admin',
                    'upload_type' => 'single',
                    'delete_file' => emptyValue($user->avatar)
                ]);
            }
            $user->update($data);
            return redirect()->back()->with('success', _trans('Done Update profile Successfully'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage())->withInput($request->all());
        }
    }


    public function changePassword(AuthRequest $request): \Illuminate\Http\RedirectResponse
    {
        try {
            $user = Auth::user();
            if (Hash::check($request->old_password, $user->password)) {

                if ($user->update(['password' => $request->new_password])) {
                    return redirect()->back()->with('success', _trans('Password changed successfully'));
                }
                return redirect()->back()->with('error', _trans('Please try again'))->withInput($request->all());
            }
            return redirect()->back()->with('warning', _trans('Please make sure your old password correct'))->withInput($request->all());

        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage())->withInput($request->all());
        }
    }


}
