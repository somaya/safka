<?php

namespace App\Http\Controllers\Dashboard;

use App\Enums\Status;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\AuthRequest;
use App\Interfaces\Auth\AuthRepositoryInterface;
use App\Mail\ResetPassword;
use App\Models\Admin;
use App\Models\Owner;
use App\Models\ShopStaff;
use App\Models\User;
use App\Traits\UploadFileTrait;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    private AuthRepositoryInterface $authRepositoryInterface;
    use UploadFileTrait;

    public function __construct(AuthRepositoryInterface $authRepositoryInterface)
    {
        $this->authRepositoryInterface = $authRepositoryInterface;
        $this->middleware(['guest:web'])->only(['showLoginForm', 'login', 'resetPassword', 'postResetPassword', 'reset', 'postReset']);
        $this->middleware(['auth:web'])->only(['logout', 'profile', 'storeProfile', 'changePassword']);
    }

    public function showLoginForm(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $routeLogin = route('owner.login');
            $resetPassword = route('owner.reset.password');
            return view('auth.login', compact('routeLogin', 'resetPassword'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }

    }

    public function login(AuthRequest $request): \Illuminate\Http\RedirectResponse
    {
        try {
            if (Auth::guard('web')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
                // if successful, then redirect to their intended location
                if (Auth::guard('web')->user()->status == Status::Active->value) {
                    return match (Auth::user()->userable_type) {
                       Owner::class,ShopStaff::class => redirect()->intended(route('owner.dashboard')),
                        default => redirect()->intended(route('owner.logout')),
                    };
                }
                Auth::guard('web')->logout();
                return redirect()->back()->with('warning', _trans('Your account has been blocked'))
                    ->withInput($request->only('email', 'remember'));
            }
            // if unsuccessful, then redirect back to the login with the form data
            return redirect()->back()->with('warning', _trans('Please Check your email and password is correct'))
                ->withInput($request->only('email', 'remember'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage())->withInput($request->only('email', 'remember'));
        }
    }

    public function logout(): \Illuminate\Http\RedirectResponse
    {
        try {
            $this->authRepositoryInterface->logout('web');
            return redirect()->route('owner.login');
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }

    }

    public function resetPassword(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $routeForgetPassword = route('owner.reset.password');
            $routeLogin = route('owner.login');

            return view('auth.passwords.email', compact('routeForgetPassword', 'routeLogin'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }

    }

    public function postResetPassword(AuthRequest $request): \Illuminate\Http\RedirectResponse
    {
        $user = User::query()->whereEmail($request->email)->first();
        $token = Str::random(70);
        DB::table('password_resets')->updateOrInsert([
            'email' => $user->email,
        ], [
            'email' => $user->email,
            'token' => $token,
            'created_at' => Carbon::now()
        ]);
        $user['url'] = route('owner.reset', $token);
        try {
            Mail::to($user->email)->send(new ResetPassword(['data' => $user, 'token' => $token]));
            return redirect()->route('owner.login')->with('success', _trans('Done send reset link to account'));

        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage())->withInput($request->all());
        }
    }

    public function reset($token): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        $reset = DB::table('password_resets')
            ->where([
                ['token', '=', $token],
                ['created_at', '>', Carbon::now()->subHours(2)],
            ])->first();
        if ($reset) {
            $routeReset = route('owner.reset', $token);
            return view('auth.passwords.reset', compact('routeReset', 'reset'));
        } else {
            return redirect()->route('admin.reset.password');
        }
    }

    public function postReset(AuthRequest $request, $token): \Illuminate\Http\RedirectResponse
    {
        $reset = DB::table('password_resets')
            ->where([
                ['token', '=', $token],
                ['created_at', '>', Carbon::now()->subHours(2)],
            ])
            ->first();

        if ($reset) {
            User::query()->where('email', '=', $reset->email)->update([
                'email' => $reset->email,
                'password' => Hash::make($request->password)
            ]);
            DB::table('password_resets')->where('email', '=', $request->email)->delete();
            return redirect()->route('owner.login')->with('success', _trans('Done reset password please login'));

        }
        return redirect()->route('owner.reset.password');
    }

    public function profile(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {

        $user = User::query()->with(['role'])->findOrFail(\auth()->user()->id);
        return view('dashboard.profile.form', compact('user'));
    }

    public function storeProfile(AuthRequest $request): \Illuminate\Http\RedirectResponse
    {
        $data = $request->validated();
        $user = User::query()->with(['role'])->findOrFail(\auth()->user()->id);

        if ($request->hasFile('avatar')) {
            $data['avatar'] = $this->upload([
                'file' => 'avatar',
                'path' => 'user',
                'upload_type' => 'single',
                'delete_file' => $admin->avatar ?? ''
            ]);
        }
        $user->update($data);
        return redirect()->back()->with('success', _trans('Done Update profile Successfully'));

    }


    public function changePassword(AuthRequest $request): \Illuminate\Http\RedirectResponse
    {
        $user = Auth::guard('web')->user();
        if ($this->authRepositoryInterface->changePassword($user, $request)) {
            return redirect()->back()->with('success', _trans('Password changed successfully'));
        }
        return redirect()->back()->withErrors([
            'old_password' => _trans('Please make sure your current password correct')
        ])->withInput($request->all());
    }
}
