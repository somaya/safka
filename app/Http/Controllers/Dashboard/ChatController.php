<?php

namespace App\Http\Controllers\Dashboard;

use App\Events\SendNotificationUser;
use App\Http\Controllers\Controller;
use App\Http\Resources\Message\ConversationsResource;
use App\Http\Resources\Message\MessageResource;
use App\Models\Conversation;
use App\Models\Message;
use App\Traits\ApiResponses;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ChatController extends Controller
{
    use ApiResponses;

    public function __construct()
    {
        $this->middleware(['permission:Chat list'])->only(['index']);
    }

    public function index()
    {
        $conversations = Conversation::query()
            ->whereHas('order', function ($query) {
                $query->whereIn('shop_id', userType());
            })
            ->with(['order', 'lastMessage'])
            ->withCount(['messages as message_not_read' => function ($query) {
                $query->where('read', '=', 0);
            }])->whereHas('order.shop', function ($query) {
                $query->whereIn('id', userType());
            })->with(['customer'])->orderByDesc('last_time_message')->get();

        $data = json_encode($conversations->toArray(), true);
        return view('dashboard.chat.index', compact('conversations', 'data'));
    }

    public function messages(Request $request)
    {
        $conversation = Conversation::query()->with(['customer'])->whereHas('order', function ($query) {
            $query->whereIn('shop_id', userType());
        })->find($request->conversation_id);
        if ($conversation) {
            $customer = $conversation->customer;
            $messages = Message::query()->whereConversionId($conversation->id);
            Message::query()->where([
                'conversion_id' => $conversation->id,
                'read' => 0,
            ])->update(['read' => 1]);
            $messages = $messages->orderBy('created_at')->get();
            return $this->success([
                'view' => view('dashboard.chat.messages', compact('messages', 'customer', 'conversation'))->render(),
            ]);
        }
    }

    public function sendMessage(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'conversation_id' => ['required', 'integer', Rule::exists('conversations', 'id')],
                'message' => ['required', 'string', 'min:2', 'max:255'],
            ]);

            if ($validator->fails()) {
                return $this->success( $validator->errors()->first(),  false);
            }
            $conversation = Conversation::query()
                ->with(['customer.fcmTokens'])->whereHas('order.shop', function ($query) {
                    $query->whereIn('id', userType());
                })->find($request->conversation_id);
            if (!$conversation) {
                return $this->success( _trans('not found this conversation'), false);
            }
            $message = $conversation->messages()->create([
                'sender_id' => $conversation->receiver_id,
                'receiver_id' => $conversation->sender_id,
                'user_id' => auth()->user()->id,
                'read' => false,
                'body' => $request->message,
                'type' => 'text',
            ]);
            Message::query()->where([
                'conversion_id' => $conversation->id,
                'read' => 0,
            ])->update(['read' => 1]);
            $token = $conversation->customer->fcmTokens()->pluck('fcm_token')->toArray();
            $conversation->update(['last_time_message' => now()]);
            event(new SendNotificationUser([
                'notification_type' => 'chat',
                'order_id' => $conversation->order_id,
                'message' => MessageResource::make($message),
            ], $token, $message->body, $message->body));
            return $this->success([
                'view' => view('dashboard.chat.message', compact('message'))->render()
            ]);
            //return $this->apiResponse(MessageResource::make($message));
        } catch (\Exception $exception) {
            return $this->success( $exception->getMessage());
        }
    }

    public function search(Request $request)
    {
        $conversations = Conversation::query()->with(['order'])
            ->withCount(['messages as message_not_read' => function ($query) {
                $query->where('read', '=', 0);
            }])
            ->orWhereRelation('order', 'code', 'LIKE', '%' . $request->search . '%')
            ->orWhereRelation('customer.user', 'name', 'LIKE', '%' . $request->search . '%')
            ->whereHas('order', function ($query) {
                $query->whereIn('shop_id', userType());
            })->with(['customer', 'lastMessage'])->orderByDesc('last_time_message')->get();

        return $this->success(ConversationsResource::collection($conversations));
    }
}
