<?php

namespace App\Http\Controllers\Dashboard;

use App\Enums\UserType;
use App\Http\Controllers\Controller;
use App\Models\Conversation;
use App\Models\Discount;
use App\Models\Extra;
use App\Models\Offer;
use App\Models\Order;
use App\Models\Payment;
use App\Models\Product;
use App\Models\Save;
use App\Models\Shop;
use App\Models\ShopStaff;
use Illuminate\Http\Request;
use LaravelDaily\Invoices\Classes\Buyer;
use LaravelDaily\Invoices\Classes\InvoiceItem;
use LaravelDaily\Invoices\Invoice;

class DashboardController extends Controller
{

    public function dashboard(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Contracts\Foundation\Application
    {
        $all_orders = Order::query()->whereIn('shop_id', userType())->count();

        $shops = Shop::query()->whereIn('id', userType())->count();

        $offers = Offer::query()->whereIn('shop_id', userType())->count();

        $saves = Save::query()->whereIn('shop_id', userType())->count();

        $discounts = Discount::query()->whereIn('shop_id', userType())->count();

        $products = Product::query()->whereIn('shop_id', userType())->count();


        $delivery_price = Order::query()->whereIn('shop_id', userType())
            ->where('status_pay', 'paid')->sum('delivery_price');

        $cash_orders = Order::query()->whereIn('shop_id', userType())
            ->where(['status_pay' => 'paid', 'payment_method' => 'cash'])->count();

        $credit_orders = Order::query()->whereIn('shop_id', userType())
            ->where(['status_pay' => 'paid', 'payment_method' => 'visa'])->count();

        $staffs = ShopStaff::query()->whereIn('shop_id', userType())
            ->whereRelation('user', 'userable_type', '=', ShopStaff::class)->count();

        $complaints = Conversation::query()->whereHas('order', function ($query) {
            $query->whereIn('shop_id', userType());
        })->groupBy('sender_id')->count();
        return view('dashboard.dashboard', compact('all_orders', 'shops', 'offers', 'saves', 'discounts',
            'products', 'delivery_price', 'cash_orders', 'credit_orders', 'staffs', 'complaints'));
    }


    public function payments(Request $request)
    {
        try {
            $payments = Payment::query()->with(['shop' => [
                'owner.user',
                'country.currency'
            ]])->orderByDesc('created_at')
                ->whereIn('shop_id', userType())
                ->when($request->payment, function ($q) use ($request) {
                    $q->where('id', '=', $request->payment);
                });
            $payments = Payment::allPayments($payments);
            $columns = [
                'all' => _trans('All'),
                'code' => _trans('Payment code'),
                'shop' => _trans('Shop name'),
            ];
            return view('dashboard.payment.index', compact('payments', 'columns'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

}
