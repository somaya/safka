<?php

namespace App\Http\Controllers\Dashboard;

use App\Helpers\CPU\Models;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\DeliveryRequest;
use App\Models\Areas;
use App\Models\DeliveryArea;
use App\Models\Owner;
use App\Models\ShopStaff;
use App\Traits\ApiResponses;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DeliveryAreaController extends Controller
{
    use ApiResponses;
    public function __construct()
    {
        $this->middleware(['permission:Delivery list'])->only(['index']);
        $this->middleware(['permission:Delivery add'])->only(['create', 'store']);
        $this->middleware(['permission:Delivery edit'])->only(['edit', 'update', 'updateStatus']);
        $this->middleware(['permission:Delivery delete'])->only(['destroy']);
    }

    public function index(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $query = DeliveryArea::query()->with(['shop', 'owner.user'])
                ->orderByDesc('created_at');
            if (auth()->user()->userable_type == Owner::class || auth()->user()->userable_type ==ShopStaff::class  ) {
                $query = $query->whereIn('shop_id', userType());
            }
            $data = DeliveryArea::allAreas($query);
            $columns = [
                'all' => _trans('All'),
                'code' => _trans('Area Code'),
            ];
            if (auth()->user()->userable_type == Owner::class || auth()->user()->userable_type ==ShopStaff::class) {
                $columns += [
                    'shop' => _trans(' Branch name'),
                ];
            } else {
                $columns += [
                    'owner' => _trans('Owner name'),
                    'Shop' => _trans('Branch name'),
                ];
            }

            return view('dashboard.delivery_area.index', compact('data', 'columns'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }


    public function create(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $edit = false;
            $user = auth()->user();
            if (\request('guard')=='admin') {
                $data = Models::ownerAndShop(0)['owner'];
            } else {
                $data = Models::ownerAndShop($user->id)['shop'];
            }
            $countries = Models::countries();
            return view('dashboard.delivery_area.form', compact('edit', 'data', 'countries'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    private function areas($delivery, $areas, $type = 'store')
    {
        foreach ($areas as $value) {
            $regions = Arr::exists($value, 'region_id') ? $value['region_id'] : [];
            $areaId = Arr::exists($value, 'id') ? $value['id'] : null;
            $data = [
                'governorate_id' => $value['governorate_id'],
                'min_time' => $value['min_time'],
                'max_time' => $value['max_time'],
                'delivery_price' => $value['delivery_price'],
                'min_order_price' => $value['min_order_price'],
            ];
            if ($type == 'store') {
                $area = $delivery->areas()->create($data);
                $area->regions()->attach($regions);
            } else {
                if ($areaId) {
                    $area = $delivery->areas()->updateOrCreate([
                        'id' => $areaId
                    ], $data);
                    $area->regions()->sync($regions);
                } else {
                    $area = $delivery->areas()->create($data);
                    $area->regions()->attach($regions);
                }
            }
        }

    }

    public function store(DeliveryRequest $request): string|\Illuminate\Http\RedirectResponse
    {
        try {
            DB::beginTransaction();
            $data = $request->validated();
            if (auth()->user()->userable_type ==Owner::class  || auth()->user()->userable_type ==ShopStaff::class ) {
                $data['owner_id'] = getOwner();
            }

            $delivery = DeliveryArea::query()->create($data);
            if ($delivery) {
                $this->areas($delivery, $request->areas);
                DB::commit();
                return redirect()->route('delivery-area.index')->with('success', _trans('Done Save Data Successfully'));
            }
            return redirect()->back()->with('warning', _trans('Some failed errors'))->withInput($data);
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage())->withInput($request->all());
        }
    }


    public function edit($id): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {

        try {
            $edit = true;
            $user = auth()->user();
            $delivery_area = DeliveryArea::query()->with(['areas'])->find($id);
            if (\request('guard')=='admin') {
                $data = Models::ownerAndShop(0)['owner'];
            } else {
                $data = Models::ownerAndShop($user->id)['shop'];
            }
            $countries = Models::countries();
            return view('dashboard.delivery_area.form', compact('edit', 'delivery_area', 'data', 'countries'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function update(DeliveryRequest $request, $id): string|\Illuminate\Http\RedirectResponse
    {
        try {
            DB::beginTransaction();
            $data = $request->validated();
            $deliveryArea = DeliveryArea::query()->findOrFail($id);
            if ($deliveryArea->update($data)) {
                $this->areas($deliveryArea, $request->areas, 'update');
                DB::commit();
                return redirect()->route('delivery-area.index')->with('success', _trans('Done Save Data Successfully'));
            }
            return redirect()->back()->with('warning', _trans('Some failed errors'))->withInput($data);
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage())->withInput($request->all());
        }
    }

    public function destroy(DeliveryArea $area): \Illuminate\Http\RedirectResponse
    {
        try {
            DB::beginTransaction();
            $status = !$area->status;
            if ($area->update(['status' => $status])) {
                DB::commit();
                return redirect()->route('delivery-area.index')->with('success', _trans('Done Updated Data Successfully'));
            }
            return redirect()->back()->with('warning', _trans('Some failed errors'));
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }


    public function updateStatus(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            DB::beginTransaction();
            $area = DeliveryArea::query()->findOrFail($request->id);
            if (!$area) {
                return $this->success( _trans('Not Found'), false);
            }
            $status = !$area->status;
            if ($area->update(['status' => $status])) {
                DB::commit();
                return $this->success(_trans('Done Updated Data Successfully'), '');
            }
            return $this->success( _trans('Some failed errors'), false);
        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->success( $exception->getMessage(), false);
        }
    }

    public function updateAreaStatus(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            DB::beginTransaction();
            $area = Areas::query()->findOrFail($request->id);
            if (!$area) {
                return $this->success( _trans('Not Found'), false);
            }
            $status = !$area->status;
            if ($area->update(['status' => $status])) {
                DB::commit();
                return $this->success(_trans('Done Updated Data Successfully'));
            }
            return $this->success(_trans('Some failed errors'), false);
        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->success($exception->getMessage(), false);
        }
    }
}
