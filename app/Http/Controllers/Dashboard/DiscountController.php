<?php

namespace App\Http\Controllers\Dashboard;

use App\Helpers\CPU\Models;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\DiscountRequest;
use App\Models\Discount;
use App\Models\Order;
use App\Models\Owner;
use App\Models\Product;
use App\Models\ShopStaff;
use App\Traits\ApiResponses;
use Illuminate\Http\Request;

class DiscountController extends Controller
{
    use ApiResponses;

    public function __construct()
    {
        $this->middleware(['permission:Discount list'])->only(['index']);
        $this->middleware(['permission:Discount add'])->only(['create', 'store']);
        $this->middleware(['permission:Discount edit'])->only(['edit', 'update', 'updateStatus']);
        $this->middleware(['permission:Discount delete'])->only(['destroy']);
    }

    public function index(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $query = Discount::query()->orderBy('created_at','DESC')->with(['owner.user', 'shop', 'product']);
            if (auth()->user()->userable_type == Owner::class || auth()->user()->userable_type ==ShopStaff::class  ) {
                $query = $query->whereIn('shop_id', userType());
            }
            $data = Discount::allDiscounts($query);
            $columns = [
                'all' => _trans('All'),
                'code' => _trans('Discount Code'),
                'product' => _trans('Product name'),
            ];
            if (auth()->user()->userable_type == Owner::class || auth()->user()->userable_type ==ShopStaff::class) {
                $columns += [
                    'shop' => _trans('Shop name'),
                ];
            } else {
                $columns += [
                    'owner' => _trans('Owner name'),
                    'Shop' => _trans('Shop name'),
                ];
            }
            return view('dashboard.discount.index', compact('data', 'columns'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function create(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        $edit = false;
//        $owners = Models::owners()->get();
        $user = auth()->user();
        if (\request('guard')=='admin') {
            $data = Models::ownerAndShop(0)['owner'];
        } else {
            $data = Models::ownerAndShop($user->id)['shop'];
        }
        return view('dashboard.discount.form', compact('edit', 'data'));
    }

    private function onData($request)
    {
        $product = Product::query()->findOrFail($request->product_id);
        $data = $request->validated();
        if (auth()->user()->userable_type ==Owner::class  || auth()->user()->userable_type ==ShopStaff::class ) {
            $data['owner_id'] = getOwner();
        }
        $price_after = round($product->price - ($product->price * $request->percent / 100), 2);
        $data['price_before'] = $product->price;
        $data['price_after'] = $price_after;
        return $data;
    }

    public function store(DiscountRequest $request)
    {
        Discount::query()->create($this->onData($request));
        return redirect()->route('discount.index')->with('success', _trans('Done Save Data Successfully'));
    }

    public function show($id)
    {
        try {
            $discount = Discount::query()->findOrFail($id);
            $orders = Order::whereHas('orderDetails', function ($q) use ($id) {
                $q->where('modelable_id', $id)->where('modelable_type', Discount::class);
            })->get();
            return view('dashboard.discount.show', compact('discount', 'orders'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }

    }

    public function edit(Discount $discount): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        $edit = true;
//        $owners = Models::owners()->get();
        $user = auth()->user();
        if (\request('guard')=='admin') {
            $data = Models::ownerAndShop(0)['owner'];
        } else {
            $data = Models::ownerAndShop($user->id)['shop'];
        }
        return view('dashboard.discount.form', compact('edit', 'discount', 'data'));
    }

    public function update(DiscountRequest $request, Discount $discount): \Illuminate\Http\RedirectResponse
    {
        $discount->update($this->onData($request));
        return redirect()->route('discount.index')->with('success', _trans('Done Updated Data Successfully'));
    }


    public function destroy(Discount $discount)
    {
        //
    }

    public function updateStatus(Request $request): \Illuminate\Http\JsonResponse
    {
        $discount = Discount::query()->findOrFail($request->id);
        if (!$discount) {
            return $this->success(_trans('Not Found'), status: false);
        }
        $status = !$discount->status;
        $discount->update(['status' => $status]);
        return $this->success(_trans('Done Updated Data Successfully'));
    }
}
