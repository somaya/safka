<?php

namespace App\Http\Controllers\Dashboard;

use App\Enums\OwnerType;
use App\Helpers\CPU\Models;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\DriverRequest;
use App\Http\Requests\Dashboard\StaffRequest;
use App\Models\Driver;
use App\Models\Owner;
use App\Models\ShopStaff;
use App\Traits\ApiResponses;
use App\Traits\UploadFileTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DriverController extends Controller
{

    use UploadFileTrait, ApiResponses;


    public function __construct()
    {
        $this->middleware(['permission:Driver list'])->only(['index']);
        $this->middleware(['permission:Driver add'])->only(['create', 'store']);
        $this->middleware(['permission:Driver edit'])->only(['edit', 'update', 'updateStatus']);
        $this->middleware(['permission:Driver delete'])->only(['destroy']);

    }

    public function index(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $query = Driver::query()->withCount(['owner', 'orders'])
                ->orderByDesc('created_at');
            if (auth()->user()->userable_type == Owner::class) {
                $query = $query->where([
                    'owner_type' => OwnerType::Owner->value,
                    'owner_id' => auth()->user()->userable->id,
                ])->with(['shop','owner.user']);
            }
            elseif (auth()->user()->userable_type == ShopStaff::class) {
                $query = $query->where([
                    'owner_type' => OwnerType::Owner->value,
                    'owner_id' => auth()->user()->userable->owner->id,
                ])->whereIn('shop_id', userType())->with(['shop','owner.user']);
            }
            else {
                $query = $query->with(['shop','owner.user']);
            }
            $data = Driver::allDrivers($query);
            $columns = [];
            if (auth()->user()->userable_type != ShopStaff::class) {
                $columns += [
                    'all' => _trans('All'),
                ];
            }
            $columns += [
                'code' => _trans('Code'),
                'name' => _trans('Name'),
            ];
            if (\request('guard')=='admin') {
                $columns += [
                    'owner_name' => _trans('Owner name'),
                    'shop_name' => _trans('Shop name'),
                ];
            }
            if (auth()->user()->userable_type == Owner::class) {
                $columns += [
                    'shop_name' => _trans('Shop name'),
                ];
            }
            $columns += [
                'phone' => _trans('Phone'),
            ];

            return view('dashboard.driver.index', compact('data', 'columns'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }


    public function create(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $edit = false;
            $vehicleTypes = Models::vehicleTypes();
            $user = auth()->user();
            if (in_array($user->userable_type, [Owner::class,ShopStaff::class])) {
                $shops = Models::ownerShop();
            } else {
                $shops = [];
            }
            return view('dashboard.driver.form', compact('edit', 'vehicleTypes', 'shops'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }


    public function store(DriverRequest $request): \Illuminate\Http\RedirectResponse
    {
        try {
            DB::beginTransaction();
            $dataDriver = $request->only(['owner_type', 'owner_id', 'shop_id', 'name', 'avatar',
                'address', 'birthday', 'phone', 'notional_id', 'email', 'password', 'percentage']);

            $dataLicenses = $request->only(['license_number', 'license_expiry', 'license_image']);

            $dataVehicles = $request->only(['vehicle_type_id', 'vehicle_model', 'plate_number',
                'vehicle_license_photo', 'vehicle_license_expiry', 'vehicle_image']);

            if ($request->hasFile('avatar')) {
                $dataDriver['avatar'] =
                    $this->upload([
                        'file' => 'avatar',
                        'path' => 'driver',
                        'upload_type' => 'single',
                        'delete_file' => ''
                    ]);
            }
            if (in_array(auth()->user()->userable_type, [Owner::class , ShopStaff::class])) {
                $dataDriver['owner_type'] = 'owner';
                $dataDriver['owner_id'] = getOwner();
            }
            $driver = Driver::query()->create($dataDriver);
            if ($driver) {
                /* license */
                if ($request->hasFile('license_image')) {
                    $dataLicenses['license_image'] =
                        $this->upload([
                            'file' => 'license_image',
                            'path' => 'driver',
                            'upload_type' => 'single',
                            'delete_file' => ''
                        ]);

                }
                $driver->license()->updateOrCreate([
                    'driver_id' => $driver->id
                ], $dataLicenses);;

                /* vehicle license */
                if ($request->hasFile('vehicle_license_image')) {
                    $dataVehicles['vehicle_license_image'] =
                        $this->upload([
                            'file' => 'vehicle_license_image',
                            'path' => 'driver',
                            'upload_type' => 'single',
                            'delete_file' => ''
                        ]);

                }
                if ($request->hasFile('license_image')) {
                    $dataVehicles['vehicle_image'] =
                        $this->upload([
                            'file' => 'vehicle_image',
                            'path' => 'driver',
                            'upload_type' => 'single',
                            'delete_file' => ''
                        ]);
                }
                $driver->vehicle()->updateOrCreate([
                    'driver_id' => $driver->id
                ], $dataVehicles);;
                DB::commit();
                return redirect()->route('driver.index')->with('success', _trans('Done Save Data Successfully'));
            }
            return redirect()->back()->with('warning', _trans('Some failed errors'))->withInput($request->all());
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage())
                ->withInput($request->all());
        }

    }

    public function show($id)
    {
        //
    }

    public function edit($id): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $edit = true;
            $driver = Driver::query();
            $user = auth()->user();
            if (in_array($user->userable_type, [Owner::class, ShopStaff::class])) {
                $shops = Models::ownerShop();
                $driver = $driver->where([
                    'owner_type' => OwnerType::Owner->value,
                    'owner_id' => getOwner(),
                ])->whereIn('shop_id', userType());
            } else {
                $shops = [];
            }
            $driver = $driver->find($id);
            if (!$driver) {
                return redirect()->back()->with('warning', _trans('Not allow access here'));
            }
            $vehicleTypes = Models::vehicleTypes();
            return view('dashboard.driver.form', compact('edit', 'vehicleTypes', 'driver', 'shops'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function update(DriverRequest $request, $id): \Illuminate\Http\RedirectResponse
    {
        try {
            DB::beginTransaction();
            $dataDriver = $request->only(['owner_type', 'owner_id', 'shop_id', 'name', 'avatar',
                'address', 'birthday', 'phone', 'notional_id', 'email', 'percentage']);

            $dataLicenses = $request->only(['license_number', 'license_expiry', 'license_image']);

            $dataVehicles = $request->only(['vehicle_type_id', 'vehicle_model', 'plate_number',
                'vehicle_license_photo', 'vehicle_license_expiry', 'vehicle_image']);

            $driver = Driver::query()->with(['license', 'vehicle']);
            if (in_array(auth()->user()->userable_type, [Owner::class , ShopStaff::class])) {
                $driver = $driver->where([
                    'owner_type' => OwnerType::Owner->value,
                    'owner_id' => getOwner(),
                ])->whereIn('shop_id', userType());
            }

            $driver = $driver->find($id);
            if (!$driver) {
                return redirect()->back()->with('warning', _trans('Not allow access here'));
            }
            if ($request->hasFile('avatar')) {
                $dataDriver['avatar'] =
                    $this->upload([
                        'file' => 'avatar',
                        'path' => 'driver',
                        'upload_type' => 'single',
                        'delete_file' => $driver->avatar ?? ''
                    ]);
            }
            if (in_array(auth()->user()->userable_type, [Owner::class,ShopStaff::class])) {
                $dataDriver['owner_type'] = 'owner';
                $dataDriver['owner_id'] = getOwner();
            }
            if ($driver->update($dataDriver)) {
                /* license */
                if ($request->hasFile('license_image')) {
                    $dataLicenses['license_image'] =
                        $this->upload([
                            'file' => 'license_image',
                            'path' => 'driver',
                            'upload_type' => 'single',
                            'delete_file' => $driver->license->license_image ?? ''
                        ]);

                }
                $driver->license()->updateOrCreate([
                    'driver_id' => $driver->id
                ], $dataLicenses);

                /* vehicle license */
                if ($request->hasFile('vehicle_license_image')) {
                    $dataVehicles['vehicle_license_image'] =
                        $this->upload([
                            'file' => 'vehicle_license_image',
                            'path' => 'driver',
                            'upload_type' => 'single',
                            'delete_file' => $driver->vehicle->vehicle_license_image ?? ''
                        ]);

                }
                if ($request->hasFile('license_image')) {
                    $dataVehicles['vehicle_image'] =
                        $this->upload([
                            'file' => 'vehicle_image',
                            'path' => 'driver',
                            'upload_type' => 'single',
                            'delete_file' =>$driver->vehicle->vehicle_image ?? ''
                        ]);

                }
                $driver->vehicle()->updateOrCreate([
                    'driver_id' => $driver->id
                ], $dataVehicles);
                DB::commit();
                return redirect()->route('driver.index')->with('success', _trans('Done Save Data Successfully'));
            }
            return redirect()->back()->with('warning', _trans('Some failed errors'))->withInput($request->all());
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage())->withInput($request->all());
        }
    }


    public function updateStatus(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            DB::beginTransaction();
            $driver = Driver::query();
            if (in_array(auth()->user()->userable_type, [Owner::class,ShopStaff::class])) {
                $driver = $driver->where([
                    'owner_type' => OwnerType::Owner->value,
                    'owner_id' => getOwner(),
                ])->whereIn('shop_id', userType());
            }
            $driver = $driver->findOrFail($request->id);
            if (!$driver) {
                return $this->success( _trans('Not Found'), false);
            }
            $blocked = !$driver->blocked;
            if ($driver->update(['blocked' => $blocked])) {
                DB::commit();
                return $this->success(_trans('Done Updated Data Successfully'), '');
            }
            return $this->success( _trans('Some failed errors'), false);
        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->success( $exception->getMessage(), false);
        }
    }

    public function changePassword(StaffRequest $request): \Illuminate\Http\RedirectResponse
    {
        try {
            DB::beginTransaction();
            $driver = Driver::query();
            if (in_array(auth()->user()->userable_type, [Owner::class , ShopStaff::class])) {
                $driver = $driver->where([
                    'owner_type' => OwnerType::Owner->value,
                    'owner_id' => getOwner(),
                ])->whereIn('shop_id', userType());
            }
            $driver = $driver->find($request->id);
            if (!$driver) {
                return redirect()->back()->with('warning', _trans('Not allowed access here.'));
            }

            if ($driver->update(['password' => $request->password])) {
                DB::commit();
                return redirect()->route('driver.index')->with('success', _trans('Done Updated Data Successfully'));
            }
            return redirect()->back()->with('warning', _trans('Some failed errors'));
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage())->withInput($request->all());
        }

    }
}
