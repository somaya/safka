<?php

namespace App\Http\Controllers\Dashboard;

use App\Enums\OwnerType;
use App\Helpers\CPU\Models;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\DriverOrderRequest;
use App\Models\Driver;
use App\Models\Order;
use App\Models\Owner;
use App\Models\ShopStaff;
use Illuminate\Support\Facades\DB;

class DriverOrderController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:DriverOrder list'])->only(['index']);
        $this->middleware(['permission:DriverOrder add'])->only(['create', 'store']);
        $this->middleware(['permission:DriverOrder edit'])->only(['edit', 'update']);
    }

    public function index(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $user = auth()->user();
            $data = Driver::query()->with(['owner', 'shop', 'orders'])
                ->orderByDesc('created_at')
                ->withCount(['orders'])
                ->withSum('orders', 'total_price')
                ->has('orders')
                ->whereHas('orders', function ($query) {
//                    $query->where('service_type_id', 1); // is delivery
                });
            if (in_array($user->userable_type, [Owner::class,ShopStaff::class])) {
                $data = $data->whereIn('shop_id', userType());
            }
            $data = Driver::allDrivers($data);

            $columns = [
                'all' => _trans('All'),
                'name' => _trans('Driver name'),
                'shop_name' => _trans('Shop name'),
//                'order_code' => _trans('Order code'),
            ];
            return view('dashboard.driver-order.index', compact('data', 'columns'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function create(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $edit = false;
            $user = auth()->user();
            if (in_array($user->userable_type, [Owner::class,ShopStaff::class])) {
                $owners = [];
                $shops = Models::ownerShop();
            } else {
                $owners = Models::owners()->get();
                $shops = [];
            }
            return view('dashboard.driver-order.form', compact('edit', 'owners', 'shops'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function store(DriverOrderRequest $request): \Illuminate\Http\RedirectResponse
    {
        try {
            DB::beginTransaction();
            $user = auth()->user();
            $driver = Driver::query();
            if (in_array($user->userable_type, [Owner::class,ShopStaff::class])) {
                $driver = $driver->whereIn('shop_id', userType());
            } else {
                $driver = $driver->where([
                    'shop_id' => $request->shop_id,
                ])->orWhere('owner_type', '=', OwnerType::System->value);
            }
            $driver = $driver->find($request->driver_id);
            if ($driver) {
                $driver->orders()->detach($request->order_id);
                $driver->orders()->attach($request->order_id);
                $order = Order::query();
                foreach ($request->order_id as $orderId) {
                    $order = $order->find($orderId);
                }
                DB::commit();
                return redirect()->route('driver-order.index')->with('success', _trans('Done add orders to driver successfully'))->withInput($request->all());
            }
            return redirect()->back()->with('warning', _trans('This driver not allowed to access this orders'))->withInput($request->all());
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage())
                ->withInput($request->all());
        }

    }

    public function edit($id): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $edit = true;
            $driver = Driver::query()
                ->has('orders')
                ->with(['orders'])
                ->withCount(['statusOrders'])
                ->whereHas('orders');
            $user = auth()->user();
            if (in_array($user->userable_type, [Owner::class,ShopStaff::class])) {
                $owners = [];
                $shops = Models::ownerShop();
                $driver = $driver->where([
                    'owner_id' => getOwner(),
                ]);

            } else {
                $owners = Models::owners()->get();
                $shops = [];
                $driver = $driver->orWhere('owner_type', '=', OwnerType::System->value);
            }
            $driver =Driver::query()->find($id);
            if (!$driver) {
                return redirect()->back()->with('warning', _trans('Not found this driver'));
            }
            $orders = [];
            if ($driver->orders->count() > 0) {
                $orders = $driver->orders->pluck('id')->toArray();
            }
            return view('dashboard.driver-order.form', compact('edit', 'owners', 'shops', 'driver', 'orders'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function update(DriverOrderRequest $request): \Illuminate\Http\RedirectResponse
    {
        try {
            DB::beginTransaction();
            $user = auth()->user();
            $driver = Driver::query()->withCount(['statusOrders']);
            if (in_array($user->userable_type, [Owner::class,ShopStaff::class])) {
                $driver = $driver->whereIn('shop_id', userType());
            } else {
                $driver = $driver->where([
                    'shop_id' => $request->shop_id,
                ])->orWhere('owner_type', '=', OwnerType::System->value);
            }
            $driver = Driver::query()->find($request->driver_id);

            if ($driver) {
                if ($driver->status_orders_count > 0) {
                    return redirect()->back()->with('warning', _trans('It is not possible to add or modify the requests because this driver left and did not return or change the status of the requests with him to a status completed'))->withInput($request->all());
                }
                $driver->orders()->detach($request->order_id);
                $driver->orders()->attach($request->order_id);
                DB::commit();
                return redirect()->route('driver-order.index')->with('success', _trans('Done add orders to driver successfully'))->withInput($request->all());
            }
            return redirect()->back()->with('warning', _trans('Not found this driver'));
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage())
                ->withInput($request->all());
        }

    }
}
