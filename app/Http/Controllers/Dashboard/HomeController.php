<?php

namespace App\Http\Controllers\Dashboard;

use App\Enums\Direction;
use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\Discount;
use App\Models\Offer;
use App\Models\Product;
use App\Models\Save;
use App\Models\Shop;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class HomeController extends Controller
{
    public function language($local, $guard = null): \Illuminate\Http\RedirectResponse
    {
        try {
            $direction = Direction::RTL->value;
            $language = languages();
            $lang = 'ar';

            foreach ($language as $data) {
                if ($data->code == $local) {
                    $direction = $data->direction ?? Direction::RTL->value;
                    $lang = $data->code;
                }
            }
            if (Arr::exists(config('auth.guards'), $guard)) {
                if (Auth::guard($guard)->check()) {
                    Auth::guard($guard)->user()->update(['lang' => $lang]);
                }
            }
            session()->put('local', $local);
            session()->put('direction', $direction);
            App::setLocale($lang);
            return redirect()->back();
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }
    public function getSharedLinkPreview(Request $request)
    {
        $request->validate(
            [
                'type' => ['required', Rule::in(['product', 'shop','offer','discount','save','blog'])],
                'redirect' => 'required|url',
                'item_id' => 'required',
            ]
        );
        if ($request->type === 'shop') {
            $item = Shop::query()->whereTranslation('slug',$request->item_id, $request->lang ?? locale())->first();
            $arr_path = explode('/', $item->logo);
            $data['image'] = getReducedAvatar($item->logo, end($arr_path));

        }
        elseif ($request->type === 'offer') {
            $item = Offer::query()->whereTranslation('slug',$request->item_id, $request->lang ?? locale())->first();
            $data['image'] = getReducedAvatar($item->images()->first()->full_file, $item->images()->first()->file);

        }
        elseif ($request->type === 'product') {
            $item = Product::query()->whereTranslation('slug',$request->item_id, $request->lang ?? locale())->first();
            $data['image'] = getReducedAvatar($item->images()->first()->full_file, $item->images()->first()->file);
        }
        elseif ($request->type === 'save') {
            $item = Save::query()->whereTranslation('slug',$request->item_id, $request->lang ?? locale())->first();
            $data['image'] = getReducedAvatar($item->images()->first()->full_file, $item->images()->first()->file);

        }
        elseif ($request->type === 'discount') {
            $discount = Discount::query()->findOrFail($request->item_id);
            $item = Product::query()->findOrFail($discount->product_id);
            $data['image'] = getReducedAvatar($item->images()->first()->full_file, $item->images()->first()->file);

        }
        else {
            $item = Blog::query()->whereTranslation('slug',$request->item_id, $request->lang ?? locale())->first();
            $data['image'] = getReducedAvatar($item->images()->first()->full_file, $item->images()->first()->file);

        }
        if ($request->type === 'blog'){
            $data['name'] = wordwrap($item?->translate($request->lang ?? locale())?->title, 35);
            $data['description'] = wordwrap($item?->translate($request->lang ?? locale())?->content, 30);
            $data['description']=strip_tags($data['description']);
            $data['redirect'] = $request->input('redirect');
        }else {
            $data['name'] = wordwrap($item?->translate($request->lang ?? locale())?->name, 35);
            $data['description'] = wordwrap($item?->translate($request->lang ?? locale())?->description, 30);
            $data['description']=strip_tags($data['description']);
            $data['redirect'] = $request->input('redirect');
        }
        return view('for-frontend.link-preview', compact('data'));
    }
}
