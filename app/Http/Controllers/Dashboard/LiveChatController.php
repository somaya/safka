<?php

namespace App\Http\Controllers\Dashboard;

use App\Events\SendNotificationUser;
use App\Http\Controllers\Controller;
use App\Http\Resources\Chat\ChatDetailResource;
use App\Models\_Conversations;
use App\Models\Admin;
use App\Models\Chat;
use App\Models\Notification;
use App\Models\Shop;
use App\Traits\ApiResponses;
use App\Traits\UploadFileTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class LiveChatController extends Controller
{
    use UploadFileTrait, ApiResponses;

    public function __construct()
    {
        $this->middleware(['user-permission:Live#Chat list'])->only(['index']);
        $this->middleware(['user-permission:Live#Chat add'])->only(['create']);
        $this->middleware(['user-permission:Live#Chat edit'])->only(['edit']);
        $this->middleware(['user-permission:Live#Chat delete'])->only(['destroy']);
    }

    public function index($shopId = null, $conversionId = null): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $shops = Shop::query()
                ->when(request('guard') != 'admin', function (Builder $builder) {
                    $ownerId = getOwner();
                    $builder->where('owner_id', '=', $ownerId);
                })
                /*->where([
                    'chat_enable' => 1,
                    'status' => 1,
                ])*/
                ->orderByDesc('created_at')
                ->get();
            if (request('guard') == 'admin') {
                Notification::query()->where([
                    'type' => 'live-chat',
                    'receiverable_type' => Admin::class,
                    'receiverable_id' => Admin::query()->first()?->id,
                ])->update(['read_at' => now()]);
            } else {
                $shop = Shop::query()->find($shopId);
                Notification::query()->where([
                    'type' => 'live-chat',
                    'receiverable_type' => Shop::class,
                    'receiverable_id' => $shop?->owner->user->id,
                ])->update(['read_at' => now()]);
            }
            return view('dashboard.live-chats.index', compact('shops', 'shopId', 'conversionId'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function conversations(Request $request)
    {
        try {
            $conversations = _Conversations::query()
                ->when(request('guard') != 'admin', function (Builder $builder) use ($request) {
                    $builder
                        ->whereHasMorph('receiverable', [Shop::class], function (Builder $query) use ($request) {
                            $ownerId = getOwner(auth()->user());
                            $query->where('id', '=', $request->shop_id)
                                ->where('owner_id', '=', $ownerId)
                                ->where('chat_enable', '=', 1);
                        });
                })
                ->when($request->shop_id, function (Builder $builder) use ($request) {
                    $builder->where([
                        'receiverable_type' => Shop::class,
                        'receiverable_id' => $request->shop_id,
                    ]);
                })
                ->withCount([
                    'chats as chats_un_seen' => function (Builder $builder) use ($request) {
                        $builder->where(function (Builder $builder) use ($request) {
                            $builder->where('seen', '=', 0)
                                ->where('modelable_type', '!=', Shop::class)
                                ->where('modelable_id', '!=', $request->shop_id);
                        });
                    }
                ])
                ->orderByDesc('updated_at')
                ->get();
            return $this->success([
                'view' => view('dashboard.live-chats.conversations', compact('conversations'))->render(),
            ]);

        } catch (\Exception $exception) {
            return $this->success($exception->getMessage(), status: false);
        }
    }

    public function show(Request $request)
    {
        $conversation = _Conversations::query()
            ->where([
                'receiverable_type' => Shop::class,
                'receiverable_id' => $request->shop_id,
            ])
            ->find($request->conversation_id);
        if (!$conversation) {
            return $this->success(_trans('not found this conversation'), false);
        }
        $chats = Chat::query()
            ->where('conversation_id', '=', $request->conversation_id)
            ->orderBy('created_at')
            ->get();

        Chat::query()->where([
            'conversation_id' => $conversation->id,
            'seen' => 0,
        ])->update(['seen' => 1]);
        return $this->success([
            'view' => view('dashboard.live-chats.chats', compact('chats', 'conversation'))->render(),
        ]);


    }

    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'conversation_id' => ['required', 'integer', Rule::exists('_conversations', 'id')],
                'message' => ['required', 'string', 'min:1', 'max:500'],
            ]);
            $shop = Shop::query()->where([
                'id' => $request->shop_id,
                'status' => 1,
                'chat_enable' => 1,
            ])->first();
            if (!$shop) {
                return $this->success(_trans('You can ot chat on this shop close chat or not active'), status: false);
            }

            if ($validator->fails()) {
                return $this->success($validator->errors()->first(), status: false);
            }
            $conversation = _Conversations::query()
                ->where('receiverable_type', '=', Shop::class)
                ->when(request('guard') != 'admin', function (Builder $builder) {
                    $builder->whereHasMorph('receiverable', [Shop::class], function (Builder $query) {
                        $ownerId = getOwner(auth()->user());
                        $query->where('owner_id', '=', $ownerId);
                    });
                })
                ->find($request->conversation_id);
            if (!$conversation) {
                return $this->success(_trans('not found this conversation'), status: false);
            }
            $chat = Chat::query()->create([
                'conversation_id' => $conversation->id,
                'modelable_type' => $conversation->receiverable_type,
                'modelable_id' => $conversation->receiverable_id,
                'message' => $request->message,
            ]);

            $token = $conversation->senderable->fcmTokens()->pluck('fcm_token')->toArray();
            $conversation->update(['last_time_message' => now()]);
            event(new SendNotificationUser([
                'notification_type' => 'live-chat',
                'conversation_id' => $conversation->id,
                'message' => ChatDetailResource::make($chat),
            ], $token, $chat->message, $chat->message));
            return $this->success([
                'view' => view('dashboard.live-chats.chat', compact('chat'))->render()
            ]);
            //return $this->apiResponse(MessageResource::make($message));
        } catch (\Exception $exception) {
            return $this->success($exception->getMessage(), status: false);
        }

    }

}
