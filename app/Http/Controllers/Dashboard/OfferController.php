<?php

namespace App\Http\Controllers\Dashboard;

use App\Helpers\CPU\Models;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\OfferRequest;
use App\Models\Offer;
use App\Models\Order;
use App\Models\Owner;
use App\Models\ShopStaff;
use App\Traits\ApiResponses;
use App\Traits\UploadFileTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OfferController extends Controller
{
    use ApiResponses, UploadFileTrait;

    public function __construct()
    {
        $this->middleware(['permission:Offer list'])->only(['index', 'show']);
        $this->middleware(['permission:Offer add'])->only(['create', 'store']);
        $this->middleware(['permission:Offer edit'])->only(['edit', 'update', 'updateStatus', 'deleteImage']);
        $this->middleware(['permission:Offer delete'])->only(['destroy']);
    }

    public function index(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $query = Offer::query()->orderBy('created_at','DESC')->with(['category', 'owner.user', 'shop']);
            if (auth()->user()->userable_type == Owner::class || auth()->user()->userable_type ==ShopStaff::class  ) {
                $query = $query->whereIn('shop_id', userType());
            }
            $data = Offer::allOffers($query);
            $columns = [
                'all' => _trans('All'),
                'code' => _trans('Offer Code'),
                'name' => _trans('Offer name'),

            ];
            if (auth()->user()->userable_type == Owner::class || auth()->user()->userable_type ==ShopStaff::class) {
                $columns += [
                    'shop' => _trans('Shop name'),
                ];
            } else {
                $columns += [
                    'owner' => _trans('Owner name'),
                    'Shop' => _trans('Shop name'),
                ];
            }
            return view('dashboard.offer.index', compact('data', 'columns'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function create(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        $edit = false;
        $categories = Models::categories();
//        $owners = Models::owners()->get();
        $user = auth()->user();
        if (\request('guard')=='admin') {
            $data = Models::ownerAndShop(0)['owner'];
        } else {
            $data = Models::ownerAndShop($user->id)['shop'];
        }
        return view('dashboard.offer.form', compact('edit', 'categories', 'data'));
    }

    public function store(OfferRequest $request)
    {
        DB::beginTransaction();
        $data = $request->validated();
        if (auth()->user()->userable_type ==Owner::class  || auth()->user()->userable_type ==ShopStaff::class ) {
            $data['owner_id'] = getOwner();
        }
        $data['icon'] = $this->upload([
            'file' => 'icon',
            'path' => 'offer',
            'upload_type' => 'single',
            'delete_file' => ''
        ]);
        try {
            $offer = Offer::query()->create($data);
            if ($request->has('images')) {
                $this->upload([
                    'file' => 'images',
                    'path' => 'save',
                    'upload_type' => 'files',
                    'multi_upload' => true,
                    'relationable_id' => $offer->id,
                    'relationable_type' => Offer::class,
                ]);
            }
            $offer->products()->createMany($request->products);
            DB::commit();
            return redirect()->route('offer.index')->with('success', _trans('Done Save Data Successfully'));
        } catch (\Exception $exception) {
            DB::rollBack();
            return $exception->getMessage();
        }
    }

    public function show($id)
    {
        try {
            $offer = Offer::query()->findOrFail($id);
            $orders = Order::whereHas('orderDetails', function ($q) use ($id) {
                $q->where('modelable_id', $id)->where('modelable_type', Offer::class);
            })->get();
            return view('dashboard.offer.show', compact('offer', 'orders'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }

    }

    public function edit(Offer $offer): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        $edit = true;
        $categories = Models::categories();
//        $owners = Models::owners()->get();
        $user = auth()->user();
        if (\request('guard')=='admin') {
            $data = Models::ownerAndShop(0)['owner'];
        } else {
            $data = Models::ownerAndShop($user->id)['shop'];
        }
        return view('dashboard.offer.form', compact('edit', 'offer', 'categories', 'data'));
    }

    public function update(OfferRequest $request, Offer $offer): \Illuminate\Http\RedirectResponse
    {
        $data = $request->validated();
        if ($request->hasFile('icon')) {
            $data['icon'] = $this->upload([
                'file' => 'icon',
                'path' => 'offer',
                'upload_type' => 'single',
                'delete_file' => $offer->icon ?? ''
            ]);;
        }
        try {
            DB::beginTransaction();
            if ($request->has('images')) {
                $this->upload([
                    'file' => 'images',
                    'path' => 'save',
                    'upload_type' => 'files',
                    'multi_upload' => true,
                    'relationable_id' => $offer->id,
                    'relationable_type' => Offer::class,
                ]);
            }
            $offer->update($data);
            $offer->products()->delete();
            $offer->products()->createMany($request->products);
            DB::commit();
            return redirect()->route('offer.index')->with('success', _trans('Done Updated Data Successfully'));
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage())
                ->withInput($request->all());
        }
    }

    public function destroy(Offer $offer)
    {
        //
    }

    public function updateStatus(Request $request): \Illuminate\Http\JsonResponse
    {
        $offer = Offer::query()->findOrFail($request->id);
        if (!$offer) {
            return $this->success(_trans('Not Found'), status: false);
        }
        $status = !$offer->status;
        $offer->update(['status' => $status]);
        return $this->success(_trans('Done Updated Data Successfully'));
    }

    public function deleteImage(Request $request)
    {
        $file = Models::deleteFile([
            'id' => $request->id,
            'relationable_type' => Offer::class,
            'relationable_id' => $request->relation_id,
        ]);
        if ($file) {
            $this->deleteFile($file->full_file);
            $file->delete();
            return $this->success(_trans('Done Deleted Image Successfully'));
        }
        return $this->success(_trans('Not found this image'), status: false);
    }
}
