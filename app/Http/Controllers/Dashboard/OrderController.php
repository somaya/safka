<?php

namespace App\Http\Controllers\Dashboard;

use App\Enums\OrderStatus;
use App\Enums\OwnerType;
use App\Events\SendNotificationUser;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\OrderRequest;
use App\Models\Driver;
use App\Models\Notification;
use App\Models\Order;
use App\Models\Owner;
use App\Models\ShopStaff;
use App\Models\TrackingOrder;
use App\Traits\ApiResponses;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    use ApiResponses;

    public function __construct()
    {
        $this->middleware(['permission:Order list'])->only(['index', 'show','updateStatus']);
    }

    public function index(Request $request)
    {
        try {
            $orders = Order::query()->with(['shop', 'customer'])
                ->orderByDesc('created_at')
                ->whereIn('shop_id', userType())

                ->when($request->customer, function ($q) use ($request) {
                    $q->whereHas('customer.user', function ($query) use ($request) {
                        return $query->where('name', '=', $request->customer);
                    });
                })
                ->when($request->start, function ($q) use ($request) {
                    $q->where('created_at', '>=', request('start'));

                })
                ->when($request->end, function ($q) use ($request) {
                    $q->where('created_at', '<=', Carbon::parse(request('end'))->addDay()->format('Y-m-d'));

                })
                ->when($request->payment_method, function ($q) use ($request) {
                    $q->where('payment_method', '=', $request->payment_method);

                });
            if (auth()->user()->userable_type == Owner::class || auth()->user()->userable_type ==ShopStaff::class ) {
                $orders = $orders->whereIn('shop_id', userType());

            }
            $orders = $orders->withCount(['orderDetails']);
            $data = Order::allOrders($orders);

            $columns = [
                'all' => _trans('All'),
                'shop' => _trans('Shop name'),
                'customer' => _trans('Customer name'),
                'code' => _trans('Order Code'),
            ];
            if (auth()->user()->userable_type == Owner::class  || auth()->user()->userable_type ==ShopStaff::class) {
                $columns += [
                    'customer_id' => _trans('Customer name'),
                ];
            }
            return view('dashboard.order.index', compact('data', 'columns'
               ));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function show($id, $notificationId = null)
    {
        try {
            $order = Order::query()
                ->whereIn('shop_id', userType())
                ->with([
                    'orderDetails' => [
                        'product',
                        'offer',
                        'saveProduct',
                        'discount',
                        'size',
                    ], 'shop', 'trackingOrder'])
                ->findOrFail($id);
            $notification = Notification::query()->whereNull('read_at')->find($notificationId);
            if ($notification) {
                $notification->update(['read_at' => now()]);
            }
            if ($order) {
                return view('dashboard.order.show', compact('order'));
            }
            return redirect()->back()->with('warning', _trans('Not found this customer'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function updateStatus(OrderRequest $request, $orderId)
    {
        $order = Order::query()->with(['customer.fcmTokens', 'shop'])
            ->whereIn('shop_id', userType())
            ->findOrFail($orderId);
        if (!$order && in_array($order->order_status, [OrderStatus::completed->value, OrderStatus::canceled->value])) {
            return redirect()->back()->with('warning', _trans('Not found this order or order is completed or canceled'));
        }
        try {
            $preview = $order->order_status;
            DB::beginTransaction();
           if (request()->routeIs('order.cancelled')) {
                TrackingOrder::changeStatus($order, OrderStatus::canceled->value);
                $order->update(['note' => $request->reason_message]);
                $message = _trans('Done cancelled order successfully');
            } elseif (request()->routeIs('order.returned')) {
                TrackingOrder::changeStatus($order, OrderStatus::returned->value);
                $order->update(['note' => $request->returned_message]);
                $message = _trans('Done returned order successfully');
            } else {
                TrackingOrder::changeStatus($order);
                $message = _trans('Done updated order successfully');
            }

            $fcmToken = $order->customer->fcmTokens->pluck('fcm_token')->toArray();

            event(new SendNotificationUser([
                'notification_type' => 'change_order_status',
                'order_id' => $order->order_id,
                'order_status' => $order->order_status,
            ], $fcmToken, 'OrderID' . $order->code, _trans('Done change status from' . ' ' . $preview . ' ' . 'to' . $order->order_status)));
            DB::commit();
            return redirect()->route('order.show', $order->id)->with('success', $message);
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('warning', $exception->getMessage());
        }

    }

}
