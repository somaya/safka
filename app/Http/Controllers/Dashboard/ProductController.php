<?php

namespace App\Http\Controllers\Dashboard;

use App\Helpers\CPU\Models;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ProductRequest;
use App\Models\Color;
use App\Models\Discount;
use App\Models\Order;
use App\Models\Owner;
use App\Models\Product;
use App\Models\ShopStaff;
use App\Models\Size;
use App\Traits\ApiResponses;
use App\Traits\UploadFileTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    use UploadFileTrait, ApiResponses;

    public function __construct()
    {
        $this->middleware(['permission:Product list'])->only(['index']);
        $this->middleware(['permission:Product add'])->only(['create', 'store']);
        $this->middleware(['permission:Product edit'])->only(['edit', 'update', 'updateStatus']);
        $this->middleware(['permission:Product delete'])->only(['destroy']);
    }

    public function index(Request $request): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {

        try {
            $query = Product::query()->orderBy('created_at','DESC')->with(['category', 'shop'])
                ->when($request->shop, function ($query) use ($request) {
                    $query->whereHas('shop', function ($query) use ($request) {
                        return $query->whereTranslation('name', $request->shop, default_lang());
                    });
                });
            if (auth()->user()->userable_type == Owner::class || auth()->user()->userable_type ==ShopStaff::class  ) {
                $query = $query->whereIn('shop_id', userType());
            }

            $data = Product::allProducts($query);
            $columns = [
                'all' => _trans('All'),
                'code' => _trans('Product Code'),
                'name' => _trans('Product name'),
                'category' => _trans('Category name'),
                'shop' => _trans('Shop name'),
            ];

            return view('dashboard.product.index', compact('data', 'columns'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function create(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        $edit = false;
        $categories = Models::categories();
//        $owners = Models::owners()->get();
        $user = auth()->user();
        if (\request('guard')=='admin') {
            $data = Models::ownerAndShop(0)['owner'];
        } else {
            $data = Models::ownerAndShop($user->id)['shop'];
        }
        return view('dashboard.product.form', compact('edit', 'categories', 'data'));
    }

    public function store(ProductRequest $request)
    {

        $data = $request->validated();
        if (auth()->user()->userable_type ==Owner::class  || auth()->user()->userable_type ==ShopStaff::class ) {
            $data['owner_id'] = getOwner();
        }
        if ($request->hasFile('icon')) {
            $data['icon'] = $this->upload([
                'file' => 'icon',
                'path' => 'product',
                'upload_type' => 'single',
                'delete_file' => ''
            ]);
        }
        try {
            DB::beginTransaction();
            $product = Product::query()->create($data);
            if ($request->has('images')) {
                $this->upload([
                    'file' => 'images',
                    'path' => 'product',
                    'upload_type' => 'files',
                    'multi_upload' => true,
                    'relationable_id' => $product->id,
                    'relationable_type' => Product::class,
                ]);
            }
            if ($request->sizes && count($request->sizes) > 0) {
                foreach ($request->sizes as $size) {
                    if ($size['name:ar'] && $size['name:en'] && $size['price']) {
                        $product->sizes()->create(array_merge($size, [
                            'owner_id' => $product->owner_id,
                            'shop_id' => $product->shop_id,
                            'status' => 1
                        ]));
                    }
                }
            }

            if ($request->colors && count($request->colors) > 0) {
                foreach ($request->colors as $color) {
                    if ($color['name:ar'] && $color['name:en'] && $color['code']) {
                        $product->colors()->create(array_merge($color, [
                            'owner_id' => $product->owner_id,
                            'shop_id' => $product->shop_id,
                            'status' => 1
                        ]));
                    }
                }
            }
            DB::commit();
            return redirect()->route('product.index')->with('success', _trans('Done Save Data Successfully'));
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage())->withInput($request->all());
        }
    }

    public function show($id)
    {
        try {
            $product = Product::query()->findOrFail($id);
            $sizes = Models::sizes($id);
            $discounts = Discount::where('product_id', $id)->status()->count();
            $orders = Order::whereHas('orderDetails', function ($q) use ($id) {
                $q->where('modelable_id', $id)->where('modelable_type', Product::class);
            })->get();

            return view('dashboard.product.show', compact('sizes', 'product', 'discounts', 'orders'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }

    }

    public function edit($id): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        $edit = true;
        $product = Product::query()->withCount('images')->findOrFail($id);
        $categories = Models::categories();
//        $owners = Models::owners()->get();
        $user = auth()->user();
        if (\request('guard')=='admin') {
            $data = Models::ownerAndShop(0)['owner'];
        } else {
            $data = Models::ownerAndShop($user->id)['shop'];
        }
        return view('dashboard.product.form', compact('edit', 'product', 'categories', 'data'));
    }

    public function update(ProductRequest $request, Product $product): \Illuminate\Http\RedirectResponse
    {
        $data = $request->validated();
        if ($request->hasFile('icon')) {
            $data['icon'] = $this->upload([
                'file' => 'icon',
                'path' => 'product',
                'upload_type' => 'single',
                'delete_file' => $product->icon ?? ''
            ]);
        }
        try {
            DB::beginTransaction();
            if ($product->update($data)) {
                if ($request->has('images')) {
                    $this->upload([
                        'file' => 'images',
                        'path' => 'product',
                        'upload_type' => 'files',
                        'multi_upload' => true,
                        'relationable_id' => $product->id,
                        'relationable_type' => Product::class,
                    ]);
                }
                if ($request->sizes && count($request->sizes) > 0)
                {
                    foreach ($request->sizes as $size) {
                        if (array_key_exists("id", $size) && $size['id']) {
                            Size::findOrFail($size['id'])->update($size);
                        } else {
                            if ($size['name:ar'] && $size['name:en'] && $size['price']) {
                                $product->sizes()->create(array_merge($size, [
                                    'owner_id' => $product->owner_id,
                                    'shop_id' => $product->shop_id,
                                    'product_id' => $product->id,
                                    'status' => 1
                                ]));;
                            }
                        }
                    }
                }
                if ($request->colors && count($request->colors) > 0)
                {
                    foreach ($request->colors as $color) {
                        if (array_key_exists("id", $color) && $color['id']) {
                            Color::findOrFail($color['id'])->update($color);
                        } else {
                            if ($color['name:ar'] && $color['name:en'] && $color['code']) {
                                Color::create(array_merge($color, [
                                    'owner_id' => $product->owner_id,
                                    'shop_id' => $product->shop_id,
                                    'product_id' => $product->id,
                                    'status' => 1
                                ]));
                            }
                        }
                    }
                }
                DB::commit();
                return redirect()->route('product.index')->with('success', _trans('Done Updated Data Successfully'));
            }
            return redirect()->back()->with('warning', _trans('Some failed errors'))->withInput($data);
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage())->withInput($request->all());
        }
    }


    public function destroy(Product $product)
    {
        //
    }

    public function updateStatus(Request $request): \Illuminate\Http\JsonResponse
    {
        $product = Product::query()->findOrFail($request->id);
        if (!$product) {
            return $this->success(_trans('Not Found'), status: false);
        }
        $status = !$product->status;
        $product->update(['status' => $status]);
        return $this->success(_trans('Done Updated Data Successfully'));
    }

    public function updateWanted(Request $request): \Illuminate\Http\JsonResponse
    {
        $product = Product::query()->findOrFail($request->id);
        if (!$product) {
            return $this->success(_trans('Not Found'), status: false);
        }
        $wanted = !$product->wanted;
        $product->update(['wanted' => $wanted]);
        return $this->success(_trans('Done Updated Data Successfully'));
    }
    public function updateColorStatus(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            DB::beginTransaction();
            $color = Color::query()->findOrFail($request->id);
            if (!$color) {
                return $this->apiResponse('', _trans('Not Found'), false);
            }
            $status = !$color->status;
            if ($color->update(['status' => $status])) {
                DB::commit();
                return $this->apiResponse(_trans('Done Updated Data Successfully'), '');
            }
            return $this->apiResponse('', _trans('Some failed errors'), false);
        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->apiResponse('', $exception->getMessage(), false);
        }
    }


    public function deleteSize(Request $request): \Illuminate\Http\JsonResponse
    {
        $size = Size::query()->whereHas('product', function ($query) use ($request) {
            $query->where('id', $request->product_id)
                ->where('owner_id', $request->owner_id)
                ->where('shop_id', $request->shop_id);
        })->findOrFail($request->size_id);
        if (!$size) {
            return $this->success(_trans('Not Found'), status: false);
        }
        $size->delete();
        return $this->success(_trans('Done Deleted Size Successfully'));
    }

    public function deleteImage(Request $request)
    {
        $file = Models::deleteFile([
            'id' => $request->id,
            'relationable_type' => Product::class,
            'relationable_id' => $request->relation_id,
        ]);
        if ($file) {
            $this->deleteFile($file->full_file);
            $file->delete();
            return $this->success(_trans('Done Deleted Image Successfully'));
        }
        return $this->success(_trans('Not found this image'), status: false);
    }

}
