<?php

namespace App\Http\Controllers\Dashboard;

use App\Enums\Status;
use App\Helpers\CPU\Models;
use App\Helpers\PermissionAdmin;
use App\Helpers\PermissionOwner;
use App\Helpers\Setting\Utility;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\RoleStaffRequest;
use App\Interfaces\Role\RoleRepositoryInterface;
use App\Models\Owner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    private RoleRepositoryInterface $roleRepositoryInterface;

    public function __construct(RoleRepositoryInterface $roleRepositoryInterface)
    {
        $this->middleware(['user-permission:Role#Staff list'])->only(['index']);
        $this->middleware(['user-permission:Role#Staff add'])->only(['create']);
        $this->middleware(['user-permission:Role#Staff edit'])->only(['edit']);
        $this->middleware(['user-permission:Role#Staff delete'])->only(['destroy']);
        $this->roleRepositoryInterface = $roleRepositoryInterface;
    }

    public function index(Request $request): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $roles = $this->roleRepositoryInterface->all()
                ->withCount(['permissions', 'users'])
                ->when(request('guard') != 'admin', function ($query) {
                    $query->where('created_by', '=', getOwner());
                })
                ->when($request->name, function ($query) use ($request) {
                    $query->where('name', 'LIKE', "%{$request->name}%");
                })
                ->when($request->owner, function ($query) use ($request) {
                    $query->whereHas('user', function ($query) use ($request) {
                        $query->where('name', 'LIKE', "%{$request->owner}%");
                    });
                })
                ->whereNotNull('created_by')
                ->where('guard_name', '=', 'web')
                ->orderBy('created_at','DESC')
                ->paginate(Utility::getValByName('pagination_limit'));

            $owners = [];
            foreach ($roles as $row) {
                $owner = Owner::query()->with(['user'])->find($row->created_by);
                $owners[] = [
                    'id' => $row->created_by,
                    'name' => !empty($owner) ? $owner->user->name : _trans('Not Owner')
                ];
            }
            $columns = [
                'all' => _trans('All'),
                'name' => _trans('Role name'),
            ];
            if (auth('admin')->check()) {
                $columns = array_merge($columns, [
                    'owner' => _trans('Owner name'),
                ]);
            }
            return view('dashboard.role.index', compact('roles', 'columns', 'owners'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function create(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        $edit = false;
        $owners = [];
        if (request('guard') == 'admin') {
            $owners = Models::owners(Status::Active)->get();
        }
        $modules = PermissionOwner::models();
        return view('dashboard.role.form', compact('edit', 'modules', 'owners'));
    }

    public function store(RoleStaffRequest $request): \Illuminate\Http\RedirectResponse
    {
        $data = $request->validated();
        $data['guard_name'] = 'web';
        if (\request('guard')=='admin') {
            $data['created_by'] = $request->owner_id;
        }else{
            $data['created_by'] =getOwner();
        }
        try {
            DB::beginTransaction();
            $this->roleRepositoryInterface->store($request, $data, PermissionOwner::models(), PermissionOwner::lists(), $data['guard_name']);
            DB::commit();
            return redirect()->route('role.index')->with('success', _trans('Done Save Data Successfully'));
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage())->withInput($request->all());
        }
    }

    public function show($id): \Illuminate\Http\RedirectResponse
    {
        return redirect()->back()->with('error', _trans('Not Allow Access Here'));
    }

    public function edit($id): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        $edit = true;
        $owners = [];
        if (request('guard') == 'admin') {
            $owners = Models::owners(status: Status::Active)->get();
        }
        $modules = PermissionOwner::models();
        $role = $this->roleRepositoryInterface->edit($id, 'owner');
        return view('dashboard.role.form', compact('edit', 'modules', 'owners', 'role'));
    }

    public function update(RoleStaffRequest $request, $id): \Illuminate\Http\RedirectResponse
    {
        DB::beginTransaction();
        $role=Role::query();
        if (\request('guard')=='admin'){
            $role=$role->where('created_by',$request->owner_id);
        }else{
            $role=$role->where('created_by',getOwner());
        }
        $role=$role->findOrFail($id);
        $data = $request->validated();
        try {
            $this->roleRepositoryInterface->update($request, $data, $id, 'owner', PermissionOwner::models(), PermissionOwner::lists(), type: 'update');
            DB::commit();
            return redirect()->route('role.index')->with('success', _trans('Done Updated Data Successfully'));
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage())->withInput($request->all());
        }
    }

    public function destroy($id): \Illuminate\Http\RedirectResponse
    {
        try {
            DB::beginTransaction();
            $this->roleRepositoryInterface->destroy($id, 'owner');
            DB::commit();
            return redirect()->route('role.index')->with('success', _trans('Done Deleted Data Successfully'));
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }
}
