<?php

namespace App\Http\Controllers\Dashboard;

use App\Helpers\CPU\Models;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\SaveRequest;
use App\Models\Order;
use App\Models\Owner;
use App\Models\Save;
use App\Models\ShopStaff;
use App\Traits\ApiResponses;
use App\Traits\UploadFileTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SaveController extends Controller
{
    use ApiResponses, UploadFileTrait;

    public function __construct()
    {
        $this->middleware(['permission:Save list'])->only(['index']);
        $this->middleware(['permission:Save add'])->only(['create', 'store']);
        $this->middleware(['permission:Save edit'])->only(['edit', 'update', 'updateStatus']);
        $this->middleware(['permission:Save delete'])->only(['destroy']);
    }

    public function index(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $query = Save::query()->orderBy('created_at','DESC')->with(['category', 'owner.user', 'shop']);
            if (auth()->user()->userable_type == Owner::class || auth()->user()->userable_type ==ShopStaff::class  ) {
                $query = $query->whereIn('shop_id', userType());
            }
            $data = Save::allSaves($query);
            $columns = [
                'all' => _trans('All'),
                'code' => _trans('Save Code'),
                'name' => _trans('Save name'),

            ];
            if (auth()->user()->userable_type == Owner::class || auth()->user()->userable_type ==ShopStaff::class) {
                $columns += [
                    'shop' => _trans('Shop name'),
                ];
            } else {
                $columns += [
                    'owner' => _trans('Owner name'),
                    'Shop' => _trans('Shop name'),
                ];
            }
            return view('dashboard.save.index', compact('data', 'columns'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function create(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        $edit = false;
        $categories = Models::categories();
//        $owners = Models::owners()->get();
        $user = auth()->user();
        if (\request('guard')=='admin') {
            $data = Models::ownerAndShop(0)['owner'];
        } else {
            $data = Models::ownerAndShop($user->id)['shop'];
        }
        return view('dashboard.save.form', compact('edit', 'categories', 'data'));
    }

    public function store(SaveRequest $request)
    {
        $data = $request->validated();
        if (auth()->user()->userable_type ==Owner::class  || auth()->user()->userable_type ==ShopStaff::class ) {
            $data['owner_id'] = getOwner();
        }
        if ($request->hasFile('icon')) {
            $data['icon'] = $this->upload([
                'file' => 'icon',
                'path' => 'save',
                'upload_type' => 'single',
                'delete_file' => ''
            ]);
        }
        try {
            DB::beginTransaction();
            $save = Save::query()->create($data);
            if ($request->has('images')) {
                $this->upload([
                    'file' => 'images',
                    'path' => 'save',
                    'upload_type' => 'files',
                    'multi_upload' => true,
                    'relationable_id' => $save->id,
                    'relationable_type' => Save::class,
                ]);
            }
            $save->products()->createMany($request->products);
            DB::commit();
            return redirect()->route('save.index')->with('success', _trans('Done Save Data Successfully'));
        } catch (\Exception $exception) {
            DB::rollBack();
            return $exception->getMessage();
        }
    }

    public function show($id)
    {
        try {
            $save = Save::query()->findOrFail($id);
            $orders = Order::whereHas('orderDetails', function ($q) use ($id) {
                $q->where('modelable_id', $id)->where('modelable_type', Save::class);
            })->get();
            return view('dashboard.save.show', compact('save', 'orders'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }

    }

    public function edit(Save $save): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        $edit = true;
        $categories = Models::categories();
//        $owners = Models::owners()->get();
        $user = auth()->user();
        if (\request('guard')=='admin') {
            $data = Models::ownerAndShop(0)['owner'];
        } else {
            $data = Models::ownerAndShop($user->id)['shop'];
        }
        return view('dashboard.save.form', compact('edit', 'save', 'categories', 'data'));
    }

    public function update(SaveRequest $request, Save $save): \Illuminate\Http\RedirectResponse
    {
        $data = $request->validated();
        if ($request->hasFile('icon')) {
            $data['icon'] = $this->upload([
                'file' => 'icon',
                'path' => 'save',
                'upload_type' => 'single',
                'delete_file' => $save->icon ?? ''
            ]);;
        }
        try {
            DB::beginTransaction();
            if ($request->has('images')) {
                $this->upload([
                    'file' => 'images',
                    'path' => 'save',
                    'upload_type' => 'files',
                    'multi_upload' => true,
                    'relationable_id' => $save->id,
                    'relationable_type' => Save::class,
                ]);
            }
            $save->update($data);
            $save->products()->delete();
            $save->products()->createMany($request->products);
            DB::commit();
            return redirect()->route('save.index')->with('success', _trans('Done Updated Data Successfully'));
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage())->withInput($request->all());
        }
    }


    public function destroy(Save $save)
    {
        //
    }

    public function updateStatus(Request $request): \Illuminate\Http\JsonResponse
    {
        $save = Save::query()->findOrFail($request->id);
        if (!$save) {
            return $this->success(_trans('Not Found'), status: false);
        }
        $status = !$save->status;
        $save->update(['status' => $status]);
        return $this->success(_trans('Done Updated Data Successfully'));
    }

    public function deleteImage(Request $request)
    {
        $file = Models::deleteFile([
            'id' => $request->id,
            'relationable_type' => Save::class,
            'relationable_id' => $request->relation_id,
        ]);
        if ($file) {
            $this->deleteFile($file->full_file);
            $file->delete();
            return $this->success(_trans('Done Deleted Image Successfully'));
        }
        return $this->success(_trans('Not found this image'), status: false);
    }
}
