<?php

namespace App\Http\Controllers\Dashboard;

use App\Enums\Status;
use App\Enums\UserType;
use App\Helpers\CPU\Models;
use App\Helpers\Setting\Utility;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\StaffRequest;
use App\Interfaces\Role\RoleRepositoryInterface;
use App\Models\Owner;
use App\Models\ShopStaff;
use App\Traits\ApiResponses;
use App\Traits\UploadFileTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use PhpParser\Node\Expr\AssignOp\Mod;
use Spatie\Permission\Models\Role;

class StaffController extends Controller
{
    use UploadFileTrait, ApiResponses;

    private RoleRepositoryInterface $roleRepositoryInterface;

    public function __construct(RoleRepositoryInterface $roleRepositoryInterface)
    {
        $this->middleware(['user-permission:Staff list'])->only(['index']);
        $this->middleware(['user-permission:Staff add'])->only(['create']);
        $this->middleware(['user-permission:Staff edit'])->only(['edit']);
        $this->middleware(['user-permission:Staff delete'])->only(['destroy']);
        $this->roleRepositoryInterface = $roleRepositoryInterface;
    }

    public function index(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        // this is staff owner
        try {
            $data = ShopStaff::query()->with(['user.role','owner.user','shop'])
                ->when(request('guard') != 'admin', function ($query) {
                    $query->where('owner_id', '=', getOwner());
//                        ->where('id', '!=', auth()->user()->userable->id);
                })->orderBy('created_at','DESC');

            $columns = [
                'all' => _trans('All'),
                'code' => _trans('Code'),
                'name' => _trans('Staff name'),
                'job_number' => _trans('Job number'),
                'national_number' => _trans('National number'),
            ];
            if (\request('guard')=='admin') {
                $columns += [
                    'owner_name' => _trans('Owner name'),
                    'shop_name' => _trans('Shop name'),
                ];
            }
            $data = ShopStaff::allStaff($data);
            return view('dashboard.staff.index', compact('data', 'columns'));
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }


    public function create(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        $edit = false;
        $data=$roles= [];
        if (request('guard') == 'admin') {
            $data = Models::owners(Status::Active)->get();

        }
        else {
            $data = Models::ownerAndShop(auth()->user()->id)['shop'];
            $roles = Role::query()
                ->where('created_by', getOwner())
                ->where('guard_name', '=', 'web')
                ->orderByDesc('created_at')->get();

        }
        return view('dashboard.staff.form', compact('edit', 'data', 'roles'));
    }

    public function store(StaffRequest $request): \Illuminate\Http\RedirectResponse
    {
        $data = $request->validated();
        $dataUser = $request->only(['name', 'email', 'password', 'country_id', 'governorate_id', 'region_id']);
        if (request('guard') == 'admin') {
            $ownerId = $request->owner_id;
            $data['owner_id'] = $ownerId;
            $data['shop_id'] =$request->shop_id;
        } else {
            $ownerId = getOwner();
            $data['owner_id'] = $ownerId;
            $data['shop_id'] =$request->shop_id;
        }
        $data['status'] = true;
        $role = $this->roleRepositoryInterface->all()
            ->where('created_by', $ownerId)
            ->where('guard_name', '=', 'web')
            ->find($request->role_id);
        if (!$role) {
            return redirect()->back()->with('error', _trans('Please Select Role First'))->withInput($request->all());
        }
        $dataUser['role_id'] = $role->id;
        if ($request->hasFile('avatar')) {
            $dataUser['avatar'] = $this->upload([
                'file' => 'avatar',
                'path' => 'staff',
                'upload_type' => 'single',
                'delete_file' => ''
            ]);
        }
        try {
            DB::beginTransaction();
            $staff = ShopStaff::query()->create($data);
            $user = $staff->user()->create($dataUser);
            $user->assignRole($role);
            DB::commit();
            return redirect()->route('staff.index')->with('success', _trans('Done Save Data Successfully'));
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage())->withInput($request->all());
        }

    }

    public function show($id)
    {
        //
    }

    public function edit($id): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        $edit = true;
        $roles=[];
        $staff = ShopStaff::query()->with(['user'])->when(request('guard') != 'admin', function ($query) {
            $query->where('owner_id', '=', getOwner());
//                ->where('id', '!=', auth()->user()->userable()->id);
        })->findOrFail($id);
        if (request('guard') == 'admin') {
            $data = Models::owners(Status::Active)->get();
//            $roles=Role::query()->whereNotIn('name', ['super_admin', 'owner'])
//                ->where('guard_name', '=', 'web')
//                ->orderByDesc('created_at')->get();
        } else {
            $data = Models::ownerAndShop(auth()->user()->id)['shop'];
            $roles = $this->roleRepositoryInterface->all()
                ->where('created_by', getOwner())
                ->where('guard_name', '=', 'web')
                ->orderByDesc('created_at')->get();
        }
        return view('dashboard.staff.form', compact('edit', 'data', 'roles', 'staff'));
    }

    public function update(StaffRequest $request, $id): \Illuminate\Http\RedirectResponse
    {
        if (request('guard') == 'admin') {
            $ownerId = $request->owner_id;
        } else {
            $ownerId = getOwner();
        }
        $staff = ShopStaff::query()->with(['user'])
            ->whereHas('user', function ($query) use ($id, $request) {
                $query->where('userable_type', ShopStaff::class)
                    ->where('userable_id', $id);
            })->where('owner_id', '=', $ownerId)->first();
        if (!$staff) {
            return redirect()->back()->with('waring', _trans('Not found this staff'))->withInput($request->all());
        }
        $data = $request->validated();
        $dataUser = $request->only(['name', 'email', 'country_id', 'governorate_id', 'region_id']);
        if (request('guard') == 'admin') {
            $ownerId = $request->owner_id;
            $data['shop_id'] =$request->shop_id;
        } else {
            $ownerId = getOwner();
            $data['shop_id'] =$request->shop_id;
        }
        $role = $this->roleRepositoryInterface->all()
            ->where('created_by', $ownerId)
            ->where('guard_name', '=', 'web')
            ->find($request->role_id);
        if (!$role) {
            return redirect()->back()->with('error', _trans('Please Select Role First'))->withInput($request->all());
        }

        try {
            DB::beginTransaction();
            if ($role->id != $request->role_id) {
                $oldRole = $this->roleRepositoryInterface->index('owner')->where('id', '=', $staff->user->role_id)->first();
                if (!empty($oldRole)) {
                    $staff->user()->removeRole($oldRole);
                }
            }
            $dataUser['role_id'] = $role->id;
            if ($request->hasFile('avatar')) {
                $dataUser['avatar'] = $this->upload([
                    'file' => 'avatar',
                    'path' => 'employee',
                    'upload_type' => 'single',
                    'delete_file' => $staff->user()->avatar ?? ''
                ]);
            }
            $staff->update($data);
            $user = $staff->user()->update($dataUser);
            if ($role->id != $request->role_id) {
                $user->assignRole($role);
            }
            DB::commit();
            return redirect()->route('staff.index')->with('success', _trans('Done Updated Data Successfully'));
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage())->withInput($request->all());
        }
    }

    public function destroy($id)
    {
        /*try {
            DB::beginTransaction();
            $employee = Admin::query()->whereNotIn('id', [1])->findOrFail($id);
            $oldRole = Role::query()->where('guard_name', '=', 'admin')
                ->where('id', '=', $employee->role_id)->first();
            if (!empty($oldRole)) {
                $employee->removeRole($oldRole);
            }
            $image = $employee->avatar;
            if ($employee->forceDelete()) {
                Storage::delete($image);
                DB::commit();
                return redirect()->route('staff.index')->with('success', _trans('Done Deleted Data Successfully'));
            }
            return redirect()->back()->with('warning', _trans('Some failed errors'));
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage());
        }*/
    }

    public function updateStatus(Request $request): \Illuminate\Http\JsonResponse
    {
        $staff = ShopStaff::query()->when(request('guard') != 'admin', function ($query) {
            $query->where('owner_id', '=', getOwner());
//                ->where('id', '!=', auth()->user()->userable()->id);
        })->findOrFail($request->id);

        if (!$staff) {
            return $this->success(_trans('Not Found'));
        }
        $status = !$staff->user->status;
        $staff->user->update(['status' => $status]);
        return $this->success(_trans('Done Updated Data Successfully'));
    }

    public function changePassword(StaffRequest $request): \Illuminate\Http\RedirectResponse
    {
        $staff = ShopStaff::query()->with(['user'])->when(request('guard') != 'admin', function ($query) {
            $query->where('owner_id', '=', getOwner());
//                ->where('id', '!=', auth()->user()->userable()->id);
        })->findOrFail($request->id);


        $staff->user()->update(['password' =>Hash::make( $request->password)]);
        return redirect()->route('staff.index')->with('success', _trans('Done Updated Data Successfully'));
    }
}
