<?php

namespace App\Http\Middleware;

use App\Enums\Status;
use Closure;
use Illuminate\Support\Facades\Auth;

class AuthUser
{
    public function handle($request, Closure $next)
    {
        if (Auth::guard('admin')->check()) {
            $user = Auth::guard('admin')->user();
            $guard = 'admin';
        } else {
            $user = Auth::user();
            $guard = 'web';
        }
        if ($guard == 'admin' && $user->status == Status::Not_Active->value) {
            session('warning', _trans('Your account is not active'));
            Auth::guard($guard)->logout();
        }
        if ($guard == 'web' && $user->status == Status::Not_Active->value) {
            session('warning', _trans('Your account is not active'));
            Auth::logout();
        }
        request()->merge(['guard' => $guard]);
        return $next($request);

    }
}
