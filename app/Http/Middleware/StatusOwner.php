<?php

namespace App\Http\Middleware;

use App\Enums\Status;
use App\Models\Owner;
use App\Models\ShopStaff;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StatusOwner
{
    public function handle(Request $request, Closure $next)
    {
        if (Auth::guard('web')->check() && (in_array(Auth::guard('web')->user()->userable_type,[Owner::class,ShopStaff::class])) ) {
            return $next($request);
        }
        Auth::guard('web')->logout();
        return redirect()->route('owner.login');
    }
}
