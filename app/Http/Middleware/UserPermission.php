<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class UserPermission
{
    public function handle($request, Closure $next, $permission)
    {
        if (Auth::guard('admin')->check()) {
            $authGuard = Auth::guard('admin')->user();
        } else {
            $authGuard = Auth::user();
        }
        $permissions = is_array($permission) ? $permission : explode('|', $permission);

        foreach ($permissions as $permission) {
            if ($authGuard->can($permission)) {
                return $next($request);
            }
        }
    }
}
