<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class BlogRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }
    protected function onData(): array
    {
        return [
            'category_id' => ['required', 'integer', Rule::exists('blog_categories', 'id')
                ->where('status', 1)],
            /*tags*/
            'tags' => ['nullable', 'array','min:1'],


        ];
    }

    protected function onCreate(): array
    {
        $rules = [

            'default_image' => ['required', validationImage()],
            'images.*' => ['image', 'mimes:jpg,jpeg,png,bmp' ],

        ];

        foreach (locales() as $locale) {
            $rules += [
                'tags.*.name:'.$locale  => ['nullable', 'string'],
                $locale . '.title' => ['required', 'string', 'min:2', 'max:100'],
                $locale . '.content' => ['sometimes', 'nullable', 'string', 'min:2'],
            ];
        }
        return array_merge($this->onData(), $rules);
    }

    protected function onUpdate(): array
    {
        $rules = [
            'id' => ['required', 'integer', Rule::exists('blogs', 'id')],
            'default_image' => ['nullable', validationImage()],
            'images.*' => ['nullable','image', 'mimes:jpg,jpeg,png,bmp' ],
        ];

        foreach (locales() as $locale) {
            $rules += [
                'tags.*.name:'.$locale  => ['nullable', 'string'],
                $locale . '.title' => ['required', 'string', 'min:2', 'max:100'],
                $locale . '.content' => ['sometimes', 'nullable', 'string', 'min:2'],

            ];
        }
        return array_merge($this->onData(), $rules);
    }

    public function rules(): array
    {
        if (request()->routeIs('admin.blogs.store')) {
            return $this->onCreate();
        } elseif (request()->routeIs('admin.blogs.update')) {
            return $this->onUpdate();
        } else {
            return [];
        }
    }
}
