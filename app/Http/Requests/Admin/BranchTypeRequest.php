<?php

namespace App\Http\Requests\Admin;

use App\Enums\Status;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class BranchTypeRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    protected function onCreate(): array
    {
        $rules = [
            'image' => ['required', validationImage()],
            'published' => ['required', 'in:0,1'],
            'ranking' => ['required', 'integer', 'min:1'],
        ];
        foreach (locales() as $locale) {
            $rules += [
                $locale . '.name' => ['required', 'string', 'min:2', 'max:100', Rule::unique('branch_type_translations', 'name')],
            ];
        }
        return $rules;
    }

    protected function onUpdate(): array
    {
        $rules = [
            'id' => ['required', 'integer', Rule::exists('branch_types', 'id')],
            'published' => ['required', 'in:0,1'],
            'ranking' => ['required', 'integer', 'min:1'],
            'image' => ['nullable', validationImage()],
        ];
        foreach (locales() as $locale) {
            $rules += [
                $locale . '.name' => ['required', 'string', 'min:2', 'max:100',
                    Rule::unique('branch_type_translations', 'name')->ignore($this->id, 'branch_type_id')],
            ];
        }
        return $rules;
    }

    public function rules(): array
    {
        if (request()->routeIs('admin.branch-type.store')) {
            return $this->onCreate();
        } elseif (request()->routeIs('admin.branch-type.update')) {
            return $this->onUpdate();
        } else {
            return [];
        }
    }

    public function attributes()
    {
        $messages = [
            'image' => _trans('Image'),
            'published' => _trans('Published'),
        ];
        foreach (locales() as $locale) {
            $messages += [
                $locale . '.name' => _trans('Branch type name') . ' (' . ucfirst($locale) . ')',
            ];
        }
        return $messages;
    }
}
