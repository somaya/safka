<?php

namespace App\Http\Requests\Admin;

use App\Enums\Status;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CategoryRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    protected function onCreate(): array
    {
        $rules = [
            'ranking' => ['required', 'integer', 'min:1'],
            'parent_id' => ['sometimes', 'nullable', 'integer',
                Rule::exists('categories', 'id')
                    ->whereNull('parent_id')
                    ->where('status', Status::Active->value)
            ],
            'image' => ['required', validationImage()],
        ];
        foreach (locales() as $locale) {
            $rules += [
                $locale . '.name' => ['required', 'string', 'min:2', 'max:100', Rule::unique('category_translations', 'name')],
            ];
        }
        return $rules;
    }

    protected function onUpdate(): array
    {
        $rules = [
            'id' => ['required', 'integer', Rule::exists('categories', 'id')],
            'ranking' => ['required', 'integer', 'min:1'],
            'image' => ['nullable', validationImage()]
        ];
        foreach (locales() as $locale) {
            $rules += [
                $locale . '.name' => ['required', 'string', 'min:2', 'max:100',
                    Rule::unique('category_translations', 'name')->ignore($this->id, 'category_id')],
            ];
        }
        return $rules;
    }

    public function rules(): array
    {
        if (request()->routeIs('admin.category.store')) {
            return $this->onCreate();
        } elseif (request()->routeIs('admin.category.update')) {
            return $this->onUpdate();
        } else {
            return [];
        }
    }

    public function attributes()
    {
        $messages = [
            'ranking' => _trans('Category ranking'),
            'parent_id' => _trans('Main Category name'),
        ];
        foreach (locales() as $locale) {
            $messages += [
                $locale . '.name' => _trans('Category name') . ' (' . ucfirst($locale) . ')',
            ];
        }
        return $messages;
    }
}
