<?php

namespace App\Http\Requests\Admin;

use App\Enums\Status;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CompanyRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    protected function onCreate(): array
    {
        $rules = [
            'ranking' => ['required', 'integer', 'min:1'],
            'link' => ['nullable', 'url', 'string', 'min:2', 'max:255'],
            'logo' => ['required', validationImage()],
        ];
        return $rules;
    }

    protected function onUpdate(): array
    {
        $rules = [
            'id' => ['required', 'integer', Rule::exists('companies', 'id')],
            'ranking' => ['required', 'integer', 'min:1'],
            'link' => ['nullable', 'url', 'string', 'min:2', 'max:255'],
            'logo' => ['nullable', validationImage()]
        ];
        return $rules;
    }

    public function rules(): array
    {
        if (request()->routeIs('admin.company.store')) {
            return $this->onCreate();
        } elseif (request()->routeIs('admin.company.update')) {
            return $this->onUpdate();
        } else {
            return [];
        }
    }

    public function attributes()
    {
        $messages = [
            'ranking' => _trans('Company ranking'),
        ];
        return $messages;
    }
}
