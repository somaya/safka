<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ContactUsRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    protected function onCreate(): array
    {
        return [
            'name' => ['required', 'string', 'min:2', 'max:255'],
            'subject' => ['required', 'string', 'min:2', 'max:255'],
            'email' => ['required', 'email', 'string', 'min:2', 'max:255',],
            'phone' => ['required', 'string', 'min:2', 'max:255'],
            'message' => ['required', 'string', 'min:2',],
        ];
    }

    protected function onUpdate(): array
    {
        return [
            'id' => ['required', 'integer', Rule::exists('contact-us', 'id')],
            'message' => ['required', 'string', 'min:2',],

        ];
    }


    public function rules(): array
    {
        if (request()->routeIs('admin.contact-us.store')) {
            return $this->onCreate();
        } elseif (request()->routeIs('admin.contact-us.update')) {
            return $this->onUpdate();
        } else {
            return [];
        }
    }

    public function attributes()
    {
        return [
            'full_name' => _trans('full_name'),
            'name' => _trans('full_name'),
            'message' => _trans('Send message'),
        ];
    }
}
