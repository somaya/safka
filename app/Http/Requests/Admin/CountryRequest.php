<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CountryRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    protected function onCreate(): array
    {
        $rules = [
            'icon' => ['nullable', validationImage()],
            'code' => ['required', 'string', 'min:1', 'max:10', Rule::unique('countries', 'code')],
            'currency_id' => ['nullable', 'integer', Rule::exists('currencies', 'id')->where('status', 1)],

        ];
        foreach (locales() as $locale) {
            $rules += [
                $locale . '.name' => ['required', 'string', 'min:2', 'max:100', Rule::unique('country_translations', 'name')],
                $locale . '.nationality' => ['sometimes', 'nullable', 'string', 'min:2', 'max:100'],
            ];
        }
        return $rules;
    }

    protected function onUpdate(): array
    {
        $rules = [
            'id' => ['required', 'integer', Rule::exists('countries', 'id')],
            'icon' => ['nullable', validationImage()],
            'code' => ['required', 'string', 'min:1', 'max:10',
                Rule::unique('countries', 'code')->ignore($this->id, 'id')],
            'currency_id' => ['nullable', 'integer', Rule::exists('currencies', 'id')->where('status', 1)],

        ];
        foreach (locales() as $locale) {
            $rules += [
                $locale . '.name' => ['required', 'string', 'min:2', 'max:100',
                    Rule::unique('country_translations', 'name')->ignore($this->id, 'country_id')],
                $locale . '.nationality' => ['sometimes', 'nullable', 'string', 'min:2', 'max:100'],
            ];
        }
        return $rules;
    }

    public function rules(): array
    {
        if (request()->routeIs('admin.country.store')) {
            return $this->onCreate();
        } elseif (request()->routeIs('admin.country.update')) {
            return $this->onUpdate();
        } else {
            return [];
        }
    }

    public function attributes()
    {
        $messages = [
            'currency_id' => _trans('Currency name'),
        ];
        foreach (locales() as $locale) {
            $messages += [
                $locale . '.name' => _trans('Country name') . ' (' . ucfirst($locale) . ')',
                $locale . '.nationality' => _trans('Nationality') . ' (' . ucfirst($locale) . ')',
            ];
        }
        return $messages;
    }
}
