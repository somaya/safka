<?php

namespace App\Http\Requests\Admin;

use App\Enums\CouponType;
use App\Enums\Status;
use App\Rules\IsOwner;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class CouponRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    protected function onData(): array
    {
        $rules = [
            'coupon_type' => ['required', 'string', new Enum(CouponType::class)],
            'discount_type' => ['required', 'string', 'in:amount,percentage'],
            'discount' => ['required', 'min:1', 'numeric',
                Rule::when($this->discount_type == 'percentage', [
                    'between:0,99.99'
                ])],
            'min_purchase' => ['required', 'min:1', 'numeric'],
            'limit' => ['required', 'min:1', 'integer'],
            'code' => ['required', 'string', 'min:2', 'max:10'],
            'start_date' => ['required', 'date', 'date_format:Y-m-d', 'after_or_equal:' . now()->format('Y-m-d')],
            'end_date' => ['required', 'date', 'date_format:Y-m-d', 'after:start_date'],
        ];
        if ($this->coupon_type == CouponType::Customized->value) {
            $rules = array_merge($rules, [
                'owner_id' => ['required', 'integer', new IsOwner()],
                'shop_id' => ['required', 'integer', Rule::exists('shops', 'id')
                    ->where('owner_id', $this->owner_id)
                    ->where('status', Status::Active->value)
                ],
            ]);
        }
        return $rules;
    }

    protected function onCreate(): array
    {
        $rules = [];
        foreach (locales() as $locale) {
            $rules += [
                $locale . '.name' => ['required', 'string', 'min:2', 'max:100', Rule::unique('coupon_translations', 'name')],
                $locale . '.description' => ['nullable', 'string', 'min:2',],
            ];
        }
        return array_merge($this->onData(), $rules);
    }

    protected function onUpdate(): array
    {
        $rules = [];
        foreach (locales() as $locale) {
            $rules += [
                $locale . '.name' => ['required', 'string', 'min:2', 'max:100',
                    Rule::unique('coupon_translations', 'name')->ignore(request()->segment(3), 'coupon_id')],
                $locale . '.description' => ['required', 'string', 'min:2',],
            ];
        }
        return array_merge($this->onData(), $rules);
    }

    public function rules(): array
    {
        if (request()->routeIs('admin.coupon.store')) {
            return $this->onCreate();
        } elseif (request()->routeIs('admin.coupon.update')) {
            return $this->onUpdate();
        } else {
            return [];
        }
    }

    public function attributes()
    {
        $messages = [
            'owner_id' => _trans('Owner name'),
            'shop_id' => _trans('Shop name'),
            'discount_type' => _trans('Discount type'),
            'discount' => _trans('Discount'),
            'min_purchase' => _trans('Min purchase'),
            'limit' => _trans('Limit'),
            'code' => _trans('Code'),
            'date_from' => _trans('Date from'),
            'date_to' => _trans('Date to'),
        ];
        foreach (locales() as $locale) {
            $messages += [
                $locale . '.name' => _trans('Coupon name') . ' (' . ucfirst($locale) . ')',
                $locale . '.description' => _trans('Coupon description') . ' (' . ucfirst($locale) . ')',
            ];
        }
        return $messages;
    }
}
