<?php

namespace App\Http\Requests\Admin;

use App\Enums\Status;
use App\Rules\IsOwner;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class DiscountRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    protected function onCreate(): array
    {

        if (Auth::guard('admin')->check()) {
            $rules = [
                'owner_id' => ['required', 'integer', new IsOwner()],
                'shop_id' => ['required', 'integer', Rule::exists('shops', 'id')
                    ->where('status', 1)->where('owner_id', $this->owner_id)],

            ];
        } else {
            $rules = [
                'shop_id' => ['required', 'integer', Rule::exists('shops', 'id')
                    ->where('status', 1)->where('owner_id', getOwner())],

            ];
        }
        $rules+= [
//            'owner_id' => ['required', 'integer', Rule::exists('owners', 'id')->whereNull('created_by')],
//            'shop_id' => ['required', 'integer', Rule::exists('shops', 'id')
//                ->where('owner_id', $this->owner_id)
//                ->where('status', Status::Active->value)
//            ],
            'product_id' => ['required', 'integer', Rule::exists('products', 'id')
                ->where('shop_id', $this->shop_id)
                ->where('status', Status::Active->value)
            ],
            'percent' => ['required', 'numeric', 'between:0,99.99'],
            'start' => ['required', 'date_format:Y-m-d', 'after_or_equal:' . now()->format('Y-m-d')],
            'end' => ['required', 'date_format:Y-m-d', 'after:start'],
        ];

        return $rules;
    }


    protected function onUpdate(): array
    {
        return array_merge([
            'id' => ['required', 'integer', Rule::exists('discounts', 'id')
//                ->where('owner_id', $this->owner_id)
//                ->where('shop_id', $this->shop_id)
            ],
        ], $this->onCreate());
    }

    public function rules(): array
    {
        if (request()->routeIs('discount.store')) {
            return $this->onCreate();
        } elseif (request()->routeIs('discount.update')) {
            return $this->onUpdate();
        } else {
            return [];
        }
    }

    public function attributes()
    {
        return [
            'owner_id' => _trans('Owner name'),
            'shop_id' => _trans('Shop name'),
            'product_id' => _trans('Product name'),
        ];
    }
}
