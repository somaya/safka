<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class EmployeeRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    protected function onCreate(): array
    {
        return [
            'name' => ['required', 'string', 'min:2', 'max:255'],
            'email' => ['required', 'email', 'string', 'min:2', 'max:255', Rule::unique('admins', 'email')],
            'phone' => ['required', 'string', Rule::unique('admins', 'phone')],
            'password' => ['required', 'string', 'min:2', 'max:255', 'confirmed'],
            'password_confirmation' => ['required', 'string', 'min:2', 'max:255', 'same:password'],
            'role_id' => ['required', 'integer', Rule::exists('roles', 'id')
                ->where('guard_name', 'admin')
                ->whereNot('name', 'super_admin')],
            'avatar' => ['sometimes', 'nullable', validationImage()],
        ];
    }

    protected function onUpdate(): array
    {
        return [
            'id' => ['required', 'integer', Rule::exists('admins', 'id')
                ->whereNot('id', 1)],
            'name' => ['required', 'string', 'min:2', 'max:255'],
            'email' => ['required', 'email', 'string', 'min:2', 'max:255',
                Rule::unique('admins', 'email')->ignore($this->id, 'id')],
            'phone' => ['required', 'string',
                Rule::unique('admins', 'phone')->ignore($this->id, 'id')],
            'role_id' => ['required', 'integer', Rule::exists('roles', 'id')
                ->where('guard_name', 'admin')
                ->whereNot('name', 'super_admin')],
            'avatar' => ['sometimes', 'nullable', validationImage()],
        ];
    }

    protected function onChangePassword(): array
    {
        return [
            'id' => ['required', 'integer', Rule::exists('admins', 'id')
                ->whereNot('id', 1)],
            'password'=>'required|confirmed',
            'password_confirmation'=>'required|same:password'

        ];
    }

    public function rules(): array
    {
        if (request()->routeIs('admin.employee.store')) {
            return $this->onCreate();
        } elseif (request()->routeIs('admin.employee.update')) {
            return $this->onUpdate();
        } elseif (request()->routeIs('admin.employee.change-password')) {
            return $this->onChangePassword();
        } else {
            return [];
        }
    }

    public function attributes()
    {
        return [
            'role_id' => _trans('Role name'),
        ];
    }
}
