<?php

namespace App\Http\Requests\Admin;

use App\Enums\Status;
use App\Helpers\CPU\Models;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class JoinUsRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {

        $data = [
            'owner_name' => ['required', 'string', 'min:2', 'max:255'],
            'owner_email' => ['required', 'email', 'string', 'min:2', 'max:255', Rule::unique('users', 'email')],
            'owner_phone' => ['required', 'string', Rule::unique('owners', 'phone')],
            'subscription_type' => ['required', 'in:participation,percent'],
            'subscription_value' => [
                'required',
                'numeric',
                Rule::when($this->subscription_type == 'percent', [
                    'between:0,99.99'
                ]),
            ],
            'branch_type_id' => ['required', 'array', 'min:1', 'max:4',],
            'branch_type_id.*' => ['required', 'integer',
                Rule::exists('branch_types', 'id')
                    ->where('status', Status::Active->value),
            ],
            'country_id' => ['required', 'integer', Rule::exists('countries', 'id')->where('status', 1)],
            'governorate_id' => ['required', 'integer', Rule::exists('governorates', 'id')
                ->where('country_id', $this->country_id)
                ->where('status', 1)],
            'region_id' => ['required', 'integer', Rule::exists('regions', 'id')
                ->where('governorate_id', $this->governorate_id)
                ->where('status', 1)],
            'address' => ['required', 'string', 'min:2', 'max:255'],
            /*'lat' => ['required', 'string', 'regex:/^(\+|-)?(?:90(?:(?:\.0{1,8})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,8})?))$/'],
            'lng' => ['required', 'string', 'regex:/^(\+|-)?(?:180(?:(?:\.0{1,8})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,8})?))$/'],*/
            'lat' => ['required', 'string', 'max:255'],
            'lng' => ['required', 'string', 'max:255'],
            /*times_of_week*/
            'shops' => ['required', 'array', 'min:1', 'max:7'],
            'shops.*.day_id' => ['required', 'integer', Rule::exists('week_days', 'id')->where('status', 1)],//lte
            'shops.*.from' => ['required', 'date_format:H:i',],
            'shops.*.to' => ['required', 'date_format:H:i', 'after:shops.*.from'],
            'email' => ['required', 'email', 'string', 'min:2', 'max:255', Rule::unique('shops', 'email')],
            'phone' => ['required', 'string', Rule::unique('shops', 'phone')],
            'logo' => ['required', validationImage()],
            'password' => ['required', 'string', 'min:2', 'max:255', 'confirmed'],
            'password_confirmation' => ['required', 'string', 'min:2', 'max:255', 'same:password'],
            'brand_name' => ['required', 'string', 'min:2', 'max:255', Rule::unique('owners', 'brand_name')],
        ];

        foreach (locales() as $locale) {
            $data += [
                $locale . '.name' => ['required', 'string', 'min:2', 'max:100',
                    Rule::unique('shop_translations', 'name')],
                $locale . '.description' => ['required', 'string', 'min:2'],
            ];
        }
        return $data;
    }

    public function attributes()
    {
        $messages = [
            'branch_type_id' => _trans('Branch type name'),
            'country_id' => _trans('Country name'),
            'governorate_id' => _trans('Governorate name'),
            'region_id' => _trans('Region name'),

        ];
        foreach (locales() as $locale) {
            $messages += [
                $locale . '.name' => _trans('shop name') . ' (' . ucfirst($locale) . ')',
                $locale . '.description' => _trans('shop description') . ' (' . ucfirst($locale) . ')',
            ];
        }
        return $messages;
    }
}
