<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class LanguageRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    protected function onData(): array
    {
        return [
            'flag' => ['required', 'string', 'min:2', 'max:255'],
            'direction' => ['required', 'string', 'in:ltr,rtl'],
        ];
    }

    protected function onCreate(): array
    {
        return array_merge($this->onData(), [
            'name' => ['required', 'string', 'min:2', 'max:255', Rule::unique('languages', 'name')],
            'code' => ['required', 'string', 'min:2', 'max:255', Rule::unique('languages', 'code')],
        ]);
    }

    protected function onUpdate(): array
    {
        return array_merge($this->onData(), [
            'id' => ['required', 'integer', Rule::exists('languages', 'id')],
            'name' => ['required', 'string', 'min:2', 'max:255', Rule::unique('languages', 'name')
                ->ignore($this->id, 'id')
            ],
            'code' => ['required', 'string', 'min:2', 'max:255', Rule::unique('languages', 'code')
                ->ignore($this->id, 'id')
            ],
        ]);
    }

    public function rules(): array
    {
        if (request()->routeIs('admin.language.store')) {
            return $this->onCreate();
        } elseif (request()->routeIs('admin.language.update')) {
            return $this->onUpdate();
        } else {
            return [];
        }
    }
}
