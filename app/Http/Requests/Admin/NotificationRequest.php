<?php

namespace App\Http\Requests\Admin;

use App\Enums\Status;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class NotificationRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        $rules = [
            'notification_type' => ['required', 'string', 'in:all,customer,shop'],
            'custom_type' => ['required_unless:notification_type,all'],
            //'file' => ['required', 'mimes:csv'],
            /*'notification_type' => ['required', 'string', 'in:all,customer,restaurant'],
            'customers' => ['nullable', 'array', 'min:1'],
            'customers.*' => ['nullable', 'integer', Rule::exists('customers', 'id')],
            'customer_id' => ['nullable', 'integer', Rule::exists('restaurants', 'id')],*/
        ];
        if ($this->notification_type == 'customer' && $this->custom_type == 'custom') {
            $rules += [
                'file' => ['required',],
            ];
        }
        if ($this->notification_type == 'shop' && $this->custom_type == 'custom') {
            $rules += [
                'shop_id' => ['required', 'integer', Rule::exists('shops', 'id')
                    ->where('status', Status::Active->value)
                ],
                'file' => ['required',],

            ];
        }
        foreach (locales() as $locale) {
            $rules += [
                $locale . '.title' => ['required', 'string', 'min:2', 'max:255'],
                $locale . '.body' => ['required', 'string', 'min:2', 'max:10000'],
            ];
        }
        if ($this->notification_type == 'shop') {
            $rules += [
                'shop_id' => ['required', 'integer', Rule::exists('shops', 'id')
                    ->where('status', Status::Active->value)
                ]
            ];
        }
        return $rules;
    }

    public function attributes(): array
    {
        $messages = [];
        foreach (locales() as $locale) {
            $messages += [
                $locale . '.title' => _trans('title') . ' (' . ucfirst($locale) . ')',
                $locale . '.body' => _trans('description') . ' (' . ucfirst($locale) . ')',
            ];
        }
        return $messages;
    }
}
