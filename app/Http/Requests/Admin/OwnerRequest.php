<?php

namespace App\Http\Requests\Admin;

use App\Enums\Status;
use App\Models\Owner;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class OwnerRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    protected function onData(): array
    {
        return [
            'name' => ['required', 'string', 'min:2', 'max:255'],
            'address' => ['nullable', 'string', 'min:2', 'max:255'],
            'avatar' => ['nullable', validationImage()],
            'country_id' => ['required', 'integer', Rule::exists('countries', 'id')->where('status', 1)],
            'governorate_id' => ['required', 'integer', Rule::exists('governorates', 'id')
                ->where('country_id', $this->country_id)
                ->where('status', Status::Active->value)
            ],
            'region_id' => ['required', 'integer', Rule::exists('regions', 'id')
                ->where('governorate_id', $this->governorate_id)
                ->where('status', Status::Active->value)
            ],
        ];
    }

    protected function onCreate(): array
    {
        return array_merge($this->onData(), [
            'email' => ['required', 'email', 'string', 'min:2', 'max:255',
                Rule::unique('users', 'email')
                    ->where('userable_type', Owner::class)
            ],
            'phone' => ['required', 'string', Rule::unique('owners', 'phone')
            ],
            'password' => ['required', 'string', 'min:2', 'max:255', 'confirmed'],
            'password_confirmation' => ['required', 'string', 'min:2', 'max:255', 'same:password'],
            'brand_name' => ['required', 'string', 'min:2', 'max:255', Rule::unique('owners', 'brand_name')],
        ]);
    }

    protected function onUpdate(): array
    {
        return array_merge($this->onData(), [
            'id' => ['required', 'integer', Rule::exists('owners', 'id')],
            'email' => ['required', 'email', 'string', 'min:2', 'max:255',
                Rule::unique('users', 'email')
                    ->where('userable_type', Owner::class)
                    ->ignore($this->id, 'userable_id')
            ],
            'phone' => ['required', 'string',
                Rule::unique('owners', 'phone')
                    ->ignore($this->id, 'id')
            ],
            'brand_name' => ['required', 'string', 'min:2', 'max:255',
                Rule::unique('owners', 'brand_name')
                ->ignore($this->id, 'id')
            ],
        ]);
    }

    protected function onChangePassword(): array
    {
        return [
            'id' => ['required', 'integer', Rule::exists('owners', 'id')
//                ->where('userable_type', Owner::class)
            ],
            'password' => ['required', 'string', 'min:3', 'confirmed'],
            'password_confirmation' => ['required', 'same:password'],

        ];
    }

    public function rules(): array
    {
        if (request()->routeIs('admin.owner.store')) {
            return $this->onCreate();
        } elseif (request()->routeIs('admin.owner.update')) {
            return $this->onUpdate();
        } elseif (request()->routeIs('admin.owner.change-password')) {
            return $this->onChangePassword();
        } else {
            return [];
        }
    }

    public function attributes()
    {
        return [
            'brand_name' => _trans('Brand Name'),
            'country_id' => _trans('Country name'),
            'governorate_id' => _trans('Governorate name'),
            'region_id' => _trans('Region name'),
        ];
    }
}
