<?php

namespace App\Http\Requests\Admin;

use App\Enums\Status;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PaymentRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }


    public function rules(): array
    {
        return [
            'payment_id' => ['required', 'integer', Rule::exists('payments', 'id')
                ->where('status', Status::Not_Active->value)
            ],
            'amount' => ['required', 'numeric', 'min:0'],
        ];
    }
}
