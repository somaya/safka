<?php

namespace App\Http\Requests\Admin;

use App\Enums\DiscountType;
use App\Enums\Status;
use App\Helpers\CPU\Models;
use App\Models\Product;
use App\Rules\IsOwner;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class ProductRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    protected function onData(): array
    {
        $rules = [
            'category_id' => ['required', 'integer', Rule::exists('categories', 'id')
                ->whereNull('parent_id')
                ->where('status', Status::Active->value)
            ],
            'sub_category_id' => ['nullable', 'integer', Rule::exists('categories', 'id')
                ->where('parent_id', $this->category_id)
                ->where('status', Status::Active->value)
            ],
//            'owner_id' => ['required', 'integer', Rule::exists('owners', 'id')],
//            'shop_id' => ['required', 'integer', Rule::exists('shops', 'id')
//                ->where('owner_id', $this->owner_id)
//                ->where('status', Status::Active->value)
//            ],
            'price' => ['required', 'numeric', 'min:0'],
            'maximum_order_number' => ['required', 'numeric', 'min:1'],
            'video' => ['sometimes', 'nullable', 'url'],

            /*sizes*/
            'sizes' => ['nullable', 'array', 'max:3'],
            'colors' => ['nullable', 'array', 'max:10'],
            /* discount */
            'discount_type' => ['nullable', 'string', 'in:percentage,amount'],
        ];
        if (Auth::guard('admin')->check()) {
            $rules += [
                'owner_id' => ['required', 'integer', new IsOwner()],
                'shop_id' => ['required', 'integer', Rule::exists('shops', 'id')
                    ->where('status', 1)->where('owner_id', $this->owner_id)],

            ];
        } else {
            $rules += [
                'shop_id' => ['required', 'integer', Rule::exists('shops', 'id')
                    ->where('status', 1)->where('owner_id', getOwner())],

            ];
        }
        if (in_array($this->discount_type, [DiscountType::Amount->value, DiscountType::Percentage->value])) {
            $rules += [
                'start_date' => ['required', 'date_format:Y-m-d', 'after_or_equal:' . now()->format('Y-m-d')],
                'end_date' => ['required', 'date_format:Y-m-d', 'after:start_date'],
            ];
            if ($this->discount_type == DiscountType::Amount->value) {
                $rules += [
                    'discount_price' => ['required', 'numeric', 'min:0', 'lte:price'],
                ];
            } else {
                $rules += [
                    'discount_price' => ['required', 'numeric', 'min:0', 'max:100'],
                ];
            }
        }

        foreach (locales() as $locale) {
            $rules += [
                'sizes.*.name:' . $locale => ['nullable', 'required_with:sizes.*.price', 'string'],
                'sizes.*.price' => ['nullable', 'required_with:sizes.*.name:' . $locale, 'numeric'],
                'colors.*.name:'.$locale  => ['nullable', 'string'],
                'colors.*.code' => ['nullable','required_with:colors.*.name:'.$locale, 'string'],
                $locale . '.name' => ['required', 'string', 'min:2', 'max:100'],
                $locale . '.description' => ['sometimes', 'nullable', 'string', 'min:2'],
            ];
        }
        return $rules;
    }

    protected function onCreate(): array
    {
        $rules = [
            'icon' => ['required', validationImage()],
            'images' => ['required', 'array', 'min:1', 'max:5'],
            'images.*' => ['required', validationImage()],
        ];
        return array_merge($this->onData(), $rules);
    }

    protected function onUpdate(): array
    {
        $files = getFiles(['id' => $this->id, 'type' => Product::class])->count();
        return array_merge(array_merge([
            'id' => ['required', 'integer', Rule::exists('products', 'id')
//                ->where('owner_id', $this->owner_id)
//                ->where('shop_id', $this->shop_id)
            ],
            'icon' => ['nullable', validationImage()],
            'images.*' => ['nullable', validationImage()],
        ], Models::ruleImages($files)), $this->onData());
    }

    public function rules(): array
    {
        if (request()->routeIs('product.store')) {
            return $this->onCreate();
        } elseif (request()->routeIs('product.update')) {
            return $this->onUpdate();
        } else {
            return [];
        }
    }
}
