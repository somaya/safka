<?php

namespace App\Http\Requests\Admin;

use App\Enums\Status;
use App\Helpers\CPU\Models;
use App\Models\Save;
use App\Rules\IsOwner;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class SaveRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    protected function onData(): array
    {
        $rules = [
            'category_id' => ['required', 'integer', Rule::exists('categories', 'id')
                ->whereNull('parent_id')
                ->where('status', Status::Active->value)
            ],
//            'owner_id' => ['required', 'integer', Rule::exists('owners', 'id')
//                ->whereNull('created_by')
//            ],
//            'shop_id' => ['required', 'integer', Rule::exists('shops', 'id')
//                ->where('owner_id', $this->owner_id)
//                ->where('status', Status::Active->value)
//            ],
            'video' => ['nullable', 'string', 'url', 'max:255'],
            'price' => ['required', 'numeric', 'min:0'],
            'start' => ['required', 'date_format:Y-m-d', 'after_or_equal:' . now()->format('Y-m-d')],
            'end' => ['required', 'date_format:Y-m-d', 'after_or_equal:start'],
            'from' => ['required', 'date_format:H:i',],
            'to' => ['required', 'date_format:H:i', 'after:from'],
            /* products */
            'products' => ['required', 'array', 'min:1', 'max:3'],
            'products.*.product_id' => ['required', 'integer', Rule::exists('products', 'id')
                ->where('shop_id', $this->shop_id)
                ->where('status', Status::Active->value)
            ],
            'products.*.quantity' => ['required', 'integer', 'min:1'],
        ];
        if (Auth::guard('admin')->check()) {
            $rules += [
                'owner_id' => ['required', 'integer', new IsOwner()],
                'shop_id' => ['required', 'integer', Rule::exists('shops', 'id')
                    ->where('status', 1)->where('owner_id', $this->owner_id)],

            ];
        } else {
            $rules += [
                'shop_id' => ['required', 'integer', Rule::exists('shops', 'id')
                    ->where('status', 1)->where('owner_id', getOwner())],

            ];
        }
        foreach (locales() as $locale) {
            $rules += [
                $locale . '.name' => ['required', 'string', 'min:2', 'max:100'],
                $locale . '.description' => ['sometimes', 'nullable', 'string', 'min:2'],

            ];
        }
        return $rules;
    }

    protected function onCreate(): array
    {
        return array_merge([
            'images' => ['required', 'array', 'min:1', 'max:5'],
            'images.*' => ['required', validationImage()],
            'icon' => ['required', validationImage()],

        ], $this->onData());
    }

    protected function onUpdate(): array
    {
        $files = getFiles(['id' => $this->id, 'type' => Save::class])->count();
        return array_merge(array_merge([
            'id' => ['required', 'integer', Rule::exists('saves', 'id')
//                ->where('owner_id', $this->owner_id)
//                ->where('shop_id', $this->shop_id)
            ],
            'icon' => ['nullable', validationImage()],
        ], Models::ruleImages($files)), $this->onData());
    }

    public function rules(): array
    {
        if (request()->routeIs('save.store')) {
            return $this->onCreate();
        } elseif (request()->routeIs('save.update')) {
            return $this->onUpdate();
        } else {
            return [];
        }
    }

    public function attributes(): array
    {
        $messages = [
            'category_id' => _trans('Category name'),
            'owner_id' => _trans('Owner name'),
            'shop_id' => _trans('Shop name'),
            'icon' => _trans('Icon'),
            'products' => _trans('Product name'),
            'products.*.product_id' => _trans('Product name'),
            'products.*.quantity' => _trans('Quantity'),
            'images' => _trans('Save images'),
            'images.*' => _trans('Save images'),
        ];
        foreach (locales() as $locale) {
            $messages += [
                $locale . '.name' => _trans('Save name') . ' (' . ucfirst($locale) . ')',
                $locale . '.description' => _trans('Save description') . ' (' . ucfirst($locale) . ')',
            ];
        }
        return $messages;
    }
}
