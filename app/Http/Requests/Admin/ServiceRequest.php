<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class ServiceRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    protected function onData(): array
    {
        $rules = [
            'ranking' => ['required', 'integer'],
        ];
        foreach (locales() as $locale) {
            $rules += [
                $locale . '.name' => ['required', 'string', 'min:2', 'max:100'],
                $locale . '.description' => ['sometimes', 'nullable', 'string', 'min:2',],
            ];
        }
        return $rules;
    }

    protected function onCreate(): array
    {
        return array_merge($this->onData(), [
            'image' =>  validationImage(),
        ]);
    }

    protected function onUpdate(): array
    {
        return array_merge($this->onData(), [
            'image' => validationImage(),
        ]);
    }

    public function rules(): array
    {
        if (request()->routeIs('admin.service.store')) {
            return $this->onCreate();
        } elseif (request()->routeIs('admin.service.update')) {
            return $this->onUpdate();
        } else {
            return [];
        }
    }

    public function attributes(): array
    {
        $messages = [
            'ranking' => _trans('Package price'),
        ];
        foreach (locales() as $locale) {
            $messages += [
                $locale . '.name' => _trans('Service name') . ' (' . ucfirst($locale) . ')',
                $locale . '.description' => _trans('Service name') . ' (' . ucfirst($locale) . ')',
            ];
        }
        return $messages;
    }
}
