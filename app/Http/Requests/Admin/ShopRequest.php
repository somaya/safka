<?php

namespace App\Http\Requests\Admin;

use App\Enums\Status;
use App\Enums\SubscriptionType;
use App\Models\Shop;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ShopRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    protected function onData(): array
    {
        $data= [
            'owner_id' => ['required', 'integer', Rule::exists('owners', 'id')->whereNull('created_by')],
            'subscription_type' => ['required', 'in:participation,percent'],
            'subscription_value' => ['required', 'numeric', 'min:0',
                Rule::when($this->subscription_type == 'percent', [
                    'between:0,99.99'
                ]),
            ],
            'country_id' => ['required', 'integer', Rule::exists('countries', 'id')
                ->where('status', Status::Active->value)
            ],
            'governorate_id' => ['required', 'integer', Rule::exists('governorates', 'id')
                ->where('country_id', $this->country_id)
                ->where('status', Status::Active->value)
            ],
            'region_id' => ['required', 'integer', Rule::exists('regions', 'id')
                ->where('governorate_id', $this->governorate_id)
                ->where('status', Status::Active->value)
            ],
            'address' => ['nullable', 'string', 'min:2', 'max:255'],
            /*'lat' => ['required', 'string', 'regex:/^(\+|-)?(?:90(?:(?:\.0{1,8})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,8})?))$/'],
            'lng' => ['required', 'string', 'regex:/^(\+|-)?(?:180(?:(?:\.0{1,8})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,8})?))$/'],*/
            'lat' => ['nullable', 'string', 'max:255'],
            'lng' => ['nullable', 'string', 'max:255'],
            'minimum_order_price' => ['required','numeric', 'min:1'],
            /*times_of_week*/
            'shops' => ['nullable', 'array', 'min:1', 'max:14'],
            'shops.*.day_id' => ['nullable', 'integer', Rule::exists('week_days', 'id')
                ->where('status', Status::Active->value)
            ],
            'shops.*.from' => ['nullable', 'date_format:H:i',],
            'shops.*.to' => ['nullable', 'date_format:H:i', 'after:shops.*.from'],
            'branch_type_id' => ['nullable', 'array', 'min:1', 'max:4',],
            'branch_type_id.*' => ['nullable', 'integer',
                Rule::exists('branch_types', 'id')
                    ->where('status', Status::Active->value),
            ],
        ];
        if ($this->subscription_type == SubscriptionType::Percent->value) {
            $data += [
                'invoice_duration' => ['required', 'integer', 'min:1'],
            ];
        }
        return $data;
    }

    public function onCreate(): array
    {
        $rules = [
            'email' => ['required', 'email', 'string', 'min:2', 'max:255', Rule::unique('shops', 'email')],
            'phone' => ['nullable', 'string', Rule::unique('shops', 'phone')],
            'logo' => ['required', validationImage()],
            'image' => ['required', validationImage()],
        ];
        if ($this->subscription_type == SubscriptionType::participation->value) {
            $rules += [
                'subscribe_date' => ['required', 'date', 'date_format:Y-m-d', 'after_or_equal:' . now()->format('Y-m-d')], //Subscription
            ];
        }
        foreach (locales() as $locale) {
            $rules += [
                $locale . '.name' => ['required', 'string', 'min:2', 'max:100',
                    Rule::unique('shop_translations', 'name')],
                $locale . '.description' => ['required', 'string', 'min:2'],
            ];
        }
        return array_merge($this->onData(), $rules);
    }

    public function onUpdate(): array
    {
        $rules = [
            'id' => ['required', 'integer', Rule::exists('shops', 'id')],
            'email' => ['required', 'email', 'string', 'min:2', 'max:255',
                Rule::unique('shops', 'email')->ignore($this->id, 'id')],
            'phone' => ['nullable', 'string',
                Rule::unique('shops', 'phone')->ignore($this->id, 'id')],
            'logo' => ['sometimes', 'nullable', validationImage()],
            'image' => ['sometimes', 'nullable', validationImage()],
        ];
        if ($this->subscription_type == SubscriptionType::participation->value) {
            $shop = Shop::query()->find($this->id);
            if ($shop->subscribe_date == $this->subscribe_date) {
                $rules += [
                    'subscribe_date' => ['required', 'date', 'date_format:Y-m-d'],
                ];
            } else {
                $rules += [
                    'subscribe_date' => ['required', 'date', 'date_format:Y-m-d', 'after_or_equal:' . now()->format('Y-m-d')], //Subscription
                ];
            }
        }
        foreach (locales() as $locale) {
            $rules += [
                $locale . '.name' => ['required', 'string', 'min:2', 'max:100',
                    Rule::unique('shop_translations', 'name')
                        ->ignore($this->id, 'shop_id')],
                $locale . '.description' => ['required', 'string', 'min:2'],
            ];
        }
        return array_merge($this->onData(), $rules);
    }


    public function rules(): array
    {
        if (request()->routeIs('admin.shop.store')) {
            return $this->onCreate();
        } elseif (request()->routeIs('admin.shop.update')) {
            return $this->onUpdate();
        } else {
            return [];
        }
    }

    public function attributes()
    {
        $messages = [
            'owner_id' => _trans('Owner name'),
            'branch_type_id' => _trans('Branch type name'),
            'country_id' => _trans('Country name'),
            'governorate_id' => _trans('Governorate name'),
            'region_id' => _trans('Region name'),
            'shops' => _trans('Working days'),
            'shops.*.day_id' => _trans('day'),
            'shops.*.from' => _trans('from'),
            'shops.*.to' => _trans('to'),

        ];
        foreach (locales() as $locale) {
            $messages += [
                $locale . '.name' => _trans('Shop name') . ' (' . ucfirst($locale) . ')',
                $locale . '.description' => _trans('Shop description') . ' (' . ucfirst($locale) . ')',
            ];
        }
        return $messages;
    }
}
