<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SocialMediaRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    protected function onCreate(): array
    {
        return [
            'slug' => ['required', 'string', 'min:1', 'max:100', Rule::unique('social_media', 'slug')],
            'icon' => ['required', validationImage()],
            'url' => ['required', 'string', 'min:1', 'max:100', 'url'],
        ];
    }

    protected function onUpdate(): array
    {
        return [
            'id' => ['required', 'integer', Rule::exists('social_media', 'id')],
            'slug' => ['required', 'string', 'min:1', 'max:100', Rule::unique('social_media', 'slug')
                ->ignore($this->id, 'id')],
            'icon' => ['nullable', validationImage()],
            'url' => ['required', 'string', 'min:1', 'max:100', 'url'],
        ];
    }

    public function rules(): array
    {
        if (request()->routeIs('admin.social-media.store')) {
            return $this->onCreate();
        } elseif (request()->routeIs('admin.social-media.update')) {
            return $this->onUpdate();
        } else {
            return [];
        }
    }

    public function attributes()
    {
        return [
            'url' => _trans('Link social media'),
            'slug' => _trans('Name social media'),
            'icon' => _trans('Icon'),
        ];
    }
}
