<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class VehicleTypeRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    protected function onCreate(): array
    {
        $rules = [];
        foreach (locales() as $locale) {
            $rules += [
                $locale . '.name' => ['required', 'string', 'min:2', 'max:100',
                    Rule::unique('vehicle_type_translations', 'name')],
            ];
        }
        return $rules;
    }

    protected function onUpdate(): array
    {
        $rules = [
            'id' => ['required', 'integer', Rule::exists('vehicle_types', 'id')],
        ];
        foreach (locales() as $locale) {
            $rules += [
                $locale . '.name' => ['required', 'string', 'min:2', 'max:100',
                    Rule::unique('vehicle_type_translations', 'name')
                        ->ignore($this->id, 'vehicle_type_id')
                ],
            ];
        }
        return $rules;
    }

    public function rules(): array
    {
        if (request()->routeIs('admin.vehicle-type.store')) {
            return $this->onCreate();
        } elseif (request()->routeIs('admin.vehicle-type.update')) {
            return $this->onUpdate();
        } else {
            return [];
        }
    }
}
