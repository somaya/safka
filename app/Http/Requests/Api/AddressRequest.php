<?php

namespace App\Http\Requests\Api;

use App\Traits\ApiResponses;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class AddressRequest extends FormRequest
{
    use ApiResponses;

    public function authorize(): bool
    {
        return true;
    }

    protected function onData(): array
    {
        return [
            'country_id' => ['required', 'integer', Rule::exists('countries', 'id')
                ->where('status', 1)],
            'governorate_id' => ['required', 'integer', Rule::exists('governorates', 'id')
                ->where('country_id', $this->country_id)
                ->where('status', 1)],
            'region_id' => ['required', 'integer', Rule::exists('regions', 'id')
                ->where('governorate_id', $this->governorate_id)
                ->where('status', 1)],
            'address' => ['required', 'string', 'min:2', 'max:255'],

        ];
    }

    protected function onUpdate(): array
    {
        return array_merge($this->onData(), [
            'address_id' => ['required', 'integer', Rule::exists('addresses', 'id')],
        ]);
    }

    public function rules(): array
    {
        if (request()->routeIs('api.address.store')) {
            return $this->onData();
        } elseif (request()->routeIs('api.address.update')) {
            return $this->onUpdate();
        } else {
            return [];
        }
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException($this->failure(message: $validator->errors()));
    }
}
