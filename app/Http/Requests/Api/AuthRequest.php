<?php

namespace App\Http\Requests\Api;

use App\Enums\Status;
use App\Enums\UserType;
use App\Models\Customer;
use App\Models\User;
use App\Traits\ApiResponses;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;
use Propaganistas\LaravelPhone\PhoneNumber;

class AuthRequest extends FormRequest
{
    use ApiResponses;

    public function authorize(): bool
    {
        return true;
    }

    protected function onRegister(): array
    {
        $phone=(string) PhoneNumber::make($this->request->get('phone'), $this->request->get('country_code'));
        return [
            'first_name' => ['required', 'string', 'min:2', 'max:255'],
            'last_name' => ['required', 'string', 'min:2', 'max:255'],
            'facility_name' => ['required', 'string', 'min:2', 'max:255'],
            'branch_type' => ['required', 'string','min:2', 'max:255'],
            'lang' => ['required', 'string', 'in:' . implode(',', languages()->pluck('code')->toArray())],
            'email' => ['nullable', 'email', 'min:2', 'max:100', Rule::unique('users', 'email')],
            'country_code' => ['required', 'string', 'min:2'],
            'phone' => ['required','phone:country_code','min:10',  function($attribute, $value, $fail) use($phone){
                $exists = Customer::where('phone',$phone)->exists();
                if($exists){
                    $fail($attribute.trans(' has already been taken.'));
                }
            }],
            'password' => ['required', 'string', 'min:2', 'max:100', 'confirmed'],
            'password_confirmation' => ['required', 'string', 'min:2', 'max:100', 'same:password'],
            'avatar' => ['nullable', validationImage()],
            'country_id' => ['nullable', 'integer', Rule::exists('countries', 'id')->where('status', 1)],
            'governorate_id' => ['nullable', 'integer', Rule::exists('governorates', 'id')
                ->where('country_id', $this->country_id)
                ->where('status', 1)],
            'region_id' => ['nullable', 'integer', Rule::exists('regions', 'id')
                ->where('governorate_id', $this->governorate_id)
                ->where('status', 1)],
            'address' => ['required', 'string', 'min:2', 'max:255'],
            'lat' => ['required', 'string', 'max:255'],
            'lng' => ['required', 'string', 'max:255'],
            'birthday' => ['nullable', 'date', 'date_format:Y-m-d', 'before:13 years ago'],
            'gender' => ['nullable', 'string', 'in:male,female', 'min:1', 'max:255'],

        ];
    }

    protected function onSendVerificationCode(): array
    {
        return [
            'phone' => [
                'required',
                'regex:/^(01)[0-9]{9}$/',
                Rule::unique('users', 'phone')
            ],
        ];
    }


    protected function onLogin(): array
    {
        return [
            'email' => ['required', 'email', 'min:2', 'max:100', Rule::exists('users', 'email')],
            'password' => ['required', 'string', 'min:2', 'max:100'],
        ];
    }

    protected function onCheckSocial(): array
    {
        return [
            'provider_type' => ['required', 'string', 'min:2', 'max:100', 'in:facebook,google'],
            'provider_id' => ['required', 'string', 'min:2', 'max:100'],
        ];
    }

    protected function onLoginSocial(): array
    {
        $rule = [
            'provider_type' => ['required', 'string', 'min:2', 'max:100', 'in:facebook,google'],
            'provider_id' => ['required', 'string', 'min:2', 'max:100'],
        ];
        $user = Customer::query()->where([
            ['provider_type', '=', $this->provider_type],
            ['provider_id', '=',$this->provider_id],
        ])->first();
        if (!$user) {
            $rule += [
                'first_name' => ['required', 'string', 'min:1', 'max:255'],
                'last_name' => ['required', 'string', 'min:1', 'max:255'],
                'email' => ['nullable', 'email', 'min:2', 'max:100'],
                'fcm_token' => ['nullable', 'string', 'min:2', 'max:255', Rule::unique('notification_tokens', 'fcm_token')],
            ];
        }
        return $rule;
    }

    protected function onForgotPassword(): array
    {
        return [
            'username' => ['required', 'min:2', 'max:100',],
            'country_code' => ['nullable', 'string', Rule::exists('countries', 'code')
                ->where('status', Status::Active->value)
            ]
        ];
    }

    protected function onConfirm()
    {
        return [
            'username' => ['required', 'min:2', 'max:100',],
            'code' => ['required', 'string', 'min:2', 'max:100'],
            'country_code' => ['nullable', 'string', Rule::exists('countries', 'code')
                ->where('status', Status::Active->value)
            ]
        ];
    }

    protected function onResetPassword()
    {
        return [
            'token' => ['required', 'string', 'min:2', 'max:100'],
            'password' => ['required', 'string', 'min:2', 'max:100', 'confirmed'],
            'password_confirmation' => ['required', 'string', 'min:2', 'max:100', 'same:password'],
        ];
    }

    protected function onUpdateProfile(): array
    {
        $phone=(string) PhoneNumber::make($this->request->get('phone'), 'EG');

        return [
            'name' => ['required', 'string', 'min:2', 'max:255'],
            'facility_name' => ['required', 'string', 'min:2', 'max:255'],
            'email' => ['nullable', 'email', 'min:2', 'max:100',
                Rule::unique('users', 'email')->ignore(auth()->id(), 'id')],
//            'phone' => ['required', 'numeric',
//                Rule::unique('users', 'phone')->ignore(auth()->id(), 'id')],
            'country_code' => ['required', 'string', 'min:2'],
            'phone' => ['required','phone:country_code','min:10',  function($attribute, $value, $fail) use($phone){
                $exists = Customer::where('phone',$phone)->where('id','!=',auth()->user()->userable->id)->exists();
                if($exists){
                    $fail($attribute.trans(' has already been taken.'));
                }
            }],
            'avatar' => ['sometimes', 'nullable', validationImage()],
            'country_id' => ['nullable', 'integer', Rule::exists('countries', 'id')->where('status', 1)],
            'branch_type' => ['nullable', 'string'],
            'governorate_id' => ['nullable', 'integer', Rule::exists('governorates', 'id')
                ->where('country_id', $this->country_id)
                ->where('status', 1)],
            'region_id' => ['nullable', 'integer', Rule::exists('regions', 'id')
                ->where('governorate_id', $this->governorate_id)
                ->where('status', 1)],
            'address' => ['required', 'string', 'min:2', 'max:255'],
            'lat' => ['required', 'string', 'max:255'],
            'lng' => ['required', 'string', 'max:255'],
            'gender' => ['nullable', 'string', 'in:male,female', 'max:255'],
            'birthday' => ['nullable', 'date', 'date_format:Y-m-d', 'before:10 years ago'],

        ];

    }


    protected function onChangePassword(): array
    {
        return [
            'old_password' => 'required|min:2|max:100',
            'password' => ['required', 'string', 'min:2', 'max:100', 'confirmed'],
            'password_confirmation' => ['required', 'same:password', 'min:2', 'max:100'],
        ];
    }

    protected function onFirebaseToken(): array
    {
        return [
            'fcm_token' => ['required', 'string', 'min:2', 'max:255',
                /*Rule::unique('notification_tokens', 'fcm_token')
                    ->where('tokenable_type', Customer::class)
                    ->where('tokenable_id', request()->user()->customer->id)*/],
        ];
    }

    protected function onChangeLanguage(): array
    {
        return [
            'code' => ['required', 'string', 'min:2', 'max:255', Rule::exists('languages', 'code')
                ->where('status', Status::Active->value)],
        ];
    }

    protected function onFurnitureBy(): array
    {
        return [
            'lat' => ['required', 'numeric'],
            'lng' => ['required', 'numeric'],
        ];
    }

    public function rules(): array
    {
        if (request()->routeIs('api.register')) {
            return $this->onRegister();
        } elseif (request()->routeIs('api.verification-code')) {
            return $this->onSendVerificationCode();
        } elseif (request()->routeIs('api.login')) {
            return $this->onLogin();
        } elseif (request()->routeIs('api.forget-password') || request()->routeIs('api.forget-password')) {
            return $this->onForgotPassword();
        } elseif (request()->routeIs('api.confirm.forgot-password')) {
            return $this->onConfirm();
        } elseif (request()->routeIs('api.reset-password')) {
            return $this->onResetPassword();
        } elseif (request()->routeIs('api.update-profile')) {
            return $this->onUpdateProfile();
        } elseif (request()->routeIs('api.change-password')) {
            return $this->onChangePassword();
        } elseif (request()->routeIs('api.firebase-token')) {
            return $this->onFirebaseToken();
        } elseif (request()->routeIs('api.check-social')) {
            return $this->onCheckSocial();
        } elseif (request()->routeIs('api.login-social')) {
            return $this->onLoginSocial();
        } elseif (request()->routeIs('api.change-language')) {
            return $this->onChangeLanguage();
        } elseif (request()->routeIs('api.furniture-nearby')) {
            return $this->onFurnitureBy();
        } else {
            return [];
        }
    }

    public function attributes(): array
    {
        return [
            'name' => _trans('Full name'),
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException($this->failure(message: $validator->errors()));
    }
}
