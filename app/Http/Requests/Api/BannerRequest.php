<?php

namespace App\Http\Requests\Api;

use App\Traits\ApiResponses;
use Illuminate\Foundation\Http\FormRequest;

class BannerRequest extends FormRequest
{
    use ApiResponses;

    public function authorize(): bool
    {
        return true;
    }

    public function rules()
    {
        return [
            'resource_type' => ['sometimes', 'string', 'in:Product,Home,Category,Shop'],
            'banner_type' => ['sometimes', 'string', 'in:Main,Footer,Popup'],
        ];
    }
}
