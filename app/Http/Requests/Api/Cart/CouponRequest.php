<?php

namespace App\Http\Requests\Api\Cart;

use App\Enums\Status;
use App\Rules\ExpireCoupon;
use App\Traits\ApiResponses;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class CouponRequest extends FormRequest
{
    use ApiResponses;

    public function authorize(): bool
    {
        return true;
    }


    public function rules(): array
    {
        return [
            'code' => ['required', 'string', 'max:10', Rule::exists('coupons', 'code')
                ->where('status',Status::Active->value),
                new ExpireCoupon()
            ],
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException($this->failure(message: $validator->errors()));
    }
}
