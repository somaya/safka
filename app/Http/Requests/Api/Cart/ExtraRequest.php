<?php

namespace App\Http\Requests\Api\Cart;

use App\Enums\Status;
use App\Traits\ApiResponses;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class ExtraRequest extends FormRequest
{
    use ApiResponses;

    public function authorize(): bool
    {
        return true;
    }


    public function rules(): array
    {
        return [
            'restaurant_id' => ['required', 'integer', Rule::exists('restaurants', 'id')
                ->where('status', Status::Active->value)],
            'extra_id' => ['required', 'integer', Rule::exists('extras', 'id')
                ->where('status', Status::Active->value)
                ->where('restaurant_id', $this->restaurant_id)],
            'qty' => ['required', 'integer', 'min:1'],
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException($this->failure(message: $validator->errors()));
    }
}
