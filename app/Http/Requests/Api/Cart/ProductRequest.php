<?php

namespace App\Http\Requests\Api\Cart;

use App\Enums\Status;
use App\Rules\CheckCount;
use App\Traits\ApiResponses;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class ProductRequest extends FormRequest
{
    use ApiResponses;

    public function authorize(): bool
    {
        return true;
    }


    public function rules(): array
    {
        return [
//            'country_id' => ['required', 'integer', Rule::exists('countries', 'id')
//                ->where('status', Status::Active->value)
//            ],
            'shop_id' => ['required', 'integer', Rule::exists('shops', 'id')
                ->where('country_id', request('country_id'))
                ->where('status', Status::Active->value)
            ],

            'model_type' => ['required', 'string', 'in:product'],

            'model_id' => ['required', 'integer', Rule::exists('products', 'id')
                ->where('status', Status::Active->value)
                ->where('shop_id', $this->shop_id)],

            'qty' => ['required', 'integer', 'min:1'],

            'size_id' => ['nullable', 'integer', Rule::exists('sizes', 'id')
                ->where('status', Status::Active->value)
                ->where('product_id', $this->model_id)
                ->where('shop_id', $this->shop_id)
            ],
            'color_id' => ['nullable', 'integer', Rule::exists('colors', 'id')
                ->where('status', Status::Active->value)
                ->where('product_id', $this->model_id)
                ->where('shop_id', $this->shop_id)
            ],
            'extras' => ['nullable', 'array', 'min:1',  new CheckCount()],

            'extras.*.id' => ['nullable', 'integer', Rule::exists('extras', 'id')
                ->where('status', Status::Active->value)
                ->where('shop_id', $this->shop_id),
            ],

            'extras.*.qty' => ['nullable', 'integer', 'min:1'],
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException($this->failure(message: $validator->errors()));
    }
}
