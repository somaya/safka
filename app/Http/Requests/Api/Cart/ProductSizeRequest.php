<?php

namespace App\Http\Requests\Api\Cart;

use App\Enums\Status;
use App\Traits\ApiResponses;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class ProductSizeRequest extends FormRequest
{
    use ApiResponses;

    public function authorize(): bool
    {
        return true;
    }


    public function rules(): array
    {
        return [
            'shop_id' => ['required', 'integer', Rule::exists('shops', 'id')
                ->where('status', Status::Active->value)],
            'model_type' => ['required', 'string', 'in:product_size'],
            'model_id' => ['required', 'integer', Rule::exists('products', 'id') // product_id
            ->where('status', Status::Active->value)
                ->where('shop_id', $this->shop_id)],
            'size_id' => ['required', 'integer', Rule::exists('sizes', 'id') // siz_id
            ->where('status', Status::Active->value)
                ->where('shop_id', $this->shop_id)
                ->where('product_id', $this->model_id)
            ],
            'qty' => ['required', 'integer', 'min:1'],
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException($this->failure(message: $validator->errors()));
    }
}
