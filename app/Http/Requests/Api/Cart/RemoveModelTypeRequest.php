<?php

namespace App\Http\Requests\Api\Cart;

use App\Models\Discount;
use App\Models\Offer;
use App\Models\Product;
use App\Models\Save;
use App\Traits\ApiResponses;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class RemoveModelTypeRequest extends FormRequest
{
    use ApiResponses;

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        $data = [
            'model_type' => ['required', 'string', 'in:product,offer,discount,save'],
        ];
        if ($this->model_type == 'product') {
            $data += [
                'model_id' => [
                    'required',
                    'integer',
                    Rule::exists('cart_details', 'modelable_id')
                        ->where('modelable_type', Product::class)
                ],
                'size_id' => ['nullable', 'integer', Rule::exists('cart_details', 'size_id')
                    ->where('modelable_id', $this->model_id)
                    ->where('modelable_type', Product::class)]
            ];
        }

        if ($this->model_type == 'offer') {
            $data += [
                'model_id' => [
                    'required',
                    'integer',
                    Rule::exists('cart_details', 'modelable_id')
                        ->where('modelable_type', Offer::class)
                ],
            ];
        }

        if ($this->model_type == 'discount') {
            $data += [
                'model_id' => [
                    'required',
                    'integer',
                    Rule::exists('cart_details', 'modelable_id')
                        ->where('modelable_type', Discount::class)
                ],
            ];
        }

        if ($this->model_type == 'save') {
            $data += [
                'model_id' => [
                    'required',
                    'integer',
                    Rule::exists('cart_details', 'modelable_id')
                        ->where('modelable_type', Save::class)
                ],
            ];
        }
        return $data;

    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException($this->failure(message: $validator->errors()));
    }
}
