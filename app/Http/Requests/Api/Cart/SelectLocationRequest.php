<?php

namespace App\Http\Requests\Api\Cart;

use App\Enums\Status;
use App\Rules\ExpireCoupon;
use App\Traits\ApiResponses;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class SelectLocationRequest extends FormRequest
{
    use ApiResponses;

    public function authorize(): bool
    {
        return true;
    }

    protected function onSelectLocation()
    {
        return [
//            'country_id' => ['required', 'integer', Rule::exists('countries', 'id')
//                ->where('status', Status::Active->value)
//            ],

            'location_id' => ['required', 'integer', Rule::exists('locations', 'id')
                ->where('country_id', request('country_id'))
                ->where('customer_id', request()->user()->userable->id)
            ],
        ];
    }

    protected function onvVerifyPhoneLocation()
    {
        return [
            'location_id' => ['required', 'integer', Rule::exists('locations', 'id')
                ->where('customer_id', request()->user()->userable->id)],
            'phone_verified' => ['required', 'in:1,0', 'string']
        ];
    }

    public function rules(): array
    {
        if (request()->routeIs('api.cart.select-location')) {
            return $this->onSelectLocation();
        } elseif (request()->routeIs('api.cart.verify-phone-location')) {
            return $this->onvVerifyPhoneLocation();
        } else {
            return [];
        }
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException($this->failure(message: $validator->errors()));
    }
}
