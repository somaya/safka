<?php

namespace App\Http\Requests\Api;

use App\Enums\Status;
use App\Rules\ExpireOfferOrDiscount;
use App\Rules\ExpireSave;
use App\Traits\ApiResponses;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class CartRequest extends FormRequest
{
    use ApiResponses;

    public function authorize(): bool
    {
        return true;
    }

    protected function onData(): array
    {
        $data = [
//            'country_id' => ['required', 'integer', Rule::exists('countries', 'id')
//                ->where('status', Status::Active->value)
//            ],

            'shop_id' => ['required', 'integer', Rule::exists('shops', 'id')
                ->where('country_id', request('country_id'))
                ->where('status', Status::Active->value)
            ],
            'model_type' => ['required', 'string', 'in:offer,discount,save,extra'],

            'qty' => ['required', 'integer', 'min:1'],
        ];

        if ($this->model_type == 'offer') {
            $data += [
                'model_id' => [
                    'required',
                    'integer',
                    Rule::exists('offers', 'id')
                        ->where('shop_id', $this->shop_id)
                        ->where('status', Status::Active->value),
                    new ExpireOfferOrDiscount($this->model_type)
                ],
            ];
        }

        if ($this->model_type == 'discount') {
            $data += [
                'model_id' => [
                    'required',
                    'integer',
                    Rule::exists('discounts', 'id')
                        ->where('shop_id', $this->shop_id)
                        ->where('status', Status::Active->value),
                    //new CheckProductSamefurniture($this->furniture_id),
                    new ExpireOfferOrDiscount($this->model_type)
                ]
            ];
        }
        if ($this->model_type == 'extra') {
            $data += [
                'model_id' => [
                    'required',
                    'integer',
                    Rule::exists('extras', 'id')
                        ->where('shop_id', $this->shop_id)
                        ->where('status', Status::Active->value),
                ]
            ];
        }

        if ($this->model_type == 'save') {
            $data += [
                'model_id' => [
                    'required',
                    'integer',
                    Rule::exists('saves', 'id')
                        ->where('shop_id', $this->shop_id)
                        ->where('status', Status::Active->value),
                    new ExpireSave()
                ]
            ];
        }

        return $data;
    }

    public function rules(): array
    {
        if (request()->routeIs('api.cart.add-offer') ||
            request()->routeIs('api.cart.add-discount') ||
            request()->routeIs('api.cart.add-extra') ||
            request()->routeIs('api.cart.add-save')) {
            return $this->onData();
        } else {
            return [];
        }
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException($this->failure(message: $validator->errors()));
    }
}
