<?php

namespace App\Http\Requests\Api;

use App\Traits\ApiResponses;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class FavouriteRequest extends FormRequest
{
    use ApiResponses;

    public function authorize(): bool
    {
        return true;
    }

    protected function onFavourite(): array
    {
        return [
            'favourite_type' => ['required', 'string','in:product,shop,offer,save,discount'],
            'favourite_id' => ['required', 'integer'],
        ];
    }
    public function rules(): array
    {
        if (request()->routeIs('api.add-favourite')) {
            return $this->onFavourite();
        }else {
            return [];
        }
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException($this->failure($validator->errors()));
    }
}
