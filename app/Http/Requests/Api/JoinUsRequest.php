<?php

namespace App\Http\Requests\Api;

use App\Enums\Status;
use App\Traits\ApiResponses;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class JoinUsRequest extends FormRequest
{
    use ApiResponses;

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'shop_name' => ['required', 'string', 'min:2', 'max:255'],
            'branches_count' => ['required', 'integer', 'min:1', 'max:100'],
            'branch_type_id' => ['required', 'array', 'min:1', 'max:4',],
            'branch_type_id.*' => ['required', 'integer',
                Rule::exists('branch_types', 'id')
                    ->where('status', Status::Active->value),
            ],
            'country_id' => ['required', 'integer', Rule::exists('countries', 'id')
                ->where('status', Status::Active->value)],
            'governorate_id' => ['required', 'integer', Rule::exists('governorates', 'id')
                ->where('country_id', $this->country_id)
                ->where('status', Status::Active->value)],
            'region_id' => ['required', 'integer', Rule::exists('regions', 'id')
                ->where('governorate_id', $this->governorate_id)
                ->where('status', Status::Active->value)],
            'lat' => ['required', 'numeric', /*'regex:/^(\+|-)?(?:90(?:(?:\.0{1,8})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,8})?))$/'*/],
            'lng' => ['required', 'numeric', /*'regex:/^(\+|-)?(?:180(?:(?:\.0{1,8})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,8})?))$/'*/],
            'owner_name' => ['required', 'string', 'min:2', 'max:255'],
            'owner_phone' => ['required', 'string', 'regex:/(01)[0-9]{9}/'],
            'owner_email' => ['nullable', 'string', 'email', 'min:2', 'max:255'],
            'time_to_contact' => ['nullable', 'string', 'min:2', 'max:255'],
            'notes' => ['nullable', 'string', 'min:2', 'max:500'],

        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException($this->failure($validator->errors()));
    }
}
