<?php

namespace App\Http\Requests\Api;

use App\Traits\ApiResponses;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class ListingRequest extends FormRequest
{
    use ApiResponses;

    public function authorize(): bool
    {
        return true;
    }

    protected function onData(): array
    {
        return [
            'date' => ['required', 'date', 'date_format:Y-m-d'],
            'item' => ['required', 'string', 'min:2', 'max:255'],

        ];
    }

    protected function onUpdate(): array
    {
        return array_merge($this->onData(), [
            'listing_id' => ['required', 'integer', Rule::exists('listings', 'id')],
        ]);
    }

    public function rules(): array
    {
        if (request()->routeIs('api.listing.store')) {
            return $this->onData();
        } elseif (request()->routeIs('api.listing.update')) {
            return $this->onUpdate();
        } else {
            return [];
        }
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException($this->failure(message: $validator->errors()));
    }
}
