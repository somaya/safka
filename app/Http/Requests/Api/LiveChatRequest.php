<?php

namespace App\Http\Requests\Api;

use App\Enums\Status;
use App\Traits\ApiResponses;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class LiveChatRequest extends FormRequest
{
    use ApiResponses;

    public function authorize(): bool
    {
        return true;
    }

    protected function onData(): array
    {
        return [
            'shop_id' => ['required', 'integer', Rule::exists('shops', 'id')
                ->where('country_id', $this->country_id)
                ->where('status', Status::Active->value)
                ->where('chat_enable', Status::Active->value)
            ],
            'message' => ['required','string','min:1','max:1000']
        ];
    }

    public function rules(): array
    {
        if (request()->routeIs('api.live-chat.store')) {
            return $this->onData();
        }  else {
            return [];
        }
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException($this->failure(message: $validator->errors()));
    }
}
