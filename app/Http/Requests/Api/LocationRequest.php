<?php

namespace App\Http\Requests\Api;

use App\Enums\Status;
use App\Traits\ApiResponses;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class LocationRequest extends FormRequest
{
    use ApiResponses;

    public function authorize(): bool
    {
        return true;
    }

    protected function onData(): array
    {
        return [
            'first_name' => ['nullable', 'string', 'min:2', 'max:255'],
            'last_name' => ['nullable', 'string', 'min:2', 'max:255'],
            'address' => ['required', 'string', 'min:2', 'max:255'],
            'country_code' => ['nullable', 'string', 'min:2'],
//            'phone' => ['required', 'string', 'regex:/(01)[0-9]{9}/'],
            'phone' => ['nullable','phone:country_code','min:10'],
            'lat' => ['required', 'string', 'regex:/^(\+|-)?(?:90(?:(?:\.0{1,8})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,8})?))$/'],
            'lng' => ['required', 'string', 'regex:/^(\+|-)?(?:180(?:(?:\.0{1,8})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,8})?))$/'],
            'country_id' => ['nullable', 'integer', Rule::exists('countries', 'id')
                ->where('status', Status::Active->value)],
            'governorate_id' => ['nullable', 'integer', Rule::exists('governorates', 'id')
                ->where('country_id', $this->country_id)
                ->where('status', Status::Active->value)],
            'region_id' => ['nullable', 'integer', Rule::exists('regions', 'id')
                ->where('governorate_id', $this->governorate_id)
                ->where('status', Status::Active->value)],

            'building_type' => ['nullable', 'string', 'min:2', 'max:255'],
            'street' => ['nullable', 'string', 'min:2', 'max:255'],
            'building_number' => ['nullable', 'integer'],
            'floor_no' => ['nullable', 'integer',],
            'apartment_number' => ['nullable', 'integer'],
            'notes' => ['nullable', 'string', 'min:2', 'max:255'],

        ];
    }

    protected function onUpdate(): array
    {
        return array_merge($this->onData(), [
            'location_id' => ['required', 'integer', Rule::exists('locations', 'id')],
        ]);
    }

    public function rules(): array
    {
        if (request()->routeIs('api.location.store')) {
            return $this->onData();
        } elseif (request()->routeIs('api.location.update')) {
            return $this->onUpdate();
        } else {
            return [];
        }
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException($this->failure(message: $validator->errors()));
    }
}
