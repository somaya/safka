<?php

namespace App\Http\Requests\Api;

use App\Traits\ApiResponses;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class MessageRequest extends FormRequest
{
    use ApiResponses;

    public function authorize(): bool
    {
        return true;
    }

    protected function onData(): array
    {
        return [
            'order_id' => ['required', 'integer', Rule::exists('orders', 'id')
                ->where('customer_id', request()->user()->userable->id)
                ->whereIn('order_status', ['pending', 'receive', 'processing', 'out_for_delivery', 'delivered'])
            ],

            'message' => ['required', 'string', 'min:2', 'max:255'],

        ];
    }

    protected function onTicketMessage(): array
    {
        return [
            'message' => ['required', 'string', 'min:2', 'max:255'],
        ];
    }


    public function rules(): array
    {
        if (request()->routeIs('api.message.store')) {
            return $this->onData();
        } elseif (request()->routeIs('api.ticket.store')) {
            return $this->onTicketMessage();
        } else {
            return [];
        }
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException($this->failure(message: $validator->errors()));
    }
}
