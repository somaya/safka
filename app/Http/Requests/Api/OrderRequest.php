<?php

namespace App\Http\Requests\Api;

use App\Enums\Status;
use App\Rules\ExpireOfferOrDiscount;
use App\Rules\ExpireSave;
use App\Traits\ApiResponses;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class OrderRequest extends FormRequest
{
    use ApiResponses;

    public function authorize(): bool
    {
        return true;
    }

    protected function onData(): array
    {
        return [
//            'country_id' => ['required', 'integer', Rule::exists('countries', 'id')
//                ->where('status', Status::Active->value)
//            ],
            'payment_method' => ['required', 'string'],
        ];
    }

    public function rules(): array
    {
        if (request()->routeIs('api.checkout')) {
            return $this->onData();
        } else {
            return [];
        }
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException($this->failure(message: $validator->errors()));
    }
}
