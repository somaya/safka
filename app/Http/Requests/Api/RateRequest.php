<?php

namespace App\Http\Requests\Api;

use App\Enums\Status;
use App\Models\Discount;
use App\Models\Offer;
use App\Models\Product;
use App\Models\Save;
use App\Rules\IsCountry;
use App\Traits\ApiResponses;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class RateRequest extends FormRequest
{
    use ApiResponses;

    public function authorize(): bool
    {
        return true;
    }

    protected function onRate(): array
    {
        $rules = [
            'rate_type' => ['required', 'string', 'in:shop,product,offer,save,discount'],
//            'country_id' => ['required', 'integer', Rule::exists('countries', 'id')
//                ->where('status', Status::Active->value)],
        ];
        if ($this->rate_type == 'shop') {
            $rules += [
                'rate_id' => ['required', 'integer', Rule::exists('shops', 'id')
                    ->where('country_id',request('country_id'))
                    ->where('status', Status::Active->value)
                ],
            ];
        }
        if ($this->rate_type == 'product') {
            $rules += [
                'rate_id' => ['required', 'integer', new IsCountry(request('country_id'), Product::class),
                    Rule::exists('products', 'id')->where('status', Status::Active->value)
                ],
            ];
        }

        if ($this->rate_type == 'offer') {
            $rules += [
                'rate_id' => ['required', 'integer',
                    new IsCountry(request('country_id'), Offer::class),
                    Rule::exists('offers', 'id')
                        ->where('status', Status::Active->value)
                ],
            ];
        }
        if ($this->rate_type == 'save') {
            $rules += [
                'rate_id' => ['required', 'integer',
                    new IsCountry(request('country_id'), Save::class),
                    Rule::exists('saves', 'id')
                        ->where('status', Status::Active->value)
                ],
            ];
        }
        if ($this->rate_type == 'discounts') {
            $rules += [
                'rate_id' => ['required', 'integer',
                    new IsCountry(request('country_id'), Discount::class),
                    Rule::exists('discounts', 'id')
                        ->where('status', Status::Active->value)
                ],
            ];
        }
        return array_merge($rules, [
            'degree' => ['required', 'integer'],
            'comment' => ['nullable', 'string', 'min:1'],
        ]);
    }

    public function rules(): array
    {
        if (request()->routeIs('api.add-rate')) {
            return $this->onRate();
        } else {
            return [];
        }
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException($this->failure($validator->errors()));
    }
}
