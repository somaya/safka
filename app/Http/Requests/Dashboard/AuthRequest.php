<?php

namespace App\Http\Requests\Dashboard;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AuthRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    protected function onLogin(): array
    {
        return [
            'email' => ['required', 'email', 'min:2', 'max:100', Rule::exists('users', 'email')],
            'password' => ['required', 'string', 'min:2', 'max:100'],
            'remember_me' => ['sometimes', 'nullable']
        ];
    }

    protected function onForgotPassword(): array
    {
        return [
            'email' => ['required', 'email', 'min:2', 'max:100', Rule::exists('users', 'email')],
        ];
    }

    protected function onResetPassword(): array
    {
        return [
            'email' => ['required', 'email', 'string', Rule::exists('users', 'email')],
            'password' => ['required', 'confirmed', 'min:2', 'max:100'],
            'password_confirmation' => ['required', 'same:password', 'min:2', 'max:100'],
        ];
    }

    protected function onUpdateProfile(): array
    {
        return [
            'name' => ['required', 'string', 'min:2', 'max:100'],
            'email' => ['required', 'email', 'min:2', 'max:100',
                Rule::unique('users', 'email')->ignore($this->user()->id, 'id')],
            'phone' => ['required', 'numeric',
                Rule::unique('users', 'phone')->ignore($this->user()->id, 'id')],
            'avatar' => ['sometimes', 'nullable', validationImage()],
        ];
    }


    protected function onChangePassword(): array
    {
        return [
            'old_password' => 'required|min:2|max:100',
            'password' => ['required', 'string', 'min:2', 'max:100', 'confirmed'],
            'password_confirmation' => ['required', 'same:password', 'min:2', 'max:100'],
        ];
    }

    public function rules(): array
    {
        if (request()->routeIs('login')) {
            return $this->onLogin();
        } elseif (request()->routeIs('reset.password')) {
            return $this->onForgotPassword();
        } elseif (request()->routeIs('reset')) {
            return $this->onResetPassword();
        } elseif (request()->routeIs('profile.submit-information')) {
            return $this->onUpdateProfile();
        } elseif (request()->routeIs('profile.change-password')) {
            return $this->onChangePassword();
        } else {
            return [];
        }
    }

    public function attributes(): array
    {
        return [
            'name' => _trans('Full name'),
            'password' => _trans('New password'),
        ];
    }
}
