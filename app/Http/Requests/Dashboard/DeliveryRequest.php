<?php

namespace App\Http\Requests\Dashboard;

use App\Enums\Status;
use App\Rules\IsOwner;
use App\Rules\RegionDeliveryArea;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class DeliveryRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    protected function onData(): array
    {
        $rules = [
            'country_id' => ['required', 'integer', Rule::exists('countries', 'id')
                ->where('status', Status::Active->value)],

            /*areas*/
            'areas' => ['required', 'array', 'min:1'],
            'areas.*.governorate_id' => ['required', 'integer',
                Rule::exists('governorates', 'id')
                    ->where('country_id', $this->country_id)
                    ->where('status', Status::Active->value),
                //new GovernorateDeliveryArea($this->areas)
            ],
            'areas.*.region_id' => ['required', 'array', 'min:1'],
            'areas.*.region_id.*' => ['required', 'integer', new RegionDeliveryArea($this->areas),
            ],
            'areas.*.min_time' => ['required', 'integer', 'min:0', 'max:60'],
            'areas.*.max_time' => ['required', 'integer', 'min:0', 'max:60'],
            'areas.*.delivery_price' => ['required', 'numeric', 'min:0'],
            'areas.*.min_order_price' => ['required', 'numeric', 'min:0'],
            'areas.*.notes' => ['nullable', 'string', 'min:2'],
        ];
        if (Auth::guard('admin')->check()) {
            $rules += [
                'owner_id' => ['required', 'integer', new IsOwner()],
                'shop_id' => ['required', 'integer', Rule::exists('shops', 'id')
                    ->where('status', 1)->where('owner_id', $this->owner_id)],

            ];
        } else {
            $rules += [
                'shop_id' => ['required', 'integer', Rule::exists('shops', 'id')
                    ->where('status', 1)->where('owner_id', getOwner(auth()->user()->id))],

            ];
        }

        return $rules;
    }

    protected function onCreate(): array
    {
        return $this->onData();
    }

    protected function onUpdate(): array
    {
        return array_merge($this->onData(), [
            'id' => ['required', 'integer', Rule::exists('delivery_areas', 'id')],
        ]);
    }


    public function rules(): array
    {
        if (request()->routeIs('delivery-area.store')) {
            return $this->onCreate();
        } elseif (request()->routeIs('delivery-area.update')) {
            return $this->onUpdate();
        } else {
            return [];
        }
    }

    public function attributes()
    {
        return [
            'country_id' => _trans('country name'),
            'areas.*.governorate_id' => _trans('governorate name'),
            'areas.*.region_id' => _trans('region name'),
            'areas.*.min_time' => _trans('min time'),
            'areas.*.max_time' => _trans('max time'),
            'areas.*.delivery_price' => _trans('delivery price'),
            'areas.*.min_order_price' => _trans('min order price'),
            'owner_id' => _trans('Owner name'),
            'shop_id' => _trans('shop name'),
        ];
    }
}
