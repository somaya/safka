<?php

namespace App\Http\Requests\Dashboard;

use App\Enums\OrderStatus;
use App\Enums\UserType;
use App\Models\Owner;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class DriverOrderRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    protected function onData(): array
    {
        $rules = [
            'order_id' => ['required', 'array', 'min:1'],
        ];
        $user = auth()->user();
        if (in_array($user->userable_type, [Owner::class])) {
            $rules += [
                'shop_id' => ['required', 'integer', Rule::exists('shops', 'id')
                    ->where('owner_id', getOwner($user->id))
                ],
                'driver_id' => ['required', 'integer', Rule::exists('drivers', 'id')
                    ->where('owner_id', getOwner($user->id))
                    ->where('shop_id', $this->shop_id)
                ],
                'order_id.*' => ['required', 'integer', Rule::exists('orders', 'id')
                    ->where('shop_id', $this->shop_id)
                    ->whereIn('order_status', [
                        OrderStatus::pending->value,
                        OrderStatus::receive->value,
                        OrderStatus::processing->value,
                    ])
                ],
            ];
        } else {
            $rules += [
                'owner_id' => ['required', 'integer', Rule::exists('owners', 'id')],
                'shop_id' => ['required', 'integer', Rule::exists('shops', 'id')
                    ->where('owner_id', $this->owner_id)
                ],
                'driver_id' => ['required', 'integer', Rule::exists('drivers', 'id')
                    ->where('shop_id', $this->shop_id)
                ],
                'order_id.*' => ['required', 'integer', Rule::exists('orders', 'id')
                    ->where('shop_id', $this->shop_id)
                    ->whereIn('order_status', [
                        OrderStatus::pending->value,
                        OrderStatus::receive->value,
                        OrderStatus::processing->value,
                    ])
                ],
            ];
        }
        return $rules;
    }

    protected function onCreate(): array
    {
        return $this->onData();
    }

    protected function onUpdate(): array
    {
        return $this->onData();
    }


    public function rules(): array
    {
        if (request()->routeIs('delivery-area.store')) {
            return $this->onCreate();
        } elseif (request()->routeIs('admin.driver-order.update')) {
            return $this->onUpdate();
        } else {
            return [];
        }
    }

    public function attributes()
    {
        return [
            'owner_id' => _trans('owner name'),
            'driver_id' => _trans('driver name'),
            'order_id' => _trans('order code'),
            'order_id.*' => _trans('order code'),
            'shop_id' => _trans('Shop name'),
        ];
    }
}
