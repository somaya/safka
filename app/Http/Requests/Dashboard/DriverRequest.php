<?php

namespace App\Http\Requests\Dashboard;

use App\Enums\Status;
use App\Enums\UserType;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class DriverRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    protected function onData(): array
    {
        $rules = [
            /* data driver */
            'name' => ['required', 'string', 'min:2', 'max:255'],
            'avatar' => ['sometimes', 'nullable', validationImage()],
            'address' => ['sometimes', 'nullable', 'string', 'min:2', 'max:255'],
            'birthday' => ['nullable', 'date', 'date_format:Y-m-d', 'before:15 years ago'],
            /* data licenses */
            'license_expiry' => ['nullable', 'date', 'date_format:Y-m-d',
                'after_or_equal:' . Carbon::now()->addMonth()->format('Y-m-d')
            ],
            'license_image' => ['sometimes', 'nullable', validationImage()],

            /* data licenses */
            'vehicle_type_id' => ['required', 'integer', Rule::exists('vehicle_types', 'id')
                ->where('status', Status::Active->value)
            ],
            'vehicle_model' => ['required', 'min:2', 'max:100', 'string'],
            'vehicle_license_photo' => ['sometimes', 'nullable', validationImage()],
            'vehicle_license_expiry' => ['nullable', 'date_format:Y-m-d',
                'after_or_equal:' . Carbon::now()->addMonth()->format('Y-m-d')
            ],
            'vehicle_image' => ['sometimes', 'nullable', validationImage()],
        ];
        $user = auth()->user();
        if (Auth::guard('admin')->check()) {
            $rules += [
                'owner_type' => ['required', 'string', 'min:2', 'max:100', 'in:system,owner'],
                'percentage' => ['required', 'numeric', 'min:0', 'max:99.99',],
            ];
            if ($this->owner_type == 'owner') {
                $rules += [
                    'owner_id' => ['required', 'integer', Rule::exists('owners', 'id')],
                    'shop_id' => ['required', 'integer', Rule::exists('shops', 'id')
                        ->where('owner_id', $this->owner_id)
                    ],
                ];
            }
        } else {
            $rules += [
                'shop_id' => ['required', 'integer', Rule::exists('shops', 'id')
                    ->where('owner_id', getOwner($user->id))
                ],
            ];
        }

        return $rules;
    }

    protected function onCreate(): array
    {
        return array_merge($this->onData(), [
            /* data driver */
            'phone' => ['required', 'numeric', Rule::unique('drivers', 'phone')],
            'notional_id' => ['required', 'numeric', Rule::unique('drivers', 'notional_id')],
            'email' => ['nullable', 'email', 'string', 'min:2', 'max:255', Rule::unique('drivers', 'email')],
            'password' => ['required', 'string', 'min:2', 'max:255', 'confirmed'],
            'password_confirmation' => ['required', 'string', 'min:2', 'max:255', 'same:password'],


            /* data licenses */
            'license_number' => ['required', 'string', 'numeric', Rule::unique('licenses', 'license_number')],

            /* data licenses */
            'plate_number' => ['required', 'min:2', 'max:100', 'string', Rule::unique('vehicles', 'plate_number')],
        ]);
    }

    protected function onUpdate(): array
    {
        return array_merge($this->onData(), [
            'id' => ['required', 'integer', Rule::exists('drivers', 'id')],
            /* data driver */
            'phone' => ['required', 'numeric',
                Rule::unique('drivers', 'phone')->ignore($this->id, 'id')
            ],
            'notional_id' => ['required', 'numeric',
                Rule::unique('drivers', 'notional_id')->ignore($this->id, 'id')
            ],
            'email' => ['nullable', 'email', 'string', 'min:2', 'max:255',
                Rule::unique('drivers', 'email')->ignore($this->id, 'id')
            ],
            /* data licenses */
            'license_number' => ['required', 'string', 'numeric',
                Rule::unique('licenses', 'license_number')->ignore($this->id, 'driver_id')],

            /* data licenses */
            'plate_number' => ['required', 'min:2', 'max:100', 'string',
                Rule::unique('vehicles', 'plate_number')
                    ->ignore($this->id, 'driver_id')
            ],
        ]);
    }

    protected function onChangePassword(): array
    {
        return [
            'id' => ['required', 'integer', Rule::exists('drivers', 'id')],
            'password' => 'required|confirmed',
            'password_confirmation' => 'required|same:password'

        ];
    }

    public function rules(): array
    {
        if (request()->routeIs('driver.store')) {
            return $this->onCreate();
        } elseif (request()->routeIs('driver.update')) {
            return $this->onUpdate();
        } elseif (request()->routeIs('driver.change-password')) {
            return $this->onChangePassword();
        } else {
            return [];
        }
    }

    public function attributes()
    {
        return [
            'notional_id' => _trans('notional number'),
            'owner_type' => _trans('owner type'),
            'owner_id' => _trans('owner name'),
            'shop_id' => _trans('shop name'),
            'percentage' => _trans('Commission delivery'),
            'license_number' => _trans('License number'),
            'license_expiry' => _trans('License expiry'),
            'license_image' => _trans('License image'),
            'vehicle_type_id' => _trans('Vehicle type name'),
            'vehicle_model' => _trans('vehicle model'),
            'plate_number' => _trans('Plate number'),
            'vehicle_license_expiry' => _trans('Vehicle license expiry'),
            'vehicle_license_image' => _trans('License vehicle image'),
            'vehicle_image' => _trans('Vehicle image'),
        ];
    }
}
