<?php

namespace App\Http\Requests\Dashboard;

use App\Enums\OwnerType;
use App\Enums\UserType;
use App\Models\Driver;
use App\Models\Order;
use App\Models\Owner;
use App\Traits\ApiResponses;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class OrderRequest extends FormRequest
{
    use ApiResponses;

    public function authorize(): bool
    {
        return true;
    }

    protected function onData(): array
    {
        $user = auth()->user();

        $rules = [
            'order_id' => ['required', 'integer', Rule::exists('orders', 'id')
                ->whereIn('order_status', ['pending', 'receive', 'processing', 'out_for_delivery', 'delivered'])
            ],
        ];
        if (request()->routeIs('order.cancelled')) {
            $rules += [
                'reason_message' => ['required', 'string', 'min:2', 'max:255']
            ];
        }
        if (request()->routeIs('order.returned')) {
            $rules += [
                'returned_message' => ['required', 'string', 'min:2', 'max:255']
            ];
        }

        return $rules;
    }

    public function rules(): array
    {
        if (request()->routeIs('order.cancelled')) {
            return $this->onData();
        } elseif (request()->routeIs('order.returned')) {
            return $this->onData();
        } elseif (request()->routeIs('order.assign-driver')) {
            return $this->onData();
        } else {
            return [];
        }
    }

    public function attributes()
    {
        return [
            'driver_id' => _trans('driver name'),
            'order_id' => _trans('order code'),
            'returned_message' => _trans('message returned'),
            'reason_message' => _trans('message reason'),
        ];
    }

}
