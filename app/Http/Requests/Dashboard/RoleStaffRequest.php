<?php

namespace App\Http\Requests\Dashboard;

use App\Helpers\CPU\Models;
use App\Rules\OwnerAvailability;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RoleStaffRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    protected function onData(): array
    {
        $rules = [];
        if (request('guard') == 'admin') {
            $rules = [
                'owner_id' => ['required', 'integer', new OwnerAvailability()],
            ];
        } else {
            request()->merge(['owner_id' => getOwner()]);
        }
        return $rules;
    }

    protected function onCreate(): array
    {
        return array_merge(Models::ruleOwner(), [
            'name' => ['required', 'string', 'min:2', 'max:255',
                Rule::unique('roles', 'name')
                    ->where('created_by', $this->owner_id)
                    ->where('guard_name', 'web')
            ],
        ]);
    }

    protected function onUpdate(): array
    {
        return array_merge(Models::ruleOwner(), [
            'id' => ['required', 'integer', Rule::exists('roles', 'id')
                ->where('guard_name', 'web')
            ],
            'name' => ['required', 'string', 'min:2', 'max:255',
                Rule::unique('roles', 'name')
                    ->where('guard_name', 'web')
                    ->ignore($this->id, 'id')
            ],
        ]);

    }

    protected function onDelete(): array
    {
        return array_merge(Models::ruleOwner(), [
            'id' => ['required', 'integer', Rule::exists('roles', 'id')
                ->where('guard_name', 'web')
                ->where('created_by', $this->owner_id)
            ],
        ]);
    }

    public function rules(): array
    {
        if (request()->routeIs('role.store')) {
            return $this->onCreate();
        } elseif (request()->routeIs('role.update')) {
            return $this->onUpdate();
        } elseif (request()->routeIs('role.destroy')) {
            return $this->onDelete();
        } else {
            return [];
        }
    }

    public function attributes()
    {
        return [
            'name' => _trans('Staff Role name'),
            'owner_id' => _trans('Owner name'),
        ];
    }
}
