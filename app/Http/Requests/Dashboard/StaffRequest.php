<?php

namespace App\Http\Requests\Dashboard;

use App\Enums\Status;
use App\Models\Owner;
use App\Models\ShopStaff;
use App\Rules\IsOwner;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StaffRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    protected function onData(): array
    {
        $user = auth()->user();
        $rules = [
            'name' => ['required', 'string', 'min:2', 'max:255'],
            'job_number' => ['required', 'string', 'min:2', 'max:255'],
            'address' => ['required', 'string', 'min:2', 'max:255'],
            'role_id' => ['required', 'integer'],
        ];
        if (request('guard')=='admin') {
            $rules += [
                'owner_id' => ['required', 'integer', new IsOwner()],
                'shop_id' => ['required', 'integer', Rule::exists('shops', 'id')
                    ->where('status', Status::Active->value)
                    ->where('owner_id', $this->owner_id)],

            ];
        } else {
            $rules += [
                'shop_id' => ['required', 'integer', Rule::exists('shops', 'id')
                    ->where('status', Status::Active->value)
                    ->where('owner_id', getOwner())],

            ];
        }

        return $rules;
    }

    protected function onCreate(): array
    {
        return array_merge($this->onData(), [
            'national_number' => ['required', 'string', 'min:2', 'max:255', Rule::unique('shop_staff', 'national_number')],
            'email' => ['required', 'string', 'min:2', 'max:255', 'email', Rule::unique('users', 'email')],
            'phone' => ['required', 'string', Rule::unique('shop_staff', 'phone')],
            'avatar' => ['nullable', validationImage()],
            'password' => ['required', 'string', 'min:2', 'max:255', 'confirmed'],
            'password_confirmation' => ['required', 'string', 'min:2', 'max:255', 'same:password'],
            'work_starting_date' => ['required', 'date_format:Y-m-d', 'after_or_equal:' . formatDate('Y-m-d', now())],

        ]);
    }

    protected function onUpdate(): array
    {
        $rules = [
            'id' => ['required', 'integer', Rule::exists('shop_staff', 'id')
                ],
            'national_number' => ['required', 'string', 'min:2', 'max:255',
                Rule::unique('shop_staff', 'national_number')->ignore($this->id, 'id')],
            'email' => ['required', 'string', 'min:2', 'max:255', 'email',
                Rule::unique('users', 'email')->ignore($this->id, 'userable_id')],
            'phone' => ['required', 'string',
                Rule::unique('shop_staff', 'phone')->ignore($this->id, 'id')],

            'avatar' => ['sometimes', 'nullable', validationImage()],

        ];
        $staff = ShopStaff::query()
            ->where('id', '=', $this->id)->first();
        if ($staff->work_starting_date != $this->work_starting_date) {
            $rules += [
                'work_starting_date' => ['required', 'date_format:Y-m-d', 'after_or_equal:' . formatDate('Y-m-d', now())],
            ];
        } else {
            $rules += [
                'work_starting_date' => ['required', 'date_format:Y-m-d'],
            ];
        }

        return array_merge($this->onData(), $rules);
    }

    protected function onChangePassword(): array
    {
        return [
            'id' => ['required', 'integer', Rule::exists('shop_staff', 'id')],
            'password' => ['required', 'string', 'min:3', 'confirmed'],
            'password_confirmation' => ['required', 'same:password'],

        ];
    }

    public function rules(): array
    {
        if (request()->routeIs('staff.store')) {
            return $this->onCreate();
        } elseif (request()->routeIs('staff.update')) {
            return $this->onUpdate();
        } elseif (request()->routeIs('staff.change-password')) {
            return $this->onChangePassword();
        } else {
            return [];
        }
    }
}
