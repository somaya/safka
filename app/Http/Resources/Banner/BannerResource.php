<?php

namespace App\Http\Resources\Banner;

use App\Http\Resources\Country\CountryResource;
use App\Http\Resources\Governorate\GovernorateResource;
use App\Http\Resources\Region\RegionResource;
use Illuminate\Http\Resources\Json\JsonResource;

class BannerResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'banner_id' => $this->id,
            'content' => $this->translate(locale())?->content,
            'link' => $this->link,
            'image' => getAvatar($this->image),
//            'resource_type' =>$this->resource_type,
//            'banner_type' =>$this->banner_type,
//            'resource_id' =>$this->resource_id,

        ];
    }
}
