<?php

namespace App\Http\Resources\Blog;

use Illuminate\Http\Resources\Json\JsonResource;

class BlogCategoryResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'category_id' => $this->id,
            'image' => getAvatar($this->image),
            'name' => $this->translate(locale())?->name,
            'slug' => $this->translate(locale())?->slug,
            'count' => $this->blogs_count,
        ];
    }
}
