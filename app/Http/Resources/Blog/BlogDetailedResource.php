<?php

namespace App\Http\Resources\Blog;

use App\Http\Resources\File\FileResource;
use Illuminate\Http\Resources\Json\JsonResource;

class BlogDetailedResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'blog_id' => $this->id,
            'title' => $this->translate(locale())?->title,
            'slug' => $this->translate(locale())?->slug,
            'content' => $this->translate(locale())?->content,
            'email' => $this->email,
            'date' => $this->created_at->format('Y-m-d'),
            'category' => BlogCategoryResource::make($this->whenLoaded('category')),
            'image' => getAvatar($this->default_image),
            'images' => FileResource::collection($this->whenLoaded('images')),
            'tags' => TagResource::collection($this->whenLoaded('tags')),

        ];
    }
}
