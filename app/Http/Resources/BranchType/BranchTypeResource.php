<?php

namespace App\Http\Resources\BranchType;

use App\Http\Resources\Shop\ShopResource;
use Illuminate\Http\Resources\Json\JsonResource;

class BranchTypeResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'branch_type_id' => $this->id,
            'image' => getAvatar($this->image),
            'name' => $this->translate(locale())?->name,
            'count' => $this->shops_count,
            'shops' => ShopResource::collection($this->whenLoaded('shops')),

        ];
    }
}
