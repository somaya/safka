<?php

namespace App\Http\Resources\Cart;

use App\Http\Resources\DeliveryArea\AreaResource;
use App\Http\Resources\Location\LocationResource;
use Illuminate\Http\Resources\Json\JsonResource;

class CartResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'cart_id' => $this->id,
            'shop' => CartShopResource::make($this->shop),
            'location' => LocationResource::make($this->location),
            'delivery_area' => AreaResource::make($this->area),
            'delivery_price' => $this->delivery_price,
            'coupon_code' => $this->coupon_code,
            'coupon_price' => $this->coupon_price,
            'total_price' => $this->total_price,
            'amount' => $this->total_price + $this->delivery_price - $this->coupon_price,
            'cart_details' => CartDetailResource::collection($this->cartDetails),
        ];
    }
}
