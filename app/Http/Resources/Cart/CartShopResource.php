<?php

namespace App\Http\Resources\Cart;

use App\Http\Resources\Country\CountryResource;
use App\Http\Resources\Governorate\GovernorateResource;
use App\Http\Resources\Ranking\RankingResource;
use App\Http\Resources\Region\RegionResource;
use Illuminate\Http\Resources\Json\JsonResource;

class CartShopResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'shop_id' => $this->id,
            'name' => $this->translate(locale())?->name,
            'slug' => $this->translate(locale())?->slug,
            'logo' => getAvatar($this->logo),
            'open' => getWeeks($this->workingHours),
            'minimum_order_price' =>$this->minimum_order_price,

        ];
    }
}
