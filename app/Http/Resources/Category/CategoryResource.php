<?php

namespace App\Http\Resources\Category;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'category_id' => $this->id,
            'image' => getAvatar($this->image),
            'name' => $this->translate(locale())?->name,
            'slug' => $this->translate(locale())?->slug,
            'count' => $this->products_count,
            'subcategories' => SubcategoryResource::collection($this->whenLoaded('children')),

        ];
    }
}
