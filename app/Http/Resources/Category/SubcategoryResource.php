<?php

namespace App\Http\Resources\Category;

use Illuminate\Http\Resources\Json\JsonResource;

class SubcategoryResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'category_id' => $this->parent_id,
            'image' => getAvatar($this->image),
            'name' => $this->translate(locale())?->name,
            'slug' => $this->translate(locale())?->slug,
        ];
    }
}
