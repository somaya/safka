<?php

namespace App\Http\Resources\Chat;

use Illuminate\Http\Resources\Json\JsonResource;

class ChatDetailResource extends JsonResource
{

    public function toArray($request): array
    {
        return [
            'direction' => 'left',
            'receiver_id' => $this->modelable_id,
            'seen' => 0,
            'message' => $this->message,
            'conversation_id' => $this->conversation_id,
            'created_at' => $this->created_at,
        ];
    }
}
