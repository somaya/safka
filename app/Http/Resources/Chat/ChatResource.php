<?php

namespace App\Http\Resources\Chat;

use Illuminate\Http\Resources\Json\JsonResource;

class ChatResource extends JsonResource
{

    public function toArray($request): array
    {
        $user = request()->user();
        return [
            $this->mergeWhen($this->modelable_type == $user->userable_type && $this->modelable_id == $user->userable_id, [
                'direction' => 'right',
                'sender_id' => $this->modelable_id,
//                'seen' => 1,
            ]),
            $this->mergeWhen($this->modelable_type != $user->userable_type && $this->modelable_id != $user->userable_id, [
                'direction' => 'left',
                'receiver_id' => $this->modelable_id,
//                'seen' => 0,
            ]),
            'seen' => $this->seen,
            'message' => $this->message,
            'conversation_id' => $this->conversation_id,
            'created_at' => $this->created_at,
        ];
    }
}
