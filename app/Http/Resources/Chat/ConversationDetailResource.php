<?php

namespace App\Http\Resources\Chat;

use Illuminate\Http\Resources\Json\JsonResource;

class ConversationDetailResource extends JsonResource
{

    public function toArray($request): array
    {
        return [
            'conversation_id' => $this->id,
            'chats_un_seen' => $this->chats_un_seen,
            $this->mergeWhen($this->lastMessage, [
                'last_message' => $this->lastMessage ? $this->lastMessage->message : '',
                'date_time' => $this->lastMessage ? $this->lastMessage->created_at : '',
                'format_date' => $this->lastMessage ? $this->lastMessage->created_at->diffForHumans() : '',
                'format_date_1' => $this->lastMessage ? formatDate('d-m-Y h:i A', $this->lastMessage->created_at) : '',
            ]),

        ];
    }
}
