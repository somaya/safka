<?php

namespace App\Http\Resources\Chat;

use App\Http\Resources\Customer\CustomerResource;
use App\Http\Resources\Shop\ShopResource;
use App\Models\Customer;
use App\Models\Shop;
use Illuminate\Http\Resources\Json\JsonResource;

class _ConversationsResource extends JsonResource
{

    public function toArray($request): array
    {
        return [
            'conversation_id' => $this->id,
            $this->mergeWhen($this->senderable_type == Customer::class, [
                'sender_info' => CustomerResource::make($this->senderable_type::query()->find($this->senderable_id)?->user),
            ]),
            $this->mergeWhen($this->receiverable_type == Shop::class, [
                'receiver_info' => ShopResource::make($this->receiverable_type::query()->find($this->receiverable_id)),
            ]),
            'chats_un_seen' => $this->chats_un_seen,
            $this->mergeWhen($this->lastMessage, [
                'last_message' => $this->lastMessage ? $this->lastMessage->message : '',
                'date_time' => $this->lastMessage ? $this->lastMessage->created_at : '',
                'format_date' => $this->lastMessage ? $this->lastMessage->created_at->diffForHumans() : '',
                'format_date_1' => $this->lastMessage ? formatDate('d-m-Y h:i A', $this->lastMessage->created_at) : '',
            ]),

        ];
    }
}
