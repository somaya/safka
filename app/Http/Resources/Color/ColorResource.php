<?php

namespace App\Http\Resources\Color;

use Illuminate\Http\Resources\Json\JsonResource;

class ColorResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'color_id' => $this->id,
            'code' => $this->code,
            'name' => $this->translate(locale())?->name,
        ];
    }
}
