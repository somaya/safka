<?php

namespace App\Http\Resources\Company;

use Illuminate\Http\Resources\Json\JsonResource;

class CompanyResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'company_id' => $this->id,
            'link' => $this->limk,
            'logo' => getAvatar($this->logo),
        ];
    }
}
