<?php

namespace App\Http\Resources\Country;

use Illuminate\Http\Resources\Json\JsonResource;

class CountryResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'country_id' => $this->id,
            'country_code' => $this->code,
            'flag' => getAvatar($this->icon),
            'name' => $this->translate(locale())?->name,
            'slug' => $this->translate(locale())?->slug,
        ];
    }
}
