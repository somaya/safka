<?php

namespace App\Http\Resources\Customer;

use App\Http\Resources\BranchType\BranchTypeResource;
use App\Http\Resources\Country\CountryResource;
use App\Http\Resources\Governorate\GovernorateResource;
use App\Http\Resources\Region\RegionResource;
use App\Models\AdminMessage;
use App\Models\Chat;
use App\Models\Message;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Route;

class CustomerResource extends JsonResource
{

    public function toArray($request)
    {
        $routes = [
            'api.register',
            'api.login',
            'api.check-social',
            'api.login-social',
        ];
        $orderMessage = Message::query()->whereHas('conversion.order', function ($query) {
            $query->where('customer_id', $this->userable->id);
        })->whereRead(0)->count();

        $supportMessage = AdminMessage::query()->whereHas('conversion', function ($query) {
            $query->where('sender_id', $this->userable->id);
        })->whereRead(0)->count();

        $liveChats = Chat::query()
            ->whereHas('conversation', function (Builder $builder) {
                $builder->where([
                    'senderable_type' => $this->userable_type,
                    'senderable_id' => $this->userable_id,
                ]);
            })
            ->where('seen', '=', 0)
            ->count();
        if ($this->userable?->gender == 'female') {
            $image = asset('assets/images/female.jpeg');
        } else {
            $image = asset('assets/images/male.jpeg');
        }
        return [
            //'id' => $this->id,
            'name' => $this->name,
            'facility_name' => $this->userable->facility_name,
            'email' => $this->email,
            'phone' => $this->userable->phone,
            'avatar' => $this->avatar ? getAvatar($this->avatar) : $image,
            'country' => CountryResource::make($this->whenLoaded('country')),
            'branch_type' => $this->userable->branch_type,
            'governorate' => GovernorateResource::make($this->whenLoaded('governorate')),
            'region' => RegionResource::make($this->whenLoaded('region')),
            'address' => $this->userable->address,
            'lat' => $this->userable->lat,
            'lng' => $this->userable->lng,
            'gender' => $this->userable->gender,
            'birthday' => $this->userable->birthday,
            'age' => $this->userable->age,
            'id' => $this->userable->id,
            'user_id' => $this->id,
            $this->mergeWhen(in_array(Route::currentRouteName(), $routes), [
                'token' => 'Bearer ' . $this->token,
            ]),
            'total_of_unread_orders_chats' => $orderMessage,
            'total_of_unread_support_chats' => $supportMessage,
            'total_of_unread_live_chats' => $liveChats,
            'all_unread' => $orderMessage + $supportMessage + $liveChats,
        ];
    }
}
