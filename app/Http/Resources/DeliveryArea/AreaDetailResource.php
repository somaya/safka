<?php

namespace App\Http\Resources\DeliveryArea;

use App\Http\Resources\Country\CountryResource;
use App\Http\Resources\Governorate\GovernorateResource;
use App\Http\Resources\Region\RegionResource;
use Illuminate\Http\Resources\Json\JsonResource;

class AreaDetailResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'delivery_area_id' => $this->delivery_area_id,
            'area_id' => $this->id,
            'governorate' => GovernorateResource::make($this->governorate),
            'region' => RegionResource::collection($this->regions),
            'average_time' => $this->avgTime,
            'min_time' => $this->min_time,
            'max_time' => $this->max_time,
            'delivery_price' => $this->delivery_price,
            'min_order_price' => $this->min_order_price,
        ];
    }
}
