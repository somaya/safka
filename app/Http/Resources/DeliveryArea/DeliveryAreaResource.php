<?php

namespace App\Http\Resources\DeliveryArea;

use App\Http\Resources\Country\CountryResource;
use Illuminate\Http\Resources\Json\JsonResource;

class DeliveryAreaResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'delivery_area_id' => $this->id,
            'country' => CountryResource::make($this->country),
            //'areas' => AreaResource::collection($this->areas),
        ];
    }
}
