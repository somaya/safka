<?php

namespace App\Http\Resources\Discount;

use App\Http\Resources\File\FileResource;
use App\Http\Resources\Shop\ShopResource;
use App\Models\Discount;
use Illuminate\Http\Resources\Json\JsonResource;

class DiscountDetailsResource extends JsonResource
{

    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'discount_id' => $this->id,
            'price_before' => $this->price_before,
            'price_after' => $this->price_after,
            'percent' => $this->percent,
            'product_name' => $this->product->translate(locale())?->name,
            'product_description' => $this->product->translate(locale())?->description,
            'images' => FileResource::collection($this->product->images),
            'shop' => ShopResource::make($this->whenLoaded('shop')),
            'rate' => $this->getRate(),
            'rate_count' => $this->rates_count,
            'model_type' => 'discount',
            'qty_cart' => getCartQty(Discount::class, $this->id,auth('sanctum')->user()),
        ];
    }
}
