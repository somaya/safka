<?php

namespace App\Http\Resources\Driver;

use App\Enums\OwnerType;
use App\Http\Resources\Cart\CartShopResource;
use App\Http\Resources\Country\CountryResource;
use App\Http\Resources\Governorate\GovernorateResource;
use App\Http\Resources\Owner\OwnerResource;
use App\Http\Resources\Region\RegionResource;
use Illuminate\Http\Resources\Json\JsonResource;

class DriverResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'driver_id' => $this->id,
            'name' => $this->name,
            'phone' => $this->phone,
            'email' => $this->email,
            'notional_id' => $this->notional_id,
            'birthday' => $this->birthday ? formatDate('d-m-Y', $this->birthday) : '',
            'age' => $this->age,
            'avatar' => getAvatar($this->avatar),
            'country' => CountryResource::make($this->whenLoaded('country')),
            'governorate' => GovernorateResource::make($this->whenLoaded('governorate')),
            'region' => RegionResource::make($this->whenLoaded('region')),
            'address' => $this->address,
            $this->mergeWhen(request()->routeIs('api-driver.login'), [
                'token' => 'Bearer ' . $this->token,
            ]),
            $this->mergeWhen($this->owner_type == OwnerType::System->value, [
                'percentage' => $this->percentage,
            ]),
            $this->mergeWhen($this->owner_type == OwnerType::Owner->value, [
                'owner' => OwnerResource::make($this->owner),
                'shop' => CartShopResource::make($this->shop),
            ]),
        ];
    }
}
