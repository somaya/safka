<?php

namespace App\Http\Resources\Favourite;

use App\Http\Resources\File\FileResource;
use App\Http\Resources\Size\SizeResource;
use App\Models\Product;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductFavouriteResource extends JsonResource
{

    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'favourite_id' => $this->id,
            'name' => $this->favourite->translate(locale())?->name,
            'product_slug' => $this->favourite->translate(locale())?->slug,
            'product_id' => $this->favourite->id,
            'description' => $this->favourite->translate(locale())?->description,
            'price' => $this->favourite->price,
            'maximum_order_number' => $this->favourite->maximum_order_number,
            'qty_cart' => getCartQty(Product::class, $this->favourite->id,auth('sanctum')->user()),
            'video' => $this->favourite->video,
            'sizes' => SizeResource::collection($this->favourite->sizes),
            'images' => FileResource::collection($this->favourite->images),
            'shop_id' => $this->favourite->shop->id,
            'shop_name' => $this->favourite->shop->translate(locale())?->name,
            'shop_slug' => $this->favourite->shop->translate(locale())?->slug,
            'shop_logo' => getAvatar($this->favourite->shop->logo),
            'shop_minimum_order_price' => $this->favourite->shop->minimum_order_price,
            'rate'=>$this->favourite->getRate(),
            'rate_count'=>$this->favourite->rates_count,
        ];
    }
}
