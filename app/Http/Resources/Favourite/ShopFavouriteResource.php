<?php

namespace App\Http\Resources\Favourite;

use Illuminate\Http\Resources\Json\JsonResource;

class ShopFavouriteResource extends JsonResource
{

    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'favourite_id' => $this->id,
            'name' => $this->favourite->name,
            'shop_id' => $this->favourite?->id,
            'shop_logo' => getAvatar($this->favourite->logo),
            'shop_slug' => $this->favourite->translate(locale())?->slug,
            'shop_address'=>$this->favourite->address,
            'shop_description'=>$this->favourite->translate(locale())?->description,
            'shop_lat'=>$this->favourite->lat,
            'shop_lng'=>$this->favourite->lng,
            'open' => getWeeks($this->favourite->workingHours),
            'rate'=>$this->favourite->getRate(),
            'rate_count'=>$this->favourite->rates_count,
        ];
    }
}
