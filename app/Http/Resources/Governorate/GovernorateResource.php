<?php

namespace App\Http\Resources\Governorate;

use Illuminate\Http\Resources\Json\JsonResource;

class GovernorateResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'governorate_id' => $this->id,
            'name' => $this->translate(locale())?->name,
            'slug' => $this->translate(locale())?->slug,
        ];
    }
}
