<?php

namespace App\Http\Resources\Home;

use Illuminate\Http\Resources\Json\JsonResource;

class LanguageResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'language_id' => $this->id,
            'code' => $this->code,
            'flag' => asset('flags/'.pathinfo($this->flag)['filename'].'.png')
        ];
    }
}
