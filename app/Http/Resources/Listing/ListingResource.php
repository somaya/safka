<?php

namespace App\Http\Resources\Listing;

use Illuminate\Http\Resources\Json\JsonResource;

class ListingResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'item' => $this->item,
            'date' => $this->date,
            'buying' => $this->buying,

        ];
    }
}
