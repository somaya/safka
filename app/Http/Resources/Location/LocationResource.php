<?php

namespace App\Http\Resources\Location;

use App\Http\Resources\Country\CountryResource;
use App\Http\Resources\Governorate\GovernorateResource;
use App\Http\Resources\Region\RegionResource;
use Illuminate\Http\Resources\Json\JsonResource;

class LocationResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'location_id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'full_name' => $this->first_name . ' ' . $this->last_name,
            'phone' => $this->phone,
            'phone_verified' => $this->phone_verified,
            'address' => $this->address,
            'lat' => $this->lat,
            'lng' => $this->lng,
            'country' => CountryResource::make($this->whenLoaded('country')),
            'governorate' => GovernorateResource::make($this->whenLoaded('governorate')),
            'region' => RegionResource::make($this->whenLoaded('region')),
            'building_type' => $this->building_type,
            'street' => $this->street,
            'building_number' => $this->building_number,
            'floor_no' => $this->floor_no,
            'apartment_number' => $this->apartment_number,
            'notes' => $this->notes,
        ];
    }
}
