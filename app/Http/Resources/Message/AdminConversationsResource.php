<?php

namespace App\Http\Resources\Message;

use App\Http\Resources\Customer\CustomerResource;
use App\Models\AdminMessage;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

class AdminConversationsResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'admin_conversation_id' => $this->id,
            'sender_id' => $this->sender_id,
            'receiver_id' => $this->receiver_id,
            'customer' => CustomerResource::make($this->customer->user),
            'last_time_message' => $this->last_time_message,
            'last_time' => formatDate('d-m-Y', $this->last_time_message),
            'last_message' => Str::limit($this->lastMessage->body, 10),
            'count_unread' => AdminMessage::query()->where([
                'admin_conversion_id' => $this->id,
                'read' => 0,
            ])->count(),
        ];
    }
}
