<?php

namespace App\Http\Resources\Message;

use App\Http\Resources\Customer\CustomerResource;
use App\Models\Message;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

class ConversationsResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'conversation_id' => $this->id,
            'sender_id' => $this->sender_id,
            'receiver_id' => $this->receiver_id,
            'order' => $this->order,
            'customer' => CustomerResource::make($this->customer->user),
            'last_time_message' => $this->last_time_message,
            'last_time' => formatDate('d-m-Y',$this->last_time_message),
            'last_message' => Str::limit($this->lastMessage->body, 10),
            'count_unread' => Message::query()->where([
                'conversion_id' => $this->id,
                'read' => 0,
            ])->count(),
        ];
    }
}
