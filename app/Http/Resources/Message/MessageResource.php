<?php

namespace App\Http\Resources\Message;

use App\Models\Message;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

class MessageResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'sender_id' => $this->sender_id,
            'receiver_id' => $this->receiver_id,
            'user_id' => $this->user_id,
            'read' => $this->read,
            'message' => $this->body,
            'date' => formatDate('d-m-Y', $this->created_at),
            'time' => formatDate('H:i A', $this->created_at),
            'format_date' => $this->created_at->diffForHumans(),
            'last_message' => Str::limit($this->body, 10),
            'count_unread' => Message::query()->where([
                'conversion_id' => $this->conversion_id,
                'read' => 0,
            ])->count(),
        ];
    }
}
