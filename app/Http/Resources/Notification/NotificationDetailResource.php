<?php

namespace App\Http\Resources\Notification;

use App\Http\Resources\Customer\CustomerResource;
use App\Models\Customer;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class NotificationDetailResource extends JsonResource
{

    public function toArray($request): array
    {
        $data = [];
        if ($this->type == 'coupon') {
            $data += [
                'code' => $this->data['coupon_code'],
                'title' => $this->data['title'],
                'description' => $this->data['description'],
            ];
        }
        if ($this->type == 'global') {
            $data += [
                'code' => $this->data['code'],
                'title' => $this->data['title'],
                'description' => $this->data['description'],
            ];
        }
        if ($this->type == 'shop') {
            $data += [
                'code' => $this->data['code'],
                'title' => $this->data['title'],
                'description' => $this->data['description'],
            ];
        }
        return [
            'notify_type' => $this->type,
            'read_at' => $this->read_at,
            'read_date' => $this->read_at ? formatDate('d-m-Y', $this->read_at) : null,
            'read_time' => $this->read_at ? formatDate('H:i A', $this->read_time) : null,
            'created_at' => $this->created_at,
            'data' => $data,
        ];
    }
}
