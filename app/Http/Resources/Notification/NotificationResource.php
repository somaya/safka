<?php

namespace App\Http\Resources\Notification;

use App\Http\Resources\Customer\CustomerResource;
use App\Models\Admin;
use App\Models\Customer;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class NotificationResource extends JsonResource
{

    public function toArray($request)
    {
        $data = [];
        if ($this->type == 'order' || $this->type == 'account_order') {
            $data += [
                'order_id' => $this->data['order_id'],
                'order_route' => route('order.show', [$this->data['order_id'], $this->id]),
                'order_code' => $this->data['order_code'],
                'shop_id' => $this->data['shop_id'],
            ];
        }
        if ($this->type == 'join_us') {
            $data += [
                'join_us_id' => $this->data['join_us_id'],
                'owner_name' => $this->data['owner_name'],
                'owner_avatar' => $this->data['owner_avatar'],
                'join_us_route' => route('admin.join-us.show', $this->data['join_us_id']),
            ];
        }
        if ($this->type == 'contact-us') {
            $data += [
                'contact_us_id' => $this->data['contact_us_id'],
                'contact_us_route' => route('admin.contact-us.edit', $this->data['contact_us_id']),
            ];
        }
        if ($this->type == 'message') {
            $data += [
                'order_id' => $this->data['order_id'],
                'message_route' => route('owner.chat.index'),
                'message_id' => $this->data['message_id'],
                'shop_id' => $this->data['shop_id'],
            ];
        }
        if ($this->type == 'ticket') {
            $data += [
                'customer_id' => $this->data['customer_id'],
                'customer_route' => route('admin.customer.show',$this->data['customer_id']),
            ];
        }
        if ($this->type == 'invoice') {
            $data += [
                'payment_id' => $this->data['payment_id'],
                'invoice_route' => route('owner.payments') . '/?payment=' . $this->data['payment_id'],
                'admin_name' => Admin::first()->name,
            ];
        }
        $user = User::first();
        return [
            'id' => $this->id,
            'notification_type' => $this->type,
            //'senderable' => $this->senderable,
            //'receiverable' => $this->receiverable,
            'customer' => $this->senderable_type == Customer::class ? CustomerResource::make($this->customer->user) : [
                'name' => $user->name,
                'avatar' => getAvatar($user->avatar),
            ],
            'read_at' => $this->read_at,
            'read_date' => formatDate('d-m-Y', $this->read_at),
            'read_time' => formatDate('H:i A', $this->read_time),
            'created_at' => $this->created_at->format('d-m-Y'),
            'data' => $data,
        ];
    }
}
