<?php

namespace App\Http\Resources\Offer;


use App\Http\Resources\File\FileResource;
use App\Http\Resources\Shop\ShopResource;
use App\Models\Offer;
use App\Models\Product;
use Illuminate\Http\Resources\Json\JsonResource;

class OfferResource extends JsonResource
{

    public function toArray($request)
    {
        $products = [];
        foreach ($this->products as $product) {
            $prod = Product::findOrFail($product['product_id']);
            $product['product_id'] = (integer)$prod->id;
            $product['product'] = $prod->translate(locale())?->name;
            $product['icon'] = getAvatar($prod->icon);
            $product['quantity'] = (integer)$product['quantity'];
            array_push($products, $product);
        }

        return [
            'id' => $this->id,
            'offer_id' => $this->id,
            'name' => $this->translate(locale())?->name,
            'slug' => $this->translate(locale())?->slug,
            'price' => $this->price,
            'video' => $this->video,
            'images' => FileResource::collection($this->whenLoaded('images')),
            'shop' => ShopResource::make($this->whenLoaded('shop')),
            'products' => $products,
            'rate' => $this->getRate(),
            'rate_count' => $this->rates_count,
            'model_type' => 'offer',
            'qty_cart' => getCartQty(Offer::class, $this->id,auth('sanctum')->user()),
        ];
    }
}
