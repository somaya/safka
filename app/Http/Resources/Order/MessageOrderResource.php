<?php

namespace App\Http\Resources\Order;

use App\Http\Resources\Customer\CustomerResource;
use Illuminate\Http\Resources\Json\JsonResource;

class MessageOrderResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'order_id' => $this->id,
            'order_code' => $this->code,
            'location' => $this->location_info,
            'customer_name' => $this->customer->user->name,
            'amount' => $this->total_price + $this->delivery_price - $this->coupon_price,
            'date' => formatDate('d-m-Y', $this->created_at),
            'time' => formatDate('h:i', $this->created_at),
            'format_date' => $this->created_at->diffForHumans(),
        ];
    }
}
