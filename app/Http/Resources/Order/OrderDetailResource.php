<?php

namespace App\Http\Resources\Order;

use App\Http\Resources\File\FileResource;
use App\Models\Discount;
use App\Models\Extra;
use App\Models\Offer;
use App\Models\Product;
use App\Models\Save;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderDetailResource extends JsonResource
{

    public function products($products): array
    {
        $data = [];
        foreach ($products as $product) {
            $pro = Product::query()->with(['images'])->find($product['product_id']);
            $data[] = [
                'product_name' => $pro->translate(locale())?->name,
                'product_slug' => $pro->translate(locale())?->slug,
                'product_qty' => $product['quantity'],
                'product_icon' => getAvatar($pro->icon),
                'product_images' => FileResource::collection($pro->images),
            ];
        }
        return $data;
    }


    public function toArray($request)
    {
        $models = [
            Product::class => 'product',
            Offer::class => 'offer',
            Discount::class => 'discount',
            Save::class => 'save',
        ];
        $data = [
            'order_detail_id' => $this->id,
            'model_id' => $this->modelable_id,
            'model_type' => $models[$this->modelable_type],
            'qty' => $this->qty,
            'price' => $this->price,
        ];
        if ($this->modelable_type == Product::class) {
            if (!is_null($this->size_id)) {
                $data += [
                    'size_id' => $this->size->id,
                    'size_name' => $this->size->translate(locale())?->name,
                    'size_price' => $this->size->price,
                ];
            }
            if (!is_null($this->color_id)) {
                $data += [
                    'color_id' => $this->color->id,
                    'color_name' => $this->color->translate(locale())?->name,
                    'color_code' => $this->color->price,
                ];
            }
            $data += [
                'product_id' => $this->product->id,
                'product_name' => $this->product->translate(locale())?->name,
                'product_slug' => $this->product->translate(locale())?->slug,
                //'product_price' => $this->product->price,
                'product_icon' => getAvatar($this->product->icon),
                'product_images' => FileResource::collection($this->product->images),

            ];

        }

        if ($this->modelable_type == Offer::class) {
            $data += [
                'product_id' => $this->offer->id,
                'product_name' => $this->offer->translate(locale())?->name,
                'product_slug' => $this->offer->translate(locale())?->slug,
                'product_icon' => getAvatar($this->offer->icon),
                'product_images' => FileResource::collection($this->offer->images),
                'products' => $this->products($this->products),
            ];
        }
        if ($this->modelable_type == Discount::class) {
            $data += [
                'product_id' => $this->discount->product->id,
                'product_name' => $this->discount->product->translate(locale())?->name,
                'product_slug' => $this->discount->product->translate(locale())?->slug,
                'product_icon' => getAvatar($this->discount->product->icon),
                'product_images' => FileResource::collection($this->discount->product->images),

                //'price_before' => $this->discount->price_before,
                //'price_after' => $this->discount->price_after,
            ];
        }

        if ($this->modelable_type == Save::class) {
            $data += [
                'product_id' => $this->saveProduct->id,
                'product_name' => $this->saveProduct->translate(locale())?->name,
                'product_slug' => $this->saveProduct->translate(locale())?->slug,
                'product_icon' => getAvatar($this->saveProduct->icon),
                'product_images' => FileResource::collection($this->saveProduct->images),
                'products' => $this->products($this->products),

            ];
        }

        return $data;
    }
}
