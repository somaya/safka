<?php

namespace App\Http\Resources\Order;

use App\Http\Resources\Cart\CartFurnitureResource;
use App\Http\Resources\Cart\CartShopResource;
use App\Http\Resources\DeliveryArea\AreaResource;
use App\Http\Resources\ServiceType\ServiceTypeResource;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'order_id' => $this->id,
            'order_code' => $this->code,
            'shop' => CartShopResource::make($this->shop),
            'delivery_area' => AreaResource::make($this->area),
            'location' => $this->location_info,
            'delivery_price' => $this->delivery_price,
            'order_status' =>  _trans($this->order_status,locale()),
            'status_pay' => _trans($this->status_pay,locale()),
            'payment_method' => _trans($this->payment_method,locale()),
            'coupon_code' => $this->coupon_code,
            'coupon_price' => $this->coupon_price,
            'total_price' => $this->total_price,
            'amount' => $this->total_price + $this->delivery_price - $this->coupon_price,
            'date' => formatDate('d-m-Y', $this->created_at),
            'time' => formatDate('h:i', $this->created_at),
            'format_date' => $this->created_at->diffForHumans(),
            'day' => Carbon::create($this->created_at->format('l'))->locale(locale())->dayName,
            'special_order' => $this->special_order,
            'order_details' => OrderDetailResource::collection($this->orderDetails),
        ];
    }
}
