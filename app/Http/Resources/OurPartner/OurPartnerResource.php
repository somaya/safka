<?php

namespace App\Http\Resources\OurPartner;

use App\Http\Resources\Country\CountryResource;
use App\Http\Resources\Governorate\GovernorateResource;
use App\Http\Resources\Region\RegionResource;
use Illuminate\Http\Resources\Json\JsonResource;

class OurPartnerResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'link' => $this->link,
            'logo' => getAvatar($this->logo),
        ];
    }
}
