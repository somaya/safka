<?php

namespace App\Http\Resources\Product;

use App\Http\Resources\Category\CategoryResource;
use App\Http\Resources\Category\SubcategoryResource;
use App\Http\Resources\Color\ColorResource;
use App\Http\Resources\File\FileResource;
use App\Http\Resources\Shop\ShopResource;
use App\Http\Resources\Size\SizeResource;
use App\Models\Product;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'product_id' => $this->id,
            'name' => $this->translate(locale())?->name,
            'slug' => $this->translate(locale())?->slug,
            'description' => $this->translate(locale())?->description,
            'price' => $this->price,
            'maximum_order_number' => $this->maximum_order_number,
            'video' => $this->video,
            'icon' => getAvatar($this->icon),
            'images' => FileResource::collection($this->whenLoaded('images')),
            'sizes' => SizeResource::collection($this->whenLoaded('sizes')),
            'colors' => ColorResource::collection($this->whenLoaded('colors')),
            'category' => CategoryResource::make($this->whenLoaded('category')),
            'subCategory' => SubcategoryResource::make($this->whenLoaded('Subcategory')),
            'shop' => ShopResource::make($this->whenLoaded('shop')),
            'is_fav' => $this->favouritedBy(auth('sanctum')->user()),
            'rate' => $this->getRate(),
            'rate_count' => $this->rates_count,
            'model_type' => 'product',
            'qty_cart' => getCartQty(Product::class, $this->id,auth('sanctum')->user()),
        ];
    }
}
