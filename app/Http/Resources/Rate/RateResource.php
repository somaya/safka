<?php

namespace App\Http\Resources\Rate;

use Illuminate\Http\Resources\Json\JsonResource;

class RateResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'rate_id' => $this->id,
            'degree' => $this->degree,
            'comment' => $this->comment,
            'date' => $this->created_at->format('Y-m-d'),
            'user' => $this->user->name,
            'avatar' => getAvatar($this->user->avatar),

        ];
    }
}
