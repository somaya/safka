<?php

namespace App\Http\Resources\Save;

use App\Http\Resources\File\FileResource;
use App\Http\Resources\Shop\ShopResource;
use App\Models\Product;
use App\Models\Save;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class SaveDetailsResource extends JsonResource
{

    public function toArray($request)
    {
        $products = [];
        foreach ($this->products as $product) {
            $prod = Product::findOrFail($product['product_id']);
            $product['product_id'] = (integer)$prod->id;
            $product['product'] = $prod->translate(locale())?->name;
            $product['icon'] = getAvatar($prod->icon);
            $product['quantity'] = (integer)$product['quantity'];
            array_push($products, $product);
        }
        $date = now();
        $day = $date->diffInDays(Carbon::parse($this->start));
        $hours = Carbon::parse($this->from)->diffInHours(Carbon::parse($this->to));
        $minutes = Carbon::parse($this->from)->diffInMinutes(Carbon::parse($this->to));
        return [
            'id' => $this->id,
            'save_id' => $this->id,
            'name' => $this->translate(locale())?->name,
            'slug' => $this->translate(locale())?->slug,
            'price' => $this->price,
            'video' => $this->video,
            'start' => $this->start,
            'end' => $this->end,
            'diff_day' => $day,
            'from' => $this->from,
            'to' => $this->to,
            'hours' => $hours,
            'minutes' => $minutes,
            'images' => FileResource::collection($this->whenLoaded('images')),
            'shop' => ShopResource::make($this->whenLoaded('shop')),
            'products' => $products,
            'rate' => $this->getRate(),
            'rate_count' => $this->rates_count,
            'model_type' => 'save',
            'qty_cart' => getCartQty(Save::class, $this->id,auth('sanctum')->user()),
        ];
    }
}
