<?php

namespace App\Http\Resources\Service;

use Illuminate\Http\Resources\Json\JsonResource;

class ServiceResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'name' => $this->translate(locale())?->name,
            'slug' => $this->translate(locale())?->slug,
            'description' => $this->translate(locale())?->description,
            'image' => getAvatar($this->image),
        ];
    }
}
