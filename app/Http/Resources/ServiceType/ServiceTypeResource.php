<?php

namespace App\Http\Resources\ServiceType;

use Illuminate\Http\Resources\Json\JsonResource;

class ServiceTypeResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'service_type_id' => $this->id,
            'name' => $this->translate(locale())?->name,
        ];
    }
}
