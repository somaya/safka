<?php

namespace App\Http\Resources\Shop;

use App\Http\Resources\Country\CountryResource;
use App\Http\Resources\Governorate\GovernorateResource;
use App\Http\Resources\Region\RegionResource;
use Illuminate\Http\Resources\Json\JsonResource;

class BranchResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'branch_id' => $this->id,
            'name' => $this->translate(locale())?->name,
            'slug' => $this->translate(locale())?->slug,
            'country' => CountryResource::make($this->whenLoaded('country')),
            'governorate' => GovernorateResource::make($this->whenLoaded('governorate')),
            'region' => RegionResource::make($this->whenLoaded('region')),
            'address' => $this->address,
            'minimum_order_price' => $this->minimum_order_price,
            'phone' => $this->phone,
            'email' => $this->email,
            'logo' => getAvatar($this->logo),
            'times_of_week' => $this->workingHours,
            'chat_enable' => (bool)$this->chat_enable,
        ];
    }
}
