<?php

namespace App\Http\Resources\Shop;

use App\Http\Resources\Product\ProductResource;
use App\Http\Resources\Category\SubcategoryResource;
use Illuminate\Http\Resources\Json\JsonResource;

class MenuResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'menu_id' => $this->id,
            'code' => $this->code,
            'image' => getAvatar($this->image),
            'name' => $this->translate(locale())?->name,
            'subcategories' => SubcategoryResource::collection($this->whenLoaded('children')),
            'products' => ProductResource::collection($this->whenLoaded('products'))
        ];
    }
}
