<?php

namespace App\Http\Resources\Shop;

use App\Helpers\Setting\Utility;
use App\Http\Resources\BranchType\BranchTypeResource;
use App\Http\Resources\Country\CountryResource;
use App\Http\Resources\Governorate\GovernorateResource;
use App\Http\Resources\Region\RegionResource;
use App\Http\Resources\ServiceType\ServiceTypeResource;
use App\Models\Cart;
use App\Models\WeekDay;
use Illuminate\Http\Resources\Json\JsonResource;

class ShopDetailResource extends JsonResource
{
    private function weeks($weeks)
    {
        $data = [];
        foreach ($weeks as $week) {
            $data [] = [
                'to' =>$week['to'],
                'day' => WeekDay::query()->find($week['day_id'])->translate(locale())?->name,
                'from' => $week['from'],
            ];
        }
        return $data;
    }

    public function toArray($request)
    {
        if (auth('sanctum')->check()) {
            $cart = Cart::query()->whereCustomerId(auth('sanctum')->user()->userable->id)->first();
        }
        return [
            'id' => $this->id,
            'shop_id' => $this->id,
            'favourited_count' => $this->favourited_count(auth('sanctum')->user()),
            'name' => $this->translate(locale())?->name,
            'slug' => $this->translate(locale())?->slug,
            'description' => $this->translate(locale())?->description,
            'logo' => getAvatar($this->logo),
            'image' => getAvatar($this->image),
            'phone' => $this->phone,
            'email' => $this->email,
            'country' => CountryResource::make($this->whenLoaded('country')),
            'governorate' => GovernorateResource::make($this->whenLoaded('governorate')),
            'region' => RegionResource::make($this->whenLoaded('region')),
            'address' => $this->address,
            'minimum_order_price' => $this->minimum_order_price,
            'branch_types' => BranchTypeResource::collection($this->whenLoaded('branchTypes')),
            'times_of_week' => $this->weeks($this->workingHours),
            'is_fav' => $this->favouritedBy(auth('sanctum')->user()),
            'is_rate' => $this->ratedBy(auth('sanctum')->user()),
            'rate' => $this->getRate(),
            'rate_count' => $this->rates_count,
            'open' => getWeeks($this->workingHours),
            'link_google_play' => Utility::getValByName('link_google_play'),
            'link_apple_store' => Utility::getValByName('link_apple_store'),
            'qr_image' => getAvatar($this->qr_code),
            'lat' => $this->lat,
            'lng' => $this->lng,
            'chat_enable' => (bool)$this->chat_enable,
            'count_items_cart' => auth('sanctum')->check() ? ($cart ? $cart->cart_details_count : 0) : 0,
            'total_amount_cart' => auth('sanctum')->check() ? ($cart ? $cart->total_price : 0) : 0,

        ];
    }
}
