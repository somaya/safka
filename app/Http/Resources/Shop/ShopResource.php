<?php

namespace App\Http\Resources\Shop;

use App\Helpers\Setting\Utility;
use App\Http\Resources\BranchType\BranchTypeResource;
use App\Http\Resources\Chat\ConversationDetailResource;
use App\Http\Resources\Country\CountryResource;
use App\Http\Resources\Governorate\GovernorateResource;
use App\Http\Resources\Region\RegionResource;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Route;

class ShopResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'shop_id' => $this->id,
            'distance' => $this->distance,
            'branch_types' => BranchTypeResource::collection($this->whenLoaded('branchTypes')),
            'name' => $this->translate(locale())?->name,
            'slug' => $this->translate(locale())?->slug,
            'description' => $this->translate(locale())?->description,
            'logo' => getAvatar($this->logo),
            'country' => CountryResource::make($this->whenLoaded('country')),
            'governorate' => GovernorateResource::make($this->whenLoaded('governorate')),
            'region' => RegionResource::make($this->whenLoaded('region')),
            'address' => $this->address,
            'minimum_order_price' => $this->minimum_order_price,
            'is_fav' => $this->favouritedBy(auth('sanctum')->user()),
            'rate' => $this->getRate(),
            'rate_count' => $this->rates_count,
            'open' => getWeeks($this->workingHours),
            'link_google_play' => Utility::getValByName('link_google_play'),
            'link_apple_store' => Utility::getValByName('link_apple_store'),
            'qr_image' => getAvatar($this->qrCode),
            'lat' => $this->lat,
            'lng' => $this->lng,
            'chat_enable' => (bool)$this->chat_enable,
            'conversation' => ConversationDetailResource::make($this->whenLoaded('_conversation')),
            'distance' => $this->when(Route::currentRouteName() == 'api.shop-nearby', round($this->distance,2))
        ];
    }
}
