<?php

namespace App\Http\Resources\Size;

use Illuminate\Http\Resources\Json\JsonResource;

class SizeResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'size_id' => $this->id,
            'price' => $this->price,
            'name' => $this->translate(locale())?->name,
        ];
    }
}
