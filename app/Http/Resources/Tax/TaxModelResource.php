<?php

namespace App\Http\Resources\Tax;

use Illuminate\Http\Resources\Json\JsonResource;

class TaxModelResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'tax_id' => $this->tax_id,
            'tax_percent' => $this->tax_percent,
            'tax_amount' => $this->tax_amount,
            'total_price' => $this->total_price,
            'total' => $this->total,
            'tax_detail' => TaxResource::make($this->tax),
        ];
    }
}
