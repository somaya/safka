<?php

namespace App\Http\Resources\Tax;

use Illuminate\Http\Resources\Json\JsonResource;

class TaxResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'tax_id' => $this->id,
            'name' => $this->translate(locale())?->name,
            'tax' => $this->tax,
        ];
    }
}
