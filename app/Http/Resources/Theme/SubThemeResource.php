<?php

namespace App\Http\Resources\Theme;

use Illuminate\Http\Resources\Json\JsonResource;

class SubThemeResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'sub_theme_id' => $this->id,
            'code' => $this->code,
            'name' => $this->translate(locale())?->name,
        ];
    }
}
