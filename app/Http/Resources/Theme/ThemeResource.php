<?php

namespace App\Http\Resources\Theme;

use Illuminate\Http\Resources\Json\JsonResource;

class ThemeResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'theme_id' => $this->id,
            'code' => $this->code,
            'name' => $this->translate(locale())?->name,
        ];
    }
}
