<?php

namespace App\Interfaces\Api;

interface FavouriteRepositoryInterface
{
    public function favourite($request, $user);
}
