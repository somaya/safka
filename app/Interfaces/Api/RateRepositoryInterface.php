<?php

namespace App\Interfaces\Api;

interface RateRepositoryInterface
{
    public function Rate($request, $user);
}
