<?php

namespace App\Listeners;


use App\Helpers\CPU\FirebaseNotification;

class SendNotificationToUser
{
    public function handle($event)
    {
        $firebase = new FirebaseNotification();
        $firebase->to($event->token);
        $firebase->withTitle($event->title);
        $firebase->withBody($event->body);
        $firebase->withData($event->data);
        $firebase->asNotification();
    }
}
