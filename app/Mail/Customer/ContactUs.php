<?php

namespace App\Mail\Customer;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactUs extends Mailable
{
    use Queueable, SerializesModels;

    private $data;

    public function __construct($data = [])
    {
        $this->data = $data;
    }

    public function build(): static
    {
        return $this->markdown('customer.mails.contact-us')
            ->subject(_trans('Ticket'))
            ->with('data', $this->data);
    }
}
