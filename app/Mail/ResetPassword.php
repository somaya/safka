<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ResetPassword extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    public function __construct($data = [])
    {
        $this->data = $data;
    }

    public function build(): static
    {
        return $this->markdown('mails.reset-password')
            ->subject(_trans('Reset Password'))
            ->with('data', $this->data);
    }
}
