<?php

namespace App\Models;

use App\Enums\UserType;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminConversation extends Model
{
    use HasFactory;

    protected $table = 'admin_conversations';

    protected $fillable = [
        'sender_id', 'receiver_id', 'last_time_message'
    ];

    /*relation*/

    public function customer(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Customer::class, 'sender_id');
    }

    public function messages(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(AdminMessage::class, 'admin_conversion_id');
    }

    public function lastMessage(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(AdminMessage::class, 'admin_conversion_id')->latest();
    }

}
