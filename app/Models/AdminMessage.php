<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminMessage extends Model
{
    use HasFactory;

    protected $table = 'admin_messages';

    protected $fillable = [
        'admin_conversion_id', 'sender_id', 'receiver_id', 'user_id', 'read', 'body', 'type'
    ];

    /*relation*/

    public function customer(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Customer::class);
    }

    public function conversion(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(AdminConversation::class, 'admin_conversion_id');
    }
}
