<?php

namespace App\Models;

use App\Enums\Status;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Areas extends Model
{
    use HasFactory;

    protected $table = 'areas';

    protected $fillable = [
        'delivery_area_id', 'governorate_id', 'min_time', 'max_time', 'delivery_price', 'min_order_price',
    ];
    protected $with = ['governorate', 'regions'];

    protected $appends = [
        'avgTime'
    ];

    public function getAvgTimeAttribute(): string
    {
        return ($this->min_time + $this->max_time) / 2;
    }
    /* scope */
    public function scopeStatus($query, $status = Status::Active)
    {
        return $query->where('status', $status);
    }

    public function deliveryArea(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(DeliveryArea::class, 'delivery_area_id');
    }

    public function governorate(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Governorate::class, 'governorate_id');
    }

    public function regions(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Region::class, 'area_regions','area_id','region_id')
            ->as('area_regions')->withTimestamps();
    }
}
