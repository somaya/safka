<?php

namespace App\Models;

use App\Enums\Status;
use App\Helpers\Setting\Utility;
use App\QueryFilters\Name\NameQueryFilter;
use App\QueryFilters\BranchType\BranchTypeQueryFilter;
use App\QueryFilters\Sort\SortFilter;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pipeline\Pipeline;
use OwenIt\Auditing\Contracts\Auditable;

class BranchType extends Model implements TranslatableContract, Auditable
{
    use HasFactory, Translatable, \OwenIt\Auditing\Auditable;

    protected $table = 'branch_types';

    protected $fillable = [
        'image', 'published', 'ranking', 'status'
    ];

    protected $with = ['translations'];

    protected $translationForeignKey = 'branch_type_id';

    public $translatedAttributes = [
        'name', 'slug'
    ];

    /* Pipeline Query Filter */
    public static function allBranches()
    {
        return app(Pipeline::class)
            ->send(BranchType::query()->orderBy('ranking')->withCount(['shops']))
            ->through([
                SortFilter::class,
                NameQueryFilter::class,
                BranchTypeQueryFilter::class,
            ])
            ->thenReturn()
            ->orderBy('ranking')
            ->paginate(Utility::getValByName('pagination_limit'));
    }


    /* scope */
    public function scopeStatus($query, $status = Status::Active)
    {
        return $query->where('status', $status->value);
    }

    public function scopePublished($query, $status = Status::Active)
    {
        return $query->where('published', $status->value);
    }

    /* relation */

    public function shops(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Shop::class, 'branch_type_shops', 'branch_type_id', 'shop_id')
            ->as('branch_type_shops')
            ->withTimestamps()->status();
    }


}
