<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CartDetail extends Model
{
    use HasFactory;

    protected $table = 'cart_details';

    protected $fillable = [
        'cart_id', 'modelable_type', 'modelable_id', 'qty', 'price', 'size_id', 'color_id','products', 'extra_info', 'total_price_extras'
    ];

    protected $appends = [
        'total_price'
    ];

    public function getTotalPriceAttribute(): float|int
    {
        return $this->qty * $this->price;
    }

    /*public function getTotalPriceExtrasAttribute(): float|int
    {
        $total = 0;
        if (!is_null($this->extras_info) && count($this->extras_info) > 0) {
            foreach ($this->extras_info as $extra) {
                $total += $extra['total_price'];
            }
        }
        return $total;
    }*/

    public function products(): Attribute
    {
        return new Attribute(
            get: fn($value) => json_decode($value, true),
            set: fn($value) => json_encode($value),
        );
    }

    public function extraInfo(): Attribute
    {
        return new Attribute(
            get: fn($value) => json_decode($value, true),
            set: fn($value) => json_encode($value),
        );
    }

    // relation

    public function cart(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Cart::class, 'cart_id');
    }

    public function size(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Size::class, 'size_id');
    }
    public function color(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Color::class, 'color_id');
    }

    public function product(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Product::class, 'modelable_id');
    }

    public function offer(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Offer::class, 'modelable_id');
    }

    public function saveProduct(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Save::class, 'modelable_id');
    }

    public function discount(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Discount::class, 'modelable_id')->with(['product']);
    }

    public function extra(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Extra::class, 'modelable_id');
    }

}
