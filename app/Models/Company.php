<?php

namespace App\Models;

use App\Enums\Status;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Company extends Model implements  Auditable
{
use HasFactory, \OwenIt\Auditing\Auditable;

    protected $table = 'companies';

    protected $fillable = [
        'ranking','logo','link', 'status'
    ];
    public function scopeStatus($query, $status = Status::Active)
    {
        return $query->where('status', $status->value);
    }

}
