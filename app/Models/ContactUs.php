<?php

namespace App\Models;

use App\Helpers\Setting\Utility;
use App\QueryFilters\ContactUs\ContactUsQueryFilter;
use App\QueryFilters\Sort\SortFilter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pipeline\Pipeline;

class ContactUs extends Model
{
    use HasFactory;

    protected $table = 'contact-us';

    protected $fillable = [
        'first_name', 'last_name', 'email', 'phone','subject', 'message', 'reply'
    ];

    protected $appends = [
        'full_name'
    ];

    public function getFullNameAttribute(): string
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public static function allContactUs()
    {
        return app(Pipeline::class)
            ->send(ContactUs::query())
            ->through([
                SortFilter::class,
                ContactUsQueryFilter::class,
            ])
            ->thenReturn()
            ->paginate(Utility::getValByName('pagination_limit'));
    }
}
