<?php

namespace App\Models;

use App\Enums\Status;
use App\Helpers\Setting\Utility;
use App\QueryFilters\Coupon\CouponQueryFilter;
use App\QueryFilters\Name\NameQueryFilter;
use App\QueryFilters\Sort\SortFilter;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pipeline\Pipeline;
use OwenIt\Auditing\Contracts\Auditable;

class Coupon extends Model implements TranslatableContract, Auditable
{
    use HasFactory, Translatable, \OwenIt\Auditing\Auditable;

    protected $table = 'coupons';

    protected $fillable = [
        'owner_id', 'shop_id','coupon_type', 'code', 'discount', 'limit', 'start_date', 'end_date', 'min_purchase', 'discount_type', 'status',
    ];

    protected $with = ['translations'];

    protected $translationForeignKey = 'coupon_id';

    public $translatedAttributes = [
        'name', 'description'
    ];

    /* Pipeline Query Filter */
    public static function allCoupons($query)
    {
        return app(Pipeline::class)
            ->send($query)
            ->through([
                SortFilter::class,
                CouponQueryFilter::class,
            ])
            ->thenReturn()
            ->paginate(Utility::getValByName('pagination_limit'));
    }

    /*scope*/
    public function ScopeStatus($query, $status = Status::Active)
    {
        $query->where('status', $status);
    }

    /* relation */

    public function owner(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Owner::class, 'owner_id');
    }

    public function shop(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Shop::class, 'shop_id');
    }

    public function usedCoupon(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(UserCoupon::class, 'coupon_id');
    }

}
