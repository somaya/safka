<?php

namespace App\Models;

use App\Enums\UserType;
use App\Helpers\Setting\Utility;
use App\QueryFilters\Customer\CustomerQueryFilter;
use App\QueryFilters\Sort\SortFilter;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pipeline\Pipeline;
use OwenIt\Auditing\Contracts\Auditable;

class Customer extends Model implements Auditable
{
    use  HasFactory, \OwenIt\Auditing\Auditable;

    protected $table = 'customers';

    protected $fillable = [
         'address','lat','lng','facility_name','branch_type', 'gender','phone', 'birthday', 'provider_type', 'provider_id','blocked'
    ];

    protected $appends = [
        'age'
    ];
    protected $with = ['user'];


    public function getAgeAttribute(): string
    {
        if (!empty($this->birthday)) {
            return Carbon::parse($this->birthday)->diff(Carbon::now())->y;
        }
        return 0;
    }

    public static function customerData($userId, $relation = null)
    {
        $customer = Customer::query();
        if (!empty($relation)) {
            $customer = $customer->with($relation);
        }
        return $customer->whereUserId($userId)->first();
    }

    public static function allCustomers($query)
    {
        return app(Pipeline::class)
            ->send($query)
            ->through([
                SortFilter::class,
                CustomerQueryFilter::class,
            ])
            ->thenReturn()
            ->paginate(Utility::getValByName('pagination_limit'));
    }

    /* relation */

    public function user(): \Illuminate\Database\Eloquent\Relations\MorphOne
    {
        return $this->morphOne(User::class, 'userable');
    }


    public function fcmTokens(): \Illuminate\Database\Eloquent\Relations\MorphMany
    {
        return $this->morphMany(NotificationToken::class, 'tokenable');
    }

    public function locations(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Location::class, 'customer_id');
    }

    public function addresses(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Address::class, 'customer_id');
    }

    public function orders(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Order::class, 'customer_id');
    }

    public function conversations(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Conversation::class,'sender_id');
    }

    public function senderable()
    {
        return $this->morphMany(Notification::class, 'senderable');
    }

    public function receiverable()
    {
        return $this->morphMany(Notification::class, 'receiverable');
    }
    public function _conversations(): \Illuminate\Database\Eloquent\Relations\MorphMany
    {
        return $this->morphMany(_Conversations::class,'senderable');
    }
    public function coupons(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Coupon::class,'user_coupons','customer_id','coupon_id')
            ->as('user_coupons')
            ->withPivot(['order_id'])
            ->withTimestamps();
    }
}
