<?php

namespace App\Models;

use App\Enums\Status;
use App\Helpers\Setting\Utility;
use App\QueryFilters\Discount\DiscountQueryFilter;
use App\QueryFilters\Name\NameQueryFilter;
use App\QueryFilters\Sort\SortFilter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pipeline\Pipeline;
use OwenIt\Auditing\Contracts\Auditable;

class Discount extends Model implements Auditable
{
    use HasFactory, \OwenIt\Auditing\Auditable;

    protected $table = 'discounts';

    protected $fillable = [
        'owner_id', 'shop_id', 'start', 'end', 'status', 'price_before', 'percent', 'price_after', 'product_id'
    ];

    public static function allDiscounts($query)
    {
        return app(Pipeline::class)
            ->send($query)
            ->through([
                SortFilter::class,
                DiscountQueryFilter::class,
            ])
            ->thenReturn()
            ->paginate(Utility::getValByName('pagination_limit'));
    }

    /*scope*/
    public function ScopeStatus($query, $status = Status::Active)
    {
        $query->where('status', $status);
    }

    /* relation */

    public function owner(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Owner::class, 'owner_id');
    }

    public function shop(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Shop::class, 'shop_id');
    }

    public function product(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Product::class, 'product_id')->with('images');
    }

    public function rates()
    {
        return $this->morphMany(Rate::class, 'rate');
    }

    public function ratedBy(?User $user)
    {
        if (is_null($user)) {
            return false;
        }
        return $this->rates()->where('user_id', $user->id)->exists();
    }

    public function getRate()
    {
        return $this->rates->isNotEmpty() ? round($this->rates()->avg('degree'), 1) : 0;
    }
}
