<?php

namespace App\Models;

use App\Enums\OrderStatus;
use App\Enums\Status;
use App\Helpers\Setting\Utility;
use App\QueryFilters\Driver\DriverQueryFilter;
use App\QueryFilters\Sort\SortFilter;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Pipeline\Pipeline;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\HasApiTokens;
use OwenIt\Auditing\Contracts\Auditable;

class Driver extends Authenticatable implements Auditable
{
    use HasFactory, \OwenIt\Auditing\Auditable, HasApiTokens;

    protected $table = 'drivers';

    protected $guard_name = 'driver';

    protected $fillable = [
        'owner_type', 'owner_id', 'shop_id', 'name', 'phone', 'notional_id',
        'email', 'password', 'avatar', 'address', 'birthday', 'status', 'percentage', 'firebase_token',
        'country_id', 'governorate_id', 'region_id', 'blocked'
    ];


    protected $appends = [
        'age'
    ];

    public static function allDrivers($query)
    {
        return app(Pipeline::class)
            ->send($query)
            ->through([
                SortFilter::class,
                DriverQueryFilter::class,
            ])
            ->thenReturn()
            ->paginate(Utility::getValByName('pagination_limit'));
    }

    public function password(): Attribute
    {
        return new Attribute(
            set: fn($value) => Hash::make($value),
        );
    }

    public function name(): Attribute
    {
        return new Attribute(
            get: fn($value) => ucfirst($value),
            set: fn($value) => ucfirst($value),
        );
    }

    /* scope */
    public function scopeStatus($query, $status = Status::Active)
    {
        return $query->where('status', $status->value);
    }

    public function getAgeAttribute(): string
    {
        if (!empty($this->birthday)) {
            return Carbon::parse($this->birthday)->diff(Carbon::now())->y;
        }
        return '';
    }

    public static function findByEmailOrPhone($username)
    {
        return static::query()->where(function ($row) use ($username) {
            $row->where('email', $username)->orWhere('phone', $username);
        })->first();
    }

    /* relation */

    public function license(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(License::class, 'driver_id');
    }

    public function vehicle(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Vehicle::class, 'driver_id');
    }

    public function owner(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Owner::class, 'owner_id');
    }

    public function shop(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Shop::class, 'shop_id');
    }

    public function orders(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Order::class, 'driver_orders', 'driver_id', 'order_id')
            ->withTimestamps() // use insert data created_at and updated_at
            ->as('driver_orders'); // rename pivot to showrooms_days
    }

    public function statusOrders(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Order::class, 'driver_orders', 'driver_id', 'order_id')
            ->where(function (Builder $query) {
                return $query->where('order_status', '=', OrderStatus::out_for_delivery->value)
                    ->orWhere('order_status', '=', OrderStatus::delivered->value);
            });
    }

    public function transactions(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(DriverTransaction::class, 'driver_id');
    }

    public function lastBalance($in = 0, $out = 0)
    {
        $sumIn = $this->hasMany(DriverTransaction::class, 'driver_id')->sum('in');

        $sumOut = $this->hasMany(DriverTransaction::class, 'driver_id')->sum('out');

        return ($sumIn + $in) - ($sumOut + $out);
    }

    public function transaction(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(DriverTransaction::class, 'driver_id');
    }

    public function verificationCode(): \Illuminate\Database\Eloquent\Relations\MorphMany
    {
        return $this->morphMany(VerificationCode::class, 'modelable');
    }

    public function country(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function governorate(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Governorate::class, 'governorate_id');
    }

    public function region(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Region::class, 'region_id');
    }

    public function workingHours(): \Illuminate\Database\Eloquent\Relations\MorphMany
    {
        return $this->morphMany(WorkingHour::class, 'modelable');
    }


}
