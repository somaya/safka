<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Favourite extends Model
{
    use HasFactory;
    protected $table = 'favourites';

    protected $fillable = [
        'favourite_type','user_id','favourite_id'
    ];
    public function favourite()
    {
        return $this->morphTo();
    }



}
