<?php

namespace App\Models;

use App\Helpers\Setting\Utility;
use App\QueryFilters\JoinUs\JoinUsQueryFilter;
use App\QueryFilters\Sort\SortFilter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pipeline\Pipeline;

class JoinUs extends Model
{
    use HasFactory;

    protected $table = 'join_us';

    protected $fillable = [
        'shop_name', 'branches_count', 'country_id', 'governorate_id', 'region_id',
        'lat', 'lng', 'owner_name', 'owner_phone', 'owner_email', 'time_to_contact', 'notes',
    ];
    public static function allJoinUs()
    {
        return app(Pipeline::class)
            ->send(JoinUs::query()->with(['branchTypes'])->orderByDesc('created_at'))
            ->through([
                SortFilter::class,
                JoinUsQueryFilter::class,
            ])
            ->thenReturn()
            ->paginate(Utility::getValByName('pagination_limit'));
    }

    /* relation */

    public function country(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function governorate(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Governorate::class, 'governorate_id');
    }

    public function branchTypes(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(BranchType::class, 'branch_type_join', 'join_id', 'branch_type_id')
            ->as('branch_type_join')
            ->withTimestamps();
    }
}
