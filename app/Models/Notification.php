<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    use HasFactory;

    protected $table = 'notifications';

    protected $fillable = [
        'type', 'senderable_type', 'senderable_id', 'receiverable_type', 'receiverable_id', 'data', 'read_at'
    ];

    protected $appends = ['date'];

    protected $with = ['customer.user'];

    public function getDateAttribute(): string
    {
        return $this->created_at->diffForHumans();
    }

    public function data(): Attribute
    {
        return new Attribute(
            get: fn($value) => json_decode($value, true),
            set: fn($value) => json_encode($value),
        );
    }

    /* relation */

    public function senderable(): \Illuminate\Database\Eloquent\Relations\MorphTo
    {
        return $this->morphTo();
    }

    public function receiverable(): \Illuminate\Database\Eloquent\Relations\MorphTo
    {
        return $this->morphTo();
    }

    public function customer(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Customer::class, 'senderable_id')->with(['user']);
    }

}
