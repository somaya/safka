<?php

namespace App\Models;

use App\Enums\Status;
use App\Helpers\Setting\Utility;
use App\QueryFilters\Name\NameQueryFilter;
use App\QueryFilters\Save\SaveQueryFilter;
use App\QueryFilters\Sort\SortFilter;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pipeline\Pipeline;
use OwenIt\Auditing\Contracts\Auditable;

class Offer extends Model implements TranslatableContract, Auditable
{
    use HasFactory, Translatable, \OwenIt\Auditing\Auditable;

    protected $table = 'offers';

    protected $fillable = [
        'price', 'icon', 'video', 'status', 'category_id', 'owner_id', 'shop_id', 'start', 'end'
    ];

    protected $with = ['translations'];

    protected $translationForeignKey = 'offer_id';

    public $translatedAttributes = [
        'name', 'slug'
    ];

    public static function allOffers($query)
    {
        return app(Pipeline::class)
            ->send($query)
            ->through([
                SortFilter::class,
                NameQueryFilter::class,
                SaveQueryFilter::class,
            ])
            ->thenReturn()
            ->paginate(Utility::getValByName('pagination_limit'));
    }

    /*scope*/
    public function ScopeStatus($query, $status = Status::Active)
    {
        $query->where('status', $status);
    }

    /* relation */

    public function category(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function owner(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Owner::class, 'owner_id');
    }

    public function shop(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Shop::class, 'shop_id');
    }

    public function images(): \Illuminate\Database\Eloquent\Relations\MorphMany
    {
        return $this->morphMany(File::class, 'relationable');
    }

    public function products(): \Illuminate\Database\Eloquent\Relations\MorphMany
    {
        return $this->morphMany(ProductModel::class, 'modelable');
    }

    public function rates()
    {
        return $this->morphMany(Rate::class, 'rate');
    }

    public function ratedBy(?User $user)
    {
        if (is_null($user)) {
            return false;
        }
        return $this->rates()->where('user_id', $user->id)->exists();
    }

    public function getRate()
    {
        return $this->rates->isNotEmpty() ? round($this->rates()->avg('degree'), 1) : 0;
    }
}
