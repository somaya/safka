<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class OfferTranslation extends Model implements Auditable
{
    use HasFactory,Sluggable, \OwenIt\Auditing\Auditable;

    protected $table = 'offer_translations';

    protected $fillable = [
        'offer_id', 'name','slug', 'locale'
    ];
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
}
