<?php

namespace App\Models;

use App\Events\AddTaxOnModel;
use App\Helpers\Setting\Utility;
use App\QueryFilters\Order\OrdersQueryFilter;
use App\QueryFilters\Sort\SortFilter;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pipeline\Pipeline;
use OwenIt\Auditing\Contracts\Auditable;

class Order extends Model implements Auditable
{
    use HasFactory, \OwenIt\Auditing\Auditable;

    protected $table = 'orders';

    protected $fillable = [
        'customer_id', 'shop_id', 'delivery_price', 'location_id', 'location_info', 'coupon_code', 'coupon_price',
        'total_price', 'order_status', 'status_pay', 'payment_method', 'code', 'note', 'area_id',
         'special_order','is_created_invoice'
    ];

    protected $appends = [
        'amount'
    ];

    public function locationInfo(): Attribute
    {
        return new Attribute(
            get: fn($value) => json_decode($value, true),
            set: fn($value) => json_encode($value),
        );
    }

    public static function allOrders($query)
    {
        return app(Pipeline::class)
            ->send($query)
            ->through([
                SortFilter::class,
                OrdersQueryFilter::class,
            ])
            ->thenReturn()
            ->paginate(Utility::getValByName('pagination_limit'));
    }

    public function getAmountAttribute(): string
    {
        return $this->total_price + $this->delivery_price - $this->coupon_price + $this->total_tax;
    }

    // relation
    public function customer(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Customer::class, 'customer_id', 'id');
    }

    public function shop(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Shop::class, 'shop_id');
    }

    public function location(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Location::class, 'location_id');
    }

    public function orderDetails(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(OrderDetail::class, 'order_id');
    }

    public function area(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Areas::class, 'area_id');
    }


    public function trackingOrder(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(TrackingOrder::class, 'order_id');
    }

    public function trackingOrderHasOne($orderId, $option = null)
    {
        $trackingOrder = $this->trackingOrder()->where('order_id', '=', $orderId);
        if (!is_null($option)) {
            $trackingOrder = $trackingOrder->latest()->first();
        } else {
            $trackingOrder = $trackingOrder->first();
        }
        return $trackingOrder;
    }

    public function lastTrackingOrder($orderId)
    {
        return $this->trackingOrder()
            ->where('order_id', '=', $orderId)->latest()->first();
    }

    public function drivers(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Driver::class, 'driver_orders', 'order_id', 'driver_id')
            ->withTimestamps() // use insert data created_at and updated_at
            ->as('driver_orders'); // rename pivot to showrooms_days
    }

    public function driver(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(DriverOrder::class, 'order_id')->with(['driver']);
    }


}
