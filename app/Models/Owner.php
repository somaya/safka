<?php

namespace App\Models;

use App\Helpers\Setting\Utility;
use App\QueryFilters\Owner\OwnerQueryFilter;
use App\QueryFilters\Sort\SortFilter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pipeline\Pipeline;
use OwenIt\Auditing\Contracts\Auditable;

class Owner extends Model implements Auditable
{
    use  HasFactory, \OwenIt\Auditing\Auditable;

    protected $table = 'owners';

    protected $fillable = [
        'address', 'brand_name','phone', 'created_by'
    ];

    public function creatorId()
    {
        if (is_null($this->created_by)) {
            return $this->id;
        } else {
            return $this->created_by;
        }
    }

    public static function allOwners($query)
    {
        return app(Pipeline::class)
            ->send($query)
            ->through([
                SortFilter::class,
                OwnerQueryFilter::class,
            ])
            ->thenReturn()
            ->paginate(Utility::getValByName('pagination_limit'));
    }

    /* relation */

    public function user(): \Illuminate\Database\Eloquent\Relations\MorphOne
    {
        return $this->morphOne(User::class, 'userable');
    }

    public function createdBy(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(self::class, 'created_by');
    }

    public function shops(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Shop::class, 'owner_id');
    }
}
