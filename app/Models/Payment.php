<?php

namespace App\Models;

use App\Helpers\Setting\Utility;
use App\QueryFilters\Payment\PaymentQueryFilter;
use App\QueryFilters\Sort\SortFilter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pipeline\Pipeline;
use OwenIt\Auditing\Contracts\Auditable;

class Payment extends Model implements Auditable
{
    use HasFactory, \OwenIt\Auditing\Auditable;

    protected $fillable = [
        'shop_id', 'start_date', 'end_date', 'subscription_type', 'net_amount', 'amount', 'notes', 'status'
    ];

    public static function allPayments($query)
    {
        return app(Pipeline::class)
            ->send($query)
            ->through([
                SortFilter::class,
                PaymentQueryFilter::class,
            ])
            ->thenReturn()
            ->paginate(Utility::getValByName('pagination_limit'));
    }

    /* relation */

    public function shop(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Shop::class, 'shop_id');
    }
}
