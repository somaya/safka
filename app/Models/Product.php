<?php

namespace App\Models;

use App\Enums\DiscountType;
use App\Enums\Status;
use App\Helpers\Setting\Utility;
use App\QueryFilters\Name\NameQueryFilter;
use App\QueryFilters\Product\ProductQueryFilter;
use App\QueryFilters\Sort\SortFilter;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pipeline\Pipeline;
use OwenIt\Auditing\Contracts\Auditable;

class Product extends Model implements TranslatableContract, Auditable
{
    use HasFactory, Translatable, \OwenIt\Auditing\Auditable;

    protected $table = 'products';

    protected $fillable = [
        'category_id', 'owner_id', 'sub_category_id', 'shop_id', 'price', 'video', 'status', 'wanted', 'icon',
        'discount_type', 'discount_price', 'start_date', 'end_date','maximum_order_number'
    ];

    protected $appends = ['is_discount'];

    protected $with = ['translations'];

    protected $translationForeignKey = 'product_id';

    public $translatedAttributes = [
        'name', 'slug', 'description'
    ];

    public function getIsDiscountAttribute(): array
    {
        if (!empty($this->discount_type)) {
            if ($this->start_date >= now()->format('Y-m-d') && $this->end_date <= now()->format('Y-m-d')) {
                if ($this->discount_type == DiscountType::Percentage->value) {
                    $data = calculatePercentage($this->price, $this->discount_price, 'decrease');
                    return [
                        'percentage' => $this->discount_price,
                        'amount' => $data['amount'],
                        'total_amount' => $data['total_amount'],
                        'is_active' => true,
                    ];
                }
                else {
                    $percentage = ($this->discount_price / $this->price) / 100;
                    return [
                        'percentage' => round($percentage, 2),
                        'amount' => $this->discount_price,
                        'total_amount' => $this->price - $this->discount_price,
                        'is_active' => true
                    ];
                }
            }
            return [
                'percentage' => 0,
                'amount' => 0,
                'unit_price' => $this->price,
                'total_amount' => $this->discount_price,
                'is_active' => false
            ];
        }
        return [
            'percentage' => 0,
            'amount' => 0,
            'unit_price' => $this->price,
            'total_amount' => $this->discount_price,
            'is_active' => false
        ];
    }

    public static function allProducts($query)
    {
        return app(Pipeline::class)
            ->send($query)
            ->through([
                SortFilter::class,
                NameQueryFilter::class,
                ProductQueryFilter::class,
            ])
            ->thenReturn()
            ->paginate(Utility::getValByName('pagination_limit'));
    }

    /*scope*/
    public function ScopeStatus($query, $status = Status::Active)
    {
        $query->where('status', $status->value);
    }

    public function ScopeWanted($query, $wanted = Status::Active)
    {
        $query->where('wanted', $wanted->value);
    }


    /* relation */

    public function category(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function Subcategory(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Category::class, 'sub_category_id');
    }

    public function owner(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Owner::class, 'owner_id');
    }

    public function shop(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Shop::class, 'shop_id');
    }

    public function images(): \Illuminate\Database\Eloquent\Relations\MorphMany
    {
        return $this->morphMany(File::class, 'relationable');
    }

    public function sizes(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Size::class, 'product_id');
    }
    public function colors(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Color::class, 'product_id');
    }

    public function saves(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Save::class, 'save_products', 'product_id', 'save_id')
            ->as('save_products')->withPivot('quantity')->withTimestamps();
    }

    public function favourites(): \Illuminate\Database\Eloquent\Relations\MorphMany
    {
        return $this->morphMany(Favourite::class, 'favourite');
    }

    public function rates(): \Illuminate\Database\Eloquent\Relations\MorphMany
    {
        return $this->morphMany(Rate::class, 'rate');
    }

    public function favouritedBy(?User $user): bool
    {
        if (is_null($user)) {
            return false;
        }
        return $this->favourites()->where('user_id', $user->id)->exists();
    }

    public function ratedBy(?User $user): bool
    {
        if (is_null($user)) {
            return false;
        }
        return $this->rates()->where('user_id', $user->id)->exists();
    }

    public function getRate(): float|int
    {
        return $this->rates->isNotEmpty() ? round($this->rates()->avg('degree'), 1) : 0;
    }

    public function discount(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Discount::class, 'product_id');
    }

    public function orderDetails(): \Illuminate\Database\Eloquent\Relations\MorphMany
    {
        return $this->morphMany(OrderDetail::class, 'modelable');
    }

    public function getUses()
    {
        return OrderDetail::query()->where('modelable_type', Product::class)
            ->where('modelable_id', $this->id)->count();
    }

}
