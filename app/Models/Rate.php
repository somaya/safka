<?php

namespace App\Models;

use App\Helpers\Setting\Utility;
use App\QueryFilters\Rate\RateQueryFilter;
use App\QueryFilters\Sort\SortFilter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pipeline\Pipeline;

class Rate extends Model
{
    use HasFactory;
    protected $table = 'rates';

    protected $fillable = [
        'rate_type','user_id','rate_id','degree','comment'
    ];
    public static function allRates()
    {
        return app(Pipeline::class)
            ->send(Rate::query()->with(['user','rate']))
            ->through([
                SortFilter::class,
                RateQueryFilter::class,
            ])
            ->thenReturn()
            ->paginate(Utility::getValByName('pagination_limit'));
    }
    public function rate()
    {
        return $this->morphTo();
    }
    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}
