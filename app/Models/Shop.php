<?php

namespace App\Models;

use App\Enums\Status;
use App\Helpers\Setting\Utility;
use App\QueryFilters\Name\NameQueryFilter;
use App\QueryFilters\Shop\ShopQueryFilter;
use App\QueryFilters\Sort\SortFilter;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pipeline\Pipeline;
use OwenIt\Auditing\Contracts\Auditable;

class Shop extends Model implements TranslatableContract, Auditable
{
    use HasFactory, Translatable, \OwenIt\Auditing\Auditable;

    protected $table = 'shops';

    protected $fillable = [
        'owner_id', 'country_id', 'governorate_id', 'region_id', 'address', 'logo', 'image','phone', 'email', 'status',
        'subscription_type', 'subscription_value','minimum_order_price', 'subscribe_date','invoice_duration','qr_code', 'lat', 'lng','chat_enable'
    ];

    protected $with = ['translations'];

    protected $translationForeignKey = 'shop_id';

    public $translatedAttributes = [
        'name', 'slug', 'description'
    ];

    public static function allShops($query)
    {
        return app(Pipeline::class)
            ->send($query)
            ->through([
                SortFilter::class,
                NameQueryFilter::class,
                ShopQueryFilter::class,
            ])
            ->thenReturn()
            ->paginate(Utility::getValByName('pagination_limit'));
    }

    /* scope */
    public function scopeStatus($query, $status = Status::Active)
    {
        return $query->where('status', $status->value);
    }

    /* relation */

    public function owner(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Owner::class, 'owner_id');
    }

    public function country(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function governorate(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Governorate::class, 'governorate_id');
    }

    public function region(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Region::class, 'region_id');
    }

    public function branchTypes(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(BranchType::class, 'branch_type_shops', 'shop_id', 'branch_type_id')
            ->as('branch_type_shops')
            ->withTimestamps();
    }

    public function workingHours(): \Illuminate\Database\Eloquent\Relations\MorphMany
    {
        return $this->morphMany(WorkingHour::class, 'modelable');
    }

    public function favourites()
    {
        return $this->morphMany(Favourite::class, 'favourite');
    }

    public function rates()
    {
        return $this->morphMany(Rate::class, 'rate');
    }

    public function favouritedBy(?User $user)
    {
        if (is_null($user)) {
            return false;
        }
        return $this->favourites()->where('user_id', $user->id)->exists();
    }

    public function favourited_count(?User $user)
    {
        if (is_null($user)) {
            return 0;
        }
        return Favourite::where('user_id', $user->id)
            ->whereHasMorph('favourite', Product::class, function ($query) {
                $query->where('shop_id', $this->id)->status();
            })->count();
    }


    public function ratedBy(?User $user)
    {
        if (is_null($user)) {
            return false;
        }
        return $this->rates()->where('user_id', $user->id)->exists();
    }

    public function getRate()
    {
        return $this->rates->isNotEmpty() ? round($this->rates()->avg('degree'), 1) : 0;
    }


    public function products(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Product::class, 'shop_id');
    }

    public function offers(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Offer::class, 'shop_id');
    }

    public function saves(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Save::class, 'shop_id');
    }

    public function discounts(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Discount::class, 'shop_id');
    }

    public function getUsers()
    {
        return Order::query()->where('shop_id', $this->id)->distinct()->pluck('customer_id');
    }

    public function getTotalOrders()
    {
        $orders = Order::query()->where('shop_id', $this->id)->get();
        $total = $orders->sum('total_price') - $orders->sum('coupon_price');
        return $total;
    }

    public function transactions(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Transaction::class, 'shop_id');
    }
    public function payments(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Payment::class, 'shop_id');
    }
    public function orders(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Order::class, 'shop_id');
    }
    public function _conversation(): \Illuminate\Database\Eloquent\Relations\MorphOne
    {
        return $this->morphOne(_Conversations::class,'receiverable');
    }
}
