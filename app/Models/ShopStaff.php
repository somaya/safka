<?php

namespace App\Models;

use App\Helpers\Setting\Utility;
use App\QueryFilters\Shop\ShopStaffFilter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pipeline\Pipeline;
use OwenIt\Auditing\Contracts\Auditable;

class ShopStaff extends Model implements Auditable
{
    use HasFactory, \OwenIt\Auditing\Auditable;

    protected $table = 'shop_staff';

    protected $fillable = [
        'shop_id', 'owner_id', 'national_number','phone', 'job_number', 'address', 'work_starting_date'
    ];

    public static function allStaff($query)
    {
        return app(Pipeline::class)
            ->send($query)
            ->through([
               ShopStaffFilter::class,
            ])
            ->thenReturn()
            ->paginate(Utility::getValByName('pagination_limit'));
    }

    public function user(): \Illuminate\Database\Eloquent\Relations\MorphOne
    {
        return $this->morphOne(User::class, 'userable');
    }


    public function owner(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Owner::class, 'owner_id');
    }

    public function shop(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Shop::class, 'shop_id');
    }
}
