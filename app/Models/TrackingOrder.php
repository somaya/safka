<?php

namespace App\Models;

use App\Enums\OrderStatus;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrackingOrder extends Model
{
    use HasFactory;

    protected $table = 'tracking_orders';

    protected $fillable = [
        'order_id', 'order_status', 'start_at', 'end_at'
    ];

    protected $appends = [
        'diffDate', 'diffDateHour', 'start', 'end', 'diffHumans'
    ];

    public function getStartAttribute()
    {
        if ($this->start_at != '' && $this->start_at != null) {
            return formatDate('d-m-Y h:i A', $this->start_at);
        }
        return '';
    }

    public function getEndAttribute()
    {
        if ($this->end_at != '' && $this->end_at != null) {
            return formatDate('d-m-Y h:i A', $this->end_at);
        }
        return '';
    }

    public function getDiffHumansAttribute()
    {
        if (($this->start_at != '' && $this->start_at != null) && ($this->end_at != '' && $this->end_at != null)) {
            $to = Carbon::parse($this->start_at);
            $from = Carbon::parse($this->end_at);
            return $to->diffForHumans($from);
        }
        return 0;
    }

    public function getDiffDateAttribute()
    {
        if (($this->start_at != '' && $this->start_at != null) && ($this->end_at != '' && $this->end_at != null)) {
            $to = Carbon::parse($this->start_at);
            $from = Carbon::parse($this->end_at);
            return $to->diffInMinutes($from);
        }
        return 0;
    }

    public function getDiffDateHourAttribute()
    {
        if (($this->start_at != '' && $this->start_at != null) && ($this->end_at != '' && $this->end_at != null)) {
            $to = Carbon::parse($this->start_at);
            $from = Carbon::parse($this->end_at);
            return $to->diffInHours($from);
        }
        return 0;
    }

    public static function createOrUpdateStatus($order, $previousStatus, $nextStatus)
    {
        $trackingOrder = $order->trackingOrder()->where([
            'order_id' => $order->id,
            'order_status' => $previousStatus,
        ])->first();
        if ($trackingOrder) {
            $trackingOrder->update(['end_at' => now()]);
            $order->trackingOrder()->create([
                'order_status' => $nextStatus,
                'start_at' => now(),
                'end_at' => in_array($nextStatus, [OrderStatus::completed->value, OrderStatus::canceled->value]) ? now() : null,
            ]);

        } else {
            $order->trackingOrder()->create([
                'order_id' => $order->id,
                'order_status' => $nextStatus,
                'start_at' => now(),
            ]);
        }

    }

    public static function changeStatus($order, $status = null)
    {
        switch ($status) {
            case (OrderStatus::canceled->value):
                $order->update([
                    'order_status' => OrderStatus::canceled->value
                ]);
                TrackingOrder::createOrUpdateStatus($order, $order->order_status, OrderStatus::canceled->value);
                break;
            case (OrderStatus::returned->value):
                $order->update([
                    'order_status' => OrderStatus::returned->value
                ]);
                TrackingOrder::createOrUpdateStatus($order, $order->order_status, OrderStatus::returned->value);
                break;
        }
        switch ($order->order_status) {
            case (OrderStatus::pending->value):
                $order->update([
                    'order_status' => OrderStatus::receive->value
                ]);
                TrackingOrder::createOrUpdateStatus($order, OrderStatus::pending->value, OrderStatus::receive->value);
                break;

            case (OrderStatus::receive->value):
                $order->update([
                    'order_status' => OrderStatus::completed->value
                ]);
                TrackingOrder::createOrUpdateStatus($order, OrderStatus::receive->value, OrderStatus::completed->value);
                break;
            default:
                break;
        }
    }

    /* relation */

    public function order(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Order::class, 'order_id');
    }
}
