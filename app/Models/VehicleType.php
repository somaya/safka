<?php

namespace App\Models;

use App\Enums\Status;
use App\Helpers\Setting\Utility;
use App\QueryFilters\Name\NameQueryFilter;
use App\QueryFilters\Sort\SortFilter;
use App\QueryFilters\VehicleType\VehicleTypeQueryFilter;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pipeline\Pipeline;

class VehicleType extends Model implements TranslatableContract
{
    use HasFactory, Translatable;

    protected $table = 'vehicle_types';

    protected $fillable = [
        'status',
    ];

    protected $with = ['translations'];

    protected $translationForeignKey = 'vehicle_type_id';

    public $translatedAttributes = [
        'name',
    ];

    /* Pipeline Query Filter */
    public static function allVehicleTypes()
    {
        return app(Pipeline::class)
            ->send(VehicleType::query()->orderByDesc('created_at'))
            ->through([
                SortFilter::class,
                NameQueryFilter::class,
                VehicleTypeQueryFilter::class,
            ])
            ->thenReturn()
            ->paginate(Utility::getValByName('pagination_limit'));
    }

    /*scope*/
    public function ScopeStatus($query, $status = Status::Active)
    {
        $query->where('status', $status);
    }
}
