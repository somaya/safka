<?php

namespace App\Providers;

use App\Events\UpdateSetting;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (Schema::hasTable('languages')) {
            $languages = languages();
            if (!$languages->isEmpty()) {
                config()->set('translatable.locales', $languages->pluck('code')->toArray());
                config()->set('translatable.locale', default_lang());
                config()->set('translatable.locale', default_lang());
            }
        }
        if (Schema::hasTable('settings')) {
            if (!file_exists(storage_path('setting/site.json'))) {
                event(new UpdateSetting());
            }
        }
    }
}
