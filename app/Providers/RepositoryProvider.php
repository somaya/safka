<?php

namespace App\Providers;

use App\Interfaces\Api\RateRepositoryInterface;
use App\Interfaces\Auth\AuthRepositoryInterface;
use App\Interfaces\BaseRepository\BaseRepositoryInterface;
use App\Interfaces\Role\RoleRepositoryInterface;
use App\Interfaces\UploadFile\UploadFileRepositoryInterface;
use App\Repositories\Api\RateRepository;
use App\Repositories\Auth\AuthRepository;
use App\Repositories\BaseRepository\BaseRepository;
use App\Repositories\Role\RoleRepository;
use App\Repositories\UploadFile\UploadFileRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryProvider extends ServiceProvider
{

    public function register()
    {
        //
    }

    public function boot(): void
    {
        $this->app->bind(BaseRepositoryInterface::class, BaseRepository::class);
        $this->app->bind(AuthRepositoryInterface::class, AuthRepository::class);
        $this->app->bind(RoleRepositoryInterface::class, RoleRepository::class);
        $this->app->bind(UploadFileRepositoryInterface::class, UploadFileRepository::class);
        $this->app->bind(RateRepositoryInterface::class, RateRepository::class);


    }
}
