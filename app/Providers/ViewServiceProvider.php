<?php

namespace App\Providers;

use App\View\Composers\LanguageComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{

    public function register(): void
    {
        //
    }

    public function boot(): void
    {
        // Using class based composers...
        View::composer([
            'admin.currency.form',
            'admin.country.form',
            'admin.governorate.form',
            'admin.region.form',
            'admin.branch-type.form',
            'admin.category.form',
            'admin.shop.form',
            'admin.page.form',
            'admin.coupon.form',
            'dashboard.product.form',
            'dashboard.save.form',
            'dashboard.offer.form',
            'dashboard.discount.form',
            'admin.banner.form',
            'admin.setting.tabs.setting',
            'admin.setting.index',
            'admin.blog.form',
            'admin.blog_categories.form',
            'admin.join-us.form',
            'admin.day.form',
            'admin.notification.index',
            'admin.vehicle-type.form',
            'admin.service.form',
        ], LanguageComposer::class);
    }
}
