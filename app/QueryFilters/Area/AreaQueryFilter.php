<?php

namespace App\QueryFilters\Area;

use App\QueryFilters\Filter;
use Illuminate\Support\Facades\Auth;

class AreaQueryFilter extends Filter
{
    protected function applyFilter($builder)
    {
        $search = request('search');

        if (request('column_name') == 'code') {
            $builder->where('id', str_replace('Area#', '', $search));
        }

        if (Auth::guard('admin')->check()) {
            if (request('column_name') == 'owner') {
                $builder->whereHas('owner.user', function ($query) use ($search) {
                    $query->where('name', 'LIKE', "%{$search}%",);
                });
            }
        }

        if (request('column_name') == 'shop') {
            $builder->whereHas('shop', function ($query) use ($search) {
                $query->whereTranslationLike('name', "%{$search}%", default_lang());
            });
        }

        if (request('column_name') == 'all') {
            $builder->where('id', str_replace('Area#', '', $search))
                ->orWhereHas('shop', function ($query) use ($search) {
                    $query->whereTranslationLike('name', "%{$search}%", default_lang());
                });
            if (Auth::guard('admin')->check()) {
                $builder->orWhereHas('owner.user', function ($query) use ($search) {
                    $query->where('name', 'LIKE', "%{$search}%",);
                });
            }
        }

        return $builder;
    }
}
