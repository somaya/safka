<?php

namespace App\QueryFilters\Banner;

use App\QueryFilters\Filter;

class BannerQueryFilter extends Filter
{
    protected function applyFilter($builder)
    {
        $search = str_replace('Bann#', '', request('search'));

        if (request('column_name') == 'code') {
            $builder->where('id', '=', $search);
        }
        if (request('column_name') == 'resource_type') {
            $builder->where('resource_type', 'LIKE', "%%{$search}");
        }
        if (request('column_name') == 'banner_type') {
            $builder->where('banner_type', 'LIKE', "%%{$search}");
        }
        if (request('column_name') == 'all') {
            $builder->orWhere(function ($q) use ($search) {
                $q->where('id', '=', $search)
                    ->orWhere('resource_type', 'LIKE', "%%{$search}")
                    ->orWhere('banner_type', 'LIKE', "%%{$search}");
            });
        }

        return $builder;
    }

}
