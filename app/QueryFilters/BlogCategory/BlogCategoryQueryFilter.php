<?php

namespace App\QueryFilters\BlogCategory;

use App\QueryFilters\Filter;

class BlogCategoryQueryFilter extends Filter
{

        protected function applyFilter($builder)
    {
        $search = request('search');

        if (request('column_name') == 'code') {
            $builder->where('id', str_replace('BlogCategory#', '', $search));
        }

        if (request('column_name') == 'all') {
            $builder->orWhere('id', str_replace('BlogCategory#', '', $search));
        }
        return $builder;
    }

}
