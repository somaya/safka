<?php

namespace App\QueryFilters\BlogCategory;

use App\QueryFilters\Filter;

class BlogQueryFilter extends Filter
{
    protected function applyFilter($builder)
    {
        $search = request('search');

        $columnName = request()->has('column_name') ? request('column_name') : $this->filterName();

        if (request('column_name') == 'title' && request()->filled('column_name')) {
            return $builder->whereTranslationLike($columnName, "%{$search}%", default_lang());
        }
        if (request('column_name') == 'code' && request()->filled('column_name')) {
            return $builder->where('id', str_replace('Blog#', '', $search));
        }

        if (request('column_name') == 'all' && request()->filled('column_name')) {
            return $builder->orWhereTranslationLike('title', "%{$search}%", default_lang())
                ->orWhere('id', str_replace('Blog#', '', $search))
                ->orWhereHas('category', function ($q) use ($search) {
                    $q->whereTranslationLike('name', "%{$search}%", default_lang());
                });
        }


    }
}
