<?php

namespace App\QueryFilters\BranchType;

use App\QueryFilters\Filter;

class BranchTypeQueryFilter extends Filter
{
    protected function applyFilter($builder)
    {
        $search = str_replace('BranchType#', '', request('search'));

        if (request('column_name') == 'code') {
            $builder->where('id', '=', $search);
        }
        if (request('column_name') == 'ranking' && request()->filled('column_name')) {
            $builder->where('ranking', '=', $search);
        }

        if (request('column_name') == 'all') {
            $builder->orWhere(function ($q) use ($search) {
                $q->where('id', '=', $search)
                    ->orWhere('ranking', '=', $search);
            });
        }
        return $builder;
    }
}
