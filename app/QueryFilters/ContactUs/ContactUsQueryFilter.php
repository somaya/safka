<?php

namespace App\QueryFilters\ContactUs;

use App\QueryFilters\Filter;
use Illuminate\Support\Facades\DB;

class ContactUsQueryFilter extends Filter
{
    protected function applyFilter($builder)
    {
        $search = request('search');

        if (request('column_name') == 'full_name') {
            $builder->where(DB::raw('CONCAT_WS(" ", first_name, last_name)'), 'LIKE', "%{$search}%");
        }
        if (request('column_name') == 'phone') {
            $builder->where('phone', 'LIKE', "%{$search}%");
        }
        if (request('column_name') == 'email') {
            $builder->where('email', 'LIKE', "%{$search}%");
        }
        if (request('column_name') == 'all') {
            $builder->where(function ($query) use ($search) {
                $query->orWhere(DB::raw('CONCAT_WS(" ", first_name, last_name)'), 'LIKE', "%{$search}%")
                    ->orWhere('phone', 'LIKE', "%{$search}%")
                    ->orWhere('email', 'LIKE', "%{$search}%");
            });
        }
        return $builder;
    }

}
