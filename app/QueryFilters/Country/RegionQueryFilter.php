<?php

namespace App\QueryFilters\Country;

use App\QueryFilters\Filter;

class RegionQueryFilter extends Filter
{
    protected function applyFilter($builder)
    {
        $search = str_replace('Region#', '', request('search'));

        if (request('column_name') == 'code') {
            $builder->where('id', '=', $search);
        }

        if (request('column_name') == 'country') {
            $builder->whereHas('country', function ($query) use ($search) {
                $query->whereTranslationLike('name', "%{$search}%", default_lang());
            });;
        }

        if (request('column_name') == 'governorate') {
            $builder->whereHas('governorate', function ($query) use ($search) {
                $query->whereTranslationLike('name', "%{$search}%", default_lang());
            });;
        }
        if (request('column_name') == 'all') {
            $builder->orWhere('id', '=', $search)
                ->orWhereHas('country', function ($query) use ($search) {
                    $query->whereTranslationLike('name', "%{$search}%", default_lang());
                })->orWhereHas('governorate', function ($query) use ($search) {
                    $query->whereTranslationLike('name', "%{$search}%", default_lang());
                });
        }

        return $builder;
    }

}
