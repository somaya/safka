<?php

namespace App\QueryFilters\Coupon;

use App\QueryFilters\Filter;

class CouponQueryFilter extends Filter
{
    protected function applyFilter($builder)
    {
        $search = str_replace('Coupon#', '', request('search'));

        if (request('column_name') == 'code') {
            $builder->where('id', $search);
        }

        if (request('column_name') == 'name') {
            $builder->whereTranslationLike('name', "%{$search}%", default_lang());
        }

        if (request('column_name') == 'owner') {
            $builder->whereHas('owner.user', function ($q) use ($search) {
                $q->where('name', 'LIKE', "%{$search}%");
            });
        }
        if (request('column_name') == 'shop') {
            $builder->whereHas('shop', function ($q) use ($search) {
                $q->whereTranslationLike('name', "%{$search}%", default_lang());
            });
        }

        if (request('column_name') == 'all') {
            $builder->where('id', '=', $search)
                ->orWhereTranslationLike('name', "%{$search}%", default_lang())
                ->orWhereHas('owner.user', function ($q) use ($search) {
                    $q->where('name', 'LIKE', "%{$search}%");
                })->orWhereHas('shop', function ($q) use ($search) {
                    $q->whereTranslationLike('name', "%{$search}%", default_lang());
                });
        }
        return $builder;
    }
}
