<?php

namespace App\QueryFilters\Customer;

use App\QueryFilters\Filter;

class CustomerQueryFilter extends Filter
{
    protected function applyFilter($builder)
    {
        $search = request('search');

        $columnName = request()->has('column_name') ? request('column_name') : $this->filterName();

        if (request('column_name') == 'name' && request()->filled('column_name')) {
            return $builder->whereHas('user', function ($query) use ($search, $columnName) {
                $query->where($columnName, 'LIKE', "%{$search}%");
            });
        }
        if (request('column_name') == 'email' && request()->filled('column_name')) {
            return $builder->whereHas('user', function ($query) use ($search, $columnName) {
                $query->where($columnName, 'LIKE', "%{$search}%");
            });
        }
        if (request('column_name') == 'phone' && request()->filled('column_name')) {
            return $builder->where('phone', 'LIKE', "%{$search}%");
        }
        if (request('column_name') == 'shop' && request()->filled('column_name')) {
            return $builder->whereHas('orders', function ($query) use ($search, $columnName) {
                $query->whereHas('shop',function ($q) use($search){
                    $q->whereTranslationLike('name', "%{$search}%", default_lang());
                });
            });
        }

        if (request('column_name') == 'all' && request()->filled('column_name')) {
            return $builder->orWhere('phone', 'LIKE', "%{$search}%")
                ->orWhereHas('user', function ($query) use ($search) {
                $query->where('name', 'LIKE', "%{$search}%")
                    ->orWhere('email', 'LIKE', "%{$search}%");
            })->orWhereHas('orders', function ($query) use ($search, $columnName) {
                $query->whereHas('shop',function ($q) use($search){
                    $q->whereTranslationLike('name', "%{$search}%", default_lang());
                });
            });
        }

    }
}
