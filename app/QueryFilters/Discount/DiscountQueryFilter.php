<?php

namespace App\QueryFilters\Discount;

use App\QueryFilters\Code\CodeQueryFilter;
use App\QueryFilters\Filter;

class DiscountQueryFilter extends Filter
{
    protected function applyFilter($builder)
    {
        $search = str_replace('Discount#', '', request('search'));

        if (request('column_name') == 'code') {
            (new CodeQueryFilter($search))->code($builder);
        }

        if (request('column_name') == 'owner') {
             $builder->whereRelation('owner.user', 'name', 'LIKE', "%{$search}%");
        }

        if (request('column_name') == 'shop') {
             $builder->whereHas('shop', function ($q) use ($search) {
                $q->whereTranslationLike('name', "%{$search}%", default_lang());
            });
        }

        if (request('column_name') == 'product') {
             $builder->whereHas('product', function ($q) use ($search) {
                $q->whereTranslationLike('name', "%{$search}%", default_lang());
            });
        }


        if (request('column_name') == 'all') {
            (new CodeQueryFilter($search))->all($builder);
             $builder
                ->orWhereRelation('owner.user', 'name', 'LIKE', "%{$search}%")
                ->orWhereHas('product', function ($q) use ($search) {
                    $q->whereTranslationLike('name', "%{$search}%", default_lang());
                })->orWhereHas('shop', function ($q) use ($search) {
                    $q->whereTranslationLike('name', "%{$search}%", default_lang());
                });;
        }
        return $builder;
    }
}
