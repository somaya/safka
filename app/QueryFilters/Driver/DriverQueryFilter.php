<?php

namespace App\QueryFilters\Driver;

use App\Enums\UserType;
use App\QueryFilters\Filter;
use Illuminate\Support\Facades\Auth;

class DriverQueryFilter extends Filter
{
    protected function applyFilter($builder)
    {
        $search = str_replace('Driver#', '', request('search'));
        if (request('column_name') == 'code') {
            $builder->where('id', '=', $search);
        }

        if (request('column_name') == 'name') {
            $builder->where('name', 'LIKE', "%{$search}%");
        }

        if (request('column_name') == 'phone') {
            $builder->where('phone', 'LIKE', "%{$search}%");
        }

        if (request('column_name') == 'owner_name') {
            $builder->whereHas('owner.user', function ($q) use ($search) {
                $q->where('name', 'LIKE', "%{$search}%");
            });
        }
        if (request('column_name') == 'order_code') {
            $driverId = str_replace('#', '', $search);
            $builder->where('id', '=', $driverId);
        }

        if (request('column_name') == 'shop_name') {
            $builder->whereHas('shop', function ($q) use ($search) {
                $q->whereTranslationLike('name', "%{$search}%", default_lang());
            });
        }
//        if (auth()->user()->userable_type == UserType::Staff_Owner->value) {
//            if (request('column_name') == 'all') {
//                $builder->whereIn('shop_id', userType())->where(function ($q) use ($search) {
//                    $q->orWhere('id', 'LIKE', "%{$search}%")
//                        ->orWhere('name', 'LIKE', "%{$search}%")
//                        ->orWhere('phone', 'LIKE', "%{$search}%")
//                        ->orWhere('owner_type', 'LIKE', "%{$search}%");
//                });
//            }
//        }
//        if (auth()->user()->userable_type == UserType::Staff_Owner->value) {
//            if (request('column_name') == 'all') {
//                $builder->where(function ($q) use ($search) {
//                    $q->orWhere('id', 'LIKE', "%{$search}%")
//                        ->orWhere('name', 'LIKE', "%{$search}%")
//                        ->orWhere('phone', 'LIKE', "%{$search}%")
//                        ->orWhere('owner_type', 'LIKE', "%{$search}%");
//                })->orWhereHas('shop', function ($q) use ($search) {
//                    $q->whereTranslationLike('name', "%{$search}%", default_lang());
//                });
//            }
//        }

        if (Auth::guard('admin')->check()) {
            if (request('column_name') == 'all') {
                $builder->where(function ($q) use ($search) {
                    $q->orWhere('id', 'LIKE', "%{$search}%")
                        ->orWhere('name', 'LIKE', "%{$search}%")
                        ->orWhere('phone', 'LIKE', "%{$search}%")
                        ->orWhere('owner_type', 'LIKE', "%{$search}%");
                })->orWhereHas('owner.user', function ($q) use ($search) {
                    $q->where('name', 'LIKE', "%{$search}%");
                })->orWhereHas('shop', function ($q) use ($search) {
                    $q->whereIn('id', userType())->whereTranslationLike('name', "%{$search}%", default_lang());
                });
            }
        }


        return $builder;
    }
}
