<?php

namespace App\QueryFilters\JoinUs;

use App\QueryFilters\Filter;

class JoinUsQueryFilter extends Filter
{
    protected function applyFilter($builder)
    {
        $search = request('search');

        if (request('column_name') == 'shop_name') {
            $builder->where('shop_name', 'LIKE', "%{$search}%");
        }

        if (request('column_name') == 'owner_name') {
            $builder->where('owner_name', 'LIKE', "%{$search}%");
        }

        if (request('column_name') == 'owner_phone') {
            $builder->where('owner_phone', 'LIKE', "%{$search}%");
        }


        if (request('column_name') == 'all') {
            $builder->where('shop_name', 'LIKE', "%{$search}%")
                ->orWhere('owner_name', 'LIKE', "%{$search}%")
                ->orWhere('owner_phone', 'LIKE', "%{$search}%")
            ;
        }
        return $builder;
    }
}
