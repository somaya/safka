<?php

namespace App\QueryFilters\Name;

use App\QueryFilters\Filter;

class NameQueryFilter extends Filter
{
    protected function applyFilter($builder)
    {
        $search = request('search');
        if (request('column_name') == 'name') {
            $builder->whereTranslationLike('name', "%{$search}%", default_lang());
        }

        if (request('column_name') == 'all') {
            $builder->orWhereTranslationLike('name', "%{$search}%", default_lang());
        }
        return $builder;
    }
}
