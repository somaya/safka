<?php

namespace App\QueryFilters\Order;

use App\QueryFilters\Filter;
use Carbon\Carbon;

class OrdersQueryFilter extends Filter
{
    protected function applyFilter($builder)
    {
        $search = request('search');

        if (request()->filled('start')) {
            $builder = $builder->where('created_at', '>=', request('start'));
        }
        if (request()->filled('end')) {
            $builder = $builder->where('created_at', '<=', Carbon::parse(request('end'))->addDay()->format('Y-m-d'));
        }

        if (request('column_name') == 'shop') {
            $builder = $builder->whereHas('shop', function ($q) use ($search) {
                $q->whereTranslationLike('name', "%{$search}%", default_lang());
            });
        }
        if (request('column_name') == 'customer') {
            $builder = $builder->whereHas('customer.user', function ($q) use ($search) {
                $q->where('name', 'LIKE', "%{$search}%");

            });
        }
        if (request('column_name') == 'code' && request()->filled('column_name')) {
            $builder = $builder->where('code', $search);
        }

        if (request('column_name') == 'all') {
            $builder = $builder->WhereHas('shop', function ($q) use ($search) {
                $q->whereTranslationLike('name', "%{$search}%", default_lang());
            })->orWhereHas('customer.user', function ($q) use ($search) {
                $q->where('name', 'LIKE', "%{$search}%");
            })->orWhere('code', $search);
        }
        return $builder;

    }
}
