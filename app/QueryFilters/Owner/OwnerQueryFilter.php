<?php

namespace App\QueryFilters\Owner;

use App\QueryFilters\Code\CodeQueryFilter;
use App\QueryFilters\Filter;

class OwnerQueryFilter extends Filter
{
    protected function applyFilter($builder)
    {
        $search = str_replace('Owner#', '', request('search'));

        if (request('column_name') == 'code') {
            (new CodeQueryFilter($search))->code($builder);
        }

        if (request('column_name') == 'name') {
            $builder->whereRelation("user", "name", "LIKE", "%{$search}%");
        }
        if (request('column_name') == 'shop') {
            $builder->where('brand_name', 'LIKE', "%{$search}%");
        }
        if (request('column_name') == 'country') {
            $builder->whereHas('user.country', function ($q) use ($search) {
                $q->whereTranslationLike('name', "%{$search}%", default_lang());
            });
        }
        if (request('column_name') == 'governorate') {
            $builder->whereHas('user.governorate', function ($q) use ($search) {
                $q->whereTranslationLike('name', "%{$search}%", default_lang());
            });
        }
        if (request('column_name') == 'region') {
            $builder->whereHas('user.region', function ($q) use ($search) {
                $q->whereTranslationLike('name', "%{$search}%", default_lang());
            });
        }

        if (request('column_name') == 'all') {
            (new CodeQueryFilter($search))->all($builder);
                $builder->orWhere('brand_name', 'LIKE', "%{$search}%")
                ->orWhereRelation("user", "name", "LIKE", "%{$search}%")
                ->orWhereHas('user.country', function ($q) use ($search) {
                    $q->whereTranslationLike('name', "%{$search}%", default_lang());
                })->orWhereHas('user.governorate', function ($q) use ($search) {
                    $q->whereTranslationLike('name', "%{$search}%", default_lang());
                })->orWhereHas('user.region', function ($q) use ($search) {
                    $q->whereTranslationLike('name', "%{$search}%", default_lang());
                });
        }
        return $builder;
    }
}
