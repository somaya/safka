<?php

namespace App\QueryFilters\Payment;

use App\QueryFilters\Filter;

class PaymentQueryFilter extends Filter
{
    protected function applyFilter($builder)
    {
        $search = str_replace('Invoice#', '', request('search'));

        if (request('column_name') == 'code') {
            $builder->where('id', 'LIKE', "%{$search}%");
        }

        if (request('column_name') == 'shop') {
            $builder->whereHas('shop', function ($query) use ($search) {
                $query->whereTranslationLike('name', "%{$search}%", default_lang());
            });;
        }
        if (request('column_name') == 'all') {
            $builder->orWhere('id', 'LIKE', "%{$search}%")
                ->orWhereHas('shop', function ($query) use ($search) {
                    $query->whereTranslationLike('name', "%{$search}%", default_lang());
                });
        }

        return $builder;
    }

}
