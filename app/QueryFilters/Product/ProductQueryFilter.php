<?php

namespace App\QueryFilters\Product;

use App\QueryFilters\Filter;

class ProductQueryFilter extends Filter
{
    protected function applyFilter($builder)
    {
        $search = str_replace('Product#', '', request('search'));

        if (request('column_name') == 'code') {
            $builder->where('id', '=', $search);
        }

        if (request('column_name') == 'category') {
            $builder->whereHas('category', function ($q) use ($search) {
                $q->whereTranslationLike('name', "%{$search}%", default_lang());
            });
        }
        if (request('column_name') == 'shop') {
            $builder->whereHas('shop', function ($q) use ($search) {
                $q->whereTranslationLike('name', "%{$search}%", default_lang());
            });
        }

        if (request('column_name') == 'all') {
            $builder->orWhere('id', '=', $search)
                ->orWhereHas('category', function ($q) use ($search) {
                    $q->whereTranslationLike('name', "%{$search}%", default_lang());
                })->orWhereHas('shop', function ($q) use ($search) {
                    $q->whereTranslationLike('name', "%{$search}%", default_lang());
                });
        }
        return $builder;

    }
}
