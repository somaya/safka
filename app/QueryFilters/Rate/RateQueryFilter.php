<?php

namespace App\QueryFilters\Rate;

use App\QueryFilters\Filter;

class RateQueryFilter extends Filter
{
    protected function applyFilter($builder)
    {
        $search = request('search');

        if (request('column_name') == 'code') {
            $builder->where('id', str_replace('Rate#', '', $search));
        }

        if (request('column_name') == 'model') {
            $builder->where('rate_type', 'LIKE', "%{$search}%");
        }
        if (request('column_name') == 'comment') {
            $builder->where('comment', 'LIKE', "%{$search}%");
        }
        if (request('column_name') == 'username') {
            $builder->whereHas('user', function ($q) use ($search) {
                $q->where('name', 'LIKE', "%{$search}%");
            });
        }

        if (request('column_name') == 'all') {
            $builder->where(function ($q) use ($search) {
                $q->where('id', str_replace('Rate#', '', $search))
                    ->orWhere('rate_type', 'LIKE', "%{$search}%")
                    ->orWhere('comment', 'LIKE', "%{$search}%");
            })->orWhereHas('user', function ($q) use ($search) {
                $q->where('name', 'LIKE', "%{$search}%");
            });
        }
        return $builder;
    }

}
