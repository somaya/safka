<?php

namespace App\QueryFilters\Save;

use App\QueryFilters\Code\CodeQueryFilter;
use App\QueryFilters\Filter;
use Illuminate\Support\Facades\Auth;

class SaveQueryFilter extends Filter
{
    protected function applyFilter($builder)
    {
        if (request()->is('save*')) {
            $search = str_replace('Save#', '', request('search'));
        } else {
            $search = str_replace('Offer#', '', request('search'));
        }

        if (request('column_name') == 'code') {
            (new CodeQueryFilter($search))->code($builder);
        }
        if (Auth::guard('admin')->check())
        {
            if (request('column_name') == 'owner') {
                $builder->whereRelation('owner.user', 'name', 'LIKE', "%{$search}%");
            }
        }

        if (request('column_name') == 'shop') {
            $builder->whereHas('shop', function ($q) use ($search) {
                $q->whereTranslationLike('name', "%{$search}%", default_lang());
            });
        }

        if (request('column_name') == 'all') {
            (new CodeQueryFilter($search))->all($builder);
            if (Auth::guard('admin')->check()) {
                $builder->orWhereRelation('owner.user', 'name', 'LIKE', "%{$search}%");
            }
               $builder->orWhereHas('shop', function ($q) use ($search) {
                    $q->whereTranslationLike('name', "%{$search}%", default_lang());
                });
        }
        return $builder;
    }
}
