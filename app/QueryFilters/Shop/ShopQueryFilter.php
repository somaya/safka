<?php

namespace App\QueryFilters\Shop;

use App\QueryFilters\Code\CodeQueryFilter;
use App\QueryFilters\Filter;

class ShopQueryFilter extends Filter
{
    protected function applyFilter($builder)
    {
        $search = str_replace('Shop#', '', request('search'));

        if (request('column_name') == 'code') {
            (new CodeQueryFilter($search))->code($builder);
        }

        if (request('column_name') == 'country') {
            $builder->whereHas('country', function ($q) use ($search) {
                $q->whereTranslationLike('name', "%{$search}%", default_lang());
            });
        }
        if (request('column_name') == 'governorate') {
            $builder->whereHas('governorate', function ($q) use ($search) {
                $q->whereTranslationLike('name', "%{$search}%", default_lang());
            });
        }

        if (request('column_name') == 'region') {
            $builder->whereHas('region', function ($q) use ($search) {
                $q->whereTranslationLike('name', "%{$search}%", default_lang());
            });
        }

        if (request('column_name') == 'all') {
            (new CodeQueryFilter($search))->all($builder);
            $builder->orWhereHas('country', function ($q) use ($search) {
                $q->whereTranslationLike('name', "%{$search}%", default_lang());
            })->orWhereHas('governorate', function ($q) use ($search) {
                $q->whereTranslationLike('name', "%{$search}%", default_lang());
            })->orWhereHas('region', function ($q) use ($search) {
                $q->whereTranslationLike('name', "%{$search}%", default_lang());
            });
        }

        return $builder;
    }
}
