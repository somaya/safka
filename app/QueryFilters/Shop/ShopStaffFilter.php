<?php

namespace App\QueryFilters\Shop;

use App\QueryFilters\Filter;

class ShopStaffFilter extends Filter
{
    protected function applyFilter($builder)
    {
        $search = request('search');

        $columnName = request()->has('column_name') ? request('column_name') : $this->filterName();

        if (request('column_name') == 'name' && request()->filled('column_name')) {
            return $builder->whereHas('user',function($q) use($search){
                    $q->where('name','LIKE',"%{$search}%");
            });
        }
        if (request('column_name') == 'owner_name' && request()->filled('column_name')) {
            return $builder->whereHas('owner',function($q) use($search){
                $q->whereHas('user',function($q) use($search){
                    $q->where('name','LIKE',"%{$search}%");
                });
                });
        }
        if (request('column_name') == 'code' && request()->filled('column_name')) {
            return $builder->where('id', str_replace('Staff#', '', $search));
        }
        if (request('column_name') == 'job_number' && request()->filled('column_name')) {
            return $builder->where('job_number',$search);
        }
        if (request('column_name') == 'national_number' && request()->filled('column_name')) {
            return $builder->where('national_number',$search);
        }
        if (request('column_name') == 'shop_name' && request()->filled('column_name')) {
            return $builder->whereHas('owner',function($q) use($search){
                $q->where('brand_name','LIKE',"%{$search}%");

            });
        }

        if (request('column_name') == 'all' && request()->filled('column_name')) {
            return $builder->WhereHas('user',function($q) use($search){
                    $q->where('name','LIKE',"%{$search}%");
                     })
                ->orWhereHas('owner',function($q) use($search){
                    $q->whereHas('user',function($q) use($search){
                        $q->where('name','LIKE',"%{$search}%");
                    });
                })
                ->orWhere('id', str_replace('Staff#', '', $search))
                ->orWhere('job_number',$search)
                ->orWhere('national_number',$search)
                ->orWhereHas('owner',function($q) use($search){
                    $q->where('brand_name','LIKE',"%{$search}%");

                });
        }

    }
}
