<?php

namespace App\QueryFilters\SocialMedia;

use App\QueryFilters\Filter;

class SocialMediaQueryFilter extends Filter
{
    protected function applyFilter($builder)
    {
        $search = request('search');

        $columnName = request()->has('column_name') ? request('column_name') : $this->filterName();

        if (request('column_name') == 'slug' && request()->filled('column_name')) {
            return $builder->where($columnName, 'LIKE', "%{$search}%");
        }
        if (request('column_name') == 'all' && request()->filled('column_name')) {
            return $builder->where('slug', 'LIKE', "%{$search}%");
        }

    }
}
