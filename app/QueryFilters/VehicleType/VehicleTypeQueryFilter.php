<?php

namespace App\QueryFilters\VehicleType;

use App\QueryFilters\Filter;

class VehicleTypeQueryFilter extends Filter
{
    protected function applyFilter($builder)
    {
        $search = str_replace('VehicleType#', '', request('search'));

        if (request('column_name') == 'code') {
            $builder->where('id', '=', $search);
        }
        if (request('column_name') == 'all' && request()->filled('column_name')) {
            $builder->orWhere('id', '=', $search);
        }
        return $builder;
    }

}
