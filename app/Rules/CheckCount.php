<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Arr;

class CheckCount implements Rule
{

    public $extras;

    public function __construct()
    {

    }

    public function passes($attribute, $value)
    {
        $this->extras = $value;
        if (!is_null($value) && count($value) > 0) {
            foreach ($value as $key) {
                if (Arr::exists($key, 'id')) {
                    if (is_null($key['id'])) {
                        return false;
                    }
                } else {
                    return false;
                }

                if (Arr::exists($key, 'qty')) {
                    if (is_null($key['qty'])) {
                        return false;
                    }
                } else {
                    return false;
                }

            }

        }
        return true;
    }


    public function message(): string
    {
        $value = $this->extras;
        if (!is_null($value) && count($value) > 0) {
            foreach ($value as $item => $key) {
                if (Arr::exists($key, 'id')) {
                    if (is_null($key['id'])) {
                        return _trans('invalid id can not be null in index array extras ' . $item);
                    }
                } else {
                    return _trans('Please add key (id) in array extras in index ' . $item);
                }
                if (Arr::exists($key, 'id')) {
                    if (is_null($key['qty'])) {
                        return _trans('invalid qty can not be null in index array extras ' . $item);
                    }
                } else {
                    return _trans('Please add key (qty) in array extras in index ' . $item);
                }
            }
        }
    }
}
