<?php

namespace App\Rules;

use App\Models\Coupon;
use Illuminate\Contracts\Validation\Rule;


class ExpireCoupon implements Rule
{

    public function passes($attribute, $value): bool
    {
        $date = now()->format('Y-m-d');
        $query = [
            ['start_date', '<=', $date],
            ['end_date', '>=', $date],
        ];
        $data = Coupon::query()->where($query)->whereCode($value)->first();
        if ($data) {
            return true;
        }
        return false;
    }


    public function message(): string
    {
        return _trans('this coupon is expired');
    }
}
