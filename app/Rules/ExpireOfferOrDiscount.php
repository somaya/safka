<?php

namespace App\Rules;

use App\Models\Discount;
use App\Models\Offer;
use Illuminate\Contracts\Validation\Rule;


class ExpireOfferOrDiscount implements Rule
{

    private $model_type;

    public function __construct($model_type)
    {
        $this->model_type = $model_type;
    }

    public function passes($attribute, $value): bool
    {
        $date = now()->format('Y-m-d');
        $query = [
            ['start', '<=', $date],
            ['end', '>=', $date],
        ];
        if ($this->model_type == 'offer') {
            $data = Offer::query()->where($query)->find($value);
        } else {
            $data = Discount::query()->where($query)->find($value);
        }

        if ($data) {
            return true;
        }
        return false;
    }


    public function message(): string
    {
        return _trans('this item' . $this->model_type . ' is expired');
    }
}
