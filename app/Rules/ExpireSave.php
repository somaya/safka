<?php

namespace App\Rules;

use App\Models\Save;
use Illuminate\Contracts\Validation\Rule;


class ExpireSave implements Rule
{

    public function passes($attribute, $value): bool
    {
        $date = now();
        $time = $date->format('H:i');
        // DB::raw("CONCAT(saves.start,' ',saves.end) as full_name")
        $query = [
            ['start', '<=', $date],
            ['end', '>=', $date],
            /* ['from', '<=', $time],
            ['to', '>=', $time],*/
        ];
        $data = Save::query()->where($query)->find($value);
        if ($data) {
            return true;
        }
        return false;
    }


    public function message(): string
    {
        return _trans('This save 50 is not available or expired');
    }
}
