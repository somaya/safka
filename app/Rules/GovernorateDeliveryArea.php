<?php

namespace App\Rules;

use App\Models\Governorate;
use Illuminate\Contracts\Validation\Rule;


class GovernorateDeliveryArea implements Rule
{
    public $governorateIds;

    public function __construct($governorateIds)
    {
        $this->governorateIds = $governorateIds;
    }

    private function checkRules(): array
    {
        $governorates = $this->governorateIds;
        $data = array_count_values(array_column($governorates, 'governorate_id'));
        return array_filter($governorates, function ($item) use ($data) {
            return $data[$item['governorate_id']] > 1;
        });
    }

    public function passes($attribute, $value): bool
    {
        $items = $this->checkRules();
        if (count($items) > 0) {
            return false;
        }
        return true;
    }


    public function message(): string
    {
        return _trans('This governorate exists with a verb, please delete it or change to another governorate');
    }
}
