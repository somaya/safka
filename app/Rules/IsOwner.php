<?php

namespace App\Rules;

use App\Models\Owner;
use App\Models\ShopStaff;
use Illuminate\Contracts\Validation\Rule;

class IsOwner implements Rule
{

    public function __construct()
    {
        //
    }


    public function passes($attribute, $value): bool
    {
        $owner = Owner::query()->whereHas('user', function ($query) {
            $query->where('status', '=', 1);
        })->findOrFail($value);
        if ($owner && ($owner->user->userable_type == Owner::class || $owner->user->userable_type == ShopStaff::class )){
            return true;
        }
        return false;
    }


    public function message(): string
    {
        return _trans('invalid this owner');
    }
}
