<?php

namespace App\Rules;

use App\Enums\Status;
use App\Models\Owner;
use Illuminate\Contracts\Validation\Rule;

class OwnerAvailability implements Rule
{
    public $owner;

    public function __construct($owner = null)
    {
        $this->owner = $owner;
    }

    public function passes($attribute, $value)
    {
        $owner = Owner::query()->whereRelation('user', 'status', '=', Status::Active->value);
        if ($this->owner) {
            $owner = $owner->where('created_by', '=', $this->owner);
        } else {
            $owner = $owner->whereNull('created_by');
        }
        return (bool)$owner->find($value);
    }


    public function message(): string
    {
        return _trans('Owner name is not valid');
    }
}
