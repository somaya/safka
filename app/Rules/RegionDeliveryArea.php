<?php

namespace App\Rules;

use App\Models\Region;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Arr;


class RegionDeliveryArea implements Rule
{
    public $governorateIds;

    public function __construct($governorateIds)
    {
        $this->governorateIds = $governorateIds;
    }

    public function passes($attribute, $value): bool
    {
        $governorates = $this->governorateIds;
        foreach ($governorates as $value) {
            if (Arr::exists($value, 'region_id')) {
                foreach ($value['region_id'] as $item) {
                    $region = Region::query()->whereGovernorateId($value['governorate_id'])->find($item);
                    if ($region) {
                        return true;
                    }
                    return false;
                }
                return false;
            }
            return false;
        }
        return false;
    }


    public function message(): string
    {
        return _trans('This region does not exist in the province I selected');

    }
}
