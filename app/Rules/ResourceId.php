<?php

namespace App\Rules;

use App\Enums\Status;
use App\Models\Category;
use App\Models\Product;
use App\Models\Ranking;
use App\Models\Furniture;
use App\Models\Shop;
use Illuminate\Contracts\Validation\Rule;

class ResourceId implements Rule
{

    public $resource_type;

    public function __construct($resource_type)
    {
        $this->resource_type = $resource_type;
    }


    public function passes($attribute, $value): bool
    {
        if ($this->resource_type == 'Product') {
            $data = Product::query()->whereStatus(Status::Active->value)->find($value);
            return (bool)$data;
        }

        if ($this->resource_type=='Shop') {
            $data = Shop::query()->whereStatus(Status::Active->value)->find($value);
            return (bool)$data;
        }
        if ($this->resource_type=='Category') {
            $data = Category::query()->whereStatus(Status::Active->value)->find($value);
            return (bool)$data;
        }

        return false;
    }


    public function message(): string
    {
        return _trans('invalid resource ' . strtolower($this->resource_type).' name');
    }
}
