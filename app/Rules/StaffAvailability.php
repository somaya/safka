<?php

namespace App\Rules;

use App\Enums\Status;
use App\Models\Owner;
use Illuminate\Contracts\Validation\Rule;

class StaffAvailability implements Rule
{
    public $owner;

    public function __construct($owner)
    {
        $this->owner = $owner;
    }

    public function passes($attribute, $value)
    {
        $staff = Owner::query()->whereHas('user', function ($query) use ($value) {
            $query->where('userable_type', Owner::class)->where('userable_id', $value);
        })->where('created_by', '=', $this->owner);
        return (bool)$staff->find($value);
    }


    public function message(): string
    {
        return _trans('Staff name is not valid');
    }
}
