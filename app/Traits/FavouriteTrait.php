<?php

namespace App\Traits;

use App\Models\Discount;
use App\Models\Offer;
use App\Models\Product;
use App\Models\Save;
use App\Models\Shop;
use Illuminate\Support\Facades\DB;

trait FavouriteTrait
{
    public function favourite($request, $user)
    {

        if ($request->favourite_type=='product')
        {
            $product=Product::findOrFail($request->favourite_id);
            $is_fav=$product->favouritedBy($user);
            if ($is_fav){
                $product->favourites()->where('user_id', '=', $user->id)->delete();
                return -1;
            }else{
                $product->favourites()->create([
                    'user_id' => $user->id,
                    'favourite_id'=>$request->favourite_id
                ]);
                DB::commit();
                return 1;
            }
        }
        if ($request->favourite_type=='shop')
        {
            $shop=Shop::findOrFail($request->favourite_id);
            $is_fav=$shop->favouritedBy($user);
            if ($is_fav){
                $shop->favourites()->where('user_id', '=', $user->id)->delete();
                return -1;
            }else {
                $shop->favourites()->create([
                    'user_id' => $user->id,
                    'favourite_id' => $request->favourite_id
                ]);
                DB::commit();
                return 1;
            }

        }
        if ($request->favourite_type=='offer')
        {
            $offer=Offer::findOrFail($request->favourite_id);
            $is_fav=$offer->favouritedBy($user);
            if ($is_fav){
                $offer->favourites()->where('user_id', '=', $user->id)->delete();
                return -1;
            }else{
                $offer->favourites()->create([
                    'user_id' => $user->id,
                    'favourite_id'=>$request->favourite_id
                ]);
                DB::commit();
                return 1;
            }
        }
        if ($request->favourite_type=='save')
        {
            $save=Save::findOrFail($request->favourite_id);
            $is_fav=$save->favouritedBy($user);
            if ($is_fav){
                $save->favourites()->where('user_id', '=', $user->id)->delete();
                return -1;
            }else{
                $save->favourites()->create([
                    'user_id' => $user->id,
                    'favourite_id'=>$request->favourite_id
                ]);
                DB::commit();
                return 1;
            }
        }
        if ($request->favourite_type=='discount')
        {
            $discount=Discount::findOrFail($request->favourite_id);
            $is_fav=$discount->favouritedBy($user);
            if ($is_fav){
                $discount->favourites()->where('user_id', '=', $user->id)->delete();
                return -1;
            }else{
                $discount->favourites()->create([
                    'user_id' => $user->id,
                    'favourite_id'=>$request->favourite_id
                ]);
                DB::commit();
                return 1;
            }
        }
        else{
            return false;
        }
    }


}
