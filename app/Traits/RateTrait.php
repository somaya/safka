<?php

namespace App\Traits;

use App\Models\Discount;
use App\Models\Offer;
use App\Models\Product;
use App\Models\Save;
use App\Models\Shop;
use Illuminate\Support\Facades\DB;

trait RateTrait
{
    public function rate($request, $user): bool|int
    {
        if ($request->rate_type == 'product') {
            $product = Product::query()->findOrFail($request->rate_id);
            return $this->store($product, $user, $request);
        }
        if ($request->rate_type == 'shop') {
            $shop = Shop::query()->findOrFail($request->rate_id);
            return $this->store($shop, $user, $request);
        }
        if ($request->rate_type == 'offer') {
            $offer = Offer::query()->findOrFail($request->rate_id);
            return $this->store($offer, $user, $request);
        }
        if ($request->rate_type == 'save') {
            $save = Save::query()->findOrFail($request->rate_id);
            return $this->store($save, $user, $request);
        }
        if ($request->rate_type == 'discount') {
            $discount = Discount::query()->findOrFail($request->rate_id);
            return $this->store($discount, $user, $request);
        } else {
            return false;
        }
    }

    private function store($model, $user, $request): int
    {
        $is_rate = $model->ratedBy($user);
        if ($is_rate) {
            return -1;
        }
        $model->rates()->create([
            'user_id' => $user->id,
            'degree' => $request->degree,
            'comment' => $request->comment,
        ]);
        return 1;

    }


}
