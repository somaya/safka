<?php

namespace App\Traits;


use App\Models\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

trait UploadFileTrait
{
    private function storeFile($file, $data): \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Builder|File
    {
        $size = $file->getSize();
        $main_type = $file->getType();
        $name = $file->getClientOriginalName();
        $hash_name = $file->hashName();
        $full_file = $data['path'] . '/' . $hash_name;

        $file->store($data['path']);
        return File::query()->create([
            'name' => $name,
            'size' => $size,
            'file' => $hash_name,
            'path' => $data['path'],
            'full_file' => $full_file,
            'mime_type' => $main_type,
            'relationable_type' => $data['relationable_type'],
            'relationable_id' => $data['relationable_id'],
        ]);
    }

    public function uploadAsName($data, $nameFileNew)
    {
        if ($data['upload_type'] == 'single') {
            $file = \request()->hasFile($data['file']);
            if ($file) {
                Storage::has($data['delete_file']) ? Storage::delete($data['delete_file']) : '';
                \request()->file($data['file'])->storeAs($data['path'], $nameFileNew);
                return true;
            }
        }
    }

    public function upload($data = [])
    {

        if (\request()->hasFile($data['file']) && $data['upload_type'] == 'single') {
            Storage::has($data['delete_file']) ? Storage::delete($data['delete_file']) : '';
            return \request()->file($data['file'])->store($data['path']);
        } elseif (\request()->hasFile($data['file']) && $data['upload_type'] == 'files' && $data['multi_upload'] == null) {

            $file = \request()->file($data['file']);
            return $this->storeFile($file, $data);
        } elseif (\request()->hasFile($data['file']) && $data['upload_type'] == 'files' && $data['multi_upload'] == true) {
            $files = \request()->file($data['file']);

            foreach ($files as $file) {
                $this->storeFile($file, $data);
            }
        }
    }

    public static function uploadSingleFile($data = [])
    {
        if (\request()->hasFile($data['file']) && $data['upload_type'] == 'single') {
            Storage::has($data['delete_file']) ? Storage::delete($data['delete_file']) : '';
            return \request()->file($data['file'])->store($data['path']);
        }
    }

    public function uploadImageBase64($data = []): string
    {
        $image_64 = $data['file_name']; //your base64 encoded data

        $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1];   // .jpg .png .pdf

        $replace = substr($image_64, 0, strpos($image_64, ',') + 1);

        $image = str_replace($replace, '', $image_64);

        $image = str_replace(' ', '+', $image);

        $imageName = Str::random(10) . '.' . $extension;

        $path = $data['path'] . '/' . $imageName;

        Storage::put($path, base64_decode($image));
        return $path;
    }

    public function deleteFile($path): void
    {
        Storage::has($path) ? Storage::delete($path) : '';
    }

}
