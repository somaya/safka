<?php

namespace Database\Factories;


use App\Factories\Provider\PicsumImage;
use App\Models\BranchType;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class BranchTypeFactory extends Factory
{
    protected $model = BranchType::class;

    public function definition(): array
    {
        $this->faker->addProvider(new PicsumImage($this->faker));
        $imageFile = $this->faker->image();
//        $image = new File($imageFile);

        $locales = locales();
        $data = [
            'status' => $this->faker->randomElement([1]),
            'published' => $this->faker->randomElement([1]),
            'ranking' => random_int(1, 150),
//            'image' => Storage::disk('public')->putFile('branch_type', $image),
        ];

        foreach ($locales as $locale) {
            $data += [
                'name:' . $locale => $this->faker->word . '_' . $locale,
            ];
        }
        return $data;
    }
}
