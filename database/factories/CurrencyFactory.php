<?php

namespace Database\Factories;

use App\Factories\Provider\PicsumImage;
use App\Models\Category;
use App\Models\Currency;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class CurrencyFactory extends Factory
{
    protected $model = Currency::class;

    public function definition(): array
    {
        $locales = locales();
        $data = [
            'status' => $this->faker->randomElement([1]),
            'code' => $this->faker->currencyCode(),
        ];
        foreach ($locales as $locale) {
            $data += [
                'name:' . $locale => $this->faker->word . '_' . $locale,
            ];
        }
        return $data;
    }
}
