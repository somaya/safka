<?php

namespace Database\Factories;

use App\Models\Discount;
use App\Models\Owner;
use App\Models\Product;
use App\Models\Restaurant;
use App\Models\Shop;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;


class DiscountFactory extends Factory
{
    protected $model = Discount::class;

    public function definition(): array
    {
        $date = Carbon::now();
        $owners = Owner::query()->has('shops')->pluck('id')->toArray();
        $data = [
            'owner_id' => $this->faker->randomElement($owners),
            'percent' => random_int(1, 99),
            'start' => $date->addDays(random_int(1, 5))->format('Y-m-d'),
        ];

        $shops = Shop::query()->whereOwnerId($data['owner_id'])->status()->pluck('id')->toArray();
        $data += [
            'shop_id' => $shops->faker->randomElement($shops),
        ];
        $products = Product::query()->whereShopId($data['shop_id'])->status()->pluck('id')->toArray();
        $data += [
            'product_id' => $this->faker->randomElement($products),
        ];
        $product = Product::query()->status()->find($data['product_id']);
        $data += [
            'price_before' => $product->price,
            'price_after' => calculatePercentage($product->price, $data['percent'], 'decrease')['total_amount'],
            'end' => Carbon::parse($data['start'])->addDays($this->faker->randomNumber(1, 20))->format('Y-m-d'),
        ];
        return $data;
    }
}
