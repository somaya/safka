<?php

namespace Database\Factories;

use App\Factories\Provider\PicsumImage;
use App\Models\Category;
use App\Models\Offer;
use App\Models\Product;
use App\Models\Shop;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;


class OfferFactory extends Factory
{
    protected $model = Offer::class;

    public function definition(): array
    {
        $shops = Shop::query()->status()->pluck('id')->toArray();
        $categories = Category::query()->status()->pluck('id')->toArray();
        $this->faker->addProvider(new PicsumImage($this->faker));
        $imageFile = $this->faker->image();
        $image = new File($imageFile);

        $data = [
            'price' => $this->faker->randomDigit(),
            'video' => $this->faker->url(),
            'icon' => Storage::disk('public')->putFile('offer', $image),
            'category_id' => $this->faker->randomElement($categories),
            'shop_id' => $this->faker->randomElement($shops),
            'start' => Carbon::now()->format('Y-m-d'),
            'end' => Carbon::now()->addDays(random_int(1, 5))->format('Y-m-d'),
        ];
        $products = Product::query()->status()->where('shop_id', $data['shop_id'])->pluck('id')->toArray();
        $products = [
            [
                "product_id" => $this->faker->randomElement($products),
                "quantity" => $this->faker->randomElement([2, 3, 4, 5]),
            ], [
                "product_id" => $this->faker->randomElement($products),
                "quantity" => $this->faker->randomElement([2, 3, 4, 5]),
            ], [
                "product_id" => $this->faker->randomElement($products),
                "quantity" => $this->faker->randomElement([2, 3, 4, 5]),
            ],

        ];
        $data += [
            'products' => $products,
        ];
        $locales = locales();
        foreach ($locales as $locale) {
            $data += [
                'name:' . $locale => $this->faker->word . '_' . $locale,
            ];
        }
        return $data;
    }
}
