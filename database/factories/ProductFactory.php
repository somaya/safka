<?php

namespace Database\Factories;

use App\Enums\Status;
use App\Factories\Provider\PicsumImage;
use App\Models\Category;
use App\Models\Owner;
use App\Models\Product;
use App\Models\Shop;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class ProductFactory extends Factory
{
    protected $model = Product::class;

    public function definition(): array
    {
        $owners = Owner::query()->has('shops')->whereRelation('user', 'status', '=', Status::Active->value)->pluck('id')->toArray();
        $categories = Category::query()->has('parent')->status()->pluck('id')->toArray();
        $this->faker->addProvider(new PicsumImage($this->faker));
        $imageFile = $this->faker->image();
        $image = new File($imageFile);
        $data = [
            'category_id' => $this->faker->randomElement($categories),
            'owner_id' => $this->faker->randomElement($owners),
            'price' => $this->faker->randomFloat(2, 20, 300),
            'video' => $this->faker->url(),
            'icon' => Storage::disk('public')->putFile('product', $image),
        ];
        $locales = locales();
        $restaurants = Shop::query()->whereOwnerId($data['owner_id'])->pluck('id')->toArray();
        $subCategories = Category::query()->has('children')->whereParentId($data['category_id'])->pluck('id')->toArray();
        $data += [
            'shop_id' => $this->faker->randomElement($restaurants),
            'sub_category_id' => $this->faker->randomElement($subCategories),
        ];
        foreach ($locales as $locale) {
            $data += [
                'name:' . $locale => $this->faker->word . '_' . $locale,
                'description:' . $locale => $this->faker->sentence . '_' . $locale,
            ];
        }
        return $data;
    }
}
