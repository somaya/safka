<?php

namespace Database\Factories;

use App\Enums\Status;
use App\Factories\Provider\PicsumImage;
use App\Models\Category;
use App\Models\Owner;
use App\Models\Save;
use App\Models\Shop;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class SaveFactory extends Factory
{

    protected $model = Save::class;

    public function definition(): array
    {
        $owners = Owner::query()->whereRelation('user', 'status', '=', Status::Active->value)
            ->whereNull('created_by')->pluck('id')->toArray();
        $categories = Category::query()->has('parent')->status()->pluck('id')->toArray();
        $this->faker->addProvider(new PicsumImage($this->faker));
        $imageFile = $this->faker->image();
        $image = new File($imageFile);
        $data = [
            'price' => $this->faker->randomDigit(),
            'video' => $this->faker->url(),
            'icon' => Storage::disk('public')->putFile('save', $image),
            'category_id' => $this->faker->randomElement($categories),
            'owner_id' => $this->faker->randomElement($owners),
            'start' => Carbon::now()->format('Y-m-d'),
            'end' => Carbon::now()->addDays(random_int(1, 5))->format('Y-m-d'),
            'from' => "10:00",
            'to' => "23:00",
        ];
        $shops = Shop::query()->status()->whereOwnerId($data['owner_id'])->pluck('id')->toArray();

        $data += [
            'shop_id' => $this->faker->randomElement($shops),
        ];
        $locales = locales();
        foreach ($locales as $locale) {
            $data += [
                'name:' . $locale => $this->faker->word . '_' . $locale,
            ];
        }
        return $data;
    }
}
