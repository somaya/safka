<?php

namespace Database\Factories;

use App\Enums\Status;
use App\Factories\Provider\PicsumImage;
use App\Models\BranchType;
use App\Models\Country;
use App\Models\Governorate;
use App\Models\Owner;
use App\Models\Region;
use App\Models\Shop;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class ShopFactory extends Factory
{
    protected $model = Shop::class;

    public function definition(): array
    {
        $owners = Owner::query()->whereNull('created_by')
            ->whereRelation('user', 'status', '=', Status::Active->value)
            ->pluck('id')->toArray();
        $country = Country::query()->status()->pluck('id')->toArray();
        $this->faker->addProvider(new PicsumImage($this->faker));
        $imageFile = $this->faker->image();
//        $image = new File($imageFile);
        $data = [
            'owner_id' => $this->faker->randomElement($owners),
            'country_id' => $this->faker->randomElement($country),
            'address' => $this->faker->word,
//            'logo' => Storage::disk('public')->putFile('shop', $image),
            'phone' => $this->faker->phoneNumber,
            'email' => $this->faker->safeEmail,
            'status' => 1,
            'subscription_type' => $this->faker->randomElement(['participation', 'percent']),
            'subscription_value' => random_int(1, 60),
        ];
        $governorate = Governorate::query()->whereCountryId($data['country_id'])->status()->pluck('id')->toArray();
        $data += [
            'governorate_id' => $this->faker->randomElement($governorate),
        ];
        $regions = Region::query()->whereGovernorateId($data['governorate_id'])->status()->pluck('id')->toArray();
        $data += [
            'region_id' => $this->faker->randomElement($regions),
            'lat' => $this->faker->latitude,
            'lng' => $this->faker->longitude,
        ];
        $locales = locales();
        foreach ($locales as $locale) {
            $data += [
                'name:' . $locale => $this->faker->word . '_' . $locale,
                'description:' . $locale => $this->faker->sentence . '_' . $locale,
            ];
        }
        return $data;
    }
}
