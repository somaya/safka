<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branch_type_translations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('branch_type_id');
            $table->string('name', 100);
            $table->string('slug', 100);
            $table->string('locale')->index();
            $table->unique(['branch_type_id', 'locale']);
            $table->foreign('branch_type_id')->references('id')->on('branch_types')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branch_type_translations');
    }
};
