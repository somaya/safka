<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('week_day_translations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('week_day_id');
            $table->string('name', 100);
            $table->string('slug', 100);
            $table->string('locale')->index();
            $table->unique(['week_day_id', 'locale']);
            $table->foreign('week_day_id')->references('id')->on('week_days')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('week_day_translations');
    }
};
