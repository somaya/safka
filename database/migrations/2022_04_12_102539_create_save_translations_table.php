<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('save_translations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('save_id');
            $table->string('name', 100);
            $table->string('slug', 100);
            $table->string('locale')->index();
            $table->unique(['save_id', 'locale']);
            $table->foreign('save_id')->references('id')->on('saves')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('save_translations');
    }
};
