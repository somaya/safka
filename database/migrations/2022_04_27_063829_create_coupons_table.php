<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->id();
            $table->char('coupon_type', 100)->default('for_all');
            $table->unsignedBigInteger('owner_id')->nullable();
            $table->unsignedBigInteger('shop_id')->nullable();
            $table->string('code');
            $table->string('discount_type')->nullable(); // enum amount or percentage
            $table->float('discount')->default(0.00);
            $table->bigInteger('limit')->default(0);
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->float('min_purchase')->nullable();
            $table->boolean('status')->default(true);
            $table->foreign('owner_id')->references('id')->on('owners')->onDelete('cascade');
            $table->foreign('shop_id')->references('id')->on('shops')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
};
