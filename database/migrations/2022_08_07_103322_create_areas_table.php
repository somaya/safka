<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{

    public function up()
    {
        Schema::create('areas', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('delivery_area_id');
            $table->unsignedBigInteger('governorate_id');
            $table->string('lat')->nullable();
            $table->string('lng')->nullable();
            $table->integer('min_time')->default(0);
            $table->integer('max_time')->default(0);
            $table->boolean('status')->default(true);
            $table->float('delivery_price')->default(0);
            $table->float('min_order_price')->default(0);
            $table->foreign('delivery_area_id')->references('id')->on('delivery_areas')->onDelete('cascade');
            $table->foreign('governorate_id')->references('id')->on('governorates')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('areas');
    }
};
