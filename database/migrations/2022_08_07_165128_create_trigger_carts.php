<?php

use Illuminate\Database\Migrations\Migration;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('CREATE TRIGGER update_total_price_after_insert_cart
        AFTER INSERT ON cart_details
        FOR EACH ROW
        BEGIN
            UPDATE carts
            SET total_price = total_price + ( (NEW.qty * NEW.price) + NEW.total_price_extras)
            WHERE id = NEW.cart_id;
        END');

        DB::unprepared('CREATE TRIGGER update_total_price_after_update_cart
        AFTER UPDATE ON cart_details
        FOR EACH ROW
        BEGIN
            UPDATE carts
            SET total_price = total_price + ( ( (NEW.qty * NEW.price) + NEW.total_price_extras) - ( (OLD.qty * OLD.price) + OLD.total_price_extras) )
            WHERE id = OLD.cart_id;
        END');

        DB::unprepared('CREATE TRIGGER update_total_price_after_delete_cart
        AFTER DELETE ON cart_details
        FOR EACH ROW
        BEGIN
            UPDATE carts
            SET total_price = total_price - (OLD.qty * OLD.price) + OLD.total_price_extras
            WHERE id = OLD.cart_id;
        END');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER "update_total_price_after_insert_cart"');
        DB::unprepared('DROP TRIGGER "update_total_price_after_update_cart"');
        DB::unprepared('DROP TRIGGER "update_total_price_after_delete_cart"');
    }
};
