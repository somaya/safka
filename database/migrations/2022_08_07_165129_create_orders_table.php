<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('customer_id');
            $table->unsignedBigInteger('shop_id');
            $table->unsignedBigInteger('location_id')->nullable();
            $table->unsignedBigInteger('area_id')->nullable();
            $table->json('location_info')->nullable();
            $table->float('delivery_price')->default(0);
            $table->string('coupon_code')->nullable();
            $table->float('coupon_price',14, 2)->default(0);
            $table->float('total_price',14, 2)->default(0);
            $table->string('order_status')->default('pending'); // pending,receive,processing,out_for_delivery,delivered,returned,failed,canceled,completed
            $table->string('status_pay')->default('unpaid'); // unpaid or paid
            $table->string('payment_method')->default('cash'); // cash,visa,vodafone_cash,mobile_balance
            $table->string('code')->nullable();
            $table->string('note')->nullable();
            $table->string('special_order')->nullable();
            $table->string('is_created_invoice')->default(0);


            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
            $table->foreign('shop_id')->references('id')->on('shops')->onDelete('cascade');
            $table->foreign('area_id')->references('id')->on('areas')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
};
