<?php

use Illuminate\Database\Migrations\Migration;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('CREATE TRIGGER update_total_price_after_insert_orders
        AFTER INSERT ON order_details
        FOR EACH ROW
        BEGIN
            UPDATE orders
            SET total_price = total_price + ( (NEW.qty * NEW.price) + NEW.total_price_extras)
            WHERE id = NEW.order_id;
        END');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER "update_total_price_after_insert_orders"');
    }
};
