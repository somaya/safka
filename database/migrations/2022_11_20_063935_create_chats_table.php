<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{

    public function up()
    {
        Schema::create('chats', function (Blueprint $table) {
            $table->id();
            $table->foreignId('conversation_id')->constrained('_conversations')->cascadeOnUpdate()->cascadeOnDelete();
            $table->morphs('modelable');
            $table->text('message');
            $table->boolean('seen')->default(0);
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('chats');
    }
};
