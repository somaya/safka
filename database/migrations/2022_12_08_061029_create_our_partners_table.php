<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::create('our_partners', function (Blueprint $table) {
            $table->id();
            $table->string('logo');
            $table->string('link')->nullable();
            $table->float('ranking')->default(0);
            $table->boolean('status')->default(1);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('our_partners');
    }
};
