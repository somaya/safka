<?php

namespace Database\Seeders;

use App\Helpers\CPU\Models;
use App\Models\BranchType;
use App\Models\Category;
use App\Models\Shop;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Crypt;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call([
            LanguageSeeder::class,
            CountrySeeder::class,
            RolePermissionSeeder::class,
            WeekDaySeeder::class,
//            OwnerSeeder::class,
        ]);
//        BranchType::factory(1)->create();
//        Category::factory(1)->create();
//        Shop::factory(5)->create();
//        $shops = Shop::all();
//        if ($shops->count() > 0) {
//            foreach ($shops as $shop) {
//                $encrypt = Crypt::encryptString($shop->id);
//                Models::qrCodeGenerator($encrypt, 'shop/shop_id_' . $shop['id'] . '.png');
//                $shop->update(['qr_code' => 'shop/shop_id_' . $shop['id'] . '.png']);
//            }
//        }
//        Category::factory(10)->create();

    }
}
