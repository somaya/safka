<?php

namespace Database\Seeders;

use App\Models\Country;
use App\Models\Governorate;
use App\Models\Owner;
use App\Models\Region;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Spatie\Permission\Models\Role;

class OwnerSeeder extends Seeder
{

    public function run()
    {
        $country = Country::query()->status()->pluck('id')->toArray();
        $role = Role::findByName('owner', 'web');
        $data = [
            'country_id' => Arr::random($country),
        ];
        $governorate = Governorate::query()->whereCountryId($data['country_id'])->status()->pluck('id')->toArray();
        $data += [
            'governorate_id' => Arr::random($governorate),
        ];
        $regions = Region::query()->whereGovernorateId($data['governorate_id'])->status()->pluck('id')->toArray();
        $data += [
            'region_id' => Arr::random($regions),
        ];
        $owner1 = Owner::query()->create([
            'address' => 'Str cairo',
            'brand_name' => 'Main Club',
            'phone' => rand(1, 2555),
        ]);
        $user1 = $owner1->user()->create(array_merge($data, [
            'role_id' => $role->id,
            'name' => 'Ahmed Ali',
            'email' => 'admin1@gmail.com',
            'password' => '123456789',
            'status' => true,
        ]));
        $user1->assignRole($role);

        $owner2 = Owner::query()->create([
            'address' => 'Str fayoum',
            'brand_name' => 'Man Cloth ',
            'phone' => rand(1, 2555),
        ]);
        $user2 = $owner2->user()->create(array_merge($data, [
            'role_id' => $role->id,
            'name' => 'Said Ali',
            'email' => 'admin2@gmail.com',
            'password' => '123456789',
            'status' => true,
        ]));
        $user2->assignRole($role);
    }
}
