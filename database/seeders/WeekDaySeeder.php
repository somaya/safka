<?php

namespace Database\Seeders;

use App\Models\WeekDay;
use Illuminate\Database\Seeder;

class WeekDaySeeder extends Seeder
{

    private $days;

    public function __construct()
    {
        $jsonString = file_get_contents(__DIR__ . '/week_days/data.json');
        $days = json_decode($jsonString, true);
        $this->days = $days;
    }


    public function run()
    {
        $locales = locales();
        $weeks = [];
        foreach ($this->days as $key => $day) {
            $weeks[$key] = [
                'code' => $key + 1
            ];
            foreach ($locales as $locale) {
                $weeks[$key] += [
                    'name:' . $locale => $day['name_' . $locale],
                ];
            }
            WeekDay::query()->updateOrCreate([
                'code' => $weeks[$key]['code']
            ], $weeks[$key]);
        }


    }
}
