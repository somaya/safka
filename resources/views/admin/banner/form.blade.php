@extends('layouts.master')

@section('title',_trans('Banners'))


@section('content')
    <div class="container-fluid">
        <div class="page-title">
            <div class="row">
                <div class="col-6 col-sm-6">
                    <h3>{{ $edit ? _trans('Edit Banner') : _trans('Add Banner') }}</h3>
                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid default-dash">
        <div class="row">
            <div class="col-12 col-sm-6">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/') }}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                        </a>
                    </li>
                    @canany(['Banner list','Banner edit','Banner delete'])
                        <li class="breadcrumb-item ">
                            <a href="{{ route('admin.banner.index') }}">{{ _trans('Banners') }}</a>
                        </li>
                    @endcanany

                    <li class="breadcrumb-item active">
                        {{ $edit ? _trans('Edit Banner') : _trans('Add Banner') }}
                    </li>
                </ol>
            </div>

            <div class="col-xl-12 col-md-12 ">
                @if ($edit)
                    {{ Form::open(['route' => ['admin.banner.update',$banner->id],'method' => 'PUT','files' => true,'class' => 'form mb-15 form-submit','id' =>'kt_contact_form']) }}
                    {{ Form::hidden('id',$banner->id) }}
                @else
                    {{ Form::open(['route' => 'admin.banner.store','method' => 'POST','files' => true,'class' => 'form mb-15','id' =>'kt_contact_form']) }}
                @endif

                <div class="row g-3">
                    <div class="col-md-6">
                        <label id="code" class="form-label" for="">{{ _trans('Banner Code') }}</label>
                        <input id="code"
                               disabled
                               type="text"
                               class="form-control"
                               placeholder="{{ _trans('Banner Code') }}"
                               value="{{ getCodeTable('Bann','banners',$edit,$edit ? $banner->id : null) }}"
                        >
                    </div>

                    <div class="col-md-6">
                        <div class="form-group form-image">
                            <label for="link" class="font-md">{{ _trans('Link') }}</label>
                            <input id="link"
                                   type="url"
                                   name="link"
                                   placeholder="{{ _trans('Link') }}"
                                   value="{{ old('link',$edit ? $banner->link : null) }}"
                                   class="form-control @error('link') is-invalid @enderror">
                            @error('link')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>
                    </div>

                    @foreach($languages as $lang)
                        <div class="col-md-6">
                            <label id="{{ $lang['code'] }}[content]" class="form-label" for="">{{ _trans('Content') }} ({{ ucfirst($lang['code']) }})</label>
                            <textarea id="{{ $lang['code'] }}[content]"
                                      type="text"
                                      class="form-control @error($lang['code'].'.content') is-invalid @enderror "
                                      name="{{ $lang['code'] }}[content]"
                                      rows="5">{{ old($lang['code'].'.content',$edit ? $banner->translate($lang['code'])?->content : null) }}</textarea>

                            @error($lang['code'].'.content')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>
                    @endforeach

{{--                    <div class="col-md-6">--}}
{{--                        <label  class="form-label" for="">{{ _trans('Resource Type') }}</label>--}}
{{--                        <select id="resource_type" name="resource_type"--}}
{{--                                class="js-example-basic-single @error('resource_type') is-invalid @enderror" >--}}
{{--                            <option value="">{{ _trans('Select Resource Type') }}</option>--}}
{{--                            <option value="Home"  @selected(old('resource_type',$edit ? $banner->resource_type : null) == 'Home')>{{ _trans('Home') }}</option>--}}
{{--                            <option value="Product"  @selected(old('resource_type',$edit ? $banner->resource_type : null) == 'Product')>{{ _trans('Product') }}</option>--}}
{{--                            <option value="Category"  @selected(old('resource_type',$edit ? $banner->resource_type : null) == 'Category')>{{ _trans('Category') }}</option>--}}
{{--                            <option value="Shop"  @selected(old('resource_type',$edit ? $banner->resource_type : null) == 'Shop')>{{ _trans('Shop') }}</option>--}}
{{--                        </select>--}}
{{--                        @error('resource_type')--}}
{{--                        <span class="text-danger">{!! $message !!} </span>--}}
{{--                        @enderror--}}
{{--                    </div>--}}
{{--                    <div class="col-md-6">--}}
{{--                        <label id="resource_id_lable" class="form-label" for="">{{ _trans('Product name') }}</label>--}}
{{--                        <select id="resource_id" name="resource_id"--}}
{{--                                class="js-example-basic-single @error('resource_id') is-invalid @enderror" >--}}
{{--                            <option value="">{{ _trans('Select Product') }}</option>--}}

{{--                        </select>--}}
{{--                        @error('resource_id')--}}
{{--                        <span class="text-danger">{!! $message !!} </span>--}}
{{--                        @enderror--}}
{{--                    </div>--}}
{{--                    <div class="col-md-6">--}}
{{--                        <label  class="form-label" for="">{{ _trans('Banner Type') }}</label>--}}
{{--                        <select id="banner_type" name="banner_type"--}}
{{--                                class="js-example-basic-single @error('banner_type') is-invalid @enderror" required="">--}}
{{--                            <option value="">{{ _trans('Select Banner Type') }}</option>--}}
{{--                            <option value="Main"  @selected(old('banner_type',$edit ? $banner->banner_type : null) == 'Main')>{{ _trans('Main') }}</option>--}}
{{--                            <option value="Footer"  @selected(old('banner_type',$edit ? $banner->banner_type : null) == 'Footer')>{{ _trans('Footer') }}</option>--}}
{{--                            <option value="Popup"  @selected(old('banner_type',$edit ? $banner->banner_type : null) == 'Popup')>{{ _trans('Popup') }}</option>--}}
{{--                        </select>--}}
{{--                        @error('banner_type')--}}
{{--                        <span class="text-danger">{!! $message !!} </span>--}}
{{--                        @enderror--}}
{{--                    </div>--}}

                    <div class="col-md-6">
                        <div class="form-group form-image">
                            <label class="font-md">{{ _trans('Image') }}</label>
                            <img src="{{ $edit ? getAvatar($banner->image) : asset('assets/images/no-image.png') }}"
                                 class="mb-2 image-preview">
                            <input id="image"
                                   type="file"
                                   name="image"
                                   class="form-control image @error('image') is-invalid @enderror">
                            @error('image')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>
                    </div>


                    <div class="col-md-12">
                        <div class="m-t-50 d-flex justify-content-end">
                            <button type="submit" class="btn btn-primary">{{ $edit ?_trans('Update') : _trans('Save') }}</button>
                        </div>
                    </div>


                </div>

                {{ Form::close() }}
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->

@endsection

@include('layouts.partials.read-photo',['inputName' => 'image'])
@include('layouts.partials.ckeditor',['inputClass' => 'textarea'])
{{--@push('scripts')--}}
{{--    <script>--}}
{{--        $(document).ready(function () {--}}
{{--            @if(!is_null(old('resource_type')))--}}
{{--            var resource_type=$('#resource_type').val();--}}
{{--            getResourceData(resource_type, {{ !is_null(old('resource_id')) ? old('resource_id') : 0 }})--}}
{{--            @endif--}}
{{--            @if($edit)--}}
{{--                 var resource_type=$('#resource_type').val();--}}

{{--                    if (resource_type && resource_type!='Home'){--}}
{{--                    getResourceData(resource_type, {{ old('resource_id',$edit ? $banner->resource_id : 0) }})--}}
{{--                }--}}
{{--            @endif--}}
{{--        })--}}


{{--    </script>--}}
{{--    @include('layouts.ajax.banner')--}}

{{--@endpush--}}
