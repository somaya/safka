@if (count($data) > 0)
    <div class="repeater-default">
        <div data-repeater-list="tags">
            @foreach($data as $key => $value)
                <div data-repeater-item="" style="">
                    <div class="row">
                        @foreach($languages as $lang)

                            <div class="col-md-3">
                                <label class="form-label" for="name:{{ $lang['code'] }}">{{ _trans('Tag') }} ({{ ucfirst($lang['code']) }})</label>
                                <input type="text"
                                       name="tags[{{ $key }}][name:{{ $lang['code'] }}]"
                                       value="{{ $value['name:'.$lang['code'] ] }}"
                                       class="form-control @error('tags.'.$key.'.name:'.$lang['code'] ) is-invalid @enderror"
                                       id="from"
                                />
                                @error('tags.'.$key.'.name:'.$lang['code'] )
                                <span class="text-danger">{!! $message !!} </span>
                                @enderror

                            </div>
                        @endforeach
                        <div class="col-md-1">
                            <div class="form-group col-sm-12 col-md-2 text-center mt-2">
                                <br>
                                <button type="button" id="deleteRepeater" class="btn btn-danger" data-repeater-delete="">
                                    <i class="fas fa-trash-alt"></i> {{ _trans('Delete') }}
                                </button>

                            </div>
                        </div>

                    </div>
                    <br>
                </div>
            @endforeach
        </div>
        <div class="form-group overflow-hidden">
            <div class="col-12">
                <button type="button" id="addRepeater" data-repeater-create="" class="btn btn-primary">
                    <i class="far fa-plus-square"></i> {{ _trans('Add') }}
                </button>
            </div>
        </div>
    </div>

@else
    <div class="repeater-default">
        <div data-repeater-list="tags">
            <div data-repeater-item="" style="">
                <div class="row">
                    @foreach($languages as $lang)
                        <div class="col-md-4">
                            <label class="form-label" for="name:{{ $lang['code'] }}">{{ _trans('Tag') }} ({{ ucfirst($lang['code']) }})</label>
                            <input type="text"
                                   name="name:{{ $lang['code'] }}"
                                   id="name:{{ $lang['code'] }}"
                                   class="form-control">
                        </div>
                    @endforeach


                    <div class="col-md-1">
                        <div class="form-group col-sm-12 col-md-2 text-center mt-2">
                            <br>
                            <button type="button" id="deleteRepeater" class="btn btn-danger" data-repeater-delete="">
                                {{--<i class="fas fa-trash-alt"></i>--}} {{ _trans('Delete') }}
                            </button>
                        </div>
                    </div>
                </div>
                <br>
            </div>
        </div>
        <div class="form-group overflow-hidden">
            <div class="col-12">
                <button type="button" id="addRepeater" data-repeater-create="" class="btn btn-primary">
                    {{--<i class="far fa-plus-square"></i> --}} {{ _trans('Add') }}
                </button>
            </div>
        </div>
    </div>
@endif


@push('js')
    <script>
        var limit = 1;
        $(document).ready(function () {
            limit = {{ count($data) == 0 ? 1 : count($data)  }};
            limitRepeater();
        })
        $(document).on('click', '#addRepeater', function () {
            limit++;
            if (limit > 20) {
                $('#addRepeater').attr('disabled', true)
            }
        })
        $(document).on('click', '#deleteRepeater', function () {
            limit--;
            limitRepeater();
        })

        function limitRepeater() {
            if (limit > 20) {
                $('#addRepeater').attr('disabled', true)
            } else {
                $('#addRepeater').attr('disabled', false)
            }
        }

    </script>
@endpush
