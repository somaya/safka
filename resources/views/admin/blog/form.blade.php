@extends('layouts.master')
@section('title',_trans('Blog'))
@section('content')
    <div class="container-fluid">
        <div class="page-title">
            <div class="row">
                <div class="col-6 col-sm-6">
                    <h3>{{ $edit ? _trans('Edit Blog') : _trans('Add Blog') }}</h3>
                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid default-dash">
        <div class="row">

            <div class="row m-t-20">
                <div class="col-12 col-sm-6">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-home">
                                    <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                    <polyline points="9 22 9 12 15 12 15 22"></polyline>
                                </svg>
                            </a></li>
                        <li class="breadcrumb-item">
                            @canany(['Blog list','Blog edit','Blog delete'])
                                <a href="{{route('admin.blogs.index')}}">{{ _trans('blogs') }}</a>
                            @endcanany
                        </li>
                        <li class="breadcrumb-item active">{{ $edit ? _trans('Edit Blog') : _trans('Add Blog') }}</li>
                    </ol>
                </div>
                <div class="col-md-12">
                    @if ($edit)
                        {{ Form::open(['route' => ['admin.blogs.update',$blog->id],'method' => 'PUT','files' => true,'class' => 'form mb-15 form-submit','id' =>'kt_contact_form']) }}
                        {{ Form::hidden('id',$blog->id) }}
                    @else
                        {{ Form::open(['route' => 'admin.blogs.store','method' => 'POST','files' => true,'class' => 'form mb-15','id' =>'kt_contact_form']) }}
                    @endif

                    <div class="row g-3">
                        <div class="col-md-6">
                            <label id="code" class="form-label" for="">{{ _trans('Code') }}</label>
                            <input id="code"
                                   disabled
                                   type="text"
                                   class="form-control"
                                   placeholder="{{ _trans('Code') }}"
                                   value="{{ getCodeTable('Blog','blogs',$edit,$edit ? $blog->id : null) }}"
                            >
                        </div>
                        <div class="col-md-6">
                            <label class="form-label" for="">{{ _trans('Blog Category') }}</label>
                            <select id="category_id" name="category_id"
                                    class="js-example-basic-single @error('category_id') is-invalid @enderror"
                                    required="">
                                <option value="">{{ _trans('Select Blog Category') }}</option>
                                @foreach($categories as $category)
                                    <option
                                        value="{{ $category->id }}" @selected(old('category_id',$edit ? $blog->category_id : null) == $category->id)>{{ $category->translate(locale())?->name }}</option>
                                @endforeach
                            </select>
                            @error('category_id')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>


                        @foreach($languages as $lang)
                            <div class="col-md-6">
                                <label id="{{ $lang['code'] }}[title]" class="form-label"
                                       for="">{{ _trans('Blog').' '._trans('Title') }} ({{ ucfirst($lang['code']) }}
                                    )</label>
                                <input id="{{ $lang['code'] }}[title]"
                                       type="text"
                                       name="{{ $lang['code'] }}[title]"
                                       class="form-control @error($lang['code'].'.title') is-invalid @enderror"
                                       placeholder="{{ _trans('Blog').' '._trans('Title') }}"
                                       value="{{ old($lang['code'].'.title',$edit ? $blog->translate($lang['code'])?->title : null) }}"
                                       required="">
                                @error($lang['code'].'.title')
                                <span class="text-danger">{!! $message !!} </span>
                                @enderror
                            </div>
                        @endforeach
                        @foreach($languages as $lang)
                            <div class="col-md-6">
                                <label class="form-label" for="{{ $lang['code'] }}[content]">{{ _trans('Content') }}
                                    ({{ ucfirst($lang['code']) }})</label>
                                <textarea id="{{ $lang['code'] }}[content]"
                                          type="text"
                                          class="form-control @error($lang['code'].'.content') is-invalid @enderror textarea"
                                          name="{{ $lang['code'] }}[content]"
                                          required=""
                                          rows="5">{{ old($lang['code'].'.content',$edit ? $blog->translate($lang['code'])?->content : null) }}</textarea>

                                @error($lang['code'].'.content')
                                <span class="text-danger">{!! $message !!} </span>
                                @enderror
                            </div>
                        @endforeach

                        <div class="col-md-6">
                            <div class="form-group form-image">
                                <label class="font-md">{{ _trans('Default Image') }}</label>
                                <img
                                    src="{{ $edit ? getAvatar($blog->default_image) : asset('assets/images/no-image.png') }}"
                                    class="mb-2 default_image-preview">
                                <input id="default_image"
                                       type="file"
                                       name="default_image"
                                       class="form-control default_image @error('icon') is-invalid @enderror">
                                @error('default_image')
                                <span class="text-danger">{!! $message !!} </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-image mb-3">
                                <label class="font-md">{{_trans('Add Photos')}}</label>
                                @if($edit)
                                    <div class="upload_imgs">
                                        @foreach($blog->images as $image)
                                            <div class="single">
                                                <img class="mb-2" src="{{getAvatar($image->full_file) }}">
                                                <div class="icon"><i class="fa fa-times delete-img" id="{{$image->id}}"
                                                                     aria-hidden="true"></i></div>
                                            </div>
                                        @endforeach

                                    </div>
                                @endif
                                <input class="form-control @error('image.*') is-invalid @enderror" id="images.*"
                                       type="file" name="images[]" multiple>
                            </div>

                        </div>
                        <div class="col-md-12">
                            <h5>{{ _trans('Tags') }}</h5>
                            <hr>
                            @include('admin.blog.form-repeater',['data' => $edit ? old('tags',$blog->tags ?:[]) :  (!empty(old('tags')) ? (old('tags')) : []) ])

                        </div>

                    </div>
                </div>

                <div class="col-md-12">
                    <div class="m-t-50 d-flex justify-content-end">
                        <button type="submit"
                                class="btn btn-primary">{{ $edit ?_trans('Update') : _trans('Save') }}</button>
                    </div>
                </div>
                {{ Form::close() }}

            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection
@include('layouts.partials.read-photo',['inputName' => 'default_image'])
@include('layouts.partials.ckeditor',['inputClass' => 'textarea'])
@push('scripts')
    @include('layouts.ajax.delete-product-images')

    @include('layouts.ajax.countries')
    <script src="{{ asset('assets/repeater/jquery.repeater.min.js') }}"></script>
    <script src="{{ asset('assets/repeater/form-repeater.min.js') }}"></script>
@endpush


