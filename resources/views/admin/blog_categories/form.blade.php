@extends('layouts.master')

@section('title',_trans('Blog Categories'))


@section('content')
    <div class="container-fluid">
        <div class="page-title">
            <div class="row">
                <div class="col-6 col-sm-6">
                    <h3>{{ $edit ? _trans('Edit Blog Category') : _trans('Add Blog Category') }}</h3>
                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid default-dash">
        <div class="row">
            <div class="col-12 col-sm-6">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                        </a>
                    </li>
                    @canany(['Blog Category list','Blog Category edit','Blog Category delete'])
                        <li class="breadcrumb-item ">
                            <a href="{{ route('admin.blog-category.index') }}">{{ _trans('Blog Categories') }}</a>
                        </li>
                    @endcanany

                    <li class="breadcrumb-item active">
                        {{ $edit ? _trans('Edit Blog Category') : _trans('Add Blog Category') }}
                    </li>
                </ol>
            </div>

            <div class="col-xl-12 col-md-12 ">
                @if ($edit)
                    {{ Form::open(['route' => ['admin.blog-category.update',$blog_category->id],'method' => 'PUT','files' => true,'class' => 'form mb-15 form-submit','id' =>'kt_contact_form']) }}
                    {{ Form::hidden('id',$blog_category->id) }}
                @else
                    {{ Form::open(['route' => 'admin.blog-category.store','method' => 'POST','files' => true,'class' => 'form mb-15','id' =>'kt_contact_form']) }}
                @endif

                <div class="row g-3">
                    <div class="col-md-6">
                        <label id="code" class="form-label" for="">{{ _trans('Category Code') }}</label>
                        <input id="code"
                               disabled
                               type="text"
                               class="form-control"
                               placeholder="{{ _trans('Category Code') }}"
                               value="{{ getCodeTable('BlogCat','blog_categories',$edit,$edit ? $blog_category->id : null) }}"
                        >
                    </div>
                    <div>

                    </div>

                    @foreach($languages as $lang)
                        <div class="col-md-6">
                            <label id="{{ $lang['code'] }}[name]" class="form-label" for="">{{ _trans('Name').' '._trans('Category') }} ({{ ucfirst($lang['code']) }})</label>
                            <input id="{{ $lang['code'] }}[name]"
                                   type="text"
                                   name="{{ $lang['code'] }}[name]"
                                   class="form-control @error($lang['code'].'.name') is-invalid @enderror"
                                   placeholder="{{ _trans('Name').' '._trans('Category') }}"
                                   value="{{ old($lang['code'].'.name',$edit ? $blog_category->translate($lang['code'])?->name : null) }}"
                                   required="">
                            @error($lang['code'].'.name')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>
                    @endforeach

                    <div class="col-md-6">
                        <div class="form-group form-image">
                            <label class="font-md">{{ _trans('Image') }}</label>
                            <img src="{{ $edit ? getAvatar($blog_category->image) : asset('assets/images/no-image.png') }}"
                                 class="mb-2 image-preview">
                            <input id="image"
                                   type="file"
                                   name="image"
                                   class="form-control image @error('image') is-invalid @enderror">
                            @error('image')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>
                    </div>


                    <div class="col-md-12">
                        <div class="m-t-50 d-flex justify-content-end">
                            <button type="submit" class="btn btn-primary">{{ $edit ?_trans('Update') : _trans('Save') }}</button>
                        </div>
                    </div>


                </div>

                {{ Form::close() }}
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection
@include('layouts.partials.read-photo',['inputName' => 'image'])
