<div class="answer {{ $message->user_id == auth()->user()->id ? 'left' : 'right' }}">
    <div class="text">{{ $message->body }}</div>
    <div class="time">{{ $message->created_at->format('d-m-Y') }}</div>
</div>
