@extends('layouts.master')
@section('title',$edit ? _trans('Edit Company') : _trans('Add Company'))
@section('content')
    <div class="container-fluid">
        <div class="page-title">
            <div class="row">
                <div class="col-6 col-sm-6">
                    <h3>{{ $edit ? _trans('Edit Company') : _trans('Add Company') }}</h3>
                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid default-dash">
        <div class="row">
            <div class="col-12 col-sm-6">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/') }}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                        </a>
                    </li>
                    @canany(['Company list','Company edit','Company delete'])
                        <li class="breadcrumb-item ">
                            <a href="{{ route('admin.company.index') }}">{{ _trans('Companies') }}</a>
                        </li>
                    @endcanany

                    <li class="breadcrumb-item active">
                        {{ $edit ? _trans('Edit Company') : _trans('Add Company') }}
                    </li>
                </ol>
            </div>

            <div class="col-xl-12 col-md-12 ">
                @if ($edit)
                    {{ Form::open(['route' => ['admin.company.update',$company->id],'method' => 'PUT','files' => true,'class' => 'form mb-15 form-submit','id' =>'kt_contact_form']) }}
                    {{ Form::hidden('id',$company->id) }}
                @else
                    {{ Form::open(['route' => 'admin.company.store','method' => 'POST','files' => true,'class' => 'form mb-15','id' =>'kt_contact_form']) }}
                @endif

                <div class="row g-3">
                    <div class="col-md-6">
                        <label class="form-label" for="code">{{ _trans('Company Code') }}</label>
                        <input id="code"
                               disabled
                               type="text"
                               class="form-control"
                               placeholder="{{ _trans('Company Code') }}"
                               value="{{ getCodeTable('Company','companies',$edit,$edit ? $company->id : null) }}"
                        >
                    </div>

                    <div class="col-md-6">
                        <label for="ranking" class="form-label">{{ _trans('Company ranking') }}</label>
                        <input id="ranking"
                               name="ranking"
                               type="number"
                               step="1"
                               min="1"
                               class="form-control @error('ranking') is-invalid @enderror"
                               placeholder="{{ _trans('Company ranking') }}"
                               value="{{ old('ranking',$edit ? $company->ranking : null) }}"
                        >
                        @error('ranking')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>
                    <div class="col-md-6">
                        <div class="form-group form-image">
                            <label for="link" class="font-md">{{ _trans('Link') }}</label>
                            <input id="link"
                                   type="url"
                                   name="link"
                                   placeholder="{{ _trans('Link') }}"
                                   value="{{ old('link',$edit ? $company->link : null) }}"
                                   class="form-control @error('link') is-invalid @enderror">
                            @error('link')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="mb-1">
                            <label class="form-label">{{ _trans('Logo') }}</label>
                        </div>
                        <div class="p-2 border border-dashed" style="max-width:230px;">
                            <div class="row" id="logo"></div>
                        </div>
                        @error('logo')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>


                    <div class="col-md-12">
                        <div class="m-t-50 d-flex justify-content-end">
                            <button type="submit" class="btn btn-primary">{{ $edit ?_trans('Update') : _trans('Save') }}</button>
                        </div>
                    </div>


                </div>

                {{ Form::close() }}
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection
@push('scripts')
    <script src="{{ asset('assets/js/spartan-multi-image-picker.js') }}"></script>
    @include('layouts.partials.spartan-multi-image',[
    'single' => true,
    'file_name' => 'logo',
    'image' => $edit ? getAvatar($company->logo) :  asset('assets/images/img/400x400/img2.jpg')
    ])

@endpush
