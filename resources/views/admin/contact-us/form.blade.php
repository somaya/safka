@extends('layouts.master')

@section('title',_trans('Contact us'))


@section('content')
    <div class="container-fluid">
        <div class="page-title">
            <div class="row">
                <div class="col-6 col-sm-6">
                    <h3>{{ $edit ? _trans('Reply') : _trans('Send') }}</h3>
                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid default-dash">
        <div class="row">
            <div class="col-12 col-sm-6">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                        </a>
                    </li>
                    @canany(['ContactUs list','ContactUs edit','ContactUs delete'])
                        <li class="breadcrumb-item ">
                            <a href="{{ route('admin.contact-us.index') }}">{{ _trans('Contact us') }}</a>
                        </li>
                    @endcanany
                    <li class="breadcrumb-item active">
                        {{ $edit ? _trans('Reply') : _trans('Send') }}
                    </li>
                </ol>
            </div>
            <div class="col-md-12">
                @if ($edit)
                    {{ Form::open(['route' => ['admin.contact-us.update',$contact->id],'method' => 'PUT','files' => true,'class' => 'form mb-15 form-submit','id' =>'kt_contact_form']) }}
                    {{ Form::hidden('id',$contact->id) }}
                @else
                    {{ Form::open(['route' => 'admin.contact-us.store','method' => 'POST','files' => true,'class' => 'form mb-15','id' =>'kt_contact_form']) }}
                @endif

                <div class="row g-3">
                    <div class="col-md-4">
                        <label id="name" class="form-label" for="">{{ _trans('Full name') }}</label>
                        <input id="name"
                               type="text"
                               name="name"
                               {{ $edit ? 'disabled' : '' }}
                               class="form-control @error('name') is-invalid @enderror"
                               placeholder="{{ _trans('Full name') }}"
                               value="{{ old('name',$edit ? $contact->full_name : null) }}"
                               required="">
                        @error('name')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label id="email" class="form-label" for="">{{ _trans('E-mail') }}</label>
                        <input id="email"
                               type="email"
                               {{ $edit ? 'disabled' : '' }}
                               name="email"
                               class="form-control @error('email') is-invalid @enderror"
                               placeholder="{{ _trans('E-mail') }}"
                               value="{{ old('email',$edit ? $contact->email : null) }}"
                               required="">
                        @error('email')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label id="phone" class="form-label" for="">{{ _trans('Phone') }}</label>
                        <input id="phone"
                               type="text"
                               {{ $edit ? 'disabled' : '' }}
                               name="phone"
                               class="form-control @error('phone') is-invalid @enderror"
                               placeholder="{{ _trans('Phone') }}"
                               value="{{ old('phone',$edit ? $contact->phone : null) }}"
                               required="">
                        @error('phone')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>
                    <div class="col-md-4">
                        <label id="subject" class="form-label" for="">{{ _trans('Subject') }}</label>
                        <input id="subject"
                               type="text"
                               name="subject"
                               {{ $edit ? 'disabled' : '' }}
                               class="form-control @error('subject') is-invalid @enderror"
                               placeholder="{{ _trans('Subject') }}"
                               value="{{ old('subject',$edit ? $contact->subject : null) }}"
                               required="">
                        @error('subject')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    @if($edit)
                        <div class="col-md-12">
                            <label class="form-label" for="">{{ _trans('Message received') }}</label>
                            <textarea cols="3"
                                      rows="3"
                                      class="form-control "
                                      disabled
                            >{{ $contact->message ?? _trans('Not send message yet') }}</textarea>
                            @error('message')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>
                    @endif

                    <div class="col-md-12">
                        <label for="message" class="form-label" for="">{{ _trans('Send message') }}</label>
                        <textarea id="message"
                                  cols="3"
                                  rows="3"
                                  name="message"
                                  class="form-control @error('message') is-invalid @enderror "
                                  placeholder="{{ _trans('Message') }}"
                        >{{ old('message') }}</textarea>
                        @error('message')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-12">
                        <div class="m-t-50 d-flex justify-content-end">
                            <button type="submit" class="btn btn-primary">{{ $edit ?_trans('Reply') : _trans('Send') }}</button>
                        </div>
                    </div>


                </div>

                {{ Form::close() }}
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection
@include('layouts.partials.ckeditor',['inputClass' => 'textarea'])

