@extends('layouts.master')
@section('title',_trans('Contact us'))
@section('content')
    <div class="container-fluid">
        <div class="page-title">
            <div class="row">
                <div class="col-6 col-sm-6">
                    <h3>{{_trans('Contact us')}}</h3>
                </div>
{{--                @can('ContactUs add')--}}
{{--                    <div class="col-6 text-right">--}}
{{--                        <a href="{{ route('admin.contact-us.create') }}" class="btn btn-primary">--}}
{{--                            <i class="fa fa-send" aria-hidden="true"></i>--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                @endcan--}}

            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid default-dash">
        <div class="row">
            <div class="col-12 col-sm-6">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/') }}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                        </a></li>
                    <li class="breadcrumb-item active"><a href="#">{{ _trans('Contact us') }}</a></li>
                </ol>
            </div>
            <div class="col-xl-12 col-md-12 ">
                <form action="{{ url()->current() }}" method="GET">
                    <div class="left-side-header">
                        <div class="row justify-content-between align-items-center">
                            <x-search :columns="$columns"/>
                            <div class="col-md-4">
                                <p class="mb-0 font-sm d-flex align-items-center justify-content-end">{{ _trans('Count Contact us') }} :
                                    <span class="d-block font-md text-danger">({{ $data->total() }})</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="table-responsive custom-scrollbar p-t-30">
                    <table class="table">
                        <thead>
                        <tr>
                            <th><span>{{ _trans('Code')}}  </span></th>
                            <th><span>{{ _trans('Full name')}} </span></th>
                            <th><span>{{ _trans('Phone')}} </span></th>
                            <th><span>{{ _trans('E-mail')}} </span></th>
                            <th><span>{{ _trans('Subject')}} </span></th>
                            <th><span>{{ _trans('Message')}} </span></th>
                            <th><span>{{ _trans('action')}}</span></th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $index => $item)
                            <tr>
                                <td class="text-main">
                                    <a href="{{ route('admin.contact-us.edit',$item->id) }}">ContactID#{{$item->id}}</a>
                                </td>
                                <td>{{$item->full_name}}</td>
                                <td>{{$item->phone}}</td>
                                <td>
                                    <a href="{{ route('admin.contact-us.edit',$item->id) }}">{{$item->email}}</a>
                                </td>
                                <td>{{Str::limit(strip_tags($item->message),15)}}</td>
                                <td>{{Str::limit(strip_tags($item->subject),15)}}</td>
                                <td>
{{--                                    @can('ContactUs edit')--}}
{{--                                        <a href="{{ route('admin.contact-us.edit',$item->id) }}" class="btn btn-primary btn-sm">--}}
{{--                                            <i class="fa fa-reply" aria-hidden="true"></i>--}}
{{--                                        </a>--}}
{{--                                    @endcan--}}
                                    @can('ContactUs edit')
                                        <a onclick="return confirm('{{ _trans('Are you sure you want to delete ?') }}');" href="{{ route('admin.contact-us.destroy',$item->id) }}" class="btn btn-primary btn-sm">
                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                        </a>
                                    @endcan
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $data->appends(request()->query())->links('layouts.partials.pagination') }}

                @if( $data->count() == 0)
                    <div class="empty-data">
                        <img src="{{ asset('assets')}}/images/nodata.svg">
                        <h4>{{ _trans('No_data_to_show')}}</h4>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection
@push('js')
    {{--<script>
        function  onDeleted(){
            if (confirm("{{ _trans('Are you sure you want to delete this contact ?') }}") == true) {
                return true;
            }
            return false;
        }
    </script>--}}
@endpush
