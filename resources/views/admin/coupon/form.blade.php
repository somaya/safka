@extends('layouts.master')
@section('title',$edit ? _trans('Edit Coupon') : _trans('Add Coupon'))
@section('content')
    <div class="container-fluid">
        <div class="page-title">
            <div class="row">
                <div class="col-6 col-sm-6">
                    <h3>{{ $edit ? _trans('Edit Coupon') : _trans('Add Coupon') }}</h3>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid default-dash">
        <div class="row">
            <div class="col-12 col-sm-6">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="{{ route('admin.dashboard') }}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                        </a>
                    </li>
                    <li class="breadcrumb-item ">
                        @canany(['Coupon list','Coupon edit','Coupon delete'])
                            <a href="{{ route('admin.coupon.index') }}">{{ _trans('Coupons') }}</a>
                        @endcanany
                    </li>
                    <li class="breadcrumb-item active">
                        {{ $edit ? _trans('Edit Coupon') : _trans('Add Coupon') }}
                    </li>
                </ol>
            </div>

            <div class="col-md-12">
                @if ($edit)
                    {{ Form::open(['route' => ['admin.coupon.update',$coupon->id],'method' => 'PUT','class' => 'form mb-15 form-submit','id' =>'kt_contact_form']) }}
                @else
                    {{ Form::open(['route' => 'admin.coupon.store','method' => 'POST','class' => 'form mb-15','id' =>'kt_contact_form']) }}
                @endif

                <div class="row g-4">
                    <div class="col-md-4">
                        <label class="form-label" for="code">{{ _trans('Coupon Code') }}</label>
                        <input id="code"
                               disabled
                               type="text"
                               class="form-control"
                               placeholder="{{ _trans('Enter') }} {{ _trans('Coupon Code') }}"
                               value="{{ getCodeTable('Coup','coupons',$edit,$edit ? $coupon->id : null) }}"
                        >
                    </div>

                    <div class="col-md-4">
                        <label class="form-label" for="coupon_type">{{ _trans('Coupon type') }}</label>
                        <select id="coupon_type"
                                name="coupon_type"
                                class="js-example-basic-single @error('coupon_type') is-invalid @enderror">
                            <option value="">{{ _trans('Select coupon type') }}</option>
                            @foreach(\App\Enums\CouponType::cases() as $row)
                                <option value="{{ $row->value }}"
                                    @selected(old('coupon_type',$edit ? $coupon->coupon_type : null) == $row->value)
                                >{{ _trans($row->name) }}</option>
                            @endforeach
                        </select>
                        @error('coupon_type')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>


                    <div class="col-md-4" id="div-owner-id" style="display: none">
                        <label class="form-label" for="owner_id">{{ _trans('Owner name') }}</label>
                        <select id="owner_id" name="owner_id"
                                class="js-example-basic-single @error('owner_id') is-invalid @enderror">
                            <option value="">{{ _trans('Select owner name') }}</option>

                        </select>
                        @error('owner_id')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4" id="div-shop-id" style="display: none">
                        <label class="form-label" for="shop_id">{{ _trans('shop name') }}</label>
                        <select id="shop_id" name="shop_id"
                                class="js-example-basic-single @error('shop_id') is-invalid @enderror">
                            <option value="">{{ _trans('Select shop name') }}</option>

                        </select>
                        @error('shop_id')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="form-label" for="discount_type">{{ _trans('Discount type') }}</label>
                        <select id="discount_type" name="discount_type"
                                class="js-example-basic-single @error('discount_type') is-invalid @enderror">
                            <option value="">{{ _trans('Select discount type') }}</option>
                            @foreach(\App\Enums\DiscountType::cases() as $data)
                                <option value="{{ $data->value }}"
                                    @selected(old('discount_type',$edit ? $coupon->discount_type : null) == $data->value)
                                >{{ _trans($data->name) }}</option>
                            @endforeach
                        </select>
                        @error('discount_type')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label for="discount" class="form-label">{{ _trans('Discount') }}</label>
                        <input id="discount"
                               type="number"
                               step="any"
                               min="1"
                               name="discount"
                               class="form-control @error('discount') is-invalid @enderror"
                               placeholder="{{ _trans('Enter') }} {{ _trans('Discount') }}"
                               value="{{ old('discount',$edit ? $coupon->discount : null) }}"
                        >
                        @error('discount')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="form-label" for="min_purchase">{{ _trans('Min purchase') }}</label>
                        <input id="min_purchase"
                               type="number"
                               step="any"
                               min="1"
                               name="min_purchase"
                               class="form-control @error('min_purchase') is-invalid @enderror"
                               placeholder="{{ _trans('Enter') }} {{ _trans('Min purchase') }}"
                               value="{{ old('min_purchase',$edit ? $coupon->min_purchase : null) }}"
                        >
                        @error('min_purchase')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="form-label" for="limit">{{ _trans('Limit user') }} </label>
                        <input id="limit"
                               type="number"
                               min="1"
                               step="1"
                               name="limit"
                               class="form-control @error('limit') is-invalid @enderror"
                               placeholder="{{ _trans('Enter') }} {{ _trans('Limit user') }}"
                               value="{{ old('limit',$edit ? $coupon->limit : null) }}"
                        >
                        @error('limit')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="form-label" for="start_date">{{ _trans('Start date') }} </label>
                        <input id="start_date"
                               type="date"
                               name="start_date"
                               class="form-control @error('start_date') is-invalid @enderror"
                               placeholder="{{ _trans('Enter') }} {{ _trans('Start date') }}"
                               value="{{ old('start_date',$edit ? $coupon->start_date : null) }}"
                        >
                        @error('start_date')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="form-label" for="end_date">{{ _trans('End date') }} </label>
                        <input id="end_date"
                               type="date"
                               name="end_date"
                               class="form-control @error('end_date') is-invalid @enderror"
                               placeholder="{{ _trans('Enter') }} {{ _trans('End date') }}"
                               value="{{ old('end_date',$edit ? $coupon->end_date : null) }}"
                        >
                        @error('end_date')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    @foreach($languages as $lang)
                        <div class="col-md-6">
                            <label for="{{ $lang['code'] }}[name]" class="form-label">{{ _trans('Coupon name')}} ({{ ucfirst($lang['code']) }})</label>
                            <input id="{{ $lang['code'] }}[name]"
                                   type="text"
                                   name="{{ $lang['code'] }}[name]"
                                   class="form-control @error($lang['code'].'.name') is-invalid @enderror"
                                   placeholder="{{ _trans('Enter') }} {{ _trans('Coupon name') }} ({{ ucfirst($lang['code']) }})"
                                   value="{{ old($lang['code'].'.name',$edit ? $coupon->translate($lang['code'])?->name : null) }}"
                            >
                            @error($lang['code'].'.name')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>
                    @endforeach
                    @foreach($languages as $lang)
                        <div class="col-md-6">
                            <label for="{{ $lang['code'] }}[description]" class="form-label">{{ _trans('Coupon description') }} ({{ ucfirst($lang['code']) }})</label>
                            <textarea id="{{ $lang['code'] }}[description]"
                                      cols="3"
                                      rows="3"
                                      name="{{ $lang['code'] }}[description]"
                                      class="form-control @error($lang['code'].'.description') is-invalid @enderror"
                                      placeholder="{{ _trans('Enter') }} {{ _trans('Coupon description') }} ({{ ucfirst($lang['code']) }})"
                            >{{ old($lang['code'].'.description',$edit ? $coupon->translate($lang['code'])?->description : null) }}</textarea>
                            @error($lang['code'].'.description')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>
                    @endforeach

                    <div class="col-md-2">
                        <br>
                        <button id="code-generate" type="button" class="btn btn-primary">Generate Code</button>
                    </div>
                    <div class="col-md-6">
                        <label class="form-label" for="code_coupon">{{ _trans('Code') }} </label>
                        <input id="code_coupon"
                               type="text"
                               name="code"
                               max="10"
                               class="form-control @error('code') is-invalid @enderror"
                               placeholder="{{ _trans('Enter') }} {{ _trans('Code') }}"
                               value="{{ old('code',$edit ? $coupon->code : null) }}"
                        >
                        @error('code')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>


                    <div class="col-md-12">
                        <div class="m-t-50 d-flex justify-content-end">
                            <button type="submit" class="btn btn-primary">{{ $edit ?_trans('Update') : _trans('Save') }}</button>
                        </div>
                    </div>

                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection
@push('scripts')
    <script>
        $(document).on('click', '#code-generate', function () {
            let length = 10;
            let result = '';
            let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
            let charactersLength = characters.length;
            for (let i = 0; i < length; i++) {
                result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }
            $('#code_coupon').val(result);
        });

        $(document).on('change', '#coupon_type', function (e) {
            e.preventDefault();
            let couponType = $('#coupon_type option:selected').val();
            showOrHideOwner(couponType,null)
        });


        $(document).ready(function () {
            showOrHideOwner("{{ old('coupon_type',$edit ? $coupon->coupon_type : null) }}","{{ old('owner_id',$edit ? $coupon->owner_id : null) }}")
            getShops("{{ old('owner_id',$edit ? $coupon->owner_id : null) }}", "{{ old('shop_id',$edit ? $coupon->shop_id : null) }}")
        })

        function showOrHideOwner(couponType,selectedId) {
            $("#owner_id").empty().append('<option value="">{{ _trans('Select owner name') }}</option>');
            $("#shop_id").empty().append('<option value="">{{ _trans('Select shop name') }}</option>');
            if (couponType == 'customized') {
                $('#div-owner-id,#div-shop-id').css('display', 'block');
                $.ajax({
                    url: "{{ route('ajax.owners') }}",
                    method: 'GET',
                    cache: false,
                    success: function (response) {
                        if (response.status == true) {
                            if (response.data.length > 0) {
                                $.each(response.data, function (index, value) {
                                    let selected = '';
                                    if (selectedId == value.id) {
                                        selected = 'selected';
                                    }
                                    $("#owner_id").append('<option ' + selected + ' value="' + value.id + '">' + value.name + '</option>');
                                });
                            }
                        } else {
                            toastr.error(response.data)
                        }
                    },
                });
            } else {
                $('#div-owner-id,#div-shop-id').css('display', 'none');
            }
        }
    </script>
    @include('layouts.ajax.shop')
@endpush
