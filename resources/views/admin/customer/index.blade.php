@extends('layouts.master')
@section('title',_trans('Customers'))
@section('content')
    <div class="container-fluid">
        <div class="page-title">
            <div class="row">
                <div class="col-6 col-sm-6">
                    <h3>{{_trans('Customers')}}</h3>
                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid default-dash">
        <div class="row">
            <div class="col-12 col-sm-6">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        {{--@if (in_array(auth()->user()->user_type,['suer_admin','admin']))
                            <a href="{{ route('admin.dashboard') }}">
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home">
                                    <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                    <polyline points="9 22 9 12 15 12 15 22"></polyline>
                                </svg>
                            </a>
                        @else
                            <a href="{{ route('owner.dashboard') }}">
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home">
                                    <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                    <polyline points="9 22 9 12 15 12 15 22"></polyline>
                                </svg>
                            </a>
                        @endif--}}
                        <a href="#">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                        </a>

                    </li>
                    <li class="breadcrumb-item active"><a href="#">{{ _trans('Customers') }}</a></li>
                </ol>
            </div>
            <div class="col-xl-12 col-md-12 ">
                <form action="{{ url()->current() }}" method="GET" id="form_search">
                    <div class="left-side-header">
                        <div class="row justify-content-between align-items-center">
                            <div class="col-md-8"> {{-- $attributes->merge(['class' => 'text-xl']) --}}
                                <div class="d-flex justify-content-between align-items-center">

                                    <div class="input-group">
                                        <input class="form-control" type="text"
                                               name="search"
                                               placeholder="{{ _trans('Search') }}"
                                               aria-label="search"
                                               value="{{ request('search') }}"
                                               aria-describedby="basic-addon1" data-bs-original-title="" title="">
                                        <span class="input-group-text" id="basic-addon1">
                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <g>
                                                    <g>
                                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                                              d="M11.2753 2.71436C16.0029 2.71436 19.8363 6.54674 19.8363 11.2753C19.8363 16.0039 16.0029 19.8363 11.2753 19.8363C6.54674 19.8363 2.71436 16.0039 2.71436 11.2753C2.71436 6.54674 6.54674 2.71436 11.2753 2.71436Z"
                                                              stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">

                                                        </path>
                                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                                              d="M19.8987 18.4878C20.6778 18.4878 21.3092 19.1202 21.3092 19.8983C21.3092 20.6783 20.6778 21.3097 19.8987 21.3097C19.1197 21.3097 18.4873 20.6783 18.4873 19.8983C18.4873 19.1202 19.1197 18.4878 19.8987 18.4878Z"
                                                              stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">

                                                        </path>
                                                    </g>
                                                </g>
                                            </svg>
                                        </span>
                                    </div>

                                    <select name="column_name" class="js-example-basic-single">
                                        <optgroup label="">
                                            @foreach($columns as $key => $column)
                                                <option @selected(request('column_name') == $key) value="{{ $key }}">{{ $column }}</option>
                                            @endforeach
                                        </optgroup>
                                    </select>

                                    <div class="form-check form-check-inline">
                                        <input
                                            class="form-check-input"
                                            type="radio"
                                            name="sort"
                                            id="ASC"
                                            value="ASC"
                                            @checked(request('sort') == 'ASC')
                                        />
                                        <label class="form-check-label" for="ASC">
                                            <i class="fa fa-arrow-up" aria-hidden="true"></i>
                                        </label>
                                    </div>

                                    <div class="form-check form-check-inline">
                                        <input
                                            class="form-check-input"
                                            type="radio"
                                            name="sort"
                                            id="DESC"
                                            value="DESC"
                                            @checked(request('sort') == 'DESC')
                                        />
                                        <label class="form-check-label" for="DESC">
                                            <i class="fa fa-arrow-down" aria-hidden="true"></i>
                                        </label>
                                    </div>

                                    <button type="button" class="btn btn-primary" id="search_customers">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                    </button>

                                    <button type="button" class="btn btn-primary" id="export_customers">
                                        <i class="fa fa-download" aria-hidden="true"></i>
                                    </button>

                                </div>
                            </div>


                            <div class="col-md-2">
                                <p class="mb-0 font-sm d-flex align-items-center justify-content-end">{{ _trans('Count Customers') }} :
                                    <span class="d-block font-md text-danger">({{ $data->total() }})</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </form>
                <br>
                <form action="{{ route('admin.customer.export-excel') }}" method="GET" id="form_export" style="display: none">
                    <div class="left-side-header">
                        <div class="row justify-content-between align-items-center">
                            <div class="col-md-8"> {{-- $attributes->merge(['class' => 'text-xl']) --}}
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="input-group">
                                        <input class="form-control" type="text"
                                               name="search"
                                               placeholder="{{ _trans('Search') }}"
                                               aria-label="search"
                                               value="{{ request('search') }}"
                                               aria-describedby="basic-addon1" data-bs-original-title="" title="">
                                        <span class="input-group-text" id="basic-addon1">
                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <g>
                                                    <g>
                                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                                              d="M11.2753 2.71436C16.0029 2.71436 19.8363 6.54674 19.8363 11.2753C19.8363 16.0039 16.0029 19.8363 11.2753 19.8363C6.54674 19.8363 2.71436 16.0039 2.71436 11.2753C2.71436 6.54674 6.54674 2.71436 11.2753 2.71436Z"
                                                              stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">
                                                        </path>
                                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                                              d="M19.8987 18.4878C20.6778 18.4878 21.3092 19.1202 21.3092 19.8983C21.3092 20.6783 20.6778 21.3097 19.8987 21.3097C19.1197 21.3097 18.4873 20.6783 18.4873 19.8983C18.4873 19.1202 19.1197 18.4878 19.8987 18.4878Z"
                                                              stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">
                                                        </path>
                                                    </g>
                                                </g>
                                            </svg>
                                        </span>
                                    </div>
                                    <select name="column_name" class="js-example-basic-single">
                                        <optgroup label="">
                                            @foreach($columns as $key => $column)
                                                <option @selected(request('column_name') == $key) value="{{ $key }}">{{ $column }}</option>
                                            @endforeach
                                        </optgroup>
                                    </select>

                                </div>
                            </div>
                        </div>
                    </div>
                </form>


                <div class="table-responsive custom-scrollbar p-t-30">
                    <table class="table">
                        <thead>
                        <tr>
                            <th><span>{{ _trans('SL')}}  </span></th>
                            <th><span>{{ _trans('Date')}} </span></th>
                            <th><span>{{ _trans('Name')}}  </span></th>
                            <th><span>{{ _trans('Facility Name')}}  </span></th>
                            <th><span>{{ _trans('Branch Type')}}  </span></th>
                            <th><span>{{ _trans('Phone')}}  </span></th>
                            <th><span>{{ _trans('Orders')}} </span></th>
                            <th><span>{{ _trans('Status')}} </span></th>
{{--                            <th><span>{{ _trans('Blocked')}} </span></th>--}}
                            <th><span>{{ _trans('action')}}</span></th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $index => $item)
                            <tr>
                                <td class="text-main">#{{ $index + 1 }}</td>
                                <td>{{$item->created_at->format('d-m-Y')}}</td>
                                <td>{{ $item->user->name }}</td>
                                <td>{{ $item->facility_name }}</td>
                                <td>{{ $item->branch_type }}</td>
                                <td>{{ $item->phone }}</td>
                                <td>
                                    <a href="{{ route('order.index').'/?customer='.$item->user->name }}">{{ $item->orders_count }}</a>
                                </td>
                                <td>
                                    <div class="media-body icon-state">
                                        <label class="switch">
                                            <input id="{{ $item->id }}" type="checkbox" class="status" @checked($item->user->status == 1) >
                                            <span class="switch-state ">
                                            </span>
                                        </label>
                                    </div>
                                </td>
{{--                                <td>--}}
{{--                                    <div class="media-body icon-state">--}}
{{--                                        <label class="switch">--}}
{{--                                            <input type="checkbox"  @checked($item->blocked == 1) >--}}
{{--                                            <span class="switch-state "></span>--}}
{{--                                        </label>--}}
{{--                                    </div>--}}
{{--                                </td>--}}

                                <td>
                                    <a href="{{ route('admin.customer.show',$item->id) }}" class="btn btn-primary">
                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                    </a>
                                    <a data-bs-toggle="modal" data-bs-target="#change_password" data-id="{{ $item->id }}"
                                       class="btn btn-primary">
                                        <i class="fa fa-lock" aria-hidden="true"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $data->appends(request()->query())->links('layouts.partials.pagination') }}

                @if( $data->count() == 0)
                    <div class="empty-data">
                        <img src="{{ asset('assets')}}/images/nodata.svg">
                        <h4>{{ _trans('No_data_to_show')}}</h4>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
    @include('layouts.partials.change-password',['route' => route('admin.customer.change-password'),'name' => 'change_password'])

@endsection
@include('layouts.ajax.update-status',['class' => 'status','route' => route('admin.customer.update-status')])
@include('layouts.ajax.search_input')
@push('scripts')
    <script>
        $(document).ready(function () {
            $("#search_customers").click(function () {
                $("#form_search").submit(); // Submit the form
            });
            $("#export_customers").click(function () {
                $("#form_export").submit(); // Submit the form
            });
        });

    </script>
@endpush
