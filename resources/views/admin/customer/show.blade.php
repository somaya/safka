@extends('layouts.master')
@section('title',_trans('Customers'))
@section('content')
    <div class="container-fluid">
        <div class="page-title">
            <div class="row">
                <div class="col-6 col-sm-6">
                    <h3>{{_trans('Customer name ') . $customer->user->name }}</h3>
                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid default-dash">
        <div class="row">

            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card card-details">
                            <h4 class="title-details">{{_trans('customer Details')}}</h4>
                            <div class="">
                                <p>{{_trans('name')}} :<span>{{$customer->user->name}}</span></p>
                                <p>{{_trans('Facility Name')}} :<span>{{$customer->facility_name}}</span></p>
                                <p>{{_trans('Facility Type')}} :<span>{{$customer->branchType?->translate(locale())?->name}}</span></p>
                                <p>{{_trans('Email')}} :<span>{{$customer->user->email}}</span></p>
                                <p>{{_trans('Phone')}} :<span>{{$customer->phone}}</span></p>
                                <p>{{_trans('Governorate')}} :<span>{{$customer->user->governorate?->translate(locale())?->name}}</span></p>
                                <p>{{_trans('Region')}} :<span>{{$customer->user->region?->translate(locale())?->name}}</span></p>
                                <p>{{_trans('Address')}} :<span>{{$customer->address}}</span></p>
                                <p>{{_trans('Birthdate')}} :<span>{{$customer->birthday}}</span></p>
                                <p>{{_trans('Gender')}} :<span>{{$customer->gender}}</span></p>


                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="card card-details">
                            <h4 class="title-details">{{ _trans('Orders')}}</h4>
                            <div class="">
                                <p>{{ _trans('Count')}} : <span>{{$orders->count()}}</span></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-details">
                            <h4 class="title-details">{{ _trans('Orders Total Price')}}</h4>
                            <div class="">
                                <p> <span>{{$orders->sum('total_price')-$orders->sum('coupon_price')}}</span></p>
                            </div>
                        </div>
                    </div>



                </div>
            </div>

        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection

