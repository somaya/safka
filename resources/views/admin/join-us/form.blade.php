@extends('layouts.master')

@section('title',_trans('Create new shop'))

@push('styles')
    <style>
        #geomap {
            width: 100%;
            height: 400px;
        }
    </style>

@endpush

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="container-fluid">
        <div class="page-title">
            <div class="row">
                <div class="col-6 col-sm-6">
                    <h3>{{ _trans('Create new shop') }}</h3>
                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid default-dash">
        <div class="row">
            <div class="col-12 col-sm-6">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                        </a>
                    </li>
                    @canany(['JoinUs list','JoinUs edit','JoinUs delete'])
                        <li class="breadcrumb-item ">
                            <a href="{{ route('admin.join-us.index') }}">{{ _trans('Join Us') }}</a>
                        </li>
                    @endcanany
                    <li class="breadcrumb-item active">
                        {{  _trans('Create new shop') }}
                    </li>
                </ol>
            </div>

            <div class="col-md-12">
                {{ Form::open(['url' => route('admin.join-us.store'),'method' => 'POST','files' => true,'class' => 'form mb-15','id' =>'kt_contact_form']) }}
                {{ Form::hidden('join_id',$shop->id) }}


                <div class="row g-3">
                    <div class="col-md-4">
                        <label class="form-label" for="code">{{ _trans('shop Code') }}</label>
                        <input id="code"
                               disabled
                               type="text"
                               class="form-control"
                               placeholder="{{ _trans('Enter') }} {{ _trans('shop Code') }}"
                               value="{{ getCodeTable('Shop','shops') }}"
                        >
                    </div>

                    <div class="col-md-4">
                        <label class="form-label" for="owner_name">{{ _trans('Owner name') }}</label>
                        <input id="owner_name"
                               type="text"
                               name="owner_name"
                               class="form-control @error('owner_name') is-invalid @enderror"
                               placeholder="{{ _trans('Enter') }} {{ _trans('Owner phone') }}"
                               value="{{ old('owner_name',$shop->owner_name) }}"
                        >
                        @error('owner_name')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="form-label" for="owner_phone">{{ _trans('Owner phone') }}</label>
                        <input id="owner_phone"
                               type="text"
                               name="owner_phone"
                               class="form-control @error('owner_phone') is-invalid @enderror"
                               placeholder="{{ _trans('Enter') }} {{ _trans('Owner phone') }}"
                               value="{{ old('owner_phone',$shop->owner_phone) }}"
                        >
                        @error('owner_phone')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="form-label" for="owner_email">{{ _trans('Owner email') }}</label>
                        <input id="owner_email"
                               type="email"
                               name="owner_email"
                               class="form-control @error('owner_email') is-invalid @enderror"
                               placeholder="{{ _trans('Enter') }} {{ _trans('Owner name') }}"
                               value="{{ old('owner_email',$shop->owner_email) }}"
                        >
                        @error('owner_email')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="form-label" for="time_to_contact">{{ _trans('Time to contact') }}</label>
                        <input id="time_to_contact"
                               type="text"
                               name="time_to_contact"
                               class="form-control @error('time_to_contact') is-invalid @enderror"
                               placeholder="{{ _trans('Enter') }} {{ _trans('Time to contact') }}"
                               value="{{ old('time_to_contact',$shop->time_to_contact) }}"
                        >
                        @error('time_to_contact')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="form-label" for="notes">{{ _trans('Note') }}</label>
                        <input id="notes"
                               type="text"
                               name="notes"
                               class="form-control @error('notes') is-invalid @enderror"
                               placeholder="{{ _trans('Enter') }} {{ _trans('Note') }}"
                               value="{{ old('notes',$shop->notes) }}"
                        >
                        @error('notes')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label id="password" class="form-label" for="">{{ _trans('Password') }}</label>
                        <input id="password"
                               type="password"
                               name="password"
                               class="form-control @error('password') is-invalid @enderror"
                               placeholder="{{ _trans('Enter') }} {{ _trans('Password') }}"
                               value="{{ old('password') }}"
                        >
                        @error('password')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label id="password_confirmation" class="form-label" for="">{{ _trans('Confirm Password') }}</label>
                        <input id="password_confirmation"
                               type="password"
                               name="password_confirmation"
                               class="form-control @error('password_confirmation') is-invalid @enderror"
                               placeholder="{{ _trans('Enter') }} {{ _trans('Confirm Password') }}"
                               value="{{ old('password_confirmation') }}"
                        >
                        @error('confirm_password')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label id="brand_name" class="form-label" for="">{{ _trans('Brand name') }}</label>
                        <input id="brand_name"
                               type="text"
                               name="brand_name"
                               class="form-control @error('brand_name') is-invalid @enderror"
                               placeholder="{{ _trans('Enter') }} {{ _trans('Brand name') }}"
                               value="{{ old('brand_name') }}"
                        >
                        @error('brand_name')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    @foreach($languages as $lang)
                        <div class="col-md-4">
                            <label id="{{ $lang['code'] }}[name]" class="form-label" for="">{{ _trans('shop name') }} ({{ ucfirst($lang['code']) }})</label>
                            <input id="{{ $lang['code'] }}[name]"
                                   type="text"
                                   name="{{ $lang['code'] }}[name]"
                                   class="form-control @error($lang['code'].'.name') is-invalid @enderror"
                                   placeholder="{{ _trans('Enter') }} {{ _trans('shop name') }} ({{ ucfirst($lang['code']) }})"
                                   value="{{ old($lang['code'].'.name',$shop->shop_name) }}"
                            >
                            @error($lang['code'].'.name')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>
                    @endforeach

                    @foreach($languages as $lang)
                        <div class="col-md-12">
                            <label id="{{ $lang['code'] }}[description]" class="form-label" for="">{{ _trans('shop description') }} ({{ ucfirst($lang['code']) }})</label>
                            <textarea id="{{ $lang['code'] }}[description]"
                                      cols="3"
                                      rows="3"
                                      name="{{ $lang['code'] }}[description]"
                                      class="form-control @error($lang['code'].'.description') is-invalid @enderror "
                                      placeholder="{{ _trans('Enter') }} {{ _trans('shop description') }} ({{ ucfirst($lang['code']) }})"
                            >{{ old($lang['code'].'.description') }}</textarea>
                            @error($lang['code'].'.description')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>
                    @endforeach

                    <div class="col-md-6">
                        <label class="form-label" for="branch_type_id">{{ _trans('Branch Type name') }}</label>
                        <select id="branch_type_id"
                                name="branch_type_id[]"
                                multiple
                                class="js-example-basic-single @error('branch_type_id') is-invalid @enderror">
                            @foreach($branchTypes as $branchType)
                                <option value="{{ $branchType->id }}"
                                    @selected(in_array($branchType->id,old('branch_type_id', ($shop->branchTypes ? $shop->branchTypes->pluck('id')->toArray() : []) )))
                                >{{ $branchType->translate(locale())?->name }}
                                </option>
                            @endforeach
                        </select>
                        @error('branch_type_id')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                        @error('branch_type_id.*')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>
                    <div class="col-md-4" id="subscription_type_div">
                        <label class="form-label" for="subscription_type">{{ _trans('Subscription Type') }}</label>
                        <select id="subscription_type" name="subscription_type"
                                class="js-example-basic-single @error('subscription_type') is-invalid @enderror">
                            <option value="">{{ _trans('Select Subscription Type') }}</option>
                            @foreach(\App\Enums\SubscriptionType::cases() as $type)
                                <option value="{{ $type->value }}"
                                    @selected(old('subscription_type') == $type->value)>{{ _trans($type->name) }}</option>
                            @endforeach
                        </select>
                        @error('subscription_type')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4" id="subscription_value_div">
                        <label class="form-label" for="subscription_value">{{ _trans('Subscription Value') }}</label>
                        <input id="subscription_value"
                               type="number"
                               step="any"
                               name="subscription_value"
                               class="form-control @error('subscription_value') is-invalid @enderror"
                               placeholder="{{ _trans('Enter') }} {{ _trans('Subscription Value') }}"
                               value="{{ old('subscription_value') }}"
                        >
                        @error('subscription_value')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="form-label" for="email">{{ _trans('E-mail') }}</label>
                        <input id="email"
                               type="email"
                               name="email"
                               class="form-control @error('email') is-invalid @enderror"
                               placeholder="{{ _trans('Enter') }} {{ _trans('E-mail') }}"
                               value="{{ old('email') }}"
                        >
                        @error('email')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="form-label" for="phone">{{ _trans('Phone') }}</label>
                        <input id="phone"
                               type="text"
                               name="phone"
                               class="form-control @error('phone') is-invalid @enderror"
                               placeholder="{{ _trans('Enter') }} {{ _trans('Phone') }}"
                               value="{{ old('phone') }}"
                        >
                        @error('phone')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="form-label" for="country_id">{{ _trans('Country Name') }}</label>
                        <select id="country_id" name="country_id"
                                class="js-example-basic-single @error('country_id') is-invalid @enderror">
                            <option value="">{{ _trans('Select Country Name') }}</option>
                            @foreach($countries as $country)
                                <option value="{{ $country->id }}" @selected(old('country_id',$shop->country_id) == $country->id)>{{ $country->name }}</option>
                            @endforeach
                        </select>
                        @error('country_id')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="form-label" for="governorate_id">{{ _trans('Governorate Name') }}</label>
                        <select id="governorate_id" name="governorate_id"
                                class="js-example-basic-single @error('governorate_id') is-invalid @enderror">
                            <option value="">{{ _trans('Select Governorate Name') }}</option>

                        </select>
                        @error('governorate_id')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="form-label" for="region_id">{{ _trans('Region Name') }}</label>
                        <select id="region_id" name="region_id"
                                class="js-example-basic-single @error('region_id') is-invalid @enderror">
                            <option value="">{{ _trans('Select Region Name') }}</option>
                        </select>
                        @error('region_id')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="form-label" for="address">{{ _trans('Address') }}</label>
                        <input id="address"
                               type="text"
                               name="address"
                               class="form-control @error('address') is-invalid @enderror"
                               placeholder="{{ _trans('Enter') }} {{ _trans('Address') }}"
                               value="{{ old('address',$shop->address) }}"
                        >
                        @error('address')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <div class="form-group form-image">
                            <label class="font-md">{{ _trans('Logo') }}</label>
                            <img src="{{asset('assets/images/no-image.png') }}"
                                 class="mb-2 logo-preview">
                            <input id="logo"
                                   type="file"
                                   name="logo"
                                   class="form-control logo @error('logo') is-invalid @enderror">
                            @error('logo')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>

                    </div>

                    <div class="col-md-4">
                        <input type="text" id="search_location" class="form-control"
                               placeholder="{{ _trans('Search location') }}"
                               value="{{ old('search_location') }}"
                               name="search_location"/>
                    </div>

                    <div class="col-md-12">
                        <div id="divInfo" style="font-family: Arial; font-size: 12px; color: Red;"></div>
                        <div id="geomap"></div>
                    </div>

                    <input type="hidden" name="lat" value="{{ old('lat',$shop->lat)  }}" class="search_latitude"/>
                    <input type="hidden" name="lng" value="{{ old('lng',$shop->lng)  }}" class="search_longitude"/>


                    <div class="col-md-12">
                        <h5>{{ _trans('Working Hours') }}</h5>
                        <hr>
                        @include('admin.shop.form-repeater',['data' => !empty(old('shop')) ? old('shop') : []])
                    </div>
                    <div class="col-md-12">
                        <div class="m-t-50 d-flex justify-content-end">
                            <button type="submit" class="btn btn-primary">{{ _trans('Join us') }}</button>
                        </div>
                    </div>


                </div>


                {{ Form::close() }}
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection
@include('layouts.partials.read-photo',['inputName' => 'logo'])
@include('layouts.partials.ckeditor',['inputClass' => 'textarea'])
@push('scripts')
    <script src="https://maps.googleapis.com/maps/api/js?key={{ Utility::getValByName('google_maps_api') }}&libraries=places&callback=initMap&v=weekly" async></script>
    <script>
        let map, infoWindow;
        let marker;
        let geocoder;

        function initMap() {
            let initialLat = $('.search_latitude').val();
            let initialLong = $('.search_longitude').val();
            initialLat = initialLat ? initialLat : 31.205753;
            initialLong = initialLong ? initialLong : 29.924526;
            let latlng = new google.maps.LatLng(initialLat, initialLong);
            let options = {
                zoom: 12,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                mapTypeControl: false,
                streetViewControl: true,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.SMALL
                }
            };

            map = new google.maps.Map(document.getElementById('geomap'), options);

            /*Geolocation  */
            infoWindow = new google.maps.InfoWindow();

            const locationButton = document.createElement("button");

            locationButton.textContent = "Pan to Current Location";
            locationButton.classList.add("custom-map-control-button");
            map.controls[google.maps.ControlPosition.TOP_CENTER].push(locationButton);

            locationButton.addEventListener("click", (e) => {
                // Try HTML5 geolocation.
                e.preventDefault();
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(
                        (position) => {
                            console.log(position)

                        },
                        () => {
                            handleLocationError(true, infoWindow, map.getCenter());
                        }
                    );
                } else {
                    // Browser doesn't support Geolocation
                    handleLocationError(false, infoWindow, map.getCenter());
                }
            });

            geocoder = new google.maps.Geocoder();

            var input = document.getElementById('search_location');
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            var autocomplete = new google.maps.places.Autocomplete(input);
            autocomplete.bindTo('bounds', map);

            var infowindow = new google.maps.InfoWindow();

            marker = new google.maps.Marker({
                map: map,
                draggable: true,
                position: latlng,
                anchorPoint: new google.maps.Point(0, -29)
            });

            autocomplete.addListener('place_changed', function () {
                infowindow.close();
                marker.setVisible(false);
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    window.alert("Autocomplete's returned place contains no geometry");
                    return;
                }

                // If the place has a geometry, then present it on a map.
                if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(12);
                }
                /*marker.setIcon(({
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(35, 35)
                }));*/
                //marker.setPosition(place.geometry.location);
                map.setCenter(place.geometry.location);
                map.setZoom(12)
                marker.setPosition(place.geometry.location);
                marker.setVisible(true);


                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');
                }

                infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
                $('#address').val(address)
                infowindow.open(map, marker);

                // Location details
                /*for (var i = 0; i < place.address_components.length; i++) {
                    if(place.address_components[i].types[0] == 'postal_code'){
                        document.getElementById('postal_code').innerHTML = place.address_components[i].long_name;
                    }
                    if(place.address_components[i].types[0] == 'country'){
                        document.getElementById('country').innerHTML = place.address_components[i].long_name;
                    }
                }*/
                $('.search_latitude').val(place.geometry.location.lat());
                $('.search_longitude').val(place.geometry.location.lng());
            });

            marker.addListener('dragend', function () {
                let latlng;
                geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            latlng = new google.maps.LatLng(marker.getPosition().lat(), marker.getPosition().lng());
                            $('.search_latitude').val(marker.getPosition().lat());
                            $('.search_longitude').val(marker.getPosition().lng());
                            $('.place_id').val(results[0].place_id);
                            map.setCenter(marker.getPosition())
                            map.setZoom(12)
                            marker.setPosition(marker.getPosition());
                            infowindow.setContent('<div><strong>' + results[0].formatted_address + '</strong>');
                            $('#address').val(results[0].formatted_address);
                            infowindow.open(map, marker);
                        }
                    }
                });
            });

            map.addListener('click', function (event) {
                geocoder.geocode({'latLng': event.latLng}, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            $('.search_latitude').val(event.latLng.lat());
                            $('.search_longitude').val(event.latLng.lng());
                            $('.place_id').val(results[0].place_id);
                            map.setCenter(event.latLng)
                            map.setZoom(12)
                            marker.setPosition(event.latLng);

                            infowindow.setContent('<div><strong>' + results[0].formatted_address + '</strong>');
                            $('#address').val(results[0].formatted_address);
                            infowindow.open(map, marker);
                        }
                    }
                });
            });
        }

        function handleLocationError(browserHasGeolocation, infoWindow, pos) {
            infoWindow.setPosition(pos);
            console.log(browserHasGeolocation)
            infoWindow.setContent(
                browserHasGeolocation
                    ? "Error: The Geolocation service failed."
                    : "Error: Your browser doesn't support geolocation."
            );
            infoWindow.open(map);
        }


    </script>

    <script src="{{ asset('assets/repeater/jquery.repeater.min.js') }}"></script>
    <script src="{{ asset('assets/repeater/form-repeater.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            window.initMap = initMap;

            getGovernorate({{ old('country_id',$shop->country_id) }}, {{ old('governorate_id',$shop->governorate_id)  }})
            getRegion({{ old('governorate_id',$shop->governorate_id) }}, {{ old('region_id',$shop->region_id) }})
        })

    </script>
    @include('layouts.ajax.countries')
    {{--    @include('layouts.ajax.subscription')--}}

@endpush

