@extends('layouts.master')

@section('title',_trans('Notification'))

@section('content')
    <div class="container-fluid">
        <div class="page-title">
            <div class="row">
                <div class="col-6 col-sm-6">
                    <h3>{{ _trans('Notification') }}</h3>
                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid default-dash">
        <div class="row">
            <div class="col-12 col-sm-6">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                        </a>
                    </li>
                    <li class="breadcrumb-item active">
                        {{  _trans('Notification') }}
                    </li>
                </ol>
            </div>

            <div class="col-md-12">
                {{ Form::open(['url' => route('admin.notification.store'),'method' => 'POST','files' => true,'class' => 'form mb-15','id' =>'kt_contact_form']) }}
                <div class="row g-3">

                    @foreach($languages as $lang)
                        <div class="col-md-6">
                            <label id="{{ $lang['code'] }}[title]" class="form-label" for="">{{ _trans('Title') }} ({{ ucfirst($lang['code']) }})</label>
                            <input id="{{ $lang['code'] }}[title]"
                                   type="text"
                                   name="{{ $lang['code'] }}[title]"
                                   class="form-control @error($lang['code'].'.title') is-invalid @enderror"
                                   placeholder="{{ _trans('Title') }} ({{ ucfirst($lang['code']) }})"
                                   value="{{ old($lang['code'].'.title') }}"
                            >
                            @error($lang['code'].'.title')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>
                    @endforeach

                    @foreach($languages as $lang)
                        <div class="col-md-12">
                            <label for="{{ $lang['code'] }}[body]" class="form-label">{{ _trans('Description') }} ({{ ucfirst($lang['code']) }})</label>
                            <textarea id="{{ $lang['code'] }}[body]"
                                      cols="3"
                                      rows="3"
                                      name="{{ $lang['code'] }}[body]"
                                      class="form-control @error($lang['code'].'.body') is-invalid @enderror"
                                      placeholder="{{ _trans('Description') }} ({{ ucfirst($lang['code']) }})"
                            >{{ old($lang['code'].'.body') }}</textarea>
                            @error($lang['code'].'.body')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>
                    @endforeach

                    <div class="col-md-4">
                        <label class="form-label" for="notification_type">{{ _trans('Notification type') }}</label>
                        <select id="notification_type"
                                name="notification_type"
                                class="js-example-basic-single @error('notification_type') is-invalid @enderror">
                            <option value="">{{ _trans('Select notification type') }}</option>
                            @foreach(\App\Enums\NotificationType::cases() as $row)
                                <option value="{{ $row->value }}"
                                    @selected(old('notification_type') == $row->value)>{{ _trans($row->name) }}
                                </option>
                            @endforeach
                        </select>
                        @error('notification_type')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4" id="div_customers" style="display: none">
                        <label class="form-label" for="customers">{{ _trans('Customers') }}</label>
                        <select id="customers"
                                name="customers[]"
                                multiple
                                class="js-example-basic-single @error('customers')  is-invalid @enderror  @error('customers.*')is-invalid @enderror">
                        </select>
                        @error('customers')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                        @error('customers.*')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4" id="div_shop_id" style="display: none">
                        <label class="form-label" for="shop_id">{{ _trans('Shop name') }}</label>
                        <select id="shop_id"
                                name="shop_id"
                                class="js-example-basic-single @error('shop_id') is-invalid @enderror">
                            <option value="">{{ _trans('Select shop name') }}</option>
                        </select>
                        @error('shop_id')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>


                    <div class="col-md-4" id="div_custom_type" style="display: none">
                        <label class="form-label" for="custom_type">{{ _trans('Type') }}</label>
                        <select id="custom_type"
                                name="custom_type"
                                class="js-example-basic-single @error('custom_type') is-invalid @enderror">
                            <option value="">{{ _trans('Select type') }}</option>
                            <option value="all" @selected(old('custom_type') == 'all')>{{ _trans('All') }}</option>
                            <option value="custom" @selected(old('custom_type') == 'custom')>{{ _trans('Custom') }}</option>

                        </select>
                        @error('custom_type')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4" id="div_upload_file" style="display: none">
                        <label class="form-label" for="file">{{ _trans('Upload file') }}</label>
                        <input id="file"
                               type="file"
                               name="file"
                               class="form-control @error('file') is-invalid @enderror"
                        >
                        @error('file')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>


                    <div class="col-md-12">
                        <div class="m-t-50 d-flex justify-content-end">
                            <button type="submit" class="btn btn-primary">{{ _trans('Send')  }}</button>
                        </div>
                    </div>

                </div>


                {{ Form::close() }}
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection


@push('scripts')
    <script>
        $(document).ready(function () {

            @if(old('notification_type') == 'customer')
            $('#div_custom_type').css('display', 'block');
            $("#custom_type").trigger("change");

            $('#div_shop_id').css('display', 'none');
            $("#shop_id").empty();
            $("#shop_id").append('<option value="">{{ _trans('Select shop name') }}</option>');

            $('#div_customers').css('display', 'none');
            $("#customers").empty();
            @elseif(old('notification_type') == 'shop')
            $('#div_custom_type').css('display', 'block');
            $("#custom_type").trigger("change");

            $('#div_customers').css('display', 'none');
            $("#customers").empty();

            $('#div_shop_id').css('display', 'block');
            $("#shop_id").empty();
            $("#shop_id").append('<option value="">{{ _trans('Select shop name') }}</option>');
            getshops('{!! old('shop_id') !!}')
            @else
            $('#div_custom_type').css('display', 'none');
            $('#div_upload_file').css('display', 'none');

            $('#div_shop_id').css('display', 'none');
            $("#shop_id").empty();
            $("#shop_id").append('<option value="">{{ _trans('Select shop name') }}</option>');

            $('#div_customers').css('display', 'none');
            $('customers').empty();
            @endif
        })

        $(document).on('change', '#notification_type', function (e) {
            e.preventDefault();
            var notification_type = $('#notification_type option:selected').val();
            if (notification_type == '' || notification_type == null) {
                $('#div_custom_type').css('display', 'none');
                $('#div_upload_file').css('display', 'none');

                $('#div_customers').css('display', 'none');
                $('customers').empty();

                $("#shop_id").empty();
                $("#shop_id").append('<option value="">{{ _trans('Select shop name') }}</option>');
                $('#div_shop').css('display', 'none');
                return false;
            } else {
                if (notification_type == 'all') {
                    $('#div_custom_type').css('display', 'none');
                    $('#div_upload_file').css('display', 'none');

                    $('#div_shop_id').css('display', 'none');
                    $("#shop_id").empty();
                    $("#shop_id").append('<option value="">{{ _trans('Select shop name') }}</option>');

                    $('#div_customers').css('display', 'none');
                    $('customers').empty();

                } else if (notification_type == 'customer') {
                    $('#div_custom_type').css('display', 'block');
                    $("#custom_type").trigger("change");

                    $('#div_shop_id').css('display', 'none');
                    $("#shop_id").empty();
                    $("#shop_id").append('<option value="">{{ _trans('Select shop name') }}</option>');

                    $('#div_customers').css('display', 'none');
                    $("#customers").empty();
                    //getCustomers(null, [])
                } else if (notification_type == 'shop') {
                    $('#div_custom_type').css('display', 'block');
                    $("#custom_type").trigger("change");

                    $('#div_customers').css('display', 'none');
                    $("#customers").empty();

                    $('#div_shop_id').css('display', 'block');
                    $("#shop_id").empty();
                    $("#shop_id").append('<option value="">{{ _trans('Select shop name') }}</option>');
                    getshops(null)
                }
            }
        });

        $(document).on('change', '#custom_type', function () {
            var custom_type = $('#custom_type option:selected').val();
            var notification_type = $('#notification_type option:selected').val();
            if (custom_type == '' || custom_type == null) {
                $('#div_upload_file').css('display', 'none');
                return false;
            } else {
                if (notification_type == 'customer' && custom_type == 'custom') {
                    $('#div_upload_file').css('display', 'block');
                } else if (notification_type == 'shop' && custom_type == 'custom') {
                    $('#div_upload_file').css('display', 'block');
                } else {
                    $('#div_upload_file').css('display', 'none');
                }
            }
        });

        /*$(document).on('change', '#shop_id', function () {
            var shop_id = $('#shop_id option:selected').val();
            console.log(shop_id)
            if (shop_id == '' || shop_id == null) {
                $("#customers").empty();
                return false;
            } else {
                $('#div_customers').css('display', 'block');
                $("#customers").empty();
                getCustomers(shop_id, [])
            }
        });*/

        function getshops(selectedId) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                url: "{{ route('ajax.getResourceData') }}",
                method: 'POST',
                data: {
                    resource: 'Shop'
                },
                success: function (response) {
                    if (response.status == true) {
                        if (response.data.length > 0) {
                            $.each(response.data, function (index, value) {
                                var selected = '';
                                if (value.id == selectedId) {
                                    selected = 'selected';
                                }
                                $("#shop_id").append('<option ' + selected + ' value="' + value.id + '">' + value.name + '</option>');
                            });
                        }

                    } else {
                        toastr.error(response.error)
                    }

                },

            });
        }

        function getCustomers(shopId, selectedId) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                url: "{{ route('ajax.get-customer-token') }}",
                method: 'POST',
                data: {
                    shop_id: shopId
                },
                success: function (response) {
                    if (response.status == true) {
                        if (response.data.length > 0) {
                            $.each(response.data, function (index, value) {
                                var selected = '';
                                if (jQuery.inArray(value.id.toString(), selectedId) !== -1) {
                                    selected = 'selected';
                                }
                                $("#customers").append('<option ' + selected + ' value="' + value.id + '">' + value.name + '</option>');
                            });
                        }

                    } else {
                        toastr.error(response.error)
                    }

                },

            });
        }
    </script>

@endpush

