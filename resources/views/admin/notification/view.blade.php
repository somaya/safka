@extends('layouts.master')
@section('title',_trans('Notification'))
@section('content')
    <div class="container-fluid">
        <div class="page-title">
            <div class="row">
                <div class="col-6 col-sm-6">
                    <h3>{{_trans('Notifications')}}</h3>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid default-dash">
        <div class="row">
            <div class="col-12 col-sm-6">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/') }}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                        </a></li>
                    <li class="breadcrumb-item active"><a href="#">{{ _trans('Notifications') }}</a></li>
                </ol>
            </div>
            <div class="col-xl-12 col-md-12 ">
                <div class="table-responsive custom-scrollbar p-t-30">
                    <table class="table">
                        <thead>
                        <tr>
                            <th><span>{{ _trans('SL')}}  </span></th>
                            <th><span>{{ _trans('Notification type')}}  </span></th>
                            <th><span>{{ _trans('Customer name')}} </span></th>
                            <th><span>{{ _trans('Date')}} </span></th>
                            <th><span>{{ _trans('Read')}} </span></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $index => $item)
                            <tr>
                                <td>#{{ $index+1 }}</td>
                                <td>
                                    @if ($item['type'] == 'order')
                                        {{ $item['data']['order_code'] }}
                                    @elseif ($item['type'] == 'account_order')
                                        {{ $item['data']['order_code'] }}
                                    @elseif ($item['type'] == 'message')
                                        {{ _trans('Message from ').$item['customer']['user']['name'] }}
                                    @elseif ($item['type'] == 'ticket')
                                        {{ _trans('Message ticket from ').$item['customer']['user']['name'] }}
                                    @elseif ($item['type'] == 'join_us')
                                        {{ _trans('Request join us ').$item['data']['owner_name'] }}
                                    @elseif ($item['type'] == 'invoice')
                                        <a href="{{ route('owner.payments').'/?payment='.$item['data']['payment_id'] }}">
                                            {{ _trans('Invoice reminder').' '.$item['data']['payment_id'] }}
                                        </a>

                                    @endif
                                </td>
                                <td>
                                    @if ($item['type'] == 'order' || $item['type'] == 'account_order')
                                        <a href="{{ route('order.show',$item['data']['order_id']) }}">{{ $item['customer']['user']['name'] }}</a>
                                    @elseif ($item['type'] == 'message')
                                        <a href="{{ route('owner.chat.index')}}">{{ $item['customer']['user']['name'] }}</a>
                                    @elseif ($item['type'] == 'join_us')
                                        <a href="{{ route('admin.join-us.show',$item['data']['join_us_id'])}}">{{ $item['data']['owner_name'] }}</a>
                                    @elseif ($item['type'] == 'ticket')
                                        <a href="{{ route('admin.customer.show',$item['data']['customer_id']) }}">{{ $item['customer']['user']['name'] }}</a>
                                    @elseif ($item['type'] == 'invoice')
                                        <a href="{{ route('owner.payments').'/?payment='.$item['data']['payment_id'] }}">{{ \App\Models\User::first()->name }}</a>
                                    @endif
                                </td>
                                <td>{{ formatDate('d-m-Y H:i A',$item['created_at']) }}</td>
                                <td>{{ is_null($item['read_at']) ? _trans('No') : _trans('Yes') }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{-- {{ $data->appends(request()->query())->links('layouts.partials.pagination') }}--}}

                {{--@if( $data->count() == 0)
                    <div class="empty-data">
                        <img src="{{ asset('assets-admin/')}}/images/nodata.svg">
                        <h4>{{ _trans('No_data_to_show')}}</h4>
                    </div>
                @endif--}}
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection

