@extends('layouts.master')
@section('title',_trans('Our Partner'))
@section('content')
    <div class="container-fluid">
        <div class="page-title">
            <div class="row">
                <div class="col-6 col-sm-6">
                    <h3>{{ $edit ? _trans('Edit Our Partner') : _trans('Add Our Partner') }}</h3>
                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid default-dash">
        <div class="row">

            <div class="row m-t-20">
                <div class="col-12 col-sm-6">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-home">
                                    <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                    <polyline points="9 22 9 12 15 12 15 22"></polyline>
                                </svg>
                            </a></li>
                        <li class="breadcrumb-item">
                            @canany(['Our#Partner list','Our#Partner edit','Our#Partner delete'])
                                <a href="{{route('admin.our-partner.index')}}">{{ _trans('Our Partners') }}</a>
                            @endcanany
                        </li>
                        <li class="breadcrumb-item active">{{ $edit ? _trans('Edit Our Partner') : _trans('Add Our Partner') }}</li>
                    </ol>
                </div>
                <div class="col-md-12">
                    @if ($edit)
                        {{ Form::open(['route' => ['admin.our-partner.update',$ourPartner->id],'method' => 'PUT','files' => true,'class' => 'form mb-15 form-submit','id' =>'kt_contact_form']) }}
                    @else
                        {{ Form::open(['route' => 'admin.our-partner.store','method' => 'POST','files' => true,'class' => 'form mb-15','id' =>'kt_contact_form']) }}
                    @endif

                    <div class="row g-3">
                        <div class="col-md-3">
                            <label for="code" class="form-label">{{ _trans('Code') }}</label>
                            <input id="code"
                                   disabled
                                   type="text"
                                   class="form-control"
                                   placeholder="{{ _trans('Code') }}"
                                   value="{{ getCodeTable('Our#Partner','our_partners',$edit,$edit ? $ourPartner->id : null) }}"
                            >
                        </div>
                        <div class="col-md-3">
                            <label class="form-label" for="ranking">{{ _trans('Ranking') }}</label>
                            <input id="ranking"
                                   type="number"
                                   min="0"
                                   step="any"
                                   name="ranking"
                                   class="form-control @error('ranking') is-invalid @enderror"
                                   placeholder="{{ _trans('Ranking') }}"
                                   value="{{ old('ranking',$edit ? $ourPartner->ranking : 0) }}"
                            >
                            @error('name')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>


                        <div class="col-md-12">
                            <label class="form-label" for="link">{{ _trans('Link') }}</label>
                            <input id="link"
                                   type="url"
                                   name="link"
                                   class="form-control @error('link') is-invalid @enderror"
                                   placeholder="{{ _trans('Link') }}"
                                   value="{{ old('link',$edit ? $ourPartner->link : null) }}"
                            >
                            @error('name')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <div class="mb-1">
                                <label class="form-label">{{ _trans('Logo') }}</label>
                            </div>
                            <div class="p-2 border border-dashed" style="max-width:230px;">
                                <div class="row" id="logo"></div>
                            </div>
                            @error('logo')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>

                    </div>
                </div>

                <div class="col-md-12">
                    <div class="m-t-50 d-flex justify-content-end">
                        <button type="submit"
                                class="btn btn-primary">{{ $edit ?_trans('Update') : _trans('Save') }}</button>
                    </div>
                </div>
                {{ Form::close() }}

            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection
@push('scripts')
    <script src="{{ asset('assets/js/spartan-multi-image-picker.js') }}"></script>

    @include('layouts.partials.spartan-multi-image',[
   'single' => true,
   'file_name' => 'logo',
   'image' => $edit ? getAvatar($ourPartner->logo) :  asset('assets/images/img/400x400/img2.jpg')
   ])


@endpush


