@extends('layouts.master')
@section('title',$edit ? _trans('Edit Owner') : _trans('Add Owner'))
@section('content')
    <div class="container-fluid">
        <div class="page-title">
            <div class="row">
                <div class="col-6 col-sm-6">
                    <h3>{{ $edit ? _trans('Edit Owner') : _trans('Add Owner') }}</h3>
                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid default-dash">
        <div class="row">
            <div class="col-12 col-sm-6">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                        </a>
                    </li>
                    @canany(['Owner list','Owner edit','Owner delete'])
                        <li class="breadcrumb-item ">
                            <a href="{{ route('admin.owner.index') }}">{{ _trans('Owners') }}</a>
                        </li>
                    @endcanany
                    <li class="breadcrumb-item active">
                        {{ $edit ? _trans('Edit Owner') : _trans('Add Owner') }}
                    </li>
                </ol>
            </div>

            <div class="col-md-12">
                @if ($edit)
                    {{ Form::open(['route' => ['admin.owner.update',$owner->id],'method' => 'PUT','files' => true,'class' => 'form mb-15 form-submit','id' =>'kt_contact_form']) }}
                    {{ Form::hidden('id',$owner->id) }}
                @else
                    {{ Form::open(['route' => 'admin.owner.store','method' => 'POST','files' => true,'class' => 'form mb-15','id' =>'kt_contact_form']) }}
                @endif

                <div class="row g-3">
                    <div class="col-md-4">
                        <label class="form-label" for="code">{{ _trans('Owner Code') }}</label>
                        <input id="code"
                               disabled
                               type="text"
                               class="form-control"
                               placeholder="{{ _trans('Owner Code') }}"
                               value="{{ getCodeTable('Owner','owners',$edit,$edit ? $owner->id : null) }}"
                        >
                    </div>

                    @if ($edit)
                        <div class="col-md-4">
                            <label class="form-label" for="date">{{ _trans('Date') }}</label>
                            <input id="date"
                                   disabled
                                   type="text"
                                   class="form-control"
                                   value="{{ $owner->created_at->diffForHumans() }}"
                            >
                        </div>
                    @endif

                    <div class="col-md-4">
                        <label class="form-label" for="name">{{ _trans('Owner name') }}</label>
                        <input id="name"
                               type="text"
                               name="name"
                               class="form-control @error('name') is-invalid @enderror"
                               placeholder="{{ _trans('Owner name') }}"
                               value="{{ old('name',$edit ? $owner->user->name : null) }}"
                        >
                        @error('name')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="form-label" for="email">{{ _trans('E-mail') }}</label>
                        <input id="email"
                               type="email"
                               name="email"
                               class="form-control @error('email') is-invalid @enderror"
                               placeholder="{{ _trans('E-mail') }}"
                               value="{{ old('email',$edit ? $owner->user->email : null) }}"
                        >
                        @error('email')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>


                    @if (!$edit)
                        <div class="col-md-4">
                            <label class="form-label" for="password">{{ _trans('Password') }}</label>
                            <input id="password"
                                   type="password"
                                   name="password"
                                   class="form-control @error('password') is-invalid @enderror"
                                   placeholder="{{ _trans('Password') }}"
                                   value="{{ old('password') }}"
                            >
                            @error('password')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>
                        <div class="col-md-4">
                            <label class="form-label" for="password_confirmation">{{ _trans('Confirm Password') }}</label>
                            <input id="password_confirmation"
                                   type="password"
                                   name="password_confirmation"
                                   class="form-control @error('password_confirmation') is-invalid @enderror"
                                   placeholder="{{ _trans('Confirm Password') }}"
                                   value="{{ old('password_confirmation') }}"
                            >
                            @error('confirm_password')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>
                    @endif

                    <div class="col-md-4">
                        <label class="form-label" for="phone">{{ _trans('Phone') }}</label>
                        <input id="phone"
                               type="text"
                               name="phone"
                               class="form-control @error('phone') is-invalid @enderror"
                               placeholder="{{ _trans('Phone') }}"
                               value="{{ old('phone',$edit ? $owner->phone : null) }}"
                        >
                        @error('phone')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="form-label" for="country_id">{{ _trans('Country Name') }}</label>
                        <select id="country_id" name="country_id"
                                class="js-example-basic-single @error('country_id') is-invalid @enderror">
                            <option value="">{{ _trans('Select Country Name') }}</option>
                            @foreach($countries as $country)
                                <option value="{{ $country->id }}"
                                    @selected(old('country_id',$edit ? $owner->user->country_id : null) == $country->id)>
                                    {{ $country->translate(locale())?->name }}</option>
                            @endforeach
                        </select>
                        @error('country_id')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="form-label" for="governorate_id">{{ _trans('Governorate Name') }}</label>
                        <select id="governorate_id" name="governorate_id"
                                class="js-example-basic-single @error('governorate_id') is-invalid @enderror">
                            <option value="">{{ _trans('Select Governorate Name') }}</option>

                        </select>
                        @error('governorate_id')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="form-label" for="region_id">{{ _trans('Region Name') }}</label>
                        <select id="region_id" name="region_id"
                                class="js-example-basic-single @error('region_id') is-invalid @enderror">
                            <option value="">{{ _trans('Select Region Name') }}</option>
                        </select>
                        @error('region_id')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="form-label" for="address">{{ _trans('Address') }}</label>
                        <input id="address"
                               type="text"
                               name="address"
                               class="form-control @error('address') is-invalid @enderror"
                               placeholder="{{ _trans('Address') }}"
                               value="{{ old('address',$edit ? $owner->address : null) }}"
                        >
                        @error('address')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="form-label" for="brand_name">{{ _trans('Brand name') }}</label>
                        <input id="brand_name"
                               type="text"
                               name="brand_name"
                               class="form-control @error('brand_name') is-invalid @enderror"
                               placeholder="{{ _trans('Brand name') }}"
                               value="{{ old('brand_name',$edit ? $owner->brand_name : null) }}"
                        >
                        @error('brand_name')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <div class="col-md-6">
                            <div class="mb-1">
                                <label class="form-label">{{ _trans('Avatar') }}</label>
                            </div>
                            <div class="p-2 border border-dashed" style="max-width:230px;">
                                <div class="row" id="avatar"></div>
                            </div>
                            @error('avatar')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>

                    </div>

                    <div class="col-md-12">
                        <div class="m-t-50 d-flex justify-content-end">
                            <button type="submit" class="btn btn-primary">{{ $edit ?_trans('Update') : _trans('Save') }}</button>
                        </div>
                    </div>
                </div>

                {{ Form::close() }}
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection
@include('layouts.partials.read-photo',['inputName' => 'avatar'])
@push('scripts')
    <script src="{{ asset('assets/js/spartan-multi-image-picker.js') }}"></script>
    @include('layouts.partials.spartan-multi-image',[
    'single' => true,
    'file_name' => 'avatar',
    'image' => $edit ? getAvatar($owner->user->avatar) :  asset('assets/images/img/400x400/img2.jpg')
    ])

@endpush
@push('scripts')
    <script>
        $(document).ready(function () {
            getGovernorate('{{ old('country_id',$edit ? $owner->user->country_id : null) }}', '{{ old('governorate_id',$edit ? $owner->user->governorate_id : null)  }}')
            getRegion('{{ old('governorate_id',$edit ? $owner->user->governorate_id : null) }}', '{{ old('region_id',$edit ? $owner->user->region_id : null) }}')
        })
    </script>

    @include('layouts.ajax.countries')

@endpush


