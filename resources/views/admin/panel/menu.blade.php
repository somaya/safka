<div>
    <div class="logo-wrapper"><a href="{{ route('admin.dashboard') }}">
            <img src="{{ getAvatar(Utility::getValByName('web_logo') ) }}">
        </a>
        <div class="back-btn"><i class="fa fa-angle-left"></i></div>
    </div>
    <div class="logo-icon-wrapper"><a href="{{ route('admin.dashboard') }}">Logo</a></div>
    <nav class="sidebar-main">
        <div class="left-arrow" id="left-arrow"><i data-feather="arrow-left"></i></div>
        <div id="sidebar-menu">
            <ul class="sidebar-links" id="simple-bar">
                <li class="back-btn">
                    <a href="{{ url('/') }}">
                        <img class="img-fluid" src="{{ getAvatar(Utility::getValByName('web_logo') ) }}" alt=""></a>
                    <div class="mobile-back text-end">
                        <span>{{ _trans('Back') }}</span>
                        <i class="fa fa-angle-right ps-2" aria-hidden="true">
                        </i>
                    </div>
                </li>

                <li class="sidebar-list {{ menuRoute('admin.dashboard') }}">
                    <a class="sidebar-link sidebar-title link-nav" href="{{ route('admin.dashboard') }}">
                        <div class="curve1"></div>
                        <div class="curve2"></div>
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g>
                                <g>
                                    <path d="M15.596 15.6963H8.37598" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                    <path d="M15.596 11.9365H8.37598" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                    <path d="M11.1312 8.17725H8.37622" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                          d="M3.61011 12C3.61011 18.937 5.70811 21.25 12.0011 21.25C18.2951 21.25 20.3921 18.937 20.3921 12C20.3921 5.063 18.2951 2.75 12.0011 2.75C5.70811 2.75 3.61011 5.063 3.61011 12Z" stroke="#130F26"
                                          stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                </g>
                            </g>
                        </svg>
                        <span>{{ _trans('Dashboard') }}</span>
                    </a>
                </li>

                @canany(['Owner list','Owner add','Owner edit','Owner delete'])
                    <li class="sidebar-list {{ menuRoute('admin/owner*','li') }}">
                        <a class="sidebar-link sidebar-title link-nav" href="{{ route('admin.owner.index') }}">
                            <div class="curve1"></div>
                            <div class="curve2"></div>
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g>
                                    <g>
                                        <path d="M15.596 15.6963H8.37598" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path d="M15.596 11.9365H8.37598" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path d="M11.1312 8.17725H8.37622" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M3.61011 12C3.61011 18.937 5.70811 21.25 12.0011 21.25C18.2951 21.25 20.3921 18.937 20.3921 12C20.3921 5.063 18.2951 2.75 12.0011 2.75C5.70811 2.75 3.61011 5.063 3.61011 12Z" stroke="#130F26"
                                              stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                    </g>
                                </g>
                            </svg>
                            <span>{{ _trans('Owners') }}</span>
                        </a>
                    </li>
                @endcanany

                @canany(['Shop list','Shop add','Shop edit','Shop delete'])
                    <li class="sidebar-list {{ menuRoute('admin/shop*','li') }}">
                        <a class="sidebar-link sidebar-title link-nav" href="{{ route('admin.shop.index') }}">
                            <div class="curve1"></div>
                            <div class="curve2"></div>
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g>
                                    <g>
                                        <path d="M15.596 15.6963H8.37598" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path d="M15.596 11.9365H8.37598" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path d="M11.1312 8.17725H8.37622" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M3.61011 12C3.61011 18.937 5.70811 21.25 12.0011 21.25C18.2951 21.25 20.3921 18.937 20.3921 12C20.3921 5.063 18.2951 2.75 12.0011 2.75C5.70811 2.75 3.61011 5.063 3.61011 12Z" stroke="#130F26"
                                              stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                    </g>
                                </g>
                            </svg>
                            <span>{{ _trans('Shops') }}</span>
                        </a>
                    </li>
                @endcanany

                @canany(['Category list','Category add','Category edit','Category delete',
                            'Product list','Product add','Product edit','Product delete',
                            'Offer list','Offer add','Offer edit','Offer delete',
                            'Save list','Save add','Save edit','Save delete',
                            'Discount list','Discount add','Discount edit','Discount delete',
                        ])
                    <li class="sidebar-list ">
                        <a class="sidebar-link sidebar-title" href="#">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g>
                                    <g>
                                        <path d="M9.07861 16.1355H14.8936" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M2.3999 13.713C2.3999 8.082 3.0139 8.475 6.3189 5.41C7.7649 4.246 10.0149 2 11.9579 2C13.8999 2 16.1949 4.235 17.6539 5.41C20.9589 8.475 21.5719 8.082 21.5719 13.713C21.5719 22 19.6129 22 11.9859 22C4.3589 22 2.3999 22 2.3999 13.713Z"
                                              stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                    </g>
                                </g>
                            </svg>
                            <span>{{_trans('Menus And Offers')}}</span></a>
                        <ul class="sidebar-submenu">
                            @canany(['Category list','Category add','Category edit','Category delete'])
                                <li class="{{ menuRoute('admin/category*','li') }}">
                                    <a href="{{ route('admin.category.index') }}">{{ _trans('Categories') }}</a></li>
                            @endcanany
                            @canany(['Product list','Product add','Product edit','Product delete'])
                                <li class="{{menuRoute('product*','li') }}">
                                    <a href="{{ route('product.index') }}">{{_trans('Products')}}</a></li>
                            @endcanany

                            @canany(['Offer list','Offer add','Offer edit','Offer delete'])
                                <li class="{{ menuRoute('offer*','li') }}">
                                    <a href="{{ route('offer.index') }}">{{_trans('Offers')}}</a></li>
                            @endcanany
                            @canany(['Save list','Save add','Save edit','Save delete'])
                                <li class="{{ menuRoute('save*','li') }}">
                                    <a href="{{ route('save.index') }}">{{_trans('Saves')}}</a></li>
                            @endcanany
                            @canany(['Discount list','Discount add','Discount edit','Discount delete'])
                                <li class="{{ menuRoute('discount*','li') }}">
                                    <a href="{{ route('discount.index') }}">{{_trans('Discounts')}}</a></li>
                            @endcanany
                        </ul>
                    </li>
                @endcanany

{{--                <li class="sidebar-list ">--}}
{{--                    <a class="sidebar-link sidebar-title" href="#">--}}
{{--                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">--}}
{{--                            <g>--}}
{{--                                <g>--}}
{{--                                    <path d="M9.07861 16.1355H14.8936" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>--}}
{{--                                    <path fill-rule="evenodd" clip-rule="evenodd"--}}
{{--                                          d="M2.3999 13.713C2.3999 8.082 3.0139 8.475 6.3189 5.41C7.7649 4.246 10.0149 2 11.9579 2C13.8999 2 16.1949 4.235 17.6539 5.41C20.9589 8.475 21.5719 8.082 21.5719 13.713C21.5719 22 19.6129 22 11.9859 22C4.3589 22 2.3999 22 2.3999 13.713Z"--}}
{{--                                          stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>--}}
{{--                                </g>--}}
{{--                            </g>--}}
{{--                        </svg>--}}
{{--                        <span>{{_trans('Delivery')}}</span></a>--}}
{{--                    <ul class="sidebar-submenu">--}}
{{--                        @canany(['Delivery list','Delivery add','Delivery edit','Delivery delete'])--}}
{{--                            <li class="{{ menuRoute('delivery-area*','li') }}">--}}
{{--                                <a href="{{ route('delivery-area.index') }}">{{_trans('Delivery Areas')}}</a>--}}
{{--                            </li>--}}
{{--                        @endcanany--}}

{{--                        @canany(['VehicleType list','VehicleType add','VehicleType edit','VehicleType delete'])--}}
{{--                            <li class="{{ menuRoute('admin/vehicle-type*','li') }}">--}}
{{--                                <a href="{{ route('admin.vehicle-type.index') }}">{{_trans('Vehicle type')}}</a>--}}
{{--                            </li>--}}
{{--                        @endcanany--}}

{{--                        @canany(['Driver list','Driver add','Driver edit','Driver delete'])--}}
{{--                            <li class="{{ menuRoute('driver','li') }}">--}}
{{--                                <a href="{{ route('driver.index') }}">{{_trans('Drivers')}}</a>--}}
{{--                            </li>--}}
{{--                        @endcanany--}}

{{--                        @canany(['DriverOrder list','DriverOrder add','DriverOrder edit','DriverOrder delete'])--}}
{{--                            <li class="{{ menuRoute('driver-order*','li') }}">--}}
{{--                                <a href="{{ route('driver-order.index') }}">{{_trans('Delivery Order')}}</a>--}}
{{--                            </li>--}}
{{--                        @endcanany--}}

{{--                    </ul>--}}
{{--                </li>--}}

                @canany(['Role list','Role add','Role edit','Role delete',
                          'Employee list','Employee add','Employee edit','Employee delete',
                          'Role#Staff list','Role#Staff add','Role#Staff edit','Role#Staff delete',
                           'Staff list','Staff add','Staff edit','Staff delete',
                        ])
                    <li class="sidebar-list ">
                        <a class="sidebar-link sidebar-title" href="#">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g>
                                    <g>
                                        <path d="M9.07861 16.1355H14.8936" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M2.3999 13.713C2.3999 8.082 3.0139 8.475 6.3189 5.41C7.7649 4.246 10.0149 2 11.9579 2C13.8999 2 16.1949 4.235 17.6539 5.41C20.9589 8.475 21.5719 8.082 21.5719 13.713C21.5719 22 19.6129 22 11.9859 22C4.3589 22 2.3999 22 2.3999 13.713Z"
                                              stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                    </g>
                                </g>
                            </svg>
                            <span>{{_trans('Employees')}}</span></a>
                        <ul class="sidebar-submenu">
                            @canany(['Role#Staff list','Role#Staff add','Role#Staff edit','Role#Staff delete'])
                                <li class="{{ menuRoute('role*','li') }}">
                                    <a href="{{ route('role.index') }}">{{ _trans('Role Staff') }}</a></li>
                            @endcanany
                            @canany(['Staff list','Staff add','Staff edit','Staff delete'])
                                <li class="{{ menuRoute('staff*','li') }}">
                                    <a href="{{ route('staff.index') }}">{{ _trans('Staff') }}</a></li>
                            @endcanany
                            @canany(['Role list','Role add','Role edit','Role delete'])
                                <li class="{{ menuRoute('admin/role*','li') }}">
                                    <a href="{{ route('admin.role.index') }}">{{ _trans('Roles') }}</a></li>
                            @endcanany
                            @canany(['Employee list','Employee add','Employee edit','Employee delete'])
                                <li class="{{ menuRoute('admin/employee*','li') }}">
                                    <a href="{{ route('admin.employee.index') }}">{{ _trans('Employees') }}</a></li>
                            @endcanany

                        </ul>
                    </li>
                @endcanany
                @canany(['Payment list','Payment add','Payment edit','Payment delete'])
                    <li class="sidebar-list {{ menuRoute('admin/payment*','li') }}">
                        <a class="sidebar-link sidebar-title link-nav" href="{{ route('admin.payment.index') }}">
                            <div class="curve1"></div>
                            <div class="curve2"></div>
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g>
                                    <g>
                                        <path d="M15.596 15.6963H8.37598" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path d="M15.596 11.9365H8.37598" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path d="M11.1312 8.17725H8.37622" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M3.61011 12C3.61011 18.937 5.70811 21.25 12.0011 21.25C18.2951 21.25 20.3921 18.937 20.3921 12C20.3921 5.063 18.2951 2.75 12.0011 2.75C5.70811 2.75 3.61011 5.063 3.61011 12Z" stroke="#130F26"
                                              stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                    </g>
                                </g>
                            </svg>
                            <span>{{ _trans('Payments') }}</span>
                        </a>
                    </li>
                @endcanany

                @canany(['Customer list','Customer Customer','Customer edit','Customer delete'])
                    <li class="sidebar-list {{ menuRoute('admin/customer*','li') }}">
                        <a class="sidebar-link sidebar-title link-nav" href="{{ route('admin.customer.index') }}">
                            <div class="curve1"></div>
                            <div class="curve2"></div>
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g>
                                    <g>
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M2.75024 12C2.75024 5.063 5.06324 2.75 12.0002 2.75C18.9372 2.75 21.2502 5.063 21.2502 12C21.2502 18.937 18.9372 21.25 12.0002 21.25C5.06324 21.25 2.75024 18.937 2.75024 12Z" stroke="#130F26"
                                              stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path d="M15.2045 13.8999H15.2135" stroke="#130F26" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path d="M12.2045 9.8999H12.2135" stroke="#130F26" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path d="M9.19557 13.8999H9.20457" stroke="#130F26" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                    </g>
                                </g>
                            </svg>
                            <span>{{_trans('Customers')}}</span>
                        </a>
                    </li>
                @endcanany

               @canany(['Order list','Order add','Order edit','Order delete'])
                    <li class="sidebar-list {{ menuRoute('order*','li') }}">
                        <a class="sidebar-link sidebar-title link-nav" href="{{ route('order.index') }}">
                            <div class="curve1"></div>
                            <div class="curve2"></div>
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g>
                                    <g>
                                        <path d="M15.596 15.6963H8.37598" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path d="M15.596 11.9365H8.37598" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path d="M11.1312 8.17725H8.37622" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M3.61011 12C3.61011 18.937 5.70811 21.25 12.0011 21.25C18.2951 21.25 20.3921 18.937 20.3921 12C20.3921 5.063 18.2951 2.75 12.0011 2.75C5.70811 2.75 3.61011 5.063 3.61011 12Z" stroke="#130F26"
                                              stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                    </g>
                                </g>
                            </svg>
                            <span>{{ _trans('Orders') }}</span>
                        </a>
                    </li>
                @endcanany
                @canany(['Rate list','Rate add','Rate edit','Rate delete'])
                    <li class="sidebar-list {{ menuRoute('admin/rate*','li') }}">
                        <a class="sidebar-link sidebar-title link-nav" href="{{ route('admin.rate.index') }}">
                            <div class="curve1"></div>
                            <div class="curve2"></div>
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g>
                                    <g>
                                        <path d="M15.596 15.6963H8.37598" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path d="M15.596 11.9365H8.37598" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path d="M11.1312 8.17725H8.37622" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M3.61011 12C3.61011 18.937 5.70811 21.25 12.0011 21.25C18.2951 21.25 20.3921 18.937 20.3921 12C20.3921 5.063 18.2951 2.75 12.0011 2.75C5.70811 2.75 3.61011 5.063 3.61011 12Z" stroke="#130F26"
                                              stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                    </g>
                                </g>
                            </svg>
                            <span>{{ _trans('Rates') }}</span>
                        </a>
                    </li>
                @endcanany

               @canany(['ContactUs list','ContactUs add','ContactUs edit','ContactUs delete'])
                    <li class="sidebar-list {{ menuRoute('admin/contact-us*','li') }}">
                        <a class="sidebar-link sidebar-title link-nav" href="{{ route('admin.contact-us.index') }}">
                            <div class="curve1"></div>
                            <div class="curve2"></div>
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g>
                                    <g>
                                        <path d="M15.596 15.6963H8.37598" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path d="M15.596 11.9365H8.37598" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path d="M11.1312 8.17725H8.37622" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M3.61011 12C3.61011 18.937 5.70811 21.25 12.0011 21.25C18.2951 21.25 20.3921 18.937 20.3921 12C20.3921 5.063 18.2951 2.75 12.0011 2.75C5.70811 2.75 3.61011 5.063 3.61011 12Z" stroke="#130F26"
                                              stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                    </g>
                                </g>
                            </svg>
                            <span>{{ _trans('Contact us') }}</span>
                        </a>
                    </li>
                @endcanany

                @canany(['Chat list','Chat add','Chat edit','Chat delete'])
                    <li class="sidebar-list {{ menuRoute('admin/chat*','li') }}">
                        <a class="sidebar-link sidebar-title link-nav" href="{{ route('admin.chat.index') }}">
                            <div class="curve1"></div>
                            <div class="curve2"></div>
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g>
                                    <g>
                                        <path d="M15.596 15.6963H8.37598" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path d="M15.596 11.9365H8.37598" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path d="M11.1312 8.17725H8.37622" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M3.61011 12C3.61011 18.937 5.70811 21.25 12.0011 21.25C18.2951 21.25 20.3921 18.937 20.3921 12C20.3921 5.063 18.2951 2.75 12.0011 2.75C5.70811 2.75 3.61011 5.063 3.61011 12Z" stroke="#130F26"
                                              stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                    </g>
                                </g>
                            </svg>
                            <span>{{ _trans('Chats') }}</span>
                        </a>
                    </li>
                @endcanany
                @canany(['Live#Chat list','Live#Chat add','Live#Chat edit','Live#Chat delete'])
                    <li class="sidebar-list {{ menuRoute('live-chat*','li') }}">
                        <a class="sidebar-link sidebar-title link-nav" href="{{ route('live-chat.index') }}">
                            <div class="curve1"></div>
                            <div class="curve2"></div>
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g>
                                    <g>
                                        <path d="M15.596 15.6963H8.37598" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path d="M15.596 11.9365H8.37598" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path d="M11.1312 8.17725H8.37622" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M3.61011 12C3.61011 18.937 5.70811 21.25 12.0011 21.25C18.2951 21.25 20.3921 18.937 20.3921 12C20.3921 5.063 18.2951 2.75 12.0011 2.75C5.70811 2.75 3.61011 5.063 3.61011 12Z" stroke="#130F26"
                                              stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                    </g>
                                </g>
                            </svg>
                            <span>{{ _trans('Live Chats') }}</span>
                        </a>
                    </li>
                @endcanany

               @canany(['JoinUs list','JoinUs add','JoinUs edit','JoinUs delete'])
                    <li class="sidebar-list {{ menuRoute('admin/join-us*','li') }}">
                        <a class="sidebar-link sidebar-title link-nav" href="{{ route('admin.join-us.index') }}">
                            <div class="curve1"></div>
                            <div class="curve2"></div>
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g>
                                    <g>
                                        <path d="M15.596 15.6963H8.37598" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path d="M15.596 11.9365H8.37598" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path d="M11.1312 8.17725H8.37622" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M3.61011 12C3.61011 18.937 5.70811 21.25 12.0011 21.25C18.2951 21.25 20.3921 18.937 20.3921 12C20.3921 5.063 18.2951 2.75 12.0011 2.75C5.70811 2.75 3.61011 5.063 3.61011 12Z" stroke="#130F26"
                                              stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                    </g>
                                </g>
                            </svg>
                            <span>{{ _trans('Join us') }}</span>
                        </a>
                    </li>
                @endcanany

                {{--@canany(['Chart list','Chart add','Chart edit','Chart delete'])
                    <li class="sidebar-list ">
                        <a class="sidebar-link sidebar-title" href="#">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g>
                                    <g>
                                        <path d="M9.07861 16.1355H14.8936" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M2.3999 13.713C2.3999 8.082 3.0139 8.475 6.3189 5.41C7.7649 4.246 10.0149 2 11.9579 2C13.8999 2 16.1949 4.235 17.6539 5.41C20.9589 8.475 21.5719 8.082 21.5719 13.713C21.5719 22 19.6129 22 11.9859 22C4.3589 22 2.3999 22 2.3999 13.713Z"
                                              stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                    </g>
                                </g>
                            </svg>
                            <span>{{_trans('Charts')}}</span></a>
                        <ul class="sidebar-submenu">

                            <li class="{{ menuRoute('admin/reports/restaurant-city-chart','li') }}">
                                <a href="{{ route('admin.reports.restaurant-city-chart') }}">{{ _trans('Restaurant By City Chart') }}</a>
                            </li>
                            <li class="{{ menuRoute('reports/restaurant-orders-chart','li') }}">
                                <a href="{{ route('reports.restaurant-orders-chart') }}">{{ _trans('Restaurant By Orders Chart')  }}</a>
                            </li>

                            <li class="{{ menuRoute('admin/reports/product-order-chart','li')  }}">
                                <a href="{{ route('admin.reports.product-order-chart') }}">{{_trans('Product By order Chart')}}</a>
                            </li>
                            <li class="{{ menuRoute('admin/reports/offer-order-chart','li')  }}">
                                <a href="{{ route('admin.reports.offer-order-chart') }}">{{_trans('Offer By order Chart')}}</a>
                            </li>
                            <li class="{{ menuRoute('admin/reports/save-order-chart','li')  }}">
                                <a href="{{ route('admin.reports.save-order-chart') }}">{{_trans('Save By order Chart')}}</a>
                            </li>
                            <li class="{{ menuRoute('admin/reports/discount-order-chart','li')  }}">
                                <a href="{{ route('admin.reports.discount-order-chart') }}">{{_trans('Discount By order Chart')}}</a>
                            </li>
                            <li class="{{ menuRoute('reports/customer-order-chart','li')  }}">
                                <a href="{{ route('reports.customer-order-chart') }}">{{_trans('Customer By order Chart')}}</a>
                            </li>
                            <li class="{{ menuRoute('reports/customer-order-total-chart','li')  }}">
                                <a href="{{ route('reports.customer-order-total-chart') }}">{{_trans('Customer By order Total Chart')}}</a>
                            </li>
                            <li class="{{ menuRoute('admin/reports/owner-order-chart','li')  }}">
                                <a href="{{ route('admin.reports.owner-order-chart') }}">{{_trans('Owner By order Chart')}}</a>
                            </li>
                            <li class="{{ menuRoute('reports/restaurant-order-paid-type-chart','li')  }}">
                                <a href="{{ route('reports.restaurant-order-paid-type-chart') }}">{{_trans('Restaurant Order Paid Type Chart')}}</a>
                            </li>
                            <li class="{{ menuRoute('admin/reports/restaurant-participation-chart','li')  }}">
                                <a href="{{ route('admin.reports.restaurant-participation-chart') }}">{{_trans('Restaurant Participation Chart')}}</a>
                            </li>
                            <li class="{{ menuRoute('admin/reports/restaurant-percent-chart','li')  }}">
                                <a href="{{ route('admin.reports.restaurant-percent-chart') }}">{{_trans('Restaurant Percent Chart')}}</a>
                            </li>
                            <li class="{{ menuRoute('admin/reports/product-rate-chart','li') }}">
                                <a href="{{ route('admin.reports.product-rate-chart') }}">{{_trans('Product Rate Chart')}}</a>
                            </li>
                            <li class="{{ menuRoute('admin/reports/restaurant-rate-chart','li') }}">
                                <a href="{{ route('admin.reports.restaurant-rate-chart') }}">{{_trans('Restaurant Rate Chart')}}</a>
                            </li>
                            <li class="{{ menuRoute('admin/reports/restaurant-transaction-chart','li') }}">
                                <a href="{{ route('admin.reports.restaurant-transaction-chart') }}">{{_trans('Restaurant Transaction Chart')}}</a>
                            </li>

                        </ul>
                    </li>
                @endcanany--}}

                @canany(['Blog list','Blog add','Blog edit','Blog delete',
                          'Blog Category list','Blog Category add','Blog Category edit','Blog Category delete',
                          ])
                    <li class="sidebar-list ">
                        <a class="sidebar-link sidebar-title" href="#">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g>
                                    <g>
                                        <path d="M9.07861 16.1355H14.8936" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M2.3999 13.713C2.3999 8.082 3.0139 8.475 6.3189 5.41C7.7649 4.246 10.0149 2 11.9579 2C13.8999 2 16.1949 4.235 17.6539 5.41C20.9589 8.475 21.5719 8.082 21.5719 13.713C21.5719 22 19.6129 22 11.9859 22C4.3589 22 2.3999 22 2.3999 13.713Z"
                                              stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                    </g>
                                </g>
                            </svg>
                            <span>{{_trans('Blogs')}}</span></a>
                        <ul class="sidebar-submenu">
                             @canany(['Blog list','Blog add','Blog edit','Blog delete'])
                                 <li class="{{ menuRoute('admin/blogs*','li') }}">
                                     <a href="{{ route('admin.blogs.index') }}">{{ _trans('Blogs') }}</a></li>
                             @endcanany

                            @canany(['Blog Category list','Blog Category add','Blog Category edit','Blog Category delete'])
                                <li class="{{ menuRoute('admin/blog-category*','li') }}">
                                    <a href="{{ route('admin.blog-category.index') }}">{{ _trans('Blog Categories')  }}</a></li>
                            @endcanany


                        </ul>
                    </li>
                @endcanany

                @canany(['Coupon list','Coupon add','Coupon edit','Coupon delete',
                          'Branch#Type list','Branch#Type add','Branch#Type edit','Branch#Type delete',
                          'Notification list','Notification add','Notification edit','Notification delete',
                          'Country list','Country add','Country edit','Country delete',
                          'Governorate list','Governorate add','Governorate edit','Governorate delete',
                          'Region list','Region add','Region edit','Region delete',
                          'Language list','Language add','Language edit','Language delete',
                          'Currency list','Currency add','Currency edit','Currency delete',
                          'Social#Media list','Social#Media add','Social#Media edit','Social#Media delete',
                          'Page#Setup list','Page#Setup add','Page#Setup edit','Page#Setup delete',
                          'Banner list','Banner add','Banner edit','Banner delete',
                          'SocialMedia list','SocialMedia add','SocialMedia edit','SocialMedia delete',
                          'Setting list',
                          'Day list','Day add','Day edit','Day delete',
//                          'Our#Partner list','Our#Partner add','Our#Partner edit','Our#Partner delete',
//                          'Service list','Service add','Service edit','Service delete',
                          ])
                    <li class="sidebar-list ">
                        <a class="sidebar-link sidebar-title" href="#">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g>
                                    <g>
                                        <path d="M9.07861 16.1355H14.8936" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M2.3999 13.713C2.3999 8.082 3.0139 8.475 6.3189 5.41C7.7649 4.246 10.0149 2 11.9579 2C13.8999 2 16.1949 4.235 17.6539 5.41C20.9589 8.475 21.5719 8.082 21.5719 13.713C21.5719 22 19.6129 22 11.9859 22C4.3589 22 2.3999 22 2.3999 13.713Z"
                                              stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                    </g>
                                </g>
                            </svg>
                            <span>{{_trans('Settings')}}</span></a>
                        <ul class="sidebar-submenu">
                            @canany(['Coupon list','Coupon add','Coupon edit','Coupon delete'])
                                <li class="{{ menuRoute('admin/coupon*','li') }}">
                                    <a href="{{ route('admin.coupon.index') }}">{{ _trans('Coupons') }}</a></li>
                            @endcanany
                            @canany(['Company list','Company add','Company edit','Company delete'])
                                <li class="{{ menuRoute('admin/company*','li') }}">
                                    <a href="{{ route('admin.company.index') }}">{{ _trans('Companies') }}</a></li>
                            @endcanany
{{--                            @canany(['Service list','Service add','Service edit','Service delete'])--}}
{{--                                <li class="{{ menuRoute('admin/service*','li') }}">--}}
{{--                                    <a href="{{ route('admin.service.index') }}">{{ _trans('Services') }}</a></li>--}}
{{--                            @endcanany--}}
                            @canany(['Branch#Type list','Branch#Type add','Branch#Type edit','Branch#Type delete'])
                                <li class="{{ menuRoute('admin/branch-type*','li') }}">
                                    <a href="{{ route('admin.branch-type.index') }}">{{ _trans('BranchType') }}</a></li>
                            @endcanany

                            @canany(['Notification list','Notification add','Notification edit','Notification delete'])
                                <li class="{{ menuRoute('admin/notification*','li') }}">
                                    <a href="{{ route('admin.notification.index') }}">{{_trans('Notifications')}}</a>
                                </li>
                            @endcanany

                            @canany(['Country list','Country add','Country edit','Country delete'])
                                <li class="{{ menuRoute('admin/country*','li') }}">
                                    <a href="{{ route('admin.country.index') }}">{{_trans('Countries')}}</a></li>
                            @endcanany

                            @canany(['Governorate list','Governorate add','Governorate edit','Governorate delete'])
                                <li class="{{ menuRoute('admin/governorate*','li') }}">
                                    <a href="{{ route('admin.governorate.index') }}">{{_trans('Governorates')}}</a></li>
                            @endcanany

                            @canany(['Region list','Region add','Region edit','Region delete'])
                                <li class="{{ menuRoute('admin/region*','li') }}">
                                    <a href="{{ route('admin.region.index') }}">{{_trans('Regions')}}</a></li>
                            @endcanany
                            @canany(['Day list','Day add','Day edit','Day delete'])
                                <li class="{{ menuRoute('admin/day*','li') }}">
                                    <a href="{{ route('admin.day.index') }}">{{_trans('Days')}}</a></li>
                            @endcanany
                            @canany(['Language list','Language add','Language edit','Language delete'])
                                <li class="{{ menuRoute('admin/language*','li') }}">
                                    <a href="{{ route('admin.language.index') }}">{{_trans('Languages')}}</a></li>
                            @endcanany

                            @canany(['Currency list','Currency add','Currency edit','Currency delete'])
                                <li class="{{ menuRoute('admin/currency*','li') }}">
                                    <a href="{{ route('admin.currency.index') }}">{{_trans('Currencies')}}</a></li>
                            @endcanany
{{--                            @canany(['Our#Partner list','Our#Partner add','Our#Partner edit','Our#Partner delete'])--}}
{{--                                <li class="{{ menuRoute('admin/our-partner*','li') }}">--}}
{{--                                    <a href="{{ route('admin.our-partner.index') }}">{{ _trans('Our Partners') }}</a></li>--}}
{{--                            @endcanany--}}

                            @canany(['Banner list','Banner add','Banner edit','Banner delete'])
                                <li class="{{ menuRoute('admin/banner*','li') }}">
                                    <a href="{{ route('admin.banner.index') }}">{{ _trans('Banner') }}</a></li>
                            @endcanany

                            @canany(['SocialMedia list','SocialMedia add','SocialMedia edit','SocialMedia delete'])
                                <li class="{{ menuRoute('admin/social-media*','li') }}">
                                    <a href="{{ route('admin.social-media.index') }}">{{ _trans('Social Media') }}</a></li>
                            @endcanany

                            @canany(['Page#Setup list','Page#Setup add','Page#Setup edit','Page#Setup delete'])
                                <li class="{{ menuRoute('admin/page*','li') }}">
                                    <a href="{{ route('admin.page.index') }}">{{ _trans('Page Setup') }}</a></li>
                            @endcanany

                            @canany(['Setting list'])
                                <li class="{{ menuRoute('admin/setting*','li') }}">
                                    <a href="{{ route('admin.setting.index') }}">{{_trans('Setting')}}</a></li>
                            @endcanany
                                <li class="{{ menuRoute('admin/profile/account','li') }}">
                                    <a href="{{route('admin.profile.account')}}">{{_trans('Account Setting')}}</a></li>

                        </ul>
                    </li>
                @endcanany

                <li class="sidebar-list">
                    <a class="sidebar-link sidebar-title link-nav" href="{{ route('admin.logout') }}">
                        <div class="curve1"></div>
                        <div class="curve2"></div>
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g>
                                <g>
                                    <path d="M15.596 15.6963H8.37598" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                    <path d="M15.596 11.9365H8.37598" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                    <path d="M11.1312 8.17725H8.37622" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                          d="M3.61011 12C3.61011 18.937 5.70811 21.25 12.0011 21.25C18.2951 21.25 20.3921 18.937 20.3921 12C20.3921 5.063 18.2951 2.75 12.0011 2.75C5.70811 2.75 3.61011 5.063 3.61011 12Z" stroke="#130F26"
                                          stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                </g>
                            </g>
                        </svg>
                        <span>{{ _trans('Logout') }}</span>
                    </a>
                </li>
            </ul>

        </div>
        <div class="right-arrow" id="right-arrow"><i data-feather="arrow-right"></i></div>
    </nav>
</div>
