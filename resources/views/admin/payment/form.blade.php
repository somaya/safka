<div class="modal fade" id="paymentId_{{$payment->id}}" tabindex="-1" role="dialog" aria-labelledby="paymentId_{{$payment->id}}" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ _trans('Pay the shop invoice') }} {{ $payment->shop?->translate(locale())?->name }} </h3>
                <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="{{ route('admin.payment.store') }}" method="POST">
                {{ Form::hidden('payment_id',$payment->id) }}
                @csrf
                <div class="modal-body theme-form login-form p-0">
                    <div class="form-group">
                        <label for="amount" class="mb-2">{{ _trans('Amount') }}</label>
                        <input class="form-control @error('amount') @enderror"
                               value="{{ old('amount',$payment->net_amount) }}"
                               name="amount"
                               id="amount"
                        />
                        @error('reason_message')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" type="submit">{{ _trans('Pay') }}</button>
                    <button class="btn btn-danger" type="button" data-bs-dismiss="modal" aria-label="Close">{{ _trans('Close') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
