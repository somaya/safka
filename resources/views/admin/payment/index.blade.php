@extends('layouts.master')

@section('title',_trans('Payments'))

@push('styles')
    <!-- html to pdf packages Cdn -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
    <script type="text/javascript" src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>
    <!-- End -->
@endpush

@section('content')
    <div class="container-fluid">
        <div class="page-title">
            <div class="row">
                <div class="col-6 col-sm-6">
                    <h3>{{ _trans('Payments') }}</h3>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid default-dash">
        <div class="row">
            <div class="col-12 col-sm-6">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/') }}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                        </a></li>
                    <li class="breadcrumb-item active"><a href="#">{{ _trans('Payments') }}</a></li>
                </ol>
            </div>

            <div class="col-xl-12 col-md-12 ">

                <form action="{{ url()->current() }}" method="GET">
                    <div class="left-side-header">
                        <div class="row justify-content-between align-items-center">
                            <x-search :columns="$columns" sort="true"/>
                            <div class="col-md-4">
                                <p class="mb-0 font-sm d-flex align-items-center justify-content-end">{{ _trans('Count Payments') }} :
                                    <span class="d-block font-md text-danger">({{ $payments->total() }})</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </form>

                <div class="table-responsive custom-scrollbar p-t-30">
                    <table class="table">
                        <thead>
                        <tr>
                            <th><span>{{ _trans('SL')}} </span></th>
                            <th><span>{{ _trans('Shop name')}}</span></th>
                            <th><span>{{ _trans('Start date')}}</span></th>
                            <th><span>{{ _trans('End date')}}</span></th>
                            <th><span>{{ _trans('Subscription')}}</span></th>
                            <th><span>{{ _trans('Net amount')}}</span></th>
                            <th><span>{{ _trans('Amount')}}</span></th>
                            <th><span>{{ _trans('Status')}}</span></th>
                            <th><span>{{ _trans('action')}}</span></th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($payments as $key => $row)
                            <tr>
                                <td class="text-main">Invoice#{{ $row->id }}</td>
                                <td>
                                    <a href="{{ route('admin.shop.show',$row->shop_id) }}">{{ $row->shop?->translate(locale())?->name  }}</a>
                                </td>
                                <td>{{ formatDate('d-m-Y',$row->start_date)  }}</td>
                                <td>{{ formatDate('d-m-Y',$row->end_date)  }}</td>
                                <td>{{ _trans($row->subscription_type)  }}</td>
                                <td>{{ $row->net_amount  }}</td>
                                <td>{{ $row->amount  }}</td>
                                <td>
                                    {{ $row->status == 1 ? _trans('Paid') : _trans('Unpaid') }}
                                </td>

                                <td>
                                    @if($row->status == 0)
                                        @can('Payment add')
                                            <a data-bs-toggle="modal" data-bs-target="#paymentId_{{ $row->id }}" class="btn btn-primary">{{ _trans('Pay Invoice') }}</a>
                                        @endcan
                                    @else
                                        <a class="btn btn-primary">{{ _trans('Paid') }}</a>
                                    @endif
                                        <a href="/admin/payment/invoice/{{$row->id}}" class="btn btn-primary">{{ _trans('Invoice') }}</a>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

                {{ $payments->appends(request()->query())->links('layouts.partials.pagination') }}

                @if( $payments->count() == 0)
                    <div class="empty-data">
                        <img src="{{ asset('assets')}}/images/nodata.svg">
                        <h4>{{ _trans('No_data_to_show')}}</h4>
                    </div>
                @endif

            </div>

        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection

@foreach($payments as $payment)
    @include('admin.payment.form',['payment'=>$payment])
@endforeach


@push('scripts')
    <script>
        $(document).ready(function () {
            @error('amount')
            $("#paymentId_{{ old('payment_id') }}").modal('show');
            @enderror
        })
    </script>
@endpush
