<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Invoice shop {{ $payment->shop?->translate('en')?->name }}</title>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400&display=swap" rel="stylesheet">

    <style>
        body{
            background-color: #fff;
            margin: 0;
            padding: 40px 0 0;
            font-family: 'Roboto', sans-serif;
        }
        h1,h2,h3,h4,h5,h6{
            font-family: 'Roboto', sans-serif;
            margin: 0;
            padding: 0;
        }
        p{
            font-family: 'Roboto', sans-serif;
            margin: 0;
            padding: 0;
        }
        .container{
            width: 80%;
            margin-right: auto;
            margin-left: auto;
        }
        .brand-section{
            margin-bottom: 50px;
        }
        .logo{
            width: 50%;
        }

        .row{
            display: flex;
            flex-wrap: wrap;
        }
        .col-6{
            width: 50%;
            flex: 0 0 auto;
        }
        .text-white{
            color: #fff;
        }
        .body-section{
            margin-bottom: 50px;
        }
        .company-details{
            float: right;
            margin-top: 40px;
        }
        .company-details p{
            font-size: 16px;
            margin-bottom: 10px;
        }
        .title{
            font-size: 25px;
            padding-bottom: 20px;
            border-bottom: 3px solid #9e9e9e;
            margin-bottom: 20px;
        }
        .sub-heading{
            color: #262626;
            margin-bottom: 8px;
            font-size: 16px;
        }
        table{
            background-color: #fff;
            width: 100%;
            border-collapse: collapse;
        }
        table thead tr{
            border-bottom: 1px solid #111;
        }
        .total{
            font-weight: 900;
            font-size: 22px;
        }
        table thead th{
            font-weight: 600;
            font-size: 16px;
        }
        table td {
            vertical-align: middle !important;
            text-align: center;
        }
        table th, table td {
            padding-top: 08px;
            padding-bottom: 08px;
        }
        .border_bottom{
            border-bottom: 1px solid #111;
        }
        .text-right{
            text-align: end;
        }
        .w-20{
            width: 20%;
        }
        .float-right{
            float: right;
        }
        .footer{
            display: flex;
            flex-direction: row;
            align-items: center;
            justify-content: space-between;
            padding: 15px 0;
        }
        .footer p{
            margin-bottom: 0;
            font-size: 18px;
            color: #111;
        }
        .mb-200{
            margin-bottom: 200px;
        }
    </style>
</head>
<body>

<div class="container">
    <div class="brand-section">
        <div class="row">
            <div class="col-6">
                <h1>Invoice shop {{ $payment->shop?->translate('en')?->name }}
                </h1>
            </div>
            <div class="col-6">
                <div class="company-details">
                    <p>Serial No: <b>Invoice#{{$payment->id}}</b>
                    </p>
                    <p>Invoice date: <b>{{ formatDate('d/m/Y',$payment->created_at) }}</b></p>
                    <p>Status: <b>{{ $payment->status == 1 ? _trans('Paid') : _trans('Unpaid')}}</b></p>
                    <p>Please pay until: <b>{{ \Carbon\Carbon::parse($payment->end_date)->addDays(Utility::getValByName('grace_period'))->format('d/m/Y') }}</b></p>
                </div>
            </div>
        </div>
    </div>


    <div class="body-section">
        <div class="row">
            <div class="col-3">
                <h2 class="title">Client Info:
                </h2>
                <p class="sub-heading"><b>Owner name</b>{{ $payment->shop?->owner?->user->name }}</p>
                <p class="sub-heading"><b>Brand name</b> {{ $payment->shop?->owner->brand_name }}</p>
                <p class="sub-heading"><b>Shop name</b>{{ $payment->shop->translate('en')?->name }}</p>
                <p class="sub-heading"><b>phone</b> {{ $payment->shop?->owner?->phone }}</p>
                <p class="sub-heading"><b>Email</b>{{ $payment->shop?->owner?->user?->email }}</p>
                <p class="sub-heading"><b>Country: </b>{{ $payment->shop?->owner?->user?->country->translate('en')?->name }}</p>
                <p class="sub-heading"><b>Governorate:</b>{{ $payment->shop?->owner?->user?->governorate->translate('en')?->name }}</p>
                <p class="sub-heading"><b>Region:</b>{{ $payment->shop?->owner?->user?->region->translate('en')?->name }}</p>
                <p class="sub-heading"><b>Address:</b>{{ $payment->shop?->owner?->address }}</p>
            </div>
        </div>
    </div>

    <div class="body-section mb-200">

        <table class="table-bordered">
            <thead>
            <tr>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Subscription</th>
                <th>Amount </th>
                <th>Subtotal</th>
                <th>Total</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>{{ formatDate('d-m-Y',$payment->start_date)  }}</td>
                <td>{{ formatDate('d-m-Y',$payment->end_date)  }}</td>
                <td>{{ _trans($payment->subscription_type)  }}</td>
                <td>{{ $payment->net_amount  }} {{ $payment->shop?->country->currency?->code }}</td>
                <td class="border_bottom">{{ $payment->net_amount  }} {{ $payment->shop?->country->currency?->code }}</td>
                <td class="border_bottom">{{ $payment->net_amount  }} {{ $payment->shop?->country->currency?->code }}</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td class="border_bottom">{{ _trans('Discount') }}</td>
                <td class="border_bottom">0 {{ $payment->shop?->country->currency?->code }}</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td class="border_bottom">{{ _trans('VAT') }}</td>
                <td class="border_bottom">0{{ $payment->shop?->country->currency?->code }}</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td class="border_bottom total"><b>{{ _trans('Total Amount') }}</b></td>
                <td class="border_bottom total"><b>{{ $payment->net_amount  }} {{ $payment->shop?->country->currency?->code }}</b></td>
            </tr>


            </tbody>
        </table>

    </div>


    <div class="footer">
        <p> <a href="#">{{Utility::getValByName('company_email')}}</a></p>
        <p>Email: &nbsp;<a href="#">{{Utility::getValByName('company_email')}}</a></p>
        <p>Phone : &nbsp;<span>{{Utility::getValByName('phone')}}</span></p>
        <p>Address : &nbsp;<span>{{Utility::getValByName('address')}}</span></p>
    </div>
</div>

</body>
</html>
