@extends('layouts.master')

@section('title',_trans('Rates'))


@section('content')
    <div class="container-fluid">
        <div class="page-title">
            <div class="row">
                <div class="col-6 col-sm-6">
                    <h3>{{ _trans('Rates') }}</h3>
                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid default-dash">
        <div class="row">
            <div class="col-12 col-sm-6">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                        </a></li>
                    <li class="breadcrumb-item active"><a href="#">{{ _trans('Rates') }}</a></li>
                </ol>
            </div>

            <div class="col-xl-12 col-md-12 ">
                <form action="{{ url()->current() }}" method="GET">
                    <div class="left-side-header">
                        <div class="row justify-content-between align-items-center">
                            <x-search :columns="$columns"/>
                            <div class="col-md-4">
                                <p class="mb-0 font-sm d-flex align-items-center justify-content-end">{{ _trans('Count Rates') }} :
                                    <span class="d-block font-md text-danger">({{ $data->total() }})</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </form>


                <div class="table-responsive custom-scrollbar p-t-30">
                    <table class="table">
                        <thead>
                        <tr>
                            <th><span>{{ _trans('SL')}} </span></th>
                            <th><span>{{ _trans('Degree')}}</span></th>
                            <th><span>{{ _trans('Model')}}</span></th>
                            <th><span>{{ _trans('Name')}}</span></th>
                            <th><span>{{ _trans('User')}}</span></th>
                            <th><span>{{ _trans('comment')}}</span></th>
                            <th><span>{{ _trans('action')}}</span></th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $key => $row)
                            <tr>
                                <td class="text-main">Rate#{{ $row->id }}</td>
                                <td>{{ $row->degree }}</td>
                                <td>
                                    @if($row->rate_type=='App\Models\Discount')
                                    {{'Discount'}}
                                    @elseif($row->rate_type=='App\Models\Product')
                                    {{'Product'}}
                                    @elseif($row->rate_type=='App\Models\Save')
                                        {{'Save'}}
                                    @elseif($row->rate_type=='App\Models\Offer')
                                        {{'Offer'}}
                                    @elseif($row->rate_type=='App\Models\Shop')
                                       {{'Shop'}}
                                    @endif


                                </td>
                                <td>{{$row->rate_type=='App\Models\Discount' ?$row->rate->product->translate(locale())?->name: $row->rate->translate(locale())?->name  }}</td>
{{--                                <td>{{$row->rate_id }}</td>--}}
                                <td>{{ $row->user->name }}</td>
                                <td>{{Str::limit(strip_tags($row->comment),15)  }}</td>


                                <td>
                                    <a href="{{ route('admin.rate.show',$row->id) }}" class="btn btn-primary">{{ _trans('View Comment') }}</a>
                                    <a onclick="return confirm('{{ _trans('Are you sure you want to delete ?') }}');" href="{{ route('admin.rate.destroy',$row->id) }}" class="btn btn-primary btn-sm">
                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                    </a>
                                </td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

                {{ $data->appends(request()->query())->links('layouts.partials.pagination') }}

                @if( $data->count() == 0)
                    <div class="empty-data">
                        <img src="{{ asset('assets')}}/images/nodata.svg">
                        <h4>{{ _trans('No_data_to_show')}}</h4>
                    </div>
                @endif

            </div>

        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection

@include('layouts.ajax.update-status',['class' => 'status','route' => route('admin.rate.update-status')])
@include('layouts.ajax.search_input')
