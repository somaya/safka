@extends('layouts.master')

@section('title',_trans('Comment'))
@section('content')
    <div class="container-fluid">
        <div class="page-title">
            <div class="row">
                <div class="col-6 col-sm-6">
                    <h3>{{_trans('Comment') }}</h3>
                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid default-dash">
        <div class="row">
            <div class="col-12 col-sm-6">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                        </a>
                    </li>
                    @canany(['Rate list','Rate edit','Rate delete'])
                        <li class="breadcrumb-item ">
                            <a href="{{ route('admin.rate.index') }}">{{ _trans('Rates') }}</a>
                        </li>
                    @endcanany

                    <li class="breadcrumb-item active">
                        {{ _trans('View Comment') }}
                    </li>
                </ol>
            </div>

            <div class="col-xl-12 col-md-12 ">
                <div class="row g-3">
                        <div class="col-md-12">
                            <label id="comment" class="form-label" for="">{{ _trans('Comment') }}</label>
                            <textarea id="comment"
                                      type="text"
                                      class="form-control textarea"
                                      name="comment"
                                      rows="5">{{ $rate->comment }}</textarea>
                        </div>

                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->

@endsection

@include('layouts.partials.ckeditor',['inputClass' => 'textarea'])

