@extends('layouts.master')
@section('title',$edit ? _trans('Edit Service') : _trans('Add Service'))
@section('content')
    <div class="container-fluid">
        <div class="page-title">
            <div class="row">
                <div class="col-6 col-sm-6">
                    <h3>{{ $edit ? _trans('Edit Service') : _trans('Add Service') }}</h3>
                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid default-dash">
        <div class="row">

            <div class="row m-t-20">
                <div class="col-12 col-sm-6">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home">
                                    <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                    <polyline points="9 22 9 12 15 12 15 22"></polyline>
                                </svg>
                            </a></li>
                        <li class="breadcrumb-item">
                            @canany(['Service list','Service edit','Service delete'])
                                <a href="{{route('admin.service.index')}}">{{ _trans('Services') }}</a>
                            @endcanany
                        </li>
                        <li class="breadcrumb-item active">{{ $edit ? _trans('Edit Service') : _trans('Add Service') }}</li>
                    </ol>
                </div>
                <div class="col-md-12">
                    @if ($edit)
                        {{ Form::open(['route' => ['admin.service.update',$service->id],'method' => 'PUT','files' => true,'class' => 'form mb-15 form-submit','id' =>'kt_contact_form']) }}
                    @else
                        {{ Form::open(['route' => 'admin.service.store','method' => 'POST','files' => true,'class' => 'form mb-15','id' =>'kt_contact_form']) }}
                    @endif

                    <div class="row g-3">
                        @foreach($languages as $lang)
                            <div class="col-md-6">
                                <label for="{{ $lang['code'] }}[name]" class="form-label">{{ _trans('Service name') }} ({{ ucfirst($lang['code']) }})</label>
                                <input id="{{ $lang['code'] }}[name]"
                                       type="text"
                                       name="{{ $lang['code'] }}[name]"
                                       class="form-control @error($lang['code'].'.name') is-invalid @enderror"
                                       placeholder="{{ _trans('Service name') }} ({{ ucfirst($lang['code']) }})"
                                       value="{{ old($lang['code'].'.name',$edit ? $service->translate($lang['code'])?->name : null) }}"
                                >
                                @error($lang['code'].'.name')
                                <span class="text-danger">{!! $message !!} </span>
                                @enderror
                            </div>
                        @endforeach

                        @foreach($languages as $lang)
                            <div class="col-md-12">
                                <label for="{{ $lang['code'] }}[description]" class="form-label">{{ _trans('Service description') }} ({{ ucfirst($lang['code']) }})</label>
                                <textarea id="{{ $lang['code'] }}[description]"
                                          class="form-control @error($lang['code'].'.description') is-invalid @enderror textarea"
                                          name="{{ $lang['code'] }}[description]"
                                          rows="5">{{ old($lang['code'].'.description',$edit ? $service->translate($lang['code'])?->description : null) }}</textarea>

                                @error($lang['code'].'.description')
                                <span class="text-danger">{!! $message !!} </span>
                                @enderror
                            </div>
                        @endforeach

                        <div class="col-md-6">
                            <label class="form-label" for="ranking">{{ _trans('Ranking') }}</label>
                            <input id="ranking"
                                   type="number"
                                   min="0"
                                   step="any"
                                   name="ranking"
                                   class="form-control @error('ranking') is-invalid @enderror"
                                   placeholder="{{ _trans('Ranking') }}"
                                   value="{{ old('ranking',$edit ? $service->ranking : 0) }}"
                            >
                            @error('code')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>

                        <div class="col-md-6">
                            <div class="mb-1">
                                <label class="form-label">{{ _trans('Image') }}</label>
                            </div>
                            <div class="p-2 border border-dashed" style="max-width:200px;">
                                <div class="row" id="image"></div>
                            </div>
                            @error('image')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>

                        {{--<div class="col-md-4">
                            <div class="mb-1">
                                <label class="form-label">{{ _trans('Service images') }}</label></div>
                            <div class="p-2 border border-dashed" style="max-width:430px;">
                                <div class="row" id="images">
                                    @if($edit)
                                        @foreach ($service->images as $key => $photo)
                                            <div class="col-6" id="image_{{ $photo->id }}">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <img style="width: 100%" height="auto"
                                                             src="{{ getAvatar($photo->full_file) }}"
                                                             alt="Service image">
                                                        <button type="button" class="btn btn-danger btn-block delete-img" id="{{ $photo->id }}">
                                                            <i class="fa fa-times"></i>
                                                        </button>

                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                            @error('images','images.*')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                            <br>
                            <br>
                        </div>--}}
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="m-t-50 d-flex justify-content-end">
                        <button type="submit" class="btn btn-primary">{{ $edit ?_trans('Update') : _trans('Save') }}</button>
                    </div>
                </div>
                {{ Form::close() }}

            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection
{{--@include('layouts.partials.ckeditor',['inputClass' => 'textarea'])--}}
{{--@push('scripts')--}}
{{--    <script src="{{ asset('assets/js/spartan-multi-image-picker.js') }}"></script>--}}
{{--    @includeWhen($edit,'layouts.ajax.delete-images',[--}}
{{--         'class_name' => 'delete-img',--}}
{{--         'route' => route('admin.service.delete-image'),--}}
{{--         'relation_id' => $edit ? $service->id : null--}}
{{--         ])--}}
{{--@endpush--}}
@push('scripts')
    <script src="{{ asset('assets/js/spartan-multi-image-picker.js') }}"></script>
    @include('layouts.partials.spartan-multi-image',[
    'single' => true,
    'file_name' => 'image',
    'image' => $edit ? getAvatar($service->image) :  asset('assets/images/img/400x400/img2.jpg')
    ])

    {{--@include('layouts.partials.spartan-multi-image',[
    'multi' => true,
    'file_name' => 'images',
    'count' => $edit ? 5 - $service->images_count : 5,
    ])--}}

@endpush


