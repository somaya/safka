@extends('layouts.master')
@section('title',_trans('Service Detail'))
@section('content')

    <div class="container-fluid">
        <div class="page-title">
            <div class="row">
                <div class="col-6 col-sm-6">
                    <h3>{{ _trans('Service Detail') }}</h3>
                </div>
                <div class="col-6 text-right"></div>
            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid default-dash">
        <div class="row">
            <div class="col-12 col-sm-6">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                        </a></li>
                    <li class="breadcrumb-item"><a href="{{ route('admin.service.index') }}">{{ _trans('Services') }}</a></li>
                    <li class="breadcrumb-item active">{{ _trans('Service Detail') }}</li>
                </ol>
            </div>

            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card card-details">
                            <h4 class="title-details">{{_trans('Service Details')}}</h4>
                            <div class="">
                                <p>{{_trans('Name')}} :<span>{{$service->translate(locale())?->title}}</span></p>
                                <p>{{_trans('Ranking')}} :<span>{{$service->ranking}}</span></p>
                                <p>{{_trans('Image')}} : <span>
                                  <img class="mb-2 image-preview" src="{{getAvatar($service->image)}}">
                              </span></p>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- Container-fluid Ends-->

@endsection
