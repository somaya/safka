@if (count($data) > 0)
    <div class="repeater-default">
        <div data-repeater-list="shops">
            @foreach($data as $key => $value)
                <div data-repeater-item="" style="">
                    <div class="row">
                        <div class="col-md-3">
                            <label class="form-label" for="day_id">{{ _trans('Day') }}</label>
                            <select id="day_id"
                                    name="shops[{{ $key }}][day_id]"
                                    class="js-example-basic-single @error('shops.'.$key.'.day_id') is-invalid @enderror">
                                <option value="">{{ _trans('Select Day') }}</option>
                                @foreach($weeks as $week)
                                    <option value="{{ $week->id }}"
                                        @selected($value['day_id'] == $week->id)
                                    > {{ $week->translate(locale())?->name }}</option>
                                @endforeach
                            </select>
                            @error('shops.'.$key.'.day_id')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror

                        </div>

                        <div class="col-md-3">
                            <label class="form-label" for="from">{{ _trans('Time From') }}</label>
                            <input type="time"
                                   name="shops[{{ $key }}][from]"
                                   value="{{ formatDate('H:i',$value['from']) }}"
                                   class="form-control @error('shops.'.$key.'.from') is-invalid @enderror"
                                   id="from"
                            />
                            @error('shops.'.$key.'.from')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>
                        <div class="col-md-3">
                            <label class="form-label" for="to">{{ _trans('Time To') }}</label>
                            <input type="time"
                                   name="shops[{{ $key }}][to]"
                                   id="to"
                                   value="{{ formatDate('H:i',$value['to']) }}"
                                   class="form-control @error('shops.'.$key.'.to') is-invalid @enderror"
                            />
                            @error('shops.'.$key.'.to')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror

                        </div>
                        <div class="col-md-1">
                            <div class="form-group col-sm-12 col-md-2 text-center mt-2">
                                <br>
                                <button type="button" id="deleteRepeater" class="btn btn-danger" data-repeater-delete="">
                                    {{--<i class="fas fa-trash-alt"></i>--}} {{ _trans('Delete') }}
                                </button>
                            </div>
                        </div>
                    </div>
                    <hr>
                </div>
            @endforeach
        </div>
        <div class="form-group overflow-hidden">
            <div class="col-12">
                <button type="button" id="addRepeater" data-repeater-create="" class="btn btn-primary">
                    {{--<i class="far fa-plus-square"></i> --}}{{ _trans('Add') }}
                </button>
            </div>
        </div>
    </div>

@else
    <div class="repeater-default">
        <div data-repeater-list="shops">
            <div data-repeater-item="" style="">
                <div class="row">
                    <div class="col-md-3">
                        <label class="form-label" for="day_id">{{ _trans('Day') }}</label>
                        <select id="day_id"
                                name="day_id"
                                class="js-example-basic-single">
                            <option value="">{{ _trans('Select Day') }}</option>
                            @foreach($weeks as $week)
                                <option value="{{ $week->id }}"> {{ $week->translate(locale())?->name }}</option>
                            @endforeach
                        </select>

                    </div>

                    <div class="col-md-3">
                        <label class="form-label" for="from">{{ _trans('Time From') }}</label>
                        <input type="time"
                               name="from"
                               id="from"
                               class="form-control"
                        >
                    </div>
                    <div class="col-md-3">
                        <label class="form-label" for="to">{{ _trans('Time To') }}</label>
                        <input type="time"
                               name="to"
                               id="to"
                               class="form-control"
                        />

                    </div>
                    <div class="col-md-1">
                        <div class="form-group col-sm-12 col-md-2 text-center mt-2">
                            <br>
                            <button type="button" id="deleteRepeater" class="btn btn-danger" data-repeater-delete="">
                                {{--<i class="fas fa-trash-alt"></i>--}} {{ _trans('Delete') }}
                            </button>
                        </div>
                    </div>
                </div>
                <hr>
            </div>
        </div>
        <div class="form-group overflow-hidden">
            <div class="col-12">
                <button type="button" id="addRepeater" data-repeater-create="" class="btn btn-primary">
                    {{--<i class="far fa-plus-square"></i> --}} {{ _trans('Add') }}
                </button>
            </div>
        </div>
    </div>
@endif

@push('scripts')
    <script>
        var limit = 1;
        $(document).ready(function () {
            limit = {{ count($data) == 0 ? 1 : count($data)  }};
            limitRepeater();
        })
        $(document).on('click', '#addRepeater', function () {
            limit++;
            if (limit > 14) {
                $('#addRepeater').attr('disabled', true)
            }
            setTimeout(function () {
                $('.js-example-basic-single').select2();
            }, 100);
        })
        $(document).on('click', '#deleteRepeater', function () {
            limit--;
            limitRepeater();
        })

        function limitRepeater() {
            if (limit > 14) {
                $('#addRepeater').attr('disabled', true)
            } else {
                $('#addRepeater').attr('disabled', false)
            }
        }

    </script>
@endpush
