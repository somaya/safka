@extends('layouts.master')

@section('title',$edit ? _trans('Edit Shop') : _trans('Add Shop'))

@push('styles')
    <style>
        #geomap {
            width: 100%;
            height: 400px;
        }
    </style>

@endpush

@section('content')
    <div class="container-fluid">
        <div class="page-title">
            <div class="row">
                <div class="col-6 col-sm-6">
                    <h3>{{ $edit ? _trans('Edit Shop') : _trans('Add Shop') }}</h3>
                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid default-dash">
        <div class="row">
            <div class="col-12 col-sm-6">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="{{ route('admin.dashboard') }}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                        </a>
                    </li>
                    @canany(['Shop list','Shop edit','Shop delete'])
                        <li class="breadcrumb-item ">
                            <a href="{{ route('admin.shop.index') }}">{{ _trans('Shops') }}</a>
                        </li>
                    @endcanany
                    <li class="breadcrumb-item active">
                        {{ $edit ? _trans('Edit Shop') : _trans('Add Shop') }}
                    </li>
                </ol>
            </div>

            <div class="col-md-12">
                @if ($edit)
                    {{ Form::open(['route' => ['admin.shop.update',$shop->id],'method' => 'PUT','files' => true,'class' => 'form mb-15 form-submit','id' =>'kt_contact_form']) }}
                    {{ Form::hidden('id',$shop->id) }}
                @else
                    {{ Form::open(['route' => 'admin.shop.store','method' => 'POST','files' => true,'class' => 'form mb-15','id' =>'kt_contact_form']) }}
                @endif

                <div class="row g-3">
                    <div class="col-md-6">
                        <label class="form-label" for="code">{{ _trans('Shop Code') }}</label>
                        <input id="code"
                               disabled
                               type="text"
                               class="form-control"
                               placeholder="{{ _trans('Enter') }} {{ _trans('Shop Code') }}"
                               value="{{ getCodeTable('Shop','shops',$edit,$edit ? $shop->id : null) }}"
                        >
                    </div>

                    <div class="col-md-6">
                        <label class="form-label" for="branch_type_id">{{ _trans('Branch Type name') }}</label>
                        <select id="branch_type_id"
                                name="branch_type_id[]"
                                multiple
                                class="js-example-basic-single @error('branch_type_id') is-invalid @enderror">
                            @foreach($branchTypes as $branchType)
                                <option value="{{ $branchType->id }}"
                                    @selected(in_array($branchType->id,old('branch_type_id',$edit ? ($shop->branchTypes ? $shop->branchTypes->pluck('id')->toArray() : []) : [])))
                                >{{ $branchType->translate(locale())?->name }}
                                </option>
                            @endforeach
                        </select>
                        @error('branch_type_id')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                        @error('branch_type_id.*')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    @foreach($languages as $lang)
                        <div class="col-md-6">
                            <label for="{{ $lang['code'] }}[name]" class="form-label">{{ _trans('Shop name') }} ({{ ucfirst($lang['code']) }})</label>
                            <input id="{{ $lang['code'] }}[name]"
                                   type="text"
                                   name="{{ $lang['code'] }}[name]"
                                   class="form-control @error($lang['code'].'.name') is-invalid @enderror"
                                   placeholder="{{ _trans('Enter') }} {{ _trans('Shop name') }} ({{ ucfirst($lang['code']) }})"
                                   value="{{ old($lang['code'].'.name',$edit ? $shop->translate($lang['code'])?->name : null) }}"
                            >
                            @error($lang['code'].'.name')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>
                    @endforeach

                    @foreach($languages as $lang)
                        <div class="col-md-6">
                            <label class="form-label" for="{{ $lang['code'] }}[description]">{{ _trans('Shop description') }} ({{ ucfirst($lang['code']) }})</label>
                            <textarea id="{{ $lang['code'] }}[description]"
                                      cols="3"
                                      rows="3"
                                      name="{{ $lang['code'] }}[description]"
                                      class="form-control @error($lang['code'].'.description') is-invalid @enderror "
                                      placeholder="{{ _trans('Enter') }} {{ _trans('Shop description') }} ({{ ucfirst($lang['code']) }})"
                            >{{ old($lang['code'].'.description',$edit ? $shop->translate($lang['code'])?->description : null) }}</textarea>
                            @error($lang['code'].'.description')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>
                    @endforeach

                    <div class="col-md-6">
                        <label class="form-label" for="owner_id">{{ _trans('Owner Name') }}</label>
                        <select id="owner_id" name="owner_id"
                                class="js-example-basic-single @error('owner_id') is-invalid @enderror">
                            <option value="">{{ _trans('Select Owner Name') }}</option>
                            @foreach($owners as $owner)
                                <option value="{{ $owner->id }}" @selected(old('owner_id',$edit ? $shop->owner_id : null) == $owner->id)>{{ $owner->user->name }}</option>
                            @endforeach
                        </select>
                        @error('owner_id')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-6" id="subscription_type_div">
                        <label class="form-label" for="subscription_type_div">{{ _trans('Subscription Type') }}</label>
                        <select id="subscription_type" name="subscription_type"
                                class="js-example-basic-single @error('subscription_type') is-invalid @enderror">
                            <option value="">{{ _trans('Select Subscription Type') }}</option>
                            @foreach(\App\Enums\SubscriptionType::cases() as $type)
                                <option value="{{ $type->value }}"
                                    @selected(old('subscription_type',$edit ? $shop->subscription_type : null) == $type->value)
                                >{{ _trans($type->name) }}</option>
                            @endforeach
                        </select>
                        @error('subscription_type')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-6" id="subscription_value_div">
                        <label class="form-label" for="subscription_value">{{ _trans('Subscription Value') }}</label>
                        <input id="subscription_value"
                               type="number"
                               min="0"
                               step="any"
                               name="subscription_value"
                               class="form-control @error('subscription_value') is-invalid @enderror"
                               placeholder="{{ _trans('Enter') }} {{ _trans('Subscription Value') }}"
                               value="{{ old('subscription_value',$edit ? $shop->subscription_value : null) }}"
                        >
                        @error('subscription_value')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>
                    <div class="col-md-6" id="invoice_duration_div" style="display: none;">
                        <label for="invoice_duration" class="form-label">{{ _trans('Invoice duration') }}</label>
                        <input id="invoice_duration"
                               type="number"
                               min="1"
                               step="1"
                               name="invoice_duration"
                               class="form-control @error('invoice_duration') is-invalid @enderror"
                               placeholder="{{ _trans('Enter') }} {{ _trans('Invoice duration') }}"
                               value="{{ old('invoice_duration',$edit ? $shop->invoice_duration : null) }}"
                        >
                        @error('invoice_duration')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-6" id="subscribe_date_div" style="display: none;">
                        <label for="subscribe_date" class="form-label">{{ _trans('Subscribe date') }}</label>
                        <input id="subscribe_date"
                               type="date"
                               name="subscribe_date"
                               class="form-control @error('subscribe_date') is-invalid @enderror"
                               placeholder="{{ _trans('Subscribe date') }}"
                               value="{{ old('subscribe_date',$edit ? $shop->subscribe_date : null) }}"
                        >
                        @error('subscribe_date')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>


                    <div class="col-md-6">
                        <label class="form-label" for="email">{{ _trans('E-mail') }}</label>
                        <input id="email"
                               type="email"
                               name="email"
                               class="form-control @error('email') is-invalid @enderror"
                               placeholder="{{ _trans('Enter') }} {{ _trans('E-mail') }}"
                               value="{{ old('email',$edit ? $shop->email : null) }}"
                        >
                        @error('email')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-6">
                        <label class="form-label" for="phone">{{ _trans('Phone') }}</label>
                        <input id="phone"
                               type="text"
                               name="phone"
                               class="form-control @error('phone') is-invalid @enderror"
                               placeholder="{{ _trans('Enter') }} {{ _trans('Phone') }}"
                               value="{{ old('phone',$edit ? $shop->phone : null) }}"
                        >
                        @error('phone')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-6">
                        <label class="form-label" for="country_id">{{ _trans('Country Name') }}</label>
                        <select id="country_id" name="country_id"
                                class="js-example-basic-single @error('country_id') is-invalid @enderror">
                            <option value="">{{ _trans('Select Country Name') }}</option>
                            @foreach($countries as $country)
                                <option value="{{ $country->id }}" @selected(old('country_id',$edit ? $shop->country_id : null) == $country->id)>{{ $country->name }}</option>
                            @endforeach
                        </select>
                        @error('country_id')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-6">
                        <label class="form-label" for="governorate_id">{{ _trans('Governorate Name') }}</label>
                        <select id="governorate_id" name="governorate_id"
                                class="js-example-basic-single @error('governorate_id') is-invalid @enderror">
                            <option value="">{{ _trans('Select Governorate Name') }}</option>

                        </select>
                        @error('governorate_id')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-6">
                        <label class="form-label" for="region_id">{{ _trans('Region Name') }}</label>
                        <select id="region_id" name="region_id"
                                class="js-example-basic-single @error('region_id') is-invalid @enderror">
                            <option value="">{{ _trans('Select Region Name') }}</option>
                        </select>
                        @error('region_id')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-6">
                        <label class="form-label" for="address">{{ _trans('Address') }}</label>
                        <input id="address"
                               type="text"
                               name="address"
                               class="form-control @error('address') is-invalid @enderror"
                               placeholder="{{ _trans('Enter') }} {{ _trans('Address') }}"
                               value="{{ old('address',$edit ? $shop->address : null) }}"
                        >
                        @error('address')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>
                    <div class="col-md-6">
                        <label class="form-label" for="minimum_order_price">{{ _trans('Minimum Order Price') }}</label>
                        <input id="minimum_order_price"
                               type="number"
                               min="1"
                               step="any"
                               name="minimum_order_price"
                               class="form-control @error('minimum_order_price') is-invalid @enderror"
                               placeholder="{{ _trans('minimum_order_price') }}"
                               value="{{ old('minimum_order_price',$edit ? $shop->minimum_order_price : 1) }}"
                        >
                        @error('minimum_order_price')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-6">
                        <div class="mb-1">
                            <label class="form-label">{{ _trans('Logo') }}</label>
                        </div>
                        <div class="p-2 border border-dashed" style="max-width:230px;">
                            <div class="row" id="logo"></div>
                        </div>
                        @error('icon')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="col-md-6">
                        <div class="mb-1">
                            <label class="form-label">{{ _trans('Image') }}</label>
                        </div>
                        <div class="p-2 border border-dashed" style="max-width:230px;">
                            <div class="row" id="image"></div>
                        </div>
                        @error('image')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="col-md-6">
                        <input type="text" id="search_location" class="form-control"
                               placeholder="{{ _trans('Search location') }}"
                               value="{{ old('search_location') }}"
                               name="search_location"/>
                    </div>

                    <div class="col-md-12">
                        <div id="divInfo" style="font-family: Arial; font-size: 12px; color: Red;"></div>
                        <div id="geomap"></div>
                    </div>

                    <input type="hidden" name="lat" value="{{ old('lat',$edit ? $shop->lat : null)  }}" class="search_latitude"/>
                    <input type="hidden" name="lng" value="{{ old('lng',$edit ? $shop->lng : null)  }}" class="search_longitude"/>


                    <div class="col-md-12">
                        <h5>{{ _trans('Working Hours') }}</h5>
                        <hr>
                        @include('admin.shop.form-repeater',['data' => $edit ? old('shops',$shop->workingHours) :  (!empty(old('shops')) ? (old('shops')) : []) ])
                    </div>
                    <div class="col-md-12">
                        <div class="m-t-50 d-flex justify-content-end">
                            <button type="submit" class="btn btn-primary">{{ $edit ?_trans('Update') : _trans('Save') }}</button>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->

    <button id="current_location" style="bottom: 189px;right: 8px;">
        <img src="{{ asset('google-map/current-location.png') }}" style="width:30px;height:40px;" alt="">
    </button>
@endsection
@push('scripts')
    <script src="{{ asset('assets/js/spartan-multi-image-picker.js') }}"></script>
    @include('layouts.partials.spartan-multi-image',[
    'single' => true,
    'file_name' => 'logo',
    'image' => $edit ? getAvatar($shop->logo) :  asset('assets/images/img/400x400/img2.jpg')
    ])
    @include('layouts.partials.spartan-multi-image',[
    'single' => true,
    'file_name' => 'image',
    'image' => $edit ? getAvatar($shop->image) :  asset('assets/images/img/400x400/img2.jpg')
    ])

@endpush
@include('layouts.partials.ckeditor',['inputClass' => 'textarea'])
@push('scripts')
    <script src="https://maps.googleapis.com/maps/api/js?key={{ Utility::getValByName('google_maps_api') }}&libraries=places&callback=initMap&v=weekly" async></script>
    <script>
        let map, infoWindow;
        let marker;
        let geocoder;

        function initMap() {
            let initialLat = $('.search_latitude').val();
            let initialLong = $('.search_longitude').val();
            initialLat = initialLat ? initialLat : 24.715418264775263;
            initialLong = initialLong ? initialLong : 46.67420617456056;
            let latlng = new google.maps.LatLng(initialLat, initialLong);

            const imageMarker = {
                url: "{{ asset('google-map/address.png') }}", // url
                scaledSize: new google.maps.Size(35, 40), // scaled size
                origin: new google.maps.Point(0, 0), // origin
                anchor: new google.maps.Point(0, 0) // anchor
            };
            const currentLocale = "{{ asset('google-map/current-location.png') }}";
            let options = {
                zoom: 12,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                mapTypeControl: false,
                streetViewControl: true,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.SMALL
                }
            };

            map = new google.maps.Map(document.getElementById('geomap'), options);

            geocoder = new google.maps.Geocoder();

            /* current location */
            const locationButton = document.getElementById("current_location");
            map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(locationButton);
            locationButton.addEventListener("click", (e) => {
                e.preventDefault();
                // Try HTML5 geolocation.
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(
                        (position) => {
                            let address = '';
                            const pos = {
                                lat: position.coords.latitude,
                                lng: position.coords.longitude,
                            };
                            latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                            $('.search_latitude').val(position.coords.latitude);
                            $('.search_longitude').val(position.coords.longitude);
                            map.setCenter(pos)
                            map.setZoom(12)
                            marker.setPosition(pos);
                            getAddByLatLng(position.coords.latitude, position.coords.longitude, infowindow, map, marker)
                        },
                        () => {
                            handleLocationError(true, infoWindow, map.getCenter());
                        }
                    );
                } else {
                    // Browser doesn't support Geolocation
                    handleLocationError(false, infoWindow, map.getCenter());
                }
            });

            var input = document.getElementById('search_location');
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            var autocomplete = new google.maps.places.Autocomplete(input);
            autocomplete.bindTo('bounds', map);

            var infowindow = new google.maps.InfoWindow();

            marker = new google.maps.Marker({
                map: map,
                draggable: true,
                position: latlng,
                anchorPoint: new google.maps.Point(0, -29),
                icon: imageMarker,
            });

            /* search in input map */
            autocomplete.addListener('place_changed', function () {
                infowindow.close();
                marker.setVisible(false);
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    window.alert("Autocomplete's returned place contains no geometry");
                    return;
                }

                // If the place has a geometry, then present it on a map.
                if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(12);
                }
                /*marker.setIcon(({
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(35, 35)
                }));*/
                //marker.setPosition(place.geometry.location);
                map.setCenter(place.geometry.location);
                map.setZoom(12)
                marker.setPosition(place.geometry.location);
                marker.setVisible(true);
                let address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');
                }
                $('#address').val(address);
                infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
                infowindow.open(map, marker);

                // Location details
                /*for (var i = 0; i < place.address_components.length; i++) {
                    if(place.address_components[i].types[0] == 'postal_code'){
                        document.getElementById('postal_code').innerHTML = place.address_components[i].long_name;
                    }
                    if(place.address_components[i].types[0] == 'country'){
                        document.getElementById('country').innerHTML = place.address_components[i].long_name;
                    }
                }*/
                $('.search_latitude').val(place.geometry.location.lat());
                $('.search_longitude').val(place.geometry.location.lng());
                $('.place_id').val(place.place_id);
            });

            /* drop and down mark */
            marker.addListener('dragend', function () {
                let latlng;
                geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            let address = '';
                            latlng = new google.maps.LatLng(marker.getPosition().lat(), marker.getPosition().lng());
                            $('.search_latitude').val(marker.getPosition().lat());
                            $('.search_longitude').val(marker.getPosition().lng());
                            $('.place_id').val(results[0].place_id);
                            map.setCenter(marker.getPosition())
                            map.setZoom(12)
                            marker.setPosition(marker.getPosition());
                            if (results.length > 0) {
                                address = [
                                    (results[0].address_components[0] && results[0].address_components[0].short_name || ''),
                                    (results[0].address_components[1] && results[0].address_components[1].short_name || ''),
                                    (results[0].address_components[2] && results[0].address_components[2].short_name || '')
                                ].join(' ');
                            }
                            $('#address').val(address);
                            infowindow.setContent('<div><strong>' + address + '</strong>');
                            infowindow.open(map, marker);
                        }
                    }
                });
            });

            /* click in map */
            map.addListener('click', function (event) {
                geocoder.geocode({'latLng': event.latLng}, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            let address = '';
                            $('.search_latitude').val(event.latLng.lat());
                            $('.search_longitude').val(event.latLng.lng());
                            $('.place_id').val(results[0].place_id);
                            map.setCenter(event.latLng)
                            map.setZoom(12)
                            marker.setPosition(event.latLng);
                            if (results.length > 0) {
                                address = [
                                    (results[0].address_components[0] && results[0].address_components[0].short_name || ''),
                                    (results[0].address_components[1] && results[0].address_components[1].short_name || ''),
                                    (results[0].address_components[2] && results[0].address_components[2].short_name || '')
                                ].join(' ');
                            }
                            $('#address').val(address);
                            infowindow.setContent('<div><strong>' + address + '</strong>');
                            infowindow.open(map, marker);
                        }
                    }
                });
            });

        }

        /* current location in map */
        function handleLocationError(browserHasGeolocation, infoWindow, pos) {
            infoWindow.setPosition(pos);
            infoWindow.setContent(
                browserHasGeolocation
                    ? "Error: The Geolocation service failed."
                    : "Error: Your browser doesn't support geolocation."
            );
            infoWindow.open(map);
        }

        function getAddByLatLng(lat, lng, infowindow, map, marker) {
            $.ajax({
                url: 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + lat + ',' + lng + '&key=AIzaSyBGNB1noDBHyu5VfUKONwJATgyFA-8Mkv4',
                method: 'GET',
                success: function (data, textStatus, jqXHR) {
                    let results = data.results;
                    let address = '';
                    if (results.length > 0) {
                        address = [
                            (results[0].address_components[0] && results[0].address_components[0].short_name || ''),
                            (results[0].address_components[1] && results[0].address_components[1].short_name || ''),
                            (results[0].address_components[2] && results[0].address_components[2].short_name || '')
                        ].join(' ');
                    }
                    $('#address').val(address);
                    infowindow.setContent('<div><strong>' + address + '</strong>');
                    infowindow.open(map, marker);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR)
                }
            })
            return false;
        }
    </script>

    <script src="{{ asset('assets/repeater/jquery.repeater.min.js') }}"></script>
    <script src="{{ asset('assets/repeater/form-repeater.min.js') }}"></script>
    <script>

        $(document).ready(function () {
            window.initMap = initMap;
            getGovernorate('{{ old('country_id',$edit ? $shop->country_id : null) }}', '{{ old('governorate_id',$edit ? $shop->governorate_id : null)  }}')
            getRegion('{{ old('governorate_id',$edit ? $shop->governorate_id : null) }}', '{{ old('region_id',$edit ? $shop->region_id : null) }}')
            getInvoiceDuration('{{ old('subscription_type',$edit ? $shop->subscription_type : null) }}')

        })
        $(document).on('change', '#subscription_type', function (e) {
            e.preventDefault();
            let subscription_type = $('#subscription_type option:selected').val();
            getInvoiceDuration(subscription_type);
        });

        function getInvoiceDuration(subscription_type) {
            if (subscription_type == '' || subscription_type == null) {
                $('#invoice_duration_div').css('display', 'none');
                $('#subscribe_date_div').css('display', 'none');
            } else {
                if (subscription_type == 'percent') {
                    $('#invoice_duration_div').css('display', 'block');
                    $('#subscribe_date_div').css('display', 'none');
                } else {
                    $('#invoice_duration_div').css('display', 'none');
                    $('#subscribe_date_div').css('display', 'block');
                }
            }
        }
    </script>
    @include('layouts.ajax.countries')
@endpush

