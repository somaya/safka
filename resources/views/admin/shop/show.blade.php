@extends('layouts.master')
@section('title',_trans('Shops'))
@section('content')

    <div class="container-fluid">
        <div class="page-title">
            <div class="row">
                <div class="col-6 col-sm-6">
                    <h3>{{ _trans('Shop Details') }}</h3>
                </div>
                <div class="col-6 text-right">

                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid default-dash">
        <div class="row">
            <div class="col-12 col-sm-6">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/') }}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                        </a></li>
                    <li class="breadcrumb-item"><a href="{{ url('/') }}">{{ _trans('Shop Details') }}</a></li>
                    <li class="breadcrumb-item active">{{ _trans('Products') }}</li>
                </ol>
            </div>

            <div class="col-xl-12 col-md-12 mb-4">
                <form action="{{ url()->current() }}" method="GET">
                    <div class="left-side-header">
                        <div class="row justify-content-between align-items-center">
                            <x-search :columns="$columns"/>
                            <div class="col-md-4">
                                <p class="mb-0 font-sm d-flex align-items-center justify-content-end">{{ _trans('Count Products') }} :
                                    <span class="d-block font-md text-danger">({{ $data->total() }})</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="table-responsive custom-scrollbar p-t-30">
                    <table class="table">
                        <thead>
                        <tr>
                            <th><span>{{ _trans('Code')}}  </span></th>
                            <th><span>{{ _trans('Date')}} </span></th>
                            <th><span>{{ _trans('Category')}} </span></th>
                            <th><span>{{ _trans('Product')}} </span></th>
                            <th><span>{{ _trans('Icon')}} </span></th>
                            <th><span>{{ _trans('Rating')}} </span></th>
                            <th><span>{{ _trans('action')}}</span></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $index => $item)
                            <tr>
                                <td class="text-main"><a href="{{ route('product.show',$item->id) }}">Product#{{$item->id}}</a></td>
                                <td>{{$item->created_at->format('d-m-Y') }}</td>
                                <td>{{$item->category->name}}</td>
                                <td>{{$item->translate(locale())?->name}}</td>
                                <td>
                                    <img src="{{getAvatar($item->icon) }}" class="mb-2 image-preview">
                                </td>
                                <td>{{$item->getRate() }}</td>

                                <td>
                                    @can('Product edit')
                                        <a href="{{ route('product.edit',$item->id) }}" class="btn btn-primary">
                                            <i class="fa fa-edit" aria-hidden="true"></i>
                                        </a>
                                    @endcan
                                        <a href="{{ route('product.show',$item->id) }}" class="btn btn-primary">
                                            <i class="fa fa-eye" aria-hidden="true"></i>
                                        </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $data->appends(request()->query())->links('layouts.partials.pagination') }}

                @if( $data->count() == 0)
                    <div class="empty-data">
                        <img src="{{ asset('assets/')}}/images/nodata.svg">
                        <h4>{{ _trans('No_data_to_show')}}</h4>
                    </div>
                @endif
            </div>

            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card card-details">
                            <h4 class="title-details">{{_trans('Shop Details')}}</h4>
                            <div class="">
                                <p>{{_trans('Name')}} :<span>{{$shop->translate(locale())?->name}}</span></p>
                                <p>{{_trans('Owner')}} :<span>{{$shop->owner->user->name}}</span></p>
                                <p>{{_trans('pone')}} :<span>{{$shop->phone}}</span></p>
                                <p>{{_trans('Subscription')}} :<span>{{ $shop->subscription_value }} {{$shop->subscription_type=='participation' ?'':'%'}}</span></p>


                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-details">
                            <h4 class="title-details">{{ _trans('Offers')}}</h4>
                            <div class="">
                                <p>{{ _trans('Count')}} : <span>{{$offers}}</span></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-details">
                            <h4 class="title-details">{{ _trans('Saves')}}</h4>
                            <div class="">
                                <p>{{ _trans('Count')}} : <span>{{$saves}}</span></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-details">
                            <h4 class="title-details">{{ _trans('Discounts')}}</h4>
                            <div class="">
                                <p>{{ _trans('Count')}} : <span>{{$discounts}}</span></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-details">
                            <h4 class="title-details">{{ _trans('Delivery Areas')}}</h4>
                            <div class="">
                                <p>{{ _trans('Count')}} : <span>{{$delivery_areas}}</span></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-details">
                            <h4 class="title-details">{{ _trans('Coupons')}}</h4>
                            <div class="">
                                <p>{{ _trans('Count')}} : <span>{{$coupons}}</span></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-details">
                            <h4 class="title-details">{{ _trans('Orders')}}</h4>
                            <div class="">
                                <p>{{ _trans('Count')}} : <span>{{$orders->count()}}</span></p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="card card-details">
                            <h4 class="title-details">{{ _trans('Orders Total Price')}}</h4>
                            <div class="">
                                <p><span>{{$orders_total_price}}</span></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-details">
                            <h4 class="title-details">{{ _trans('Subscription')}}</h4>
                            <div class="">
                                <p><span>{{$subscription}}</span></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-details">
                            <h4 class="title-details">{{ _trans('Total Profit')}}</h4>
                            <div class="">
                                <p><span>{{$orders_total_price-$subscription}}</span></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <!-- add qr code -->
                        <div class="row">
                            <div class="col-md-4">
                                <div class="card card-details">
                                    <h4 class="title-details">{{ _trans('QR Code') }}</h4>
                                    <img src="{{ getAvatar($shop->qr_code) }}" class="qr-code">
                                    <a href="{{ route('download.qr-code',encrypt($shop->id)) }}"
                                       class="download_code">{{ _trans('Download QR Code') }}</a>
                                </div>
                            </div>
                        </div>
                        <!-- end qr code -->
                    </div>

                </div>
            </div>

        </div>
    </div>
    <!-- Container-fluid Ends-->

@endsection
