@extends('layouts.master')

@section('title',_trans('Social Media'))


@section('content')
    <div class="container-fluid">
        <div class="page-title">
            <div class="row">
                <div class="col-6 col-sm-6">
                    <h3>{{ $edit ? _trans('Edit Social Media') : _trans('Add Social Media') }}</h3>
                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid default-dash">
        <div class="row">
            <div class="col-12 col-sm-6">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/') }}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                        </a>
                    </li>
                    @canany(['Social Media list','Social Media edit','Social Media delete'])
                        <li class="breadcrumb-item ">
                            <a href="{{ route('admin.social-media.index') }}">{{ _trans('Social Media') }}</a>
                        </li>
                    @endcanany

                    <li class="breadcrumb-item active">
                        {{ $edit ? _trans('Edit Social Media') : _trans('Add Social Media') }}
                    </li>
                </ol>
            </div>

            <div class="col-xl-12 col-md-12 ">
                @if ($edit)
                    {{ Form::open(['url' => route('admin.social-media.update',$socialMedia->id),'method' => 'PUT','files' => true,'class' => 'form mb-15 form-submit','id' =>'kt_contact_form']) }}
                    {{ Form::hidden('id',$socialMedia->id) }}
                @else
                    {{ Form::open(['route' => 'admin.social-media.store','method' => 'POST','files' => true,'class' => 'form mb-15','id' =>'kt_contact_form']) }}
                @endif

                <div class="row g-3">
                    <div class="col-md-6">
                        <label id="code" class="form-label" for="">{{ _trans('Social Media Code') }}</label>
                        <input id="code"
                               disabled
                               type="text"
                               class="form-control"
                               placeholder="{{ _trans('Social Media Code') }}"
                               value="{{ getCodeTable('SM','social_media',$edit,$edit ? $socialMedia->id : null) }}"
                        >
                    </div>

                    <div class="col-md-6">
                        <div class="form-group form-image">
                            <label for="slug" class="font-md">{{ _trans('Name social media') }}</label>
                            <input id="slug"
                                   type="text"
                                   name="slug"
                                   placeholder="{{ _trans('Name social media') }}"
                                   value="{{ old('slug',$edit ? $socialMedia->slug : null) }}"
                                   class="form-control @error('slug') is-invalid @enderror">
                            @error('slug')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group form-image">
                            <label for="url" class="font-md">{{ _trans('Link social media') }}</label>
                            <input id="url"
                                   type="text"
                                   name="url"
                                   placeholder="{{ _trans('Link social media') }}"
                                   value="{{ old('url',$edit ? $socialMedia->url : null) }}"
                                   class="form-control @error('url') is-invalid @enderror">
                            @error('url')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>
                    </div>



                    <div class="col-md-6">
                        <div class="form-group form-image">
                            <label class="font-md">{{ _trans('Icon') }}</label>
                            <img src="{{ $edit ? getAvatar($socialMedia->icon) : asset('assets/images/no-image.png') }}"
                                 class="mb-2 icon-preview">
                            <input id="icon"
                                   type="file"
                                   name="icon"
                                   class="form-control icon @error('icon') is-invalid @enderror">
                            @error('icon')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>
                    </div>


                    <div class="col-md-12">
                        <div class="m-t-50 d-flex justify-content-end">
                            <button type="submit" class="btn btn-primary">{{ $edit ?_trans('Update') : _trans('Save') }}</button>
                        </div>
                    </div>


                </div>

                {{ Form::close() }}
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection
@include('layouts.partials.read-photo',['inputName' => 'icon'])
