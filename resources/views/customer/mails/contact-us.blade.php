@component('mail::message')
# Ticket {{ $data['data']['full_name'] }}
Hi, {{ $data['data']['full_name'] }} <br>
Email:- {{ $data['data']['full_name'] }}
<br>
Phone:- {{ $data['data']['phone'] }}
<br>
{{ $data['data']['message'] }}

Thanks,<br>
for use Website
{{ Utility::getValByName('company_name_'.locale()) }}
@endcomponent
