@component('mail::message')
    # Rest Password Account
    Hi, {{ $data['user']['name'] }} <br>
    Forget your password ?
    <br>
    We received a request to reset the password for your account
    <br>
    To reset your password, click on the button below:
    @component('mail::button', ['url' => url('user/rest/password/'.$data['token'])])
        Rest password
    @endcomponent
    Or<br>
    copy this link
    <a target="_blank" href="{{ url('user/rest/password/'.$data['token'])}}">  {{ url('user/rest/password/'.$data['token'])}}</a>
    Thanks,<br>
    for use Website
    {{ Utility::getValByName('company_name_'.locale()) }}
@endcomponent
