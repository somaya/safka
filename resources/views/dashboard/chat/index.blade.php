@extends('layouts.master')

@section('title',_trans('Chat'))

@section('content')
    <div class="container-fluid">
        <div class="page-title">
            <div class="row">
                <div class="col-6 col-sm-6">
                    <h3>{{ _trans('Chats') }}</h3>
                </div>


            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid default-dash">

        <div class="row">

            <div class="col-12 col-lg-4">
                <div class="mb-2 search_chat">
                    <a class="icon"><i class="bi bi-search"></i> </a>
                    <input type="text" id="searchChat" class="form-control" placeholder="{{ _trans('Search customer') }}"/>
                </div>
                <div class="bx_main" id="listConversations">
                    @foreach($conversations as $conversation)
                        <a href="#" class="list-message" onclick="getMessage({{$conversation->id}})"
                           id="conversationId"
                           data-id="{{ $conversation->id }}">
                            <div class="numUnread status" id="message_not_read_{{$conversation->id}}">{{ $conversation->message_not_read }}</div>
                            <div class="d-flex align-items-start">
                                <img src="{{ getAvatar($conversation->customer->user->avatar) }}" class="rounded-circle" alt=""/>
                                <div class="flex-grow-1 ml-3">
                                    <h5 class="sub">{{ $conversation->customer->user->name }} ({{$conversation->order->code}})</h5>
                                    <small id="lastMessage_{{ $conversation->id }}">{{ Str::limit($conversation->lastMessage->body,10) }}</small>
                                </div>
                            </div>
                        </a>
                    @endforeach

                </div>
            </div>

            <div class="col-12 col-lg-8">
                <div class="bx_main content_chat" id="list_messages">

                </div>
            </div>
        </div>

    </div>
    <!-- Container-fluid Ends-->
@endsection

@push('scripts')
    <script>

        function getMessage(conversation_id) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                type: "POST",
                cache: false,
                url: '{{ route('owner.chat.index')}}',
                data: {
                    conversation_id: conversation_id
                },
                success: function (response) {
                    const data = response.data;
                    if (response.status == true) {
                        $('#list_messages').empty();
                        $('#list_messages').append(data.view);
                        $('#message_not_read_' + conversation_id).text(0);
                        $('.chat').animate({
                            scrollTop: $('.chat').get(0).scrollHeight
                        }, 2000);

                    }else {
                        toastr.error(response.error)
                    }
                }
            });
        }

        function sendMessage(event, conversation_id) {
            event.preventDefault();
            let body = $('#message_send').val();
            if (body == '' || body == null) {
                toastr.warning('{{ _trans('please enter message') }}', '').delay(10000);
                return false;
            }
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: '{{ route('owner.chat.message')}}',
                data: {
                    conversation_id: conversation_id,
                    message: body,
                },
                success: function (response) {
                    const data = response.data;
                    if (response.status == true) {
                        $('#chat_body').append(data.view);
                        $('#message_send').val('');
                        $('.chat').animate({
                            scrollTop: $('.chat').get(0).scrollHeight
                        }, 2000);
                    } else {
                        toastr.warning(response.error)
                    }
                }
            });
        }

        $('#conversationId').on('click', function (e) {
            e.preventDefault();
            var conversationId = $('#conversationId').data('id');
            var userId = '{{ auth()->user()->id }}';
            let channelConversion = Echo.channel("conversion_" + conversationId);
            channelConversion.listen('.SendMessageShop', function (data) {
                let message = data.message;
                if (message) {
                    $('#message_not_read_' + conversationId).text(message.count_unread);
                    $('#lastMessage_' + conversationId).text(message.last_message);

                    let direction = 'right';
                    if (message.user_id == userId) {
                        direction = 'left';
                    }
                    $('#chat_body').append('<div class="answer ' + direction + '">' +
                        ' <div class="text">' + message.message + '</div> ' +
                        '<div class="time">' + message.format_date + '</div>' +
                        ' </div>');
                }
            })
        });

        Echo.channel("listConversations_{{ auth()->user()->id }}")
            .listen('.GetConversationShop', function (data) {
                getListConversations(data.conversations)
            });

        function getListConversations(response) {
            var html = '';
            if (response.length > 0) {
                for (var count = 0; count < response.length; count++) {
                    var data = response[count];
                    html += '<a href="#" class="list-message" onclick="getMessage(' + data.conversation_id + ')" id="conversationId" data-id="' + data.conversation_id + '"> ';
                    html += '<div class="numUnread status" id="message_not_read_' + data.conversation_id + '">' + data.count_unread + '</div>';
                    html += '<div class="d-flex align-items-start">';
                    html += '<img src="' + data.customer.avatar + '" class="rounded-circle" alt=""/>';
                    html += '<div class="flex-grow-1 ml-3">';
                    html += '<h5 class="sub">' + data.customer.name + ' (' + data.order.code + ')</h5>';
                    html += '<small id="lastMessage_' + data.conversation_id + '">' + data.last_message + '</small>';
                    html += '</div>';
                    html += '</div>';
                    html += '</a>';
                }
            }
            $('#listConversations').html(html);
        }

        $('#searchChat').on('keyup', function (e) {
            let search = $(this).val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: '{{ route('owner.chat.search')}}',
                data: {
                    search: search
                },
                success: function (response) {
                    const data = response.data;
                    if (response.status == true) {
                        getListConversations(data)
                    }
                }
            });
        });
    </script>


@endpush
