<div class="border-bottom d-none d-lg-block">
    <div class="d-flex align-items-center mb-3">
        <div class="position-relative">
            <img
                src="{{ getAvatar($customer->user->avatar) }}"
                class="rounded-circle profile_img"
                alt=""
            />
        </div>
        <div class="flex-grow-1 pl-3">
            <div>
                <h5 class="text-dark font-md mb-0">{{ $customer->user->name }}</h5>
            </div>
        </div>
        <div>
            <button class="btn_list_chat">
                <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
            </button>
        </div>
    </div>
</div>

<div class="col-sm-12 col-xs-12 chat" tabindex="5001">
    <div class="col-inside-lg decor-default">
        <div class="chat-body" id="chat_body">
            @foreach($messages as $message)
                <div class="answer {{ $message->user_id == auth()->user()->id ? 'left' : 'right' }}">
                    <div class="text">{{ $message->body }}</div>
                    <div class="time">{{ $message->created_at->format('d-m-Y') }}</div>
                </div>
            @endforeach
        </div>
    </div>
</div>

<div class="send_chat border-top">
    <div class="input-group input_chat">
        <input type="text" id="message_send" class="form-control" placeholder="{{ _trans('Enter message') }}"/>
        <i class="fa fa-smile-o" aria-hidden="true"></i>
        <i class="fa fa-picture-o" aria-hidden="true"></i>
    </div>
    <button type="button" class="font-md icon_send" onclick="sendMessage(event,{{$conversation->id}})" >{{ _trans('Send') }}</button>

</div>

