@extends('layouts.master')

@section('title',_trans('Dashboard'))


@section('content')

    <div class="container-fluid">
        <div class="page-title">
            <div class="row">
                <div class="col-6 col-sm-6">
                    <h3>{{ _trans('Dashboard') }}</h3>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid default-dash">
        <div class="row">
            @if(Auth::user()->userable_type ==\App\Models\Owner::class)
                <div class="col-xl-4 col-sm-4 box-col-4">
                    <div class="card ecommerce-widget">
                        <div class="card-body support-ticket-font">
                            <div class="row">
                                <div class="col-7"><a href="#">{{_trans('Shops')}}</a>
                                    <h3 class="total-num counter">{{$shops}}</h3>
                                </div>
                                <div class="col-5">
                                    <div class="text-end">
                                        <div class="bg-secondary product-icon">
                                            <i class="fa fa-home"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="progress-showcase">
                                <div class="progress sm-progress-bar">
                                    <div class="progress-bar bg-secondary" role="progressbar" style="width: 70%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <div class="d-flex align-items-center justify-content-between">
                                    <small class="text-muted">الفترة السابقة</small>
                                    <small class="text-muted">70%</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            <div class="col-xl-4 col-sm-4 box-col-4">
                <div class="card ecommerce-widget">
                    <div class="card-body support-ticket-font">
                        <div class="row">
                            <div class="col-7"><a href="#">{{_trans('Products')}}</a>
                                <h3 class="total-num counter">{{$products}}</h3>
                            </div>
                            <div class="col-5">
                                <div class="text-end">
                                    <div class="bg-warning product-icon">
                                        <i class="fa fa-users" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="progress-showcase mt-4">
                            <div class="progress sm-progress-bar">
                                <div class="progress-bar bg-warning" role="progressbar" style="width: 70%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <div class="d-flex align-items-center justify-content-between">
                                <small class="text-muted">الفترة السابقة</small>
                                <small class="text-muted">70%</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-4 col-sm-4 box-col-4">
                <div class="card ecommerce-widget">
                    <div class="card-body support-ticket-font">
                        <div class="row">
                            <div class="col-7"><a href="#">{{_trans('Offers')}}</a>
                                <h3 class="total-num counter">{{$offers}}</h3>
                            </div>
                            <div class="col-5">
                                <div class="text-end">
                                    <div class="bg-danger product-icon">
                                        <i class="fa fa-bookmark-o" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="progress-showcase">
                            <div class="progress sm-progress-bar">
                                <div class="progress-bar bg-danger" role="progressbar" style="width: 70%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <div class="d-flex align-items-center justify-content-between">
                                <small class="text-muted">الفترة السابقة</small>
                                <small class="text-muted">70%</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-4 col-sm-4 box-col-4">
                <div class="card ecommerce-widget">
                    <div class="card-body support-ticket-font">
                        <div class="row">
                            <div class="col-7"><a href="#">{{_trans('Saves')}}</a>
                                <h3 class="total-num counter">{{$saves}}</h3>
                            </div>
                            <div class="col-5">
                                <div class="text-end">
                                    <div class="bg-danger product-icon">
                                        <i class="fa fa-bookmark-o" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="progress-showcase">
                            <div class="progress sm-progress-bar">
                                <div class="progress-bar bg-danger" role="progressbar" style="width: 70%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <div class="d-flex align-items-center justify-content-between">
                                <small class="text-muted">الفترة السابقة</small>
                                <small class="text-muted">70%</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-4 col-sm-4 box-col-4">
                <div class="card ecommerce-widget">
                    <div class="card-body support-ticket-font">
                        <div class="row">
                            <div class="col-7"><a href="#">{{_trans('Discounts')}}</a>
                                <h3 class="total-num counter">{{$discounts}}</h3>
                            </div>
                            <div class="col-5">
                                <div class="text-end">
                                    <div class="bg-success product-icon">
                                        <i class="fa fa-bookmark-o" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="progress-showcase mt-4">
                            <div class="progress sm-progress-bar">
                                <div class="progress-bar bg-success" role="progressbar" style="width: 70%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <div class="d-flex align-items-center justify-content-between">
                                <small class="text-muted">الفترة السابقة</small>
                                <small class="text-muted">70%</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-4 col-sm-4 box-col-4">
                <div class="card ecommerce-widget">
                    <div class="card-body support-ticket-font">
                        <div class="row">
                            <div class="col-7">
                                @can('Orders list')
                                    <a href="/order">{{_trans('All Orders')}}</a>
                                @else
                                    {{_trans('All Orders')}}
                                @endcan
                                <h3 class="total-num counter">{{$all_orders}}</h3>
                            </div>
                            <div class="col-5">
                                <div class="text-end">
                                    <div class="bg-primary product-icon">
                                        <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="progress-showcase">
                            <div class="progress sm-progress-bar">
                                <div class="progress-bar bg-primary" role="progressbar" style="width: 70%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <div class="d-flex align-items-center justify-content-between">
                                <small class="text-muted">الفترة السابقة</small>
                                <small class="text-muted">70%</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-4 col-sm-4 box-col-4">
                <div class="card ecommerce-widget">
                    <div class="card-body support-ticket-font">
                        <div class="row">
                            <div class="col-7">
                                @can('Orders list')
                                    <a href="/order?payment_method=cash">{{_trans('Cash Orders')}}</a>
                                @else
                                    {{_trans('Cash Orders')}}
                                @endcan
                                <h3 class="total-num counter">{{$cash_orders}}</h3>
                            </div>
                            <div class="col-5">
                                <div class="text-end">
                                    <div class="bg-success product-icon">
                                        <i class="fa fa-money" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="progress-showcase">
                            <div class="progress sm-progress-bar">
                                <div class="progress-bar bg-success" role="progressbar" style="width: 70%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <div class="d-flex align-items-center justify-content-between">
                                <small class="text-muted">الفترة السابقة</small>
                                <small class="text-muted">70%</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-4 col-sm-4 box-col-4">
                <div class="card ecommerce-widget">
                    <div class="card-body support-ticket-font">
                        <div class="row">
                            <div class="col-7">
                                @can('Orders list')
                                    <a href="/order?payment_method=visa">{{_trans('Credit Orders')}}</a>
                                @else
                                    {{_trans('Credit Orders')}}
                                @endcan
                                <h3 class="total-num counter">{{$credit_orders}}</h3>
                            </div>
                            <div class="col-5">
                                <div class="text-end">
                                    <div class="bg-info product-icon">
                                        <i class="fa fa-credit-card" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="progress-showcase">
                            <div class="progress sm-progress-bar">
                                <div class="progress-bar bg-info" role="progressbar" style="width: 70%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <div class="d-flex align-items-center justify-content-between">
                                <small class="text-muted">الفترة السابقة</small>
                                <small class="text-muted">70%</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if(Auth::user()->userable_type ==\App\Models\Owner::class)
                <div class="col-xl-4 col-sm-4 box-col-4">
                    <div class="card ecommerce-widget">
                        <div class="card-body support-ticket-font">
                            <div class="row">
                                <div class="col-7"><a href="#">{{_trans('Employees')}}</a>
                                    <h3 class="total-num counter">{{$staffs}}</h3>
                                </div>
                                <div class="col-5">
                                    <div class="text-end">
                                        <div class="bg-info product-icon">
                                            <i class="fa fa-users" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="progress-showcase">
                                <div class="progress sm-progress-bar">
                                    <div class="progress-bar bg-info" role="progressbar" style="width: 70%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <div class="d-flex align-items-center justify-content-between">
                                    <small class="text-muted">الفترة السابقة</small>
                                    <small class="text-muted">70%</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            <div class="col-xl-4 col-sm-6 box-col-4">
                <div class="card ecommerce-widget">
                    <div class="card-body support-ticket-font">
                        <div class="row">
                            <div class="col-7">
                                @can('Chat list')
                                    <a href="{{ route('owner.chat.index') }}"><span> {{ _trans('complaints') }}</span></a>
                                @else
                                    {{ _trans('complaints') }}
                                @endcan
                                <h3 class="total-num counter">{{ $complaints }}</h3>
                            </div>
                            <div class="col-5">
                                <div class="text-end">
                                    <div class="bg-danger product-icon">
                                        <i class="fa fa-motorcycle" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="progress-showcase">
                            <div class="progress sm-progress-bar">
                                <div class="progress-bar bg-danger" role="progressbar" style="width: 70%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <div class="d-flex align-items-center justify-content-between">
                                <small class="text-muted">الفترة السابقة</small>
                                <small class="text-muted">70%</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    <!-- Container-fluid Ends-->
@endsection

