@if (count($data) > 0)
    <div class="repeater-default">
        <div data-repeater-list="areas">
            @foreach($data as $key => $value)
                @php
                    $regions = [];
                        if (!is_array($value) && $edit){
                            $regions = $value->regions->pluck('id')->toArray();
                        }else{
                            $regions = Arr::exists($value,'region_id') ? $value['region_id'] : [];
                        }
                @endphp
                <div data-repeater-item="" style="">
                    <div class="row">
                        <input type="hidden" name="areas[{{ $key }}][id]" value="{{ $value['id'] }}">
                        <input type="hidden" name="areas[{{ $key }}][status]" value="{{ $value['status'] }}">

                        <div class="col-md-3">
                            <label class="form-label" for="areas[{{ $key }}][governorate_id]">{{ _trans('Governorate Name') }}</label>
                            <select id="areas[{{ $key }}][governorate_id]"
                                    name="areas[{{ $key }}][governorate_id]"
                                    class="js-example-basic-single governorate_id  @error('areas.'.$key.'.governorate_id') is-invalid @enderror">
                                <option value="">{{ _trans('Select Governorate Name') }}</option>
                                @foreach($governorates as $governorate)
                                    <option value="{{ $governorate->id }}"
                                        @selected($value['governorate_id'] == $governorate->id)
                                    > {{ $governorate->translate(locale())?->name }}</option>
                                @endforeach

                            </select>
                            @error('areas.'.$key.'.governorate_id')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>

                        <div class="col-md-3 region_id">
                            <label class="form-label" for="areas[{{ $key }}][region_id]">{{ _trans('Region Name') }}</label>
                            <select id="areas[{{ $key }}][region_id]" name="areas[{{ $key }}][region_id]"
                                    multiple
                                    class="js-example-basic-single region_id @error('areas.'.$key.'.region_id') is-invalid @enderror">
                                {{-- error n +1 because is call same region --}}
                                @foreach(getRegions($value['governorate_id']) as $region)
                                    <option value="{{ $region->id }}"
                                        {{ in_array($region->id,$regions) ? 'selected' : ''}}
                                    > {{ $region->translate(locale())?->name }}</option>
                                @endforeach

                            </select>
                            @error('areas.'.$key.'.region_id')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>

                        <div class="col-md-3">
                            <label class="form-label" for="delivery_price">{{ _trans('Delivery price') }}</label>
                            <input id="delivery_price"
                                   type="number"
                                   step="any"
                                   min="0"
                                   name="areas[{{ $key }}][delivery_price]"
                                   class="form-control @error('areas.'.$key.'.delivery_price') is-invalid @enderror"
                                   placeholder="{{ _trans('Delivery price') }}"
                                   value="{{ $value['delivery_price'] }}"
                            >
                            @error('areas.'.$key.'.delivery_price')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>

                        <div class="col-md-3">
                            <label class="form-label" for="">{{ _trans('Minimum order price') }}</label>
                            <input id="min_order_price"
                                   type="number"
                                   step="any"
                                   min="0"
                                   name="areas[{{ $key }}][min_order_price]"
                                   class="form-control @error('areas.'.$key.'.min_order_price') is-invalid @enderror"
                                   placeholder="{{ _trans('Minimum order price') }}"
                                   value="{{ $value['min_order_price'] }}"
                            >
                            @error('areas.'.$key.'.min_order_price')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>

                        <div class="col-md-3 min_time">
                            <label class="form-label" for="">{{ _trans('Min delivery time') }} ({{ _trans('in minutes') }})</label>
                            <input
                                type="number"
                                step="1"
                                min="0"
                                name="areas[{{ $key }}][min_time]"
                                class="form-control min_time @error('areas.'.$key.'.min_time') is-invalid @enderror"
                                placeholder="{{ _trans('Min delivery time') }}"
                                value="{{ $value['min_time'] }}"
                            >
                            @error('areas.'.$key.'.min_time')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>

                        <div class="col-md-3">
                            <label class="form-label" for="">{{ _trans('Max delivery time') }} ({{ _trans('in minutes') }})</label>
                            <input
                                type="number"
                                step="1"
                                min="0"
                                name="areas[{{ $key }}][max_time]"
                                class="form-control max_time @error('areas.'.$key.'.max_time') is-invalid @enderror"
                                placeholder="{{ _trans('Max delivery time') }}"
                                value="{{ $value['max_time'] }}"
                            >
                            @error('areas.'.$key.'.max_time')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>

                        <div class="col-md-3 avgTime">
                            <label class="form-label" for="">{{ _trans('Average delivery time') }} ({{ _trans('in minutes') }})</label>
                            <input
                                type="number"
                                class="form-control avgTime"
                                value="{{($value['max_time']+$value['min_time'])/2}}"
                                placeholder="{{ _trans('Average delivery time') }}"
                                disabled
                            >
                        </div>
                        <div class="col-md-1">
                            <div class="form-group col-sm-12 col-md-2 text-center mt-2">
                                <br>
                                <div class="media-body icon-state">
                                    <label class="switch">
                                        <input id="{{ $value['id'] }}" type="checkbox" class="status" @checked($value['status']) >
                                        <span class="switch-state"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        @if(empty($value['id']))
                            <div class="col-md-1">
                                <div class="form-group col-sm-12 col-md-2 text-center mt-2">
                                    <br>
                                    <button type="button" id="deleteRepeater" class="btn btn-danger" data-repeater-delete="">
                                        {{--<i class="fas fa-trash-alt"></i>--}} {{ _trans('Delete') }}
                                    </button>
                                </div>
                            </div>
                        @endif
                    </div>
                    <hr>
                </div>
            @endforeach
        </div>
        <div class="form-group overflow-hidden">
            <div class="col-12">
                <button type="button" id="addRepeater" data-repeater-create="" class="btn btn-primary">
                    <i class="far fa-plus-square"></i> {{ _trans('Add') }}
                </button>
            </div>
        </div>
    </div>

@else
    <div class="repeater-default">
        <div data-repeater-list="areas">
            <div data-repeater-item="" style="">
                <div class="row">
                    <input type="hidden" name="id" value=" ">
                    <input type="hidden" name="status" value="1">
                    <div class="col-md-3">
                        <label class="form-label" for="">{{ _trans('Governorate Name') }}</label>
                        <select name="governorate_id"
                                class="js-example-basic-single governorate_id @error('governorate_id') is-invalid @enderror">
                            <option value="">{{ _trans('Select Governorate Name') }}</option>

                        </select>
                        @error('governorate_id')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-3 region_id">
                        <label class="form-label" for="">{{ _trans('Region Name') }}</label>
                        <select name="region_id"
                                multiple
                                class="js-example-basic-single region_id @error('region_id') is-invalid @enderror">
                        </select>
                        @error('region_id')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-3">
                        <label class="form-label" for="">{{ _trans('Delivery price') }}</label>
                        <input id="delivery_price"
                               type="number"
                               step="any"
                               min="0"
                               name="delivery_price"
                               class="form-control @error('delivery_price') is-invalid @enderror"
                               placeholder="{{ _trans('Enter') }} {{ _trans('Delivery price') }}"
                               value="{{ old('delivery_price',$edit ? $delivery_area->delivery_price : null) }}"
                        >
                        @error('delivery_price')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-3">
                        <label class="form-label" for="">{{ _trans('Minimum order price') }}</label>
                        <input id="min_order_price"
                               type="number"
                               step="any"
                               min="0"
                               name="min_order_price"
                               class="form-control @error('min_order_price') is-invalid @enderror"
                               placeholder="{{ _trans('Enter') }} {{ _trans('Minimum order price') }}"
                               value="{{ old('min_order_price',$edit ? $delivery_area->min_order_price : null) }}"
                        >
                        @error('min_order_price')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-3 min_time">
                        <label class="form-label" for="">{{ _trans('Min delivery time') }} ({{ _trans('in minutes') }})</label>
                        <input
                            type="number"
                            step="1"
                            min="0"
                            name="min_time"
                            class="form-control min_time @error('min_time') is-invalid @enderror"
                            placeholder="{{ _trans('Enter') }} {{ _trans('Min delivery time') }}"
                            value="{{ old('min_time',$edit ? $delivery_area->min_time : 0) }}"
                        >
                        @error('min_time')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-3">
                        <label class="form-label" for="">{{ _trans('Max delivery time') }} ({{ _trans('in minutes') }})</label>
                        <input
                            type="number"
                            step="1"
                            min="0"
                            name="max_time"
                            class="form-control max_time @error('max_time') is-invalid @enderror"
                            placeholder="{{ _trans('Enter') }} {{ _trans('Max delivery time') }}"
                            value="{{ old('max_time',$edit ? $delivery_area->max_time : 0) }}"
                        >
                        @error('max_time')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-3 avgTime">
                        <label class="form-label" for="">{{ _trans('Average delivery time') }} ({{ _trans('in minutes') }})</label>
                        <input
                            type="number"
                            class="form-control avgTime"
                            placeholder="{{ _trans('Enter') }} {{ _trans('Average delivery time') }}"
                            disabled
                        >
                    </div>

                    <div class="col-md-1">
                        <div class="form-group col-sm-12 col-md-2 text-center mt-2">
                            <br>
                            <button type="button" id="deleteRepeater" class="btn btn-danger" data-repeater-delete="">
                                {{--<i class="fas fa-trash-alt"></i>--}} {{ _trans('Delete') }}
                            </button>
                        </div>
                    </div>
                </div>
                <hr>
            </div>
        </div>
        <div class="form-group overflow-hidden">
            <div class="col-12">
                <button type="button" id="addRepeater" data-repeater-create="" class="btn btn-primary">
                    {{--<i class="far fa-plus-square"></i> --}} {{ _trans('Add') }}
                </button>
            </div>
        </div>
    </div>
@endif
@include('layouts.ajax.update-status',['class' => 'status','route' => route('area.update-status')])

@push('scripts')
    <script>
        var limit = 1;
        $(document).ready(function () {
            limit = {{ count($data) == 0 ? 1 : count($data)  }};
            limitRepeater();
        })
        $(document).on('click', '#addRepeater', function () {
            limit++;
            if (limit > 30) {
                $('#addRepeater').attr('disabled', true)
            }
            var countryId = $('#country_id option:selected').val();
            getGovernorateForLastItem(countryId, null)
            setTimeout(function () {
                $('.js-example-basic-single').select2();
            }, 100);
        })
        $(document).on('click', '#deleteRepeater', function () {
            limit--;
            limitRepeater();
        })

        function limitRepeater() {
            if (limit > 30) {
                $('#addRepeater').attr('disabled', true)
            } else {
                $('#addRepeater').attr('disabled', false)
            }
        }

    </script>
@endpush
