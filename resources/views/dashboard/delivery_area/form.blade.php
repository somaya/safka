@extends('layouts.master')

@section('title',_trans('Delivery Areas'))


@section('content')
    <div class="container-fluid">
        <div class="page-title">
            <div class="row">
                <div class="col-6 col-sm-6">
                    <h3>{{ $edit ? _trans('Edit Delivery Area') : _trans('Add Delivery Area') }}</h3>
                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid default-dash">
        <div class="row">
            <div class="col-12 col-sm-6">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                        </a>
                    </li>
                    @canany(['Delivery list','Delivery edit','Delivery delete'])
                        <li class="breadcrumb-item ">
                            <a href="{{ route('delivery-area.index') }}">{{ _trans('Delivery Areas') }}</a>
                        </li>
                    @endcanany
                    <li class="breadcrumb-item active">
                        {{ $edit ? _trans('Edit Delivery Area') : _trans('Add Delivery Area') }}
                    </li>
                </ol>
            </div>

            <div class="col-md-12">
                @if ($edit)
                    {{ Form::open(['route' => ['delivery-area.update',$delivery_area->id],'method' => 'PUT','files' => true,'class' => 'form mb-15 form-submit','id' =>'kt_contact_form']) }}
                    {{ Form::hidden('id',$delivery_area->id) }}
                @else
                    {{ Form::open(['route' => 'delivery-area.store','method' => 'POST','files' => true,'class' => 'form mb-15','id' =>'kt_contact_form']) }}
                @endif
                <div class="row g-3">
                    <div class="col-md-4">
                        <label class="form-label" for="code">{{ _trans('Delivery Area Code') }}</label>
                        <input id="code"
                               disabled
                               type="text"
                               class="form-control"
                               placeholder="{{ _trans('Delivery Area Code') }}"
                               value="{{ getCodeTable('Area','delivery_areas',$edit,$edit ? $delivery_area->id : null) }}"
                        >
                    </div>

                    @if ($edit)
                        <div class="col-md-4">
                            <label class="form-label" for="date">{{ _trans('Date') }}</label>
                            <input id="date"
                                   disabled
                                   type="text"
                                   class="form-control"
                                   value="{{ $delivery_area->created_at->diffForHumans() }}"
                            >
                        </div>
                    @endif

                    @if (request('guard')=='admin')
                        <div class="col-md-4">
                            <label class="form-label" for="owner_id">{{ _trans('Owner name') }}</label>
                            <select id="owner_id" name="owner_id"
                                    class="js-example-basic-single @error('owner_id') is-invalid @enderror">
                                <option value="">{{ _trans('Select owner name') }}</option>
                                @foreach($data as $owner)
                                    <option value="{{ $owner->id }}"
                                        @selected(old('owner_id',$edit ? $delivery_area->owner_id : null) == $owner->id)
                                    >{{ $owner->user?->name }}</option>
                                @endforeach
                            </select>
                            @error('owner_id')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="form-label" for="shop_id">{{ _trans('Shop name') }}</label>
                            <select id="shop_id" name="shop_id"
                                    class="js-example-basic-single @error('shop_id') is-invalid @enderror">
                                <option value="">{{ _trans('Select Shop name') }}</option>

                            </select>
                            @error('shop_id')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>

                    @else
                        <div class="col-md-4">
                            <label class="form-label" for="shop_id">{{ _trans('Shop name') }}</label>
                            <select id="shop_id" name="shop_id"
                                    class="js-example-basic-single @error('shop_id') is-invalid @enderror">
                                <option value="">{{ _trans('Select Shop name') }}</option>
                                @foreach($data as $shop)
                                    <option value="{{ $shop->id }}"
                                        @selected(old('shop_id',$edit ? $delivery_area->shop_id : null) == $shop->id)
                                    >{{ $shop->translate(locale())?->name }}</option>
                                @endforeach
                            </select>
                            @error('shop_id')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>

                        {{--{{ Form::hidden('owner_id',$edit ? $delivery_area->owner_id : getOwner(auth()->user()->creatorId())) }}--}}

                    @endif

                    <div class="col-md-4">
                        <label class="form-label" for="country_id">{{ _trans('Country Name') }}</label>
                        <select id="country_id" name="country_id"
                                class="js-example-basic-single @error('country_id') is-invalid @enderror">
                            <option value="">{{ _trans('Select Country Name') }}</option>
                            @foreach($countries as $country)
                                <option value="{{ $country->id }}" @selected(old('country_id',$edit ? $delivery_area->country_id : null) == $country->id)>{{ $country->name }}</option>
                            @endforeach
                        </select>
                        @error('country_id')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-12">
                        <h4>{{ _trans('Delivery areas') }}</h4>
                        <hr>
                        @include('dashboard.delivery_area.form-repeater',['edit' => $edit,'governorates' => $edit ? getGovernorates($delivery_area->country_id) :  (old('country_id') ? getGovernorates(old('country_id')): []) ,'data' => $edit ? old('areas',$delivery_area->areas ?:[]) :  (!empty(old('areas')) ? (old('areas')) : []) ])

                    </div>

                    <div class="col-md-12">
                        <div class="m-t-50 d-flex justify-content-end">
                            <button type="submit" class="btn btn-primary">{{ $edit ?_trans('Update') : _trans('Save') }}</button>
                        </div>
                    </div>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection
@push('scripts')

    @include('layouts.ajax.countries')
    <script src="{{ asset('assets/repeater/jquery.repeater.min.js') }}"></script>
    <script src="{{ asset('assets/repeater/form-repeater.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            @if(!is_null(old('owner_id')))
            getShops({{ old('owner_id') }}, {{ !is_null(old('shop_id')) ? old('shop_id') : 0 }})
            @endif
            @if($edit)
            getShops({{ old('owner_id',$edit ? $delivery_area->owner_id : 0) }}, {{ old('shop_id',$edit ? $delivery_area->shop_id : 0) }})
            @endif
        })

        $(document).on('change', '#owner_id', function (e) {
            e.preventDefault();
            var ownerId = $('#owner_id option:selected').val();
            if (ownerId == '' || ownerId == null) {
                return false;
            } else {
                getShops(ownerId, null)
            }
        });

        function getShops(ownerId, selectedId = null) {
            $("#shop_id").empty();
            $("#shop_id").append('<option value="">{{ _trans('Select shop name') }}</option>');

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                url: "{{ route('ajax.shop-by-owner') }}",
                method: 'POST',
                cache: false,
                data: {
                    owner_id: ownerId
                },
                success: function (response) {
                    if (response.status == true) {
                        if (response.data.length > 0) {
                            $.each(response.data, function (index, value) {
                                var selected = '';
                                if (selectedId == value.id) {
                                    selected = 'selected';
                                }
                                $("#shop_id").append('<option ' + selected + ' value="' + value.id + '">' + value.name + '</option>');

                            });
                        }

                    } else {
                        toastr.error(response.error)
                    }

                },

            });
        }

        $(document).on('change', '.max_time', function (e) {
            e.preventDefault();
            var max_time = $(this).val();
            var min_time = $(this).parent().siblings('.min_time').children('.min_time');
            var avgTime = $(this).parent().siblings('.avgTime').children('.avgTime');
            avgTime.val(0);
            var result = (parseInt(min_time.val()) + parseInt(max_time)) / 2;
            avgTime.val(result)

        });

    </script>
    <script>
        $(document).on('change', '#country_id', function (e) {

            e.preventDefault();
            $(".governorate_id").empty();
            $(".governorate_id").append('<option value="">{{ _trans('Select Governorate') }}</option>');
            var countryId = $('#shop_id option:selected').val();
            if (countryId == '' || country_id == null) {
                return false;
            } else {

                getGovernorate(countryId, null)
            }

        });
        $(document).on('change', '.governorate_id', function (e) {

            e.preventDefault();
            var governorate = $(this);
            var region = governorate.parent().siblings('.region_id').children('.region_id');

            region.empty();
            region.append('<option value="">{{ _trans('Select Region') }}</option>');
            var governorateId = $(this).val();
            if (governorateId == '' || governorateId == null) {
                return false;
            } else {

                getRegion(governorateId, region, null)
            }

        });

        function getGovernorate(countryId, selectedId = null) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                url: "{{ route('ajax.governorate-by-country') }}",
                method: 'POST',
                cache: false,
                data: {
                    country_id: countryId
                },
                success: function (response) {
                    if (response.status == true) {
                        if (response.data.length > 0) {
                            $.each(response.data, function (index, value) {
                                var selected = '';
                                if (selectedId == value.id) {
                                    selected = 'selected';
                                }
                                $(".governorate_id").append('<option ' + selected + ' value="' + value.id + '">' + value.name + '</option>');
                            });
                            // $(".product_id option").val(function(idx, val) {
                            //     $(this).siblings('[value="'+ val +'"]').remove();
                            // });
                        }

                    } else {
                        toastr.error(response.error)
                    }

                },

            });
        }

        function getRegion(governorateId, region, selectedId = null) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                url: "{{ route('ajax.region-by-governorate') }}",
                method: 'POST',
                cache: false,
                data: {
                    governorate_id: governorateId
                },
                success: function (response) {
                    region.empty();
                    if (response.status == true) {
                        if (response.data.length > 0) {
                            $.each(response.data, function (index, value) {
                                var selected = '';
                                if (selectedId == value.id) {
                                    selected = 'selected';
                                }
                                region.append('<option ' + selected + ' value="' + value.id + '">' + value.name + '</option>');
                            });
                            // $(".product_id option").val(function(idx, val) {
                            //     $(this).siblings('[value="'+ val +'"]').remove();
                            // });
                        }

                    } else {
                        toastr.error(response.error)
                    }

                },

            });
        }

        function getGovernorateForLastItem(countryId, selectedId = null) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                url: "{{ route('ajax.governorate-by-country') }}",
                method: 'POST',
                cache: false,
                data: {
                    country_id: countryId
                },
                success: function (response) {
                    if (response.status == true) {
                        if (response.data.length > 0) {
                            $(".governorate_id").last().empty();
                            $(".governorate_id").last().append('<option value="">{{ _trans('Select Governorate') }}</option>');
                            $.each(response.data, function (index, value) {
                                var selected = '';
                                if (selectedId == value.id) {
                                    selected = 'selected';
                                }
                                $(".governorate_id").last().append('<option ' + selected + ' value="' + value.id + '">' + value.name + '</option>');
                            });
                        }

                    } else {
                        toastr.error(response.error)
                    }

                },

            });
        }

    </script>

@endpush

