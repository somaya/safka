@extends('layouts.master')
@section('title',$edit ? _trans('Edit Discount') : _trans('Add Discount'))
@section('content')
    <div class="container-fluid">
        <div class="page-title">
            <div class="row">
                <div class="col-6 col-sm-6">
                    <h3>{{ $edit ? _trans('Edit Discount') : _trans('Add Discount') }}</h3>
                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid default-dash">
        <div class="row">
            <div class="col-12 col-sm-6">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="{{ route('admin.dashboard') }}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                        </a>
                    </li>
                    <li class="breadcrumb-item ">
                        @canany(['Discount list','Discount edit','Discount delete'])
                            <a href="{{ route('discount.index') }}">{{ _trans('Discounts') }}</a>
                        @endcanany
                    </li>
                    <li class="breadcrumb-item active">
                        {{ $edit ? _trans('Edit Discount') : _trans('Add Discount') }}
                    </li>
                </ol>
            </div>

            <div class="col-md-12">
                @if ($edit)
                    {{ Form::open(['route' => ['discount.update',$discount->id],'method' => 'PUT','files' => true,'class' => 'form mb-15 form-submit','id' =>'kt_contact_form']) }}
                    {{ Form::hidden('id',$discount->id) }}
                @else
                    {{ Form::open(['route' => 'discount.store','method' => 'POST','files' => true,'class' => 'form mb-15','id' =>'kt_contact_form']) }}
                @endif

                <div class="row g-3">
                    <div class="col-md-4">
                        <label class="form-label" for="code">{{ _trans('Discount Code') }}</label>
                        <input id="code"
                               disabled
                               type="text"
                               class="form-control"
                               placeholder="{{ _trans('Discount Code') }}"
                               value="{{ getCodeTable('Discount','discounts',$edit,$edit ? $discount->id : null) }}"
                        >
                    </div>
                    @if (request('guard')=='admin')
                    <div class="col-md-4">
                        <label class="form-label" for="owner_id">{{ _trans('Owner name') }}</label>
                        <select id="owner_id" name="owner_id"
                                class="js-example-basic-single @error('owner_id') is-invalid @enderror">
                            <option value="">{{ _trans('Select owner name') }}</option>
                            @foreach($data as $owner)
                                <option value="{{ $owner->id }}"
                                    @selected(old('owner_id',$edit ? $discount->owner_id : null) == $owner->id)
                                >{{ $owner->user->name }}</option>
                            @endforeach
                        </select>
                        @error('owner_id')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="form-label" for="shop_id">{{ _trans('Shop name') }}</label>
                        <select id="shop_id" name="shop_id"
                                class="js-example-basic-single shop_id @error('shop_id') is-invalid @enderror">
                            <option value="">{{ _trans('Select shop name') }}</option>

                        </select>
                        @error('shop_id')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>
                    @else
                        <div class="col-md-4">
                            <label class="form-label" for="shop_id">{{ _trans('Shop name') }}</label>
                            <select id="shop_id" name="shop_id"
                                    class="js-example-basic-single @error('shop_id') is-invalid @enderror">
                                <option value="">{{ _trans('Select Shop name') }}</option>
                                @foreach($data as $shop)
                                    <option value="{{ $shop->id }}"
                                        @selected(old('shop_id',$edit ? $discount->shop_id : null) == $shop->id)
                                    >{{ $shop->translate(locale())?->name }}</option>
                                @endforeach
                            </select>
                            @error('shop_id')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>
                    @endif

                    <div class="col-md-4">
                        <label class="form-label" for="product_id">{{ _trans('Product name') }}</label>
                        <select id="product_id"
                                name="product_id"
                                class="js-example-basic-single product_id @error('product_id') is-invalid @enderror">
                            <option value="">{{ _trans('Select product name') }}</option>
                        </select>
                        @error('product_id')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4" id="product_description_div" style="display: {{$edit?'':'none'}}">
                        <label class="form-label" for="product_description">{{ _trans('Description') }}</label>
                        <textarea id="product_description"
                                  type="text"
                                  disabled
                                  class="form-control"
                                  required="" rows="5"></textarea>
                    </div>

                    <div class="col-md-4" style="display: none" id="product_price_div">
                        <label class="form-label" for="">{{ _trans('Price Before') }}</label>
                        <input id="product_price"
                               type="text"
                               class="form-control"
                               disabled>
                    </div>

                    <div class="col-md-4">
                        <label class="form-label" for="percent">{{ _trans('Percent') }}</label>
                        <input id="percent"
                               type="number"
                               value="{{ old('percent',$edit ? $discount->percent : 1) }}"
                               min="0"
                               max="99.99"
                               step="0.1"
                               name="percent"
                               class="form-control @error('percent') is-invalid @enderror"
                               placeholder="{{ _trans('percent') }}"
                        >
                        @error('percent')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="form-label" for="price_after">{{ _trans('Price After') }}</label>
                        <input id="price_after"
                               type="text"
                               disabled
                               class="form-control @error('price_after') is-invalid @enderror"
                               placeholder="{{ _trans('Price After') }}"
                               value="{{ old('price_after',$edit ? $discount->price_after : null) }}"
                        >
                        @error('price_after')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="form-label" for="start">{{ _trans('Start') }}</label>
                        <input id="start"
                               type="date"
                               name="start"
                               class="form-control @error('start') is-invalid @enderror"
                               value="{{ old('start',$edit ? $discount->start : null) }}"
                        >
                        @error('start')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="form-label" for="end">{{ _trans('End') }}</label>
                        <input id="end"
                               type="date"
                               name="end"
                               class="form-control @error('end') is-invalid @enderror"
                               value="{{ old('end',$edit ? $discount->end : null) }}"
                        >
                        @error('end')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>


                    <div class="col-md-12">
                        <div class="m-t-50 d-flex justify-content-end">
                            <button type="submit" class="btn btn-primary">{{ $edit ?_trans('Update') : _trans('Save') }}</button>
                        </div>
                    </div>


                </div>


                {{ Form::close() }}
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection
@include('layouts.partials.ckeditor',['inputClass' => 'textarea'])
@push('scripts')
    <script>
        $(document).ready(function () {
            @if(!is_null(old('owner_id')))
            getShops({{ old('owner_id') }}, {{ !is_null(old('shop_id')) ? old('shop_id') : 0 }})
            getProducts({{ old('shop_id') }}, {{ !is_null(old('product_id')) ? old('product_id') : 0 }})
            getProductDetails({{ old('product_id') }})
            @endif

            @if($edit)
            getShops({{ old('owner_id',$edit ? $discount->owner_id : 0) }}, {{ old('shop_id',$edit ? $discount->shop_id : 0) }})
            getProducts({{ old('shop_id',$edit ? $discount->shop_id : 0) }}, {{ old('product_id',$edit ? $discount->product_id : 0) }})
            getProductDetails({{ old('product_id',$edit ? $discount->product_id : 0) }})
            @endif
        })
    </script>
    @include('layouts.ajax.shop')
    @include('layouts.ajax.products-by-shop')
    <script>
        var typingTimer;                //timer identifier
        var doneTypingInterval = 2000;  //time in ms, 2 seconds for example
        $(document).on('change', '#product_id', function (e) {
            e.preventDefault();
            var productId = $('#product_id option:selected').val();
            $('#percent').val('');
            $('#price_after').val('');
            if (productId == '' || productId == null) {
                return false;
            } else {
                getProductDetails(productId, null)
            }

        });

        function getProductDetails(productId) {
            $("#product_price_div").hide();
            $("#product_description_div").hide();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                url: "{{ route('ajax.getProductDetails') }}",
                method: 'POST',
                cache: false,
                data: {
                    product_id: productId
                },
                success: function (response) {
                    if (response.status == true) {
                        if (response.data) {
                            $('#product_price_div').show();
                            $('#product_description_div').show();
                            $('#product_price').val(response.data.price);
                            $('#product_description').val(response.data.description);
                            var $input = $('#percent');
                            $input.on("keyup change", function (e) {
                                clearTimeout(typingTimer);
                                typingTimer = setTimeout(doneTyping(response.data.price, $('#percent').val()), doneTypingInterval);
                            });
                            $input.on("keydown change", function () {
                                clearTimeout(typingTimer);
                            });
                        }
                    } else {
                        toastr.error(response.data)
                    }
                },
            });
        }

        function doneTyping(price_before, percent) {
            var price_after = (price_before - (price_before * percent / 100)).toFixed(2)
            $('#price_after').val(price_after)
        }

    </script>

@endpush


