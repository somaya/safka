@extends('layouts.master')
@section('title',_trans('Discounts'))
@section('content')

        <div class="container-fluid">
            <div class="page-title">
                <div class="row">
                    <div class="col-6 col-sm-6">
                        <h3>{{ _trans('Discount Details') }}</h3>
                    </div>
                    <div class="col-6 text-right">

                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid default-dash">
            <div class="row">

                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card card-details">
                                <h4 class="title-details">{{_trans('Discount Details')}}</h4>
                                <div class="">
                                    <p>{{_trans('Product')}} :<span>{{$discount->product->translate(locale())?->name}}</span></p>
                                    <p>{{_trans('Shop')}} :<span>{{$discount->shop->translate(locale())?->name}}</span></p>
                                    <p>{{_trans('Start')}} :<span>{{$discount->start}}</span></p>
                                    <p>{{_trans('End')}} :<span>{{$discount->end}}</span></p>
                                    <p>{{_trans('Price Before')}} :<span>{{$discount->price_before}}</span></p>
                                    <p>{{_trans('Percent')}} :<span>{{$discount->percent}}</span></p>
                                    <p>{{_trans('Price After')}} :<span>{{$discount->price_after}}</span></p>

                                    <p>{{_trans('Icon')}} : <span>
                                  <img class="mb-2 image-preview" src="{{getAvatar($discount->product->icon)}}">
                              </span></p>

                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="card card-details">
                                <h4 class="title-details">{{ _trans('Orders')}}</h4>
                                <div class="">
                                    <p>{{ _trans('Count')}} : <span>{{$orders->count()}}</span></p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
        <!-- Container-fluid Ends-->

@endsection
