@extends('layouts.master')

@section('title',_trans('Driver'))


@section('content')
    <div class="container-fluid">
        <div class="page-title">
            <div class="row">
                <div class="col-6 col-sm-6">
                    <h3>{{ $edit ? _trans('Edit Driver') : _trans('Add Driver') }}</h3>
                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid default-dash">
        <div class="row">
            <div class="col-12 col-sm-6">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                        </a>
                    </li>
                    @canany(['Driver list','Driver edit','Driver delete'])
                        <li class="breadcrumb-item ">
                            <a href="{{ route('driver.index') }}">{{ _trans('Driver') }}</a>
                        </li>
                    @endcanany
                    <li class="breadcrumb-item active">
                        {{ $edit ? _trans('Edit Driver') : _trans('Add Driver') }}
                    </li>
                </ol>
            </div>
            <div class="col-md-12">
                @if ($edit)
                    {{ Form::open(['url' => route('driver-order.update'),'method' => 'POST','class' => 'form mb-15 form-submit','id' =>'kt_contact_form']) }}
                @else
                    {{ Form::open(['url' => route('driver-order.store'),'method' => 'POST','class' => 'form mb-15','id' =>'kt_contact_form']) }}
                @endif
                <div class="row g-3">
                    @if ($edit && $driver->status_orders_count > 0)
                        <div class="alert alert-warning" role="alert">
                            <h4 class="alert-heading">{{ _trans('Waring') }}!</h4>
                            <p>{{ _trans('It is not possible to add or modify the requests because this driver left and did not return or change the status of the requests with him to a status completed') }}</p>
                        </div>
                    @endif


                    @if (in_array(auth()->user()->userable_type, [\App\Models\Owner::class,\App\Models\ShopStaff::class]))
                        <div id="shopId_div" class="col-md-4">
                            <label class="form-label" for="shop_id">{{ _trans('Shop name') }}</label>
                            <select id="shop_id" name="shop_id"
                                    class="js-example-basic-single @error('shop_id') is-invalid @enderror">
                                <option>{{ _trans('Select shop name') }}</option>
                                @foreach($shops as $shop)
                                    <option value="{{ $shop->id }}"
                                        @selected(old('shop_id',$edit ? $driver->shop_id : null) == $shop->id)>
                                        {{ $shop->translate(locale())?->name }}</option>
                                @endforeach
                            </select>
                            @error('shop_id')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>
                    @else

                        <div id="ownerId_div" class="col-md-4">
                            <label class="form-label" for="owner_id">{{ _trans('Owner name') }}</label>
                            <select id="owner_id" name="owner_id"
                                    class="js-example-basic-single @error('owner_id') is-invalid @enderror">
                                <option>{{ _trans('Select owner name') }}</option>
                                @foreach($owners as $owner)

                                    <option value="{{ $owner->id }}"
                                        @selected(old('owner_id',$edit ? $driver->owner_id : null) == $owner->id)>
                                        {{ $owner->user->name }}</option>
                                @endforeach
                            </select>
                            @error('owner_id')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>

                        <div id="shopId_div" class="col-md-4">
                            <label class="form-label" for="shop_id">{{ _trans('Shop name') }}</label>
                            <select id="shop_id" name="shop_id"
                                    class="js-example-basic-single @error('shop_id') is-invalid @enderror">
                                <option>{{ _trans('Select shop name') }}</option>
                            </select>
                            @error('shop_id')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>
                    @endif

                    <div class="col-md-4">
                        <label class="form-label" for="driver_id">{{ _trans('Driver name') }}</label>
                        <select id="driver_id" name="driver_id"
                                class="js-example-basic-single @error('driver_id') is-invalid @enderror">
                            <option>{{ _trans('Select driver name') }}</option>

                        </select>
                        @error('driver_id')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="form-label" for="order_id">{{ _trans('Order code') }}</label>
                        <select id="order_id"
                                name="order_id[]"
                                multiple
                                class="js-example-basic-single @error('order_id') is-invalid @enderror">
                        </select>
                        @error('order_id')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-12">
                        <div class="m-t-50 d-flex justify-content-end">
                            <button type="submit" class="btn btn-primary">{{ $edit ?_trans('Update Driver Order') : _trans('Save Driver Order') }}</button>
                        </div>
                    </div>

                </div>

                {{ Form::close() }}
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection


@push('scripts')
    <script>
        $(document).ready(function () {
            @if($edit)
            getShops('{{ old('owner_id',$edit ? $driver->owner_id : 0) }}', '{{ old('shop_id',$edit ? $driver->shop_id : 0) }}')
            getDriverAndOrder('{{ old('shop_id',$edit ? $driver->shop_id : 0) }}', '{{ old('driver_id',$edit ? $driver->id : 0) }}', '{!! implode(',',old('order_id',$edit ? $orders : [])) !!}')
            @else
            @if(!is_null(old('owner_id')))
            getShops('{{ old('owner_id') }}', '{{ !is_null(old('shop_id')) ? old('shop_id') : 0 }}')
            @endif
            @if(!is_null(old('shop_id')))
            getDriverAndOrder('{{ old('shop_id') }}', '{{ !is_null(old('driver_id')) ? old('driver_id') : 0 }}', '{{ !is_null(old('order_id')) ? implode(',',old('order_id')) : '' }}')
            @endif
            @endif
        })

        $(document).on('change', '#owner_id', function (e) {
            e.preventDefault();
            $("#shop_id").empty();
            $("#shop_id").append('<option value="">{{ _trans('Select shop name') }}</option>');
            $("#driver_id").empty();
            $("#driver_id").append('<option value="">{{ _trans('Select driver name') }}</option>');
            $("#order_id").empty();

            var ownerId = $('#owner_id option:selected').val();
            if (ownerId == '' || ownerId == null) {
                return false;
            } else {
                getShops(ownerId, null)
            }
        });

        function getShops(ownerId, selectedId = null) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                url: "{{ route('ajax.shop-by-owner') }}",
                method: 'POST',
                cache: false,
                data: {
                    owner_id: ownerId
                },
                success: function (response) {
                    if (response.status == true) {
                        if (response.data.length > 0) {
                            $.each(response.data, function (index, value) {
                                var selected = '';
                                if (selectedId == value.id) {
                                    selected = 'selected';
                                }
                                $("#shop_id").append('<option ' + selected + ' value="' + value.id + '">' + value.name + '</option>');

                            });
                        }

                    } else {
                        toastr.error(response.error)
                    }

                },

            });
        }

        $(document).on('change', '#shop_id', function (e) {
            e.preventDefault();
            $("#driver_id").empty();
            $("#driver_id").append('<option value="">{{ _trans('Select driver name') }}</option>');
            $("#order_id").empty();
            var shop_id = $('#shop_id option:selected').val();
            if (shop_id == '' || shop_id == null) {
                return false;
            } else {
                getDriverAndOrder(shop_id)
            }
        });

        function getDriverAndOrder(shop_id, driverId = null, orderIds = null) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                url: "{{ route('ajax.drivers-orders-shop') }}",
                method: 'POST',
                cache: false,
                data: {
                    shop_id: shop_id
                },
                success: function (response) {
                    if (response.status == true) {
                        let drivers = response.data.drivers;
                        let orders = response.data.orders;
                        if (orders.length > 0) {
                            $.each(orders, function (index, value) {
                                $("#order_id").append('<option value="' + value.id + '">' + value.code + '</option>');
                            });
                            if (orderIds != '' && orderIds != null) {
                                $.each(orderIds.split(","), function (i, e) {
                                    $("#order_id option[value='" + e + "']").prop("selected", true);
                                });
                            }
                        }
                        if (drivers.length > 0) {
                            $.each(drivers, function (index, value) {
                                var selected = '';
                                if (driverId == value.id) {
                                    selected = 'selected';
                                }
                                $("#driver_id").append('<option ' + selected + ' value="' + value.id + '">' + value.name + '</option>');

                            });
                        }

                    } else {
                        toastr.error(response.error)
                    }

                },

            });
        }

    </script>
@endpush
