@extends('layouts.master')
@section('title',_trans('Driver orders'))
@section('content')
    <div class="container-fluid">
        <div class="page-title">
            <div class="row">
                <div class="col-6 col-sm-6">
                    <h3>{{_trans('Driver orders')}}</h3>
                </div>
                @can('DriverOrder add')
                    <div class="col-6 text-right">
                        <a href="{{ route('driver-order.create') }}" class="btn btn-primary">{{ _trans('Assign order') }}</a>
                    </div>
                @endcan
            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid default-dash">
        <div class="row">
            <div class="col-12 col-sm-6">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">

                        <a href="#">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                        </a>

                    </li>
                    <li class="breadcrumb-item active"><a href="#">{{ _trans('Driver orders') }}</a></li>
                </ol>
            </div>
            <div class="col-xl-12 col-md-12 ">
                <form action="{{ url()->current() }}" method="GET">
                    <div class="left-side-header">
                        <div class="row justify-content-between align-items-center">
                            <x-search :columns="$columns" sort="true" />
                            <div class="col-md-2">
                                <p class="mb-0 font-sm d-flex align-items-center justify-content-end">{{ _trans('Count Driver orders') }} :
                                    <span class="d-block font-md text-danger">({{ $data->total() }})</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </form>

                <div class="table-responsive custom-scrollbar p-t-30">
                    <table class="table">
                        <thead>
                        <tr>
                            <th><span>{{ _trans('SL')}}  </span></th>
                            <th><span>{{ _trans('Date')}} </span></th>
                            <th><span>{{ _trans('Driver name')}} </span></th>
                            @if(request('guard')=='admin')
                                <th><span>{{ _trans('Owner name')}}  </span></th>
                                <th><span>{{ _trans('Shop name')}}  </span></th>
                            @else
                                <th><span>{{ _trans('Working with')}}  </span></th>
                            @endif
                            <th><span>{{ _trans('Amount orders')}}  </span></th>
                            <th><span>{{ _trans('Orders')}} </span></th>
                            <th><span>{{ _trans('action')}}</span></th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $index => $item)
                            <tr>
                                <td class="text-main">#{{ $item->id }}</td>
                                <td>{{$item->created_at->format('d-m-Y') }}</td>
                                <td>{{$item->name }}</td>
                                @if(request('guard')=='admin')
                                    <td>{{ !empty($item->owner) ? $item->owner?->user?->name : _trans('driver system') }}</td>
                                    <td>{{ !empty($item->shop) ? $item->shop?->translate(locale())?->name : _trans('driver system') }}</td>
                                @else
                                    <td>{{ $item->owner_type == 'system' ? _trans('system') : _trans('With me')}}</td>
                                @endif
                                <td>{{ $item->orders_sum_total_price }}</td>
                                <td><a href="{{ route('order.index').'/?driver='.$item->id }}">{{ $item->orders_count }}</a></td>
                                <td>
                                    <a href="{{ route('driver-order.edit',$item->id) }}" class="btn btn-primary">{{ _trans('Edit') }}</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $data->appends(request()->query())->links('layouts.partials.pagination') }}

                @if( $data->count() == 0)
                    <div class="empty-data">
                        <img src="{{ asset('assets')}}/images/nodata.svg">
                        <h4>{{ _trans('No_data_to_show')}}</h4>
                    </div>
                @endif

            </div>

        </div>
    </div>

    <!-- Container-fluid Ends-->
@endsection
@include('layouts.ajax.search_input')
