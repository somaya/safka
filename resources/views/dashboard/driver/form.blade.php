@extends('layouts.master')

@section('title',_trans('Driver'))


@section('content')
    <div class="container-fluid">
        <div class="page-title">
            <div class="row">
                <div class="col-6 col-sm-6">
                    <h3>{{ $edit ? _trans('Edit Driver') : _trans('Add Driver') }}</h3>
                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid default-dash">
        <div class="row">
            <div class="col-12 col-sm-6">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                        </a>
                    </li>
                    @canany(['Driver list','Driver edit','Driver delete'])
                        <li class="breadcrumb-item ">
                            <a href="{{ route('driver.index') }}">{{ _trans('Driver') }}</a>
                        </li>
                    @endcanany
                    <li class="breadcrumb-item active">
                        {{ $edit ? _trans('Edit Driver') : _trans('Add Driver') }}
                    </li>
                </ol>
            </div>
            <div class="col-md-12">
                @if ($edit)
                    {{ Form::open(['url' => route('driver.update',$driver->id),'method' => 'PUT','files' => true,'class' => 'form mb-15 form-submit','id' =>'kt_contact_form']) }}
                    {{ Form::hidden('id',$driver->id) }}
                @else
                    {{ Form::open(['url' => route('driver.store'),'method' => 'POST','files' => true,'class' => 'form mb-15','id' =>'kt_contact_form']) }}
                @endif
                <div class="row g-3">

                    @if (auth()->user()->userable_type ==\App\Models\Owner::class || auth()->user()->userable_type==\App\Models\ShopStaff::class )
                        <div id="shopId_div" class="col-md-4">
                            <label class="form-label" for="shop_id">{{ _trans('Shop name') }}</label>
                            <select id="shop_id" name="shop_id"
                                    class="js-example-basic-single @error('shop_id') is-invalid @enderror">
                                <option>{{ _trans('Select shop name') }}</option>
                                @foreach($shops as $shop)
                                    <option value="{{ $shop->id }}"
                                        @selected(old('shop_id',$edit ? $driver->shop_id : null) == $shop->id)>
                                        {{ $shop->translate(locale())?->name }}</option>
                                @endforeach
                            </select>
                            @error('shop_id')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>
                    @else
                        <div id="ownerType_div" class="col-md-4">
                            <label class="form-label" for="owner_type">{{ _trans('Owner type') }}</label>
                            <select id="owner_type" name="owner_type"
                                    class="js-example-basic-single @error('owner_type') is-invalid @enderror">
                                <option>{{ _trans('Select owner type') }}</option>
                                @foreach(\App\Enums\OwnerType::cases() as $row)
                                    <option value="{{ $row->value }}"
                                        @selected(old('owner_type',$edit ? $driver->owner_type : null) == $row->value)>
                                        {{ _trans($row->name) }}</option>
                                @endforeach
                            </select>
                            @error('owner_type')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>

                        <div id="ownerId_div" class="col-md-4" style="display:none;">
                            <label class="form-label" for="owner_id">{{ _trans('Owner name') }}</label>
                            <select id="owner_id" name="owner_id"
                                    class="js-example-basic-single @error('owner_id') is-invalid @enderror">
                                <option value="">{{ _trans('Select owner name') }}</option>
                            </select>
                            @error('owner_id')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>

                        <div id="shopId_div" class="col-md-4" style="display:none;">
                            <label class="form-label" for="shop_id">{{ _trans('Shop name') }}</label>
                            <select id="shop_id" name="shop_id"
                                    class="js-example-basic-single @error('shop_id') is-invalid @enderror">
                                <option>{{ _trans('Select shop name') }}</option>
                            </select>
                            @error('shop_id')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>
                    @endif

                    <div class="col-md-4">
                        <label class="form-label" for="name">{{ _trans('Driver name') }}</label>
                        <input id="name"
                               type="text"
                               name="name"
                               class="form-control @error('name') is-invalid @enderror"
                               placeholder="{{ _trans('Driver name') }}"
                               value="{{ old('name',$edit ? $driver->name : null) }}"
                        >
                        @error('name')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="form-label" for="email">{{ _trans('E-mail') }}</label>
                        <input id="email"
                               type="email"
                               name="email"
                               class="form-control @error('email') is-invalid @enderror"
                               placeholder="{{ _trans('E-mail') }}"
                               value="{{ old('email',$edit ? $driver->email : null) }}"
                        >
                        @error('email')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="form-label" for="phone">{{ _trans('Phone') }}</label>
                        <input id="phone"
                               type="text"
                               name="phone"
                               class="form-control @error('phone') is-invalid @enderror"
                               placeholder="{{ _trans('Phone') }}"
                               value="{{ old('phone',$edit ? $driver->phone : null) }}"
                        >
                        @error('phone')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label for="notional_id" class="form-label">{{ _trans('Notional number') }}</label>
                        <input id="notional_id"
                               type="text"
                               name="notional_id"
                               class="form-control @error('notional_id') is-invalid @enderror"
                               placeholder="{{ _trans('Notional number') }}"
                               value="{{ old('notional_id',$edit ? $driver->notional_id : null) }}"
                        >
                        @error('notional_id')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    @if (!$edit)
                        <div class="col-md-4">
                            <label for="password" class="form-label">{{ _trans('Password') }}</label>
                            <input id="password"
                                   type="password"
                                   name="password"
                                   class="form-control @error('password') is-invalid @enderror"
                                   placeholder="{{ _trans('Password') }}"
                                   value="{{ old('password') }}"
                            >
                            @error('password')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>
                        <div class="col-md-4">
                            <label for="password_confirmation" class="form-label">{{ _trans('Confirm Password') }}</label>
                            <input id="password_confirmation"
                                   type="password"
                                   name="password_confirmation"
                                   class="form-control @error('password_confirmation') is-invalid @enderror"
                                   placeholder="{{ _trans('Confirm Password') }}"
                                   value="{{ old('password_confirmation') }}"
                            >
                            @error('confirm_password')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>
                    @endif
                    @if (Auth::guard('admin')->check())
                        <div class="col-md-4" id="percentage_div" style="display:none">
                            <label for="percentage" class="form-label">{{ _trans('Commission delivery') }}</label>
                            <input id="percentage"
                                   type="number"
                                   min="0"
                                   max="100"
                                   step="any"
                                   name="percentage"
                                   class="form-control @error('percentage') is-invalid @enderror"
                                   placeholder="{{ _trans('Commission delivery') }}"
                                   value="{{ old('percentage',$edit ? $driver->percentage : 0) }}"
                            >
                            @error('percentage')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>
                    @endif
                    <div class="col-md-4">
                        <label for="address" class="form-label">{{ _trans('Address') }}</label>
                        <input id="address"
                               type="text"
                               name="address"
                               class="form-control @error('address') is-invalid @enderror"
                               placeholder="{{ _trans('Address') }}"
                               value="{{ old('address',$edit ? $driver->address : null) }}"
                        >
                        @error('address')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label for="birthday" class="form-label">{{ _trans('Birthday') }}</label>
                        <input id="birthday"
                               type="date"
                               name="birthday"
                               class="form-control @error('birthday') is-invalid @enderror"
                               placeholder="{{ _trans('Birthday') }}"
                               value="{{ old('birthday',$edit ? $driver->birthday : null) }}"
                        >
                        @error('birthday')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <div class="form-group form-image">
                            <label class="font-md">{{ _trans('Avatar') }}</label>
                            <img src="{{ $edit ? getAvatar($driver->avatar) : asset('assets/images/no-image.png') }}"
                                 class="mb-2 avatar-preview">
                            <input id="avatar"
                                   type="file"
                                   name="avatar"
                                   class="form-control avatar @error('avatar') is-invalid @enderror">
                            @error('avatar')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>
                    </div>

                    <hr>

                    <h4 class="card-title">{{ _trans('License information')  }}</h4>

                    <div class="col-md-4">
                        <label for="license_number" class="form-label">{{ _trans('License number') }}</label>
                        <input id="license_number"
                               type="text"
                               name="license_number"
                               class="form-control @error('license_number') is-invalid @enderror"
                               placeholder="{{ _trans('License number') }}"
                               value="{{ old('license_number',$edit ? $driver->license->license_number : null) }}"
                        >
                        @error('license_number')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label for="license_expiry" class="form-label">{{ _trans('License expiry') }}</label>
                        <input id="license_expiry"
                               type="date"
                               name="license_expiry"
                               class="form-control @error('license_expiry') is-invalid @enderror"
                               placeholder="{{ _trans('License expiry') }}"
                               value="{{ old('license_expiry',$edit ? $driver->license->license_expiry : null) }}"
                        >
                        @error('license_expiry')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <div class="form-group form-image">
                            <label class="font-md">{{ _trans('License image') }}</label>
                            <img src="{{ $edit ? getAvatar($driver->license->license_image) : asset('assets/images/no-image.png') }}"
                                 class="mb-2 license_image-preview">
                            <input id="license_image"
                                   type="file"
                                   name="license_image"
                                   class="form-control license_image @error('license_image') is-invalid @enderror">
                            @error('license_image')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>
                    </div>

                    <hr>

                    <h4 class="card-title">{{ _trans('Vehicle information')  }}</h4>

                    <div class="col-md-4">
                        <label class="form-label" for="vehicle_type_id">{{ _trans('Vehicle type name') }}</label>
                        <select id="vehicle_type_id" name="vehicle_type_id"
                                class="js-example-basic-single @error('vehicle_type_id') is-invalid @enderror">
                            <option>{{ _trans('Vehicle type name') }}</option>
                            @foreach($vehicleTypes as $vehicleType)
                                <option value="{{ $vehicleType->id }}"
                                    @selected(old('vehicle_type_id',$edit ? $driver->vehicle->vehicle_type_id : null) == $vehicleType->id)>
                                    {{ _trans($vehicleType->translate(locale())?->name) }}</option>
                            @endforeach
                        </select>
                        @error('vehicle_type_id')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label for="vehicle_model" class="form-label">{{ _trans('vehicle model') }}</label>
                        <input id="vehicle_model"
                               type="text"
                               name="vehicle_model"
                               class="form-control @error('vehicle_model') is-invalid @enderror"
                               placeholder="{{ _trans('Vehicle model') }}"
                               value="{{ old('vehicle_model',$edit ? $driver->vehicle->vehicle_model : null) }}"
                        >
                        @error('vehicle_model')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label for="plate_number" class="form-label">{{ _trans('Plate number') }}</label>
                        <input id="plate_number"
                               type="text"
                               name="plate_number"
                               class="form-control @error('plate_number') is-invalid @enderror"
                               placeholder="{{ _trans('Plate number') }}"
                               value="{{ old('plate_number',$edit ? $driver->vehicle->plate_number : null) }}"
                        >
                        @error('plate_number')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label for="vehicle_license_expiry" class="form-label">{{ _trans('Vehicle license expiry') }}</label>
                        <input id="vehicle_license_expiry"
                               type="date"
                               name="vehicle_license_expiry"
                               class="form-control @error('vehicle_license_expiry') is-invalid @enderror"
                               placeholder="{{ _trans('Vehicle license expiry') }}"
                               value="{{ old('vehicle_license_expiry',$edit ? $driver->vehicle->vehicle_license_expiry : null) }}"
                        >
                        @error('vehicle_license_expiry')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <div class="form-group form-image">
                            <label class="font-md">{{ _trans('License vehicle image') }}</label>
                            <img src="{{ $edit ? getAvatar($driver->vehicle->vehicle_license_image) : asset('assets/images/no-image.png') }}"
                                 class="mb-2 vehicle_license_image-preview">
                            <input id="vehicle_license_image"
                                   type="file"
                                   name="vehicle_license_image"
                                   class="form-control vehicle_license_image @error('vehicle_license_image') is-invalid @enderror">
                            @error('vehicle_license_image')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group form-image">
                            <label class="font-md">{{ _trans('Vehicle image') }}</label>
                            <img src="{{ $edit ? getAvatar($driver->vehicle->vehicle_image) : asset('assets/images/no-image.png') }}"
                                 class="mb-2 vehicle_image-preview">
                            <input id="vehicle_image"
                                   type="file"
                                   name="vehicle_image"
                                   class="form-control vehicle_image @error('vehicle_image') is-invalid @enderror">
                            @error('vehicle_image')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="m-t-50 d-flex justify-content-end">
                            <button type="submit" class="btn btn-primary">{{ $edit ?_trans('Update Driver') : _trans('Save Driver') }}</button>
                        </div>
                    </div>

                </div>

                {{ Form::close() }}
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection

@include('layouts.partials.read-photo',['inputName' => 'avatar'])
@include('layouts.partials.read-photo',['inputName' => 'license_image'])
@include('layouts.partials.read-photo',['inputName' => 'vehicle_license_image'])
@include('layouts.partials.read-photo',['inputName' => 'vehicle_image'])

@if (Auth::guard('admin')->check())
    @push('scripts')
        <script>
            $(document).ready(function () {

                @if(!is_null(old('owner_type')))
                getOwners('{{ old('owner_type') }}', '{{ !is_null(old('owner_id')) ? old('owner_id') : 0 }}')
                getShops('{{ old('owner_id') }}', '{{ !is_null(old('shop_id')) ? old('shop_id') : 0 }}')
                @endif

                @if($edit)
                getOwners('{{ old('owner_type',$edit ? $driver->owner_type : null) }}', '{{ old('owner_id',$edit ? $driver->owner_id : null) }}')
                getShops('{{ old('owner_id',$edit ? $driver->owner_id : 0) }}', '{{ old('shop_id',$edit ? $driver->shop_id : 0) }}')
                @endif
            })

            $(document).on('change', '#owner_type', function () {
                $('#owner_id').empty();
                $('#owner_id').append('<option value="">{{ _trans('Select owner name') }}</option>');
                $('#shop_id').empty();
                $('#shop_id').append('<option value="">{{ _trans('Select shop name') }}</option>');
                var ownerType = $('#owner_type option:selected').val();
                if (ownerType == '' || ownerType == null) {
                    $('#percentage_div').css('display', 'none');
                    $('#percentage').val(0);
                    return false;
                } else {
                    getOwners(ownerType)
                }
            });

            function getOwners(ownerType, selectedId = null) {
                if (ownerType == 'owner') {
                    $('#ownerId_div').css('display', 'block');
                    $('#shopId_div').css('display', 'block');
                    $('#percentage_div').css('display', 'none');
                    $('#percentage').val(0);
                    $.ajax({
                        url: "{{ route('ajax.owners') }}",
                        method: 'GET',
                        cache: false,
                        success: function (response) {
                            if (response.status == true) {
                                if (response.data.length > 0) {
                                    $.each(response.data, function (index, value) {
                                        var selected = '';
                                        if (selectedId == value.id) {
                                            selected = 'selected';
                                        }
                                        $("#owner_id").append('<option ' + selected + ' value="' + value.id + '">' + value.name + '</option>');
                                    });
                                }
                            } else {
                                toastr.error(response.error)
                            }

                        },
                    });
                } else if (ownerType == 'system') {
                    $('#ownerId_div').css('display', 'none');
                    $('#shopId_div').css('display', 'none');
                    $('#percentage_div').css('display', 'block');
                } else {
                    $('#percentage_div').css('display', 'none');
                    $('#percentage').val(0);
                }
            }

            $(document).on('change', '#owner_id', function (e) {
                e.preventDefault();
                var ownerId = $('#owner_id option:selected').val();
                if (ownerId == '' || ownerId == null) {
                    $('#percentage_div').css('display', 'none');
                    $('#percentage').val(0);
                    return false;
                } else {
                    getShops(ownerId, null)
                }
            });

            function getShops(ownerId, selectedId = null) {
                $("#shop_id").empty();
                $("#shop_id").append('<option value="">{{ _trans('Select shop name') }}</option>');

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    },
                    url: "{{ route('ajax.shop-by-owner') }}",
                    method: 'POST',
                    cache: false,
                    data: {
                        owner_id: ownerId
                    },
                    success: function (response) {
                        if (response.status == true) {
                            if (response.data.length > 0) {
                                $.each(response.data, function (index, value) {
                                    var selected = '';
                                    if (selectedId == value.id) {
                                        selected = 'selected';
                                    }
                                    $("#shop_id").append('<option ' + selected + ' value="' + value.id + '">' + value.name + '</option>');

                                });
                            }

                        } else {
                            toastr.error(response.error)
                        }

                    },

                });
            }
        </script>
    @endpush
@endif
