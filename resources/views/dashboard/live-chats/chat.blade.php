<div class="answer {{ $chat->modelable_type != \App\Models\Customer::class ? 'left' : 'right' }}">
    <div class="text">{{ $chat->message }}</div>
    <div class="time">{{ $chat->created_at->format('d-m-Y') }}</div>
</div>
