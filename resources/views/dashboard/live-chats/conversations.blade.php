@foreach($conversations as $conversation)
    <a href="#" class="list-message" onclick="getChats({{$conversation->id}})"
       id="conversationId"
       data-id="{{ $conversation->id }}">
        <div class="numUnread status" id="message_not_read_{{$conversation->id}}">{{ $conversation->chats_un_seen }}</div>
        <div class="d-flex align-items-start">
            <img src="{{ getAvatar($conversation->senderable->user->avatar) }}" class="rounded-circle" alt=""/>
            <div class="flex-grow-1 ml-3">
                <h5 class="sub">{{ $conversation->senderable->user->name }} {{--({{ $conversation->senderable->phone }})--}}</h5>
                <small id="lastMessage_{{ $conversation->id }}">{{ Str::limit($conversation->lastMessage->message,10) }}</small>
            </div>
        </div>
    </a>
@endforeach
