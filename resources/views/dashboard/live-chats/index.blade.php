@extends('layouts.master')

@section('title',_trans('Live Chats'))

@section('content')
    <div class="container-fluid">
        <div class="page-title">
            <div class="row">
                <div class="col-6 col-sm-6">
                    <h3>{{ _trans('Live Chats') }}</h3>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid default-dash">

        <div class="row">
            <div class="col-6  col-lg-6">
                <label class="form-label" for="shop_id">{{ _trans('Shop') }}</label>
                <select id="shop_id"
                        name="shop_id"
                        class="js-example-basic-single">
                    <option value="">{{ _trans('Select shop name') }}</option>
                    @foreach($shops as $shop)
                        <option value="{{ $shop->id }}"  @selected($shop->id == $shopId)>{{ $shop->translate(locale())?->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="row">

            <div class="col-12 col-lg-4">
                <div class="mb-2 search_chat">
                    <a class="icon"><i class="bi bi-search"></i> </a>
                    <input type="text" id="searchChat" class="form-control" placeholder="{{ _trans('Search customer') }}"/>
                </div>
                <div class="bx_main" id="listConversations">


                </div>
            </div>

            <div class="col-12 col-lg-8">
                <div class="bx_main content_chat" id="list_messages">

                </div>
            </div>
        </div>

    </div>
    <!-- Container-fluid Ends-->
@endsection

@push('scripts')
    <script>
        $(document).ready(function (){
            let shopId = $('#shop_id option:selected').val();
            if (shopId == '' || shopId == null) {
                $('#listConversations').html('');
                $('#list_messages').html('');
                return false;
            }
            getConversations(shopId)
        })
        $(document).on('change', '#shop_id', function (e) {
            e.preventDefault();
            let shopId = $('#shop_id option:selected').val();
            if (shopId == '' || shopId == null) {
                $('#listConversations').html('');
                $('#list_messages').html('');
                return false;
            }
            getConversations(shopId)
        });

        function getConversations(shopId) {
            $.ajax({
                method: "GET",
                cache: false,
                url: '{{ route('live-chat.conversations')}}',
                data: {
                    shop_id: shopId,
                },
                success: function (response) {
                    const data = response.data;
                    if (response.status == true) {
                        $('#listConversations').html('');
                        $('#list_messages').html('');
                        $('#listConversations').append(data.view);
                    } else {
                        toastr.error(response.data)
                    }
                }
            });
        }

        function getChats(conversation_id) {
            let shopId = $('#shop_id option:selected').val();
            $.ajax({
                method: "GET",
                cache: false,
                url: '{{ route('live-chat.show')}}',
                data: {
                    conversation_id: conversation_id,
                    shop_id: shopId,
                },
                success: function (response) {
                    const data = response.data;
                    if (response.status == true) {
                        $('#list_messages').empty();
                        $('#list_messages').append(data.view);
                        $('#message_not_read_' + conversation_id).text(0);
                        $('.chat').animate({
                            scrollTop: $('.chat').get(0).scrollHeight
                        }, 2000);

                    } else {
                        toastr.error(response.error)
                    }
                }
            });
        }

        function sendMessage(event, conversation_id) {
            event.preventDefault();
            let body = $('#message_send').val();
            let shop_id = $('#shop_id option:selected').val();
            if (body == '' || body == null) {
                toastr.warning('{{ _trans('please enter message') }}', '').delay(10000);
                return false;
            }
            if (shop_id == '' || shop_id == null) {
                toastr.warning('{{ _trans('Please select Shop first') }}', '').delay(10000);
                return false;
            }
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: "POST",
                url: '{{ route('live-chat.store')}}',
                data: {
                    shop_id: shop_id,
                    conversation_id: conversation_id,
                    message: body,
                },
                success: function (response) {
                    const data = response.data;
                    if (response.status == true) {
                        $('#chat_body').append(data.view);
                        $('#message_send').val('');
                        $('.chat').animate({
                            scrollTop: $('.chat').get(0).scrollHeight
                        }, 2000);
                    } else {
                        toastr.warning(data)
                    }
                }
            });
        }


        function getListConversations(response) {
            var html = '';
            if (response.length > 0) {
                for (var count = 0; count < response.length; count++) {
                    var data = response[count];
                    html += '<a href="#" class="list-message" onclick="getMessage(' + data.conversation_id + ')" id="conversationId" data-id="' + data.conversation_id + '"> ';
                    html += '<div class="numUnread status" id="message_not_read_' + data.conversation_id + '">' + data.count_unread + '</div>';
                    html += '<div class="d-flex align-items-start">';
                    html += '<img src="' + data.customer.avatar + '" class="rounded-circle" alt=""/>';
                    html += '<div class="flex-grow-1 ml-3">';
                    html += '<h5 class="sub">' + data.customer.name + ' (' + data.order.code + ')</h5>';
                    html += '<small id="lastMessage_' + data.conversation_id + '">' + data.last_message + '</small>';
                    html += '</div>';
                    html += '</div>';
                    html += '</a>';
                }
            }
            $('#listConversations').html(html);
        }

        $('#searchChat').on('keyup', function (e) {
            let search = $(this).val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: '{{ route('owner.chat.search')}}',
                data: {
                    search: search
                },
                success: function (response) {
                    const data = response.data;
                    if (response.status == true) {
                        getListConversations(data)
                    }
                }
            });
        });
    </script>

@endpush
