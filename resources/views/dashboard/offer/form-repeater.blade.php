@if (count($data) > 0)
    <div class="repeater-default">
        <div data-repeater-list="products">
            @foreach($data as $key => $value)
                <div data-repeater-item="" style="">
                    <div class="row">
                        <div class="col-md-3">
                            <label class="form-label" for="product_id">{{ _trans('Product') }}</label>
                            <select id="product_id"
                                    name="products[{{ $key }}][product_id]"
                                    class="js-example-basic-single product_id @error('products.'.$key.'.product_id') is-invalid @enderror">
                                <option value="">{{ _trans('Select product name') }}</option>
                                @foreach($products as $product)
                                    <option value="{{ $product->id }}"
                                        @selected($value['product_id'] == $product->id)
                                    > {{ $product->translate(locale())?->name }}</option>
                                @endforeach
                            </select>
                            @error('products.'.$key.'.product_id')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror

                        </div>

                        <div class="col-md-3">
                            <label class="form-label" for="from">{{ _trans('َQuantity') }}</label>
                            <input type="number"
                                   name="products[{{ $key }}][quantity]"
                                   value="{{ $value['quantity'] }}"
                                   class="form-control @error('products.'.$key.'.quantity') is-invalid @enderror"
                                   id="from"
                            />
                            @error('products.'.$key.'.quantity')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>

                        <div class="col-md-1">
                            <div class="form-group col-sm-12 col-md-2 text-center mt-2">
                                <br>
                                <button type="button" id="deleteRepeater" class="btn btn-danger" data-repeater-delete="">
                                    {{--<i class="fas fa-trash-alt"></i>--}} {{ _trans('Delete') }}
                                </button>
                            </div>
                        </div>
                    </div>
                    <br>
                </div>
            @endforeach
        </div>
        <div class="form-group overflow-hidden">
            <div class="col-12">
                <button type="button" id="addRepeater" data-repeater-create="" class="btn btn-primary">
                    {{--<i class="far fa-plus-square"></i> --}}{{ _trans('Add') }}
                </button>
            </div>
        </div>
    </div>

@else
    <div class="repeater-default">
        <div data-repeater-list="products">
            <div data-repeater-item="" style="">
                <div class="row">
                    <div class="col-md-3">
                        <label class="form-label" for="day">{{ _trans('Product name') }}</label>
                        <select id="product_id"
                                name="product_id"
                                class="js-example-basic-single product_id">
                            <option value="">{{ _trans('Select product name') }}</option>
                        </select>

                    </div>

                    <div class="col-md-3">
                        <label class="form-label" for="from">{{ _trans('Quantity') }}</label>
                        <input type="number"
                               name="quantity"
                               id="quantity"
                               class="form-control"
                        >
                    </div>

                    <div class="col-md-1">
                        <div class="form-group col-sm-12 col-md-2 text-center mt-2">
                            <br>
                            <button type="button" id="deleteRepeater" class="btn btn-danger" data-repeater-delete="">
                                {{--<i class="fas fa-trash-alt"></i>--}} {{ _trans('Delete') }}
                            </button>
                        </div>
                    </div>
                </div>
                <br>
            </div>
        </div>
        <div class="form-group overflow-hidden">
            <div class="col-12">
                <button type="button" id="addRepeater" data-repeater-create="" class="btn btn-primary">
                    {{--<i class="far fa-plus-square"></i> --}} {{ _trans('Add') }}
                </button>
            </div>
        </div>
    </div>
@endif

@push('scripts')
    {{--@foreach($data as $key => $value)
        @include('layouts.ajax.products-details',[
                    'shop_id' => old('shop_id',$edit ? $save->shop_id : ''),
                    'selectedId' => $value['product_id'],
                    'key' => $key
                ])
    @endforeach--}}
    <script>

        var limit = 1;
        $(document).ready(function () {
            limit = {{ count($data) == 0 ? 1 : count($data)  }};
            limitRepeater();
        })
        $(document).on('click', '#addRepeater', function () {
            limit++;
            if (limit > 2) {
                $('#addRepeater').attr('disabled', true);
            }
            let shopId = $('#shop_id option:selected').val();
            getProducts(shopId, null)
            setTimeout(function () {
                $('.js-example-basic-single').select2();
            }, 100);
        })
        $(document).on('click', '#deleteRepeater', function () {
            limit--;
            limitRepeater();
        })

        function limitRepeater() {
            if (limit > 2) {
                $('#addRepeater').attr('disabled', true)
            } else {
                $('#addRepeater').attr('disabled', false)
            }
        }

    </script>
@endpush
