@extends('layouts.master')
@section('title',$edit ? _trans('Edit Offer') : _trans('Add Offer'))
@section('content')
    <div class="container-fluid">
        <div class="page-title">
            <div class="row">
                <div class="col-6 col-sm-6">
                    <h3>{{ $edit ? _trans('Edit Offer') : _trans('Add Offer') }}</h3>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid default-dash">
        <div class="row">
            <div class="row m-t-20">
                <div class="col-12 col-sm-6">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-home">
                                    <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                    <polyline points="9 22 9 12 15 12 15 22"></polyline>
                                </svg>
                            </a></li>
                        <li class="breadcrumb-item">
                            @canany(['Offer list','Offer edit','Offer delete'])
                                <a href="{{route('offer.index')}}">{{ _trans('Offers') }}</a>
                            @endcanany
                        </li>
                        <li class="breadcrumb-item active">{{ $edit ? _trans('Edit Offer') : _trans('Add Offer') }}</li>
                    </ol>
                </div>
                <div class="col-md-12">
                    @if ($edit)
                        {{ Form::open(['route' => ['offer.update',$offer->id],'method' => 'PUT','files' => true,'class' => 'form mb-15 form-submit','id' =>'kt_contact_form']) }}
                        {{ Form::hidden('id',$offer->id) }}
                    @else
                        {{ Form::open(['route' => 'offer.store','method' => 'POST','files' => true,'class' => 'form mb-15','id' =>'kt_contact_form']) }}
                    @endif

                    <div class="row g-3">
                        <div class="col-md-4">
                            <label class="form-label" for="code">{{ _trans('Offer Code') }}</label>
                            <input id="code"
                                   disabled
                                   type="text"
                                   class="form-control"
                                   placeholder="{{ _trans('Offer Code') }}"
                                   value="{{ getCodeTable('Offer','offers',$edit,$edit ? $offer->id : null) }}"
                            >
                        </div>
                        @if (request('guard')=='admin')

                        <div class="col-md-4">
                            <label class="form-label" for="owner_id">{{ _trans('Owner name') }}</label>
                            <select id="owner_id" name="owner_id"
                                    class="js-example-basic-single @error('owner_id') is-invalid @enderror">
                                <option value="">{{ _trans('Select owner name') }}</option>
                                @foreach($data as $owner)
                                    <option value="{{ $owner->id }}"
                                            @selected(old('owner_id',$edit ? $offer->owner_id : null) == $owner->id)
                                    >{{ $owner->user->name }}</option>
                                @endforeach
                            </select>
                            @error('owner_id')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="form-label" for="shop_id">{{ _trans('Shop name') }}</label>
                            <select id="shop_id"
                                    name="shop_id"
                                    class="js-example-basic-single @error('shop_id') is-invalid @enderror">
                                <option value="">{{ _trans('Select shop name') }}</option>

                            </select>
                            @error('shop_id')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>
                        @else
                            <div class="col-md-4">
                                <label class="form-label" for="shop_id">{{ _trans('Shop name') }}</label>
                                <select id="shop_id" name="shop_id"
                                        class="js-example-basic-single @error('shop_id') is-invalid @enderror">
                                    <option value="">{{ _trans('Select Shop name') }}</option>
                                    @foreach($data as $shop)
                                        <option value="{{ $shop->id }}"
                                            @selected(old('shop_id',$edit ? $offer->shop_id : null) == $shop->id)
                                        >{{ $shop->translate(locale())?->name }}</option>
                                    @endforeach
                                </select>
                                @error('shop_id')
                                <span class="text-danger">{!! $message !!} </span>
                                @enderror
                            </div>
                        @endif

                        @foreach($languages as $lang)
                            <div class="col-md-6">
                                <label for="{{ $lang['code'] }}[name]" class="form-label">{{ _trans('Offer name') }}
                                    ({{ ucfirst($lang['code']) }})</label>
                                <input id="{{ $lang['code'] }}[name]"
                                       type="text"
                                       name="{{ $lang['code'] }}[name]"
                                       class="form-control @error($lang['code'].'.name') is-invalid @enderror"
                                       placeholder="{{ _trans('Offer') }} ({{ ucfirst($lang['code']) }})"
                                       value="{{ old($lang['code'].'.name',$edit ? $offer->translate($lang['code'])?->name : null) }}"
                                >
                                @error($lang['code'].'.name')
                                <span class="text-danger">{!! $message !!} </span>
                                @enderror
                            </div>
                        @endforeach

                        <div class="col-md-4">
                            <label class="form-label" for="category_id">{{ _trans('Category name') }}</label>
                            <select id="category_id" name="category_id"
                                    class="js-example-basic-single @error('category_id') is-invalid @enderror">
                                <option value="">{{ _trans('Select category name') }}</option>
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}"
                                            @selected(old('category_id',$edit ? $offer->category_id : null) == $category->id)
                                    >{{ $category->translate(locale())?->name }}</option>
                                @endforeach
                            </select>
                            @error('category_id')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="form-label" for="price">{{ _trans('Price') }}</label>
                            <input id="price"
                                   type="number"
                                   step="any"
                                   name="price"
                                   class="form-control @error('price') is-invalid @enderror"
                                   placeholder="{{ _trans('Price') }}"
                                   value="{{ old('price',$edit ? $offer->price : null) }}"
                            >
                            @error('price')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="form-label" for="start">{{ _trans('Start') }}</label>
                            <input id="start"
                                   type="date"
                                   name="start"
                                   class="form-control @error('start') is-invalid @enderror"
                                   value="{{ old('start',$edit ? $offer->start : null) }}"
                            >
                            @error('start')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="form-label" for="end">{{ _trans('End') }}</label>
                            <input id="end"
                                   type="date"
                                   name="end"
                                   class="form-control @error('end') is-invalid @enderror"
                                   value="{{ old('end',$edit ? $offer->end : null) }}"
                            >
                            @error('end')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>

                        <div class="col-md-6">
                            <div class="mb-1">
                                <label class="form-label">{{ _trans('Icon') }}</label>
                            </div>
                            <div class="p-2 border border-dashed" style="max-width:230px;">
                                <div class="row" id="icon"></div>
                            </div>
                            @error('icon')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <div class="mb-1">
                                <label class="form-label">{{ _trans('Offer images') }}</label>
                            </div>
                            <div class="p-2 border border-dashed" style="max-width:430px;">
                                <div class="row" id="images">
                                    @if($edit)
                                        @foreach ($offer->images as $key => $photo)
                                            <div class="col-6" id="image_{{ $photo->id }}">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <img style="width: 100%" height="auto"
                                                             onerror="this.src='{{asset('admin/front-end/img/image-place-holder.png')}}'"
                                                             src="{{ getAvatar($photo->full_file) }}"
                                                             alt="Product image">
                                                        <button type="button"
                                                                class="btn btn-danger btn-block delete-img"
                                                                id="{{ $photo->id }}">
                                                            <i class="fa fa-times"></i>
                                                        </button>

                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                            @error('images','images.*')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>

                        <div class="col-md-12">
                            <h5>{{ _trans('Products') }}</h5>
                            <hr>
                            @include('dashboard.offer.form-repeater',[
     'products' => $edit ? getProducts($offer->shop_id) :  (old('shop_id') ? getProducts(old('shop_id')): []) ,
     'data' => $edit ? old('products',$offer->products) :  (!empty(old('products')) ? (old('products')) : [])
     ])
                        </div>

                    </div>

                </div>

                <div class="col-md-12">
                    <div class="m-t-50 d-flex justify-content-end">
                        <button type="submit"
                                class="btn btn-primary">{{ $edit ?_trans('Update') : _trans('Save') }}</button>
                    </div>
                </div>

                {{ Form::close() }}

            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection
@include('layouts.partials.read-photo',['inputName' => 'icon'])
@push('scripts')
    <script src="{{ asset('assets/repeater/jquery.repeater.min.js') }}"></script>
    <script src="{{ asset('assets/repeater/form-repeater.min.js') }}"></script>
    @include('layouts.ajax.shop')
    @include('layouts.ajax.category')
    @include('layouts.ajax.products-by-shop')
    <script>
        $(document).ready(function () {
            @if(!is_null(old('owner_id')))
            getShops({{ old('owner_id') }}, {{ !is_null(old('shop_id')) ? old('shop_id') : 0 }})
            @endif
            @if($edit)
            getShops({{ old('owner_id',$edit ? $offer->owner_id : 0) }}, {{ old('shop_id',$edit ? $offer->shop_id : 0) }})
            @endif
            getSubCategories('{{ old('category_id',$edit ? $offer->category_id : null) }}', '{{ old('sub_category_id',$edit ? $offer->sub_category_id : null) }}')
        })

    </script>
    @includeWhen($edit,'layouts.ajax.delete-product-images',[
        'route' => route('offer.delete-image'),
        'relation_id' => $edit ? $offer->id : null
        ])
@endpush

@push('scripts')
    <script src="{{ asset('assets/js/spartan-multi-image-picker.js') }}"></script>
    @include('layouts.partials.spartan-multi-image',[
    'single' => true,
    'file_name' => 'icon',
    'image' => $edit ? getAvatar($offer->icon) :  asset('assets/images/img/400x400/img2.jpg')
    ])

    @include('layouts.partials.spartan-multi-image',[
    'multi' => true,
    'file_name' => 'images',
    'count' => $edit ? 5 - $offer->images_count : 5,
    ])

@endpush
