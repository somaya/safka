@extends('layouts.master')
@section('title',_trans('Offers'))
@section('content')

        <div class="container-fluid">
            <div class="page-title">
                <div class="row">
                    <div class="col-6 col-sm-6">
                        <h3>{{ _trans('Offer Details') }}</h3>
                    </div>
                    <div class="col-6 text-right">

                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid default-dash">
            <div class="row">
                <div class="col-12 col-sm-6">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/') }}">
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
                            </a></li>
                        <li class="breadcrumb-item"> <a href="#">{{ _trans('Offer Details') }}</a></li>
                        <li class="breadcrumb-item active">{{ _trans('products') }}</li>
                    </ol>
                </div>

                <div class="col-xl-12 col-md-12 mb-4">

                    <div class="table-responsive custom-scrollbar p-t-30">
                        <table class="table">
                            <thead>
                            <tr>
                                <th> <span>{{ _trans('SL')}}  </span></th>
                                <th> <span>{{ _trans('Product')}} </span></th>
                                <th> <span>{{ _trans('Quantity')}} </span></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($offer->products as $index => $item)
                                <tr>
                                    <td class="text-main">#{{$index+1}}</td>
                                    <td><a href="{{ route('product.show',$item['product_id']) }}">Product#{{$item['product_id']}}</a></td>
                                    <td>{{$item['quantity']}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>

                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card card-details">
                                <h4 class="title-details">{{_trans('Offer Details')}}</h4>
                                <div class="">
                                    <p>{{_trans('Name')}} :<span>{{$offer->translate(locale())?->name}}</span></p>
                                    <p>{{_trans('Category')}} :<span>{{$offer->category->translate(locale())?->name}}</span></p>
                                    <p>{{_trans('Shop')}} :<span>{{$offer->shop->translate(locale())?->name}}</span></p>
                                    <p>{{_trans('Start')}} :<span>{{$offer->start}}</span></p>
                                    <p>{{_trans('End')}} :<span>{{$offer->end}}</span></p>
                                    <p>{{_trans('Price')}} :<span>{{$offer->price}}</span></p>

                                    <p>{{_trans('Icon')}} : <span>
                                  <img class="mb-2 image-preview" src="{{getAvatar($offer->icon)}}">
                              </span></p>

                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="card card-details">
                                <h4 class="title-details">{{ _trans('Orders')}}</h4>
                                <div class="">
                                    <p>{{ _trans('Count')}} : <span>{{$orders->count()}}</span></p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
        <!-- Container-fluid Ends-->

@endsection
