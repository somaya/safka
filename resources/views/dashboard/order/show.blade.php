@extends('layouts.master')
@section('title',_trans('Orders'))
@section('content')

    <div class="container-fluid">
        <div class="page-title">
            <div class="row">
                <div class="col-6 col-sm-6">
                    <h3>{{ _trans('Order Details') }}</h3>
                </div>
                <div class="col-6 text-right">

                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->

    <div class="container-fluid default-dash">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card card-details">
                            <h4 class="title-details">{{_trans('Order')}}</h4>
                            <div class="">
                                @if (request('guard')=='admin')
                                    <p>{{_trans('Customer')}} :<a href="/customer/show/{{$order->customer_id}}">{{$order->customer->user->name}}</a></p>
                                    <p>{{_trans('Shop')}} :<a href="/admin/shop/{{$order->shop_id}}">{{$order->shop?->translate(locale())?->name}}</a></p>
                                @else
                                    <p>{{_trans('Customer')}} :{{$order->customer->user->name}}</p>
                                    <p>{{_trans('Shop')}} :{{$order->shop?->translate(locale())?->name}}</p>
                                @endif

                                <p>{{_trans('Order Code')}} :<span>{{$order->code}}</span></p>
{{--                                <p>{{_trans('Date')}} :<span>{{$order->created_at->format('d-m-Y')}}</span></p>--}}
                                <p>{{_trans('Date')}} :<span>{{formatDate('d-m-Y h:i A',$order->created_at)}}</span></p>
                                <p>{{_trans('Delivery Price')}} :<span>{{$order->delivery_price}}</span></p>
                                <p>{{_trans('Coupon Code')}} :<span>{{$order->coupon_code}}</span></p>
                                <p>{{_trans('Coupon Price')}} :<span>{{$order->coupon_price}}</span></p>
                                <p>{{_trans('Order Price')}} :<span>{{$order->total_price}}</span></p>
                                <p>{{_trans('Order Total')}} :<span>{{$order->amount}}</span></p>
                                <p>{{_trans('Order Status')}} :<span>{{$order->order_status}}</span></p>
                                <p>{{_trans('Status Pay')}} :<span>{{$order->status_pay}}</span></p>
                                <p>{{_trans('Payment Method')}} :<span>{{$order->payment_method}}</span></p>
                                <p>{{_trans('Special order')}} :<span>{{$order->special_order}}</span></p>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-8">
                        <div class="card card-details">
                            <h4 class="title-details">{{_trans('Order Details')}}</h4>
                            <div class="col-md-12">
                                <div class="row">
                                    @foreach($order->orderDetails as $item)
                                        <div class="col-md-4">
                                            <div class="card card-details">
                                                @if($item->modelable_type == 'App\Models\Product')
                                                    <div class="">
                                                        <p>{{ _trans('Product')}} : <a href="/product/{{$item->product->id}}">{{$item->product->translate(locale())?->name}}</a></p>
                                                        <p>{{ _trans('Quantity')}} : <span>{{$item->qty}}</span></p>
                                                        <p>{{ _trans('Price')}} : <span>{{$item->price}}</span></p>
                                                        <p>{{ _trans('Image')}} : <span><img src="{{ getAvatar($item->product?->icon) }}" class="mb-2 image-preview"></span></p>
                                                        @if(!empty($item->size_id))
                                                            <p>{{ _trans('Size')}} : <span>{{$item->size?->translate(locale())?->name}}</span></p>
                                                        @endif
                                                    </div>
                                                @endif
                                                @if($item->modelable_type=='App\Models\Offer')
                                                    <div class="">
                                                        <p>{{ _trans('Offer')}} : <a href="/offer/{{$item->offer->id}}">{{$item->offer?->translate(locale())?->name}}</a></p>
                                                        <p>{{ _trans('Quantity')}} : <span>{{$item->qty}}</span></p>
                                                        <p>{{ _trans('Price')}} : <span>{{$item->price}}</span></p>
                                                        <p>{{ _trans('Image')}} : <span><img src="{{ getAvatar($item->offer?->icon) }}" class="mb-2 image-preview"></span></p>
                                                    </div>
                                                @endif
                                                @if($item->modelable_type=='App\Models\Save')
                                                    <div class="">
                                                        <p>{{ _trans('Save')}} : <a href="/save/{{$item->saveProduct->id}}">{{$item->saveProduct?->translate(locale())?->name}}</a></p>
                                                        <p>{{ _trans('Quantity')}} : <span>{{$item->qty}}</span></p>
                                                        <p>{{ _trans('Price')}} : <span>{{$item->price}}</span></p>
                                                        <p>{{ _trans('Image')}} : <span><img src="{{ getAvatar($item->saveProduct?->icon) }}" class="mb-2 image-preview"></span></p>
                                                    </div>
                                                @endif
                                                @if($item->modelable_type=='App\Models\Discount')
                                                    <div class="">
                                                        <p>{{ _trans('Discount')}} : <a href="/discount/{{$item->discount->id}}">{{$item->discount?->product?->translate(locale())?->name}}</a></p>
                                                        <p>{{ _trans('Quantity')}} : <span>{{$item->qty}}</span></p>
                                                        <p>{{ _trans('Price')}} : <span>{{$item->price}}</span></p>
                                                        <p>{{ _trans('Image')}} : <span><img src="{{ getAvatar($item->discount?->product?->icon) }}" class="mb-2 image-preview"></span></p>
                                                    </div>
                                                @endif
                                                @if($item->modelable_type=='App\Models\Extra')
                                                    <div class="">
                                                        <p>{{ _trans('Extra')}} : <a href="/extra/{{$item->extra->id}}/edit">{{$item->extra?->translate(locale())?->name}}</a></p>
                                                        <p>{{ _trans('Quantity')}} : <span>{{$item->qty}}</span></p>
                                                        <p>{{ _trans('Price')}} : <span>{{$item->price}}</span></p>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    @endforeach
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card card-details">
                                <h4 class="title-details">{{_trans('customer Details')}}</h4>
                                <div class="">
                                    <p>{{_trans('name')}} :<span>{{$order->customer->user->name}}</span></p>
                                    <p>{{_trans('Facility Name')}} :<span>{{$order->customer->facility_name}}</span></p>
                                    <p>{{_trans('Facility Type')}} :<span>{{$order->customer->branchType?->translate(locale())?->name}}</span></p>
                                    <p>{{_trans('Email')}} :<span>{{$order->customer->user->email}}</span></p>
                                    <p>{{_trans('Phone')}} :<span>{{$order->customer->phone}}</span></p>
                                    <p>{{_trans('Governorate')}} :<span>{{$order->customer->user->governorate?->translate(locale())?->name}}</span></p>
                                    <p>{{_trans('Region')}} :<span>{{$order->customer->user->region?->translate(locale())?->name}}</span></p>
                                    <p>{{_trans('Address')}} :<span>{{$order->customer->address}}</span></p>
                                    <p>{{_trans('Birthdate')}} :<span>{{$order->customer->birthday}}</span></p>
                                    <p>{{_trans('Gender')}} :<span>{{$order->customer->gender}}</span></p>


                                </div>
                            </div>
                        </div>
                    </div>

                    @if ($order->order_status == \App\Enums\OrderStatus::canceled->value)
                        <div class="col-md-6">
                            <div class="card card-details">
                                <h4 class="title-details">{{ _trans('Reason Cancelled') }}</h4>
                                <div class="">
                                    <p>{{ $order->note }}</p>
                                </div>
                            </div>
                        </div>
                    @endif

                    @if ($order->order_status == \App\Enums\OrderStatus::returned->value)
                        <div class="col-md-6">
                            <div class="card card-details">
                                <h4 class="title-details">{{ _trans('Returned message') }}</h4>
                                <div class="">
                                    <p>{{ $order->note }}</p>
                                </div>
                            </div>
                        </div>
                    @endif

                </div>
            </div>



            <div class="col-xl-12 col-md-12 ">
                <div class="card m-b-30">
                    <div class="card-body">
                        <h5>{{ _trans('Status Order') }}</h5>
                        <h5>{{ _trans('If you want to change status order') }}</h5>
                        <form action="{{ route('order.update-status',$order->id) }}" method="GET">

                            @if (!in_array($order->order_status,[\App\Enums\OrderStatus::completed->value,\App\Enums\OrderStatus::canceled->value,\App\Enums\OrderStatus::returned->value]))
                                <ul class="timeline">
                                    @foreach(\App\Enums\OrderStatus::cases() as $value)
                                        <li @class(['active' => $order->order_status == $value->value])>
                                            <div class="timeline-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" class="bi bi-clock-history" viewBox="0 0 16 16">
                                                    <path
                                                        d="M8.515 1.019A7 7 0 0 0 8 1V0a8 8 0 0 1 .589.022l-.074.997zm2.004.45a7.003 7.003 0 0 0-.985-.299l.219-.976c.383.086.76.2 1.126.342l-.36.933zm1.37.71a7.01 7.01 0 0 0-.439-.27l.493-.87a8.025 8.025 0 0 1 .979.654l-.615.789a6.996 6.996 0 0 0-.418-.302zm1.834 1.79a6.99 6.99 0 0 0-.653-.796l.724-.69c.27.285.52.59.747.91l-.818.576zm.744 1.352a7.08 7.08 0 0 0-.214-.468l.893-.45a7.976 7.976 0 0 1 .45 1.088l-.95.313a7.023 7.023 0 0 0-.179-.483zm.53 2.507a6.991 6.991 0 0 0-.1-1.025l.985-.17c.067.386.106.778.116 1.17l-1 .025zm-.131 1.538c.033-.17.06-.339.081-.51l.993.123a7.957 7.957 0 0 1-.23 1.155l-.964-.267c.046-.165.086-.332.12-.501zm-.952 2.379c.184-.29.346-.594.486-.908l.914.405c-.16.36-.345.706-.555 1.038l-.845-.535zm-.964 1.205c.122-.122.239-.248.35-.378l.758.653a8.073 8.073 0 0 1-.401.432l-.707-.707z"/>
                                                    <path d="M8 1a7 7 0 1 0 4.95 11.95l.707.707A8.001 8.001 0 1 1 8 0v1z"/>
                                                    <path d="M7.5 3a.5.5 0 0 1 .5.5v5.21l3.248 1.856a.5.5 0 0 1-.496.868l-3.5-2A.5.5 0 0 1 7 9V3.5a.5.5 0 0 1 .5-.5z"/>
                                                </svg>
                                            </div>
                                            <p>{{ _trans($value->name) }}</p>
                                        </li>
                                    @endforeach
                                </ul>
                                <div class="clearfix"></div>
                                <br>
                                <br>
                            @endif
                            <ul class="timeline">
                                @foreach($order->trackingOrder as $key => $trackingOrder)
                                    <li @class(['active' => $order->order_status == $trackingOrder->order_status])>
                                        <div class="timeline-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" class="bi bi-clock-history" viewBox="0 0 16 16">
                                                <path
                                                    d="M8.515 1.019A7 7 0 0 0 8 1V0a8 8 0 0 1 .589.022l-.074.997zm2.004.45a7.003 7.003 0 0 0-.985-.299l.219-.976c.383.086.76.2 1.126.342l-.36.933zm1.37.71a7.01 7.01 0 0 0-.439-.27l.493-.87a8.025 8.025 0 0 1 .979.654l-.615.789a6.996 6.996 0 0 0-.418-.302zm1.834 1.79a6.99 6.99 0 0 0-.653-.796l.724-.69c.27.285.52.59.747.91l-.818.576zm.744 1.352a7.08 7.08 0 0 0-.214-.468l.893-.45a7.976 7.976 0 0 1 .45 1.088l-.95.313a7.023 7.023 0 0 0-.179-.483zm.53 2.507a6.991 6.991 0 0 0-.1-1.025l.985-.17c.067.386.106.778.116 1.17l-1 .025zm-.131 1.538c.033-.17.06-.339.081-.51l.993.123a7.957 7.957 0 0 1-.23 1.155l-.964-.267c.046-.165.086-.332.12-.501zm-.952 2.379c.184-.29.346-.594.486-.908l.914.405c-.16.36-.345.706-.555 1.038l-.845-.535zm-.964 1.205c.122-.122.239-.248.35-.378l.758.653a8.073 8.073 0 0 1-.401.432l-.707-.707z"/>
                                                <path d="M8 1a7 7 0 1 0 4.95 11.95l.707.707A8.001 8.001 0 1 1 8 0v1z"/>
                                                <path d="M7.5 3a.5.5 0 0 1 .5.5v5.21l3.248 1.856a.5.5 0 0 1-.496.868l-3.5-2A.5.5 0 0 1 7 9V3.5a.5.5 0 0 1 .5-.5z"/>
                                            </svg>
                                        </div>
                                        @if(!$loop->last)
                                            <small class="timer">{{ $trackingOrder->diffHumans }}</small>
                                        @endif
                                        <p>{{ _trans($trackingOrder->order_status) }}</p>
                                    </li>
                                @endforeach

                            </ul>

                            <div class="col-md-12">
                                <div class="m-t-50 d-flex justify-content-start">
                                    <h6>{{ _trans('Total Time').' => '. totalTimeTrip($order->trackingOrderHasOne($order->id,'one')->start,$order->trackingOrderHasOne($order->id)->start)  }} </h6>
                                </div>
                            </div>

                            @if (in_array($order->order_status,['pending','receive']))
                                <div class="col-md-12">

                                    <div class="m-t-50 d-flex justify-content-start">
                                        <div class="modal-footer">

                                                @canany(['Order list','Order add','Order edit','Order delete'])
                                                    <button type="submit" class="btn btn-primary text-right">{{ _trans('Next Status order') }}</button>
                                                @endcanany
                                            @canany(['Order list','Order add','Order edit','Order delete'])
                                                <a data-bs-toggle="modal" data-bs-target="#cancelledOrder{{ $order->id }}"
                                                   class="btn btn-danger text-left">{{ _trans('Cancelled Order') }}</a>

                                                <a data-bs-toggle="modal" data-bs-target="#returnedOrder{{ $order->id }}"
                                                   class="btn btn-warning text-left">{{ _trans('Returned order') }}</a>
                                            @endcanany
                                        </div>
                                    </div>
                                </div>
                            @endif

                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- Container-fluid Ends-->

    <!-- Model forget password -->
    <div class="modal fade" id="cancelledOrder{{$order->id}}" tabindex="-1" role="dialog" aria-labelledby="cancelledOrder{{$order->id}}" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">{{ _trans('Cancelled order code') }} {{ $order->code }} </h3>
                    <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="{{ route('order.cancelled',$order->id) }}" method="POST">
                    {{ Form::hidden('order_id',$order->id) }}
                    @csrf
                    <div class="modal-body theme-form login-form p-0">
                        <div class="form-group">
                            <label class="mb-2">{{ _trans('Reason Cancelled') }}</label>
                            <textarea class="form-control @error('reason_message') @enderror"
                                      rows="3"
                                      cols="3"
                                      name="reason_message">{{ old('reason_message') }}</textarea>
                            @error('reason_message')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" type="submit">{{ _trans('Cancelled') }}</button>
                        <button class="btn btn-secondary" type="button" data-bs-dismiss="modal" aria-label="Close">{{ _trans('Close') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="returnedOrder{{$order->id}}" tabindex="-1" role="dialog" aria-labelledby="returnedOrder{{$order->id}}" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">{{ _trans('returned order code') }} {{ $order->code }} </h3>
                    <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="{{ route('order.returned',$order->id) }}" method="POST">
                    {{ Form::hidden('order_id',$order->id) }}
                    @csrf
                    <div class="modal-body theme-form login-form p-0">
                        <div class="form-group">
                            <label class="mb-2" for="returned_message">{{ _trans('Reason returned') }}</label>
                            <textarea id="returned_message" class="form-control @error('returned_message') @enderror"
                                      rows="3"
                                      cols="3"
                                      name="returned_message">{{ old('returned_message') }}</textarea>
                            @error('returned_message')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" type="submit">{{ _trans('Returned') }}</button>
                        <button class="btn btn-secondary" type="button" data-bs-dismiss="modal" aria-label="Close">{{ _trans('Close') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {

            @error('reason_message')
            $("#cancelledOrder{{$order->id}}").modal('show');
            @enderror
            @error('returned_message')
            $("#returnedOrder{{$order->id}}").modal('show');
            @enderror

        });
    </script>
@endpush
