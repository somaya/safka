<div class="header-wrapper row m-0">
    <div class="header-logo-wrapper col-2 p-0">
        <div class="logo-wrapper"><a href="{{ route('owner.dashboard') }}">
                <img src="{{ Utility::getValByName('web_logo') }}"></a>
        </div>
        <div class="toggle-sidebar">
            <div class="status_toggle sidebar-toggle d-flex">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <g>
                        <g>
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                  d="M21.0003 6.6738C21.0003 8.7024 19.3551 10.3476 17.3265 10.3476C15.2979 10.3476 13.6536 8.7024 13.6536 6.6738C13.6536 4.6452 15.2979 3 17.3265 3C19.3551 3 21.0003 4.6452 21.0003 6.6738Z"
                                  stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                  d="M10.3467 6.6738C10.3467 8.7024 8.7024 10.3476 6.6729 10.3476C4.6452 10.3476 3 8.7024 3 6.6738C3 4.6452 4.6452 3 6.6729 3C8.7024 3 10.3467 4.6452 10.3467 6.6738Z"
                                  stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                  d="M21.0003 17.2619C21.0003 19.2905 19.3551 20.9348 17.3265 20.9348C15.2979 20.9348 13.6536 19.2905 13.6536 17.2619C13.6536 15.2333 15.2979 13.5881 17.3265 13.5881C19.3551 13.5881 21.0003 15.2333 21.0003 17.2619Z"
                                  stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                  d="M10.3467 17.2619C10.3467 19.2905 8.7024 20.9348 6.6729 20.9348C4.6452 20.9348 3 19.2905 3 17.2619C3 15.2333 4.6452 13.5881 6.6729 13.5881C8.7024 13.5881 10.3467 15.2333 10.3467 17.2619Z"
                                  stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                        </g>
                    </g>
                </svg>
            </div>
        </div>
    </div>

    <div class="nav-right col-10 pull-right right-header p-0">
        <ul class="nav-menus">
            {{-- is components --}}
            <x-language guard="web"/>

            @canany(['Notification list','Notification add'])
                @php
                    $notifications = getNotificationOwners(['order','account_order','message','invoice','live-chat']);
                @endphp
                <li class="onhover-dropdown">
                    <div class="notification-box">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g>
                                <g>
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                          d="M11.9961 2.51416C7.56185 2.51416 5.63519 6.5294 5.63519 9.18368C5.63519 11.1675 5.92281 10.5837 4.82471 13.0037C3.48376 16.4523 8.87614 17.8618 11.9961 17.8618C15.1152 17.8618 20.5076 16.4523 19.1676 13.0037C18.0695 10.5837 18.3571 11.1675 18.3571 9.18368C18.3571 6.5294 16.4295 2.51416 11.9961 2.51416Z"
                                          stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                    <path d="M14.306 20.5122C13.0117 21.9579 10.9927 21.9751 9.68604 20.5122" stroke="#130F26"
                                          stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                </g>
                            </g>
                        </svg>
                        <span class="badge rounded-pill badge-warning" id="notification_owner">
                    {{ $notifications->count() }}
                    </span>
                    </div>
                    <div class="onhover-show-div notification-dropdown">
                        <div class="dropdown-title">
                            <h3>{{ _trans('Notifications') }}</h3><a class="f-right" href="#"> <i data-feather="bell"> </i></a>
                        </div>
                        <ul class="custom-scrollbar" id="notification_list_owner">
                            @foreach($notifications as $row)
                                <li id="notificationId_{{ $row->id }}">
                                    <div class="media">
                                        <div class="notification-img bg-light-success">
                                            <img src="{{ getAvatar($row->customer->user->avatar) }}" alt="" style="height: 20px;width: 20px;">
                                        </div>
                                        <div class="media-body">
                                            <h5>
                                                @if ($row->type == 'order' || $row->type == 'account_order')
                                                    <a class="f-14 m-0" href="{{ route('order.show',[$row->data['order_id'],$row->id]) }}">
                                                        {{ $row->customer->user->name }}
                                                    </a>
                                                @elseif ($row->type =='message')
                                                    <a class="f-14 m-0">
                                                        {{ _trans('Message from ').$row->customer->user->name }}
                                                    </a>
                                                @elseif($row->type =='live-chat')

                                                    <a href="{{ route('live-chat.index',[$row->data['shop_id'],$row->data['conversion_id']]) }}" class="f-14 m-0">
                                                        {{ _trans('Message live chat').' '.$row->customer?->user?->name }}
                                                    </a>
                                                @endif
                                            </h5>
                                            @if ($row->type == 'order' || $row->type == 'account_order')
                                                <p>{{ _trans('request order new') }}</p><span>{{ $row->created_at->diffForHumans() }}</span>
                                            @elseif ($row->type =='message')
                                                <p>{{ _trans('new message') }}</p><span>{{ $row->created_at->diffForHumans() }}</span>
                                            @elseif($row->type =='live-chat')
                                                <p>{{ _trans('new live chat') }}</p><span>{{ $row->created_at->diffForHumans() }}</span>
                                            @endif
                                        </div>
{{--                                        <div class="notification-right">--}}
{{--                                            <a href="#" onclick="deleteNotification(event,{{$row->id}})" id="deleteNotification">--}}
{{--                                                <i data-feather="x"></i>--}}
{{--                                            </a>--}}
{{--                                        </div>--}}
                                    </div>
                                </li>
                            @endforeach
                        </ul>

                        <a class="btn btn-primary btn-check-all" href="{{ route('owner.notification.index') }}">{{ _trans('Check all') }}</a>
                    </div>
                </li>
            @endcanany

            <li class="profile-nav onhover-dropdown">
                <div class="media profile-media">
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <g>
                            <g>
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                      d="M9.55851 21.4562C5.88651 21.4562 2.74951 20.9012 2.74951 18.6772C2.74951 16.4532 5.86651 14.4492 9.55851 14.4492C13.2305 14.4492 16.3665 16.4342 16.3665 18.6572C16.3665 20.8802 13.2505 21.4562 9.55851 21.4562Z"
                                      stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                      d="M9.55849 11.2776C11.9685 11.2776 13.9225 9.32356 13.9225 6.91356C13.9225 4.50356 11.9685 2.54956 9.55849 2.54956C7.14849 2.54956 5.19449 4.50356 5.19449 6.91356C5.18549 9.31556 7.12649 11.2696 9.52749 11.2776H9.55849Z"
                                      stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                <path
                                    d="M16.8013 10.0789C18.2043 9.70388 19.2383 8.42488 19.2383 6.90288C19.2393 5.31488 18.1123 3.98888 16.6143 3.68188"
                                    stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                <path
                                    d="M17.4608 13.6536C19.4488 13.6536 21.1468 15.0016 21.1468 16.2046C21.1468 16.9136 20.5618 17.6416 19.6718 17.8506"
                                    stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                            </g>
                        </g>
                    </svg>
                </div>
                <ul class="profile-dropdown onhover-show-div">
                    <li><a href="{{ route('owner.profile.account') }}"><i data-feather="user"></i><span>{{ _trans('My account') }} </span></a></li>

                    @canany(['Chat list','Chat add','Chat edit','Chat delete'])
                    <li><a href="{{ route('owner.chat.messages') }}"><i data-feather="mail"></i><span>{{ _trans('Messages') }}</span></a></li>
                    @endcanany

                    @canany(['Order list','Order add','Order edit','Order delete'])
                    <li><a href="{{ route('order.index') }}"><i data-feather="file-text"></i><span>{{ _trans('Orders') }}</span></a></li>
                    @endcanany
                    <li><a href="{{ route('owner.logout') }}"><i data-feather="log-in"> </i><span>{{ _trans('Logout') }}</span></a></li>
                </ul>
            </li>
        </ul>
    </div>

</div>
