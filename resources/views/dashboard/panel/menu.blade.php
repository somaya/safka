<div>
    <div class="logo-wrapper"><a href="{{ route('owner.dashboard') }}">
            <img src="{{ getAvatar(Utility::getValByName('web_logo') ) }}">
        </a>
        <div class="back-btn"><i class="fa fa-angle-left"></i></div>
    </div>
    <div class="logo-icon-wrapper"><a href="{{ route('owner.dashboard') }}">{{ Utility::getValByName('company_name_'.locale()) }}</a></div>
    <nav class="sidebar-main">
        <div class="left-arrow" id="left-arrow"><i data-feather="arrow-left"></i></div>
        <div id="sidebar-menu">
            <ul class="sidebar-links" id="simple-bar">
                <li class="back-btn">
                    <a href="{{ route('owner.dashboard') }}">
                        <img class="img-fluid" src="{{ getAvatar(Utility::getValByName('web_logo') ) }}" alt=""></a>
                    <div class="mobile-back text-end">
                        <span>{{ _trans('Back') }}</span>
                        <i class="fa fa-angle-right ps-2" aria-hidden="true">
                        </i>
                    </div>
                </li>

                <li class="sidebar-list {{ menuRoute('owner.dashboard') }}">
                    <a class="sidebar-link sidebar-title link-nav" href="{{ route('owner.dashboard') }}">
                        <div class="curve1"></div>
                        <div class="curve2"></div>
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g>
                                <g>
                                    <path d="M15.596 15.6963H8.37598" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                    <path d="M15.596 11.9365H8.37598" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                    <path d="M11.1312 8.17725H8.37622" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                          d="M3.61011 12C3.61011 18.937 5.70811 21.25 12.0011 21.25C18.2951 21.25 20.3921 18.937 20.3921 12C20.3921 5.063 18.2951 2.75 12.0011 2.75C5.70811 2.75 3.61011 5.063 3.61011 12Z" stroke="#130F26"
                                          stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                </g>
                            </g>
                        </svg>
                        <span>{{ _trans('Accounts') }}</span>
                    </a>
                </li>
                @canany(['Category list','Category add','Category edit','Category delete',
                            'Product list','Product add','Product edit','Product delete',
                            'Offer list','Offer add','Offer edit','Offer delete',
                            'Save list','Save add','Save edit','Save delete',
                            'Discount list','Discount add','Discount edit','Discount delete',
                        ])
                    <li class="sidebar-list ">
                        <a class="sidebar-link sidebar-title" href="#">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g>
                                    <g>
                                        <path d="M9.07861 16.1355H14.8936" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M2.3999 13.713C2.3999 8.082 3.0139 8.475 6.3189 5.41C7.7649 4.246 10.0149 2 11.9579 2C13.8999 2 16.1949 4.235 17.6539 5.41C20.9589 8.475 21.5719 8.082 21.5719 13.713C21.5719 22 19.6129 22 11.9859 22C4.3589 22 2.3999 22 2.3999 13.713Z"
                                              stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                    </g>
                                </g>
                            </svg>
                            <span>{{_trans('Menus And Offers')}}</span></a>
                        <ul class="sidebar-submenu">
                            @canany(['Product list','Product add','Product edit','Product delete'])
                                <li class="{{menuRoute('product*','li') }}">
                                    <a href="{{ route('product.index') }}">{{_trans('Products')}}</a></li>
                            @endcanany

                            @canany(['Offer list','Offer add','Offer edit','Offer delete'])
                                <li class="{{ menuRoute('offer*','li') }}">
                                    <a href="{{ route('offer.index') }}">{{_trans('Offers')}}</a></li>
                            @endcanany
                            @canany(['Save list','Save add','Save edit','Save delete'])
                                <li class="{{ menuRoute('save*','li') }}">
                                    <a href="{{ route('save.index') }}">{{_trans('Saves')}}</a></li>
                            @endcanany
                            @canany(['Discount list','Discount add','Discount edit','Discount delete'])
                                <li class="{{ menuRoute('discount*','li') }}">
                                    <a href="{{ route('discount.index') }}">{{_trans('Discounts')}}</a></li>
                            @endcanany
                        </ul>
                    </li>
                @endcanany

                @canany(['Order list','Order add','Order edit','Order delete'])
                    <li class="sidebar-list {{ menuRoute('order*','li') }}">
                        <a class="sidebar-link sidebar-title link-nav" href="{{ route('order.index') }}">
                            <div class="curve1"></div>
                            <div class="curve2"></div>
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g>
                                    <g>
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M2.75024 12C2.75024 5.063 5.06324 2.75 12.0002 2.75C18.9372 2.75 21.2502 5.063 21.2502 12C21.2502 18.937 18.9372 21.25 12.0002 21.25C5.06324 21.25 2.75024 18.937 2.75024 12Z" stroke="#130F26"
                                              stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path d="M15.2045 13.8999H15.2135" stroke="#130F26" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path d="M12.2045 9.8999H12.2135" stroke="#130F26" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path d="M9.19557 13.8999H9.20457" stroke="#130F26" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                                    </g>
                                </g>
                            </svg>
                            <span>{{_trans('Orders')}}</span>
                        </a>
                    </li>
                @endcanany


                @canany(['Chat list','Chat add','Chat edit','Chat delete'])
                    <li class="sidebar-list {{ menuRoute('owner/chat*','li') }}">
                        <a class="sidebar-link sidebar-title link-nav" href="{{ route('owner.chat.index') }}">
                            <div class="curve1"></div>
                            <div class="curve2"></div>
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g>
                                    <g>
                                        <path d="M15.596 15.6963H8.37598" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path d="M15.596 11.9365H8.37598" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path d="M11.1312 8.17725H8.37622" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M3.61011 12C3.61011 18.937 5.70811 21.25 12.0011 21.25C18.2951 21.25 20.3921 18.937 20.3921 12C20.3921 5.063 18.2951 2.75 12.0011 2.75C5.70811 2.75 3.61011 5.063 3.61011 12Z" stroke="#130F26"
                                              stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                    </g>
                                </g>
                            </svg>
                            <span>{{ _trans('Chats') }}</span>
                        </a>
                    </li>
                @endcanany
                @canany(['Live#Chat list'])
                    <li class="sidebar-list {{ menuRoute('owner/live-chat*','li') }}">
                        <a class="sidebar-link sidebar-title link-nav" href="{{ route('live-chat.index') }}">
                            <div class="curve1"></div>
                            <div class="curve2"></div>
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g>
                                    <g>
                                        <path d="M15.596 15.6963H8.37598" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path d="M15.596 11.9365H8.37598" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path d="M11.1312 8.17725H8.37622" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M3.61011 12C3.61011 18.937 5.70811 21.25 12.0011 21.25C18.2951 21.25 20.3921 18.937 20.3921 12C20.3921 5.063 18.2951 2.75 12.0011 2.75C5.70811 2.75 3.61011 5.063 3.61011 12Z" stroke="#130F26"
                                              stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                    </g>
                                </g>
                            </svg>
                            <span>{{ _trans('Live Chats') }}</span>
                        </a>
                    </li>
                @endcanany

                @canany(['Role#Staff list','Role#Staff add','Role#Staff edit','Role#Staff delete'])
                    <li class="sidebar-list {{ menuRoute('role*','li') }}">
                        <a class="sidebar-link sidebar-title link-nav" href="{{ route('role.index') }}">
                            <div class="curve1"></div>
                            <div class="curve2"></div>
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g>
                                    <g>
                                        <path d="M15.596 15.6963H8.37598" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path d="M15.596 11.9365H8.37598" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path d="M11.1312 8.17725H8.37622" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M3.61011 12C3.61011 18.937 5.70811 21.25 12.0011 21.25C18.2951 21.25 20.3921 18.937 20.3921 12C20.3921 5.063 18.2951 2.75 12.0011 2.75C5.70811 2.75 3.61011 5.063 3.61011 12Z" stroke="#130F26"
                                              stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                    </g>
                                </g>
                            </svg>
                            <span>{{ _trans('Role Staff') }}</span>
                        </a>
                    </li>
                @endcanany

                @canany(['Staff list','Staff add','Staff edit','Staff delete'])
                    <li class="sidebar-list {{ menuRoute('staff*','li') }}">
                        <a class="sidebar-link sidebar-title link-nav" href="{{ route('staff.index') }}">
                            <div class="curve1"></div>
                            <div class="curve2"></div>
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g>
                                    <g>
                                        <path d="M15.596 15.6963H8.37598" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path d="M15.596 11.9365H8.37598" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path d="M11.1312 8.17725H8.37622" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M3.61011 12C3.61011 18.937 5.70811 21.25 12.0011 21.25C18.2951 21.25 20.3921 18.937 20.3921 12C20.3921 5.063 18.2951 2.75 12.0011 2.75C5.70811 2.75 3.61011 5.063 3.61011 12Z" stroke="#130F26"
                                              stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                    </g>
                                </g>
                            </svg>
                            <span>{{ _trans('Staff') }}</span>
                        </a>
                    </li>
                @endcanany

{{--                @canany(['Delivery list','Delivery add','Delivery edit','Delivery delete',--}}
{{--                        'Driver list','Driver add','Driver edit','Driver delete',--}}
{{--                        'DriverOrder list','DriverOrder add','DriverOrder edit','DriverOrder delete'])--}}
{{--                    <li class="sidebar-list --}}{{--{{ menuRoute('driver*','li') }} {{ menuRoute('delivery*','li') }}--}}{{--">--}}
{{--                        <a class="sidebar-link sidebar-title" href="#">--}}
{{--                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">--}}
{{--                                <g>--}}
{{--                                    <g>--}}
{{--                                        <path d="M9.07861 16.1355H14.8936" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>--}}
{{--                                        <path fill-rule="evenodd" clip-rule="evenodd"--}}
{{--                                              d="M2.3999 13.713C2.3999 8.082 3.0139 8.475 6.3189 5.41C7.7649 4.246 10.0149 2 11.9579 2C13.8999 2 16.1949 4.235 17.6539 5.41C20.9589 8.475 21.5719 8.082 21.5719 13.713C21.5719 22 19.6129 22 11.9859 22C4.3589 22 2.3999 22 2.3999 13.713Z"--}}
{{--                                              stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>--}}
{{--                                    </g>--}}
{{--                                </g>--}}
{{--                            </svg>--}}
{{--                            <span>{{_trans('Delivery')}}</span></a>--}}
{{--                        <ul class="sidebar-submenu">--}}
{{--                            @canany(['Delivery list','Delivery add','Delivery edit','Delivery delete'])--}}
{{--                                <li class="{{ menuRoute('delivery-area*','li') }}">--}}
{{--                                    <a href="{{ route('delivery-area.index') }}">{{_trans('Delivery Areas')}}</a>--}}
{{--                                </li>--}}
{{--                            @endcanany--}}

{{--                            @canany(['Driver list','Driver add','Driver edit','Driver delete'])--}}
{{--                                <li class="{{ menuRoute('vehicle-type*','li') }}">--}}
{{--                                    <a href="{{ route('driver.index') }}">{{_trans('Drivers')}}</a>--}}
{{--                                </li>--}}
{{--                            @endcanany--}}

{{--                            @canany(['DriverOrder list','DriverOrder add','DriverOrder edit','DriverOrder delete'])--}}
{{--                                <li class="{{ menuRoute('driver-order*','li') }}">--}}
{{--                                    <a href="{{ route('driver-order.index') }}">{{_trans('Delivery Order')}}</a>--}}
{{--                                </li>--}}
{{--                            @endcanany--}}

{{--                        </ul>--}}
{{--                    </li>--}}
{{--                @endcanany--}}
{{--                @canany(['Chart list','Chart add','Chart edit','Chart delete'])--}}
{{--                    <li class="sidebar-list ">--}}
{{--                        <a class="sidebar-link sidebar-title" href="#">--}}
{{--                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">--}}
{{--                                <g>--}}
{{--                                    <g>--}}
{{--                                        <path d="M9.07861 16.1355H14.8936" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>--}}
{{--                                        <path fill-rule="evenodd" clip-rule="evenodd"--}}
{{--                                              d="M2.3999 13.713C2.3999 8.082 3.0139 8.475 6.3189 5.41C7.7649 4.246 10.0149 2 11.9579 2C13.8999 2 16.1949 4.235 17.6539 5.41C20.9589 8.475 21.5719 8.082 21.5719 13.713C21.5719 22 19.6129 22 11.9859 22C4.3589 22 2.3999 22 2.3999 13.713Z"--}}
{{--                                              stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>--}}
{{--                                    </g>--}}
{{--                                </g>--}}
{{--                            </svg>--}}
{{--                            <span>{{_trans('Charts')}}</span></a>--}}
{{--                        <ul class="sidebar-submenu">--}}


{{--                            <li class="{{ menuRoute('reports/restaurant-orders-chart','li') }}">--}}
{{--                                <a href="{{ route('reports.restaurant-orders-chart') }}">{{ _trans('Restaurant By Orders Chart')  }}</a>--}}
{{--                            </li>--}}
{{--                            <li class="{{ menuRoute('reports/customer-order-chart','li')  }}">--}}
{{--                                <a href="{{ route('reports.customer-order-chart') }}">{{_trans('Customer By order Chart')}}</a>--}}
{{--                            </li>--}}
{{--                            <li class="{{ menuRoute('reports/customer-order-total-chart','li')  }}">--}}
{{--                                <a href="{{ route('reports.customer-order-total-chart') }}">{{_trans('Customer By order Total Chart')}}</a>--}}
{{--                            </li>--}}
{{--                            <li class="{{ menuRoute('reports/restaurant-order-paid-type-chart','li')  }}">--}}
{{--                                <a href="{{ route('reports.restaurant-order-paid-type-chart') }}">{{_trans('Restaurant Order Paid Type Chart')}}</a>--}}
{{--                            </li>--}}


{{--                        </ul>--}}
{{--                    </li>--}}
{{--                @endcanany--}}

                <li class="sidebar-list {{ menuRoute('profile*','li') }}">
                    <a class="sidebar-link sidebar-title link-nav" href="{{ route('owner.profile.account') }}">
                        <div class="curve1"></div>
                        <div class="curve2"></div>
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g>
                                <g>
                                    <path d="M15.596 15.6963H8.37598" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                    <path d="M15.596 11.9365H8.37598" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                    <path d="M11.1312 8.17725H8.37622" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                          d="M3.61011 12C3.61011 18.937 5.70811 21.25 12.0011 21.25C18.2951 21.25 20.3921 18.937 20.3921 12C20.3921 5.063 18.2951 2.75 12.0011 2.75C5.70811 2.75 3.61011 5.063 3.61011 12Z" stroke="#130F26"
                                          stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                </g>
                            </g>
                        </svg>
                        <span>{{ _trans('My Account') }}</span>
                    </a>
                </li>
                @canany(['Payment list'])
                    <li class="sidebar-list">
                        <a class="sidebar-link sidebar-title link-nav" href="{{ route('owner.payments') }}">
                            <div class="curve1"></div>
                            <div class="curve2"></div>
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g>
                                    <g>
                                        <path d="M15.596 15.6963H8.37598" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path d="M15.596 11.9365H8.37598" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path d="M11.1312 8.17725H8.37622" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M3.61011 12C3.61011 18.937 5.70811 21.25 12.0011 21.25C18.2951 21.25 20.3921 18.937 20.3921 12C20.3921 5.063 18.2951 2.75 12.0011 2.75C5.70811 2.75 3.61011 5.063 3.61011 12Z" stroke="#130F26"
                                              stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                    </g>
                                </g>
                            </svg>
                            <span>{{ _trans('Payments') }}</span>
                        </a>
                    </li>
                @endcanany

                <li class="sidebar-list">
                    <a class="sidebar-link sidebar-title link-nav" href="{{ route('owner.logout') }}">
                        <div class="curve1"></div>
                        <div class="curve2"></div>
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g>
                                <g>
                                    <path d="M15.596 15.6963H8.37598" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                    <path d="M15.596 11.9365H8.37598" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                    <path d="M11.1312 8.17725H8.37622" stroke="#130F26" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                          d="M3.61011 12C3.61011 18.937 5.70811 21.25 12.0011 21.25C18.2951 21.25 20.3921 18.937 20.3921 12C20.3921 5.063 18.2951 2.75 12.0011 2.75C5.70811 2.75 3.61011 5.063 3.61011 12Z" stroke="#130F26"
                                          stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                                </g>
                            </g>
                        </svg>
                        <span>{{ _trans('Logout') }}</span>
                    </a>
                </li>


            </ul>

        </div>
        <div class="right-arrow" id="right-arrow"><i data-feather="arrow-right"></i></div>
    </nav>
</div>
