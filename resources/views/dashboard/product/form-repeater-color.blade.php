@if (count($data) > 0)
{{--        {{dd($data)}}--}}
    <div class="repeater-default">
        <div data-repeater-list="colors">
            @foreach($data as $key => $value)
                <div data-repeater-item="" style="">
                    <div class="row">
                        <input type="hidden" name="colors[{{ $key }}][id]" value="{{ $value['id'] }}">
                        <input type="hidden" name="colors[{{ $key }}][status]" value="{{ $value['status'] }}">
                        @foreach($languages as $lang)

                            <div class="col-md-3">
                                <label class="form-label" for="name:{{ $lang['code'] }}">{{ _trans('Color') }} ({{ ucfirst($lang['code']) }})</label>
                                <input type="text"
                                       name="colors[{{ $key }}][name:{{ $lang['code'] }}]"
                                       value="{{ $value['name:'.$lang['code'] ] }}"
                                       class="form-control @error('colors.'.$key.'.name:'.$lang['code'] ) is-invalid @enderror"
                                       id="from"
                                />
                                @error('colors.'.$key.'.name:'.$lang['code'] )
                                <span class="text-danger">{!! $message !!} </span>
                                @enderror

                            </div>
                        @endforeach

                        <div class="col-md-3">
                            <label class="form-label" for="from">{{ _trans('Code') }}</label>
                            <input type="color"
                                   name="colors[{{ $key }}][code]"
                                   value="{{ $value['code'] }}"
                                   class="form-control @error('colors.'.$key.'.code') is-invalid @enderror"
                                   id="from"
                            />
                            @error('colors.'.$key.'.code')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>
                        <div class="col-md-1">
                            <div class="form-group col-sm-12 col-md-2 text-center mt-2">
                                <br>
                                {{--                                <button type="button" id="deleteRepeater" class="btn btn-danger" data-repeater-delete="">--}}
                                {{--                                    <i class="fas fa-trash-alt"></i> {{ _trans('Delete') }}--}}
                                {{--                                </button>--}}

                                <div class="media-body icon-state">
                                    <label class="switch">
                                        <input id="{{ $value['id'] }}" type="checkbox" class="status" @checked($value['status']) >
                                        <span class="switch-state ">

                                            </span>
                                    </label>
                                </div>

                            </div>
                        </div>

                    </div>
                    <hr>
                </div>
            @endforeach
        </div>
        <div class="form-group overflow-hidden">
            <div class="col-12">
                <button type="button" id="addColorRepeater" data-repeater-create="" class="btn btn-primary">
                    <i class="far fa-plus-square"></i> {{ _trans('Add') }}
                </button>
            </div>
        </div>
    </div>

@else
    <div class="repeater-default">
        <div data-repeater-list="colors">
            <div data-repeater-item="" style="">
                <div class="row">
                    <input type="hidden" name="id" value=" ">
                    <input type="hidden" name="status" value="1">
                    @foreach($languages as $lang)
                        <div class="col-md-4">
                            <label class="form-label" for="name:{{ $lang['code'] }}">{{ _trans('Color') }} ({{ ucfirst($lang['code']) }})</label>
                            <input type="text"
                                   name="name:{{ $lang['code'] }}"
                                   id="name:{{ $lang['code'] }}"
                                   class="form-control">
                        </div>
                    @endforeach

                    <div class="col-md-3">
                        <label class="form-label" for="from">{{ _trans('Color') }}</label>
                        <input type="color"
                               name="code"
                               id="code"
                               class="form-control"
                        >
                    </div>

                    <div class="col-md-1">
                        <div class="form-group col-sm-12 col-md-2 text-center mt-2">
                            <br>
                            <button type="button" id="deleteRepeater" class="btn btn-danger" data-repeater-delete="">
                                {{--<i class="fas fa-trash-alt"></i>--}} {{ _trans('Delete') }}
                            </button>
                        </div>
                    </div>
                </div>
                <hr>
            </div>
        </div>
        <div class="form-group overflow-hidden">
            <div class="col-12">
                <button type="button" id="addColorRepeater" data-repeater-create="" class="btn btn-primary">
                    {{--<i class="far fa-plus-square"></i> --}} {{ _trans('Add') }}
                </button>
            </div>
        </div>
    </div>
@endif
@include('layouts.ajax.update-status',['class' => 'status','route' => route('color.update-status')])

@push('js')
    <script>
        var limit = 1;
        $(document).ready(function () {
            limit = {{ count($data) == 0 ? 1 : count($data)  }};
            limitRepeater();
        })
        $(document).on('click', '#addColorRepeater', function () {
            limit++;
            if (limit > 8) {
                $('#addColorRepeater').attr('disabled', true)
            }
        })
        $(document).on('click', '#deleteRepeater', function () {
            limit--;
            limitRepeater();
        })

        function limitRepeater() {
            if (limit > 8) {
                $('#addColorRepeater').attr('disabled', true)
            } else {
                $('#addColorRepeater').attr('disabled', false)
            }
        }

    </script>
@endpush
