@if (count($data) > 0)
    <div class="contact-repeater">
        <div data-repeater-list="sizes">
            @foreach($data as $key => $value)
                <div data-repeater-item="" style="">
                    <div class="row">
                        <input type="hidden" name="sizes[{{ $key }}][id]" value="{{ $value['id'] }}">
                        @foreach($languages as $lang)
                            <div class="col-md-3">
                                <label class="form-label" for="name:{{ $lang['code'] }}">{{ _trans('Size') }} ({{ ucfirst($lang['code']) }})</label>
                                <input type="text"
                                       name="sizes[{{ $key }}][name:{{ $lang['code'] }}]"
                                       value="{{ $value['name:'.$lang['code'] ] }}"
                                       class="form-control @error('sizes.'.$key.'.name:'.$lang['code'] ) is-invalid @enderror"
                                       id="from"
                                />
                                @error('sizes.'.$key.'.name:'.$lang['code'] )
                                <span class="text-danger">{!! $message !!} </span>
                                @enderror

                            </div>
                        @endforeach

                        <div class="col-md-3">
                            <label class="form-label" for="from">{{ _trans('Price') }}</label>
                            <input type="number"
                                   step="any"
                                   name="sizes[{{ $key }}][price]"
                                   value="{{ $value['price'] }}"
                                   class="form-control @error('sizes.'.$key.'.price') is-invalid @enderror"
                                   id="from"
                            />
                            @error('sizes.'.$key.'.price')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>
                        <div class="col-md-1">
                            <div class="form-group col-sm-12 col-md-2 text-center mt-2">
                                <br>
                                <button type="button" data-repeater-delete=""
                                        data-id="{{ Arr::exists($value,'id') ? $value['id'] : '' }}" id="deleteRepeater" class="btn btn-danger">
                                    <i class="fas fa-trash-alt"></i> {{ _trans('Delete') }}
                                </button>
                            </div>
                        </div>

                    </div>
                    <hr>
                </div>
            @endforeach
        </div>
        <div class="form-group overflow-hidden">
            <div class="col-12">
                <button type="button" id="addRepeater" data-repeater-create="" class="btn btn-primary">
                    <i class="far fa-plus-square"></i> {{ _trans('Add') }}
                </button>
            </div>
        </div>
    </div>

@else
    <div class="contact-repeater">
        <div data-repeater-list="sizes">
            <div data-repeater-item="" style="">
                <div class="row">
                    <input type="hidden" name="id" value=" ">
                    <input type="hidden" name="status" value="1">
                    @foreach($languages as $lang)
                        <div class="col-md-4">
                            <label class="form-label" for="name:{{ $lang['code'] }}">{{ _trans('size') }} ({{ ucfirst($lang['code']) }})</label>
                            <input type="text"
                                   name="name:{{ $lang['code'] }}"
                                   id="name:{{ $lang['code'] }}"
                                   class="form-control">
                        </div>
                    @endforeach

                    <div class="col-md-3">
                        <label class="form-label" for="from">{{ _trans('price') }}</label>
                        <input type="number"
                               step="any"
                               name="price"
                               id="price"
                               class="form-control"
                        >
                    </div>

                    <div class="col-md-1">
                        <div class="form-group col-sm-12 col-md-2 text-center mt-2">
                            <br>
                            <button type="button" id="deleteRepeater" class="btn btn-danger" data-repeater-delete="">
                                {{--<i class="fas fa-trash-alt"></i>--}} {{ _trans('Delete') }}
                            </button>
                        </div>
                    </div>
                </div>
                <hr>
            </div>
        </div>
        <div class="form-group overflow-hidden">
            <div class="col-12">
                <button type="button" id="addRepeater" data-repeater-create="" class="btn btn-primary">
                    {{--<i class="far fa-plus-square"></i> --}} {{ _trans('Add') }}
                </button>
            </div>
        </div>
    </div>
@endif

@push('scripts')
    <script>
        var limit = 1;
        $(document).ready(function () {
            limit = {{ count($data) == 0 ? 1 : count($data)  }};
            limitRepeater();
        })
        $(document).on('click', '#addRepeater', function () {
            limit++;
            if (limit > 2) {
                $('#addRepeater').attr('disabled', true)
            }
        })
        $(document).on('click', '#deleteRepeater', function () {
            limit--;
            limitRepeater();
            let sizeId = $(this).attr("data-id");
            deleteSize(sizeId,)
        })

        function limitRepeater() {
            if (limit > 2) {
                $('#addRepeater').attr('disabled', true)
            } else {
                $('#addRepeater').attr('disabled', false)
            }
        }

        function deleteSize(sizeId) {
            if (sizeId == '' || sizeId == null) {
                return false;
            }
            //console.log($('.contact-repeater'))
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                url: "{{ route('product.delete-size') }}",
                method: 'POST',
                cache: false,
                data: {
                    size_id: sizeId,
                    product_id: "{{ $edit ? ($product?->id) : null }}",
                    owner_id: "{{ $edit ? ($product?->owner_id) : null }}",
                    shop_id: "{{ $edit ? ($product?->shop_id) : null }}",
                },
                success: function (response) {
                    if (response.status == true) {
                        toastr.success(response.data)
                    } else {
                        toastr.error(response.data)
                        setTimeout(function () {
                            location.reload();
                        }, 5000)
                    }

                },

            });
        }
    </script>
@endpush
