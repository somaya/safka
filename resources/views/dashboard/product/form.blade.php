@extends('layouts.master')
@section('title',$edit ? _trans('Edit Product') : _trans('Add Product'))
@section('content')
    <div class="container-fluid">
        <div class="page-title">
            <div class="row">
                <div class="col-6 col-sm-6">
                    <h3>{{ $edit ? _trans('Edit Product') : _trans('Add Product') }}</h3>
                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid default-dash">
        <div class="row">

            <div class="row m-t-20">
                <div class="col-12 col-sm-6">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-home">
                                    <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                    <polyline points="9 22 9 12 15 12 15 22"></polyline>
                                </svg>
                            </a></li>
                        <li class="breadcrumb-item">
                            @canany(['Product list','Product edit','Product delete'])
                                <a href="{{route('product.index')}}">{{ _trans('Products') }}</a>
                            @endcanany
                        </li>
                        <li class="breadcrumb-item active">{{ $edit ? _trans('Edit Product') : _trans('Add Product') }}</li>
                    </ol>
                </div>
                <div class="col-md-12">
                    @if ($edit)
                        {{ Form::open(['route' => ['product.update',$product->id],'method' => 'PUT','files' => true,'class' => 'form mb-15 form-submit','id' =>'kt_contact_form']) }}
                        {{ Form::hidden('id',$product->id) }}
                    @else
                        {{ Form::open(['route' => 'product.store','method' => 'POST','files' => true,'class' => 'form mb-15','id' =>'kt_contact_form']) }}
                    @endif

                    <div class="row g-3">
                        <div class="col-md-4">
                            <label class="form-label" for="code">{{ _trans('Product Code') }}</label>
                            <input id="code"
                                   disabled
                                   type="text"
                                   class="form-control"
                                   placeholder="{{ _trans('Product Code') }}"
                                   value="{{ getCodeTable('Pro','products',$edit,$edit ? $product->id : null) }}"
                            >
                        </div>
                        @if (request('guard')=='admin')

                            <div class="col-md-4">
                                <label class="form-label" for="owner_id">{{ _trans('Owner name') }}</label>
                                <select id="owner_id" name="owner_id"
                                        class="js-example-basic-single @error('owner_id') is-invalid @enderror">
                                    <option value="">{{ _trans('Select owner name') }}</option>
                                    @foreach($data as $owner)

                                        <option value="{{ $owner->id }}"
                                            @selected(old('owner_id',$edit ? $product->owner_id : null) == $owner->id)
                                        >{{ $owner->user->name }}</option>
                                    @endforeach
                                </select>
                                @error('owner_id')
                                <span class="text-danger">{!! $message !!} </span>
                                @enderror
                            </div>

                            <div class="col-md-4">
                                <label class="form-label" for="shop_id">{{ _trans('Shop name') }}</label>
                                <select id="shop_id"
                                        name="shop_id"
                                        class="js-example-basic-single @error('shop_id') is-invalid @enderror">
                                    <option value="">{{ _trans('Select shop name') }}</option>

                                </select>
                                @error('shop_id')
                                <span class="text-danger">{!! $message !!} </span>
                                @enderror
                            </div>
                        @else
                            <div class="col-md-4">
                                <label class="form-label" for="shop_id">{{ _trans('Shop name') }}</label>
                                <select id="shop_id" name="shop_id"
                                        class="js-example-basic-single @error('shop_id') is-invalid @enderror">
                                    <option value="">{{ _trans('Select Shop name') }}</option>
                                    @foreach($data as $shop)
                                        <option value="{{ $shop->id }}"
                                            @selected(old('shop_id',$edit ? $product->shop_id : null) == $shop->id)
                                        >{{ $shop->translate(locale())?->name }}</option>
                                    @endforeach
                                </select>
                                @error('shop_id')
                                <span class="text-danger">{!! $message !!} </span>
                                @enderror
                            </div>
                        @endif

                        <div class="col-md-4">
                            <label class="form-label" for="category_id">{{ _trans('Category name') }}</label>
                            <select id="category_id" name="category_id"
                                    class="js-example-basic-single @error('category_id') is-invalid @enderror">
                                <option value="">{{ _trans('Select category name') }}</option>
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}"
                                        @selected(old('category_id',$edit ? $product->category_id : null) == $category->id)
                                    >{{ $category->translate(locale())?->name }}</option>
                                @endforeach
                            </select>
                            @error('category_id')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="form-label" for="sub_category_id">{{ _trans('Sub category name') }}</label>
                            <select id="sub_category_id" name="sub_category_id"
                                    class="js-example-basic-single @error('sub_category_id') is-invalid @enderror">
                                <option value="">{{ _trans('Select Product Sub Category') }}</option>

                            </select>
                            @error('sub_category_id')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="form-label" for="price">{{ _trans('Price') }}</label>
                            <input id="price"
                                   type="number"
                                   min="0"
                                   step="any"
                                   name="price"
                                   class="form-control @error('price') is-invalid @enderror"
                                   placeholder="{{ _trans('Price') }}"
                                   value="{{ old('price',$edit ? $product->price : 0) }}"
                            >
                            @error('price')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>
                        <div class="col-md-4">
                            <label class="form-label"
                                   for="maximum_order_number">{{ _trans('Maximum Order Number') }}</label>
                            <input id="maximum_order_number"
                                   type="number"
                                   min="1"
                                   step="any"
                                   name="maximum_order_number"
                                   class="form-control @error('maximum_order_number') is-invalid @enderror"
                                   placeholder="{{ _trans('maximum_order_number') }}"
                                   value="{{ old('maximum_order_number',$edit ? $product->maximum_order_number : 1) }}"
                            >
                            @error('maximum_order_number')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>

                        @foreach($languages as $lang)
                            <div class="col-md-4">
                                <label for="{{ $lang['code'] }}[name]" class="form-label">{{ _trans('Product name') }}
                                    ({{ ucfirst($lang['code']) }})</label>
                                <input id="{{ $lang['code'] }}[name]"
                                       type="text"
                                       name="{{ $lang['code'] }}[name]"
                                       class="form-control @error($lang['code'].'.name') is-invalid @enderror"
                                       placeholder="{{ _trans('Product name') }} ({{ ucfirst($lang['code']) }})"
                                       value="{{ old($lang['code'].'.name',$edit ? $product->translate($lang['code'])?->name : null) }}"
                                >
                                @error($lang['code'].'.name')
                                <span class="text-danger">{!! $message !!} </span>
                                @enderror
                            </div>
                        @endforeach

                        @foreach($languages as $lang)
                            <div class="col-md-6">
                                <label for="{{ $lang['code'] }}[description]"
                                       class="form-label">{{ _trans('Product description') }}
                                    ({{ ucfirst($lang['code']) }})</label>
                                <textarea id="{{ $lang['code'] }}[description]"
                                          class="form-control @error($lang['code'].'.description') is-invalid @enderror "
                                          name="{{ $lang['code'] }}[description]"
                                          rows="5">{{ old($lang['code'].'.description',$edit ? $product->translate($lang['code'])?->description : null) }}</textarea>

                                @error($lang['code'].'.description')
                                <span class="text-danger">{!! $message !!} </span>
                                @enderror
                            </div>
                        @endforeach

                        <div class="col-md-6">
                            <label for="video" class="font-md">{{_trans('Add Video Link')}}</label>
                            <input id="video"
                                   type="text"
                                   name="video"
                                   class="form-control @error('video') is-invalid @enderror"
                                   placeholder="{{ _trans('Video Link') }}"
                                   value="{{ old('video',$edit ? $product->video : null) }}"
                            >
                            @error('video')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>

                        <div class="col-md-6">
                            <div class="mb-1">
                                <label class="form-label">{{ _trans('Icon') }}</label>
                            </div>
                            <div class="p-2 border border-dashed" style="max-width:230px;">
                                <div class="row" id="icon"></div>
                            </div>
                            @error('icon')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>

                        <div class="col-md-6">
                            <div class="mb-1">
                                <label class="form-label">{{ _trans('Product images') }}</label></div>
                            <div class="p-2 border border-dashed" style="max-width:430px;">
                                <div class="row" id="images">
                                    @if($edit)
                                        @foreach ($product->images as $key => $photo)
                                            <div class="col-6" id="image_{{ $photo->id }}">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <img style="width: 100%" height="auto"
                                                             onerror="this.src='{{asset('admin/front-end/img/image-place-holder.png')}}'"
                                                             src="{{ getAvatar($photo->full_file) }}"
                                                             alt="Product image">
                                                        <button type="button"
                                                                class="btn btn-danger btn-block delete-img"
                                                                id="{{ $photo->id }}">
                                                            <i class="fa fa-times"></i>
                                                        </button>

                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                            @error('images','images.*')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                            <br>
                            <br>
                        </div>



                        <div class="col-md-12">
                            <h4>{{ _trans('Sizes') }}</h4>
                            <hr>
                            @include('dashboard.product.form-repeater',[
    'product' => $edit ? $product : null,
    'data' => $edit ? old('sizes',$product->sizes ?? []) :  (!empty(old('sizes')) ? (old('sizes')) : [])
     ])

                        </div>
                        <div class="col-md-12">
                            <h5>{{ _trans('colors') }}</h5>
                            <hr>
                            @include('dashboard.product.form-repeater-color',[
          'product' => $edit ? $product : null,
         'data' => $edit ? old('colors',$product->colors ?:[]) :  (!empty(old('colors')) ? (old('colors')) : []) ])

                        </div>

                    </div>
                </div>
                <div class="col-md-12">
                    <div class="m-t-50 d-flex justify-content-end">
                        <button type="submit"
                                class="btn btn-primary">{{ $edit ?_trans('Update') : _trans('Save') }}</button>
                    </div>
                </div>
                {{ Form::close() }}

            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection
@include('layouts.partials.ckeditor',['inputClass' => 'textarea'])
@push('scripts')
    <script src="{{ asset('assets/repeater/jquery.repeater.min.js') }}"></script>
    <script src="{{ asset('assets/repeater/form-repeater.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            @if(!is_null(old('owner_id')))
            getShops({{ old('owner_id') }}, {{ !is_null(old('shop_id')) ? old('shop_id') : 0 }})
            @endif
            @if($edit)
            getShops({{ old('owner_id',$edit ? $product->owner_id : 0) }}, {{ old('shop_id',$edit ? $product->shop_id : 0) }})
            @endif
            getSubCategories('{{ old('category_id',$edit ? $product->category_id : null) }}', '{{ old('sub_category_id',$edit ? $product->sub_category_id : null) }}')
        })
    </script>
    @include('layouts.ajax.shop')
    @include('layouts.ajax.category')

    @includeWhen($edit,'layouts.ajax.delete-product-images',[
         'route' => route('product.delete-image'),
         'relation_id' => $edit ? $product->id : null
         ])
@endpush
@push('scripts')
    <script src="{{ asset('assets/js/spartan-multi-image-picker.js') }}"></script>
    @include('layouts.partials.spartan-multi-image',[
    'single' => true,
    'file_name' => 'icon',
    'image' => $edit ? getAvatar($product->icon) :  asset('assets/images/img/400x400/img2.jpg')
    ])

    @include('layouts.partials.spartan-multi-image',[
    'multi' => true,
    'file_name' => 'images',
    'count' => $edit ? 5 - $product->images_count : 5,
    ])

@endpush


