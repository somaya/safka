@extends('layouts.master')
@section('title',_trans('Products'))
@section('content')
    <div class="container-fluid">
        <div class="page-title">
            <div class="row">
                <div class="col-6 col-sm-6">
                    <h3>{{_trans('Products')}}</h3>
                </div>
                @can('Product add')
                    <div class="col-6 text-right">
                        <a href="{{ route('product.create') }}" class="btn btn-primary">
                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                        </a>
                    </div>
                @endcan

            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid default-dash">
        <div class="row">
            <div class="col-12 col-sm-6">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                        </a>
                    </li>
                    <li class="breadcrumb-item active">{{ _trans('Product') }}</li>
                </ol>
            </div>
            <div class="col-xl-12 col-md-12 ">
                <form action="{{ url()->current() }}" method="GET">
                    <div class="left-side-header">
                        <div class="row justify-content-between align-items-center">
                            <x-search :columns="$columns"/>
                            <div class="col-md-4">
                                <p class="mb-0 font-sm d-flex align-items-center justify-content-end">{{ _trans('Count Products') }} :
                                    <span class="d-block font-md text-danger">({{ $data->total() }})</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </form>

                <div class="table-responsive custom-scrollbar p-t-30">
                    <table class="table">
                        <thead>
                        <tr>
                            <th><span>{{ _trans('Code')}}  </span></th>
                            <th><span>{{ _trans('Date')}} </span></th>
                            <th><span>{{ _trans('Category name')}} </span></th>
                            <th><span>{{ _trans('Shop name')}} </span></th>
                            <th><span>{{ _trans('Product name')}} </span></th>
                            <th><span>{{ _trans('Number Of Uses')}} </span></th>
                            <th><span>{{ _trans('Image')}} </span></th>
                            <th><span>{{ _trans('Active')}} </span></th>
                            <th><span>{{ _trans('Most Wanted')}} </span></th>
                            <th><span>{{ _trans('action')}}</span></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $index => $item)
                            <tr>
                                <td class="text-main">
                                    <a href="{{ route('product.show',$item->id) }}">Product#{{$item->id}}</a>
                                </td>
                                <td>{{ $item->created_at->format('d-m-Y') }}</td>
                                <td>{{ $item->category->translate(locale())?->name }}</td>
                                <td>{{ $item->shop->translate(locale())?->name }}</td>
                                <td>
                                    <a href="{{ route('product.edit',$item->id) }}">{{ $item->translate(locale())?->name }}</a>
                                </td>
                                <td>{{ $item->getUses() }}</td>
                                <td>
                                    <img src="{{ getAvatar($item->icon) }}" class="mb-2 image-preview">
                                </td>
                                <td>
                                    <div class="media-body icon-state">
                                        <label class="switch">
                                            <input id="{{ $item->id }}" type="checkbox" class="status" @checked($item->status == 1) >
                                            <span class="switch-state"></span>
                                        </label>
                                    </div>
                                </td>
                                <td>
                                    <div class="media-body icon-state">
                                        <label class="switch">
                                            <input id="{{ $item->id }}" type="checkbox" class="wanted" @checked($item->wanted == 1) >
                                            <span class="switch-state "></span>
                                        </label>
                                    </div>
                                </td>
                                <td>
                                    @can('Product list')
                                        <a href="{{ route('product.show',$item->id) }}" class="btn btn-primary">
                                            <i class="fa fa-eye" aria-hidden="true"></i>
                                        </a>
                                    @endcan
                                    @can('Product edit')
                                        <a href="{{ route('product.edit',$item->id) }}" class="btn btn-primary">
                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                        </a>
                                    @endcan
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $data->appends(request()->query())->links('layouts.partials.pagination') }}

                @if( $data->count() == 0)
                    <div class="empty-data">
                        <img src="{{ asset('assets')}}/images/nodata.svg">
                        <h4>{{ _trans('No_data_to_show')}}</h4>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection
@include('layouts.ajax.update-status',['class' => 'status','route' => route('product.update-status')])
@include('layouts.ajax.update-status',['class' => 'wanted','route' => route('product.update-wanted')])
@include('layouts.ajax.search_input')
