@extends('layouts.master')
@section('title',_trans('Products'))
@section('content')

        <div class="container-fluid">
            <div class="page-title">
                <div class="row">
                    <div class="col-6 col-sm-6">
                        <h3>{{ _trans('Product Details') }}</h3>
                    </div>
                    <div class="col-6 text-right">

                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid default-dash">
            <div class="row">
                <div class="col-12 col-sm-6">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/') }}">
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
                            </a></li>
                        <li class="breadcrumb-item"> <a href="#">{{ _trans('Product Details') }}</a></li>
                        <li class="breadcrumb-item active">{{ _trans('Sizes') }}</li>
                    </ol>
                </div>

                <div class="col-xl-12 col-md-12 mb-4">

                    <div class="table-responsive custom-scrollbar p-t-30">
                        <table class="table">
                            <thead>
                            <tr>
                                <th> <span>{{ _trans('SL')}}  </span></th>
                                <th> <span>{{ _trans('Date')}} </span></th>
                                <th> <span>{{ _trans('Size')}} </span></th>
                                <th> <span>{{ _trans('Price')}} </span></th>
{{--                                <th> <span>{{ _trans('Active')}} </span></th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($sizes as $index => $item)
                                <tr>
                                    <td class="text-main">#{{$index+1}}</td>
                                    <td>{{$item->created_at->format('d-m-Y') }}</td>
                                    <td>{{$item->translate(locale(),true)->name}}</td>
                                    <td>{{$item->price}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    @if( $sizes->count() == 0)
                        <div class="empty-data">
                            <img src="{{ asset('assets/')}}/images/nodata.svg">
                            <h4>{{ _trans('No_data_to_show')}}</h4>
                        </div>
                    @endif
                </div>

                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card card-details">
                                <h4 class="title-details">{{_trans('Product Details')}}</h4>
                                <div class="">
                                    <p>{{_trans('Name')}} :<span>{{$product->translate(locale())?->name}}</span></p>
                                    <p>{{_trans('Category')}} :<span>{{$product->category->translate(locale())?->name}}</span></p>
                                    <p>{{_trans('Shop')}} :<span>{{$product->shop->translate(locale())?->name}}</span></p>
                                    <p>{{_trans('Price')}} :<span>{{$product->price}}</span></p>

                                    <p>{{_trans('Icon')}} : <span>
                                  <img class="mb-2 image-preview" src="{{getAvatar($product->icon)}}">
                              </span></p>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card card-details">
                                        <h4 class="title-details">{{ _trans('Discounts')}}</h4>
                                        <div class="">
                                            <p>{{ _trans('Count')}} : <span>{{$discounts}}</span></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="card card-details">
                                        <h4 class="title-details">{{ _trans('Orders')}}</h4>
                                        <div class="">
                                            <p>{{ _trans('Count')}} : <span>{{$orders->count()}}</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                </div>

            </div>
        </div>
        <!-- Container-fluid Ends-->

@endsection
