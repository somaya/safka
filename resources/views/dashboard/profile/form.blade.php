@extends('layouts.master')

@section('title',_trans('Account'))


@section('content')

    <div class="container-fluid">
        <div class="page-title">
            <div class="row">
                <div class="col-6 col-sm-6">
                    <h3>{{ _trans('Account') }}</h3>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid default-dash">
        <div class="row">
            <div class="col-12 col-sm-6">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">

                        <a href="{{ url('/') }}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                 class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                        </a>
                    </li>
                    <li class="breadcrumb-item active">{{ _trans('Account') }} </li>
                </ol>
            </div>
            <div class="col-md-12">
                <ul class="nav nav-tabs nav-settings" id="myTab" role="tablist">
                    <li class="nav-item"><a class="nav-link active" id="personal-information-tab" data-bs-toggle="tab" href="#personal-information"
                                            role="tab" aria-controls="home" aria-selected="true">{{ _trans('Personal Information') }}</a>
                    </li>

                    <li class="nav-item"><a class="nav-link" id="email-setting-tabs" data-bs-toggle="tab" href="#change-password" role="tab"
                                            aria-controls="change-password" aria-selected="false">{{ _trans('Change password') }}</a></li>
                </ul>
                <div class="tab-content" id="myTabContent">

                    <div class="tab-pane fade show active" id="personal-information" role="tabpanel" aria-labelledby="personal-information-tab">
                        <br>
                        <div class="col-md-12">
                            {{ Form::open(['route' => 'owner.profile.account','method' => 'POST','files' => true]) }}

                            <div class="row g-3">

                                <div class="col-md-6">
                                    <label class="form-label" for="name">{{ _trans('Full name') }}</label>
                                    <input id="name"
                                           type="text"
                                           name="name"
                                           class="form-control @error('name') is-invalid @enderror"
                                           placeholder="{{ _trans('Full name') }}"
                                           value="{{ old('name',$user->name) }}"
                                    >
                                    @error('name')
                                    <span class="text-danger">{!! $message !!} </span>
                                    @enderror
                                </div>

                                <div class="col-md-6">
                                    <label class="form-label" for="email">{{ _trans('Email address') }}</label>
                                    <input id="email"
                                           type="email"
                                           name="email"
                                           class="form-control @error('email') is-invalid @enderror"
                                           placeholder="{{ _trans('Email address') }}"
                                           value="{{ old('email',$user->email) }}"
                                    >
                                    @error('email')
                                    <span class="text-danger">{!! $message !!} </span>
                                    @enderror
                                </div>

                                <div class="col-md-6">
                                    <label class="form-label" for="phone">{{ _trans('Phone') }}</label>
                                    <input id="phone"
                                           type="text"
                                           name="phone"
                                           class="form-control @error('phone') is-invalid @enderror"
                                           placeholder="{{ _trans('Phone') }}"
                                           value="{{ old('phone',$user->userable->phone) }}"
                                    >
                                    @error('phone')
                                    <span class="text-danger">{!! $message !!} </span>
                                    @enderror
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group form-image">
                                        <label class="font-md">{{ _trans('Avatar') }}</label>
                                        <img src="{{ getAvatar($user->avatar) }}"
                                             class="mb-2 avatar-preview">
                                        <input id="avatar"
                                               type="file"
                                               name="avatar"
                                               class="form-control avatar @error('avatar') is-invalid @enderror">
                                        @error('avatar')
                                        <span class="text-danger">{!! $message !!} </span>
                                        @enderror
                                    </div>

                                </div>

                                <div class="col-md-12">
                                    <div class="m-t-50 d-flex justify-content-end">
                                        <button type="submit" class="btn btn-primary">{{ _trans('Update profile') }}</button>
                                    </div>
                                </div>

                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="change-password" role="tabpanel" aria-labelledby="change-password-tab">
                        <br>
                        <div class="col-md-12">
                            {{ Form::open(['route' => 'owner.profile.change-password','method' => 'POST','files' => true]) }}

                            <div class="row g-3">
                                <div class="col-md-6">
                                    <label class="form-label" for="old_password">{{ _trans('Old password') }}</label>
                                    <div class="input-group">
                                        <input id="old_password"
                                               type="password"
                                               name="old_password"
                                               class="form-control @error('old_password') is-invalid @enderror"
                                               placeholder="{{ _trans('Old password') }}"
                                               value="{{ old('old_password')}}"
                                        >
                                        <div class="show-hide">
                                        <span class="show show_password">
                                            <i class="fa fa-eye"></i>
                                        </span>
                                        </div>
                                    </div>
                                    @error('old_password')
                                    <span class="text-danger">{!! $message !!} </span>
                                    @enderror
                                </div>

                                <div class="col-md-6">
                                    <label class="form-label" for="password">{{ _trans('New password') }}</label>
                                    <div class="input-group">
                                        <input id="password"
                                               type="password"
                                               name="password"
                                               class="form-control @error('password') is-invalid @enderror"
                                               placeholder="{{ _trans('New password') }}"
                                        >
                                        <div class="show-hide">
                                        <span class="show show_password">
                                            <i class="fa fa-eye"></i>
                                        </span>
                                        </div>
                                    </div>

                                    @error('password')
                                    <span class="text-danger">{!! $message !!} </span>
                                    @enderror
                                </div>

                                <div class="col-md-6">
                                    <label class="form-label" for="password_confirmation">{{ _trans('Confirm New Password') }}</label>
                                    <div class="input-group">
                                        <input id="password_confirmation"
                                               type="password"
                                               name="password_confirmation"
                                               class="form-control @error('password_confirmation') is-invalid @enderror"
                                               placeholder="{{ _trans('Confirm New Password') }}"
                                        >
                                        <div class="show-hide">
                                        <span class="show show_password">
                                            <i class="fa fa-eye"></i>
                                        </span>
                                        </div>
                                    </div>
                                    @error('password_confirmation')
                                    <span class="text-danger">{!! $message !!} </span>
                                    @enderror
                                </div>

                                <div class="col-md-12">
                                    <div class="m-t-50 d-flex justify-content-end">
                                        <button type="submit" class="btn btn-primary">{{ _trans('Change password') }}</button>
                                    </div>
                                </div>


                            </div>

                            {{ Form::close() }}


                        </div>
                    </div>

                </div>
            </div>


        </div>
    </div>
    <!-- Container-fluid Ends-->

@endsection

@include('layouts.partials.read-photo',['inputName' => 'avatar'])
@push('scripts')
    <script>
        $(document).on('click', '.show_password', function () {
            if ($('#password').attr('type') == 'password') {
                $('#password,#password_confirmation,#old_password').attr('type', 'text')
            } else {
                $('#password,#password_confirmation,#old_password').attr('type', 'password');
            }

        })
    </script>
@endpush
