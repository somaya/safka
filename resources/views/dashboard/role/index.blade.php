@extends('layouts.master')
@section('title',_trans('Roles'))
@section('content')
    <div class="container-fluid">
        <div class="page-title">
            <div class="row">
                <div class="col-6 col-sm-6">
                    <h3>{{ _trans('Role Staff') }}</h3>
                </div>
                @can('Role#Staff add')
                    <div class="col-6 text-right">
                        <a href="{{ route('role.create') }}" class="btn btn-primary">
                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                        </a>
                    </div>
                @endcan
            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid default-dash">
        <div class="row">
            <div class="col-12 col-sm-6">
                <ol class="breadcrumb">
                    <x-link-home/>
                    <li class="breadcrumb-item active">{{ _trans('Role Staff') }}</li>
                </ol>
            </div>

            <div class="col-xl-12 col-md-12 ">
                <form action="{{ url()->current() }}" method="GET">
                    <div class="left-side-header">
                        <div class="row justify-content-between align-items-center">
                            <x-search :columns="$columns"/>
                            <div class="col-md-4">
                                <p class="mb-0 font-sm d-flex align-items-center justify-content-end">{{ _trans('Count Role Staff') }} :
                                    <span class="d-block font-md text-danger">({{ $roles->total() }})</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </form>

                <div class="table-responsive custom-scrollbar p-t-30">
                    <table class="table">
                        <thead>
                        <tr>
                            <th><span>{{ _trans('SL')}} </span></th>
                            @if (auth('admin')->check())
                                <th><span>{{ _trans('Owner Name')}}</span></th>
                            @endif
                            <th><span>{{ _trans('Role Name')}}</span></th>
                            <th><span>{{ _trans('Count Staff')}}</span></th>
                            <th><span>{{ _trans('Count Permission')}}</span></th>
                            <th><span>{{ _trans('action')}}</span></th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($roles as $key => $row)
                            <tr>
                                <td class="text-main"> {{ $key + 1 }}</td>
                                @if (auth('admin')->check())
                                    <td>
                                        <a href="{{ route('admin.owner.show',$owners[$key]['id']) }}">{{ $owners[$key]['name'] }}</a>
                                    </td>
                                @endif
                                <td>
                                    @can('Role#Staff edit')
                                        <a href="{{ route('role.edit',$row->id) }}">{{ ucfirst($row->name) }}</a>
                                    @else
                                        {{ ucfirst($row->name) }}
                                    @endcan
                                </td>
                                <td>{{ $row->users_count }}</td>
                                <td>{{ $row->permissions_count }}</td>
                                <td>
                                    @can('Role#Staff edit')
                                        <a href="{{ route('role.edit',$row->id) }}" class="btn btn-primary">
                                            <i class="fa fa-edit" aria-hidden="true"></i>
                                        </a>
                                    @endcan
                                    @can('Role#Staff delete')
                                        <a onclick="return confirm('{{ _trans('Are you sure you want to delete ?') }}');" href="{{ route('role.destroy',$row->id) }}" class="btn btn-primary">
                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                        </a>
                                    @endcan
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $roles->appends(request()->query())->links('layouts.partials.pagination') }}
                @if( $roles->count() == 0)
                    <div class="empty-data">
                        <img src="{{ asset('assets')}}/images/nodata.svg">
                        <h4>{{ _trans('No_data_to_show')}}</h4>
                    </div>
                @endif
            </div>

        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection
