@extends('layouts.master')

@section('title',_trans('Staff'))


@section('content')
    <div class="container-fluid">
        <div class="page-title">
            <div class="row">
                <div class="col-6 col-sm-6">
                    <h3>{{ $edit ? _trans('Edit Staff') : _trans('Add Staff') }}</h3>
                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid default-dash">
        <div class="row">
            <div class="col-12 col-sm-6">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                        </a>
                    </li>
                    @canany(['Staff list','Staff edit','Staff delete'])
                        <li class="breadcrumb-item ">
                            <a href="{{ route('staff.index') }}">{{ _trans('Staff') }}</a>
                        </li>
                    @endcanany
                    <li class="breadcrumb-item active">
                        {{ $edit ? _trans('Edit Staff') : _trans('Add Staff') }}
                    </li>
                </ol>
            </div>

            <div class="col-md-12">
                @if ($edit)
                    {{ Form::open(['route' => ['staff.update',$staff->id],'method' => 'PUT','files' => true,'class' => 'form mb-15 form-submit','id' =>'kt_contact_form']) }}
                    {{ Form::hidden('id',$staff->id) }}
{{--                    {{ Form::hidden('user_id',$staff->user_id) }}--}}
                @else
                    {{ Form::open(['route' => 'staff.store','method' => 'POST','files' => true,'class' => 'form mb-15','id' =>'kt_contact_form']) }}
                @endif
                <div class="row g-3">
                    <div class="col-md-4">
                        <label id="code" class="form-label" for="">{{ _trans('Staff Code') }}</label>
                        <input id="code"
                               disabled
                               type="text"
                               class="form-control"
                               placeholder="{{ _trans('Staff Code') }}"
                               value="{{ getCodeTable('Staff','shop_staff',$edit,$edit ? $staff->id : null) }}"
                        >
                    </div>

                    @if ($edit)
                        <div class="col-md-4">
                            <label id="date" class="form-label" for="">{{ _trans('Date') }}</label>
                            <input id="date"
                                   disabled
                                   type="text"
                                   class="form-control"
                                   value="{{ $staff->created_at->format('d-m-Y') }}"
                            >
                        </div>
                    @endif

                    <div class="col-md-4">
                        <label class="form-label" for="">{{ _trans('Name') }}</label>
                        <input id="name"
                               type="text"
                               name="name"
                               class="form-control @error('name') is-invalid @enderror"
                               placeholder="{{ _trans('Name') }}"
                               value="{{ old('name',$edit ? $staff->user->name : null) }}"
                        >
                        @error('name')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>


                    <div class="col-md-4">
                        <label id="phone" class="form-label" for="">{{ _trans('Phone') }}</label>
                        <input id="phone"
                               type="text"
                               name="phone"
                               class="form-control @error('phone') is-invalid @enderror"
                               placeholder="{{ _trans('Phone') }}"
                               value="{{ old('phone',$edit ? $staff->phone : null) }}"
                        >
                        @error('phone')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="form-label" for="">{{ _trans('National number') }}</label>
                        <input id="national_number"
                               type="text"
                               name="national_number"
                               class="form-control @error('national_number') is-invalid @enderror"
                               placeholder="{{ _trans('National Number') }}"
                               value="{{ old('national_number',$edit ? $staff->national_number : null) }}"
                        >
                        @error('national_number')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label id="email" class="form-label" for="">{{ _trans('E-mail') }}</label>
                        <input id="email"
                               type="email"
                               name="email"
                               class="form-control @error('email') is-invalid @enderror"
                               placeholder="{{ _trans('E-mail') }}"
                               value="{{ old('email',$edit ? $staff->user->email : null) }}"
                        >
                        @error('email')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    @if (!$edit)
                        <div class="col-md-4">
                            <label id="password" class="form-label" for="">{{ _trans('Password') }}</label>
                            <input id="password"
                                   type="password"
                                   name="password"
                                   class="form-control @error('password') is-invalid @enderror"
                                   placeholder="{{ _trans('Password') }}"
                                   value="{{ old('password') }}"
                            >
                            @error('password')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>
                        <div class="col-md-4">
                            <label id="password_confirmation" class="form-label" for="">{{ _trans('Confirm Password') }}</label>
                            <input id="password_confirmation"
                                   type="password"
                                   name="password_confirmation"
                                   class="form-control @error('password_confirmation') is-invalid @enderror"
                                   placeholder="{{ _trans('Confirm Password') }}"
                                   value="{{ old('password_confirmation') }}"
                            >
                            @error('confirm_password')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>
                    @endif

                    @if (request('guard')=='admin')
                        <div class="col-md-4">
                            <label class="form-label" for="">{{ _trans('Owner name') }}</label>
                            <select id="owner_id" name="owner_id"
                                    class="js-example-basic-single owner_id @error('owner_id') is-invalid @enderror">
                                <option value="">{{ _trans('Select owner name') }}</option>
                                @foreach($data as $owner)
                                    <option value="{{ $owner->id }}"
                                        @selected(old('owner_id',$edit ? $staff->owner_id : null) == $owner->id)
                                    >{{ $owner->user?->name }}</option>
                                @endforeach
                            </select>
                            @error('owner_id')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>

                        <div class="col-md-4">
                            <label class="form-label" for="">{{ _trans('Shop Name') }}</label>
                            <select id="shop_id" name="shop_id"
                                    class="js-example-basic-single @error('shop_id') is-invalid @enderror">
                                <option value="">{{ _trans('Select Shop Name') }}</option>

                            </select>
                            @error('shop_id')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>

                    @else
                        <div class="col-md-4">
                            <label class="form-label" for="">{{ _trans('Shop Name') }}</label>
                            <select id="shop_id" name="shop_id"
                                    class="js-example-basic-single @error('shop_id') is-invalid @enderror">
                                <option value="">{{ _trans('Select Shop Name') }}</option>
                                @foreach($data as $shop)
                                    <option value="{{ $shop->id }}"
                                            data-country="{{ $shop->country->translate(locale())?->name }}"
                                            data-governorate="{{ $shop->governorate->translate(locale())?->name }}"
                                            data-region="{{ $shop->region->translate(locale())?->name }}"
                                        @selected(old('shop_id',$edit ? $staff->shop_id : null) == $shop->id)
                                    >{{ $shop->name }}</option>
                                @endforeach
                            </select>
                            @error('shop_id')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>


                    @endif

                    <div class="col-md-4">
                        <label  class="form-label" for="">{{ _trans('Role Name') }}</label>
                        <select id="role_id" name="role_id"
                                class="js-example-basic-single @error('role_id') is-invalid @enderror" required="">
                            <option>{{ _trans('Select Role Name') }}</option>
                            @foreach($roles as $role)
                                <option value="{{ $role->id }}" @selected(old('role_id',$edit ? $staff->user->role_id : null) == $role->id)>{{ ucfirst($role->name) }}</option>
                            @endforeach
                        </select>
                        @error('role_id')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>


                    <div class="col-md-4">
                        <label class="form-label" for="">{{ _trans('Job number') }}</label>
                        <input id="job_number"
                               type="text"
                               name="job_number"
                               class="form-control @error('job_number') is-invalid @enderror"
                               placeholder="{{ _trans('Job number') }}"
                               value="{{ old('job_number',$edit ? $staff->job_number : null) }}"
                        >
                        @error('job_number')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>



                    <div class="col-md-4">
                        <label class="form-label" for="">{{ _trans('Address') }}</label>
                        <input id="address"
                               type="text"
                               name="address"
                               class="form-control @error('address') is-invalid @enderror"
                               placeholder="{{ _trans('Address') }}"
                               value="{{ old('address',$edit ? $staff->address : null) }}"
                        >
                        @error('address')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label class="form-label" for="">{{ _trans('Work starting date') }}</label>
                        <input id="work_starting_date"
                               type="date"
                               name="work_starting_date"
                               class="form-control @error('work_starting_date') is-invalid @enderror"
                               placeholder="{{ _trans('Work starting date') }}"
                               value="{{ old('work_starting_date',$edit ? $staff->work_starting_date : null) }}"
                        >
                        @error('work_starting_date')
                        <span class="text-danger">{!! $message !!} </span>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <div class="form-group form-image">
                            <label class="font-md">{{ _trans('Avatar') }}</label>
                            <img src="{{ $edit ? getAvatar($staff->user->avatar) : asset('assets/images/no-image.png') }}"
                                 class="mb-2 avatar-preview">
                            <input id="avatar"
                                   type="file"
                                   name="avatar"
                                   class="form-control avatar @error('avatar') is-invalid @enderror">
                            @error('avatar')
                            <span class="text-danger">{!! $message !!} </span>
                            @enderror
                        </div>

                    </div>


                    <div class="col-md-12">
                        <div class="m-t-50 d-flex justify-content-end">
                            <button type="submit" class="btn btn-primary">{{ $edit ?_trans('Update Staff') : _trans('Save Staff') }}</button>
                        </div>
                    </div>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection
@include('layouts.partials.read-photo',['inputName' => 'avatar'])
@push('scripts')
    <script>
        $(document).ready(function () {
            @if($edit)
            getShops({{ old('owner_id',$edit ? $staff->owner_id : 0) }}, {{ old('shop_id',$edit ? $staff->shop_id : 0) }})
            getRoles({{ old('owner_id',$edit ? $staff->owner_id : 0) }}, {{ old('role_id',$edit ? $staff->user->role_id : 0)}})
            getInfoShop({{ old('owner_id',$edit ? $staff->owner_id : 0) }}, {{ old('shop_id',$edit ? $staff->shop_id : 0) }})
            @endif

            @if(!is_null(old('owner_id')))
            getShops({{ old('owner_id') }}, {{ !is_null(old('shop_id')) ? old('shop_id') : 0 }})
            getRoles({{ old('owner_id',$edit ? $staff->owner_id : 0) }} ,{{ old('role_id',$edit ? $staff->user->role_id : 0)}})
            getInfoShop({{ old('owner_id') }}, {{ old('shop_id') }})
            @endif
        })

        $(document).on('change', '.owner_id', function (e) {
            e.preventDefault();
            $("#role_id").empty();
            $("#role_id").append('<option value="">{{ _trans('Select Role Name') }}</option>');
            var ownerId = $('.owner_id option:selected').val();
            if (ownerId == '' || ownerId == null) {
                return false;
            } else {
                getRoles(ownerId, null)
            }

        });

        $(document).on('change', '#shop_id', function (e) {
            e.preventDefault();
            $("#country,#governorate,#region").val('');
            var country = $('#shop_id option:selected').data("country");
            var governorate = $('#shop_id option:selected').data("governorate");
            var region = $('#shop_id option:selected').data("region");
            $('#country').val(country)
            $('#governorate').val(governorate)
            $('#region').val(region)

        });

        function getInfoShop(owner_id, shop_id) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                url: "{{ route('ajax.shop-by-id') }}",
                method: 'POST',
                cache: false,
                data: {
                    owner_id: owner_id,
                    shop_id: shop_id,
                },
                success: function (response) {
                    if (response.status == true) {
                        var data = response.data;
                        $('#country').val(data.country.name)
                        $('#governorate').val(data.governorate.name)
                        $('#region').val(data.region.name)
                    } else {
                        toastr.error(response.error)
                    }

                },

            });

        }
        function getRoles(ownerId, selectedId = null) {
            $("#role_id").empty();
            $("#role_id").append('<option value="">{{ _trans('Select Role name') }}</option>');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                url: "{{ route('ajax.role-by-owner') }}",
                method: 'POST',
                cache: false,
                data: {
                    owner_id: ownerId
                },
                success: function (response) {
                    if (response.status == true) {
                        if (response.data.length > 0) {
                            $.each(response.data, function (index, value) {
                                var selected = '';
                                if (selectedId == value.id) {
                                    selected = 'selected';
                                }
                                $("#role_id").append('<option ' + selected + ' value="' + value.id + '">' + value.name + '</option>');
                            });
                        }
                    } else {
                        toastr.error(response.data)
                    }

                },

            });
        }

    </script>
    @include('layouts.ajax.shop')
@endpush

