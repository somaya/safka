<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Primary Meta Tags -->
    <title>{{$data["name"]}}</title>
    <meta name="title" content="{{$data["name"]}}">
    <meta name="description" content="{{$data["description"]}}">

    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:url" content="{{$data["redirect"]}}">
    <meta property="og:title" content="{{$data["name"]}}">
    <meta property="og:description" content="{{$data["description"]}}">
    <meta property="og:image" content="{{$data["image"]}}">

    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:url" content="{{$data["redirect"]}}">
    <meta property="twitter:title" content="{{$data["name"]}}">
    <meta property="twitter:description" content="{{$data["description"]}}">
    <meta property="twitter:image" content="{{$data["image"]}}">

</head>
<body>
<script src="{{ asset('assets/') }}/js/jquery-3.5.1.min.js"></script>
<script src="{{ asset('assets/') }}/js/bootstrap.bundle.min.js"></script>
<script>
    $(document).ready(function () {
        const device = navigator.userAgent
        let link;
        let brows;
        if (/android/i.test(device)) {
            link = "{{ Utility::getValByName('link_google_play') }}"
            brows = 'Android'
        } else if ((/iPad|iPhone|iPod/.test(device)) || (navigator.platform === 'MacIntel' && navigator.maxTouchPoints > 1)) {
            link = "{{ Utility::getValByName('link_apple_store') }}"
            brows = 'iOS'
        } else {
            link = "{{ $data["redirect"] }}"
            brows = 'web'
        }
        location.href = link
    })
</script>
</body>
</html>
