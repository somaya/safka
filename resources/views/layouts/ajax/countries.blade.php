<script>
    $(document).on('change', '#country_id', function (e) {
        e.preventDefault();
        $("#governorate_id,#region_id").empty();
        $("#governorate_id").append('<option value="">{{ _trans('Select Governorate Name') }}</option>');
        $("#region_id").append('<option value="">{{ _trans('Select Region Name') }}</option>');
        var countryId = $('#country_id option:selected').val();
        if (countryId == '' || countryId == null) {
            return false;
        } else {
            getGovernorate(countryId, null)
        }

    });

    $(document).on('change', '#governorate_id', function (e) {
        e.preventDefault();
        $("#region_id").empty();
        $("#region_id").append('<option value="">{{ _trans('Select Region Name') }}</option>');
        var governorateId = $('#governorate_id option:selected').val();
        if (governorateId == '' || governorateId == null) {
            return false;
        } else {
            getRegion(governorateId, null)
        }

    });

    function getGovernorate(countryId, selectedId = null) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            url: "{{ route('ajax.governorate-by-country') }}",
            method: 'POST',
            cache: false,
            data: {
                country_id: countryId
            },
            success: function (response) {
                if (response.status == true) {
                    if (response.data.length > 0) {
                        $.each(response.data, function (index, value) {
                            var selected = '';
                            if (selectedId == value.id) {
                                selected = 'selected';
                            }
                            $("#governorate_id").append('<option ' + selected + ' value="' + value.id + '">' + value.name + '</option>');
                        });
                    }

                } else {
                    toastr.error(response.data)
                }

            },

        });
    }


    function getRegion(governorateId, selectedId = null) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            url: "{{ route('ajax.region-by-governorate') }}",
            method: 'POST',
            cache: false,
            data: {
                governorate_id: governorateId
            },
            success: function (response) {
                if (response.status == true) {
                    if (response.data.length > 0) {
                        $.each(response.data, function (index, value) {
                            var selected = '';
                            if (selectedId == value.id) {
                                selected = 'selected';
                            }
                            $("#region_id").append('<option ' + selected + ' value="' + value.id + '">' + value.name + '</option>');
                        });
                    }
                } else {
                    toastr.error(response.data)
                }

            },

        });
    }

</script>
