<script>
    $(document).on('click', '.delete-img', function (e) {
        e.preventDefault();
        var obj = $(this);
        var imageId = obj.attr('id');
        if (imageId == '' || imageId == null) {
            return false;
        } else {
            const text = "{{ _trans('Are you sure you want to delete this image?') }}";
            if(confirm(text) == true){
                deleteImage(imageId)
            }
        }
    });

    function deleteImage(imageId, obj) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            url: "{{ route('ajax.delete-product-image') }}",
            method: 'POST',
            cache: false,
            data: {
                image_id: imageId
            },
            success: function (response) {
                if (response.status == true) {
                    if (response.data == 1) {
                        $('#image_' + imageId).remove();
                        //obj.parent().parent().remove();
                    }

                } else {
                    toastr.error(response.error)
                }

            },

        });
    }
</script>
