<script>
    //setup before functions
    var typingTimer;                //timer identifier
    var doneTypingInterval = 2000;  //time in ms, 2 seconds for example

    $(document).on('change', '#product_id', function (e) {

        e.preventDefault();
        var productId = $('#product_id option:selected').val();
        if (productId == '' || productId == null) {
            return false;
        } else {
            getProducts(productId, null)
        }

    });

    function getProducts(productId) {
        console.log('ppp',productId)
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            url: "{{ route('ajax.getProductDetails') }}",
            method: 'POST',
            cache: false,
            data: {
                product_id: productId
            },
            success: function (response) {
                if (response.status == true) {
                    if (response.data) {
                        $('#product_price_div').show();
                        $('#product_description_div').show();
                        $('#product_price').val(response.data.price);
                        $('#product_description').val(response.data.description);

                        //claculate price after
                        var $input = $('#percent');

                        //on keyup, start the countdown
                        $input.on('keyup', function () {
                            clearTimeout(typingTimer);
                            typingTimer = setTimeout(doneTyping(response.data.price,$('#percent').val()), doneTypingInterval);
                        });

                        //on keydown, clear the countdown
                        $input.on('keydown', function () {
                            clearTimeout(typingTimer);
                        });
                    }
                } else {
                    toastr.error(response.error)
                }
            },
        });
    }

    //user is "finished typing," do something
    function doneTyping (price_before,percent) {
        var price_after=(price_before - ( price_before * percent / 100 )).toFixed(2)
        $('#price_after').val(price_after)
    }
    @if($edit)
    var productId = $('#product_id').val();
    getProducts(productId);
    @endif

</script>
