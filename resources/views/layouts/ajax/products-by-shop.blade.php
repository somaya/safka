<script>
    $(document).on('change', '#shop_id', function (e) {
        e.preventDefault();
        $(".product_id").empty();
        $(".product_id").append('<option value="">{{ _trans('Select product name') }}</option>');
        var shopId = $('#shop_id option:selected').val();
        if (shopId == '' || shopId == null) {
            return false;
        } else {
            getProducts(shopId, null)
        }

    });

    function getProducts(shopId, selectedId = null, lasted = false) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            url: "{{ route('ajax.product-by-shop') }}",
            method: 'POST',
            cache: false,
            data: {
                shop_id: shopId
            },
            success: function (response) {

                if (response.status == true) {
                    if (response.data.length > 0) {
                        if (lasted == true){
                            $(".product_id").last().empty();
                            $(".product_id").last().append('<option value="">{{ _trans('Select product name') }}</option>');
                        }
                        $.each(response.data, function (index, value) {
                            var selected = '';
                            if (selectedId == value.id) {
                                selected = 'selected';
                            }
                            if (lasted == true) {
                                $(".product_id").last().append('<option ' + selected + ' value="' + value.id + '">' + value.name + '</option>');
                            } else {
                                $(".product_id").append('<option ' + selected + ' value="' + value.id + '">' + value.name + '</option>');
                            }
                        });
                    }
                } else {
                    toastr.error(response.data)
                }
            },

        });
    }

</script>
