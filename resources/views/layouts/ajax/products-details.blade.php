<script>
    let shopId = "{{ $shop_id }}";
    let selectedId = "{{ $selectedId }}";

    $(document).ready(function () {
        getProducts(shopId, selectedId)
    })

    function getProducts(shopId, selectedId = null) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            url: "{{ route('ajax.product-by-shop') }}",
            method: 'POST',
            cache: false,
            data: {
                shop_id: shopId
            },
            success: function (response) {
                if (response.status == true) {
                    if (response.data.length > 0) {
                        $.each(response.data, function (index, value) {
                            var selectedItem = '';
                            if (selectedId == value.id) {
                                selectedItem = 'selected';
                            }
                            if (selectedId == '' || selectedId == null) {
                                $(".product_id").append('<option value="' + value.id + '">' + value.name + '</option>');
                            } else {
                                $(".product_key_{{$key}}").append('<option ' + selectedItem + ' value="' + value.id + '">' + value.name + '</option>');
                            }
                        });
                    }
                } else {
                    toastr.error(response.data)
                }
            },

        });
    }

</script>



