<script>
    $(document).on('change', '#owner_id', function (e) {
        e.preventDefault();
        var ownerId = $('#owner_id option:selected').val();
        if (ownerId == '' || ownerId == null) {
            return false;
        } else {
            getShops(ownerId, null)
        }

    });

    function getShops(ownerId, selectedId = null) {
        $("#shop_id").empty();
        $("#shop_id").append('<option value="">{{ _trans('Select shop name') }}</option>');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            url: "{{ route('ajax.shop-by-owner') }}",
            method: 'POST',
            cache: false,
            data: {
                owner_id: ownerId
            },
            success: function (response) {
                if (response.status == true) {
                    if (response.data.length > 0) {
                        $.each(response.data, function (index, value) {
                            var selected = '';
                            if (selectedId == value.id) {
                                selected = 'selected';
                            }
                            $("#shop_id").append('<option ' + selected + ' value="' + value.id + '">' + value.name + '</option>');
                        });
                    }
                } else {
                    toastr.error(response.data)
                }

            },

        });
    }

</script>
