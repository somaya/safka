@component('mail::message')
# Rest Password Account
Hi, {{ $data['data']['name'] }} <br>
Forget your password ?
<br>
We received a request to reset the password for your account
<br>
To reset your password, click on the button below:
@component('mail::button', ['url' => $data['data']['url']])
Rest password
@endcomponent
Or<br>
copy this link
<a target="_blank" href="{{ $data['data']['url'] }}">  {{ $data['data']['url'] }}</a>
Thanks,<br>
for use Website
{{ Utility::getValByName('company_name_'.locale()) }}
@endcomponent
