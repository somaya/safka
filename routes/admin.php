<?php

use App\Http\Controllers\Admin\AuthController;
use App\Http\Controllers\Admin\BannerController;
use App\Http\Controllers\Admin\BlogCategoryController;
use App\Http\Controllers\Admin\BlogController;
use App\Http\Controllers\Admin\BranchTypeController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\ChatController;
use App\Http\Controllers\Admin\CompanyController;
use App\Http\Controllers\Admin\ContactUsController;
use App\Http\Controllers\Admin\CountryController;
use App\Http\Controllers\Admin\CouponController;
use App\Http\Controllers\Admin\CurrencyController;
use App\Http\Controllers\Admin\CustomerController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\DayController;
use App\Http\Controllers\Admin\EmployeeController;
use App\Http\Controllers\Admin\GovernorateController;
use App\Http\Controllers\Admin\JoinUsController;
use App\Http\Controllers\Admin\LanguageController;
use App\Http\Controllers\Admin\NotificationController;
use App\Http\Controllers\Admin\OurPartnerController;
use App\Http\Controllers\Admin\OwnerController;
use App\Http\Controllers\Admin\PageController;
use App\Http\Controllers\Admin\PaymentController;
use App\Http\Controllers\Admin\RatesController;
use App\Http\Controllers\Admin\RegionController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\ServiceController;
use App\Http\Controllers\Admin\SettingController;
use App\Http\Controllers\Admin\ShopController;
use App\Http\Controllers\Admin\SocialMediaController;
use App\Http\Controllers\Admin\VehicleTypesController;
use App\Http\Controllers\Dashboard\DiscountController;
use Illuminate\Support\Facades\Route;

Route::middleware(['guest:admin'])->controller(AuthController::class)->group(function () {
    Route::get('/', fn() => redirect()->route('dashboard'));
    Route::get('login', 'showLoginForm')->name('login');
    Route::post('login', 'login')->name('login');
    Route::get('rest/password', 'resetPassword')->name('reset.password');
    Route::post('rest/password', 'postResetPassword')->name('reset.password');
    Route::get('rest/password/{token}', 'reset')->name('reset');
    Route::post('rest/password/{token}', 'postReset')->name('reset');
});

Route::middleware(['auth:admin', 'check-status-admin'])->group(function () {
    Route::redirect('/admin', '/admin/dashboard');

    Route::get('logout', [AuthController::class, 'logout'])->name('logout');
    Route::get('dashboard', [DashboardController::class, 'dashboard'])->name('dashboard');
    Route::get('/per', [DashboardController::class, 'permissionAdmin']);

    /* profile */ // not working
    Route::prefix('profile')->name('profile.')->controller(AuthController::class)->group(function () {
        Route::get('account', 'profile')->name('account');
        Route::post('account', 'storeProfile')->name('account');
        Route::post('change-password', 'changePassword')->name('change-password');
    });

    /* role */
    Route::resource('role', RoleController::class)->except(['show', 'destroy']);
    Route::post('role/destroy/{id}', [RoleController::class, 'destroy'])->name('role.destroy');


    /* staff alis employee super admin */
    Route::prefix('employee')->name('employee.')->controller(EmployeeController::class)->group(function () {
        Route::post('update-status', 'updateStatus')->name('update-status');
        Route::post('change-password', 'changePassword')->name('change-password');
    });
    Route::resource('employee', EmployeeController::class)->except(['show', 'destroy']);

    /* language */
    Route::prefix('language')->name('language.')->controller(LanguageController::class)->group(function () {
        Route::post('update-status', 'updateStatus')->name('update-status');
        Route::post('update-default', 'updateDefaultStatus')->name('update-default');
        Route::get('translate/{lang}', 'translate')->name('translate');
        Route::post('translate-submit/{lang}', 'translate_submit')->name('translate-submit');
    });
    Route::resource('language', LanguageController::class);

    /* setting */
    Route::prefix('setting')->name('setting.')->controller(SettingController::class)->group(function () {
        Route::get('/', 'index')->name('index');
        Route::post('/', 'store')->name('store');
        Route::post('email', 'saveEmailSettings')->name('email');
        Route::post('test-mail', 'testSendMail')->name('send-mail');
        Route::post('pusher', 'savePusherSettings')->name('pusher');
    });

    /* day */
    Route::resource('day', DayController::class)->except(['show', 'store', 'destroy']);

    /* pages */

    Route::resource('page', PageController::class)->except(['show', 'destroy']);
    Route::prefix('page')->name('page.')->controller(PageController::class)->group(function () {
        Route::post('update-status', 'updateStatus')->name('update-status');
        Route::get('destroy/{id}', 'destroy')->name('destroy');
    });
    /* coupon */
    Route::prefix('coupon')->name('coupon.')->controller(CouponController::class)->group(function () {
        Route::post('update-status', 'updateStatus')->name('update-status');
    });
    Route::resource('coupon', CouponController::class)->except(['show']);
    /* services */
    Route::prefix('service')->name('service.')->controller(ServiceController::class)->group(function () {
        Route::post('/update-status', 'updateStatus')->name('update-status');
        Route::post('/delete-image', 'deleteImage')->name('delete-image');
    });
    Route::resource('service', ServiceController::class)->except(['destroy']);
    /* currency */
    Route::resource('currency', CurrencyController::class)->except(['show']);
    Route::post('currency/update-status', [CurrencyController::class, 'updateStatus'])->name('currency.update-status');

    /* country */
    Route::resource('country', CountryController::class)->except(['show']);
    Route::post('country/update-status', [CountryController::class, 'updateStatus'])->name('country.update-status');

    /* governorate */
    Route::post('governorate/update-status', [GovernorateController::class, 'updateStatus'])->name('governorate.update-status');
    Route::resource('governorate', GovernorateController::class)->except(['show']);

    /* regions */
    Route::post('region/update-status', [RegionController::class, 'updateStatus'])->name('region.update-status');
    Route::resource('region', RegionController::class)->except(['show']);

    /* branch-type */
    Route::prefix('branch-type')->name('branch-type.')->controller(BranchTypeController::class)->group(function () {
        Route::post('update-status', 'updateStatus')->name('update-status');
        Route::post('update-published', 'updatePublished')->name('update-published');
    });
    Route::resource('branch-type', BranchTypeController::class)->except(['show', 'destroy']);

    /* category */
    Route::prefix('category')->name('category.')->controller(CategoryController::class)->group(function () {
        Route::post('update-status', 'updateStatus')->name('update-status');
    });
    Route::resource('category', CategoryController::class)->except(['show', 'destroy']);
    /* company */
    Route::prefix('company')->name('company.')->controller(CompanyController::class)->group(function () {
        Route::post('update-status', 'updateStatus')->name('update-status');
    });
    Route::resource('company', CompanyController::class)->except(['show', 'destroy']);
    /* owners */
    Route::prefix('owner')->name('owner.')->controller(OwnerController::class)->group(function () {
        Route::post('update-status', 'updateStatus')->name('update-status');
        Route::post('change-password', 'changePassword')->name('change-password');
    });
    Route::resource('owner', OwnerController::class)->except(['destroy']);

    /* customer */
    Route::controller(CustomerController::class)->prefix('customer')->name('customer.')->group(function () {
        Route::get('/', 'index')->name('index');
        Route::get('/show/{id}', 'show')->name('show');
        Route::post('update-status', 'updateStatus')->name('update-status');
        Route::get('export-excel', 'export')->name('export-excel');
        Route::post('change-password', 'changePassword')->name('change-password');



    });
    /* rate */
    Route::get('rate/delete/{id}', [RatesController::class, 'destroy'])->name('rate.destroy');
    Route::resource('rate', RatesController::class)->except(['destroy']);
    Route::prefix('rate')->name('rate.')->group(function () {
        Route::post('rate/update-status', [RatesController::class, 'updateStatus'])->name('update-status');

    });

    /* contact us */
    Route::resource('contact-us', ContactUsController::class)->except(['show', 'destroy']);
    Route::get('contact-us/{id}', [ContactUsController::class, 'destroy'])->name('contact-us.destroy');
    /* our-partner */
    Route::prefix('our-partner')->name('our-partner.')->controller(OurPartnerController::class)->group(function () {
        Route::post('update-status', 'updateStatus')->name('update-status');
    });
    Route::resource('our-partner', OurPartnerController::class)->except(['destroy']);
    /* shop */
    Route::prefix('shop')->name('shop.')->controller(ShopController::class)->group(function () {
        Route::post('/update-status', 'updateStatus')->name('update-status');
    });
    Route::resource('shop', ShopController::class)->except(['destroy']);

    /* testing */





    /* banners */
    Route::prefix('banner')->name('banner.')->controller(BannerController::class)->group(function () {
        Route::post('update-status', 'updateStatus')->name('update-status');
    });
    Route::resource('banner', BannerController::class)->except(['destroy']);
    /* social media */
    Route::prefix('social-media')->name('social-media.')->controller(SocialMediaController::class)->group(function () {
        Route::post('update-status', 'updateStatus')->name('update-status');
    });
    Route::resource('social-media', SocialMediaController::class)->except(['destroy']);
    /* blog category */
    Route::prefix('blog-category')->name('blog-category.')->controller(BlogCategoryController::class)->group(function () {
        Route::post('update-status', 'updateStatus')->name('update-status');
    });
    Route::resource('blog-category', BlogCategoryController::class)->except(['destroy']);
    /* blogs */
    Route::prefix('blogs')->name('blogs.')->controller(BlogController::class)->group(function () {
        Route::post('update-status', 'updateStatus')->name('update-status');
    });
    Route::resource('blogs', BlogController::class)->except(['destroy']);
    /* vehicle-type */
    Route::prefix('vehicle-type')->name('vehicle-type.')->controller(VehicleTypesController::class)->group(function () {
        Route::post('update-status', 'updateStatus')->name('update-status');
    });
    Route::resource('vehicle-type', VehicleTypesController::class)->except(['show']);
    /* join-us */
    Route::controller(JoinUsController::class)->name('join-us.')->prefix('join-us')
        ->group(function () {
            Route::get('/', 'index')->name('index');
            Route::post('/', 'store')->name('store');
            Route::get('/view/{id}', 'show')->name('show');
            Route::get('/delete/{id}', 'delete')->name('delete');
        });
    /* notification */
    Route::controller(NotificationController::class)->name('notification.')->prefix('notification')
        ->group(function () {
            Route::get('/', 'index')->name('index');
            Route::post('/', 'store')->name('store');
            Route::get('/all', 'notificationAdmin')->name('all');
        });
    /* chat admin */
    Route::controller(ChatController::class)->name('chat.')->prefix('chat')->group(function () {
        Route::get('/', 'index')->name('index');
        Route::post('/', 'messages')->name('messages');
        Route::post('/message', 'sendMessage')->name('message');
        Route::post('/search', 'search')->name('search');
    });
    /* payment */
    Route::prefix('payment')->name('payment.')->controller(PaymentController::class)->group(function () {
        Route::get('/', 'index')->name('index');
        Route::get('/invoice/{id}', 'invoice')->name('invoice');
        Route::post('/', 'store')->name('store');
    });
});
