<?php

use App\Http\Controllers\Ajax\AjaxController;
use Illuminate\Support\Facades\Route;

Route::post('governorate-by-country', [AjaxController::class, 'governorates'])->name('governorate-by-country');
Route::post('region-by-governorate', [AjaxController::class, 'regions'])->name('region-by-governorate');
Route::post('sub-category-by-category', [AjaxController::class, 'subCategories'])->name('sub-category-by-category');
Route::post('delete-product-image', [AjaxController::class, 'deleteProductImage'])->name('delete-product-image');
Route::post('product-by-shop', [AjaxController::class, 'products'])->name('product-by-shop');
Route::post('transaction-by-restaurant', [AjaxController::class, 'transactions'])->name('transaction-by-restaurant');
Route::post('offer-by-restaurant', [AjaxController::class, 'offers'])->name('offer-by-restaurant');
Route::post('save-by-restaurant', [AjaxController::class, 'saves'])->name('save-by-restaurant');
Route::post('shop-by-owner', [AjaxController::class, 'shops'])->name('shop-by-owner');
Route::post('role-by-owner', [AjaxController::class, 'roles'])->name('role-by-owner');
Route::post('shop-by-id', [AjaxController::class, 'shop'])->name('shop-by-id');
Route::post('role-by-id', [AjaxController::class, 'role'])->name('role-by-id');
Route::post('getProductDetails', [AjaxController::class, 'getProductDetails'])->name('getProductDetails');
Route::post('getResourceData', [AjaxController::class, 'getResourceData'])->name('getResourceData');
Route::post('sub-theme-by-branch-type', [AjaxController::class, 'getSubThemeByBranchType'])->name('sub-theme-by-branch-type');
Route::get('get-customer-token', [AjaxController::class, 'customerTokens'])->name('get-customer-token');
Route::get('owners', [AjaxController::class, 'owners'])->name('owners');
Route::post('drivers-orders-by-shop', [AjaxController::class, 'driverOrderByShop'])->name('drivers-orders-shop');
Route::post('delete-area', [AjaxController::class, 'deleteArea'])->name('delete-area');
