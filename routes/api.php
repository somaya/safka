<?php

use App\Http\Controllers\Api\AddressController;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\BannerController;
use App\Http\Controllers\Api\CartController;
use App\Http\Controllers\Api\HomeController;
use App\Http\Controllers\Api\ListingController;
use App\Http\Controllers\Api\LiveChatController;
use App\Http\Controllers\Api\LocationController;
use App\Http\Controllers\Api\MessageController;
use App\Http\Controllers\Api\OrderController;
use App\Http\Controllers\Api\BlogController;
use App\Http\Controllers\Api\OurPartnerController;
use App\Http\Controllers\Api\ShopController;
use Illuminate\Support\Facades\Route;

Route::get('languages', [HomeController::class, 'languages']);
Route::post('get-share-link', [HomeController::class, 'getShareLink']);

/*Route::get('save-token-web', [HomeController::class, 'saveToken'])->name('save-token');
Route::get('send-notifications', [HomeController::class, 'sendNotifications']);*/
Route::get('countries', [HomeController::class, 'countries'])->name('countries');
Route::get('governorate-by-country', [HomeController::class, 'governorates'])->name('governorates');
Route::get('region-by-governorate', [HomeController::class, 'regions'])->name('regions');
Route::get('pages', [HomeController::class, 'pages'])->name('pages');
Route::get('services', [HomeController::class, 'services'])->name('services');
Route::get('web-home', [HomeController::class, 'webHome'])->name('web-home');
Route::get('banners', [BannerController::class, 'banners']);
Route::get('blogs', [BlogController::class, 'blogs']);
Route::get('product-colors', [ShopController::class, 'productColors']);
Route::get('product-sizes', [ShopController::class, 'productSizes']);
Route::get('product-colors-sizes', [ShopController::class, 'productColorsAndSizes']);
Route::get('blogsByCategory/{slug}', [BlogController::class, 'blogsByCateory']);
Route::get('blog-categories', [BlogController::class, 'blogCategory']);
Route::get('blog-details/{slug}', [BlogController::class, 'blogDetails']);
Route::get('social-media', [HomeController::class, 'socialMedia']);
Route::get('setting', [HomeController::class, 'setting']);
Route::get('count-cart-favorite', [HomeController::class, 'getCountCartOrFavorite'])->middleware(['auth:sanctum','country']);
Route::get('our-partners', OurPartnerController::class);


Route::group(['middleware' => ['guest']], function () {
    Route::post('verification-code', [AuthController::class, 'sendVerificationCode'])->name('verification-code');
    Route::post('register', [AuthController::class, 'register'])->name('register');
    Route::post('login', [AuthController::class, 'login'])->name('login');
    Route::post('check/social', [AuthController::class, 'checkSocial'])->name('check-social');
    Route::post('login/social', [AuthController::class, 'loginSocial'])->name('login-social');
    Route::post('forget-password-web', [AuthController::class,'forgetPasswordWeb'])->name('forget-password-web');
    Route::post('forget-password', [AuthController::class,'forgetPassword'])->name('forget-password');
    Route::post('confirm-password', [AuthController::class,'confirm'])->name('confirm.forgot-password');
    Route::post('reset-password', [AuthController::class,'resetPassword'])->name('reset-password');
});
Route::middleware(['country'])->group(function () {
    Route::group(['prefix' => 'home'], function () {
        Route::get('/', [HomeController::class, 'home']);
        Route::get('categories', [HomeController::class, 'categories']);
        Route::get('offers', [HomeController::class, 'offers'])->middleware('country');
        Route::get('saves', [HomeController::class, 'saves']);
        Route::get('discounts', [HomeController::class, 'discounts']);
        Route::get('discounts/{slug}', [HomeController::class, 'discountDetails']);
        Route::get('saves/{slug}', [HomeController::class, 'saveDetails']);
        Route::get('offers/{slug}', [HomeController::class, 'offerDetails']);
        Route::get('branch-types', [HomeController::class, 'branchTypes']);
        Route::get('shops', [HomeController::class, 'shops']);
        Route::get('shop-search', [HomeController::class, 'shopSearch']);
        Route::post('product-search', [HomeController::class, 'productSearch']);
        Route::post('contact-us', [HomeController::class, 'contactUs'])->name('contact-us');
        Route::get('governorate-region-category', [HomeController::class, 'governorateRegionCategory']);
        Route::get('categories-by-branchType/{slug}', [HomeController::class, 'getCategoriesByBranchType']);
        Route::get('product-by-category/{category_slug}/{branch_slug}', [HomeController::class, 'productByCategory']);
        Route::get('product-by-sub-category/{subcategory_slug}/{branch_slug}', [HomeController::class, 'productBySubCategory']);
        Route::get('shop-by-governorate/{slug}', [HomeController::class, 'shopByGovernorate']);
        Route::get('shop-by-region/{slug}', [HomeController::class, 'shopByRegion']);
        Route::get('most-wanted-products', [HomeController::class, 'mostWantedProducts']);
        Route::post('join-us', [HomeController::class, 'joinUs']);
        Route::post('shop-nearby', [HomeController::class, 'nearby'])->name('shop-nearby');
        Route::post('branch-type-nearby', [HomeController::class, 'branch_type_nearby'])->name('branch-type-nearby');

        Route::post('search/shops', [HomeController::class, 'searchShops']);
        Route::post('search/search-all', [HomeController::class, 'searchAll']);
        /* delivery areas */
        Route::get('delivery-area/{shop_id}', [HomeController::class, 'deliveryArea']);
        Route::get('governorate-areas/{delivery_area_id}', [HomeController::class, 'governorateAreas']);
        Route::get('region/{governorate_id}/{delivery_area_id}', [HomeController::class, 'areaRegions']);


    });
    Route::group(['prefix' => 'shop'], function () {
        Route::get('/{slug}', [ShopController::class, 'shopDetails'])->name('shop-details');
        Route::post('/details-by-qrcode', [ShopController::class, 'shopDetailsQrcode'])->name('details-by-qrcode');
        Route::get('/{shop_slug}/subcategory/{sub_slug}', [ShopController::class, 'productsBySubCategory']);
    });
    Route::group(['prefix' => 'product'], function () {
        Route::get('/{slug}', [ShopController::class, 'productDetails'])->name('product-details');
    });

});

Route::group(['middleware' => 'auth:sanctum'], function ()
{
    Route::get('logout', [AuthController::class, 'logout'])->name('logout');
    Route::post('change-language', [AuthController::class, 'changeLanguage'])->name('change-language');
    Route::get('profile', [AuthController::class, 'profile'])->name('profile');
    Route::post('firebase-token', [AuthController::class, 'firebaseToken'])->name('firebase-token');
    Route::post('update-profile', [AuthController::class, 'updateProfile'])->name('update-profile');
    Route::post('update-password', [AuthController::class, 'updatePassword'])->name('update-password');
    Route::post('add-favourite', [HomeController::class, 'addFavourite'])->name('add-favourite');
    Route::get('delete-account', [AuthController::class, 'deleteAccount'])->name('delete-account');
    Route::get('notifications', [AuthController::class, 'notifications']);
    Route::get('read-notification', [AuthController::class,'readNotification']);
    /* chat with */
    Route::controller(MessageController::class)->name('ticket.')->prefix('ticket')->group(function () {
        Route::get('/', 'getTicket')->name('index');
        Route::post('/send', 'storeTicket')->name('store');
    });
    Route::prefix('listing')->name('listing.')->group(function () {
        Route::get('/', [ListingController::class, 'index'])->name('index');
        Route::post('store', [ListingController::class, 'store'])->name('store');
        Route::get('show', [ListingController::class, 'show'])->name('show');
        Route::post('update', [ListingController::class, 'update'])->name('update');
        Route::get('delete', [ListingController::class, 'delete'])->name('delete');
        Route::get('buying', [ListingController::class, 'buying'])->name('buying');
    });
});

    Route::group(['middleware' => ['auth:sanctum','country']], function ()
    {
        Route::post('add-rate', [HomeController::class, 'addRate'])->name('add-Rate');
        Route::get('rates', [HomeController::class, 'rates']);
        Route::get('myFavourites', [HomeController::class, 'myFavourites'])->name('myFavourites');
        Route::prefix('address')->name('address.')->group(function () {
            Route::get('/', [AddressController::class, 'index'])->name('index');
            Route::post('store', [AddressController::class, 'store'])->name('store');
            Route::get('show', [AddressController::class, 'show'])->name('show');
            Route::post('update', [AddressController::class, 'update'])->name('update');
            Route::get('delete', [AddressController::class, 'delete'])->name('delete');
        });

        Route::prefix('location')->name('location.')->group(function () {
            Route::get('/', [LocationController::class, 'index'])->name('index');
            Route::post('store', [LocationController::class, 'store'])->name('store');
            Route::get('show', [LocationController::class, 'show'])->name('show');
            Route::post('update', [LocationController::class, 'update'])->name('update');
            Route::get('delete', [LocationController::class, 'delete'])->name('delete');
        });

        /* carts */
        Route::prefix('cart')->name('cart.')->group(function () {
            Route::get('/', [CartController::class, 'index'])->name('index');
            Route::post('select-location', [CartController::class, 'selectLocation'])->name('select-location');
            Route::post('verify-phone-location', [CartController::class, 'verifyPhoneLocation'])->name('verify-phone-location');
            Route::post('coupon', [CartController::class, 'coupon'])->name('add-coupon');

            Route::post('add-product', [CartController::class, 'addProduct'])->name('add-product');
            Route::post('add-offer', [CartController::class, 'addOfferOrSave'])->name('add-offer');
            Route::post('add-save', [CartController::class, 'addOfferOrSave'])->name('add-save');
            Route::post('add-discount', [CartController::class, 'addDiscountOrExtra'])->name('add-discount');
            Route::post('add-extra', [CartController::class, 'addDiscountOrExtra'])->name('add-extra');
            Route::post('remove-model-type', [CartController::class, 'removeModelType'])->name('remove-model-type');
            Route::get('remove-all-cart', [CartController::class, 'removeAllCart'])->name('remove-all-cart');
        });

        /* orders */
        Route::controller(OrderController::class)->group(function () {
            Route::post('checkout', 'checkout')->name('checkout');
            Route::get('orders', 'orders')->name('orders');
            Route::get('order/current', 'orders')->name('order-current');
            Route::get('order/previous', 'orders')->name('order-previous');
            Route::get('order/{order_id}', 'show')->name('order-detail');
        });

        /* message */
        Route::controller(MessageController::class)->name('message.')->prefix('message')->group(function () {
            Route::get('/{orderId}', 'index')->name('index');
            Route::post('/send', 'store')->name('store');
        });
        Route::get('/chats', [MessageController::class, 'chats'])->name('chats');
        /* live chat */
        Route::get('live-chat/shops', [LiveChatController::class, 'shops']);
        Route::get('live-chat/chats/{id}', [LiveChatController::class, 'show'])->name('live-chat.chats');
        Route::apiResource('live-chat', LiveChatController::class)
            ->except(['update', 'destroy']);
    });


