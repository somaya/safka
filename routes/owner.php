<?php

use App\Http\Controllers\Admin\NotificationController;
use App\Http\Controllers\Dashboard\AuthController;
use App\Http\Controllers\Dashboard\ChatController;
use App\Http\Controllers\Dashboard\DashboardController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});
Route::middleware(['guest:web'])->controller(AuthController::class)->group(function () {
    Route::get('/', fn() => redirect()->route('dashboard'));
    Route::get('login', 'showLoginForm')->name('login');
    Route::post('login', 'login')->name('login');
    Route::get('rest/password', 'resetPassword')->name('reset.password');
    Route::post('rest/password', 'postResetPassword')->name('reset.password');
    Route::get('rest/password/{token}', 'reset')->name('reset');
    Route::post('rest/password/{token}', 'postReset')->name('reset');
});
Route::middleware(['auth:web', 'check-status-owner'])->group(function () {
    Route::redirect('/owner', '/owner/dashboard');
    Route::get('logout', [AuthController::class, 'logout'])->name('logout');
    Route::get('dashboard', [DashboardController::class, 'dashboard'])->name('dashboard');

    Route::prefix('profile')->name('profile.')->controller(AuthController::class)->group(function () {
        Route::get('account', 'profile')->name('account');
        Route::post('account', 'storeProfile')->name('account');
        Route::post('change-password', 'changePassword')->name('change-password');
    });

    /* chat admin */
    Route::controller(ChatController::class)->name('chat.')->prefix('chat')->group(function () {
        Route::get('/', 'index')->name('index');
        Route::post('/', 'messages')->name('messages');
        Route::post('/message', 'sendMessage')->name('message');
        Route::post('/search', 'search')->name('search');
    });
    Route::controller(NotificationController::class)->name('notification.')->prefix('notification')->group(function () {
        Route::get('/', 'notificationOwner')->name('index');
    });
    Route::get('payments', [DashboardController::class, 'payments'])->name('payments');

});
