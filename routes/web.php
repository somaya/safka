<?php

use App\Http\Controllers\Admin\ShopController;
use App\Http\Controllers\Dashboard\AuthController;
use App\Http\Controllers\Dashboard\DeliveryAreaController;
use App\Http\Controllers\Dashboard\DiscountController;
use App\Http\Controllers\Dashboard\DriverController;
use App\Http\Controllers\Dashboard\DriverOrderController;
use App\Http\Controllers\Dashboard\HomeController;
use App\Http\Controllers\Dashboard\LiveChatController;
use App\Http\Controllers\Dashboard\OfferController;
use App\Http\Controllers\Dashboard\OrderController;
use App\Http\Controllers\Dashboard\ProductController;
use App\Http\Controllers\Dashboard\RoleController;
use App\Http\Controllers\Dashboard\SaveController;
use App\Http\Controllers\Dashboard\StaffController;
use Illuminate\Support\Facades\Route;

Route::get('share-url', [HomeController::class, 'getSharedLinkPreview'])->name('link-preview');
Route::get('download-app', function () {
    return view('download-app');
})->name('download-app');
Route::get('lang/{locale}/{guard?}', [HomeController::class, 'language'])->name('lang');

Route::middleware(['auth:web,admin', 'auth', 'auth-user'])->group(function () {
    Route::get('role/destroy/{id}', [RoleController::class, 'destroy'])->name('role.destroy');
    Route::resource('role', RoleController::class)->except(['show', 'destroy']);

    /* staff shop */
    Route::prefix('staff')->name('staff.')->controller(StaffController::class)->group(function () {
        Route::post('update-status', 'updateStatus')->name('update-status');
        Route::post('change-password', 'changePassword')->name('change-password');
    });
    Route::resource('staff', StaffController::class)->except(['show', 'destroy']);
    /* delivery areas */
    Route::post('delivery-area/update-status', [DeliveryAreaController::class, 'updateStatus'])->name('delivery-area.update-status');
    Route::resource('delivery-area', DeliveryAreaController::class)->except(['show']);
    Route::post('area/update-status', [DeliveryAreaController::class, 'updateAreaStatus'])->name('area.update-status');

    /* orders */
    Route::controller(OrderController::class)->prefix('order')->name('order.')->group(function () {
        Route::get('/', 'index')->name('index');
        Route::get('/show/{id}/{notificationId?}', 'show')->name('show');
        Route::get('/update-status/{orderId}', 'updateStatus')->name('update-status');
        Route::post('/cancelled/{orderId}', 'updateStatus')->name('cancelled');
        Route::post('/returned/{orderId}', 'updateStatus')->name('returned');
        Route::post('/assign-driver/{orderId}', 'updateStatus')->name('assign-driver');

    });
    /* products */
    Route::prefix('product')->name('product.')->controller(ProductController::class)->group(function () {
        Route::post('/update-status', 'updateStatus')->name('update-status');
        Route::post('/update-wanted', 'updateWanted')->name('update-wanted');
        Route::post('/delete-size', 'deleteSize')->name('delete-size');
        Route::post('/delete-image', 'deleteImage')->name('delete-image');
    });
    Route::resource('product', ProductController::class)->except(['destroy']);
    Route::post('color/update-status', [ProductController::class, 'updateColorStatus'])->name('color.update-status');
    /* offers */
    Route::prefix('offer')->name('offer.')->controller(OfferController::class)->group(function () {
        Route::post('update-status', 'updateStatus')->name('update-status');
        Route::post('/delete-image', 'deleteImage')->name('delete-image');
    });
    Route::resource('offer', OfferController::class)->except(['destroy']);
    /* saves */
    Route::prefix('save')->name('save.')->controller(SaveController::class)->group(function () {
        Route::post('update-status', 'updateStatus')->name('update-status');
        Route::post('/delete-image', 'deleteImage')->name('delete-image');
    });
    Route::resource('save', SaveController::class)->except(['destroy']);
    /* discounts */
    Route::prefix('discount')->name('discount.')->controller(DiscountController::class)->group(function () {
        Route::post('update-status', 'updateStatus')->name('update-status');
    });
    Route::resource('discount', DiscountController::class)->except(['destroy']);
    /* driver */
    Route::prefix('driver')->name('driver.')->controller(DriverController::class)->group(function () {
        Route::post('update-status', 'updateStatus')->name('update-status');
        Route::post('change-password', 'changePassword')->name('change-password');
    });
    Route::resource('driver', DriverController::class);
    Route::prefix('profile')->name('profile.')->group(function () {
        Route::get('/information', [AuthController::class, 'profile'])->name('information');
        Route::post('/submit-information', [AuthController::class, 'storeProfile'])->name('submit-information');
        Route::post('change-password', [AuthController::class, 'changePassword'])->name('change-password');
    });
    Route::prefix('driver-order')->name('driver-order.')->controller(DriverOrderController::class)->group(function () {
        Route::get('/', 'index')->name('index');
        Route::get('/create', 'create')->name('create');
        Route::post('/create', 'store')->name('store');
        Route::get('/edit/{id}', 'edit')->name('edit');
        Route::post('/update', 'update')->name('update');
    });
    Route::controller(LiveChatController::class)->name('live-chat.')->prefix('live-chat')
        ->group(function () {
            Route::get('index/{shopId?}/{conversionId?}', 'index')->name('index');
            Route::get('conversations', 'conversations')->name('conversations');
            Route::post('store', 'store')->name('store');
            Route::get('show', 'show')->name('show');
        });

});
Route::middleware(['guest'])->group(function () {
    Route::get('/', function () {
        return redirect()->route('admin.login');
    });
    Route::prefix('user')->name('user.')->group(function () {
        Route::get('rest/password', [\App\Http\Controllers\Auth\AuthController::class, 'resetPassword'])->name('get.reset.password');
        Route::post('rest/password', [\App\Http\Controllers\Auth\AuthController::class, 'postResetPassword'])->name('reset.password');
        Route::get('rest/password/{token}', [\App\Http\Controllers\Auth\AuthController::class, 'getReset'])->name('reset');
        Route::post('rest/password/{token}', [\App\Http\Controllers\Auth\AuthController::class, 'postReset'])->name('reset');
    });
});
Route::get('qr-code/{filename}', [ShopController::class, 'downloadQrCode'])->name('download.qr-code');
Route::get('/per', [\App\Http\Controllers\Admin\DashboardController::class, 'permissionAdmin']);
Route::get('/owner-per', [\App\Http\Controllers\Admin\DashboardController::class, 'permissionOwner']);



